#!/usr/bin/env bash

if [ -z "$1" ]; then
	echo "User name to authenticate with marathon is required"
    exit -1
fi
uname=$1

if [ -z "$2" ]; then
	echo "Password to authenticate with marathon is required"
    exit -1
fi
pwd=$2

if [ -z "$3" ]; then
	echo "Marathon URL is required"
    exit -1
fi
marathonUrl=$3

if [ -z "$4" ]; then
    echo "Marathon json file name is required"
    exit -1
fi
marathonJson=$4

function delete() {
    appid=$(cat ./deploy/$marathonJson | jq -r .id)
    echo "Deleting App - $appid"  
    curl --user $uname:$pwd -k -X DELETE $marathonUrl/v2/apps/$appid
}

function deploy() {
    curl --user ${uname}:${pwd} -k -X POST -H "Content-Type: application/json" $marathonUrl/v2/apps -d@deploy/$marathonJson
}

delete

echo 'sleeping for 10 seconds...'
sleep 10s

deploy
