# Pull Request webhook

This repo has a webhook configured to validate the pull request, to submit swagger documents to a linter, and do basic validation on sequence diagrams whenever a Pull Request is created or updated.  Sometimes things go wrong.  Here are some things you can check if you are getting an error or if your Pull Request is being declined.

### Does your pull request effect anything outside of the swagger, sequence_diagrams or wsdl folders?
If so then your pull request will be automatically declined.  There should rarely be a need for this to occur.  If you need assistance please ask in the clearwater slack channel.

### Is your repo public?
The linter requires access to your files in order to lint them.  If your pull request is referencing a private repo or fork then it will fail.
Here is how to make it public.
1. Go to your forked repository. The url is probably
https://bitbucket.service.edp.t-mobile.com/users/<name>/repos/flow-documentation/browse
2.    Click "Repository Settings" in Left pane.
3.    Click Security > Repository permissions : Public Access > Check "Enable"

### How does the swagger linting work?
Only files with a suffix of json, yaml, or yml that are in the swagger folder will be submitted to the swagger linter. 
The linter is located [here](https://api-linter.devcenter.t-mobile.com/).  As long as your files are public and get a score of at least 1 your pull request will not be auto-declined.  It is up to the approver to determine if the score reported from the linter is acceptable to complete the merge.

### How does the sequence diagram validation work?
Only files with a suffix of txt or puml in the sequence_diagrams folder will have basic valdation performed.
The first line of text must contain only
```
@startuml
```
and the last line of text must contain only
```
@enduml
```

### I corrected my problems and now I need to trigger the pull request webhook again. What can I do?
The webhook will automatically trigger again when the PR is updated.  An easy way to do this may be to edit the PR then
modify the Description, Title, or reviewers.  If your pull request was declined then clicking on Re-Open will also trigger the webhook. 

