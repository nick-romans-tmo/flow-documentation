# WSDL Best Practices

## Overview

WSDL is used as SOA (Service Oriented Architecture) aligned Service Interface Definition and it is in use for a very long time at T-Mobile.  Digital Services team has historically defined majority of SOAP services in use at T-Mobile and can be further engaged for clarifications and codifying additional Best Practices and Standards for SOAP services.


## Decisioning Framework

1. WSDL should be self-explanatory and be able to be understood by all stake-holders:  client as well as other fellow developers.
1. Have uniformity and consistency in the WSDL interfaces with respect to model objects and design approach. Consider setting up small group of WSDL reviewer SMEs to achieve this.
1. Promote understandability and usability of the interface.
1. Service behavior can be easily comprehended - which means Operations defined serve a clear and single purpose i.e. least overloaded behavior as possible.
1. WSDLs use XSD schema to define interfaces, take advantage of rich XSD schema capabilities to write a great interface.

## Responsibilities 

Each of us have a responsibility in delivering quality output.  This review checklist applies to the person(s) creating the WSDL as well as the person reviewing the overall design. 


## Guidelines


Level | Description 
---|---
General | Elements, documentations, & annotations do not refer to specific consumer, system, application, project, etc.
General | For any new element:  Element names are appropriate for what they represent/context and does not cause confusion.  Element names are system/company agnostic. 
General | For any new schema:  If a schema represents basic information, object is named as “…Summary” or “…BasicInfo” or “…Info”.  This will allow for extension to add additional detailed elements needed in future, with a detail object that extends summary object. 
General | Naming of boolean elements represent a situation that results in a true/false., ie, active or closedAccount.  Boolean elements should NOT be prefixed with “is” (ex., isActive) or “indicator” 
 | 
Service | Service “Port Type” is documented to provide the high level definition, purpose, vision, as well as scope limitations of the service as appropriate. 
Service | Service is defined with the appropriate target namespace. 
Service | Service is properly named to represent a business process or business domain “object”.  Naming of the service should be kept at a high level but should have a boundary of scope.
 | 
Operation | Interface design Documentation has been completed for the operation and is accurately represented; Exception scenarios/codes has been well documented.
Operation | Operation is documented/annotated properly with the following:  detailed purpose/functionality provided by the operation, special behaviors, limitations as well as special expectations from the consumer of the service, and other instructions that helps the consumer as well as other resources supporting the service easier to understand. 
Operation | Operation name is properly defined and is included in the appropriate service per the domain SMEs guidance and utilizing business terms where possible.  Operation naming should provide an immediate understanding of the overall purpose without providing the very specifics.  Operation naming should not be confusing by using terminology that is unusual or non-business term. 
Operation | Operation naming follows proper convention of beginning with lowercase, utilizing camel case, and correct spelling; abbreviations such as BAN or MSISDN should not be presented with all caps.  An exception to this rule may be in order to keep the new operation in-line with an existing service’s precedence. 
Operation | Operation naming is not specific to a parameter that limits the extendibility of the operation, ie,  LookupCustomerByBan, GetHandsetOrdersForBan, etc.,  Exceptions to this must be approved by domain SMEs. 
 | 
Request | Each element in the request object, no matter how simple or obvious, is properly documented as to what it represents or means.  Repetition of the element name in the annotation does not constitute documentation (ie., element name is OfferSection and description is “Represents Offer Section” is NOT acceptable) .   
Request | Each element name of the request is applied/named correctly – with first letter starting with lowercase, camel case.  Abbreviations are not to be used other than for “ban” and “msisdn”. 
Request | Each element within the request schema has proper multiplicity/required/optional parameters that provides easy validation and comprehension of the interface.
Request | Element documentation provides additional information to the consumer on how to “consume”/populate the value, including any assumptions that will be made if a value is not provided.  For example,  “Date from which transaction history needs to be retrieved (going in reverse chronological order). This date is used in conjunction with the numberOfBillCycles to retrieve the list of transactions. The date can be either the bill cycle end date or the current system date.When no date is provided in the input it is defaulted to the system date.” OR “Provide the number of cycles in the past(BILLED CYCLE) for which transaction history is to be retrieved.” 
Request | Elements for which there are default behavior/assumptions made (when consumer does not provide them) must be clearly documented to that effect.   This includes default behavior for an enumeration, boolean or quantity when not provided. (for example, “When not provided, this value will be defaulted to false.” Or “By default, the service will not return this information.” Or “If not provided, a value of 5000 will be defaulted by the service”).
Request | Elements in the request object are of proper data types; avoids the usage of “integer” (“int” is safer) and reuses existing types; Elements utilize existing simple data types or create new ones to represent common re-usable types (for example, BAN).  This provides consistency in the datatype, definition, as well as a single place to modify anything in regards to this datatype.   
Request | Enumeration in requests is encouraged when possible.  Ensure that the values in the enumeration do not change often.  Enumeration values should be all caps.  
Request | Schema object representing the request extends the base request object (RspRequest, Tibco Header, etc., ) 
Request | Schema object representing the request has been documented with information as to its overall purpose (ex., “Request schema to facilitate the retrieval of basic customer account information”) 
Request | Schema object representing the request is properly named and begins with capital letter, and represents a noun (ie., DeviceCatalogRetrievalRequest [good] vs GetDeviceCatalogRequest [bad]) 
Request | Various schema elements (Sequence, CHOICE, etc) are utilized to denote the different scenarios of the interface versus a series of optional fields that leads to an interface that is confusing and hard to consume.   If this is not possible, documentation shall be present to provide instructions to the client on how to populate for the different scenarios. 
 | 
Response | Each element in the response schema, no matter how simple or obvious, is properly documented as to what it represents or means.  Repetition of the element name in the annotation does not constitute documentation (ie., element name is OfferSection and description is “Represents Offer Section” is NOT acceptable) .   
Response | Each element name of the response is applied/named correctly – with first letter starting with lowercase, camel case 
Response | Each element within the response schema has proper multiplicity/required/optional parameters that provides information in the best possible way  
Response | Elements in the response schema are of proper data types; reuses existing types or creates new simple types for data that is repeated in multiple places such as BAN, MSISDN, etc.
Response | Elements in the response schema should generally be optional.  Only provide a required element or attribute if it is guaranteed to be available in the response 
Response | Ensure that the elements in the response are optional or structured in a manner that does not result in a schema validation error if there should be an error being returned. 
Response | Existing complexTypes are utilized or new ones are introduced to represent a business term/object. 
Response | Modified or newly introduced complexTypes have all of the elements are  clearly documented without just repeating the element name;  
Response | Schema object representing the response extends the base response object (RspResponse, etc) 
Response | Schema object representing the response extends the default header and does not contain any required elements/complexTypes at the root level. 
Response | Schema object representing the response has been documented with information as to its overall purpose (ex., “Response schema providing basic information about customer account”) 
Response | Schema object representing the response is properly named and begins with capital letter, and represents a noun (ie., DeviceCatalogResponse [good] vs GetDeviceCatalogResponse [bad]) 
Response | Various schema elements (Sequence, CHOICE, etc) elements are utilized to best represent the output in a manner that is not confusing as to what to expect.  When this is absolutely not possible, documentation shall be present to denote the expected output for various scenarios.   




## Additional Information

- reach out to Digital Services Group (DSGDICE)
