swagger: '2.0'
info:
  description: Autoconfig Template Processor API
  version: 1.0.0
  title: Autoconfig Processor
  contact: {}
  license:
    url: 'http://unlicense.org'
host: autoconfig-processor.px-npe1103.pks.t-mobile.com
basePath: /autoconfig
tags:
  - name: configuration-api-controller
    description: the configuration API
  - name: ping-api-controller
    description: the ping API
  - name: registration-api-controller
    description: the registration API
  - name: template-api-controller
    description: the template API
paths:
  '/configuration/environment/{environmentName}':
    post:
      tags:
        - AutoConf
        - configuration-api-controller
      summary: deployConfiguration
      operationId: deployConfiguration
      consumes:
        - application/json
      produces:
        - application/json
        - text/plain
      parameters:
        - in: body
          name: configurationRequest
          description: configurationRequest
          required: true
          schema:
            $ref: '#/definitions/ConfigurationRequest'
        - name: environmentName
          in: path
          description: Requested deployment environment ID
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/ConfigurationReport'
        '201':
          description: Template converted to configuration for environment
          schema:
            $ref: '#/definitions/ConfigurationReport'
        '400':
          description: '400, Bad Request, values in request are not known.'
          schema:
            type: array
            items:
              type: string
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
  /ping:
    get:
      tags:
        - Monitoring
      summary: pingGet
      operationId: pingGet
      produces:
        - text/plain
      responses:
        '200':
          description: OK
          schema:
            type: string
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
  '/registration/environment/{environmentName}':
    post:
      tags:
        - AutoConf
        - registration-api-controller
      summary: registerAsset
      operationId: registerAsset
      consumes:
        - application/json
      produces:
        - application/json
        - text/plain
      parameters:
        - name: environmentName
          in: path
          description: Requested deployment environment ID
          required: true
          type: string
        - in: body
          name: registrationRequest
          description: registrationRequest
          required: true
          schema:
            $ref: '#/definitions/RegistrationRequest'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/RegistrationReport'
        '201':
          description: Asset registered for environment
          schema:
            $ref: '#/definitions/RegistrationReport'
        '400':
          description: '400, Bad Request, values in request are not known.'
          schema:
            type: array
            items:
              type: string
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
  /template/publication:
    post:
      tags:
        - AutoConf
        - template-api-controller
      summary: publishTemplate
      operationId: publishTemplate
      consumes:
        - application/json
      produces:
        - application/json
        - text/plain
      parameters:
        - in: body
          name: templateRequest
          description: templateRequest
          required: true
          schema:
            $ref: '#/definitions/TemplateRequest'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/TemplateReport'
        '201':
          description: Template saved
          schema:
            $ref: '#/definitions/TemplateReport'
        '400':
          description: '400, Bad Request, values in request are not known.'
          schema:
            type: array
            items:
              type: string
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
  /template/validation:
    post:
      tags:
        - AutoConf
        - template-api-controller
      summary: validateTemplate
      operationId: validateTemplate
      consumes:
        - application/json
      produces:
        - application/json
        - text/plain
      parameters:
        - in: body
          name: templateRequest
          description: templateRequest
          required: true
          schema:
            $ref: '#/definitions/TemplateRequest'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/TemplateReport'
        '201':
          description: Template validated
          schema:
            $ref: '#/definitions/TemplateReport'
        '400':
          description: '400, Bad Request, values in request are not known.'
          schema:
            type: array
            items:
              type: string
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
definitions:
  AssetCheck:
    type: object
    properties:
      message:
        type: string
      validAsset:
        type: boolean
    title: AssetCheck
  AssetIdentity:
    type: object
    properties:
      apiVersion:
        type: integer
        format: int32
        description: 'The version of the API, indicates back-compatibility.'
      assetName:
        type: string
        description: The key name of an asset (application or service).
      endpointType:
        type: string
        enum:
          - DATABASE
          - PROXY
          - QUEUE
          - REST
          - SOAP
          - UI
          - OTHER
      groupName:
        type: string
        description: The unique namespace of an asset (application or service). It represents a logical domain of development.
    title: AssetIdentity
  ConfigurationReport:
    type: object
    properties:
      assetIdentity:
        $ref: '#/definitions/AssetIdentity'
      dependencyResolution:
        $ref: '#/definitions/DependencyResolutionResult'
      deploymentEnvironmentName:
        type: string
      overrideResults:
        $ref: '#/definitions/OverrideResult'
      request:
        $ref: '#/definitions/ConfigurationRequest'
      results:
        $ref: '#/definitions/ProcessResult'
    title: ConfigurationReport
    description: Result of template processing
  ConfigurationRequest:
    type: object
    properties:
      configurationStore:
        $ref: '#/definitions/ConfigurationRequestConfigurationStore'
      inputTemplate:
        $ref: '#/definitions/UrlReference'
      overrideFiles:
        type: array
        items:
          type: string
      semanticVersion:
        type: string
    title: ConfigurationRequest
  ConfigurationRequestConfigurationStore:
    type: object
    properties:
      sccs:
        $ref: '#/definitions/ConfigurationRequestConfigurationStoreSccs'
    title: ConfigurationRequestConfigurationStore
  ConfigurationRequestConfigurationStoreSccs:
    type: object
    properties:
      password:
        type: string
      url:
        type: string
      user:
        type: string
    title: ConfigurationRequestConfigurationStoreSccs
  DependencyCheckResult:
    type: object
    properties:
      dependencyCount:
        type: integer
        format: int32
        description: The total count of dependencies in the template. Equal to validCount + invalidCount + pendingCount.
      invalidCount:
        type: integer
        format: int32
        description: The number of dependencies that were not known  or were invalid in the asset catalog.
      invalids:
        type: object
        description: The listing of invalid dependency checks.
        additionalProperties:
          $ref: '#/definitions/DependencyInfo'
      pendingCount:
        type: integer
        format: int32
        description: The number of dependencies that could not be checked because they were placeholder data.
      pendings:
        type: object
        description: The listing of pending dependencies that could not be checked.
        additionalProperties:
          $ref: '#/definitions/DependencyInfo'
      validCount:
        type: integer
        format: int32
        description: The number of dependencies successfully validated.
    title: DependencyCheckResult
  DependencyInfo:
    type: object
    required:
      - assetName
      - groupName
    properties:
      apiVersion:
        type: integer
        format: int32
        description: 'The version of the API, indicates back-compatibility.'
      assetName:
        type: string
        description: The key name of an asset (application or service).
      endpointType:
        type: string
        enum:
          - DATABASE
          - PROXY
          - QUEUE
          - REST
          - SOAP
          - UI
          - OTHER
      explanation:
        type: string
        description: 'A note regarding this dependency, especially if ''pending''. This required if ''isPending'' is true.'
      groupName:
        type: string
        description: The unique namespace of an asset (application or service). It represents a logical domain of development.
      isPending:
        type: boolean
        description: True if dependency a placeholder for an asset that is not known to the system.
      label:
        type: string
        description: The asset's deployment topological realm or silo.
      message:
        type: string
        description: Processor message that explains the results of processing.
      semanticVersion:
        type: string
        description: A full semantic version string like 2.0.0.1.
      url:
        type: string
        description: A simple URL.
    title: DependencyInfo
    description: The description of a dependency.
  DependencyResolutionResult:
    type: object
    properties:
      dependencyCount:
        type: integer
        format: int32
        description: The total count of dependencies in the template. Equal to resolutionSuccessCount + resolutionErrorCount + pendingCount
      overrideResolvedCount:
        type: object
        description: The number of dependencies successfully resolved  but instead used override values in the configuration. These should be checked to make sure override was intended.
      overrideUnresolvedCount:
        type: integer
        format: int32
        description: 'The number of dependencies unsuccessfully resolved  and used override values in the configuration. These may be due for registry entry, or may be need continued override.'
      overridesResolved:
        type: object
        description: The listing of dependency that used override values instead of resolution. These may be unintentional and should be investigated.
        additionalProperties:
          $ref: '#/definitions/DependencyInfo'
      overridesUnresolved:
        type: object
        description: The listing of dependency that used override values and were unresolved.
        additionalProperties:
          $ref: '#/definitions/DependencyInfo'
      pendingCount:
        type: integer
        format: int32
        description: The number of dependencies that could not be checked because they were placeholder data.
      pendings:
        type: object
        description: The listing of pending dependencies that could not be checked.
        additionalProperties:
          $ref: '#/definitions/DependencyInfo'
      resolutionErrorCount:
        type: integer
        format: int32
        description: The number of dependencies unsuccessfully resolved. This ruins a configuration!
      resolutionErrors:
        type: object
        description: The listing of failed dependency resolutions.
        additionalProperties:
          $ref: '#/definitions/DependencyInfo'
      resolutionSuccessCount:
        type: integer
        format: int32
        description: The number of dependencies successfully resolved.
    title: DependencyResolutionResult
  OverrideResult:
    type: object
    properties:
      errorCount:
        type: integer
        format: int32
      overrideCount:
        type: integer
        format: int32
      sources:
        type: array
        description: The deployed assets needed by this containing asset.
        items:
          $ref: '#/definitions/OverrideSource'
    title: OverrideResult
  OverrideSource:
    type: object
    properties:
      overrides:
        type: object
        description: The map of properties overridden by the source file.
        additionalProperties:
          $ref: '#/definitions/PropertyOverride'
      source:
        type: string
    title: OverrideSource
    description: The description of a property value override.
  ProcessResult:
    type: object
    properties:
      durationMillis:
        type: integer
        format: int64
      hasErrors:
        type: boolean
      message:
        type: string
      startTime:
        type: string
    title: ProcessResult
    description: Common metadata about the results of processing a request
  PropertyOverride:
    type: object
    properties:
      message:
        type: string
        description: Processor message that explains the results of the override.
      originalValue:
        type: string
      overridden:
        type: boolean
      overridingValue:
        type: string
    title: PropertyOverride
    description: The description of a property value override.
  RegistrationReport:
    type: object
    properties:
      request:
        $ref: '#/definitions/RegistrationRequest'
      results:
        $ref: '#/definitions/ProcessResult'
    title: RegistrationReport
    description: Result of registering asset for env
  RegistrationRequest:
    type: object
    properties:
      apiVersion:
        type: integer
        format: int32
        description: 'The version of the API, indicates back-compatibility.'
      assetName:
        type: string
        description: The key name of an asset (application or service).
      endpointType:
        type: string
        enum:
          - DATABASE
          - PROXY
          - QUEUE
          - REST
          - SOAP
          - UI
          - OTHER
      environmentName:
        type: string
        description: The identifying name of a deployment environment.
      groupName:
        type: string
        description: The unique namespace of an asset (application or service). It represents a logical domain of development.
      label:
        type: string
        description: The asset's deployment topological realm or silo.
      semanticVersion:
        type: string
        description: A full semantic version string like 2.0.0.1.
      url:
        type: string
        description: A simple URL.
    title: RegistrationRequest
  TemplateReport:
    type: object
    properties:
      assetCheck:
        $ref: '#/definitions/AssetCheck'
      assetIdentity:
        $ref: '#/definitions/AssetIdentity'
      dependencyCheck:
        $ref: '#/definitions/DependencyCheckResult'
      request:
        $ref: '#/definitions/TemplateRequest'
      results:
        $ref: '#/definitions/ProcessResult'
    title: TemplateReport
    description: Result of template check
  TemplateRequest:
    type: object
    properties:
      inputTemplate:
        $ref: '#/definitions/UrlReference'
      semanticVersion:
        type: string
    title: TemplateRequest
  UrlReference:
    type: object
    properties:
      url:
        type: string
        description: A simple URL.
    title: UrlReference
    description: Reference to an external URL with optional authentication information