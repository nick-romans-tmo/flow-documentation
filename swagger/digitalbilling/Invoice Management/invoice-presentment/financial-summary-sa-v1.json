{
  "swagger": "2.0",
  "info": {
    "description": "This document lists the APIs that fetches financial summary for financial account id or subscriber.",
    "version": "1.0",
    "title": "APIs retrieving list of summary of discounts, deposits,credits and adjustments, monthly recurring charges and overages."
  },
  "basePath": "/",
  "tags": [
    {
      "name": "financial-summary",
      "description": "Get summary of charges, overages, mrcs, credits and adjustments and deposits by financial account id and subscriber."
    }
  ],
  "paths": {
    "/billing/v1/invoices/summary": {
      "get": {
        "tags": [
          "financial-summary"
        ],
        "summary": "Get Charges, Collated deposits and Adjustments data for a specified financial account Id and msisdn",
        "operationId": "retrieveFinancialSummary",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "Authorization",
            "in": "header",
            "description": "OAuth 2.0 access token with the authentication type set as Bearer.",
            "required": true,
            "type": "string"
          },
          {
            "name": "activityid",
            "in": "header",
            "description": "Unique id alphanumeric value generated and sent by the partner to identify every the service request uniquely. Sample: XyzA:D1s5s1i-X6AFRvfiPBfs-1234  Pass as is to downstream calls.",
            "required": true,
            "type": "string"
          },
          {
            "name": "applicationid",
            "in": "header",
            "description": "Identifies the application, system, or tool that is being used to perform the transaction related to this request. Below are the sample values.ACUI, ESERVICE, REBELLION, WARP",
            "required": false,
            "type": "string"
          },
          {
            "name": "applicationuserid",
            "in": "header",
            "description": "NT id of the one who is managing the transaction from CARE/RETAIL channels.",
            "required": false,
            "type": "string"
          },
          {
            "name": "authcustomerid",
            "in": "header",
            "description": "Customer id _ of the person that is interacting with the system.",
            "required": false,
            "type": "string"
          },
          {
            "name": "authfinancialaccountid",
            "in": "header",
            "description": "Financial account that the customer intends to operate on, while interacting with the system.",
            "required": false,
            "type": "string"
          },
          {
            "name": "authlineofserviceid",
            "in": "header",
            "description": "Service Line that the customer intends to operate on, while interacting with the system.",
            "required": false,
            "type": "string"
          },
          {
            "name": "channelid",
            "in": "header",
            "description": "Identifies the business unit or sales channel for the Request.Below are the sample values:            Care            TeleSales            FullService            BusinessDirect            FinancePartners            NonFinancePartners            SelfServiceAuthenticated            SelfServiceAnonymous            InteractiveVoiceResponse",
            "required": false,
            "type": "string"
          },
          {
            "name": "dealercode",
            "in": "header",
            "description": "Unique code to identify the dealer/rep user.",
            "required": false,
            "type": "integer",
            "format": "int32"
          },
          {
            "name": "interactionid",
            "in": "header",
            "description": "Alphanumeric value represent a common transaction id across all calls made from UI, while completing all business activity needs of a particular customer.",
            "required": false,
            "type": "string"
          },
          {
            "name": "masterDealercode",
            "in": "header",
            "description": "Unique code to identify a group of dealers/reps.",
            "required": false,
            "type": "integer",
            "format": "int32"
          },
          {
            "name": "segmentationid",
            "in": "header",
            "description": "Identifier of customers primary data center.Sample: Polaris,Titan",
            "required": false,
            "type": "string"
          },
          {
            "name": "senderid",
            "in": "header",
            "description": "Uniquely identifies an Operation consumer. Below are the sample values.ACUI, MYTMO, QVXP, REBELLION, WALMARTRETAIL, SAMSCLUBRETAIL.",
            "required": false,
            "type": "string"
          },
          {
            "name": "sessionid",
            "in": "header",
            "description": "A value populated by the sender used to track the transactions that occur during a session, a long-lasting interaction that ismaintained by the sender.",
            "required": false,
            "type": "string"
          },
          {
            "name": "storeid",
            "in": "header",
            "description": "Unique identifier for the retail store location.",
            "required": false,
            "type": "string"
          },
          {
            "name": "terminalid",
            "in": "header",
            "description": "Unique identifier for the retail store terminal. Needed for FullService  (Tmobile RETAIL channel). Need to discuss with T-Mobile business liaison if there is a need for Partner to pass their retail Store Id).",
            "required": false,
            "type": "string"
          },
          {
            "name": "tillid",
            "in": "header",
            "description": "Unique identifier for the retail register. Needed for FullService  (Tmobile RETAIL channel). Need to discuss with T-Mobile business liaison if there is a need for Partner to pass their retail Store Id).",
            "required": false,
            "type": "string"
          },
          {
            "name": "timestamp",
            "in": "header",
            "description": "A timestamp provided by sender to track their workflow. If it is empty APIGEE can send current system timestamp.",
            "required": false,
            "type": "string",
            "format": "date-time"
          },
          {
            "name": "workflowid",
            "in": "header",
            "description": "Workflow name (NEW/ADD/EXCHANGE/RETURN)/OrderTypes _ ACTIVATION ADDALINE CHANGEDEVICE_JUMP CHANGEDEVICE",
            "required": false,
            "type": "string"
          },
          {
            "name": "summaryLevel",
            "in": "header",
            "description": "Indicates whether summary needs to be returned for each subscriber under the financial account",
            "required": false,
            "type": "string",
            "enum": [
              "BAN",
              "SUBSCRIBER"
            ]
          },
          {
            "$ref": "#/parameters/query-financial-account-id"
          },
          {
            "$ref": "#/parameters/query-msisdn"
          },
          {
            "$ref": "#/parameters/query-filter"
          },
          {
            "$ref": "#/parameters/query-page-number"
          },
          {
            "$ref": "#/parameters/query-page-size"
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/FinancialSummaryBufferedResponse"
            },
            "headers": {
              "Content-Length": {
                "description": "The Content-Length header specifies the actual length of the returned payload.",
                "type": "string"
              },
              "Content-Type": {
                "description": "The Content-Length header specifies the actual length of the returned payload.",
                "type": "string"
              },
              "Date": {
                "description": "The date and time that the message was originated (in HTTP-date format as defined by RFC 7231 Date/Time Formats).",
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "400": {
            "description": "The client made a bad request.",
            "schema": {
              "$ref": "#/definitions/errorCollection"
            }
          },
          "404": {
            "description": "Resource not found.",
            "schema": {
              "$ref": "#/definitions/errorCollection"
            }
          },
          "500": {
            "description": "The server encountered an unexpected condition which prevented it from fulfilling the request.",
            "schema": {
              "$ref": "#/definitions/errorCollection"
            }
          },
          "503": {
            "description": "The service is unavailable.",
            "schema": {
              "$ref": "#/definitions/errorCollection"
            }
          }
        }
      }
    },
    "/billing/v1/invoices/{billSequenceNumber}/summary": {
      "get": {
        "tags": [
          "financial-summary"
        ],
        "summary": "Get Charges, Collated deposits and Adjustments data for a specified financial account Id, subscriber and bill sequence number",
        "operationId": "retrieveFinancialSummaryByBillSequenceNumber",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "Authorization",
            "in": "header",
            "description": "OAuth 2.0 access token with the authentication type set as Bearer.",
            "required": true,
            "type": "string"
          },
          {
            "name": "activityid",
            "in": "header",
            "description": "Unique id alphanumeric value generated and sent by the partner to identify every the service request uniquely. Sample: XyzA:D1s5s1i-X6AFRvfiPBfs-1234  Pass as is to downstream calls.",
            "required": true,
            "type": "string"
          },
          {
            "name": "applicationid",
            "in": "header",
            "description": "Identifies the application, system, or tool that is being used to perform the transaction related to this request. Below are the sample values.ACUI, ESERVICE, REBELLION, WARP",
            "required": false,
            "type": "string"
          },
          {
            "name": "applicationuserid",
            "in": "header",
            "description": "NT id of the one who is managing the transaction from CARE/RETAIL channels.",
            "required": false,
            "type": "string"
          },
          {
            "name": "authcustomerid",
            "in": "header",
            "description": "Customer id _ of the person that is interacting with the system.",
            "required": false,
            "type": "string"
          },
          {
            "name": "authfinancialaccountid",
            "in": "header",
            "description": "Financial account that the customer intends to operate on, while interacting with the system.",
            "required": false,
            "type": "string"
          },
          {
            "name": "authlineofserviceid",
            "in": "header",
            "description": "Service Line that the customer intends to operate on, while interacting with the system.",
            "required": false,
            "type": "string"
          },
          {
            "name": "channelid",
            "in": "header",
            "description": "Identifies the business unit or sales channel for the Request.Below are the sample values:            Care            TeleSales            FullService            BusinessDirect            FinancePartners            NonFinancePartners            SelfServiceAuthenticated            SelfServiceAnonymous            InteractiveVoiceResponse",
            "required": false,
            "type": "string"
          },
          {
            "name": "dealercode",
            "in": "header",
            "description": "Unique code to identify the dealer/rep user.",
            "required": false,
            "type": "integer",
            "format": "int32"
          },
          {
            "name": "interactionid",
            "in": "header",
            "description": "Alphanumeric value represent a common transaction id across all calls made from UI, while completing all business activity needs of a particular customer.",
            "required": false,
            "type": "string"
          },
          {
            "name": "masterDealercode",
            "in": "header",
            "description": "Unique code to identify a group of dealers/reps.",
            "required": false,
            "type": "integer",
            "format": "int32"
          },
          {
            "name": "segmentationid",
            "in": "header",
            "description": "Identifier of customers primary data center.Sample: Polaris,Titan",
            "required": false,
            "type": "string"
          },
          {
            "name": "senderid",
            "in": "header",
            "description": "Uniquely identifies an Operation consumer. Below are the sample values.ACUI, MYTMO, QVXP, REBELLION, WALMARTRETAIL, SAMSCLUBRETAIL.",
            "required": false,
            "type": "string"
          },
          {
            "name": "sessionid",
            "in": "header",
            "description": "A value populated by the sender used to track the transactions that occur during a session, a long-lasting interaction that ismaintained by the sender.",
            "required": false,
            "type": "string"
          },
          {
            "name": "storeid",
            "in": "header",
            "description": "Unique identifier for the retail store location.",
            "required": false,
            "type": "string"
          },
          {
            "name": "terminalid",
            "in": "header",
            "description": "Unique identifier for the retail store terminal. Needed for FullService  (Tmobile RETAIL channel). Need to discuss with T-Mobile business liaison if there is a need for Partner to pass their retail Store Id).",
            "required": false,
            "type": "string"
          },
          {
            "name": "tillid",
            "in": "header",
            "description": "Unique identifier for the retail register. Needed for FullService  (Tmobile RETAIL channel). Need to discuss with T-Mobile business liaison if there is a need for Partner to pass their retail Store Id).",
            "required": false,
            "type": "string"
          },
          {
            "name": "timestamp",
            "in": "header",
            "description": "A timestamp provided by sender to track their workflow. If it is empty APIGEE can send current system timestamp.",
            "required": false,
            "type": "string",
            "format": "date-time"
          },
          {
            "name": "workflowid",
            "in": "header",
            "description": "Workflow name (NEW/ADD/EXCHANGE/RETURN)/OrderTypes _ ACTIVATION ADDALINE CHANGEDEVICE_JUMP CHANGEDEVICE",
            "required": false,
            "type": "string"
          },
          {
            "name": "summaryLevel",
            "in": "header",
            "description": "Indicates whether summary needs to be returned for each subscriber under the financial account",
            "required": false,
            "type": "string",
            "enum": [
              "BAN",
              "SUBSCRIBER"
            ]
          },
          {
            "$ref": "#/parameters/query-msisdn"
          },
          {
            "$ref": "#/parameters/path-bill-sequence-number"
          },
          {
            "$ref": "#/parameters/query-filter"
          },
          {
            "$ref": "#/parameters/query-financial-account-id"
          },
          {
            "$ref": "#/parameters/query-page-number"
          },
          {
            "$ref": "#/parameters/query-page-size"
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/FinancialSummaryBufferedResponse"
            },
            "headers": {
              "Content-Length": {
                "description": "The Content-Length header specifies the actual length of the returned payload.",
                "type": "string"
              },
              "Content-Type": {
                "description": "The Content-Length header specifies the actual length of the returned payload.",
                "type": "string"
              },
              "Date": {
                "description": "The date and time that the message was originated (in HTTP-date format as defined by RFC 7231 Date/Time Formats).",
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "400": {
            "description": "The client made a bad request.",
            "schema": {
              "$ref": "#/definitions/errorCollection"
            }
          },
          "404": {
            "description": "Resource not found.",
            "schema": {
              "$ref": "#/definitions/errorCollection"
            }
          },
          "500": {
            "description": "The server encountered an unexpected condition which prevented it from fulfilling the request.",
            "schema": {
              "$ref": "#/definitions/errorCollection"
            }
          },
          "503": {
            "description": "The service is unavailable.",
            "schema": {
              "$ref": "#/definitions/errorCollection"
            }
          }
        }
      }
    }
  },
  "parameters": {
    "path-financial-account-id": {
      "in": "path",
      "name": "financialAccountNumber",
      "description": "financial account id",
      "required": true,
      "type": "string"
    },
    "path-bill-sequence-number": {
      "in": "path",
      "name": "billSequenceNumber",
      "description": "bill sequence number",
      "required": true,
      "type": "integer",
      "format": "int32"
    },
    "path-msisdn": {
      "in": "path",
      "name": "mobileNumber",
      "description": "subscriber number",
      "required": true,
      "type": "string"
    },
    "query-msisdn": {
      "in": "query",
      "name": "mobileNumber",
      "description": "subscriber number",
      "required": false,
      "type": "string"
    },
    "query-financial-account-id": {
      "in": "query",
      "name": "financialAccountNumber",
      "description": "financial account id",
      "required": false,
      "type": "string"
    },
    "query-bill-sequence-number": {
      "in": "query",
      "name": "billSequenceNumber",
      "description": "bill sequence number",
      "required": false,
      "type": "integer",
      "format": "int32"
    },
    "query-filter": {
      "in": "query",
      "name": "filter",
      "description": "Value will be passes separated by \\\",\\\" forming the combination of below ENUM in swagger",
      "required": false,
      "type": "string",
      "enum": [
        "DEPOSITS",
        "CREDITS_AND_ADJUSTMENTS",
        "DISCOUNTS",
        "ONE_TIME_CHARGES",
        "OVERAGE_AND_ROAMING",
        "MONTHLY_RECURRING_CHARGE",
        "ALL_CHARGE_CATEGORIES"
      ]
    },
    "query-page-number": {
      "in": "query",
      "name": "pagenumber",
      "description": "page number for which data needs to be fetched",
      "required": false,
      "type": "string"
    },
    "query-page-size": {
      "in": "query",
      "name": "pagesize",
      "description": "page size of data that will be returned on that page",
      "required": false,
      "type": "string"
    }
  },
  "definitions": {
    "IdRef": {
      "type": "object",
      "properties": {
        "rel": {
          "type": "string",
          "enum": [
            "self",
            "parent",
            "child",
            "next",
            "prev",
            "first",
            "last",
            "item",
            "collection",
            "organization",
            "subscriber",
            "financial-account",
            "next-organization",
            "usage",
            "bill-charges",
            "service-agreement",
            "bill-adjustments"
          ]
        },
        "id": {
          "type": "string"
        },
        "href": {
          "type": "string"
        }
      }
    },
    "AdjustmentRef": {
      "type": "object",
      "properties": {
        "amount": {
          "type": "number",
          "format": "double"
        },
        "taxAmount": {
          "type": "number",
          "format": "double"
        }
      }
    },
    "DiscountsRef": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "effectiveDate": {
          "type": "string"
        },
        "expirationDate": {
          "type": "string"
        }
      }
    },
    "ChargesRef": {
      "type": "object",
      "properties": {
        "amount": {
          "type": "number",
          "format": "double"
        },
        "taxAmount": {
          "type": "number",
          "format": "double"
        },
        "chargeCategory": {
          "type": "string"
        }
      }
    },
    "ServicesRef": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string"
        },
        "type": {
          "type": "string"
        },
        "socLevelCode": {
          "type": "string"
        },
        "socDescription": {
          "type": "string"
        },
        "socEffectiveDate": {
          "type": "string"
        },
        "socExpirationDate": {
          "type": "string"
        },
        "rate": {
          "type": "number",
          "format": "double"
        }
      }
    },
    "DiscountsBilledDetailsRef": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "netAmount": {
          "type": "number",
          "format": "double"
        },
        "taxAmount": {
          "type": "number",
          "format": "double"
        }
      }
    },
    "DiscountsBilledTotalRef": {
      "type": "object",
      "properties": {
        "totalNetAmount": {
          "type": "number",
          "format": "double"
        },
        "totalTaxAmount": {
          "type": "number",
          "format": "double"
        }
      }
    },
    "MrcDetailsRef": {
      "type": "object",
      "properties": {
        "socCode": {
          "type": "string"
        },
        "socType": {
          "type": "string"
        },
        "socDescription": {
          "type": "string"
        },
        "netAmount": {
          "type": "number",
          "format": "double"
        },
        "taxAmount": {
          "type": "number",
          "format": "double"
        }
      }
    },
    "MrcTotalRef": {
      "type": "object",
      "properties": {
        "totalNetAmount": {
          "type": "number",
          "format": "double"
        },
        "totalTaxAmount": {
          "type": "number",
          "format": "double"
        }
      }
    },
    "FinancialSummaryResponse": {
      "type": "object",
      "properties": {
        "depositAmount": {
          "type": "number",
          "format": "double"
        },
        "adjustment": {
          "$ref": "#/definitions/AdjustmentRef"
        },
        "discounts": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/DiscountsRef"
          }
        },
        "charges": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ChargesRef"
          }
        },
        "services": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ServicesRef"
          }
        },
        "usages": {
          "$ref": "#/definitions/UsagesChargesRef"
        },
        "discountsBilledDetails": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/DiscountsBilledDetailsRef"
          }
        },
        "discountsBilledTotal": {
          "$ref": "#/definitions/DiscountsBilledTotalRef"
        },
        "mrcDetails": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/MrcDetailsRef"
          }
        },
        "mrcTotal": {
          "$ref": "#/definitions/MrcTotalRef"
        },
        "productType": {
          "type": "string"
        },
        "lineType": {
          "type": "string"
        },
        "_links": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/IdRef"
          }
        }
      }
    },
    "FinancialSummaryBufferedResponse": {
      "type": "object",
      "properties": {
        "summaryLevel": {
          "type": "string"
        },
        "moreRowsIndicator": {
          "type": "boolean"
        },
		"billCycleCloseDate": {
			"type": "string"
		},
		"confirmedBillIndicator": {
			"type": "string"
		},
        "summary": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/FinancialSummaryDataResponse"
          }
        }
      }
    },
    "TotalUsages": {
      "type": "object",
      "properties": {
        "voice": {
          "type": "number",
          "format": "double"
        },
        "sms": {
          "type": "number",
          "format": "double"
        },
        "data": {
          "type": "number",
          "format": "double"
        },
        "thirdPartyPurchases": {
          "type": "number",
          "format": "double"
        },
        "tmobilePurchases": {
          "type": "number",
          "format": "double"
        },
		"overages": {
			"type": "number",
			"format": "double"
		}
      }
    },
    "FinancialSummaryDataResponse": {
      "type": "object",
      "properties": {
        "mobileNumber": {
          "type": "string"
        },
        "financialSummary": {
          "$ref": "#/definitions/FinancialSummaryResponse"
        }
      }
    },
    "error": {
      "description": "Error object.",
      "type": "object",
      "properties": {
        "code": {
          "description": "A specific T-Mobile error code.",
          "type": "string"
        },
        "userMessage": {
          "description": "A human-readable message describing the error.",
          "type": "string"
        },
        "systemMessage": {
          "description": "Backend system error message.",
          "type": "string"
        }
      }
    },
    "errorCollection": {
      "description": "The array of error objects. As defined in https://tmobileusa.sharepoint.com/teams/EnterpriseServices/ESG%20Standards%20Documents/APIs/T-Mobile%20API%20Design%20Guidelines%20v1.0.docx",
      "type": "object",
      "properties": {
        "errors": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/error"
          }
        }
      }
    },
    "UsageCharges": {
      "type": "object",
      "description": "Usage for a customer.",
      "properties": {
        "international": {
          "type": "number",
          "format": "double"
        },
        "domestic": {
          "type": "number",
          "format": "double"
        },
        "others": {
          "type": "number",
          "format": "double"
        },
        "longDistance": {
          "type": "number",
          "format": "double"
        },
        "usageType": {
          "description": "Identifies the type of usage that the customer is having",
          "type": "string",
          "enum": [
            "SMS",
            "VOICE",
            "DATA"
          ]
        }
      }
    },
    "PremiumCharges": {
      "type": "object",
      "description": "Premium Charges for customer.",
      "properties": {
        "premiumRecurring": {
          "type": "number",
          "format": "double"
        },
        "premiumOneTime": {
          "type": "number",
          "format": "double"
        },
        "premiumType": {
          "description": "Identifies the type of premiums that the customer is having",
          "type": "string",
          "enum": [
            "TMOBILE",
            "THIRDPARTY"
          ]
        }
      }
    },
    "UsagesChargesRef": {
      "type": "object",
      "description": "Usage data for customer including breakdown.",
      "properties": {
        "usageCharges": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/UsageCharges"
          }
        },
        "purchases": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/PremiumCharges"
          }
        },
        "total": {
          "$ref": "#/definitions/TotalUsages"
        }
      }
    }
  }
}
