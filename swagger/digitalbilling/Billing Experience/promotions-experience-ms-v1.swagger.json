{
  "swagger": "2.0",
  "info": {
    "description": "Promotions Experience APIs",
    "version": "1.0",
    "title": "Promotions Experience v1"
  },
  "basePath": "/promotions-experience/v1",
  "tags": [
    {
      "name": "promotions-experience-rest"
    }
  ],
  "paths": {
    "/promotions": {
      "post": {
        "tags": [
          "promotions-experience-rest"
        ],
        "summary": "Create a new request for a promotion addition/removal",
        "operationId": "postPromotionRecord",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "promotion",
            "description": "promotion object that needs to be added to the customer",
            "required": true,
            "schema": {
              "$ref": "#/definitions/PromotionEnrollmentRequest"
            }
          },
          {
            "$ref": "#/parameters/header-authorization"
          },
          {
            "$ref": "#/parameters/header-activityId"
          },
          {
            "$ref": "#/parameters/header-dealerCode"
          },
          {
            "$ref": "#/parameters/header-applicationUserId"
          },
          {
            "$ref": "#/parameters/header-userId"
          },
          {
            "$ref": "#/parameters/header-applicationId"
          },
          {
            "$ref": "#/parameters/header-authCustomerId"
          },
          {
            "$ref": "#/parameters/header-authFinancialAccountId"
          },
          {
            "$ref": "#/parameters/header-authLineOfServiceId"
          },
          {
            "$ref": "#/parameters/header-channelId"
          },
          {
            "$ref": "#/parameters/header-interactionId"
          },
          {
            "$ref": "#/parameters/header-masterDealerCode"
          },
          {
            "$ref": "#/parameters/header-segmentationId"
          },
          {
            "$ref": "#/parameters/header-senderId"
          },
          {
            "$ref": "#/parameters/header-sessionId"
          },
          {
            "$ref": "#/parameters/header-storeId"
          },
          {
            "$ref": "#/parameters/header-terminalId"
          },
          {
            "$ref": "#/parameters/header-tillId"
          },
          {
            "$ref": "#/parameters/header-workflowId"
          },
          {
            "$ref": "#/parameters/header-timestamp"
          }
        ],
        "responses": {
          "201": {
            "description": "successful created"
          },
          "400": {
            "description": "Bad request error",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "409": {
            "description": "The request could not be completed due to a conflict with the current state of the target resource",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "500": {
            "description": "The server encountered an unexpected condition which prevented it from fulfilling the request",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          }
        }
      }
    },
    "/promotions/promotion-list": {
      "get": {
        "tags": [
          "promotions-experience-rest"
        ],
        "summary": "Return a list of available promotions for a given SKU",
        "operationId": "getAvailablePromotionsBySku",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "query",
            "name": "sku",
            "description": "Device SKU",
            "required": true,
            "type": "string"
          },
          {
            "in": "query",
            "name": "startDate",
            "description": "Promotions start date in yyyymmdd format",
            "required": false,
            "type": "string",
            "pattern": "^\\d{8}$"
          },
          {
            "$ref": "#/parameters/header-authorization"
          },
          {
            "$ref": "#/parameters/header-activityId"
          },
          {
            "$ref": "#/parameters/header-dealerCode"
          },
          {
            "$ref": "#/parameters/header-applicationUserId"
          },
          {
            "$ref": "#/parameters/header-userId"
          },
          {
            "$ref": "#/parameters/header-applicationId"
          },
          {
            "$ref": "#/parameters/header-authCustomerId"
          },
          {
            "$ref": "#/parameters/header-authFinancialAccountId"
          },
          {
            "$ref": "#/parameters/header-authLineOfServiceId"
          },
          {
            "$ref": "#/parameters/header-channelId"
          },
          {
            "$ref": "#/parameters/header-interactionId"
          },
          {
            "$ref": "#/parameters/header-masterDealerCode"
          },
          {
            "$ref": "#/parameters/header-segmentationId"
          },
          {
            "$ref": "#/parameters/header-senderId"
          },
          {
            "$ref": "#/parameters/header-sessionId"
          },
          {
            "$ref": "#/parameters/header-storeId"
          },
          {
            "$ref": "#/parameters/header-terminalId"
          },
          {
            "$ref": "#/parameters/header-tillId"
          },
          {
            "$ref": "#/parameters/header-workflowId"
          },
          {
            "$ref": "#/parameters/header-timestamp"
          }
        ],
        "responses": {
          "200": {
            "description": "The request was successful",
            "schema": {
              "$ref": "#/definitions/PromotionListResponse"
            }
          },
          "400": {
            "description": "Bad request error",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "500": {
            "description": "The server encountered an unexpected condition which prevented it from fulfilling the request",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          }
        }
      }
    },
    "/promotions/{fanID}/promotions-summary": {
      "get": {
        "tags": [
          "promotions-experience-rest"
        ],
        "summary": "Return all the equipment, plans and promotions for a given account",
        "operationId": "equipment-summary-by-fan-id",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "fanID",
            "in": "path",
            "description": "customer id",
            "required": true,
            "type": "string"
          },
          {
            "name": "includeAlerts",
            "in": "query",
            "description": "Flag that marks whether the response includes alerts or not",
            "type": "boolean",
            "default": false,
            "required": false
          },
          {
            "name": "msisdn",
            "in": "query",
            "description": "Pass mobile phone number(s) to return promotion enrollments for the mobile phone number(s). API will accept at the most 10 instances of this parameter in query URL",
            "required": false,
            "type": "array",
            "minItems": 0,
            "maxItems": 10,
            "collectionFormat": "csv",
            "items": {
              "type": "string"
            }
          },
          {
            "name": "status",
            "in": "query",
            "description": "Pass promotion enrollment status to return promotion enrollment with the status value passed. Consumer application pass one or more instances of status query parameter to fetch promotions in the status values passed. If no status, parameter passed, API will return all promotion enrollments irrespective of status.  Valid values - PENDING,PAST,ACTIVE",
            "required": false,
            "type": "array",
            "collectionFormat": "csv",
            "items": {
              "type": "string"
            }
          },
          {
            "name": "view",
            "in": "query",
            "description": "Valid values are CUSTOMER/REP",
            "required": false,
            "type": "string"
          },
          {
            "$ref": "#/parameters/header-authorization"
          },
          {
            "$ref": "#/parameters/header-activityId"
          },
          {
            "$ref": "#/parameters/header-dealerCode"
          },
          {
            "$ref": "#/parameters/header-applicationUserId"
          },
          {
            "$ref": "#/parameters/header-userId"
          },
          {
            "$ref": "#/parameters/header-applicationId"
          },
          {
            "$ref": "#/parameters/header-authCustomerId"
          },
          {
            "$ref": "#/parameters/header-authFinancialAccountId"
          },
          {
            "$ref": "#/parameters/header-authLineOfServiceId"
          },
          {
            "$ref": "#/parameters/header-channelId"
          },
          {
            "$ref": "#/parameters/header-interactionId"
          },
          {
            "$ref": "#/parameters/header-masterDealerCode"
          },
          {
            "$ref": "#/parameters/header-segmentationId"
          },
          {
            "$ref": "#/parameters/header-senderId"
          },
          {
            "$ref": "#/parameters/header-sessionId"
          },
          {
            "$ref": "#/parameters/header-storeId"
          },
          {
            "$ref": "#/parameters/header-terminalId"
          },
          {
            "$ref": "#/parameters/header-tillId"
          },
          {
            "$ref": "#/parameters/header-workflowId"
          },
          {
            "$ref": "#/parameters/header-timestamp"
          }
        ],
        "responses": {
          "200": {
            "description": "The request was successful",
            "schema": {
              "$ref": "#/definitions/PromotionsSummaryResponse"
            }
          },
          "400": {
            "description": "Bad request error",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "404": {
            "description": "Record not found",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "500": {
            "description": "The server encountered an unexpected condition which prevented it from fulfilling the request",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          }
        }
      }
    },
    "/promotions/promotion-alerts": {
      "get": {
        "tags": [
          "promotions-experience-rest"
        ],
        "summary": "Returns alerts related to promotions for a given account",
        "operationId": "promotions-alerts-by-fan-id",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "fanID",
            "in": "query",
            "description": "customer id",
            "required": true,
            "type": "string"
          },
          {
            "$ref": "#/parameters/header-authorization"
          },
          {
            "$ref": "#/parameters/header-activityId"
          },
          {
            "$ref": "#/parameters/header-dealerCode"
          },
          {
            "$ref": "#/parameters/header-applicationUserId"
          },
          {
            "$ref": "#/parameters/header-userId"
          },
          {
            "$ref": "#/parameters/header-applicationId"
          },
          {
            "$ref": "#/parameters/header-authCustomerId"
          },
          {
            "$ref": "#/parameters/header-authFinancialAccountId"
          },
          {
            "$ref": "#/parameters/header-authLineOfServiceId"
          },
          {
            "$ref": "#/parameters/header-channelId"
          },
          {
            "$ref": "#/parameters/header-interactionId"
          },
          {
            "$ref": "#/parameters/header-masterDealerCode"
          },
          {
            "$ref": "#/parameters/header-segmentationId"
          },
          {
            "$ref": "#/parameters/header-senderId"
          },
          {
            "$ref": "#/parameters/header-sessionId"
          },
          {
            "$ref": "#/parameters/header-storeId"
          },
          {
            "$ref": "#/parameters/header-terminalId"
          },
          {
            "$ref": "#/parameters/header-tillId"
          },
          {
            "$ref": "#/parameters/header-workflowId"
          },
          {
            "$ref": "#/parameters/header-timestamp"
          }
        ],
        "responses": {
          "200": {
            "description": "The request was successful",
            "schema": {
              "$ref": "#/definitions/PromotionsAlertsResponse"
            }
          },
          "400": {
            "description": "Bad request error",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "404": {
            "description": "Record not found",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "500": {
            "description": "The server encountered an unexpected condition which prevented it from fulfilling the request",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          }
        }
      }
    }
  },
  "parameters": {
    "header-timestamp": {
      "in": "header",
      "name": "timestamp",
      "required": false,
      "type": "string"
    },
    "header-workflowId": {
      "in": "header",
      "name": "workflowId",
      "required": false,
      "type": "string"
    },
    "header-tillId": {
      "in": "header",
      "name": "tillId",
      "required": false,
      "type": "string"
    },
    "header-terminalId": {
      "in": "header",
      "name": "terminalId",
      "required": false,
      "type": "string"
    },
    "header-storeId": {
      "in": "header",
      "name": "storeId",
      "required": false,
      "type": "string"
    },
    "header-sessionId": {
      "in": "header",
      "name": "sessionId",
      "required": false,
      "type": "string"
    },
    "header-senderId": {
      "in": "header",
      "name": "senderId",
      "required": false,
      "type": "string"
    },
    "header-segmentationId": {
      "in": "header",
      "name": "segmentationId",
      "required": false,
      "type": "string"
    },
    "header-masterDealerCode": {
      "in": "header",
      "name": "masterDealerCode",
      "required": false,
      "type": "string"
    },
    "header-interactionId": {
      "in": "header",
      "name": "interactionId",
      "required": false,
      "type": "string"
    },
    "header-channelId": {
      "in": "header",
      "name": "channelId",
      "required": false,
      "type": "string"
    },
    "header-authLineOfServiceId": {
      "in": "header",
      "name": "authLineOfServiceId",
      "required": false,
      "type": "string"
    },
    "header-authFinancialAccountId": {
      "in": "header",
      "name": "authFinancialAccountId",
      "required": false,
      "type": "string"
    },
    "header-authorization": {
      "in": "header",
      "name": "authorization",
      "required": false,
      "type": "string"
    },
    "header-activityId": {
      "in": "header",
      "name": "activityId",
      "required": false,
      "type": "string"
    },
    "header-dealerCode": {
      "in": "header",
      "name": "dealerCode",
      "required": false,
      "type": "string"
    },
    "header-applicationUserId": {
      "in": "header",
      "name": "applicationUserId",
      "required": false,
      "type": "string"
    },
    "header-userId": {
      "in": "header",
      "name": "userId",
      "required": false,
      "type": "string"
    },
    "header-applicationId": {
      "in": "header",
      "name": "applicationId",
      "required": false,
      "type": "string"
    },
    "header-authCustomerId": {
      "in": "header",
      "name": "authCustomerId",
      "required": false,
      "type": "string"
    }
  },
  "definitions": {
    "PromotionEnrollmentRequest": {
      "type": "object",
      "required": [
        "action",
        "eipId",
        "fanId",
        "promoCode",
        "subscriberNo",
        "promotionSystemId"
      ],
      "properties": {
        "action": {
          "type": "string",
          "enum": [
            "E",
            "D"
          ],
          "description": "E - enrollment, D - un-enrollment"
        },
        "eipId": {
          "type": "string"
        },
        "fanId": {
          "type": "string",
          "description": "Account number - BAN number."
        },
        "memoUserText": {
          "type": "string",
          "maxLength": 2000
        },
        "deviceSku": {
          "type": "string",
          "description": "Required for enrollment"
        },
        "deviceId": {
          "type": "string",
          "description": "Required for enrollment"
        },
        "promoCode": {
          "type": "string"
        },
        "subscriberNo": {
          "type": "string"
        },
        "promotionSystemId": {
          "$ref": "#/definitions/promotionSystemId"
        }
      }
    },
    "PromotionListResponse": {
      "required": [
        "promotions"
      ],
      "properties": {
        "promotions": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Promotion"
          }
        }
      }
    },
    "PromotionsSummaryResponse": {
      "required": [
        "subscribersDeviceDetailsList",
        "promotionsAlertsList"
      ],
      "properties": {
        "subscribersDeviceDetailsList": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/SubscriberDeviceDetails"
          }
        },
        "promotionsAlertsList": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/PromotionAlert"
          }
        }
      }
    },
    "PromotionsAlertsResponse": {
      "required": [
        "promotionsAlertsList"
      ],
      "properties": {
        "promotionsAlertsList": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/PromotionAlert"
          }
        }
      }
    },
    "SubscriberDevicePromotion": {
      "properties": {
        "promotionCode": {
          "type": "string"
        },
        "promotionId": {
          "type": "string"
        },
        "promotionName": {
          "type": "string"
        },
        "promotionDescription": {
          "type": "string"
        },
        "promotionShortDescription": {
          "type": "string"
        },
        "promotionType": {
          "type": "string"
        },
        "promotionShortName": {
          "type": "string"
        },
        "promotionLongDescription": {
          "type": "string"
        },
        "customerFacingCodeType": {
          "type": "string"
        },
        "termsAndConditions": {
          "type": "string"
        },
        "legalCloseProximityText": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "status": {
          "type": "string"
        },
        "webStatus": {
          "type": "string"
        },
        "statusDisplayText": {
          "type": "string"
        },
        "statusDisplayDescription": {
          "type": "string"
        },
        "statusReason": {
          "type": "string"
        },
        "effectiveDate": {
          "type": "string"
        },
        "amount": {
          "type": "number"
        },
        "durationInMonths": {
          "type": "integer"
        },
        "remainingAmount": {
          "type": "number"
        },
        "remainingDurationInMonths": {
          "type": "integer"
        },
        "appliedAmount": {
          "type": "number"
        },
        "completedDurationInMonths": {
          "type": "integer"
        },
        "monthlyCredit": {
          "type": "number"
        },
        "promotionEndDate": {
          "type": "string"
        },
        "promotionStartDate": {
          "type": "string"
        },
        "endDateDescription": {
          "type": "string"
        },
        "enrollmentDeadlineDateTime": {
          "description": "Promotion enrollment deadline date",
          "type": "string" 
        },
        "statusLastUpdatedAt": {
          "description": "Promotion Enrollment Status Date and Time",
          "type": "string"
        },
        "promotionEvaluationDate": {
          "type": "string"
        },
        "promotionSystemId": {
          "$ref": "#/definitions/promotionSystemId"
        },
        "benefits": {
          "description": "promotion benefits details",
          "type": "array",
          "items": {
            "$ref": "#/definitions/Benefits"
          }
        },
        "eligibilityCriteria": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/EligibilityCriteria"
          }
        }
      }
    },
    "SubscriberDeviceEquipment": {
      "properties": {
        "sku": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "imei": {
          "type": "string"
        },
        "price": {
          "type": "number"
        },
        "msisdn": {
          "type": "string"
        },
        "transactionType": {
          "type": "string"
        },
        "actualInstallmentPaid": {
          "type": "number"
        },
        "percentTotal": {
          "type": "number"
        },
        "paymentTypeEligibilty": {
          "type": "string"
        },
        "eclmp": {
          "type": "number"
        },
        "neverReceived": {
          "type": "boolean"
        },
        "returned": {
          "type": "boolean"
        },
        "installmentEquipmentId": {
          "type": "string"
        },
        "eclb": {
          "type": "number"
        },
        "remainingUnbilledInstallments": {
          "type": "number"
        },
        "lastInstallmentAmount": {
          "type": "number"
        },
        "status": {
          "type": "string"
        },
        "deviceReturnPending": {
          "type": "boolean"
        },
        "promotionsDetails": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/SubscriberDevicePromotion"
          }
        }
      }
    },
    "SubscriberDevicePlan": {
      "properties": {
        "applicationId": {
          "type": "string"
        },
        "transactionId": {
          "type": "string"
        },
        "installmentPlanId": {
          "type": "string"
        },
        "planStartDate": {
          "type": "string"
        },
        "planEndDate": {
          "type": "string"
        },
        "totalInstallmentPeriods": {
          "type": "number"
        },
        "balanceOwed": {
          "type": "number"
        },
        "eclmp": {
          "type": "number"
        },
        "firstInstallment": {
          "type": "number"
        },
        "planStatus": {
          "type": "string"
        },
        "planType": {
          "type": "string"
        },
        "remainingUnbilledInstallments": {
          "type": "number"
        },
        "state": {
          "type": "string"
        },
        "totalFinancedAmount": {
          "type": "number"
        },
        "lastInstallment": {
          "type": "number"
        },
        "deviceLevel": {
          "type": "boolean"
        },
        "financeType": {
          "type": "string"
        },
        "equipments": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/SubscriberDeviceEquipment"
          }
        }
      }
    },
    "SubscriberDeviceDetails": {
      "required": [
        "subscriberNumber",
        "totalPendingCustomerActionPromotions",
        "totalRemovedPromotions",
        "totalInProcsessPromotions",
        "totalActivePromotions",
        "totalExpiredPromotions",
        "totalCompletedPromotions",
        "totalAvailablePromotions",
        "plans"
      ],
      "properties": {
        "subscriberNumber": {
          "type": "string"
        },
        "totalPendingCustomerActionPromotions": {
          "type": "integer",
          "default": 0
        },
        "totalRemovedPromotions": {
          "type": "integer",
          "default": 0
        },
        "totalInProcsessPromotions": {
          "type": "integer",
          "default": 0
        },
        "totalActivePromotions": {
          "type": "integer",
          "default": 0
        },
        "totalCompletedPromotions": {
          "type": "integer",
          "default": 0
        },
        "totalAvailablePromotions": {
          "type": "integer",
          "default": 0
        },
        "totalExpiredPromotions": {
          "type": "integer",
          "default": 0
        },
        "plans": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/SubscriberDevicePlan"
          }
        }
      },
      "type": "object"
    },
    "EligibilityCriteria": {
      "type": "object",
      "required": [
        "code",
        "displayText"
      ],
      "properties": {
        "code": {
          "description": "eligibility attribute code",
          "type": "string"
        },
        "displayText": {
          "description": "eligibility attribute description",
          "type": "string"
        },
        "criteriaMet": {
          "description": "flag to indicate whether customer meets promotion enrollment eligibility criteria or not. Values true/false.",
          "type": "boolean"
        },
        "details": {
          "description": "Rep facing. Expanded Promotion Eligibility Criteria Attribute Detail",
          "type": "array",
          "minItems": 1,
          "items": {
            "type": "string"
          }
        }
      }
    },
    "Benefits": {
      "type": "object",
      "description": "promotion benefits details",
      "required": [
        "description"
      ],
      "properties": {
        "type": {
          "description": "Name of the deduction",
          "type": "string"
        },
        "description": {
          "description": "descriptions is a predefined object from an igniteXml model - cabinet 1030178313 build 1036830641 key 1030671589",
          "type": "string"
        },
        "amount": {
          "description": "Deduction Amount in the transactional currency",
          "type": "number",
          "format": "double"
        },
        "duration": {
          "description": "effectivePeriod is a predefined object from an igniteXml model - cabinet 1030178313 build 1036830641 key 1030671566",
          "type": "integer",
          "format": "int32"
        },
        "providerSystem": {
          "description": "Promotion Benefit provider system",
          "type": "string"
        },
        "units": {
          "type": "string"
        }
      }
    },
    "PromotionAlert": {
      "type": "object",
      "properties": {
        "alertType": {
          "$ref": "#/definitions/promotionAlertType"
        },
        "alertDescription": {
          "type": "string"
        },
        "subscriberNumber": {
          "type": "string"
        },
        "deviceName": {
          "type": "string"
        },
        "devicePlanId": {
          "type": "string"
        },
        "promotionCode": {
          "type": "string"
        },
        "promotionDescription": {
          "type": "string"
        },
        "promotionStatus": {
          "type": "string"
        },
        "promotionEndDate": {
          "type": "string"
        },
        "promotionManualRequestDate": {
          "type": "string"
        },
        "ineligibilityReason": {
          "type": "string"
        },
        "promotionRemainingInstallments": {
          "type": "integer",
          "format": "int32"
        }
      }
    },
    "Promotion": {
      "type": "object",
      "properties": {
        "effectiveDate": {
          "type": "string"
        },
        "expirationDate": {
          "type": "string"
        },
        "financeType": {
          "type": "string"
        },
        "promoAmount": {
          "type": "number"
        },
        "promoDiscountPercentage": {
          "type": "number"
        },
        "promoCode": {
          "type": "string"
        },
        "promotionName": {
          "type": "string"
        },
        "promotionShortDescription": {
          "type": "string"
        },
        "promoDuration": {
          "type": "number",
          "format": "int32"
        },
        "promoEndDate": {
          "type": "string"
        },
        "promoStartDate": {
          "type": "string"
        },
        "tradeInGracePeriod": {
          "type": "number",
          "format": "int32"
        },
        "tradeInGroupId": {
          "type": "string"
        },
        "promotionSystemId": {
          "$ref": "#/definitions/promotionSystemId"
        }
      }
    },
    "promotionSystemId": {
      "type": "string",
      "enum": [
        "PDE",
        "EFPE",
        "EIP"
      ],
      "description": "Identifier of downstream promotion system"
    }, 
    "promotionAlertType": {
      "type": "string",
      "enum": [
        "PROMO_INSTALLMENTS_LEFT",
        "PROMO_ENDED",
        "PROMO_NOT_ELIGIBLE",
        "PROMO_REMOVED",
        "PROMO_PENDING"
      ]
    },
    "error": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string"
        },
        "userMessage": {
          "type": "string"
        },
        "systemMessage": {
          "type": "string"
        }
      }
    },
    "errors": {
      "type": "object",
      "properties": {
        "errors": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/error"
          }
        }
      }
    }
  }
}