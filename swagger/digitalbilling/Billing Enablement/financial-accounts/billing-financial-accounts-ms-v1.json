{
  "swagger": "2.0",
  "info": {
    "description": "Provides lookup APIs for postpaid Financial account and manages direct payments and subscriber lookups for a prepaid subscriber",
    "version": "1.0",
    "title": "Financial Account APIs for all channels"
  },
  "basePath": "/billing/v1/financial-accounts",
  "tags": [
    {
      "name": "subscriber",
      "description": "A party that subscribes for a product and uses them in an ongoing way"
    }
  ],
  "paths": {
    "/find-accounts": {
      "post": {
        "tags": [
          "subscriber"
        ],
        "description": "Retrieves Prepaid Subscriber Information by msisdn",
        "operationId": "prepaidSubscriberLookup",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/hal+json"
        ],
        "parameters": [
          {
            "name": "channel-id",
            "in": "header",
            "description": "channel the request originated from",
            "required": true,
            "type": "string"
          },
          {
            "name": "application-id",
            "in": "header",
            "description": "application the request originated from",
            "required": true,
            "type": "string"
          },
          {
            "name": "store-id",
            "in": "header",
            "description": "Unique identifier for the retail store location.",
            "required": false,
            "type": "string"
          },
          {
            "name": "body",
            "in": "body",
            "description": "Find subscriber by msisdn.",
            "schema": {
              "type": "object",
              "required": [
                "mobileNumber"
              ],
              "properties": {
                "mobileNumber": {
                  "type": "string",
                  "description": "10 digit mobile number of a subscriber."
                },
                "subscriberPin": {
                  "type": "string",
                  "description": "4 digit pin of a subscriber."
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success response",
            "schema": {
              "type": "object",
              "required": [
                "_embedded"
              ],
              "properties": {
                "_embedded": {
                  "type": "object",
                  "required": [
                    "accounts"
                  ],
                  "properties": {
                    "accounts": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "required": [
                          "availableEpinSkus",
                          "refillAllowedInCents"
                        ],
                        "properties": {
                          "systemInRecoveryMode": {
                            "type": "boolean",
                            "description": "Set to true if one or more dependent services are unavailable. This might result in response missing some elements."
                          },
                          "firstName": {
                            "type": "string",
                            "description": "first name of the subscriber."
                          },
                          "lastName": {
                            "type": "string",
                            "description": "last name of the subscriber."
                          },
                          "ratePlan": {
                            "type": "object",
                            "required": [
                              "ratePlanDescription",
                              "ratePlanSupportsOnlyData"
                            ],
                            "properties": {
                              "ratePlanDescription": {
                                "type": "string",
                                "description": "short description of the rate plan from product catalog."
                              },
                              "ratePlanSupportsOnlyData": {
                                "type": "boolean",
                                "description": "Indicates if rate plan supports only data"
                              }
                            }
                          },
                          "accountStatus": {
                            "type": "string",
                            "description": "return 'active' or 'suspended' based on subscriber status"
                          },
                          "moneyBalance": {
                            "type": "object",
                            "description": "real time account balance of a subscriber.",
                            "required": [
                              "balanceInCents",
                              "expirationDate"
                            ],
                            "properties": {
                              "balanceInCents": {
                                "type": "integer",
                                "format": "int32",
                                "description": "real time account balance of a subscriber in cents."
                              },
                              "expirationDate": {
                                "type": "string",
                                "format": "date",
                                "description": "expiration date of the real time account balance of a subscriber."
                              }
                            }
                          },
                          "availableEpinSkus": {
                            "type": "array",
                            "items": {
                              "type": "object",
                              "required": [
                                "skuIdentifier",
                                "skuDescription",
                                "faceValueInCents"
                              ],
                              "properties": {
                                "skuIdentifier": {
                                  "type": "string"
                                },
                                "skuDescription": {
                                  "type": "string"
                                },
                                "faceValueInCents": {
                                  "type": "integer",
                                  "format": "int32"
                                }
                              }
                            }
                          },
                          "ratePlanSupportsServiceCycle": {
                            "type": "boolean",
                            "description": "Set to true if the subscriber is on a rate plan which renews every period."
                          },
                          "isServiceCyclePaid": {
                            "type": "boolean",
                            "description": "Set to true if the subscriber is on a rate plan which has a service cycle and is paid."
                          },
                          "currentServiceCycle": {
                            "type": "object",
                            "description": "This element would be returned if the subscriber is paid on a rate plan which has a service cycle",
                            "required": [
                              "currentServicePeriodExpirationDate",
                              "nextServicePeriodStartDate",
                              "estimatedRenewalChargeInCents",
                              "estimatedRefillAmountNeededInCents"
                            ],
                            "properties": {
                              "currentServicePeriodExpirationDate": {
                                "type": "string",
                                "format": "date"
                              },
                              "nextServicePeriodStartDate": {
                                "type": "string",
                                "description": "Start date of next service period",
                                "format": "date"
                              },
                              "estimatedRenewalChargeInCents": {
                                "type": "integer",
                                "description": "estimated charge in cents for the subscriber to be paid after the scheduled renewal.",
                                "format": "int32"
                              },
                              "estimatedRefillAmountNeededInCents": {
                                "type": "integer",
                                "description": "estimated refill in cents for the subscriber to be paid after the scheduled renewal.",
                                "format": "int32"
                              }
                            }
                          },
                          "immediateRenewalInfo": {
                            "type": "object",
                            "description": "This element would be returned if the subscriber is unpaid on a rate plan which has a service cycle.",
                            "required": [
                              "totalCostInCents",
                              "refillAmountNeededInCents"
                            ],
                            "properties": {
                              "totalCostInCents": {
                                "type": "integer",
                                "description": "total charge in cents needed by the subscriber for immediate renewal.",
                                "format": "int32"
                              },
                              "refillAmountNeededInCents": {
                                "type": "integer",
                                "description": "minimum refill in cents needed by the subscriber for immediate renewal.",
                                "format": "int32"
                              }
                            }
                          },
                          "canApplyFunds": {
                            "type": "boolean",
                            "description": "Set to true if the subscriber can accept payments or voucher redemptions"
                          },
                          "refillAllowedInCents": {
                            "type": "object",
                            "required": [
                              "minimumAmount",
                              "maximumAmount"
                            ],
                            "properties": {
                              "minimumAmount": {
                                "type": "integer",
                                "description": "minimum amount of refill allowed (in cents).",
                                "format": "int32"
                              },
                              "maximumAmount": {
                                "type": "integer",
                                "description": "minimum amount of refill allowed (in cents).",
                                "format": "int32"
                              }
                            }
                          },
                          "_links": {
                            "type": "object",
                            "required": [
                              "self"
                            ],
                            "properties": {
                              "self": {
                                "type": "object",
                                "properties": {
                                  "href": {
                                    "type": "string",
                                    "format": "uri"
                                  }
                                }
                              },
                              "direct-payment": {
                                "type": "object",
                                "properties": {
                                  "href": {
                                    "type": "string",
                                    "format": "uri"
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "Invalid input in the request body."
          },
          "404": {
            "description": "Subscriber not found.",
            "schema": {
              "type": "object",
              "required": [
                "code",
                "message"
              ],
              "properties": {
                "code": {
                  "type": "integer",
                  "format": "int32"
                },
                "message": {
                  "type": "string"
                }
              }
            }
          },
          "409": {
            "description": "Subscriber with the combination of mobile number and pin does not exist."
          },
          "415": {
            "description": "Unsupported media type."
          }
        }
      }
    },
    "/{account-id}/direct-payments/{transaction-id}": {
      "put": {
        "tags": [
          "subscriber"
        ],
        "description": "Applies payment to a Prepaid Subscriber",
        "operationId": "applyDirectPaymentToPrepaidSubscriber",
        "parameters": [
          {
            "name": "channel-id",
            "in": "header",
            "description": "channel the request originated from",
            "required": true,
            "type": "string"
          },
          {
            "name": "application-id",
            "in": "header",
            "description": "application the request originated from",
            "required": true,
            "type": "string"
          },
          {
            "name": "store-id",
            "in": "header",
            "description": "Unique identifier for the retail store location.",
            "required": false,
            "type": "string"
          },
          {
            "name": "account-id",
            "in": "path",
            "description": "unique identifier for a subscriber.",
            "required": true,
            "type": "integer"
          },
          {
            "name": "transaction-id",
            "in": "path",
            "description": "Unique transaction associated with the payment.",
            "required": true,
            "type": "string"
          },
          {
            "name": "body",
            "in": "body",
            "description": "Request to apply direct payment",
            "required": true,
            "schema": {
              "type": "object",
              "required": [
                "paymentAmountInCents"
              ],
              "properties": {
                "paymentAmountInCents": {
                  "type": "integer",
                  "description": "payment amount in cents for the direct payment",
                  "format": "int32"
                }
              }
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Success response"
          },
          "400": {
            "description": "Invalid input in the request body"
          },
          "404": {
            "description": "Subscriber not found",
            "schema": {
              "type": "object",
              "required": [
                "code",
                "message"
              ],
              "properties": {
                "code": {
                  "type": "integer",
                  "format": "int32"
                },
                "message": {
                  "type": "string"
                }
              }
            }
          },
          "409": {
            "description": "If the subscriber status does not support payment."
          },
          "412": {
            "description": "If the transaction was already executed with different parameters."
          },
          "415": {
            "description": "Unsupported media type."
          }
        }
      }
    }
  },
  "parameters": {
    "header-authorization": {
      "in": "header",
      "name": "authorization",
      "description": "authorization header",
      "required": false,
      "type": "string"
    }
  },
  "definitions": {
    "skuDefinition": {
      "type": "object",
      "required": [
        "skuIdentifier",
        "skuDescription",
        "faceValueInCents"
      ],
      "properties": {
        "skuIdentifier": {
          "type": "string"
        },
        "skuDescription": {
          "type": "string"
        },
        "faceValueInCents": {
          "type": "integer",
          "format": "int32"
        }
      }
    },
    "prepaidSubscribers": {
      "type": "object",
      "required": [
        "_embedded"
      ],
      "properties": {
        "_embedded": {
          "type": "object",
          "required": [
            "accounts"
          ],
          "properties": {
            "accounts": {
              "type": "array",
              "items": {
                "type": "object",
                "required": [
                  "availableEpinSkus",
                  "refillAllowedInCents"
                ],
                "properties": {
                  "systemInRecoveryMode": {
                    "type": "boolean",
                    "description": "Set to true if one or more dependent services are unavailable. This might result in response missing some elements."
                  },
                  "firstName": {
                    "type": "string",
                    "description": "first name of the subscriber."
                  },
                  "lastName": {
                    "type": "string",
                    "description": "last name of the subscriber."
                  },
                  "ratePlan": {
                    "type": "object",
                    "required": [
                      "ratePlanDescription",
                      "ratePlanSupportsOnlyData"
                    ],
                    "properties": {
                      "ratePlanDescription": {
                        "type": "string",
                        "description": "short description of the rate plan from product catalog."
                      },
                      "ratePlanSupportsOnlyData": {
                        "type": "boolean",
                        "description": "Indicates if rate plan supports only data"
                      }
                    }
                  },
                  "accountStatus": {
                    "type": "string",
                    "description": "return 'active' or 'suspended' based on subscriber status"
                  },
                  "moneyBalance": {
                    "type": "object",
                    "description": "real time account balance of a subscriber.",
                    "required": [
                      "balanceInCents",
                      "expirationDate"
                    ],
                    "properties": {
                      "balanceInCents": {
                        "type": "integer",
                        "format": "int32",
                        "description": "real time account balance of a subscriber in cents."
                      },
                      "expirationDate": {
                        "type": "string",
                        "format": "date",
                        "description": "expiration date of the real time account balance of a subscriber."
                      }
                    }
                  },
                  "availableEpinSkus": {
                    "type": "array",
                    "items": {
                      "type": "object",
                      "required": [
                        "skuIdentifier",
                        "skuDescription",
                        "faceValueInCents"
                      ],
                      "properties": {
                        "skuIdentifier": {
                          "type": "string"
                        },
                        "skuDescription": {
                          "type": "string"
                        },
                        "faceValueInCents": {
                          "type": "integer",
                          "format": "int32"
                        }
                      }
                    }
                  },
                  "ratePlanSupportsServiceCycle": {
                    "type": "boolean",
                    "description": "Set to true if the subscriber is on a rate plan which renews every period."
                  },
                  "isServiceCyclePaid": {
                    "type": "boolean",
                    "description": "Set to true if the subscriber is on a rate plan which has a service cycle and is paid."
                  },
                  "currentServiceCycle": {
                    "type": "object",
                    "description": "This element would be returned if the subscriber is paid on a rate plan which has a service cycle",
                    "required": [
                      "currentServicePeriodExpirationDate",
                      "nextServicePeriodStartDate",
                      "estimatedRenewalChargeInCents",
                      "estimatedRefillAmountNeededInCents"
                    ],
                    "properties": {
                      "currentServicePeriodExpirationDate": {
                        "type": "string",
                        "format": "date"
                      },
                      "nextServicePeriodStartDate": {
                        "type": "string",
                        "description": "Start date of next service period",
                        "format": "date"
                      },
                      "estimatedRenewalChargeInCents": {
                        "type": "integer",
                        "description": "estimated charge in cents for the subscriber to be paid after the scheduled renewal.",
                        "format": "int32"
                      },
                      "estimatedRefillAmountNeededInCents": {
                        "type": "integer",
                        "description": "estimated refill in cents for the subscriber to be paid after the scheduled renewal.",
                        "format": "int32"
                      }
                    }
                  },
                  "immediateRenewalInfo": {
                    "type": "object",
                    "description": "This element would be returned if the subscriber is unpaid on a rate plan which has a service cycle.",
                    "required": [
                      "totalCostInCents",
                      "refillAmountNeededInCents"
                    ],
                    "properties": {
                      "totalCostInCents": {
                        "type": "integer",
                        "description": "total charge in cents needed by the subscriber for immediate renewal.",
                        "format": "int32"
                      },
                      "refillAmountNeededInCents": {
                        "type": "integer",
                        "description": "minimum refill in cents needed by the subscriber for immediate renewal.",
                        "format": "int32"
                      }
                    }
                  },
                  "canApplyFunds": {
                    "type": "boolean",
                    "description": "Set to true if the subscriber can accept payments or voucher redemptions"
                  },
                  "refillAllowedInCents": {
                    "type": "object",
                    "required": [
                      "minimumAmount",
                      "maximumAmount"
                    ],
                    "properties": {
                      "minimumAmount": {
                        "type": "integer",
                        "description": "minimum amount of refill allowed (in cents).",
                        "format": "int32"
                      },
                      "maximumAmount": {
                        "type": "integer",
                        "description": "minimum amount of refill allowed (in cents).",
                        "format": "int32"
                      }
                    }
                  },
                  "_links": {
                    "type": "object",
                    "required": [
                      "self"
                    ],
                    "properties": {
                      "self": {
                        "type": "object",
                        "properties": {
                          "href": {
                            "type": "string",
                            "format": "uri"
                          }
                        }
                      },
                      "direct-payment": {
                        "type": "object",
                        "properties": {
                          "href": {
                            "type": "string",
                            "format": "uri"
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "prepaidSubscriber": {
      "type": "object",
      "required": [
        "availableEpinSkus",
        "refillAllowedInCents"
      ],
      "properties": {
        "systemInRecoveryMode": {
          "type": "boolean",
          "description": "Set to true if one or more dependent services are unavailable. This might result in response missing some elements."
        },
        "firstName": {
          "type": "string",
          "description": "first name of the subscriber."
        },
        "lastName": {
          "type": "string",
          "description": "last name of the subscriber."
        },
        "ratePlan": {
          "type": "object",
          "required": [
            "ratePlanDescription",
            "ratePlanSupportsOnlyData"
          ],
          "properties": {
            "ratePlanDescription": {
              "type": "string",
              "description": "short description of the rate plan from product catalog."
            },
            "ratePlanSupportsOnlyData": {
              "type": "boolean",
              "description": "Indicates if rate plan supports only data"
            }
          }
        },
        "accountStatus": {
          "type": "string",
          "description": "return 'active' or 'suspended' based on subscriber status"
        },
        "moneyBalance": {
          "type": "object",
          "description": "real time account balance of a subscriber.",
          "required": [
            "balanceInCents",
            "expirationDate"
          ],
          "properties": {
            "balanceInCents": {
              "type": "integer",
              "format": "int32",
              "description": "real time account balance of a subscriber in cents."
            },
            "expirationDate": {
              "type": "string",
              "format": "date",
              "description": "expiration date of the real time account balance of a subscriber."
            }
          }
        },
        "availableEpinSkus": {
          "type": "array",
          "items": {
            "type": "object",
            "required": [
              "skuIdentifier",
              "skuDescription",
              "faceValueInCents"
            ],
            "properties": {
              "skuIdentifier": {
                "type": "string"
              },
              "skuDescription": {
                "type": "string"
              },
              "faceValueInCents": {
                "type": "integer",
                "format": "int32"
              }
            }
          }
        },
        "ratePlanSupportsServiceCycle": {
          "type": "boolean",
          "description": "Set to true if the subscriber is on a rate plan which renews every period."
        },
        "isServiceCyclePaid": {
          "type": "boolean",
          "description": "Set to true if the subscriber is on a rate plan which has a service cycle and is paid."
        },
        "currentServiceCycle": {
          "type": "object",
          "description": "This element would be returned if the subscriber is paid on a rate plan which has a service cycle",
          "required": [
            "currentServicePeriodExpirationDate",
            "nextServicePeriodStartDate",
            "estimatedRenewalChargeInCents",
            "estimatedRefillAmountNeededInCents"
          ],
          "properties": {
            "currentServicePeriodExpirationDate": {
              "type": "string",
              "format": "date"
            },
            "nextServicePeriodStartDate": {
              "type": "string",
              "description": "Start date of next service period",
              "format": "date"
            },
            "estimatedRenewalChargeInCents": {
              "type": "integer",
              "description": "estimated charge in cents for the subscriber to be paid after the scheduled renewal.",
              "format": "int32"
            },
            "estimatedRefillAmountNeededInCents": {
              "type": "integer",
              "description": "estimated refill in cents for the subscriber to be paid after the scheduled renewal.",
              "format": "int32"
            }
          }
        },
        "immediateRenewalInfo": {
          "type": "object",
          "description": "This element would be returned if the subscriber is unpaid on a rate plan which has a service cycle.",
          "required": [
            "totalCostInCents",
            "refillAmountNeededInCents"
          ],
          "properties": {
            "totalCostInCents": {
              "type": "integer",
              "description": "total charge in cents needed by the subscriber for immediate renewal.",
              "format": "int32"
            },
            "refillAmountNeededInCents": {
              "type": "integer",
              "description": "minimum refill in cents needed by the subscriber for immediate renewal.",
              "format": "int32"
            }
          }
        },
        "canApplyFunds": {
          "type": "boolean",
          "description": "Set to true if the subscriber can accept payments or voucher redemptions"
        },
        "refillAllowedInCents": {
          "type": "object",
          "required": [
            "minimumAmount",
            "maximumAmount"
          ],
          "properties": {
            "minimumAmount": {
              "type": "integer",
              "description": "minimum amount of refill allowed (in cents).",
              "format": "int32"
            },
            "maximumAmount": {
              "type": "integer",
              "description": "minimum amount of refill allowed (in cents).",
              "format": "int32"
            }
          }
        },
        "_links": {
          "type": "object",
          "required": [
            "self"
          ],
          "properties": {
            "self": {
              "type": "object",
              "properties": {
                "href": {
                  "type": "string",
                  "format": "uri"
                }
              }
            },
            "direct-payment": {
              "type": "object",
              "properties": {
                "href": {
                  "type": "string",
                  "format": "uri"
                }
              }
            }
          }
        }
      }
    },
    "directPayment": {
      "type": "object",
      "required": [
        "paymentAmountInCents"
      ],
      "properties": {
        "paymentAmountInCents": {
          "type": "integer",
          "description": "payment amount in cents for the direct payment",
          "format": "int32"
        }
      }
    },
    "errorModel": {
      "type": "object",
      "required": [
        "code",
        "message"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "format": "int32"
        },
        "message": {
          "type": "string"
        }
      }
    }
  }
}