{
  "swagger": "2.0",
  "info": {
    "description": "This document lists version 1 for financial account lookup APIs.",
    "version": "1.0",
    "title": "financial accounts lookup APIs with aggregated data"
  },
  "basePath": "/billing/v1/financial-accounts",
  "tags": [
    {
      "name": "financial-account",
      "description": "An account used for billing purposes"
    },
    {
      "name": "subscriber",
      "description": "A party that subscribes for a product and uses them in an ongoing way"
    },
    {
      "name": "organization",
      "description": "Represents a group of people identified by a shared interest or purpose such as a corporate node"
    }
  ],
  "paths": {
    "/billing/v1/financial-accounts/{financialAccountId}/account-details": {
      "get": {
        "tags": [
          "financial-account"
        ],
        "summary": "Get details for a given financial account resource with aggregated data on its sub-resources",
        "description": "This API retrives information about a specific financial account, and also includes aggregated totals attributes for its sub-resources.",
        "operationId": "billing.financial-account-details.aggregated",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "$ref": "#/parameters/header-activityid"
          },
          {
            "$ref": "#/parameters/header-authorization"
          },
          {
            "$ref": "#/parameters/header-dealercode"
          },
          {
            "$ref": "#/parameters/header-applicationuserid"
          },
          {
            "$ref": "#/parameters/path-financial-account-id"
          },
          {
            "$ref": "#/parameters/query-pagenumber"
          },
          {
            "$ref": "#/parameters/query-pagesize"
          }
        ],
        "responses": {
          "200": {
            "description": "The request was successful",
            "schema": {
              "$ref": "#/definitions/financialAccountDetailsResponse"
            }
          },
          "400": {
            "description": "Bad request error"
          },
          "404": {
            "description": "Resource not found"
          },
          "500": {
            "description": "The server encountered an unexpected condition which prevented it from fulfilling the request"
          }
        }
      }
    },
    "/billing/v1/financial-accounts/account-summary": {
      "get": {
        "tags": [
          "financial-account"
        ],
        "summary": "Get summary and totals attributes attributes for a collection of financial account resources based on search criteria",
        "description": "This API retrives information about collection of financial accounts and aggregated totals attributes for its sub-resources.",
        "operationId": "billing.financial-account-search.aggregated",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "$ref": "#/parameters/header-activityid"
          },
          {
            "$ref": "#/parameters/header-authorization"
          },
          {
            "$ref": "#/parameters/header-dealercode"
          },
          {
            "$ref": "#/parameters/header-applicationuserid"
          },
          {
            "$ref": "#/parameters/query-financial-accounts-ids"
          }
        ],
        "responses": {
          "200": {
            "description": "The request was successful",
            "schema": {
              "$ref": "#/definitions/financialAccountsSearchResponse"
            }
          },
          "400": {
            "description": "Bad request error"
          },
          "404": {
            "description": "Resource not found"
          },
          "500": {
            "description": "The server encountered an unexpected condition which prevented it from fulfilling the request"
          }
        }
      }
    }
  },
  "parameters": {
    "header-activityid": {
      "in": "header",
      "name": "activityid",
      "description": "activity id",
      "required": true,
      "type": "string"
    },
    "header-authorization": {
      "in": "header",
      "name": "authorization",
      "description": "authorization header",
      "required": false,
      "type": "string"
    },
    "header-dealercode": {
      "in": "header",
      "name": "dealercode",
      "description": "dealer code",
      "required": false,
      "type": "string"
    },
    "header-applicationuserid": {
      "in": "header",
      "name": "applicationuserid",
      "description": "application user id",
      "required": false,
      "type": "string"
    },
    "path-financial-account-id": {
      "in": "path",
      "name": "financialAccountId",
      "description": "financial account id",
      "required": true,
      "type": "string"
    },
    "query-financial-accounts-ids": {
      "in": "query",
      "name": "financialAccountId",
      "description": "financial account id",
      "required": false,
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "query-pagenumber": {
      "in": "query",
      "name": "pagenumber",
      "required": false,
      "type": "integer"
    },
    "query-pagesize": {
      "in": "query",
      "name": "pagesize",
      "required": false,
      "type": "integer"
    }
  },
  "definitions": {
    "IdRef": {
      "type": "object",
      "properties": {
        "rel": {
          "type": "string",
          "enum": [
            "self",
            "parent",
            "child",
            "next",
            "prev",
            "first",
            "last",
            "item",
            "collection",
            "organization",
            "subscriber",
            "financial-account",
            "next-organization",
            "usage",
            "bill-charges",
            "service-agreement",
            "bill-adjustments"
          ]
        },
        "id": {
          "type": "string"
        },
        "href": {
          "type": "string"
        }
      }
    },
    "ProductRef": {
      "type": "object",
      "properties": {
        "id": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "startDateTime": {
          "type": "string",
          "format": "date-time"
        },
        "endDateTime": {
          "type": "string",
          "format": "date-time"
        },
        "type": {
          "type": "string",
          "enum": [
            "ratePlan",
            "service",
            "data"
          ]
        },
        "productOffering": {
          "$ref": "#/definitions/ProductOfferingRef"
        }
      }
    },
    "Price": {
      "type": "object",
      "properties": {
        "amount": {
          "type": "number",
          "format": "double"
        }
      }
    },
    "ProductSpecificationRef": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "type": {
          "type": "string",
          "enum": [
            "ratePlan",
            "service",
            "data"
          ]
        },
        "offeringIndicator": {
          "type": "string"
        }
      }
    },
    "ContactRef": {
      "type": "object",
      "properties": {
        "title": {
          "type": "string"
        },
        "firstName": {
          "type": "string"
        },
        "middleInitial": {
          "type": "string"
        },
        "lastName": {
          "type": "string"
        },
        "suffix": {
          "type": "string"
        }
      }
    },
    "PhoneRef": {
      "type": "object",
      "properties": {
        "home": {
          "type": "string"
        },
        "contact": {
          "type": "string"
        },
        "contactExtension": {
          "type": "string"
        },
        "fax": {
          "type": "string"
        },
        "work": {
          "type": "string"
        },
        "workExtension": {
          "type": "string"
        },
        "other": {
          "type": "string"
        }
      }
    },
    "BasicNameRef": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "type": {
          "type": "string"
        },
        "description": {
          "type": "string"
        }
      }
    },
    "BillingCycleSpecificationRef": {
      "type": "object",
      "properties": {
        "cycle": {
          "type": "string"
        },
        "nextCycle": {
          "type": "string"
        }
      }
    },
    "AccountTypeRef": {
      "type": "object",
      "properties": {
        "accountTypeCode": {
          "type": "string"
        },
        "accountTypeName": {
          "type": "string"
        },
        "accountSubTypeCode": {
          "type": "string"
        },
        "accountSubTypeName": {
          "type": "string"
        }
      }
    },
    "SubscriberRef": {
      "type": "object",
      "properties": {
        "link": {
          "$ref": "#/definitions/IdRef"
        },
        "status": {
          "type": "string"
        },
        "contact": {
          "$ref": "#/definitions/ContactRef"
        },
        "totalMonthlyRecurringCharges": {
          "type": "number",
          "format": "double"
        },
        "products": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ProductRef"
          }
        }
      }
    },
    "ProductOfferingPrice": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "price": {
          "$ref": "#/definitions/Price"
        }
      }
    },
    "ProductOfferingRef": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "price": {
          "$ref": "#/definitions/ProductOfferingPrice"
        },
        "specification": {
          "$ref": "#/definitions/ProductSpecificationRef"
        }
      }
    },
    "financialAccountDetailsResponse": {
      "type": "object",
      "properties": {
        "_links": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/IdRef"
          }
        },
        "contact": {
          "$ref": "#/definitions/ContactRef"
        },
        "contactPhones": {
          "$ref": "#/definitions/PhoneRef"
        },
        "status": {
          "$ref": "#/definitions/BasicNameRef"
        },
        "collectionStatus": {
          "$ref": "#/definitions/BasicNameRef"
        },
        "creditClass": {
          "$ref": "#/definitions/BasicNameRef"
        },
        "billingMethod": {
          "$ref": "#/definitions/BasicNameRef"
        },
        "collectionStatusDate": {
          "type": "string",
          "format": "date-time"
        },
        "accountActivationDate": {
          "type": "string",
          "format": "date-time"
        },
        "billStructure": {
          "$ref": "#/definitions/BillingCycleSpecificationRef"
        },
        "currentBillSequence": {
          "type": "string",
          "format": "int64"
        },
        "financialAccountTotals": {
          "type": "object",
          "properties": {
            "subscribersTotal": {
              "type": "integer",
              "format": "int64"
            },
            "activeSubscribersTotal": {
              "type": "integer",
              "format": "int64"
            },
            "suspendedSubscribersTotal": {
              "type": "integer",
              "format": "int64"
            },
            "reservedSubscribersTotal": {
              "type": "integer",
              "format": "int64"
            },
            "cancelledSubscribersTotal": {
              "type": "integer",
              "format": "int64"
            }
          }
        },
        "subscribers": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/SubscriberRef"
          }
        },
        "type": {
          "$ref": "#/definitions/AccountTypeRef"
        }
      }
    },
    "financialAccountSummary": {
      "type": "object",
      "properties": {
        "_links": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/IdRef"
          }
        },
        "financialAccountTotals": {
          "type": "object",
          "properties": {
            "subscribersTotal": {
              "type": "integer",
              "format": "int64"
            },
            "activeSubscribersTotal": {
              "type": "integer",
              "format": "int64"
            },
            "suspendedSubscribersTotal": {
              "type": "integer",
              "format": "int64"
            },
            "reservedSubscribersTotal": {
              "type": "integer",
              "format": "int64"
            },
            "cancelledSubscribersTotal": {
              "type": "integer",
              "format": "int64"
            }
          }
        },
        "totalMonthlyRecurringCharges": {
          "type": "number",
          "format": "double"
        },
        "totalMonthlyIncidentalCharges": {
          "type": "number",
          "format": "double"
        },
        "ratePlans": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ProductRef"
          }
        },
        "services": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ProductRef"
          }
        }
      }
    },
    "financialAccountsSearchResponse": {
      "type": "object",
      "properties": {
        "financialAccounts": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/financialAccountSummary"
          }
        }
      }
    }
  }
}