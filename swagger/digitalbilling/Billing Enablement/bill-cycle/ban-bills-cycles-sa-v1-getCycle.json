{
  "swagger": "2.0",
  "info": {
    "title": "Get bill cycle details for a cycle code",
    "description": "This service  will check cycle details for a FAN for its cycle code",
    "version": "1.0.0",
    "contact": {
      "name": "Rohit Shalgar",
      "email": "Rohit.Shalgar@amdocs.com"
    },
    "x-createdBy": {
      "dateCreated": "Sun May 17 13:08:21 2018",
      "createdBy": "rohishal",
      "application": "Recite",
      "appVersion": "0.3.6.2170",
      "documentId": "bf1badac-33bc-4cfb-baf4-363194f3e53d",
      "status": "Conceptual - Initial",
      "classification": "7.3.4 Billing Inquiries & Support Mgmt.",
      "profile": "Core Business Capability Service",
      "serviceLayer": "Resource - Billing"
    }
  },
  "host": "dev01.api.t-mobile.com",
  "basePath": "/billing/v1/bill-cycle",
  "schemes": [
    "https"
  ],
  "paths": {
    "/billing/v1/bill-cycle": {
      "get": {
        "tags": [
          "get Information about bill cycle"
        ],
        "x-api-pattern": "QueryResource",
        "summary": "Get info about bill cycle  ",
        "description": "get all cycle details of a cycle based on its cycle code",
        "operationId": "getCycleinfo",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "cycleCode",
            "in": "query",
            "required": true,
            "type": "number",
            "format": "short",
            "description": "cycle code of the FAN",
            "x-example": "2",
            "pattern": "^[\\S]*$"
          },
          {
            "name": "servicePartner",
            "in": "header",
            "required": false,
            "description": "Service Partner ID of SIM number. Possible values- MKB: MOCKING_BIRD, TMO: REGULAR_T-MOBILE, MPC: METRO_PCS, WHL: MVNO",
            "x-example": "REGULAR_T-MOBILE",
            "type": "string",
            "enum": [
              "ALL",
              "TMO",
              "MPS",
              "WHL",
              "MKB"
            ]
          },
          {
            "name": "Accept",
            "in": "header",
            "description": "Preferred content type _ default to application/json",
            "x-example": "application/json",
            "required": false,
            "type": "string",
            "minLength": 1,
            "maxLength": 256,
            "pattern": "^[ \\S]+$"
          },
          {
            "name": "Accept-Encoding",
            "in": "header",
            "description": "List of acceptable encodings. See HTTP compression.",
            "x-example": "gzip,deflate",
            "required": false,
            "type": "string",
            "minLength": 1,
            "maxLength": 256,
            "pattern": "^[\\S ]*$"
          },
          {
            "name": "Accept-Language",
            "in": "header",
            "description": "List of acceptable human languages for response",
            "x-example": "en-us,en;q=0.5",
            "required": false,
            "type": "string",
            "minLength": 1,
            "maxLength": 256,
            "pattern": "^[\\S ]*$"
          },
          {
            "name": "Authorization",
            "in": "header",
            "description": "OAuth 2.0 access token with the authentication type set as Bearer.",
            "required": true,
            "type": "string",
            "pattern": "^[ \\S]+$",
            "x-example": "Bearer xAbczydfGDHji"
          },
          {
            "name": "X-Authorization",
            "in": "header",
            "description": "OAuth 2.0 access token with the authentication type set as Bearer.",
            "required": false,
            "type": "string",
            "pattern": "^[ \\S]+$",
            "x-example": "Bearer xAbczydfGDHji"
          },
          {
            "name": "X-Auth-Originator",
            "in": "header",
            "description": "OAuth 2.0 access token with the authentication type set as Bearer.",
            "required": true,
            "type": "string",
            "pattern": "^[ \\S]+$",
            "x-example": "Bearer xAbczydfGDHji"
          },
          {
            "name": "Date",
            "in": "header",
            "description": "The Date general HTTP header contains the date and time at which the message was originated.",
            "x-example": "Mon, 05 Mar 2018 16:38:08 GMT",
            "required": false,
            "type": "string",
            "format": "date-time",
            "pattern": "^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), [0-3]{1}[0-9]{1} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [1-3][0-9]{3} [0-2][0-9]:[0-6][0-9]:[0-6][0-9] GMT$"
          },
          {
            "name": "activity-id",
            "in": "header",
            "description": "Unique id alphanumeric value generated and sent by the partner to identify every the service request uniquely. Sample: XyzA:D1s5s1i-X6AFRvfiPBfs-1234  Pass as is to downstream calls.",
            "x-example": "c34e7acd-384b-4c22-8b02-ba3963682508",
            "required": false,
            "type": "string",
            "minLength": 1,
            "maxLength": 256,
            "pattern": "^[\\S]*$"
          },
          {
            "name": "authorization-idp",
            "in": "header",
            "description": "API chain initiating caller_s IDP token",
            "x-example": "c34e7acd-384b-4c22-8b02-ba3963682508",
            "required": false,
            "type": "string",
            "minLength": 1,
            "maxLength": 256,
            "pattern": "^[\\S]*$"
          },
          {
            "name": "interaction-id",
            "in": "header",
            "description": "Alphanumeric value represent a common transaction id across all calls made from UI, while completing all business activity needs of a particular customer.",
            "x-example": "cc688d54-50d1-49d4-8c5d-95459d1172e8",
            "required": false,
            "type": "string",
            "minLength": 1,
            "maxLength": 256,
            "pattern": "^[\\S]*$"
          },
          {
            "name": "service-transaction-id",
            "in": "header",
            "description": "Request ID echoed back by server",
            "x-example": "23409209723",
            "required": false,
            "type": "string",
            "minLength": 1,
            "maxLength": 256,
            "pattern": "^[\\S]*$"
          },
          {
            "name": "session-id",
            "in": "header",
            "description": "A value populated by the sender used to track the transactions that occur during a session, a long-lasting interaction that ismaintained by the sender.",
            "x-example": "350b91ec-4a64-4b10-a3f3-a78c8db3924a",
            "required": false,
            "type": "string",
            "minLength": 1,
            "maxLength": 256,
            "pattern": "^[\\S]*$"
          },
          {
            "name": "timestamp",
            "in": "header",
            "description": "ms since 1/1/70 adjusted to GMT",
            "x-example": "1524599536",
            "required": false,
            "type": "integer",
            "format": "int64",
            "minimum": 0,
            "maximum": 999999999999,
            "pattern": "^[\\d]*$"
          },
          {
            "name": "tmo-authorization",
            "in": "header",
            "description": "Contains Proof of Possession token generated by API caller",
            "x-example": "c34e7acd-384b-4c22-8b02-ba3963682508",
            "required": false,
            "type": "string",
            "minLength": 1,
            "maxLength": 256,
            "pattern": "^[\\S ]*$"
          },
          {
            "name": "workflow-id",
            "in": "header",
            "description": "Business Use Case",
            "x-example": "ACTIVATION",
            "required": false,
            "type": "string",
            "minLength": 1,
            "maxLength": 256,
            "pattern": "^[\\S]*$"
          },
          {
            "name": "channelId",
            "in": "header",
            "description": "Identifies the business unit or sales channel for the Request.Below are the sample values:            Care            TeleSales            FullService            BusinessDirect            FinancePartners            NonFinancePartners            SelfServiceAuthenticated            SelfServiceAnonymous            InteractiveVoiceResponse",
            "required": true,
            "type": "string",
            "x-example": "123",
            "pattern": "^[\\S]*$"
          },
          {
            "name": "senderId",
            "in": "header",
            "description": "Uniquely identifies an Operation consumer. Below are the sample values.ACUI, MYTMO, QVXP, REBELLION, WALMARTRETAIL, SAMSCLUBRETAIL.",
            "required": true,
            "type": "string",
            "x-example": "CSM",
            "pattern": "^[\\S]*$"
          },
          {
            "name": "applicationId",
            "in": "header",
            "description": "Identifies the application, system, or tool that is being used to perform the transaction related to this request. Below are the sample values.ACUI, ESERVICE, REBELLION, WARP",
            "required": true,
            "type": "string",
            "x-example": "DIGAPP, IHAP, VARUI",
            "pattern": "^[\\S]*$"
          }
        ],
        "responses": {
          "200": {
            "description": "Returning a cycleDetails of a cycle",
            "schema": {
              "$ref": "#/definitions/cycleDetails"
            },
            "examples": {
              "application/json": {
                "closeDay": 3,
                "totalFinancialAccounts": 20,
                "billDay": 5,
                "dueDay": 10,
                "totalSubscribers": 12,
                "financialAccountAllocationEnabled": "YES",
                "flexibleCycleCloseDayEnabled":"YES"
              }
            },
            "headers": {
              "Content-Length": {
                "description": "The Content-Length header specifies the actual length of the returned payload.",
                "type": "string",
                "x-example": "340 or any number value"
              },
              "Content-Type": {
                "description": "The Content-Type header specifies the kind of data expected by the client in response.",
                "type": "string",
                "x-example": "application/json",
                "pattern": "^[\\S]*$"
              },
              "Date": {
                "description": "The date and time that the message was originated (in HTTP-date format as defined by RFC 7231 Date/Time Formats).",
                "type": "string",
                "format": "date-time",
                "x-example": "2018-05-23T16:10:46.294Z"
              },
              "Cache-Control": {
                "description": "No cache indicators supported at this time. Samples: no-cache",
                "x-example": "no-cache",
                "type": "string"
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "400",
                "userMessage": "Bad Request",
                "systemMessage": "Bad Request",
                "detailLink": "http://www.tmus.com"
              }
            },
            "headers": {}
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "401",
                "userMessage": "Unauthorized",
                "systemMessage": "Unauthorized",
                "detailLink": "http://www.tmus.com"
              }
            },
            "headers": {}
          },
          "404": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "404",
                "userMessage": "Resource not found",
                "systemMessage": "Resource not found",
                "detailLink": "http://www.tmus.com"
              }
            },
            "headers": {}
          },
          "405": {
            "description": "Method Not Allowed",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "405",
                "userMessage": "Method Not Allowed",
                "systemMessage": "Method Not Allowed",
                "detailLink": "http://www.tmus.com"
              }
            },
            "headers": {
              "Allow": {
                "description": "list of supported methods for URI",
                "x-example": "GET",
                "type": "string",
                "minLength": 1,
                "maxLength": 256,
                "pattern": "^[\\S]*$"
              },
              "service-transaction-id": {
                "description": "Request ID echoed back by server",
                "x-example": "23409209723",
                "type": "string",
                "minLength": 1,
                "maxLength": 256,
                "pattern": "^[\\S]*$"
              }
            }
          },
          "406": {
            "description": "Mismatching data format",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "406",
                "userMessage": "Mismatching data format",
                "systemMessage": "Mismatching data format",
                "detailLink": "http://www.tmus.com"
              }
            },
            "headers": {}
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "500",
                "userMessage": "System Error",
                "systemMessage": "System Error",
                "detailLink": "http://www.tmus.com"
              }
            },
            "headers": {}
          },
          "503": {
            "description": "Service unavailable",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "503",
                "userMessage": "Service unavailable",
                "systemMessage": "Service unavailable",
                "detailLink": "http://www.tmus.com"
              }
            },
            "headers": {}
          }
        },
        "security": [
          {
            "Authorization-idp": []
          }
        ]
      }
    }
  },
  "securityDefinitions": {
    "Authorization-idp": {
      "type": "oauth2",
      "description": "oauth2 Authentication",
      "tokenUrl": "https://host:env/v1/oauth2/accesstoken?grant_type=client_credentials",
      "flow": "application",
      "scopes": {
        "": ""
      }
    }
  },
  "definitions": {
    "cycleDetails": {
      "description": "cycle details of a cycle code",
      "type": "object",
      "properties": {
        "closeDay": {
          "type": "number",
          "format": "short",
          "description": "The day of the month on which the Billing period for this cycle ends"
        },
        "totalFinancialAccounts": {
          "type": "number",
          "format": "int32",
          "description": "The number of financialAccounts in the cycle. It is updated automatically while assigning or de-assigning a cycle to a customer"
        },
        "billDay": {
          "type": "number",
          "format": "short",
          "description": "The day of the month that will be used to calculate the Bill Date of the bill"
        },
        "dueDay": {
          "type": "number",
          "format": "short",
          "description": "The day of the month that will be used to calculate the Bill Due Date of the bill"
        },
        "totalSubscribers": {
          "type": "number",
          "format": "int32",
          "description": "No of subscribers in a cycle"
        },
        "financialAccountAllocationEnabled": {
          "type": "string",
          "description": "if its Y then CSM can allocate additional FAN's for the cycle else(N) CSM will not receive the cycle for selection, but can still allocate a FAN to this cycle",
          "enum": [
            "YES",
            "NO"
          ]
        }
      }
    },
    "Error": {
      "type": "object",
      "description": "As defined in http://api-standards.apps.px-npe01.cf.t-mobile.com/http/status-codes/error-response-format/",
      "required": [
        "code",
        "userMessage"
      ],
      "properties": {
        "code": {
          "description": "A succinct, domain-specific, human-readable text string to identify the type of error for the given status code",
          "example": "ProductNotFound",
          "type": "string",
          "pattern": "^[\\S ]+$"
        },
        "userMessage": {
          "description": "A human-readable message describing the error.",
          "example": "Failed to find product.",
          "type": "string",
          "pattern": "^[\\S ]+$"
        },
        "systemMessage": {
          "description": "Text that provides a more detailed technical explanation of the error",
          "example": "PRODUCT_NOT_FOUND",
          "type": "string"
        },
        "detailLink": {
          "description": "link to custom information providing greater detail on error or errors",
          "example": "http://aaa.bbb.ccc/",
          "type": "string"
        }
      }
    }
  }
}

