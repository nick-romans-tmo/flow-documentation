swagger: '2.0'
info:
  description: This document lists version 1.0 for the billing API.
  version: '1.0'
  title: Billing
host: localhost
basePath: /billing/v1/financial-accounts
paths:
  '/{fanID}/equipment-summary':
    get:
      description: >-
        This API will return all the equipment, plans and promotions for an
        account.
      operationId: equipment-summary-by-fan-id
      produces:
        - application/json
      parameters:
        - name: authorization
          in: header
          description: authorization header
          required: false
          type: string
        - name: activityId
          in: header
          description: activityId header
          required: false
          type: string
        - name: dealerCode
          in: header
          description: dealerCode header
          required: false
          type: string
        - name: applicationUserId
          in: header
          description: application user id
          required: false
          type: string
        - name: userId
          in: header
          description: user id
          required: false
          type: string
        - name: fanID
          in: path
          description: customer id
          required: true
          type: string
        - name: includeAlerts
          in: query
          description: Flag that marks whether the response includes alerts or not
          type: boolean
          allowEmptyValue: false
          required: false
      responses:
        '200':
          description: The request was successful
          schema:
            $ref: '#/definitions/SubscribersDeviceDetailsResponse'
        '400':
          description: Bad request error
        '404':
          description: Record not found
        '500':
          description: >-
            The server encountered an unexpected condition which prevented it
            from fulfilling the request
definitions:
  SubscribersDeviceDetailsResponse:
    required:
      - subscribersDeviceDetailsList
      - promotionsAlertsList
    properties:
      subscribersDeviceDetailsList:
        type: array
        items:
          $ref: '#/definitions/SubscriberDeviceDetails'
      promotionsAlertsList:
        type: array
        items:
          $ref: '#/definitions/PromotionAlert'
  SubscriberDevicePromotion:
    properties:
      promotionCode:
        type: string
      description:
        type: string
      status:
        type: string
      statusReason:
        type: string
      effectiveDate:
        type: string
      amount:
        type: number
      durationInMonths:
        type: integer
      remainingAmount:
        type: number
      remainingDurationInMonths:
        type: integer
      appliedAmount:
        type: number
      completedDurationInMonths:
        type: integer
      monthlyCredit:
        type: number
      promotionEndDate:
        type: string
      promotionEvaluationDate:
        type: string
  SubscriberDeviceEquipment:
    properties:
      sku:
        type: string
      description:
        type: string
      imei:
        type: string
      price:
        type: number
      msisdn:
        type: string
      transactionType:
        type: string
      actualInstallmentPaid:
        type: number
      percentTotal:
        type: number
      paymentTypeEligibilty:
        type: string
      eclmp:
        type: number
      neverReceived:
        type: boolean
      returned:
        type: boolean
      installmentEquipmentId:
        type: string
      eclb:
        type: number
      remainingUnbilledInstallments:
        type: number
      lastInstallmentAmount:
        type: number
      status:
        type: string
      deviceReturnPending:
        type: boolean
      promotionsDetails:
        type: array
        items:
          $ref: '#/definitions/SubscriberDevicePromotion'
  SubscriberDevicePlan:
    properties:
      applicationId:
        type: string
      transactionId:
        type: string
      installmentPlanId:
        type: string
      planStartDate:
        type: string
      planEndDate:
        type: string
      totalInstallmentPeriods:
        type: number
      balanceOwed:
        type: number
      eclmp:
        type: number
      firstInstallment:
        type: number
      planStatus:
        type: string
      planType:
        type: string
      remainingUnbilledInstallments:
        type: number
      state:
        type: string
      totalFinancedAmount:
        type: number
      lastInstallment:
        type: number
      deviceLevel:
        type: boolean
      financeType:
        type: string
      equipments:
        type: array
        items:
          $ref: '#/definitions/SubscriberDeviceEquipment'
  SubscriberDeviceDetails:
    required:
      - subscriberNumber
      - totalActivePromotions
      - totalPendingPromotions
      - totalEndedPromotions
      - totalRemovedPromotions
      - totalNotEligiblePromotions
      - plans
    properties:
      subscriberNumber:
        type: string
      totalActivePromotions:
        type: integer
      totalPendingPromotions:
        type: integer
      totalEndedPromotions:
        type: integer
      totalRemovedPromotions:
        type: integer
      totalNotEligiblePromotions:
        type: integer
      plans:
        type: array
        items:
          $ref: '#/definitions/SubscriberDevicePlan'
    type: object
  PromotionAlert:
    type: object
    properties:
      alertDescription:
        type: string
      subscriberNumber:
        type: string
      deviceName:
        type: string
      devicePlanId:
        type: string
      promotionCode:
        type: string
      promotionDescription:
        type: string
      promotionStatus:
        type: string
      promotionEndDate:
        type: string
      promotionRemainingInstallments:
        type: number
        format: integer
