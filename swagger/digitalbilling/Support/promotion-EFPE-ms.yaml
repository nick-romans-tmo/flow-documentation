swagger: '2.0'
info:
  description: This document lists version 1.0 for the promotion API.
  version: '1.0'
  title: promotion
host: localhost
basePath: /promotion/v1
paths:
  /promotions:
    get:
      summary: Find Promotion by eipId
      description: Returns a list of Promotions by eip ID
      operationId: find-promotion-by-id
      produces:
        - application/json
      parameters:
        - name: authorization
          in: header
          description: authorization header
          required: false
          type: string
        - name: activityId
          in: header
          description: activityId header
          required: false
          type: string
        - name: dealerCode
          in: header
          description: dealerCode header
          required: false
          type: string
        - name: applicationUserId
          in: header
          description: application user id
          required: false
          type: string
        - name: userId
          in: header
          description: user id
          required: false
          type: string
        - name: eipId
          in: query
          description: EIP Plan ID
          required: true
          type: string
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/PromotionsExceptionResponse'
        '400':
          description: Bad request error
        '404':
          description: Resource not found
        '500':
          description: >-
            The server encountered an unexpected condition which prevented it
            from fulfilling the request
    post:
      summary: Add a new request for a promotion addition and removal for the customer
      description: ''
      operationId: promotion
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: authorization
          in: header
          description: authorization header
          required: false
          type: string
        - name: activityId
          in: header
          description: activityId header
          required: false
          type: string
        - name: dealerCode
          in: header
          description: dealerCode header
          required: false
          type: string
        - name: applicationUserId
          in: header
          description: application user id
          required: false
          type: string
        - name: userId
          in: header
          description: user id
          required: false
          type: string
        - in: body
          name: body
          description: promotion object that needs to be added to the customer
          required: true
          schema:
            $ref: '#/definitions/PromotionExceptionRecord'
      responses:
        '201':
          description: successful created
        '400':
          description: Bad request error
        '404':
          description: Resource not found
        '500':
          description: >-
            The server encountered an unexpected condition which prevented it
            from fulfilling the request
  /promotions/available:
    get:
      description: >-
        This API will return a list of available promotions for a given SKU
        within a range of dates.
      operationId: avaliable-promotions-by-sku
      produces:
        - application/json
      parameters:
        - name: authorization
          in: header
          description: authorization header
          required: false
          type: string
        - name: activityId
          in: header
          description: activityId header
          required: false
          type: string
        - name: dealerCode
          in: header
          description: dealerCode header
          required: false
          type: string
        - name: applicationUserId
          in: header
          description: application user id
          required: false
          type: string
        - name: userId
          in: header
          description: user id
          required: false
          type: string
        - name: SKU
          in: query
          description: Device SKU
          type: string
          required: true
        - name: startDate
          in: query
          description: Promotions start date
          type: string
          required: false
        - name: endDate
          in: query
          description: Promotions end date
          type: string
          required: false
      responses:
        '200':
          description: The request was successful
          schema:
            $ref: '#/definitions/PromotionsResponse'
        '400':
          description: Bad request error
        '404':
          description: Record not found
        '500':
          description: >-
            The server encountered an unexpected condition which prevented it
            from fulfilling the request
        '501':
          description: Not Implemeted
  /promotions/history:
    get:
      operationId: promotions-by-fan-id
      produces:
        - application/json
      parameters:
        - name: authorization
          in: header
          description: authorization header
          required: false
          type: string
        - name: activityId
          in: header
          description: activityId header
          required: false
          type: string
        - name: dealerCode
          in: header
          description: dealerCode header
          required: false
          type: string
        - name: applicationUserId
          in: header
          description: application user id
          required: false
          type: string
        - name: userId
          in: header
          description: user id
          required: false
          type: string
        - name: fanID
          in: query
          description: customer id
          required: true
          type: string
        - name: status
          in: query
          description: promotion status
          required: false
          type: string
      responses:
        '200':
          description: The request was successful
          schema:
            $ref: '#/definitions/PromotionsHistoryResponse'
        '400':
          description: Bad request error
        '404':
          description: Record not found
        '500':
          description: >-
            The server encountered an unexpected condition which prevented it
            from fulfilling the request
definitions:
  PromotionExceptionRecord:
    type: object
    properties:
      fanId:
        type: string
        description: Account number - BAN number.
      promoCode:
        type: string
        example: TMUS07
      eipID:
        type: string
        example: TMUS07
      promoStatus:
        type: string
        example: pending
      promoAmount:
        type: number
        format: double
        example: 10
      activityDate:
        type: string
      action:
        type: string
        maxLength: 1
        minLength: 1
        description: The valid values are E for Adding and D for deleting.
      memoUserText:
        type: string
        maxLength: 256
      subscriberNo:
        type: string
        maxLength: 10
        minLength: 10
      seqNumber:
        type: number
  PromotionsExceptionResponse:
    required:
      - promotionsExceptionList
    properties:
      promotionsExceptionList:
        type: array
        items:
          $ref: '#/definitions/PromotionExceptionRecord'
  Promotion:
    properties:
      promoCode:
        type: string
      promoStartDate:
        type: string
      promoEndDate:
        type: string
      promoDescription:
        type: string
      promoDuration:
        type: number
      promoAmount:
        type: number
      effectiveDate:
        type: string
      expirationDate:
        type: string
      tradeInGroupId:
        type: string
      tradeInGracePeriod:
        type: number
      financeType:
        type: string
  PromotionsResponse:
    required:
      - promotionsList
    properties:
      promotionsList:
        type: array
        items:
          $ref: '#/definitions/Promotion'
  PromotionHistory:
    properties:
      promoCodeList:
        type: array
        items:
          type: string
      eipID:
        type: string
      promoAmount:
        type: number
      promoStatus:
        type: string
      subscriberNumber:
        type: string
      promoExpirationDate:
        type: string
      adjustmentReason:
        type: string
      errorReasonDesc:
        type: string
      careReasonDesc:
        type: string
      queueStatus:
        type: string
      queueSubStatus:
        type: string
      activityDate:
        type: string
  PromotionsHistoryResponse:
    required:
      - PromotionsHistoryList
    properties:
      PromotionsHistoryList:
        type: array
        items:
          $ref: '#/definitions/PromotionHistory'
    type: object
