{
   "swagger": "2.0",
   "info": {
     "version": "0.1.0",
     "title": "Address Validation API",
     "description": "Address Validation API that validates the addresses for correctness and qualifies them on their usage against billing, primary use or E911 address.",
     "contact": {
       "name": "Umesh Kadam",
       "url": "http://www.t-mobile.com",
       "email": "umesh.kadam1@t-mobile.com"
     },
     "x-createdBy": {
       "dateCreated": "Fri Nov 15 12:00:00 2019",
       "createdBy": "Umesh Kadam",
       "application": "TPAS",
       "status": "Draft",
       "classification": "Address Validation",
       "profile": "Core Business Capability Service"
     }
   },
   "tags": [
     {
       "name": "AddressValidation",
       "description": "Address Validation API"
     }
   ],
   "schemes": [
     "https"
   ],
   "host": "tpas.t-mobile.com",
    "basePath": "/tpas/v1",

   "x-servers": [
     {
       "url": "https://tpas-staging.t-mobile.com",
       "description": "Sandbox"
     },
     {
       "url": "https://tpas.t-mobile.com",
       "description": "Production"
     }
   ],
   "paths": {
     "/validate-address": {
       "post": {
         "x-api-pattern": "ExecuteFunction",
         "operationId": "addressValidation",
         "tags": [
           "AddressValidation"
         ],
         "summary": "This Api validates the addresses for correctness ",
         "description": "This API validates the addresses for correctness as well as qualifies their use against billing, ppu and e911",
         "consumes": [
           "application/json"
         ],
         "produces": [
           "application/json"
         ],
         "parameters": [
           {
                "name": "Content-Type",
                "in": "header",
                "description": "The MIME type of the content",
                "x-example": "application/json",
                "required": true,
                "type": "string",
                "minLength": 1,
                "maxLength": 256,
                "pattern": "^[ \\S]+$"
           },
           {
               "name": "Accept",
               "in": "header",
               "description": "Preferred content type, defaults to application/json",
               "x-example": "application/json",
               "required": false,
               "type": "string",
               "minLength": 1,
               "maxLength": 256,
               "pattern": "^[ \\S]+$"
           },
           {
                "name": "service-transaction-id",
                "in": "header",
                "description": "The unique alpha-numeric id that is generated by the consumer of the API and is maintained through the customer transaction lifecycle",
                "x-example": "2ah34d092A9723",
                "required": true,
                "type": "string",
                "minLength": 10,
                "maxLength": 20,
                "pattern": "^[a-zA-Z0-9]*$"
           },
           {
                "name": "channel-id",
                "in": "header",
                "description": "Captures the channel being used to invoke the API. Supported values are WEB|CARE|RETAIL|APP",
                "x-example": "WEB",
                "required": true,
                "type": "string",
                "enum": ["WEB","RETAIL","APP","CARE"]
           },
           {
                "name": "partner-id",
                "in": "header",
                "description": "Unique id alphanumeric value generated by TMO to identify the partner uniquely. Example: Par12345ABCEF",
                "x-example": "Par12345ABCEF",
                "required": true,
                "type": "string",
                "minLength": 10,
                "maxLength": 20,
                "pattern": "^[a-zA-Z0-9]*$"
           },
           {
                "name": "activity-id",
                "in": "header",
                "description": "Unique alphanumeric ID value generated  by the consumer of the API and it is unique for each API request. This is used to track each API request.",
                "x-example": "c34e7acd-384b-4c22-8b02-ba3963682508",
                "required": true,
                "type": "string",
                "minLength": 36,
                "maxLength": 36,
                "pattern": "([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}"
           },
           {
             "name": "Authorization",
             "in": "header",
             "description": "OAuth 2.0 access token with the authentication type set as Bearer.",
             "x-example": "Bearer eyJraWQiOiI0Nzc1M2UzZi0zOGFjLTQ1ODQtZDcxYy0zZDgxYTE1Nzk5NWMiLCJ0eXAiOiJKV",
             "required": true,
             "type": "string",
             "pattern": "^[\\S ]+$"
           },
           {
             "name": "x-authorization",
             "in": "header",
             "description": "Proof of Possession POP token, signed JWT object using client Private Key.",
             "required": true,
             "type": "string",
             "pattern": "^[\\S]*$",
             "x-example": "YUFSbFZ.HQ1p5dE5KMzV6RVAzOVFneXFUS0tRM1VONHM6Qmt.YN0c5RmQxTnNEaHRsWg=="
           },
           {
             "name": "x-auth-originator",
             "in": "header",
             "description": "OAuth 2.0 access token with the authentication type set as Bearer (id token).",
             "required": true,
             "type": "string",
             "pattern": "^[\\S]*$",
             "x-example": "aasFAFq3fwsdgsw4GTgw442.sdjbfsdjksg2o34t.dsfbhifh9h2wf92f"
           },
           {
               "name": "AddressValidationRequest",
               "in": "body",
               "description": "Address validation request body",
               "schema": {
                 "$ref": "#/definitions/AddressValidationRequest"
               }
           }
         ],
         "security": [
           {
             "oauth2": []
           }
         ],
         "responses": {
           "200": {
             "description": "Address validation response body",
             "schema": {
               "$ref": "#/definitions/AddressValidationResponse"
             },
             "headers": {
               "Content-Type": {
                 "type": "string",
                 "description": "content type",
                 "x-example": "application/json",
                 "pattern": "^[\\w ]+$",
                 "minLength": 0,
                 "maxLength": 256
               },
               "service-transaction-id": {
                   "description": "The unique alpha-numeric id that is generated by the consumer of the API and is maintained through the customer transaction lifecycle",
                   "x-example": "2ah34d092A9723",
                   "type": "string",
                   "minLength": 10,
                   "maxLength": 20,
                   "pattern": "^[a-zA-Z0-9]*$"
               },
               "Date": {
                 "description": "The Date general HTTP header contains the date and time at which the message was originated.",
                 "x-example": "Mon, 05 Mar 2018 16:38:08 GMT",
                 "type": "string",
                 "format": "date-time",
                 "pattern": "^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), [0-3]{1}[0-9]{1} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [1-3][0-9]{3} [0-2][0-9]:[0-6][0-9]:[0-6][0-9] GMT$"
               }
             },
             "examples": {
               "application/json": {
                 "responses": [
                   {
                     "error": {
                       "code": 2010,
                       "userMessage": "Address Not Found",
                       "systemMessage": "Address Not Found",
                       "detailLink": "http://www.tmus.com"
                     }
                   },
                   {
                     "addressId": "AD123",
                     "isValid": true,
                     "isBilling": true,
                     "isPersonalPlaceOfUseAddress": true,
                     "isE911": true
                   }
                 ]
               }
             }
           },
           "400": {
                "description": "Bad Request.",
                "schema": {
                    "$ref": "#/definitions/ErrorResponse"
                },
                "examples": {
                    "application/json": {
                        "code": "400",
                        "userMessage": "Bad Request.",
                        "systemMessage": "Bad Request.",
                        "detailLink": "http://www.tmus.com"
                    }
                },
                "headers": {}
           },
           "401": {
                "description": "Unauthorized.",
                "schema": {
                    "$ref": "#/definitions/ErrorResponse"
                },
                "examples": {
                    "application/json": {
                        "code": "401",
                        "userMessage": "Unauthorized.",
                        "systemMessage": "Unauthorized.",
                        "detailLink": "http://www.tmus.com"
                    }
                },
                "headers": {}
           },
           "404": {
                "description": "Resource not found.",
                "schema": {
                    "$ref": "#/definitions/ErrorResponse"
                },
                "examples": {
                    "application/json": {
                        "code": "404",
                        "userMessage": "Resource not found.",
                        "systemMessage": "Resource not found.",
                        "detailLink": "http://www.tmus.com"
                    }
                },
                "headers": {}
           },
           "405": {
                "description": "Method Not Allowed.",
                "schema": {
                    "$ref": "#/definitions/ErrorResponse"
                },
                "examples": {
                    "application/json": {
                        "code": "405",
                        "userMessage": "Method Not Allowed.",
                        "systemMessage": "Method Not Allowed.",
                        "detailLink": "http://www.tmus.com"
                    }
                },
                "headers": {
                    "Allow": {
                        "description": "list of supported methods for URI",
                        "x-example": "GET",
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    "service-transaction-id": {
                       "description": "The unique alpha-numeric id that is generated by the consumer of the API and is maintained through the customer transaction lifecycle",
                       "x-example": "2ah34d092A9723",
                       "type": "string",
                       "minLength": 10,
                       "maxLength": 20,
                       "pattern": "^[a-zA-Z0-9]*$"
                   }
                }
           },
           "409": {
                "description": "Invalid data.",
                "schema": {
                    "$ref": "#/definitions/ErrorResponse"
                },
                "examples": {
                    "application/json": {
                        "code": "409",
                        "userMessage": "Invalid data.",
                        "systemMessage": "Invalid data.",
                        "detailLink": "http://www.tmus.com"
                    }
                },
                "headers": {}
           },
           "415": {
                "description": "Unsupported Media Type.",
                "schema": {
                    "$ref": "#/definitions/ErrorResponse"
                },
                "examples": {
                    "application/json": {
                        "code": "415",
                        "userMessage": "Unsupported Media Type.",
                        "systemMessage": "Unsupported Media Type.",
                        "detailLink": "http://www.tmus.com"
                    }
                },
                "headers": {}
           },
           "500": {
                "description": "System Error.",
                "schema": {
                    "$ref": "#/definitions/ErrorResponse"
                },
                "examples": {
                    "application/json": {
                        "code": "500",
                        "userMessage": "System Error.",
                        "systemMessage": "System Error.",
                        "detailLink": "http://www.tmus.com"
                    }
                },
                "headers": {}
           },
           "503": {
                "description": "Service unavailable.",
                "schema": {
                    "$ref": "#/definitions/ErrorResponse"
                },
                "examples": {
                    "application/json": {
                        "code": "503",
                        "userMessage": "Service unavailable.",
                        "systemMessage": "Service unavailable.",
                        "detailLink": "http://www.tmus.com"
                    }
                },
                "headers": {}
           }

         }
       }
     }
   },
   "definitions": {
      "ErrorResponse": {
            "description": "Error Response Object returned for all error scenarios ",
            "type": "object",
            "required": [
                "code",
                "userMessage"
            ],
            "properties": {
                "code": {
                  "description": "A succinct, domain-specific, human-readable text string to identify the type of error for the given status code",
                  "example": "503",
                  "minLength": 3,
                  "maxLength": 5,
                  "type": "string",
                  "pattern": "^[0-9]*$"
                },
                "userMessage": {
                  "description": "A human-readable message describing the error.",
                  "example": "Failed to find product.",
                  "minLength": 10,
                  "maxLength": 1024,
                  "type": "string",
                  "pattern": "^[\\S ]+$"
                },
                "systemMessage": {
                  "description": "Text that provides a more detailed technical explanation of the error",
                  "example": "PRODUCT_NOT_FOUND",
                  "minLength": 10,
                  "maxLength": 1024,
                  "type": "string",
                  "pattern": "^[ \\S]+$"
                },
                "detailLink": {
                  "description": "link to custom information providing greater detail on error or errors",
                  "example": "http://aaa.bbb.ccc/",
                  "minLength": 10,
                  "maxLength": 1024,
                  "type": "string",
                  "pattern": "^[ \\S]+$"
                }
            }
      },
      "Address": {
             "type": "object",
             "required": [
               "address1",
               "city",
               "state",
               "zipCode"
              ],
             "description": "The complete address of the customer",
             "properties": {
               "address1": {
                   "type": "string",
                   "pattern": "^[\\w- ]+$",
                   "minLength": 0,
                   "maxLength": 256,
                   "description": "Valid number and street"
               },
               "address2": {
                   "type": "string",
                   "pattern": "^[\\w- ]+$",
                   "minLength": 0,
                   "maxLength": 256,
                   "description": "Apt or Suite or PO Box"
               },
               "city": {
                   "type": "string",
                   "pattern": "^[\\w- ]+$",
                   "minLength": 0,
                   "maxLength": 256,
                   "description": "City Name"
               },
               "state": {
                   "type": "string",
                   "pattern": "^[\\w]+$",
                   "minLength": 2,
                   "maxLength": 2,
                   "description": "State Code"
               },
               "zipCode": {
                   "type": "string",
                   "pattern": "^[\\d]+$",
                   "minLength": 5,
                   "maxLength": 5,
                   "description": "Zip Code"
               },
               "zip4": {
                   "type": "string",
                   "pattern": "^[\\d]+$",
                   "minLength": 4,
                   "maxLength": 4,
                   "description": "Zip plus Code 4 digits"
               }
             }
      },
      "AddressValidationRequest": {
         "type": "object",
         "required": [
          "addresses"
         ],
         "description": "Address validation API request",
         "properties": {
           "addresses": {
             "type": "array",
             "minItems": 1,
             "description": "Array of address to validate ",
             "items": {
               "$ref": "#/definitions/Address"
             }
           }
       },
       "example": {
         "addresses": [
           {
             "address1": "193001 175th st e",
             "address2": "apt d-1",
             "city": "BONNEY LAKES",
             "state": "WA",
             "zipCode": 98391,
             "zip4": 9999
           },
           {
             "address1": "15338 NE 9th Pl",
             "address2": "apt 206",
             "city": "Bellevue",
             "state": "WA",
             "zipCode": 98007,
             "zip4": 1010
           }
         ]
       }
     },
      "AddressValidationResponse": {
       "type": "object",
       "description": "Address validation API response",
       "properties": {
         "responses": {
           "type": "array",
		   "description": "Array of address validation response",
           "items": {
             "type": "object",
             "description": "Validation response for submitted address",
             "properties": {
               "addressId": {
                 "type": "string",
                 "description": "AddressId of address passed",
				 "pattern": "^[ \\S]+$",
				 "minLength": 5,
				 "maxLength": 256
               },
               "isValid": {
                 "type": "boolean",
                 "description": "Flag for valid address"
               },
               "isBilling": {
                 "type": "boolean",
                 "description": "Flag for valid billing address"
               },
               "isPersonalPlaceOfUseAddress": {
                 "type": "boolean",
                 "description": "Flag for primary place of use"
               },
               "isE911": {
                 "type": "boolean",
                 "description": "Flag for valid emergency response address"
               },
               "error": {
                 "$ref": "#/definitions/ErrorResponse"
               }
             }
           }
         }
       }
     }
   },
  "securityDefinitions": {
    "oauth2": {
      "type": "oauth2",
      "description": "oauth2 Authentication",
      "tokenUrl": "https://host:env/v1/oauth2/accesstoken?grant_type=client_credentials",
      "flow": "application",
      "scopes": {

      }
    }
  }
 }
