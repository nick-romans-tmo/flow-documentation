swagger: '2.0'
info:
  title: dimCommProfile Report
  version: 1.0.0
  description: Reporting Service to Publish DimCommProfile Events to SQS
  contact:
    name: Commissions Adapter Notifications
    email: CommissionsAdapterNotifications@t-mobile.com
  license:
    name: Apache2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
host: stg.cpp.scm.ccp.t-mobile.com
basePath: /b2b-experience/v1/dim-comm-profile
x-servers: null
securityDefinitions:
  basicAuth:
    description: security definition
    type: basic
schemes:
  - https
tags:
  - name: DimCommProfile Events
    description: 'Returns DimCommProfile Events'
paths:
  /{event-type}:
    post:
      tags:
        - DimCommProfile Events
      operationId: sendMessageUsingPOST
      description: post DimCommProfile  payload to stg-scm-dimcommprofile-icm-e
      summary: Post DimCommProfile  events to SQS
      x-api-pattern: ExecuteFunction
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: event-type
          in: path
          x-example: stg-scm-dimcommprofile
          enum:
            - flase
          description: Unique name of the event
          required: true
          type: string
        - name: event
          in: body
          x-example: false
          description: Event Complex Element
          required: true
          schema:
            $ref: '#/definitions/Event'
        - name: Authorization
          in: header
          description: JWT access token with the authentication type set as Bearer.
          x-example: application/json
          required: true
          type: string
          pattern: '^[\S]*$'
        - name: Content-Type
          in: header
          description: The MIME type of this content
          x-example: application/json
          required: true
          type: string
          pattern: '^[\S]*$'
      responses:
        '200':
          description: Message Published Successfully.
          x-example: true
          schema:
            $ref: '#/definitions/Event'
          examples:
            code: 200
            userMessage : ok
          headers:
            Content-Type:
              description: The MIME type of this content
              x-example: application/json
              type: string
              pattern: '^[\S]*$'
            Date:
              description: The date and time that the message was originated (in HTTP-date format as defined by RFC 7231 Date/Time Formats).
              x-example: 'Wed, 18-Apr-2018 11:34:57 GMT'
              type: string
              format: date-time
        '400':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '401':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '404':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '405':
          description: Method Not Allowed
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '405'
              userMessage: Method Not Allowed
              systemMessage: Method Not Allowed
          headers:
            Allow:
              description: list of supported methods for URI
              x-example: GET
              type: string
            service-transaction-id:
              description: Internal identifier for transaction tracking an individual transaction/API request within API platform. Don_t receive/don_t send to downstream
              x-example: '23409209723'
              type: string
        '500':
          description: The server encountered an unexpected condition which prevented it from fulfilling the request.
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '500'
              userMessage: The server encountered an unexpected condition which prevented it from fulfilling the request.
              systemMessage: The server encountered an unexpected condition which prevented it from fulfilling the request.
          headers: {}
        '503':
          description: Service unavailable
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '503'
              userMessage: Service unavailable
              systemMessage: Service unavailable
          headers: {}
        '409':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '415':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
      security:
        - basicAuth: []
definitions:
  Event:
    type: object
    description: Event with corpnode data
    properties:
      eventId:
        type: string
        required:
          - 'true'
        x-example: a777334c-d2ba-4f84-9203-051e2b2e9748
        description: >-
          Unique identifier for the event generated by the event producer. This
          element will be searchable in Queue
        enum:
          - false
      eventProducerId:
        type: string
        required:
          - 'true'
        x-example: SalesCommission
        description: Unique identifier for the producer of the event.
        enum:
          - false
      eventTime:
        type: string
        required:
          - 'true'
        format: date-time
        x-example: '2018-08-02T21:47:12.602Z'
        description: The date and time that the event occurred.
        enum:
          - false
      eventType:
        type: string
        required:
          - 'true'
        x-example: dev-scm-dealersallchannels
        description: Queue Name that you want to publish
        enum:
          - false
      eventCount:
        type: integer
        required:
          - 'true'
        x-example: 100
        description: No of Records published in the Event
        enum:
          - false
      eventVersion:
        type: string
        required:
          - 'true'
        x-example: '1.0'
        description: >-
          version of the event , will be used to distinguish different versions
          by a consumer
        enum:
          - false
      payload:
        $ref: '#/definitions/Payload'
  Payload:
    type: object
    description: >-
      Event specific information that can be modeled using APIv2 or inline
      elements.
    x-example: false
    properties:
      commissionEvent:
        type: array
        required:
          - 'true'
        description: list of reasoncode events
        pattern: '^[\S]*$'
        items:
          $ref: '#/definitions/DimCommProfile'
        example:
          application/json:
            - rptSequenceId: 11111111
            - reasonId: 123
            - reasonDesc: 'reason 1'
            - reasonStatus: DEACT
  DimCommProfile:
    type: object
    description: Event specific body information for DimCommProfile
    properties:
      rptSequenceId:
        type: number
        required:
          - 'true'
        maxLength: 40
        minLength: 10
        description: Unique identifier for a record
        x-example: 1010496
        enum:
          - false
      jobLogId:
        type: number
        required:
          - 'true'
        maxLength: 40
        minLength: 10
        description: jobLogId
        x-example: 123
        enum:
          - false
      profileId:
        type: number
        required:
          - 'true'
        maxLength: 40
        minLength: 10
        description: profileId
        x-example: 123
        enum:
          - false
      profileDesc:
        type: string
        required:
          - 'true'
        maxLength: 100
        description: profileDesc
        x-example: any String value
        enum:
          - false
      commissionable:
        type: string
        required:
          - 'true'
        maxLength: 1
        description: commissionable
        x-example: any String value
        enum:
          - false
      channelId:
        type: number
        required:
          - 'true'
        maxLength: 8
        description: channelId
        x-example: 123
      channelDesc:
        type: string
        required:
          - 'true'
        maxLength: 100
        description: channelDesc
        x-example: any String value
        enum:
          - false
      methodId:
        type: number
        required:
          - 'true'
        maxLength: 8
        description: methodId
        x-example: 123
        enum:
          - false
      effectiveStartDate:
        type: string
        required:
          - 'true'
        maxLength: 6
        format: date-time
        description: effectiveStartDate
        x-example: '2017-03-27T16:20:11.108Z'
        enum:
          - false
      effectiveEndDate:
        type: string
        required:
          - 'true'
        maxLength: 6
        format: date-time
        description: effectiveEndDate
        x-example: '2017-03-27T16:20:11.108Z'
        enum:
          - false
      compType:
        type: string
        required:
          - 'true'
        maxLength: 20
        description: compType
        x-example: any String value
        enum:
          - false
      amount:
        type: number
        required:
          - 'true'
        maxLength: 40
        minLength: 10
        description: jobLogId
        x-example: 123
        enum:
          - false
      contract:
        type: string
        required:
          - 'true'
        maxLength: 30
        description: contract
        x-example: any String value
        enum:
          - false
      methodDesc:
        type: string
        required:
          - 'true'
        maxLength: 100
        description: methodDesc
        x-example: any String value
        enum:
          - false
  Error:
    type: object
    description: 'As defined in http://api-standards.apps.px-npe01.cf.t-mobile.com/http/status-codes/error-response-format/'
    required:
      - code
      - userMessage
    properties:
      code:
        description: 'A succinct, domain-specific, human-readable text string to identify the type of error for the given status code'
        example: ProductNotFound
        type: string
        pattern: '^[\S ]+$'
      userMessage:
        description: A human-readable message describing the error.
        example: Failed to find product.
        type: string
        pattern: '^[\S ]+$'
      systemMessage:
        description: Text that provides a more detailed technical explanation of the error
        example: PRODUCT_NOT_FOUND
        type: string