swagger: '2.0'
info:
  title: subActivityReport
  version: 1.0.0
  description: Reporting Service to Publish SubActivity Events to stg-scm-subactivity-icm-e
  contact:
    name: Commissions Adapter Notifications
    email: CommissionsAdapterNotifications@t-mobile.com
  license:
    name: Apache2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
host: stg.cpp.scm.ccp.t-mobile.com
basePath: /b2b-experience/v1/sub-activity-report
x-servers: null
securityDefinitions:
  basicAuth:
    description: security definition
    type: basic
schemes:
  - https
tags:
  - name: Sub Activity Events
    description: 'Returns SubActivity Events'
paths:
  /{event-type}:
    post:
      tags:
        - Sub Activity Events
      operationId: sendMessageUsingPOST
      description: post subActivity payload
      summary: Post SubActivity events to SQS
      x-api-pattern: ExecuteFunction
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: event-type
          in: path
          x-example: stg-scm-subActivity
          enum:
            - flase
          description: Unique name of the event
          required: true
          type: string
        - name: event
          in: body
          x-example: false
          description: Event Complex Element
          required: true
          schema:
            $ref: '#/definitions/Event'
        - name: Authorization
          in: header
          description: JWT access token with the authentication type set as Bearer.
          x-example: application/json
          required: true
          type: string
          pattern: '^[\S]*$'
        - name: Content-Type
          in: header
          description: The MIME type of this content
          x-example: application/json
          required: true
          type: string
          pattern: '^[\S]*$'
      responses:
        '200':
          description: Message Published Successfully.
          x-example: true
          schema:
            $ref: '#/definitions/Event'
          examples:
            code: 200
            userMessage : ok
          headers:
            Content-Type:
              description: The MIME type of this content
              x-example: application/json
              type: string
              pattern: '^[\S]*$'
            Date:
              description: The date and time that the message was originated (in HTTP-date format as defined by RFC 7231 Date/Time Formats).
              x-example: 'Wed, 18-Apr-2018 11:34:57 GMT'
              type: string
              format: date-time
        '400':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '401':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '404':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '405':
          description: Method Not Allowed
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '405'
              userMessage: Method Not Allowed
              systemMessage: Method Not Allowed
          headers:
            Allow:
              description: list of supported methods for URI
              x-example: GET
              type: string
            service-transaction-id:
              description: Internal identifier for transaction tracking an individual transaction/API request within API platform. Don_t receive/don_t send to downstream
              x-example: '23409209723'
              type: string
        '500':
          description: The server encountered an unexpected condition which prevented it from fulfilling the request.
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '500'
              userMessage: The server encountered an unexpected condition which prevented it from fulfilling the request.
              systemMessage: The server encountered an unexpected condition which prevented it from fulfilling the request.
          headers: {}
        '503':
          description: Service unavailable
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '503'
              userMessage: Service unavailable
              systemMessage: Service unavailable
          headers: {}
        '409':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '415':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
      security:
        - basicAuth: []
definitions:
  Event:
    type: object
    description: Event with subActivity data
    properties:
      eventId:
        type: string
        required:
          - 'true'
        x-example: a777334c-d2ba-4f84-9203-051e2b2e9748
        description: >-
          Unique identifier for the event generated by the event producer. This
          element will be searchable in Queue
        enum:
          - false
      eventProducerId:
        type: string
        required:
          - 'true'
        x-example: SalesCommission
        description: Unique identifier for the producer of the event.
        enum:
          - false
      eventTime:
        type: string
        required:
          - 'true'
        format: date-time
        x-example: '2018-08-02T21:47:12.602Z'
        description: The date and time that the event occurred.
        enum:
          - false
      eventType:
        type: string
        required:
          - 'true'
        x-example: stg-scm-subActivity
        description: Queue Name that you want to publish
        enum:
          - false
      eventCount:
        type: integer
        required:
          - 'true'
        x-example: 100
        description: No of Records published in the Event
        enum:
          - false
      eventVersion:
        type: string
        required:
          - 'true'
        x-example: '1.0'
        description: >-
          version of the event , will be used to distinguish different versions
          by a consumer
        enum:
          - false
      payload:
        $ref: '#/definitions/Payload'
  Payload:
    type: object
    description: >-
      Event specific information that can be modeled using APIv2 or inline
      elements.
    x-example: false
    properties:
      commissionEvent:
        type: array
        required:
          - 'true'
        description: list of subActivity events
        pattern: '^[\S]*$'
        items:
          $ref: '#/definitions/SubActivity'
        example:
          application/json:
            - rptSequenceId: 11111111
            - reasonId: 123
            - reasonDesc: 'reason 1'
            - reasonStatus: DEACT
  SubActivity:
    type: object
    description: Event specific body information for subActivity
    properties:
      rptSequenceId:
        type: number
        required:
          - 'true'
        maxLength: 40
        minLength: 10
        description: Unique identifier for a record
        x-example: 1010496
        enum:
          - false
      accountSubType:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      accountType:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      actDate:
        type: string
        required:
          - 'false'
        maxLength: 6
        format: date-time
        description: Date
        x-example: '2019-05-01 15:24:26'
        enum:
          - false
      activityCode:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      addALineSoc:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      ban:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      batchKey:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      billingSubSystemId:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      brand:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      transactionIssueDate:
        type: string
        required:
          - 'false'
        maxLength: 6
        format: date-time
        description: Date
        x-example: '2019-05-01 15:24:26'
        enum:
          - false
      cobIndicator:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      contractRenewals:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      commitStartDt:
        type: string
        required:
          - 'false'
        maxLength: 6
        format: date-time
        description: Date
        x-example: '2019-05-01 15:24:26'
        enum:
          - false
      contractTerms:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      corpNodeNumber:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      dealerCode:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      hoti:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      creditClass:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      creditType:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      deactActivityCode:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      deactDate:
        type: string
        required:
          - 'false'
        maxLength: 6
        format: date-time
        description: Date
        x-example: '2019-05-01 15:24:26'
        enum:
          - false
      deactReasonCode:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      depositAmount:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      ebtvVoiceIndicator:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      effectiveCalcDate:
        type: string
        required:
          - 'false'
        maxLength: 6
        format: date-time
        description: Date
        x-example: '2019-05-01 15:24:26'
        enum:
          - false
      eipEquipmentId:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      eipType:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      eventSourceSystem:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      eventType:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      firstName:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      fullyLoaded:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      imei:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      isAddALine:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      isValid:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      jobLogId:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      lastProductDeactDate:
        type: string
        required:
          - 'false'
        maxLength: 6
        format: date-time
        description: Date
        x-example: '2019-05-01 15:24:26'
        enum:
          - false
      lastName:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      lastSuspendDate:
        type: string
        required:
          - 'false'
        maxLength: 6
        format: date-time
        description: Date
        x-example: '2019-05-01 15:24:26'
        enum:
          - false
      lineType:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      marketCode:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      measureGroupId:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      npaNxx:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      otherNetServiceProvider:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      poolingMrc:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      portIndicator:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      profileId:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      reactDate:
        type: string
        required:
          - 'false'
        maxLength: 6
        format: date-time
        description: Date
        x-example: '2019-05-01 15:24:26'
        enum:
          - false
      recAccess:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      discountMrc:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      sameMonth:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      serviceNumber:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      serviceUniversalId:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      shipToMasterCode:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      sim:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      planCode:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      sourceHoti:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      firstUseDate:
        type: string
        required:
          - 'false'
        maxLength: 6
        format: date-time
        description: Date
        x-example: '2019-05-01 15:24:26'
        enum:
          - false
      srlznMsisdn:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      shippedDate:
        type: string
        required:
          - 'false'
        maxLength: 6
        format: date-time
        description: Date
        x-example: '2019-05-01 15:24:26'
        enum:
          - false
      shipToMaster:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      statusRsnCode:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      storeId:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      begServDate:
        type: string
        required:
          - 'false'
        maxLength: 6
        format: date-time
        description: Date
        x-example: '2019-05-01 15:24:26'
        enum:
          - false
      subscriberActivityKey:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      sourceDealerCode:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      subscriberName:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      susActivityCode:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      susDiscReason:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      tac:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      tenure:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      totalMrc:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
      transactionDt:
        type: string
        required:
          - 'false'
        maxLength: 6
        format: date-time
        description: Date
        x-example: '2019-05-01 15:24:26'
        enum:
          - false
      creditingSocCode:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      imsi:
        type: string
        required:
          - 'false'
        maxLength: 250
        description: string
        x-example: abc
        enum:
          - false
      quantity:
        type: number
        required:
          - 'false'
        maxLength: 40
        minLength: 10
        description: number
        x-example: 1010496
        enum:
          - false
  Error:
    type: object
    description: 'As defined in http://api-standards.apps.px-npe01.cf.t-mobile.com/http/status-codes/error-response-format/'
    required:
      - code
      - userMessage
    properties:
      code:
        description: 'A succinct, domain-specific, human-readable text string to identify the type of error for the given status code'
        example: ProductNotFound
        type: string
        pattern: '^[\S ]+$'
      userMessage:
        description: A human-readable message describing the error.
        example: Failed to find product.
        type: string
        pattern: '^[\S ]+$'
      systemMessage:
        description: Text that provides a more detailed technical explanation of the error
        example: PRODUCT_NOT_FOUND
        type: string