swagger: '2.0'
info:
  title: posTrans Report
  version: 1.0.0
  description: Reporting Service to Publish postrans Events to SQS
  contact:
    name: Commissions Adapter Notifications
    email: CommissionsAdapterNotifications@t-mobile.com
  license:
    name: Apache2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
host: stg.cpp.scm.ccp.t-mobile.com
basePath: /b2b-experience/v1/pos-trans-report
x-servers: null
securityDefinitions:
  basicAuth:
    description: security definition
    type: basic
schemes:
  - https
tags:
  - name: Postrans Events
    description: 'postrans report'
paths:
  /{event-type}:
    post:
      tags:
        - Postrans Events
      operationId: sendMessageUsingPOST
      description: post postrans payload to stg-scm-postrans-icm-e
      summary: Post postrans events to SQS
      x-api-pattern: ExecuteFunction
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: event-type
          in: path
          x-example: stg-scm-postrans
          enum:
            - flase
          description: Unique name of the event
          required: true
          type: string
        - name: event
          in: body
          x-example: false
          description: Event Complex Element
          required: true
          schema:
            $ref: '#/definitions/Event'
        - name: Authorization
          in: header
          description: JWT access token with the authentication type set as Bearer.
          x-example: application/json
          required: true
          type: string
          pattern: '^[\S]*$'
        - name: Content-Type
          in: header
          description: The MIME type of this content
          x-example: application/json
          required: true
          type: string
          pattern: '^[\S]*$'
      responses:
        '200':
          description: Message Published Successfully.
          x-example: true
          schema:
            $ref: '#/definitions/Event'
          examples:
            code: 200
            userMessage : ok
          headers:
            Content-Type:
              description: The MIME type of this content
              x-example: application/json
              type: string
              pattern: '^[\S]*$'
            Date:
              description: The date and time that the message was originated (in HTTP-date format as defined by RFC 7231 Date/Time Formats).
              x-example: 'Wed, 18-Apr-2018 11:34:57 GMT'
              type: string
              format: date-time
        '400':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '401':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '404':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '405':
          description: Method Not Allowed
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '405'
              userMessage: Method Not Allowed
              systemMessage: Method Not Allowed
          headers:
            Allow:
              description: list of supported methods for URI
              x-example: GET
              type: string
            service-transaction-id:
              description: Internal identifier for transaction tracking an individual transaction/API request within API platform. Don_t receive/don_t send to downstream
              x-example: '23409209723'
              type: string
        '500':
          description: The server encountered an unexpected condition which prevented it from fulfilling the request.
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '500'
              userMessage: The server encountered an unexpected condition which prevented it from fulfilling the request.
              systemMessage: The server encountered an unexpected condition which prevented it from fulfilling the request.
          headers: {}
        '503':
          description: Service unavailable
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '503'
              userMessage: Service unavailable
              systemMessage: Service unavailable
          headers: {}
        '409':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '415':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
      security:
        - basicAuth: []
definitions:
  Event:
    type: object
    description: Event with postrans data
    properties:
      eventId:
        type: string
        required:
          - 'true'
        x-example: a777334c-d2ba-4f84-9203-051e2b2e9748
        description: >-
          Unique identifier for the event generated by the event producer. This
          element will be searchable in Queue
        enum:
          - false
      eventProducerId:
        type: string
        required:
          - 'true'
        x-example: SalesCommission
        description: Unique identifier for the producer of the event.
        enum:
          - false
      eventTime:
        type: string
        required:
          - 'true'
        format: date-time
        x-example: '2018-08-02T21:47:12.602Z'
        description: The date and time that the event occurred.
        enum:
          - false
      eventType:
        type: string
        required:
          - 'true'
        x-example: stg-scm-postrans
        description: Queue Name that you want to publish
        enum:
          - false
      eventCount:
        type: integer
        required:
          - 'true'
        x-example: 100
        description: No of Records published in the Event
        enum:
          - false
      eventVersion:
        type: string
        required:
          - 'true'
        x-example: '1.0'
        description: >-
          version of the event , will be used to distinguish different versions
          by a consumer
        enum:
          - false
      payload:
        $ref: '#/definitions/Payload'
  Payload:
    type: object
    description: >-
      Event specific information that can be modeled using APIv2 or inline
      elements.
    x-example: false
    properties:
      commissionEvent:
        type: array
        required:
          - 'true'
        description: list of postrans events
        pattern: '^[\S]*$'
        items:
          $ref: '#/definitions/PosTrans'
        example:
          application/json:
            - rptSequenceId: 11111111
            - reasonId: 123
            - reasonDesc: 'reason 1'
            - reasonStatus: DEACT
  PosTrans:
    type: object
    description: Event specific body information for postrans
    properties:
      rptSequenceId:
        type: number
        required:
          - 'true'
        maxLength: 40
        description: postrans Rpt Sequence Id
        x-example: 'null'
        enum:
          - false
      accountSubtype:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: postrans ACCOUNT SUBTYPE
        x-example: 313313
        enum:
          - false
      accountType:
        type: string
        required:
          - 'false'
        maxLength: 1
        description: postrans ACCOUNT TYPE
        x-example: 313313
        enum:
          - false
      activationCode:
        type: number
        required:
          - 'false'
        maxLength: 40
        description: postrans ACTIVATION CODE
        x-example: 'null'
        enum:
          - false
      activityType:
        type: string
        required:
          - 'false'
        maxLength: 255
        description: postrans Activity TYPE
        x-example: 313313
        enum:
          - false
      ban:
        type: string
        required:
          - 'false'
        maxLength: 9
        description: postrans Ban
        x-example: 313313
        enum:
          - false
      jobLogId:
        type: number
        required:
          - 'false'
        maxLength: 40
        description: postrans Job Id
        x-example: 'null'
        enum:
          - false
      begServDate:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description: Beg Serv Date
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
      billingSubSystemId:
        type: number
        required:
          - 'false'
        maxLength: 40
        description: postrans Billing SubSystemId
        x-example: 'null'
        enum:
          - false
      customerName:
        type: string
        required:
          - 'false'
        maxLength: 150
        description: postrans cuastomer Name
        x-example: P123
        enum:
          - false
      customerPaidAmt:
        type: number
        required:
          - 'false'
        maxLength: 20
        description: postrans customer Paid Amt
        x-example: 'null'
        enum:
          - false
      eip1stPaymentAmt:
        type: number
        required:
          - 'false'
        maxLength: 20
        description: postrans eip1st Payment Amt
        x-example: 313313
        enum:
          - false
      eipIndicator:
        type: number
        required:
          - 'false'
        maxLength: 20
        description: postrans Eip Indicator
        x-example: 313313
        enum:
          - false
      eipplanId:
        type: string
        required:
          - 'false'
        maxLength: 20
        description: postrans Eipp Lan Id
        x-example: 313313
        enum:
          - false
      eventSourceSystem:
        type: string
        required:
          - 'false'
        maxLength: 50
        description: postrans Event Source System
        x-example: 313313
        enum:
          - false
      imei:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: postrans Imei
        x-example: 313313
        enum:
          - false
      invoiceNumber:
        type: string
        required:
          - 'false'
        maxLength: 65
        description: postrans Sim
        x-example: 313313
        enum:
          - false
      istmo:
        type: string
        required:
          - 'false'
        maxLength: 1
        description: postrans Istmo
        x-example: 313313
        enum:
          - false
      lineType:
        type: string
        required:
          - 'false'
        maxLength: 1
        description: postrans Line Type
        x-example: 313313
        enum:
          - false
      marketCode:
        type: string
        required:
          - 'false'
        maxLength: 3
        description: postrans Market Code
        x-example: 313313
        enum:
          - false
      msisdn:
        type: string
        required:
          - 'false'
        maxLength: 10
        description: postrans Msisdn
        x-example: 313313
        enum:
          - false
      originalTransactionId:
        type: string
        required:
          - 'false'
        maxLength: 50
        description: postrans original Transaction Id
        x-example: 313313
        enum:
          - false
      posMsrp:
        type: number
        required:
          - 'false'
        maxLength: 30
        description: postrans Pos Msrp
        x-example: 313313
        enum:
          - false
      posType:
        type: string
        required:
          - 'false'
        maxLength: 255
        description: postrans Pos Type
        x-example: 313313
        enum:
          - false
      priceOverrideCode:
        type: number
        required:
          - 'false'
        maxLength: 50
        description: postrans price Override Code
        x-example: 313313
        enum:
          - false
      productCategory:
        type: string
        required:
          - 'false'
        maxLength: 9
        description: postrans Product Category
        x-example: 313313
        enum:
          - false
      productDescription:
        type: string
        required:
          - 'false'
        maxLength: 40
        description: postrans  productDescription
        x-example: 313313
        enum:
          - false
      programType:
        type: string
        required:
          - 'false'
        maxLength: 50
        description: postrans Program Type
        x-example: 313313
        enum:
          - false
      proxyId:
        type: string
        required:
          - 'false'
        maxLength: 50
        description: Proxy Id
        x-example: 455678
        enum:
          - false
      pseq:
        type: number
        required:
          - 'false'
        maxLength: 40
        description: Pseq
        x-example: 1234
        enum:
          - false
      qty:
        type: number
        required:
          - 'false'
        maxLength: 40
        description: Qty
        x-example: 12
        enum:
          - false
      returnCode:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: postrans Return Code
        x-example: 313313
        enum:
          - false
      salesPersonCode:
        type: string
        required:
          - 'false'
        maxLength: 7
        description: postrans Sales Person Code
        x-example: 313313
        enum:
          - false
      salesTransactionDt:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description: Sales Transaction Dt
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
      sameMonth:
        type: string
        required:
          - 'false'
        maxLength: 3
        description: postrans Same Month
        x-example: 313313
        enum:
          - false
      sapCost:
        type: number
        required:
          - 'false'
        maxLength: 20
        description: postrans Sap Cost
        x-example: 3133
        enum:
          - false
      sapFullPrice:
        type: number
        required:
          - 'false'
        maxLength: 25
        description: postrans Sap Full Price
        x-example: 3133
        enum:
          - false
      sapMsrp:
        type: number
        required:
          - 'false'
        maxLength: 50
        description: postrans Sap Msrp
        x-example: 3133
        enum:
          - false
      sapOneYearPrice:
        type: number
        required:
          - 'false'
        maxLength: 20
        description: postrans Sap One Year Price
        x-example: 313313
        enum:
          - false
      sapTransTypeCode:
        type: string
        required:
          - 'false'
        maxLength: 10
        description: postrans Sap Trans Type Code
        x-example: 313313
        enum:
          - false
      sapTwoYearPrice:
        type: number
        required:
          - 'false'
        maxLength: 40
        description: postrans sapTwoYearPrice
        x-example: 123
        enum:
          - false
      sellingPrice:
        type: number
        required:
          - 'false'
        maxLength: 30
        description: postrans Selling Price
        x-example: 313313
        enum:
          - false
      sequenceKey:
        type: number
        required:
          - 'false'
        maxLength: 40
        description: postrans sequence Key
        x-example: 123
        enum:
          - false
      serviceUniversalId:
        type: number
        required:
          - 'false'
        maxLength: 40
        description: postrans Service Universal Id
        x-example: 313313
        enum:
          - false
      sku:
        type: string
        required:
          - 'false'
        maxLength: 20
        description: postrans Sku
        x-example: 123
        enum:
          - false
      sourceSalesPersonCode:
        type: string
        required:
          - 'false'
        maxLength: 7
        description: Source Sales Person Code
        x-example: 'null'
        enum:
          - false
      storeId:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: postrans Store Id
        x-example: ab123
        enum:
          - false
      transactionId:
        type: string
        required:
          - 'false'
        maxLength: 50
        description: postrans TransactionId
        x-example: 123
        enum:
          - false
      transactionDt:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description: Transaction Dt
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
  Error:
    type: object
    description: 'As defined in http://api-standards.apps.px-npe01.cf.t-mobile.com/http/status-codes/error-response-format/'
    required:
      - code
      - userMessage
    properties:
      code:
        description: 'A succinct, domain-specific, human-readable text string to identify the type of error for the given status code'
        example: ProductNotFound
        type: string
        pattern: '^[\S ]+$'
      userMessage:
        description: A human-readable message describing the error.
        example: Failed to find product.
        type: string
        pattern: '^[\S ]+$'
      systemMessage:
        description: Text that provides a more detailed technical explanation of the error
        example: PRODUCT_NOT_FOUND
        type: string