swagger: '2.0'
info:
  title: eCommSplit Report
  version: 1.0.0
  description: Reporting Service to Publish EcommSplit Events to SQS
  contact:
    name: Commissions Adapter Notifications
    email: CommissionsAdapterNotifications@t-mobile.com
  license:
    name: Apache2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
host: stg.cpp.scm.ccp.t-mobile.com
basePath: /b2b-experience/v1/e-comm-Split-report
x-servers: null
securityDefinitions:
  basicAuth:
    description: security definition
    type: basic
schemes:
  - https
tags:
  - name: EcommSplit Events
    description: 'EcommSplit report'
paths:
  /{event-type}:
    post:
      tags:
        - EcommSplit Events
      operationId: sendMessageUsingPOST
      description: post EcommSplit payload to stg-scm-ecommsplit-icm-e
      summary: Post EcommSplit events to SQS
      x-api-pattern: ExecuteFunction
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: event-type
          in: path
          x-example: stg-scm-EcommSplit
          enum:
            - flase
          description: Unique name of the event
          required: true
          type: string
        - name: event
          in: body
          x-example: false
          description: Event Complex Element
          required: true
          schema:
            $ref: '#/definitions/Event'
        - name: Authorization
          in: header
          description: JWT access token with the authentication type set as Bearer.
          x-example: application/json
          required: true
          type: string
          pattern: '^[\S]*$'
        - name: Content-Type
          in: header
          description: The MIME type of this content
          x-example: application/json
          required: true
          type: string
          pattern: '^[\S]*$'
      responses:
        '200':
          description: Message Published Successfully.
          x-example: true
          schema:
            $ref: '#/definitions/Event'
          examples:
            code: 200
            userMessage : ok
          headers:
            Content-Type:
              description: The MIME type of this content
              x-example: application/json
              type: string
              pattern: '^[\S]*$'
            Date:
              description: The date and time that the message was originated (in HTTP-date format as defined by RFC 7231 Date/Time Formats).
              x-example: 'Wed, 18-Apr-2018 11:34:57 GMT'
              type: string
              format: date-time
        '400':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '401':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '404':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '405':
          description: Method Not Allowed
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '405'
              userMessage: Method Not Allowed
              systemMessage: Method Not Allowed
          headers:
            Allow:
              description: list of supported methods for URI
              x-example: GET
              type: string
            service-transaction-id:
              description: Internal identifier for transaction tracking an individual transaction/API request within API platform. Don_t receive/don_t send to downstream
              x-example: '23409209723'
              type: string
        '500':
          description: The server encountered an unexpected condition which prevented it from fulfilling the request.
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '500'
              userMessage: The server encountered an unexpected condition which prevented it from fulfilling the request.
              systemMessage: The server encountered an unexpected condition which prevented it from fulfilling the request.
          headers: {}
        '503':
          description: Service unavailable
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '503'
              userMessage: Service unavailable
              systemMessage: Service unavailable
          headers: {}
        '409':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '415':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
      security:
        - basicAuth: []
definitions:
  Event:
    type: object
    description: Event with EcommSplit data
    properties:
      eventId:
        type: string
        required:
          - 'true'
        x-example: a777334c-d2ba-4f84-9203-051e2b2e9748
        description: >-
          Unique identifier for the event generated by the event producer. This
          element will be searchable in Queue
        enum:
          - false
      eventProducerId:
        type: string
        required:
          - 'true'
        x-example: SalesCommission
        description: Unique identifier for the producer of the event.
        enum:
          - false
      eventTime:
        type: string
        required:
          - 'true'
        format: date-time
        x-example: '2018-08-02T21:47:12.602Z'
        description: The date and time that the event occurred.
        enum:
          - false
      eventType:
        type: string
        required:
          - 'true'
        x-example: stg-scm-EcommSplit
        description: Queue Name that you want to publish
        enum:
          - false
      eventCount:
        type: integer
        required:
          - 'true'
        x-example: 100
        description: No of Records published in the Event
        enum:
          - false
      eventVersion:
        type: string
        required:
          - 'true'
        x-example: '1.0'
        description: >-
          version of the event , will be used to distinguish different versions
          by a consumer
        enum:
          - false
      payload:
        $ref: '#/definitions/Payload'
  Payload:
    type: object
    description: >-
      Event specific information that can be modeled using APIv2 or inline
      elements.
    x-example: false
    properties:
      commissionEvent:
        type: array
        required:
          - 'true'
        description: list of EcommSplit events
        pattern: '^[\S]*$'
        items:
          $ref: '#/definitions/ECommSplit'
        example:
          application/json:
            - rptSequenceId: 11111111
            - reasonId: 123
            - reasonDesc: 'reason 1'
            - reasonStatus: DEACT
  ECommSplit:
    type: object
    description: Event specific body information for ecommSplit
    properties:
      rptSequenceId:
        type: number
        required:
          - 'true'
        maxLength: 40
        description: ecommSplit Rpt Sequence Id
        x-example: 1234
        enum:
          - false
      jobLogId:
        type: number
        required:
          - 'true'
        maxLength: 40
        description: ecommSplit jobLogId
        x-example: 1234
        enum:
          - false
      orderId:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: ecommSplit orderId
        x-example: 26
        enum:
          - false
      orderLineId:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: ecommSplit orderLineId
        x-example: 432
        enum:
          - false
      accountNumber:
        type: string
        required:
          - 'false'
        maxLength: 30
        description: ecommSplit account Number
        x-example: abcd
        enum:
          - false
      masterAccountId:
        type: string
        required:
          - 'false'
        maxLength: 30
        description: ecommSplit masterAccountId
        x-example: 432
        enum:
          - false
      masterDealerCode:
        type: string
        required:
          - 'false'
        maxLength: 7
        description:  event masterDealerCode
        x-example: 26
        enum:
          - false
      masterName:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: ecommSplit masterName
        x-example: abcd
        enum:
          - false
      subDealerAccountId:
        type: string
        required:
          - 'false'
        maxLength: 30
        description: ecommSplit subDealer AccountId
        x-example: 432
        enum:
          - false
      dealerCode:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: ecommSplit dealerCode
        x-example: 432
        enum:
          - false
      msisdn:
        type: string
        required:
          - 'false'
        maxLength: 20
        description:  msisdn
        x-example: 26
        enum:
          - false
      transactionDt:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description: Transaction Date
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
      commissionAmt:
        type: number
        required:
          - 'false'
        maxLength: 40
        description: ecommSplit commissionAmt
        x-example: 432
        enum:
          - false
      masterCommissionAmt:
        type: number
        required:
          - 'false'
        maxLength: 32
        description: ecommSplit masterCommissionAmt
        x-example: abcd
        enum:
          - false
      totalCommissionAmt:
        type: number
        required:
          - 'false'
        maxLength: 100
        description: ecommSplit totalCommissionAmt
        x-example: abcd
        enum:
          - false
      sim:
        type: string
        required:
          - 'false'
        maxLength: 20
        description: ecommSplit sim
        x-example: 1234
        enum:
          - false
      imei:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: ecommSplit billing PaymentType
        x-example: 432
        enum:
          - false
      brand:
        type: string
        required:
          - 'false'
        maxLength: 20
        description: ecommSplit brand
        x-example: abcd
        enum:
          - false
      crSummary:
        type: string
        required:
          - 'false'
        maxLength: 16
        description: ecommSplit crSummary
        x-example: abcd
        enum:
          - false
      eirImei:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: ecommSplit eirImei
        x-example: abcd
        enum:
          - false
      lineOfServiceId:
        type: string
        required:
          - 'false'
        maxLength: 30
        description: ecommSplit lineOfServiceId
        x-example: abcd
        enum:
          - false
      transactionDate:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description: Transaction Date
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
      eirTimeStamp:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description: TeirTimeStamp
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
      actTimeStamp:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description: actTimeStamp
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
  Error:
    type: object
    description: 'As defined in http://api-standards.apps.px-npe01.cf.t-mobile.com/http/status-codes/error-response-format/'
    required:
      - code
      - userMessage
    properties:
      code:
        description: 'A succinct, domain-specific, human-readable text string to identify the type of error for the given status code'
        example: ProductNotFound
        type: string
        pattern: '^[\S ]+$'
      userMessage:
        description: A human-readable message describing the error.
        example: Failed to find product.
        type: string
        pattern: '^[\S ]+$'
      systemMessage:
        description: Text that provides a more detailed technical explanation of the error
        example: PRODUCT_NOT_FOUND
        type: string
