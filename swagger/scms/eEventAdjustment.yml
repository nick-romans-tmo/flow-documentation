swagger: '2.0'
info:
  title: eEventAdjustment Report
  version: 1.0.0
  description: Reporting Service to Publish eEventAdjustment Events to SQS
  contact:
    name: Commissions Adapter Notifications
    email: CommissionsAdapterNotifications@t-mobile.com
  license:
    name: Apache2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
host: stg.cpp.scm.ccp.t-mobile.com
basePath: /b2b-experience/v1/bus-rule-e-event-Adjustment-report
x-servers: null
securityDefinitions:
  basicAuth:
    description: security definition
    type: basic
schemes:
  - https
tags:
  - name: EEventAdjustment Events
    description: 'eEventAdjustment report'
paths:
  /{event-type}:
    post:
      tags:
        - EEventAdjustment Events
      operationId: sendMessageUsingPOST
      description: post eEventAdjustment payload to stg-scm-eeventadjustment-icm-e
      summary: Post eEventAdjustment events to SQS
      x-api-pattern: ExecuteFunction
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: event-type
          in: path
          x-example: stg-scm-eEventAdjustment
          enum:
            - flase
          description: Unique name of the event
          required: true
          type: string
        - name: event
          in: body
          x-example: false
          description: Event Complex Element
          required: true
          schema:
            $ref: '#/definitions/Event'
        - name: Authorization
          in: header
          description: JWT access token with the authentication type set as Bearer.
          x-example: application/json
          required: true
          type: string
          pattern: '^[\S]*$'
        - name: Content-Type
          in: header
          description: The MIME type of this content
          x-example: application/json
          required: true
          type: string
          pattern: '^[\S]*$'
      responses:
        '200':
          description: Message Published Successfully.
          x-example: true
          schema:
            $ref: '#/definitions/Event'
          examples:
            code: 200
            userMessage : ok
          headers:
            Content-Type:
              description: The MIME type of this content
              x-example: application/json
              type: string
              pattern: '^[\S]*$'
            Date:
              description: The date and time that the message was originated (in HTTP-date format as defined by RFC 7231 Date/Time Formats).
              x-example: 'Wed, 18-Apr-2018 11:34:57 GMT'
              type: string
              format: date-time
        '400':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '401':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '404':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '405':
          description: Method Not Allowed
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '405'
              userMessage: Method Not Allowed
              systemMessage: Method Not Allowed
          headers:
            Allow:
              description: list of supported methods for URI
              x-example: GET
              type: string
            service-transaction-id:
              description: Internal identifier for transaction tracking an individual transaction/API request within API platform. Don_t receive/don_t send to downstream
              x-example: '23409209723'
              type: string
        '500':
          description: The server encountered an unexpected condition which prevented it from fulfilling the request.
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '500'
              userMessage: The server encountered an unexpected condition which prevented it from fulfilling the request.
              systemMessage: The server encountered an unexpected condition which prevented it from fulfilling the request.
          headers: {}
        '503':
          description: Service unavailable
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '503'
              userMessage: Service unavailable
              systemMessage: Service unavailable
          headers: {}
        '409':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '415':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
      security:
        - basicAuth: []
definitions:
  Event:
    type: object
    description: Event with eEventAdjustment data
    properties:
      eventId:
        type: string
        required:
          - 'true'
        x-example: a777334c-d2ba-4f84-9203-051e2b2e9748
        description: >-
          Unique identifier for the event generated by the event producer. This
          element will be searchable in Queue
        enum:
          - false
      eventProducerId:
        type: string
        required:
          - 'true'
        x-example: SalesCommission
        description: Unique identifier for the producer of the event.
        enum:
          - false
      eventTime:
        type: string
        required:
          - 'true'
        format: date-time
        x-example: '2018-08-02T21:47:12.602Z'
        description: The date and time that the event occurred.
        enum:
          - false
      eventType:
        type: string
        required:
          - 'true'
        x-example: stg-scm-eEventAdjustment
        description: Queue Name that you want to publish
        enum:
          - false
      eventCount:
        type: integer
        required:
          - 'true'
        x-example: 100
        description: No of Records published in the Event
        enum:
          - false
      eventVersion:
        type: string
        required:
          - 'true'
        x-example: '1.0'
        description: >-
          version of the event , will be used to distinguish different versions
          by a consumer
        enum:
          - false
      payload:
        $ref: '#/definitions/Payload'
  Payload:
    type: object
    description: >-
      Event specific information that can be modeled using APIv2 or inline
      elements.
    x-example: false
    properties:
      commissionEvent:
        type: array
        required:
          - 'true'
        description: list of eEventAdjustment events
        pattern: '^[\S]*$'
        items:
          $ref: '#/definitions/EEventAdjustment'
        example:
          application/json:
            - rptSequenceId: 11111111
            - reasonId: 123
            - reasonDesc: 'reason 1'
            - reasonStatus: DEACT
  EEventAdjustment:
    type: object
    description: Event specific body information for eEventAdjustment
    properties:
      rptSequenceId:
        type: number
        required:
          - 'true'
        maxLength: 40
        description: eEventAdjustment Rpt Sequence Id
        x-example: 1234
        enum:
          - false
      jobLogId:
        type: number
        required:
          - 'true'
        maxLength: 40
        description: eEventAdjustment jobLogId
        x-example: 1234
        enum:
          - false
      eventId:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: eEventAdjustment eventId
        x-example: 26
        enum:
          - false
      subId:
        type: string
        required:
          - 'true'
        maxLength: 30
        description: eEventAdjustment subId
        x-example: 432
        enum:
          - false
      subMsisdn:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: eEventAdjustment subMsisdn
        x-example: abcd
        enum:
          - false
      eventType:
        type: string
        required:
          - 'true'
        maxLength: 50
        description: eEventAdjustment eventType
        x-example: 432
        enum:
          - false
      inAmount:
        type: string
        format: float
        required:
          - 'false'
        maxLength: 50
        description:  eEventAdjustment inAmount
        x-example: 26
        enum:
          - false
      eventDetail:
        type: string
        format: char
        required:
          - 'false'
        maxLength: 10
        description: eEventAdjustment masterName
        x-example: abcd
        enum:
          - false
      sim:
        type: string
        required:
          - 'false'
        maxLength: 50
        description: eEventAdjustment sim
        x-example: 1234
        enum:
          - false
      imei:
        type: string
        required:
          - 'false'
        maxLength: 200
        description: eEventAdjustment billing PaymentType
        x-example: 432
        enum:
          - false
      imsi:
        type: number
        required:
          - 'false'
        maxLength: 40
        description: eEventAdjustment imsi AccountId
        x-example: 432
        enum:
          - false
      featureCode:
        type: string
        required:
          - 'false'
        maxLength: 40
        description: eEventAdjustment featureCode
        x-example: 432
        enum:
          - false
      ratePlanId:
        type: string
        required:
          - 'false'
        maxLength: 40
        description:  ratePlanId
        x-example: 26
        enum:
          - false
      transactionDt:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description: Transaction Date
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
      ratePlanType:
        type: string
        format: char
        required:
          - 'false'
        maxLength: 40
        description: eEventAdjustment ratePlanType
        x-example: 432
        enum:
          - false
      lineOfServiceId:
        type: number
        required:
          - 'true'
        maxLength: 32
        description: lineOfServiceId lineOfServiceId
        x-example: abcd
        enum:
          - false
      orgActDate:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description: orgActDate
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
      recDate:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description: recDate
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
      renewalDate:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description: renewalDate
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
  Error:
    type: object
    description: 'As defined in http://api-standards.apps.px-npe01.cf.t-mobile.com/http/status-codes/error-response-format/'
    required:
      - code
      - userMessage
    properties:
      code:
        description: 'A succinct, domain-specific, human-readable text string to identify the type of error for the given status code'
        example: ProductNotFound
        type: string
        pattern: '^[\S ]+$'
      userMessage:
        description: A human-readable message describing the error.
        example: Failed to find product.
        type: string
        pattern: '^[\S ]+$'
      systemMessage:
        description: Text that provides a more detailed technical explanation of the error
        example: PRODUCT_NOT_FOUND
        type: string