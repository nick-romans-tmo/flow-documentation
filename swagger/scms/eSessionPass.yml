swagger: '2.0'
info:
  title: eSessionPass Report
  version: 1.0.0
  description: Reporting Service to Publish EIP Events to SQS
  contact:
    name: Commissions Adapter Notifications
    email: CommissionsAdapterNotifications@t-mobile.com
  license:
    name: Apache2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
host: stg.cpp.scm.ccp.t-mobile.com
basePath: /b2b-experience/v1/e-session-pass-report
x-servers: null
securityDefinitions:
  basicAuth:
    description: security definition
    type: basic
schemes:
  - https
tags:
  - name: ESessionPass Events
    description: 'ESessionPass report'
paths:
  /{event-type}:
    post:
      tags:
        - ESessionPass Events
      operationId: sendMessageUsingPOST
      description: post ESessionPass payload to stg-scm-esessionpass-icm-e
      summary: Post ESessionPass events to SQS
      x-api-pattern: ExecuteFunction
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: event-type
          in: path
          x-example: stg-scm-ESessionPass
          enum:
            - flase
          description: Unique name of the event
          required: true
          type: string
        - name: event
          in: body
          x-example: false
          description: Event Complex Element
          required: true
          schema:
            $ref: '#/definitions/Event'
        - name: Authorization
          in: header
          description: JWT access token with the authentication type set as Bearer.
          x-example: application/json
          required: true
          type: string
          pattern: '^[\S]*$'
        - name: Content-Type
          in: header
          description: The MIME type of this content
          x-example: application/json
          required: true
          type: string
          pattern: '^[\S]*$'
      responses:
        '200':
          description: Message Published Successfully.
          x-example: true
          schema:
            $ref: '#/definitions/Event'
          examples:
            code: 200
            userMessage : ok
          headers:
            Content-Type:
              description: The MIME type of this content
              x-example: application/json
              type: string
              pattern: '^[\S]*$'
            Date:
              description: The date and time that the message was originated (in HTTP-date format as defined by RFC 7231 Date/Time Formats).
              x-example: 'Wed, 18-Apr-2018 11:34:57 GMT'
              type: string
              format: date-time
        '400':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '401':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '404':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '405':
          description: Method Not Allowed
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '405'
              userMessage: Method Not Allowed
              systemMessage: Method Not Allowed
          headers:
            Allow:
              description: list of supported methods for URI
              x-example: GET
              type: string
            service-transaction-id:
              description: Internal identifier for transaction tracking an individual transaction/API request within API platform. Don_t receive/don_t send to downstream
              x-example: '23409209723'
              type: string
        '500':
          description: The server encountered an unexpected condition which prevented it from fulfilling the request.
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '500'
              userMessage: The server encountered an unexpected condition which prevented it from fulfilling the request.
              systemMessage: The server encountered an unexpected condition which prevented it from fulfilling the request.
          headers: {}
        '503':
          description: Service unavailable
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '503'
              userMessage: Service unavailable
              systemMessage: Service unavailable
          headers: {}
        '409':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
        '415':
          description: Bad request error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad request error
          headers: {}
      security:
        - basicAuth: []
definitions:
  Event:
    type: object
    description: Event with ESessionPass data
    properties:
      eventId:
        type: string
        required:
          - 'true'
        x-example: a777334c-d2ba-4f84-9203-051e2b2e9748
        description: >-
          Unique identifier for the event generated by the event producer. This
          element will be searchable in Queue
        enum:
          - false
      eventProducerId:
        type: string
        required:
          - 'true'
        x-example: SalesCommission
        description: Unique identifier for the producer of the event.
        enum:
          - false
      eventTime:
        type: string
        required:
          - 'true'
        format: date-time
        x-example: '2018-08-02T21:47:12.602Z'
        description: The date and time that the event occurred.
        enum:
          - false
      eventType:
        type: string
        required:
          - 'true'
        x-example: stg-scm-ESessionPass
        description: Queue Name that you want to publish
        enum:
          - false
      eventCount:
        type: integer
        required:
          - 'true'
        x-example: 100
        description: No of Records published in the Event
        enum:
          - false
      eventVersion:
        type: string
        required:
          - 'true'
        x-example: '1.0'
        description: >-
          version of the event , will be used to distinguish different versions
          by a consumer
        enum:
          - false
      payload:
        $ref: '#/definitions/Payload'
  Payload:
    type: object
    description: >-
      Event specific information that can be modeled using APIv2 or inline
      elements.
    x-example: false
    properties:
      commissionEvent:
        type: array
        required:
          - 'true'
        description: list of ESessionPass events
        pattern: '^[\S]*$'
        items:
          $ref: '#/definitions/ESessionPass'
        example:
          application/json:
            - rptSequenceId: 11111111
            - reasonId: 123
            - reasonDesc: 'reason 1'
            - reasonStatus: DEACT
  ESessionPass:
    type: object
    description: Event specific body information for ESessionPass
    properties:
      rptSequenceId:
        type: number
        required:
          - 'true'
        maxLength: 40
        description: ESessionPass Rpt Sequence Id
        x-example: 'null'
        enum:
          - false
      jobLogId:
        type: number
        required:
          - 'false'
        maxLength: 50
        description: ESessionPass Job Id
        x-example: 'null'
        enum:
          - false
      eventId:
        type: string
        required:
          - 'true'
        maxLength: 50
        description: ESessionPass eventId
        x-example: 'null'
        enum:
          - false
      espMessageIdDt:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description: espMessageId Date
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
      orderType:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: ESessionPass orderType
        x-example: 1234
        enum:
          - false
      eventType:
        type: string
        required:
          - 'true'
        maxLength: 50
        description: ESessionPass eventType
        x-example: 123
        enum:
          - false
      eventBusChannel:
        type: string
        required:
          - 'true'
        maxLength: 100
        description: ESessionPass Event BusChannel
        x-example: 313313
        enum:
          - false
      ban:
        type: string
        required:
          - 'false'
        maxLength: 30
        description: ESessionPass Ban
        x-example: 313313
        enum:
          - false
      eventSourceSystem:
        type: string
        required:
          - 'true'
        maxLength: 50
        description: ESessionPass event SourceSystem
        x-example: 313313
        enum:
          - false
      orderId:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: order  Id
        x-example: 1234
        enum:
          - false
      orderLineId:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: order LineId
        x-example: 1234
        enum:
          - false
      offerType:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: ESessionPass offer Type
        x-example: 313313
        enum:
          - false
      eventCreateDt:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description: Event Create date
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
      effectiveDt:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description: Effective date
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
      chargeEndDt:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description: charge End date
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
      featureOfferId:
        type: string
        required:
          - 'false'
        maxLength: 40
        description: ESessionPass feature OfferId
        x-example: 313313
        enum:
          - false
      featureStatusChgDt:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description: feature Status ChgDt
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
      serviceNumber:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: ESessionPass service Number
        x-example: hgfh
        enum:
          - false
      lineOfServiceId:
        type: string
        required:
          - 'false'
        maxLength: 30
        description: ESessionPass line Of ServiceId
        x-example: hgfh
        enum:
          - false
      sessionPassType:
        type: string
        required:
          - 'false'
        maxLength: 30
        description: ESessionPass session PassType
        x-example: 313313
        enum:
          - false
      chargeAmt:
        type: string
        format: float
        required:
          - 'false'
        maxLength: 50
        description: ESessionPass chargeAmt
        x-example: 313313
        enum:
          - false
      mrc:
        type: string
        format: float
        required:
          - 'false'
        maxLength: 50
        description: Mrc
        x-example: 1234
        enum:
          - false
      storeId:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: ESessionPass store Id
        x-example: hgfh
        enum:
          - false
      dealerCode:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: dealer Code
        x-example: 1234
        enum:
          - false
      banLevelOffer:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: BanLevel Offer
        x-example: 1234
        enum:
          - false
      orderCreateDt:
        type: string
        required:
          - 'false'
        maxLength: 100
        description:  order CreateDt
        x-example: 1234
        enum:
          - false
      createDt:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description: create Date
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
      eventReceiveId:
        type: number
        required:
          - 'true'
        maxLength: 50
        description:  Event ReceiveId
        x-example: 1234
        enum:
          - false
      internalFeatOfferId:
        type: string
        required:
          - 'false'
        maxLength: 100
        description: internal FeatOffer Id
        x-example: 1234
        enum:
          - false
      transactionDt:
        type: string
        required:
          - 'false'
        maxLength: 20
        format: date-time
        description:  transaction Dt
        x-example: '2999-12-31T12 16:20:11.10'
        enum:
          - false
  Error:
    type: object
    description: 'As defined in http://api-standards.apps.px-npe01.cf.t-mobile.com/http/status-codes/error-response-format/'
    required:
      - code
      - userMessage
    properties:
      code:
        description: 'A succinct, domain-specific, human-readable text string to identify the type of error for the given status code'
        example: ProductNotFound
        type: string
        pattern: '^[\S ]+$'
      userMessage:
        description: A human-readable message describing the error.
        example: Failed to find product.
        type: string
        pattern: '^[\S ]+$'
      systemMessage:
        description: Text that provides a more detailed technical explanation of the error
        example: PRODUCT_NOT_FOUND
        type: string