{
  "swagger": "2.0",
  "info": {
    "description": "A service for fetching data from database",
    "contact": {
      "name": "Nikhil Mittapally",
      "email": "nikhil.mittapally1@t-mobile.com",
      "url": "https://bitbucket.corporate.t-mobile.com/projects/SOC/repos/msg-utility-data-exporter/browse"
    },
    "version": "1.0",
    "title": "msg-utility-data-exporter",
    "termsOfService": "https://www.t-mobile.com/terms/",
    "license": {
      "name": "T-Mobile",
      "url": "https://t-mobile.com/api/license/"
    }
  },
  "host": "message.corporate.t-mobile.com",
  "basePath": "/data/export",
  "tags": [{
    "name": "data-controller",
    "description": "Data Controller"
  }],
  "schemes": [
    "https"
  ],
  "paths": {
    "/{channel}": {
      "get": {
        "tags": [
          "data-controller"
        ],
        "x-api-pattern": "QueryResource",
        "summary": "Returns the data from database into a csv file as a response",
        "description": "Get data in CSV",
        "operationId": "getFileDataUsingGET",
        "produces": [
          "text/csv"
        ],
        "parameters": [{
          "name": "channel",
          "in": "path",
          "description": "channel(facebook, twitter, social)",
          "required": false,
          "type": "string",
          "x-example": "facebook"
        },
          {
            "name": "fromDate",
            "in": "query",
            "description": "from date(pattern = yyyy-MM-dd)",
            "required": false,
            "type": "string",
            "x-example": "2018-07-29"
          },
          {
            "name": "toDate",
            "in": "query",
            "description": "to date(pattern = yyyy-MM-dd)",
            "required": false,
            "type": "string",
            "x-example": "2018-07-29"
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          }
        },
        "deprecated": false
      }
    },
    "/json/{channel}": {
      "get": {
        "tags": [
          "data-controller"
        ],
        "x-api-pattern": "QueryResource",
        "summary": "Returns the data from database in json format",
        "description": "Get data in JSON",
        "operationId": "getJsonDataUsingGET",
        "produces": [
          "application/json"
        ],
        "parameters": [{
          "name": "channel",
          "in": "path",
          "description": "channel(facebook, twitter, social)",
          "required": false,
          "type": "string",
          "x-example": "facebook"
        },
          {
            "name": "fromDate",
            "in": "query",
            "description": "from date(pattern = yyyy-MM-dd)",
            "required": false,
            "type": "string",
            "x-example": "2018-07-29"
          },
          {
            "name": "toDate",
            "in": "query",
            "description": "to date(pattern = yyyy-MM-dd)",
            "required": false,
            "type": "string",
            "x-example": "2018-07-29"
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/BindData"
              }
            }
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          }
        },
        "deprecated": false
      }
    },
    "/status": {
      "get": {
        "tags": [
          "data-controller"
        ],
        "x-api-pattern": "QueryResource",
        "summary": "An endpoint to check the health of the service",
        "description": "Get status",
        "operationId": "getStatusUsingGET",
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/Status"
            }
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          }
        },
        "deprecated": false
      }
    }
  },
  "definitions": {
    "BindData": {
      "type": "object",
      "properties": {
        "authId": {
          "type": "string",
          "example": "3c028911-d5b6-46ac-89d5-10d9a1af7d1c",
          "description": "Authentication ID for user"
        },
        "bindType": {
          "type": "string",
          "example": "tmoid",
          "description": "type of bind(tmoid,altbind)"
        },
        "channel": {
          "type": "string",
          "example": "twitter",
          "description": "Channel (FB, Twitter)"
        },
        "channelId": {
          "type": "string",
          "example": "facebook_108726828894_1238396426239776",
          "description": "Specific id of the customer"
        },
        "conversationId": {
          "type": "string",
          "example": 1231444,
          "description": "ID of the conversation"
        },
        "expiryTimestamp": {
          "type": "string",
          "example": "8/27/2018  7:38:13 PM",
          "description": "Expiration time"
        },
        "key": {
          "type": "string",
          "example": "authenticated.facebook",
          "description": "key"
        },
        "msisdn": {
          "type": "string",
          "example": 123456798,
          "description": "Customer mobile number"
        },
        "subChannel": {
          "type": "string",
          "example": 108726828894,
          "description": "from which page(T-MO Latino,T-Mobile)"
        },
        "timestamp": {
          "type": "string",
          "example": "8/27/2018  7:38:13 PM",
          "description": "Time of creation"
        }
      },
      "title": "BindData"
    },
    "Status": {
      "type": "object",
      "properties": {
        "artifactId": {
          "type": "string",
          "example": "msg-utility-data-exporter",
          "description": "artifactId of the project"
        },
        "profile": {
          "type": "string",
          "example": "dev",
          "description": "profile"
        },
        "startTime": {
          "type": "string",
          "format": "date-time",
          "example": "2018-11-12T7:14:21.316",
          "description": "start time"

        },
        "version": {
          "type": "string",
          "example": "2.11-SNAPSHOT",
          "description": "version"
        }
      },
      "title": "Status"
    }
  }
}