{
  "swagger": "2.0",
  "info": {
    "description": "This API acts as an intermediary layer between Apple Business Chat and the Service-Cards prediction ML model. It logs the details in Cassandra data store for the ML model re-training purposes.",
    "version": "1.0.0",
    "title": "msg-ai-interface-abc-service-cards",
    "contact": {
      "name": "SMPD AI Team",
      "email": "SMPD_Astrobear@T-Mobile.com"
    }
  },
  "host": "secure.message.corporate.t-mobile.com",
  "basePath": "/v1/interface/service-cards",
  "tags": [
    {
      "name": "API",
      "description": "Interface for the ML model"
    }
  ],
  "schemes": [
    "https"
  ],
  "paths": {
    "/category": {
      "post": {
        "tags": [
          "API"
        ],
        "x-api-pattern": "CreateInCollection",
        "summary": "Returns the probability of the service card category",
        "description": "Returns the probability of the category of service-cards based on customer first utterance",
        "operationId": "findServiceCardCategory",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "inputDetails",
            "description": "InputDetails: The initial message of the customer and some additional metadata from the channel",
            "required": true,
            "schema": {
              "$ref": "#/definitions/InputDetails"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "OK: Returns the service-card category and the probability",
            "examples": {
              "category": "Balance Due",
              "probability": 0.0859
            },
            "schema": {
              "$ref": "#/definitions/ServiceCardsResponse"
            }
          },
          "400": {
            "description": "Bad Request"
          }
        },
        "security": [
          {}
        ]
      }
    },
    "/thresholds": {
      "get": {
        "tags": [
          "API"
        ],
        "x-api-pattern": "QueryCollection",
        "summary": "Returns the thresholds for each of the category for Service Cards",
        "description": "Returns the thresholds for each of the category for Service Cards",
        "operationId": "serviceCardThresholdsUsingGET",
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/ServiceCardsThreshold"
              }
            }
          },
          "400": {
            "description": "Bad Request"
          }
        },
        "security": [
          {}
        ]
      }
    }
  },
  "securityDefinitions": {},
  "definitions": {
    "InputDetails": {
      "type": "object",
      "description":"The details of the request body being sent to the Service Card model interface",
      "properties": {
        "encryptedMessage": {
          "description":"The AES-256 encrypted version of the initial message. Encryption is done using Lazy Sodium",
          "type": "string",
          "x-example": "0D7A648249F7075BE2B6143875DC9AABB6222920DE3ADAB4167BE2E1B4AF19"
        },
        "nonce": {
          "description":"The key required to decrypt the AES-256 encrypted message",
          "type": "string",
          "x-example": "2E1B4AF19"
        },
        "conversationId": {
          "description":"The unique conversation id",
          "type": "string",
          "x-example": "228392"
        },
        "channel": {
          "description":"The Channel in which the message has originated",
          "type": "string",
          "x-example":"iMessage"
        },
        "channelId": {
          "description":"The unique ID of the customer in the channel",
          "type": "string",
          "x-example":"U234325-SAJMHFS34444-34536T76WWEFS"
        },
        "msisdn": {
          "type": "string",
          "description": "A customer msisdn (10 digit phone number)",
          "x-example": "5558675309",
          "minLength": 10,
          "maxLength": 10
        }
      },
      "title": "InputDetails"
    },
    "ServiceCardsResponse": {
      "type": "object",
      "properties": {
        "category": {
          "description":"The category of the service cards based on the customer initial message",
          "type": "string",
          "x-example": "Balance Due"
        },
        "probability": {
          "type": "number",
          "format": "double",
          "description": "A probability that this category is the correct category, between 0 and 1",
          "x-example": 0.42130,
          "minimum": 0,
          "maximum": 1
        }
      },
      "title": "ServiceCardsResponse"
    },
    "ServiceCardsThreshold": {
      "type": "object",
      "properties": {
        "category": {
          "type": "string",
          "description": "The category of the service cards",
          "x-example": "Order Status"
        },
        "threshold": {
          "type": "number",
          "format": "double",
          "description": "The threshold for the category of the service cards",
          "x-example": 0.3
        }
      },
      "title": "ServiceCardsThreshold"
    }
  }
}
