{
  "swagger": "2.0",
  "info": {
    "description": "A URL shortener service that allows to shorten longer URLs and redirects back from short URL to the original URL",
    "version": "1.0",
    "title": "msg-api-v1-url-shortener",
    "termsOfService": "https://www.t-mobile.com/terms/",
    "contact": {
      "name": "Samantha Snider",
      "email": "Samantha.Snider@T-Mobile.com"
    },
    "license": {
      "name": "T-Mobile",
      "url": "https://t-mobile.com/api/license/"
    }
  },
  "host": "localhost",
  "basePath": "/url/s",
  "tags": [
    {
      "name": "service-controller",
      "description": "Endpoints for creating short URL for a long URL and redirecting from short to long URL"
    }
  ],
  "paths": {
    "/redirect/fail": {
      "get": {
        "tags": [
          "service-controller"
        ],
        "summary": "Displays a static error page",
        "operationId": "redirectToFailUsingGET",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "*/*"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "type": "string"
            }
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          }
        }
      }
    },
    "/shorten": {
      "post": {
        "tags": [
          "service-controller"
        ],
        "summary": "Creates a short URl for a given long URL",
        "operationId": "shortenUsingPOST",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "longUrlRequest",
            "description": "longUrlRequest",
            "required": true,
            "schema": {
              "$ref": "#/definitions/LongUrlRequest"
            }
          },
          {
            "name": "Authorization",
            "in": "header",
            "description": "Authorization",
            "required": false,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/ShortUrlResponse"
            }
          },
          "201": {
            "description": "Created"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          }
        }
      }
    },
    "/{key}": {
      "get": {
        "tags": [
          "service-controller"
        ],
        "summary": "Redirects from a short URL to the original long URL or the static error page if the short URL is not found",
        "operationId": "redirectUsingGET",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "*/*"
        ],
        "parameters": [
          {
            "name": "key",
            "in": "path",
            "description": "key",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          }
        }
      }
    }
  },
  "definitions": {
    "LongUrlRequest": {
      "type": "object",
      "required": [
        "url"
      ],
      "properties": {
        "ageInMinutes": {
          "type": "integer",
          "format": "int64",
          "description": "Ttl for the short URL in minutes"
        },
        "customerId": {
          "type": "string",
          "description": "Unique customer ID for whom the short URL is created"
        },
        "msisdn": {
          "type": "string",
          "description": "Customer msisdn"
        },
        "url": {
          "type": "string",
          "example": "https://dev.messaging.t-mobile.com/very/long/url/that/needs/to/be/shortened",
          "description": "Long URL that needs to be shortened"
        }
      },
      "description": "A request to shorten a long URL"
    },
    "ShortUrlResponse": {
      "type": "object",
      "required": [
        "shortenedUrl"
      ],
      "properties": {
        "shortenedUrl": {
          "type": "string",
          "example": "https://t-mo.cc/u/ZTnVCxLN",
          "description": "Shortened URL"
        }
      },
      "description": "A response to a short URL creation request; contains the created shortened URl"
    }
  }
}