{
  "swagger": "2.0",
  "info": {
    "description": "A service for getting Conversations from particular account",
    "version": "1.0.0",
    "title": "msg-utility-le-conversations",
    "contact": {
      "name": "Ranjit Nair",
      "email": "ranjit.nair12@t-mobile.com"
    }
  },
  "host": "api.t-mobile.com",
  "schemes": [
    "https"
  ],
  "securityDefinitions": {
    "conversationAuth": {
      "type": "oauth2",
      "tokenUrl": "https://core.saas.api.t-mobile.com/oauth2/v4/tokens",
      "flow": "accessCode"
    }
  },
  "basePath": "/msg/retail/le/conversations",
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "tags": [
    {
      "name": "rest-controller",
      "description": "operations to find conversations from specific account"
    }
  ],
  "paths": {
    "/pending/all": {
      "get": {
        "x-api-pattern": "QueryCollection",
        "description": "Get Request to get list of conversations that are pending from Agent to respond",
        "tags": [
          "rest-controller"
        ],
        "summary": "Get all OPEN conversations from account",
        "operationId": "getConversationsUsingGET",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "*/*"
        ],
        "security": [
          {
            "conversationAuth": []
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "headers": {
              "Content-Type": {
                "type": "string",
                "description": "Return Content Type in response header",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "Describes browser may cache the response",
                "x-example": "no-cache"
              }
            },
            "schema": {
              "$ref": "#/definitions/PendingConversations"
            },
            "examples": {
              "count": 1,
              "pendingConversations": {
                "storeId": "Store_TPR_999",
                "conversationId": "conversationId123",
                "customerId": "customerId123",
                "agentName": "agent-name@t-mobile.com",
                "msisdn": "9999999999",
                "pendingAgentStartTime": 0,
                "lastCustomerResponseTime": 0
              }
            }
          },
          "401": {
            "description": "Unauthorized,",
            "headers": {
              "Content-Type": {
                "type": "string",
                "description": "Return Content Type in response header",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "Describes browser may cache the response",
                "x-example": "no-cache"
              }
            },
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "code": "401",
              "reason": "Http Unauthorized",
              "description": "Request is unauthorized"
            }
          },
          "403": {
            "description": "Forbidden",
            "headers": {
              "Content-Type": {
                "type": "string",
                "description": "Return Content Type in response header",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "Describes browser may cache the response",
                "x-example": "no-cache"
              }
            },
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "code": "400",
              "reason": "Http Forbidden",
              "description": "Forbidden Request Occured"
            }
          },
          "404": {
            "description": "Not Found",
            "headers": {
              "Content-Type": {
                "type": "string",
                "description": "Return Content Type in response header",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "Describes browser may cache the response",
                "x-example": "no-cache"
              }
            },
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "code": "400",
              "reason": "Http Not Found",
              "description": "Requested resource was not found"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "Error": {
      "type": "object",
      "required": [
        "code",
        "reason",
        "description"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "description": "Status",
          "example": 400,
          "pattern": "\\d{3}",
          "minimum": 400,
          "maximum": 599
        },
        "reason": {
          "type": "string",
          "description": "Reason Phrase",
          "example": "HTTP Bad Request",
          "pattern": ".+"
        },
        "description": {
          "type": "string",
          "description": "Description of the error",
          "example": "Bad Request Occured",
          "pattern": ".+"
        }
      },
      "description": "Error"
    },
    "PendingConversations": {
      "type": "object",
      "required": [
        "count",
        "pendingConversations"
      ],
      "properties": {
        "count": {
          "type": "integer",
          "description": "Returns Count of number of Pending Conversations",
          "pattern": "\\d{6}[0-9]",
          "format": "int32",
          "example": 1
        },
        "pendingConversations": {
          "type": "array",
          "description": "Return List of Pending Conversation Response Object",
          "items": {
            "$ref": "#/definitions/PendingConversationResponse"
          },
          "example": {
            "storeId": "Store_TPR_999",
            "conversationId": "conversationId123",
            "customerId": "customerId123",
            "agentName": "agent-name@t-mobile.com",
            "msisdn": "9999999999",
            "pendingAgentStartTime": 123456789,
            "lastCustomerResponseTime": 123456789
          }
        }
      }
    },
    "PendingConversationResponse": {
      "type": "object",
      "description": "Returns the Pending Conversation Response Object with below attributes",
      "required": [
        "storeId",
        "conversationId",
        "customerId",
        "agentName",
        "msisdn",
        "pendingAgentStartTime",
        "lastCustomerResponseTime"
      ],
      "properties": {
        "storeId": {
          "type": "string",
          "example": "Store_TPR_999",
          "description": "Returns latest skill name that conversation is under",
          "pattern": ".+"
        },
        "conversationId": {
          "type": "string",
          "example": "conversationId123",
          "description": "Returns conversationId of the Pending Conversation",
          "pattern": ".+"
        },
        "customerId": {
          "type": "string",
          "example": "customerId123",
          "description": "Returns customerId of the Pending Conversation",
          "pattern": ".+"
        },
        "agentName": {
          "type": "string",
          "example": "agent-name@t-mobile.com",
          "description": "Returns latest agent login name of the Pending Conversation",
          "pattern": ".+"
        },
        "msisdn": {
          "type": "string",
          "example": "9999999999",
          "description": "Returns msisdn of the Pending Conversation",
          "pattern": ".+"
        },
        "pendingAgentStartTime": {
          "type": "number",
          "example": 123456789,
          "description": "Returns the earliest pending agent start time info of the Pending Conversation",
          "pattern": "\\d{13-15}[0-9]"
        },
        "lastCustomerResponseTime": {
          "type": "number",
          "example": 123456789,
          "description": "Returns the latest customer response time info of the Pending Conversation",
          "pattern": "\\d{13-15}[0-9]"
        }
      }
    }
  }
}