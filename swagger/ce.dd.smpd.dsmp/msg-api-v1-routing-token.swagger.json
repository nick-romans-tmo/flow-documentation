{
  "swagger": "2.0",
  "info": {
    "description": "This service is used to store the Live Engage conversation id and FCM token per browser. This data is used to push the agent notification",
    "version": "1.0",
    "title": "msg-api-v1-routing-token",
    "termsOfService": "https://www.t-mobile.com/terms/",
    "contact": {
      "name": "Soumya Kulkarni",
      "email": "Soumya.Kulkarni1@T-Mobile.com"
    },
    "license": {
      "name": "T-Mobile",
      "url": "https://t-mobile.com/api/license/"
    }
  },
  "host": "secure.message.corporate.t-mobile.com",
  "basePath": "/v1/routing/token",
  "tags": [
    {
      "name": "routing-token-controller",
      "description": "Routing Token Controller"
    }
  ],
  "paths": {
    "/create": {
      "post": {
        "tags": [
          "routing-token-controller"
        ],
        "summary": "Create or Update the token and conversation mapping",
        "operationId": "createRoutingTokenUsingPOST",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "*/*"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "routingTokenRequest",
            "description": "Create token request",
            "required": true,
            "schema": {
              "$ref": "#/definitions/RoutingTokenRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success"
          },
          "201": {
            "description": "Created"
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          },
          "500": {
            "description": "Internal Processing Error"
          }
        }
      }
    },
    "/delete": {
      "delete": {
        "tags": [
          "routing-token-controller"
        ],
        "summary": "Delete the token and conversation mapping",
        "operationId": "deleteRoutingTokenUsingDELETE",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "*/*"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "routingTokenRequest",
            "description": "Create token request",
            "required": true,
            "schema": {
              "$ref": "#/definitions/RoutingTokenRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success"
          },
          "204": {
            "description": "No Content"
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "500": {
            "description": "Internal Processing Error"
          }
        }
      }
    },
    "/delete/{conversationId}": {
      "delete": {
        "tags": [
          "routing-token-controller"
        ],
        "summary": "Delete the token and conversation mapping",
        "operationId": "deleteRoutingTokenByConversationUsingDELETE",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "*/*"
        ],
        "parameters": [
          {
            "name": "conversationId",
            "in": "path",
            "description": "Conversation Id",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Success"
          },
          "204": {
            "description": "No Content"
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "500": {
            "description": "Internal Processing Error"
          }
        }
      }
    },
    "/getTokenByConversationId/{conversationId}": {
      "get": {
        "tags": [
          "routing-token-controller"
        ],
        "summary": "Update the token and conversation mapping",
        "operationId": "getRoutingTokenByConversationIdUsingGET",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "*/*"
        ],
        "parameters": [
          {
            "name": "conversationId",
            "in": "path",
            "description": "Conversation Id",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/RoutingTokenResponse"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          },
          "500": {
            "description": "Internal Processing Error"
          }
        }
      }
    },
    "/status": {
      "get": {
        "tags": [
          "routing-token-controller"
        ],
        "summary": "getStatus",
        "operationId": "getStatusUsingGET",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "*/*"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/Status"
            }
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          }
        }
      }
    },
    "/update": {
      "put": {
        "tags": [
          "routing-token-controller"
        ],
        "summary": "Update the token and conversation mapping",
        "operationId": "updateRoutingTokenUsingPUT",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "*/*"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "routingTokenRequest",
            "description": "Create token request",
            "required": true,
            "schema": {
              "$ref": "#/definitions/RoutingTokenRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success"
          },
          "201": {
            "description": "Created"
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          },
          "500": {
            "description": "Internal Processing Error"
          }
        }
      }
    }
  },
  "definitions": {
    "Error": {
      "type": "object",
      "properties": {
        "cause": {
          "$ref": "#/definitions/Throwable"
        },
        "localizedMessage": {
          "type": "string"
        },
        "message": {
          "type": "string"
        },
        "stackTrace": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/StackTraceElement"
          }
        },
        "suppressed": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Throwable"
          }
        }
      }
    },
    "RoutingData": {
      "type": "object",
      "properties": {
        "conversationId": {
          "type": "string"
        },
        "token": {
          "type": "string"
        }
      }
    },
    "RoutingTokenRequest": {
      "type": "object",
      "required": [
        "conversationId",
        "token"
      ],
      "properties": {
        "conversationId": {
          "type": "string",
          "description": "Live engage conversation id"
        },
        "token": {
          "type": "string",
          "description": "FCM token for the given conversation id"
        }
      }
    },
    "RoutingTokenResponse": {
      "type": "object",
      "properties": {
        "routingDataList": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/RoutingData"
          }
        }
      }
    },
    "StackTraceElement": {
      "type": "object",
      "properties": {
        "className": {
          "type": "string"
        },
        "fileName": {
          "type": "string"
        },
        "lineNumber": {
          "type": "integer",
          "format": "int32"
        },
        "methodName": {
          "type": "string"
        },
        "nativeMethod": {
          "type": "boolean"
        }
      }
    },
    "Status": {
      "type": "object",
      "properties": {
        "artifactId": {
          "type": "string"
        },
        "profile": {
          "type": "string"
        },
        "startTime": {
          "type": "string"
        },
        "version": {
          "type": "string"
        }
      }
    },
    "Throwable": {
      "type": "object",
      "properties": {
        "cause": {
          "$ref": "#/definitions/Throwable"
        },
        "localizedMessage": {
          "type": "string"
        },
        "message": {
          "type": "string"
        },
        "stackTrace": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/StackTraceElement"
          }
        },
        "suppressed": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Throwable"
          }
        }
      }
    }
  }
}