{
    "swagger": "2.0",
    "info": {
        "description": "Simplified Workflow Search API",
        "title": "wf-search-api",
        "version": "v1",
        "contact": {
            "name": "Enterprise Solution Delivery - DevOps Team",
            "url": "https://techportal.t-mobile.com",
            "email": "ESD_DEVOPS@t-mobile.com"
        }
    },
    "host": "wf-search-api-test.test.px-npe02b.cf.t-mobile.com",
    "basePath": "/itsm/workflow/v1/search",
    "tags": [{
       "name": "WorkFlow Search API",
        "description": "WorkFlow Search API"
    }],
    "paths": {
        "/activitytracker": {
            "get": {
                "tags": ["WorkFlow Search API"],
                "summary": "Get activity tracker by Task/Record.",
                "operationId": "getActivityTrackerDetailsUsingGET",
                "produces": ["application/json"],
                "parameters": [{
                    "name": "record_id",
                    "in": "query",
                    "description": "record_id",
                    "required": false,
                    "type": "string"
                }, {
                    "name": "record_type",
                    "in": "query",
                    "description": "record_type",
                    "required": false,
                    "type": "string"
                }, {
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/categories": {
            "get": {
                "tags": ["WorkFlow Search API"],
                "summary": "Get list of categories.",
                "operationId": "getCategoriesUsingGET",
                "produces": ["application/json;charset=UTF-8", "application/atom+xml"],
                "parameters": [{
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/dependency/task": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "Validate whether task has all the dependencies in closed state.",
                "operationId": "validateDependencyUsingGET",
                "produces": ["application/json"],
                "parameters": [{
                    "name": "task-id",
                    "in": "query",
                    "description": "task-id",
                    "required": false,
                    "type": "integer",
                    "format": "int64"
                }, {
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/dependency/{workflowId}": {
            "get": {
                "tags": ["WorkFlow Search API"],
                "summary": "Get dependency by workflow id.",
                "operationId": "getDependencyByWorkflowIdUsingGET",
                "produces": ["application/json"],
                "parameters": [{
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }, {
                    "name": "workflowId",
                    "in": "path",
                    "description": "workflowId",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/task/advance-search": {
            "post": {
                 "tags": ["WorkFlow Search API"],
                "summary": "View list of workflow tasks for advance search.",
                "operationId": "workflowTaskAdvanceSearchUsingPOST",
                "consumes": ["application/json"],
                "produces": ["application/json;charset=UTF-8", "application/atom+xml"],
                "parameters": [{
                    "in": "body",
                    "name": "params",
                    "description": "params",
                    "required": true,
                    "schema": {
                        "type": "object",
                        "additionalProperties": {
                            "type": "string"
                        }
                    }
                }, {
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "201": {
                        "description": "Created"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/task/document/search": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "View list of workflow document tasks assigned to user.",
                "operationId": "viewWorkflowTaskDocumentUsingGET",
                "produces": ["application/json;charset=UTF-8", "application/atom+xml"],
                "parameters": [{
                    "name": "task_id",
                    "in": "query",
                    "description": "task_id",
                    "required": true,
                    "type": "string"
                }, {
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/task/fyi-user": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "Get fyi users for workflow task.",
                "operationId": "getFyiUsersUsingGET",
                "produces": ["application/json"],
                "parameters": [{
                    "name": "task-id",
                    "in": "query",
                    "description": "task-id",
                    "required": false,
                    "type": "integer",
                    "format": "int64"
                }, {
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/taskdependency/{template-id}": {
            "get": {
                "tags": ["WorkFlow Search API"],
                "summary": "Get task dependency by template id.",
                "operationId": "getTaskDependencyByTemplateIdUsingGET",
                "produces": ["application/json"],
                "parameters": [{
                    "name": "template-id",
                    "in": "path",
                    "description": "template-id",
                    "required": true,
                    "type": "integer",
                    "format": "int64"
                }, {
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/tasks/kanban": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "View list of tasks assigned to user by days.",
                "operationId": "viewMyTaskByDaysUsingGET",
                "produces": ["application/json;charset=UTF-8", "application/atom+xml"],
                "parameters": [{
                    "name": "days",
                    "in": "query",
                    "description": "days",
                    "required": true,
                    "type": "integer",
                    "format": "int32"
                }, {
                    "name": "key",
                    "in": "query",
                    "description": "key",
                    "required": true,
                    "type": "string"
                }, {
                    "name": "time",
                    "in": "query",
                    "description": "time",
                    "required": true,
                    "type": "string"
                }, {
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/tasks/my": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "Get list of tasks assigned to user.",
                "operationId": "getMyTasksUsingGET",
                "produces": ["application/json;charset=UTF-8", "application/atom+xml"],
                "parameters": [{
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/tasks/my/startdate": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "Get list of tasks assigned to user where startDate within given bounds.",
                "operationId": "getMyTasksByStartDateRangeUsingGET",
                "produces": ["application/json;charset=UTF-8", "application/atom+xml"],
                "parameters": [{
                    "name": "lower-bound-inclusive",
                    "in": "query",
                    "description": "lower-bound-inclusive",
                    "required": true,
                    "type": "string"
                }, {
                    "name": "upper-bound-exclusive",
                    "in": "query",
                    "description": "upper-bound-exclusive",
                    "required": true,
                    "type": "string"
                }, {
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/tasks/search": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "Get workflow task by workflow id.",
                "operationId": "getWorkflowTaskByWorkflowIdUsingGET",
                "produces": ["application/json;charset=UTF-8", "application/atom+xml"],
                "parameters": [{
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }, {
                    "name": "workflow",
                    "in": "query",
                    "description": "workflow",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/tasks/view/count": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "View count of list of tasks assigned to user.",
                "operationId": "viewMyTaskUsingGET",
                "produces": ["application/json;charset=UTF-8", "application/atom+xml"],
                "parameters": [{
                    "name": "lower-bound-inclusive",
                    "in": "query",
                    "description": "lower-bound-inclusive",
                    "required": true,
                    "type": "string"
                }, {
                    "name": "lower-upper-bound",
                    "in": "query",
                    "description": "lower-upper-bound",
                    "required": true,
                    "type": "string"
                }, {
                    "name": "upper-bound-exclusive",
                    "in": "query",
                    "description": "upper-bound-exclusive",
                    "required": true,
                    "type": "string"
                }, {
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/tasks/{task-id}": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "Get workflow task by id.",
                "operationId": "getWorkflowTaskByIdUsingGET",
                "produces": ["application/json;charset=UTF-8", "application/atom+xml"],
                "parameters": [{
                    "name": "task-id",
                    "in": "path",
                    "description": "task-id",
                    "required": true,
                    "type": "integer",
                    "format": "int64"
                }, {
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/template/latestversion": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "Get templates owned by user-id, only latest version.",
                "operationId": "getTemplatesByUserIdLatestVersionUsingGET",
                "produces": ["application/json"],
                "parameters": [{
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/template/my": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "Get templates created by user-id.",
                "operationId": "getTemplatesByUserIdUsingGET",
                "produces": ["application/json"],
                "parameters": [{
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/template/popular": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "Get most popular templates by user.",
                "operationId": "getPopularTemplatesByUserIdUsingGET",
                "produces": ["application/json"],
                "parameters": [{
                    "name": "top",
                    "in": "query",
                    "description": "top",
                    "required": false,
                    "type": "integer",
                    "default": 5,
                    "format": "int32"
                }, {
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/template/recent": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "Get recent templates by user.",
                "operationId": "getRecentTemplatesByUserIdUsingGET",
                "produces": ["application/json"],
                "parameters": [{
                    "name": "top",
                    "in": "query",
                    "description": "top",
                    "required": false,
                    "type": "integer",
                    "default": 5,
                    "format": "int32"
                }, {
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/template/search": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "Get templates by detail.",
                "operationId": "getTemplatesUsingGET",
                "produces": ["application/json"],
                "parameters": [{
                    "name": "active_flag",
                    "in": "query",
                    "description": "active_flag",
                    "required": false,
                    "type": "integer",
                    "format": "int32"
                }, {
                    "name": "category",
                    "in": "query",
                    "description": "category",
                    "required": false,
                    "type": "string"
                }, {
                    "name": "description",
                    "in": "query",
                    "description": "description",
                    "required": false,
                    "type": "string"
                }, {
                    "name": "is_published",
                    "in": "query",
                    "description": "is_published",
                    "required": false,
                    "type": "integer",
                    "format": "int32"
                }, {
                    "name": "owner",
                    "in": "query",
                    "description": "owner",
                    "required": false,
                    "type": "string"
                }, {
                    "name": "previous_version",
                    "in": "query",
                    "description": "previous_version",
                    "required": false,
                    "type": "integer",
                    "format": "int64"
                }, {
                    "name": "template-name",
                    "in": "query",
                    "description": "template-name",
                    "required": false,
                    "type": "string"
                }, {
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/template/{template-id}": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "Get templates by id.",
                "operationId": "getTemplatesByIdUsingGET",
                "produces": ["application/json"],
                "parameters": [{
                    "name": "template-id",
                    "in": "path",
                    "description": "template-id",
                    "required": true,
                    "type": "integer",
                    "format": "int64"
                }, {
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/templatetasks/search": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "Get template tasks by template id.",
                "operationId": "getTemplateTasksByTemplateIdUsingGET",
                "produces": ["application/json"],
                "parameters": [{
                    "name": "active_flag",
                    "in": "query",
                    "description": "active_flag",
                    "required": false,
                    "type": "string"
                }, {
                    "name": "template-id",
                    "in": "query",
                    "description": "template-id",
                    "required": true,
                    "type": "integer",
                    "format": "int64"
                }, {
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/templatetasks/{templatetask-id}": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "Get template tasks by templatetask id.",
                "operationId": "getTemplateTasksByIdUsingGET",
                "produces": ["application/json"],
                "parameters": [{
                    "name": "templatetask-id",
                    "in": "path",
                    "description": "templatetask-id",
                    "required": true,
                    "type": "integer",
                    "format": "int64"
                }, {
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/workflows/my": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "Get workflows by user-id.",
                "operationId": "getMyWorkflowsUsingGET",
                "produces": ["application/json;charset=UTF-8", "application/atom+xml"],
                "parameters": [{
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/workflows/my/byStatus": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "Get all my workflows count by status.",
                "operationId": "getMyWorkflowsByStatusUsingGET",
                "produces": ["application/json", "application/json;charset=UTF-8"],
                "parameters": [{
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        },
        "/workflows/{workflow-id}": {
            "get": {
                 "tags": ["WorkFlow Search API"],
                "summary": "Get workflows by id.",
                "operationId": "getWorkflowsByIdUsingGET",
                "produces": ["application/json"],
                "parameters": [{
                    "name": "user-id",
                    "in": "header",
                    "description": "user-id",
                    "required": true,
                    "type": "string"
                }, {
                    "name": "workflow-id",
                    "in": "path",
                    "description": "workflow-id",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ResponseEntity"
                        }
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                },
                "deprecated": false
            }
        }
    },
    "definitions": {
        "ResponseEntity": {
            "type": "object",
            "properties": {
                "body": {
                    "type": "object"
                },
                "statusCode": {
                    "type": "string",
                    "enum": ["100", "101", "102", "103", "200", "201", "202", "203", "204", "205", "206", "207", "208", "226", "300", "301", "302", "303", "304", "305", "307", "308", "400", "401", "402", "403", "404", "405", "406", "407", "408", "409", "410", "411", "412", "413", "414", "415", "416", "417", "418", "419", "420", "421", "422", "423", "424", "426", "428", "429", "431", "451", "500", "501", "502", "503", "504", "505", "506", "507", "508", "509", "510", "511"]
                },
                "statusCodeValue": {
                    "type": "integer",
                    "format": "int32"
                }
            },
            "title": "ResponseEntity"
        }
    }
}
