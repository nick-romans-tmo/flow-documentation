swagger: "2.0"
info:
  title: ExpressPay
  description: T-Mobile OneSite Unauthenticated Payments
  version: "1.0.0"
  contact:
    name: Web PUB Core Team
    email: Web_PUB_Core@T-Mobile.com
host: "www.t-mobile.com"
schemes:
  - https
basePath: /v1/payments
x-servers:
  - url: "t-mobile.com"
    description: server path
parameters:
  Content-Type:
    name: Content-Type
    in: header
    type: string
    required: true
    description: application/json;charset=UTF-8
    x-example: application/json;charset=UTF-8
    pattern: ^.*$
  Authorization:
    name: Authorization
    in: header
    type: string
    required: true
    description: application/json;charset=UTF-8
    x-example: application/json;charset=UTF-8
    pattern: ^.*$
tags:
  - name: Customer
    description: Customer validation and account summary
  - name: Payment
    description: Payment validation and Processing
produces:
  - application/json

paths:
  /validate-customer/{customer-number}:
    post:
      summary: validates customer phone or account number
      operationId: validateCustomer
      consumes:
        - "application/json"
      produces:
        - "application/json"
      x-api-pattern: ExecuteFunction
      description: validates customer phone or account number
      parameters:
        - $ref: "#/parameters/Authorization"
        - in: path
          type: string
          name: customer-number
          description: phone or account number
          required: true
          pattern: ^\d{9,10}$
          x-example: 4251111111
      tags:
        - Customer
      responses:
        "200":
          schema:
            $ref: "#/definitions/CustomerSummary"
          description: Success response
          headers:
            Date:
              type: string
              description: THUR, 09 May 2019 16:04:21 EST
              x-example: THUR, 09 May 2019 16:04:21 EST
            Content-Type:
              type: string
              description: application/json;charset=UTF-8
              x-example: application/json;charset=UTF-8
            Cache-Control:
              type: string
              description: whether cache control is defined
              x-example: no-cache
          examples:
            data: { "isValidCustomer": "true" }
        "400":
          schema:
            $ref: "#/definitions/Error"
          description: "Bad Request, make sure all mandatory input parameters are present and are in correct format."
          examples:
            data:
              {
                "code": "400",
                "userMessage": "invalid customer number",
                "systemMessage": "",
              }
        "500":
          schema:
            $ref: "#/definitions/Error"
          description: Internal Server  error
          examples:
            data:
              {
                "code": "500",
                "userMessage": "",
                "systemMessage": "internal system error",
              }
  /public-key:
    get:
      summary: Get public Key for payment encryption
      x-api-pattern: QueryResource
      operationId: getPublicKey
      produces:
        - "application/json"
      description: Get public Key for payment encryption
      parameters:
        - $ref: "#/parameters/Authorization"
      tags:
        - Payment
      responses:
        "200":
          schema:
            $ref: "#/definitions/PublicKeyResponse"
          description: Success response
          headers:
            Date:
              type: string
              description: THUR, 09 May 2019 16:04:21 EST
              x-example: THUR, 09 May 2019 16:04:21 EST
            Content-Type:
              type: string
              description: application/json;charset=UTF-8
              x-example: application/json;charset=UTF-8
            Cache-Control:
              type: string
              description: whether cache control is defined
              x-example: no-cache
          examples:
            data: { "publicKey": "xxx" }
        "400":
          schema:
            $ref: "#/definitions/Error"
          description: "Bad Request, make sure all mandatory input parameters are present and are in correct format."
          examples:
            data:
              {
                "code": "400",
                "userMessage": "invalid customer number",
                "systemMessage": "",
              }
        "500":
          schema:
            $ref: "#/definitions/Error"
          description: Internal Server  error
          examples:
            data:
              {
                "code": "500",
                "userMessage": "",
                "systemMessage": "internal system error",
              }

  /validate-payment-method:
    post:
      summary: validates the payment method
      operationId: validatePaymentMethod
      x-api-pattern: ExecuteFunction
      description: Hapi endpoint which validates the payment method
      parameters:
        - $ref: "#/parameters/Content-Type"
        - $ref: "#/parameters/Authorization"
        - in: body
          name: body
          description: Payment object containing the subscriber's credit card number
          required: true
          schema:
            $ref: "#/definitions/ValidatePaymentMethodRequest"
      tags:
        - Payment
      responses:
        "200":
          schema:
            $ref: "#/definitions/ValidatePaymentMethodResponse"
          description: Success response
          headers:
            Date:
              type: string
              description: THUR, 09 May 2019 16:04:21 EST
              x-example: THUR, 09 May 2019 16:04:21 EST
            Content-Type:
              type: string
              description: application/json;charset=UTF-8
              x-example: application/json;charset=UTF-8
            Cache-Control:
              type: string
              description: whether cache control is defined
              x-example: no-cache
          examples:
            data: { "status": "true" }
        "400":
          schema:
            $ref: "#/definitions/Error"
          description: "Bad Request, make sure all mandatory input parameters are present and are in correct format."
          examples:
            data:
              {
                "code": "400",
                "userMessage": "invalid customer number",
                "systemMessage": "",
              }
        "500":
          schema:
            $ref: "#/definitions/Error"
          description: Internal Server  error
          examples:
            data:
              {
                "code": "500",
                "userMessage": "",
                "systemMessage": "internal system error",
              }
  /process-payment:
    post:
      summary: submits payment to DPS
      operationId: processPayment
      consumes:
        - "application/json"
      produces:
        - "application/json"
      x-api-pattern: ExecuteFunction
      description: Hapi endpoint takes user input and process payment
      parameters:
        - $ref: "#/parameters/Content-Type"
        - $ref: "#/parameters/Authorization"
        - in: body
          name: body
          description: subscriber object for payment processing
          required: true
          schema:
            $ref: "#/definitions/PaymentRequest"
      tags:
        - Payment
      responses:
        "200":
          schema:
            $ref: "#/definitions/PaymentResponse"
          description: Success response
          headers:
            Date:
              type: string
              description: THUR, 09 May 2019 16:04:21 EST
              x-example: THUR, 09 May 2019 16:04:21 EST
            Content-Type:
              type: string
              description: application/json;charset=UTF-8
              x-example: application/json;charset=UTF-8
            Cache-Control:
              type: string
              description: whether cache control is defined
              x-example: no-cache
          examples:
            data: { "status": "true" }
        "400":
          schema:
            $ref: "#/definitions/Error"
          description: "Bad Request, make sure all mandatory input parameters are present and are in correct format."
          examples:
            data:
              {
                "code": "400",
                "userMessage": "invalid customer number",
                "systemMessage": "",
              }
        "500":
          schema:
            $ref: "#/definitions/Error"
          description: Internal Server  error
          examples:
            data:
              {
                "code": "500",
                "userMessage": "",
                "systemMessage": "internal system error",
              }
definitions:
  CustomerSummary:
    type: object
    properties:
      isValidCustomer:
        type: boolean
        description: boolean indicator to identify if input Subscriber is valid or not
      CompanyIndicator:
        type: string
        description: SPRINT or T-Mobile
        enum:
          - SPRINT
          - T-Mobile
      BillingMethod:
        type: string
        description: PREPAID or POSTPAID
        enum:
          - PREPAID
          - POSTPAID
      BillingSystem:
        type: string
        description: RPX, TBILL, SAMSON, Sprint-ENSEMBLE
        enum:
          - RPX
          - TBILL
          - SAMSON
          - Sprint-ENSEMBLE
  PublicKeyResponse:
    type: object
    properties:
      publicKey:
        type: string
        description: public key for encryption
        pattern: ^.*$
  ValidatePaymentMethodRequest:
    type: object
    properties:
      bin:
        type: string
        description: The first 6 digit of the card number
        pattern: ^\d{6}$
        example: 444442
      cardNumber:
        type: string
        description: Encrypted card number
        pattern: ^.*$
        example: xxx
    required:
      - bin
      - cardNumber
  ValidatePaymentMethodResponse:
    type: object
    properties:
      status:
        type: boolean
        description: boolean indicator specifying input Card is valid or not
  PaymentCard:
    type: object
    properties:
      cardholderName:
        type: string
        description: Card holder name
        pattern: ^.*$
        example: test
      cardNumber:
        type: string
        description: Encrypted card number
        pattern: ^.*$
        example: xxx
      expirationMonthYear:
        type: string
        description: Card expiration Date
        format: MMYY
        example: 0121
      cvv:
        type: string
        description: cvv number
        pattern: ^\d{3,4}$
        example: 123
      zipCode:
        type: string
        description: Billing zip code
        pattern: ^\d{5}$
        example: 98021
    required:
      - cardholderName
      - cardNumber
      - expirationMonthYear
      - cvv
      - zipCode
  PaymentRequest:
    type: object
    properties:
      customerNumber:
        type: string
        description: phone or account number
        pattern: ^\d{9,10}$
        example: 4251111111
      paymentCard:
        $ref: "#/definitions/PaymentCard"
      amount:
        type: integer
        description: amount for payment processing
        minimum: 1.01
        maximum: 99999
        example: 1.01
      emailAddress:
        type: string
        description: email address for payment confirmation
        pattern: ^.*$
    required:
      - customerNumber
      - amount
  PaymentResponse:
    type: object
    properties:
      status:
        type: boolean
        description: Indicator if payment processed successfully or failed
      code:
        type: string
        description: Payment failure code
  Error:
    type: object
    properties:
      code:
        type: string
        description: code
        example: "400"
        pattern: ^.*$
      userMessage:
        type: string
        description: userMessage
        example: user message
        pattern: ^.*$
      systemMessage:
        type: string
        description: systemMessage
        example: system message
        pattern: ^.*$
    required:
      - code
