{
  "swagger" : "2.0",
  "info" : {
    "description" : "Source systems will invoke this API to notify Transaction Notification Event.",
    "version" : "3.0.0",
    "title" : "ProjectOne APIs - TransactionNotification",
    "license" : {
      "name" : "FedEx Supplychain"
    },
    "x-ibm-name" : "transactionNotifcation"
  },
  "tags" : [ {
    "name" : "transactionNotifcation Interface",
    "description" : "transactionNotifcation Interface"
  } ],
  "schemes" : [ "https" ],
  "consumes" : [ "application/json" ],
  "produces" : [ "application/json" ],
  "paths" : {
    "/v1/transactionNotf" : {
      "post" : {
        "parameters" : [ {
          "name" : "transOperation",
          "in" : "header",
          "description" : "Transaction Operation(CREATE/UPDATE)",
          "required" : true,
          "type" : "string",
          "pattern" : "[a-z A-Z 0-9]"
        }, {
          "name" : "transactionTypeId",
          "in" : "header",
          "description" : "Type of Transaction(RNACK,RNERR,INACK,INERR)",
          "required" : true,
          "type" : "string",
          "pattern" : "[a-z A-Z 0-9]"
        }, {
          "name" : "contentType",
          "in" : "header",
          "description" : "Type of Message(XML/JSON/FLAT)",
          "required" : true,
          "type" : "string",
          "pattern" : "[a-z]"
        }, {
          "name" : "sourceSystemId",
          "in" : "header",
          "description" : "Source where Trasaction is Initiated(Tmobile)",
          "required" : true,
          "type" : "string",
          "pattern" : "[a-z A-Z 0-9]"
        }, {
          "name" : "accessToken",
          "in" : "header",
          "description" : "Access Token(Oauth Generated Token)",
          "required" : true,
          "type" : "string",
          "pattern" : "[a-z A-Z 0-9]{0,14}"
        }, {
          "name" : "customerId",
          "in" : "header",
          "description" : "Customer Number(clientId)",
          "required" : true,
          "type" : "string",
          "pattern" : "[a-z A-Z 0-9]{0,14}"
        }, {
          "name" : "transactionRefNo",
          "in" : "header",
          "description" : "FedEx sent Transaction Reference Number",
          "required" : true,
          "type" : "string",
          "pattern" : "[a-z A-Z 0-9]{0,14}"
        }, {
          "in" : "body",
          "name" : "requestMessage",
          "description" : "",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/transactionNotf"
          },
          "x-required" : true
        } ],
        "responses" : {
          "200" : {
            "description" : "200 OK",
            "schema" : {
              "$ref" : "#/definitions/acknowledgement"
            }
          },
          "400" : {
            "description" : "400 Bad Request",
            "schema" : {
              "$ref" : "#/definitions/badRequest"
            }
          },
          "403" : {
            "description" : "Forbidden",
            "schema" : {
              "type" : "string"
            }
          },
          "404" : {
            "description" : " The Resource Not Found",
            "schema" : {
              "type" : "string"
            }
          },
          "500" : {
            "description" : "Internal Server Error",
            "schema" : {
              "type" : "string"
            }
          },
          "503" : {
            "description" : "Service Unavailable",
            "schema" : {
              "type" : "string"
            }
          }
        }
      }
    }
  },
  "securityDefinitions" : {
    "oauth-1" : {
      "description" : " When you sign up for an account, you are given your first API clientId and secret. use those to get Accesstoken which needs to sent for inbound API",
      "type" : "oauth2",
      "tokenUrl" : "https://integrationqa.supplychain.fedex.com/tmobile/wrapi1/v1/oAuth",
      "flow" : "application"
    }
  },
  "definitions" : {
    "transactionNotf" : {
      "type" : "object",
      "required" : [ "apiName", "eventDate", "eventName", "eventOrigin", "transactionRefNo" ],
      "properties" : {
        "eventDate" : {
          "type" : "string",
          "example" : "2018-08-31",
          "description" : "Date of api invocation",
          "maxLength" : 10,
          "pattern" : "^[0-9]{4}-[0-9]{2}-[0-9]{2}$"
        },
        "eventOrigin" : {
          "type" : "string",
          "example" : "FedEx/SOR",
          "description" : "Source of invocation of the API (FedEx / SOR).",
          "maxLength" : 35,
          "pattern" : "[a-zA-Z0-9]{0,34}"
        },
        "eventName" : {
          "type" : "string",
          "example" : "Receive",
          "description" : "Capability name.",
          "maxLength" : 35,
          "pattern" : "[a-zA-Z0-9]{0,34}"
        },
        "apiName" : {
          "type" : "string",
          "example" : "receive-notification",
          "description" : "Unique name of the API which was invoked.",
          "maxLength" : 50,
          "pattern" : "[a-zA-Z0-9-]{0,49}"
        },
        "invocationType" : {
          "type" : "string",
          "example" : "TransactionLog/ErrorTransactionLog",
          "description" : "An identifier to denote the type of invocation, whether the API is invoked for regular transaction logging or reporting an error scenario.",
          "maxLength" : 35,
          "pattern" : "[a-zA-Z0-9- ]{0,34}"
        },
        "transactionRefNo" : {
          "type" : "string",
          "example" : "1ZY3101E9022178739",
          "description" : "Unique identifier assigned to each transaction of transaction-notification API - unique Id from FedEx",
          "maxLength" : 40,
          "pattern" : "[a-zA-Z0-9]{0,39}"
        },
        "apiTransactionRefNo" : {
          "type" : "string",
          "example" : "1ZY3101E9022178740",
          "description" : "Transaction Reference Number for Capability specific API - unique Id from FedEx",
          "maxLength" : 40,
          "pattern" : "[a-zA-Z0-9]{0,39}"
        },
        "transConfirmNumber" : {
          "type" : "string",
          "example" : "023ec153-9599-4cb6-9374-00ba6e0b23c5",
          "description" : "A unique identifier assigned to each transaction by target system, It helps to distinctively identify transactions in records and monitor transactions",
          "maxLength" : 50,
          "pattern" : "[a-zA-Z0-9\\-]{0,49}"
        },
        "errorCode" : {
          "type" : "string",
          "example" : "TOU-003",
          "description" : "Error code generated at the Target system while processing the input transaction",
          "maxLength" : 35,
          "pattern" : "[a-zA-Z0-9-]{0,34}"
        },
        "errorDescription" : {
          "type" : "string",
          "example" : "Invalid Request",
          "description" : "Description of the error generated at the Target system while processing the input transaction",
          "maxLength" : 500
        },
        "errorSource" : {
          "type" : "string",
          "example" : "SOA/FedEx/ECC",
          "description" : "Target system where the error occurred.",
          "maxLength" : 35,
          "pattern" : "[a-zA-Z0-9]{0,34}"
        },
        "eventAction" : {
          "type" : "string",
          "example" : "Recon",
          "description" : "This will be API operation (Get/Create/Update/Delete/Recon).",
          "maxLength" : 35,
          "pattern" : "[a-zA-Z0-9]{0,34}"
        },
        "eventAttributeList" : {
          "type" : "array",
          "description" : "List of event attributes",
          "items" : {
            "$ref" : "#/definitions/transactionNotf_eventAttributeList"
          }
        }
      },
      "example" : {
        "eventDate" : "2018-08-31",
        "eventOrigin" : "FedEx",
        "eventName" : "Receive",
        "apiName" : "receive-notification",
        "transactionRefNo" : "1ZY3101E9022178739",
        "apiTransactionRefNo" : "1ZY3101E9022178740",
        "transConfirmationNumber" : "93ff724a-223f-4908-9468-d2d5de77a991",
        "errorCode" : "TOU-003",
        "errorDescription" : "Invalid Payload Format",
        "errorSource" : "FedEx",
        "eventAction" : "Recon",
        "eventAttributeList" : {
          "items" : {
            "key" : "serialNumber",
            "value" : "D000000JS7L8"
          }
        }
      }
    },
    "acknowledgement" : {
      "properties" : {
        "interfaceName" : {
          "type" : "string",
          "example" : "Transaction Notification",
          "description" : "Transaction Name",
          "maxLength" : 50
        },
        "sessionId" : {
          "type" : "string",
          "example" : "21212",
          "description" : "Document Number",
          "maxLength" : 50
        },
        "status" : {
          "type" : "string",
          "example" : "success",
          "description" : "Status",
          "maxLength" : 10
        },
        "statusCode" : {
          "type" : "string",
          "example" : "90",
          "description" : "Status Code",
          "maxLength" : 3
        },
        "statusText" : {
          "type" : "string",
          "example" : "Sent to WMS",
          "description" : "Status Description",
          "maxLength" : 50
        },
        "transactionReferenceNo" : {
          "type" : "string",
          "example" : "3321312",
          "description" : "Transaction Reference Number",
          "maxLength" : 50
        }
      },
      "example" : {
        "statusText" : "Sent to wms",
        "transactionReferenceNo" : "3321312",
        "interfaceName" : "sku",
        "sessionId" : "21212",
        "status" : "success",
        "statusCode" : "90"
      },
      "x-required" : [ "sessionId" ]
    },
    "badRequest" : {
      "required" : [ "errorCode", "errorDescription", "transactionStatus" ],
      "properties" : {
        "transactionStatus" : {
          "type" : "string",
          "example" : "91",
          "description" : "Transaction Status",
          "maxLength" : 50
        },
        "errorCode" : {
          "type" : "string",
          "example" : "400",
          "description" : "Error Code",
          "maxLength" : 50
        },
        "errorDescription" : {
          "type" : "string",
          "example" : "Bad Request,InvalidToken",
          "description" : "Error Text",
          "maxLength" : 50
        },
        "transactionReferenceNo" : {
          "type" : "string",
          "example" : "231232",
          "description" : "Transaction Reference Number",
          "maxLength" : 50
        }
      },
      "example" : "[{\"status\":\"Failed\",\"statusCode\":\"91\",\"statusText\":\"BadRequest\",\"transactionReferenceNo\":\"506720\"},{\"status\":\"Failed\",\"statusCode\":\"91\",\"statusText\":\"TokenExpired\"},{\"status\":\"Failed\",\"statusCode\":\"91\",\"statusText\":\"Invalid AccessToken\"}]"
    },
    "transactionNotf_eventAttributeList" : {
      "properties" : {
        "key" : {
          "type" : "string",
          "example" : "serialNumber",
          "description" : "Name of the Attribute",
          "maxLength" : 35,
          "pattern" : "[a-zA-Z0-9]{0,34}"
        },
        "value" : {
          "type" : "string",
          "example" : "D000000JS7L8",
          "description" : "Value of the Attribute",
          "maxLength" : 50
        }
      },
      "description" : "Line Items"
    }
  },
  "x-servers" : [ {
    "url" : "https://integrationqa.supplychain.fedex.com/tmobile/wrapi1/v1/transactionNotf",
    "description" : "Test Server"
  } ],
  "x-ibm-configuration" : {
    "enforced" : true,
    "testable" : true,
    "phase" : "realized"
  },
  "x-ibm-endpoints" : [ {
    "endpointUrl" : "https://integrationqa.supplychain.fedex.com/tmobile/wrapi1/v1/transactionNotf",
    "type" : [ "Test Server" ]
  } ]
}