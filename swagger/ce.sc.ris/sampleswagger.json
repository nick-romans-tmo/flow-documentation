{
  "swagger": "2.0",
  "info": {
    "title": "Store Locator Service",
    "version": "0.0.1",
    "description": "This document defines all of the requirements for the T-Mobile Store Locator API",
    "contact": {
      "name": "Syed Hasnie",
      "email": "syed.hasnie1@t-mobile.com"
    }
  },
  "host": "host.name",
  "basePath": "/basepath",
  "x-servers": [
    {
      "url": "https://{prodSubdomain}.t-mobile.com/{basepath}",
      "description": "Production Server"
    },
    {
      "url": "https://{devSubdomain}.t-mobile.com/{basepath}",
      "description": "Dev Server"
    }
  ],
  "paths": {
    "/search": {
      "get": {
        "tags": [
          "Search Stores"
        ],
        "summary": "Search stores in area based on lat/lng coordinates",
        "description": "By passing in lat/long you can get all the stores within a certain radius",
        "operationId": "getStoreList",
        "security": [],
        "parameters": [
          {
            "in": "query",
            "name": "latitude",
            "type": "number",
            "format": "float",
            "required": true,
            "description": "latitude of a location",
            "x-example": -120.06069
          },
          {
            "in": "query",
            "name": "longitude",
            "type": "number",
            "format": "float",
            "required": true,
            "description": "longitude of a location",
            "x-example": 50.123414
          },
          {
            "in": "query",
            "name": "count",
            "type": "number",
            "format": "integer",
            "minimum": 1,
            "maximum": 50,
            "required": true,
            "description": "number of stores that should be returned"
          },
          {
            "in": "query",
            "name": "radius",
            "type": "number",
            "format": "integer",
            "minimum": 1,
            "maximum": 20,
            "required": true,
            "description": "search distance from the provided lat/long (miles ONLY)"
          }
        ],
        "responses": {
          "200": {
            "description": "Search results matching the criteria",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/store"
              }
            },
            "headers": {}
          },
          "204": {
            "description": "No content found error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "400": {
            "description": "Invalid request error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "500": {
            "description": "Internal server error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        }
      }
    },
    "/stores/{state}": {
      "get": {
        "tags": [
          "State Search"
        ],
        "summary": "get store cities count in given state",
        "description": "By passing in state, you can get list of all cities in state with respective number of stores in that city",
        "operationId": "searchInventoryByState",
        "parameters": [
          {
            "name": "state",
            "in": "path",
            "type": "string",
            "description": "Lowercase abbreviation for state like wa for washington",
            "required": true,
            "x-example": "wa"
          },
          {
            "name": "storeType",
            "in": "query",
            "type": "string",
            "description": "One or More Store types separated by delimiter ','",
            "required": false,
            "x-example": "RETAIL,National Retail"
          }
        ],
        "responses": {
          "200": {
            "description": "Search results based on given state",
            "schema": {
              "$ref": "#/definitions/responseForState"
            }
          },
          "204": {
            "description": "No content found error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "400": {
            "description": "Invalid request error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "500": {
            "description": "Internal server error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        }
      }
    },
    "/stores/{state}/{city}": {
      "get": {
        "tags": [
          "State + City Search"
        ],
        "summary": "get stores in in city of given state",
        "description": "By passing in state and city, you can get list of all stores in city",
        "operationId": "searchInventoryByCity",
        "parameters": [
          {
            "name": "state",
            "in": "path",
            "type": "string",
            "description": "Abbreviation for state",
            "required": true,
            "x-example": "wa"
          },
          {
            "name": "city",
            "in": "path",
            "type": "string",
            "description": "Abbreviation for city",
            "required": true,
            "x-example": "bellevue"
          },
          {
            "name": "storeType",
            "in": "query",
            "description": "One or More Store types separated by delimiter ','",
            "required": false,
            "type": "string",
            "x-example": "RETAIL,National Retail"
          }
        ],
        "responses": {
          "200": {
            "description": "Search results based on given state",
            "schema": {
              "$ref": "#/definitions/responseForStateCity"
            }
          },
          "204": {
            "description": "No content found error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "400": {
            "description": "Invalid request error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "500": {
            "description": "Internal server error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        }
      }
    },
    "/stores/{state}/{city}/{store}": {
      "get": {
        "tags": [
          "Specific Store Search (Deep Link)"
        ],
        "summary": "Get specific store details",
        "description": "By passing in the state/city/storeName you can get all of the details for that particular store",
        "operationId": "getStoreDetails",
        "security": [],
        "parameters": [
          {
            "name": "state",
            "in": "path",
            "type": "string",
            "description": "Abbreviation for state",
            "required": true,
            "x-example": "wa"
          },
          {
            "name": "city",
            "in": "path",
            "type": "string",
            "description": "Abbreviation for city",
            "required": true,
            "x-example": "bellevue"
          },
          {
            "name": "store",
            "in": "path",
            "description": "Store Name",
            "required": true,
            "type": "string",
            "x-example": "4th-Union-2"
          }
        ],
        "responses": {
          "200": {
            "description": "Store details for the specific store requested",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/store"
              }
            }
          },
          "204": {
            "description": "No content found error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "400": {
            "description": "Invalid request error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "500": {
            "description": "Internal server error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        }
      }
    },
    "/reasons": {
      "get": {
        "tags": [
          "Reasons for Get In Line"
        ],
        "summary": "Get list of Reasons for Get In Line",
        "description": "Simple get request to return array of Reasons objects",
        "operationId": "getReasons",
        "security": [],
        "parameters": [],
        "responses": {
          "200": {
            "description": "Array of Reason objects",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/reason"
              }
            }
          },
          "204": {
            "description": "No content found error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "400": {
            "description": "Invalid request error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "500": {
            "description": "Internal server error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        }
      }
    },
    "/waitTime": {
      "get": {
        "tags": [
          "In-Store Wait Time"
        ],
        "summary": "Provides Wait Time Data for TPR/Retail Stores",
        "description": "API exists to fetch the current wait time for all TPR/Retail stores",
        "operationId": "getWaitTime",
        "parameters": [
        ],
        "responses": {
          "200": {
            "description": "Array of Wait Time objects",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/waitTime"
              }
            }
          },
          "204": {
            "description": "No content found error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "400": {
            "description": "Invalid request error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "500": {
            "description": "Internal server error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        }
      }
    },
    "/addCustomer": {
      "post": {
        "tags": [
          "Add Customer (Get In Line"
        ],
        "summary": "Add customer to the line",
        "description": "Send customer details to bluejett service to add customer to the line of a specific store",
        "operationId": "addCustomer",
        "security": [],
        "parameters": [],
        "responses": {
          "200": {
            "description": "Array of wait time objects",
            "schema": {
              "type": "array",
              "items": {
                "type": "object",
                "$ref": "#/definitions/positionInLine"
              }
            }
          },
          "204": {
            "description": "No content found error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "400": {
            "description": "Invalid request error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "500": {
            "description": "Internal server error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "responseForState": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string",
          "example": "washington",
          "description": "state name"
        },
        "abbr": {
          "type": "string",
          "example": "WA",
          "description": "abbreviation for state"
        },
        "contextPath": {
          "type": "string",
          "example": "/store-locator/st",
          "description": "Url pointing to the website of a particular state"
        },
        "cityList": {
          "items": {
            "$ref": "#/definitions/city"
          }
        }
      }
    },
    "responseForStateCity": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string",
          "example": "bellevue",
          "description": "city name"
        },
        "storeCount": {
          "type": "integer",
          "format": "int16",
          "example": 10,
          "description": "Number of stores in city"
        },
        "contextPath": {
          "type": "string",
          "example": "/store-locator/st/city",
          "description": "Url pointing to the website of a particular city"
        },
        "storeList": {
          "items": {
            "$ref": "#/definitions/store"
          }
        }
      }
    },
    "city": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string",
          "example": "bellevue",
          "description": "City Name"
        },
        "displayName": {
          "type": "string",
          "example": "Bellevue",
          "description": "City Name for UI"
        },
        "storeCount": {
          "type": "integer",
          "format": "int16",
          "example": 10,
          "description": "Number of stores in city"
        },
        "url": {
          "type": "string",
          "example": "/store-locator/st/city",
          "description": "Url pointing to the website of a particular city"
        },
        "centerPoint": {
          "$ref": "#/definitions/centerPoint"
        }
      }
    },
    "centerPoint": {
      "properties": {
        "latitude": {
          "type": "number",
          "format": "float",
          "example": 40.467777,
          "description": "Latitude of the store"
        },
        "longitude": {
          "type": "number",
          "format": "float",
          "example": -74.45364,
          "description": "longitude of the store"
        }
      }
    },
    "store": {
      "type": "object",
      "properties": {
        "id": {
          "type": "string",
          "example": "2963A",
          "description": "This is a T-Mobile unique store id."
        },
        "type": {
          "type": "string",
          "format": "enum",
          "example": "RETAIL",
          "description": "Type of store either 'Retail' OR 'National Retail'",
          "enum": [
            "RETAIL",
            "National Retail"
          ]
        },
        "name": {
          "type": "string",
          "example": "T-Mobile North Brunswick",
          "description": "Name of the store"
        },
        "telephone": {
          "type": "string",
          "example": "732-317-9170",
          "description": "Phone number of the store"
        },
        "url": {
          "type": "string",
          "example": "http://www.t-mobile.com/store-locator/st/city/store-name",
          "description": "Url pointing to the website of a particular store"
        },
        "inStoreAppointment": {
          "type": "boolean",
          "description": "Flag for individual store pertaining to whether it offers in-store appointments or not"
        },
        "description": {
          "type": "string",
          "example": "Visit T-Mobile North Brunswick cell phone stores and discover T-Mobile's best smartphones, cell phones, tablets, and internet devices. View our low cost plans with no annual service contracts.",
          "description": "Store description",
          "maxLength": 1024
        },
        "inStoreWaitTime": {
          "type": "string",
          "example": "5",
          "description": "Bluejett value for stores current wait time.  Not always a numeric value!"
        },
        "storeDistance": {
          "type": "number",
          "format": "float",
          "example": 5,
          "description": "Distance [miles] of the store from the given point (lat, long)"
        },
        "photos": {
          "type": "array",
          "description": "Array of photo objects from Yext",
          "items": {
            "$ref": "#/definitions/photo"
          }
        },
        "hours": {
          "type": "array",
          "description": "Array of objects containing store hours for each day of the week",
          "items": {
            "$ref": "#/definitions/day"
          }
        },
        "holidays": {
          "type": "array",
          "description": "List of holiday and/or special event hours",
          "items": {
            "$ref": "#/definitions/holidays"
          }
        },
        "location": {
          "$ref": "#/definitions/location"
        }
      }
    },
    "location": {
      "properties": {
        "utcOffset": {
          "type": "number",
          "format": "integer",
          "example": -5,
          "description": "UTC Timezone offset for the location (e.g., UTC -5)"
        },
        "latitude": {
          "type": "number",
          "format": "float",
          "example": 40.467777,
          "description": "Latitude of the store"
        },
        "longitude": {
          "type": "number",
          "format": "float",
          "example": -74.45364,
          "description": "longitude of the store"
        },
        "address": {
          "$ref": "#/definitions/address"
        }
      }
    },
    "address": {
      "properties": {
        "streetAddress": {
          "type": "string",
          "example": "695 Georges Rd Suite 100",
          "description": "Street address"
        },
        "addressLocality": {
          "type": "string",
          "example": "North Brunswick",
          "description": "City name"
        },
        "addressRegion": {
          "type": "string",
          "example": "NJ",
          "description": "State acronym"
        },
        "postalCode": {
          "type": "string",
          "example": "08902-3330",
          "description": "Zip code"
        }
      }
    },
    "holidays": {
      "properties": {
        "month": {
          "type": "string",
          "example": "December",
          "description": "Month of the year"
        },
        "day": {
          "type": "integer",
          "example": 25,
          "description": "Day within month"
        },
        "eventName": {
          "type": "string",
          "example": "Christmas",
          "description": "Name of the special day"
        },
        "hours": {
          "$ref": "#/definitions/day"
        }
      }
    },
    "day": {
      "properties": {
        "day": {
          "type": "string",
          "example": "Monday",
          "description": "Day of the week"
        },
        "opens": {
          "type": "string",
          "example": "09:00",
          "description": "Time the store opens - 24 hour time"
        },
        "closes": {
          "type": "string",
          "example": "21:00",
          "description": "Time the store closes - 24 hour time"
        }
      }
    },
    "photo": {
      "type": "object",
      "description": "Photo object from Yext",
      "properties": {
        "url": {
          "type": "string",
          "example": "/etc/clientlibs/t-mobile/publish/app/angular_app/assets/images/store-locator/fpo/carousel-store-facade.png",
          "description": "Url for image"
        },
        "description": {
          "type": "string",
          "example": "DAM image url for store photo",
          "description": "Description of this photo"
        },
        "details": {
          "type": "string",
          "example": "Unclear what the point of this property is",
          "description": "Uncertain what the value of this additional property is, however it is in the API docs from Yext"
        },
        "alternateText": {
          "type": "string",
          "example": "Store Photo 1",
          "description": "Alternate Text provided for the photo for use in page"
        },
        "width": {
          "type": "number",
          "format": "integer",
          "example": 65,
          "description": "Width of image in pixels"
        },
        "height": {
          "type": "number",
          "format": "integer",
          "example": 64,
          "description": "Height of image in pixels"
        },
        "derivatives": {
          "type": "array",
          "description": "Array of derivative photos based on the main photo in the object",
          "items": {
            "$ref": "#/definitions/derivative"
          }
        }
      }
    },
    "derivative": {
      "type": "object",
      "description": "Derivative photo object from Yext",
      "properties": {
        "url": {
          "type": "string",
          "example": "https://www.derivative.photo.url",
          "description": "Url for this derivative photo"
        },
        "width": {
          "type": "number",
          "format": "integer",
          "example": 0,
          "description": "Width of this derivative photo"
        },
        "height": {
          "type": "number",
          "format": "integer",
          "example": 0,
          "description": "Height of this derivative photo"
        }
      }
    },
    "positionInLine": {
      "type": "object",
      "properties": {
        "Waittime": {
          "type": "number",
          "format": "integer",
          "description": "Time remaining in line",
          "example": 0
        },
        "PositionInQueue": {
          "type": "number",
          "format": "integer",
          "description": "Customers position in the line",
          "example": 1
        },
        "CustomerName": {
          "type": "string",
          "description": "Customers first and last name",
          "example": "Joe Smith"
        }
      }
    },
    "reason": {
      "type": "object",
      "properties": {
        "ReasonID": {
          "type": "number",
          "format": "integer",
          "description": "Unique ID to identify specific reason",
          "example": 45
        },
        "ReasonEN": {
          "type": "string",
          "description": "Name of reason in English",
          "example": "Accessory"
        },
        "ReasonES": {
          "type": "string",
          "description": "Name of reason in Spanish",
          "example": "Accesorio"
        }
      }
    },
    "waitTime": {
      "type": "object",
      "properties": {
        "StoreID": {
          "type": "string",
          "description": "Unique Store ID (SAP ID)",
          "example": "104D"
        },
        "Waittime": {
          "type": "string",
          "description": "Current wait time in the store, or a status code",
          "example": "5"
        }
      }
    },
    "error": {
      "type": "array",
      "items": {
        "properties": {
          "code": {
            "type": "string"
          },
          "userMessage": {
            "type": "string"
          },
          "systemMessage": {
            "type": "string"
          }
        }
      }
    }
  }
}
