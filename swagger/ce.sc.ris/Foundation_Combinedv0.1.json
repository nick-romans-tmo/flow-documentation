{
	"swagger": "2.0",
	"info": {
		"description": "This capability will manage Sku Master information from T-Mobile Systems to FedEx WMS.",
		"version": "1.0.0",
		"title": "ProjectOne APIs for Foundation Capability",
		"termsOfService": "https://www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsOfUse&print=true",
		"contact": {
			"name": "Created by - Deepesh Shadija Reviewed by - Madhav Muppa (Muppa.Madhav@T-Mobile.com)",
			"email": "Deepesh.Shadija1@T-Mobile.com"
		},
		"license": {
			"name": "T-Mobile",
			"url": "https://www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsOfUse&print=true"
		}
	},
	"host": "tmobileb-sb01.apigee.net",
	"basePath": "/supplychain/foundation/v1",

	"x-servers": [{
			"url": "https://tmobileb-sb01.apigee.net/supplychain/foundation/v1",
			"description": "Sandbox Server"
		},
		{
			"url": "https://api.t-mobile.com/supplychain/foundation/v1",
			"description": "Live Server"
		}
	],
	"tags": [{
		"name": "Foundation",
		"description": "Foundation Capability"
	}],
	"schemes": ["https"],
	"paths": {
		"/skuMaster": {
		"x-api-pattern": "CreateInCollection",
			"get": {
				"tags": [
					"GET API - Retrieval of Sku Master Info"
				],
				"summary": "GET API - Retrieval of Sku Master Info.",
				"description": "GET API - Retrieval of Sku Master Info.",
				"operationId": "getSkuDetailsUsingGET",
				"consumes": [
					"application/json"
				],
				"produces": [
					"application/json"
				],
				"security": [{
					"Oauth": [
						"read:Foundation"
					]
				}],
				"parameters": [
          {
            "name": "Content-Type",
            "in": "header",
            "description": "Type of Message(XML/JSON/FLAT)",
            "required": true,
            "type": "string",
            "pattern": "[a-z]",
			"x-example": "application/json",
			"format":"string"
          },
          {
            "name": "Authorization",
            "in": "header",
            "description": "Access Token",
            "required": true,
            "type": "string",
            "pattern": "[a-zA-Z0-9]{0,14}",
			"x-example": "123456789abc",
			"format":"string"
          },
          {
            "name": "activityid",
            "in": "header",
            "description": "Unique id alphanumeric value generated and sent by the partner to identify every the service request uniquely. Sample: XyzA:D1s5s1i-X6AFRvfiPBfs-1234  Pass as is to downstream calls.",
            "enum": [
              "xyas-1234"
            ],
            "x-example": "TBD",
            "required": true,
            "type": "string",
			"format":"string"
          },
          {
            "name": "skuId",
            "in": "query",
            "description": "SKU ID",
            "required": true,
            "type": "string",
			"maxLength": 50,
			"x-example": "1234",
			"format":"string"
          },
          {
            "name": "transRefNumber",
            "in": "query",
            "description": "A reference number is a unique identifier assigned to each transaction, It helps to distinctively identify transactions in records and monitor transactions",
            "type": "string",
            "maxLength": 40,
            "required": true,
			"format":"String",
			"x-example": "12345asd",
          }
        ],
				"responses": {
					"201": {
						"description": "Created",
						"x-required": false,
						"examples": {
							"application/json": [{
								"code": "201",
								"userMessage": "Sku master Data Fetched Successfully..",
								"systemMessage": "Sku master Data Fetched Successfully.."
							}]
						},
						"schema": {
							"$ref": "#/definitions/SkuMasterGetApiResponse"
						},
						"headers": {
							"Content-Length": {
								"description": "The Content-Length header specifies the actual length of the returned payload.",
								"x-example": "1024",
								"type": "string"
							},
							"Content-Type": {
								"description": "The Content-Type header specifies the actual type of the returned payload.",
								"x-example": "text/xml",
								"type": "string"
							},
							"Location": {
								"description": "The Location header specifies the URI of a created resource, or redirects the API consumer to an alternate resource location.",
								"x-example": "http://api.t-mo.com/order-mgmt/v1/orders/123xyz-0/status",
								"type": "string"
							},
							"servicetransactionid": {
								"description": "Internal identifier for transaction tracking an individual transaction/API request within API platform. Don_t receive/don_t send to downstream",
								"x-example": "111111111111111111111111",
								"type": "string"
							}

						}
					},
					"400": {
						"description": "Bad Request",
						"schema": {
							"$ref": "#/definitions/badRequest"
						},
						"examples": {
							"application/json": [{
									"code": "400",
									"userMessage": "Bad Request",
									"systemMessage": "Bad Request"
								},
								{
									"code": "400",
									"userMessage": "FSM_005 | Missing or Invalid Input Parameters.",
									"systemMessage": "<Detailed/Runtime error message goes here>"
								}
							]
						}
					},
					"401": {
						"description": "Unauthorised Access",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "401",
								"userMessage": "Unauthorised Access",
								"systemMessage": "Unauthorised Access"
							}
						}
					},
					"403": {
						"description": "Forbidden",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "403",
								"userMessage": "Access Denied",
								"systemMessage": "Forbidden"
							}
						}
					},
					"404": {
						"description": "The resource not found",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "404",
								"userMessage": "Resource Not Found",
								"systemMessage": "Resource Not Found"
							}
						}
					},
					"405": {
						"description": "The Method Not Allowed",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "405",
								"userMessage": "Unauthorised Access",
								"systemMessage": "Unauthorised Access"
							}
						}
					},
					"406": {
						"description": "Not Acceptable",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "406",
								"userMessage": "Unauthorised Access",
								"systemMessage": "Unauthorised Access"
							}
						}
					},
					"409": {
						"description": "The Conflict in the resources",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "409",
								"userMessage": "Unauthorised Access",
								"systemMessage": "Unauthorised Access"
							}
						}
					},
					"415": {
						"description": "Unsupported Media Type",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "415",
								"userMessage": "Unauthorised Access",
								"systemMessage": "Unauthorised Access"
							}
						}
					},
					"500": {
						"description": "Internal Server Error",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "500",
								"userMessage": "Internal Server Error",
								"systemMessage": "Internal Server Error"
							}
						}
					},
					"503": {
						"description": "Service Unavailable",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "503",
								"userMessage": "Service Unavailable",
								"systemMessage": "Service Unavailable"
							}
						}
					}
				}
			}
		},
		"/skuchangenotification": {
			"post": {
			"x-api-pattern": "CreateInCollection",
				"tags": [
					"POST API - Posting data into SOA reading from SOR"
				],
				"summary": "API to post data into SOA database",
				"description": "Foundation(SKU Master API) - This API will manage Sku Master information from T-Mobile to WMS.",
				"consumes": [
					"application/json"
				],
				"produces": [
					"application/json"
				],
				"operationId": "postSkuMasterInformation",
				"security": [{
					"Oauth": [
						"read:Foundation"
					]
				}],
				"parameters": [{
						"name": "skuMasterInfo",
						"in": "body",
						"required": true,
						"description": "Request payload",
						"schema": {
							"$ref": "#/definitions/SkuMasterInfo"
						}
					},
					{
						"name": "Accept",
						"in": "header",
						"description": "Content-Types that are acceptable including application/json or application/xml (default assumed application/json)",
						"x-example": "application/json,text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
						"required": false,
						"type": "string"
					},
					{
						"name": "Accept-Charset",
						"in": "header",
						"description": "Character sets that are acceptable.",
						"x-example": "ISO-8859-1,utf-8;q=0.7,*;q=0.7",
						"required": false,
						"type": "string"
					},
					{
						"name": "Accept-Encoding",
						"in": "header",
						"description": "List of acceptable encodings. See HTTP compression.",
						"x-example": "gzip,deflate",
						"required": false,
						"type": "string"
					},
					{
						"name": "Accept-Language",
						"in": "header",
						"description": "List of acceptable human languages for response",
						"x-example": "en-us,en;q=0.5",
						"required": false,
						"type": "string"
					},
					{
						"name": "Authorization",
						"in": "header",
						"description": "OAuth 2.0 access token with the authentication type set as Bearer.",
						"x-example": "Basic 123123123",
						"required": true,
						"type": "string"
					},
					{
						"name": "Content-Length",
						"in": "header",
						"description": "The Content-Length header specifies the actual length of the returned payload.",
						"x-example": "1024",
						"required": false,
						"type": "string"
					},
					{
						"name": "Content-Type",
						"in": "header",
						"description": "The Content-Length header specifies the actual length of the returned payload.",
						"x-example": "text/xml",
						"required": true,
						"type": "string"
					},
					{
						"name": "activityid",
						"in": "header",
						"description": "Unique id alphanumeric value generated and sent by the partner to identify every the service request uniquely. Sample: XyzA:D1s5s1i-X6AFRvfiPBfs-1234  Pass as is to downstream calls.",
						"x-example": "1234",
						"required": true,
						"type": "string"
					},
					{
						"name": "applicationid",
						"in": "header",
						"description": "Identifies the application, system, or tool that is being used to perform the transaction related to this request. Below are the sample values.ACUI, ESERVICE, REBELLION, WARP",
						"x-example": "12345",
						"required": false,
						"type": "string"
					},
					{
						"name": "applicationuserid",
						"in": "header",
						"description": "NT id of the one who is managing the transaction from CARE/RETAIL channels.",
						"x-example": "123456",
						"required": false,
						"type": "string"
					},
					{
						"name": "interactionid",
						"in": "header",
						"description": "Alphanumeric value represent a common transaction id across all calls made from UI, while completing all business activity needs of a particular customer.",
						"x-example": "1234567",
						"required": false,
						"type": "string"
					},
					{
						"name": "channelid",
						"in": "header",
						"description": "Identifies the business unit or sales channel, e.g. Retail",
						"x-example": "12345678",
						"required": false,
						"type": "string"
					},
					{
						"name": "senderid",
						"in": "header",
						"description": "Uniquely identifies an Operation consumer. Below are the sample values.ACUI, MYTMO, QVXP, REBELLION, WALMARTRETAIL, SAMSCLUBRETAIL.",
						"x-example": "12345678",
						"required": false,
						"type": "string"
					},
					{
						"name": "sessionid",
						"in": "header",
						"description": "A value populated by the sender used to track the transactions that occur during a session, a long-lasting interaction that is maintained by the sender.",
						"x-example": "1212",
						"required": false,
						"type": "string"
					},
					{
						"name": "timestamp",
						"in": "header",
						"description": "A timestamp provided by sender to track their workflow. If it is empty APIGEE can send current system timestamp.",
						"x-example": "2018-06-14T16:04:37+00:00",
						"required": false,
						"type": "string",
						"format": "date-time"
					},
					{
						"$ref": "#/parameters/If-MatchParam"
					},
					{
						"$ref": "#/parameters/If-None-MatchParam"
					}
				],
				"responses": {
					"200": {
						"description": "200 OK Success Response",
						"x-required": false,
						"examples": {
							"application/json": [{
								"code": "200",
								"userMessage": "Sku master Data inserted Successfully.",
								"systemMessage": "Sku master Data inserted Successfully."
							}]
						},
						"schema": {
							"$ref": "#/definitions/SkuMasterPostApiResponse"
						},
						"headers": {
							"Content-Type": {
								"description": "The Content-Type header specifies the actual type of the returned payload.",
								"x-example": "application/json",
								"type": "string"
							},
							"Location": {
								"description": "The Location header specifies the URI of a created resource, or redirects the API consumer to an alternate resource location.",
								"x-example": "http://api.t-mo.com/order-mgmt/v1/orders/123xyz-0/status",
								"type": "string"
							}
						}
					},
					"400": {
						"description": "Bad Request",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": [{
									"code": "400",
									"userMessage": "Bad Request",
									"systemMessage": "Bad Request"
								},
								{
									"code": "400",
									"userMessage": "Invalid Request. Please pass valid values in request.",
									"systemMessage": "<Detailed/Runtime error message goes here>"
								}
							]
						}
					},
					"401": {
						"description": "Unauthorised Access",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "401",
								"userMessage": "Unauthorised Access",
								"systemMessage": "Unauthorised Access"
							}
						}
					},
					"403": {
						"description": "Forbidden",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "403",
								"userMessage": "Access Denied",
								"systemMessage": "Forbidden"
							}
						}
					},
					"404": {
						"description": "The resource not found",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "404",
								"userMessage": "Resource Not Found",
								"systemMessage": "Resource Not Found"
							}
						}
					},
					"405": {
						"description": "The Method Not Allowed",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "405",
								"userMessage": "Unauthorised Access",
								"systemMessage": "Unauthorised Access"
							}
						}
					},
					"406": {
						"description": "Not Acceptable",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "406",
								"userMessage": "Unauthorised Access",
								"systemMessage": "Unauthorised Access"
							}
						}
					},
					"409": {
						"description": "The Conflict in the resources",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "409",
								"userMessage": "Unauthorised Access",
								"systemMessage": "Unauthorised Access"
							}
						}
					},
					"415": {
						"description": "Unsupported Media Type",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "415",
								"userMessage": "Unauthorised Access",
								"systemMessage": "Unauthorised Access"
							}
						}
					},
					"500": {
						"description": "Internal Server Error",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "500",
								"userMessage": "Internal Server Error",
								"systemMessage": "Internal Server Error"
							}
						}
					},
					"503": {
						"description": "Service Unavailable",
						"schema": {
							"$ref": "#/definitions/Errors"
						},
						"examples": {
							"application/json": {
								"code": "503",
								"userMessage": "Service Unavailable",
								"systemMessage": "Service Unavailable"
							}
						}
					}
				}
			}
		}
	},
	"definitions": {
    "SkuMasterInfo": {
      "description": " Sku Master Data",
      "type": "object",
      "required": [
        "skuid",
        "skutype",
        "skugroup",
        "skufunction",
        "transactionRefNo"
      ],
      "properties": {
        "skuid": {
          "description": "Stock Keeping Unit Id",
          "type": "string",
          "maxLength": 50,
		  "format":"String",
		  "example":"skuid123"
        },
        "skutype": {
          "description": "Sku Type ",
          "type": "string",
		  "format":"String",
		  "example":"type"
        },
        "skugroup": {
          "description": "Sku Group",
          "type": "string",
          "maxLength": 50,
		  "format": "String",
		  "example":"group"
        },
        "skucategory": {
          "description": "Sku Category",
          "type": "string",
          "maxLength": 50,
		  "format":"String"
        },
        "skucreationdate": {
          "type": "string",
          "format": "date",
          "example": "yyyy-mm-dd",
          "description": "Date of Creation"
        },
        "skucreatedby": {
          "description": "Sku Created by",
          "type": "string",
          "maxLength": 50
        },
        "skulastchange": {
          "description": "Sku last change",
          "type": "string",
          "maxLength": 50
        },
        "skubaseuom": {
          "description": "Sku Category",
          "type": "string",
          "maxLength": 50
        },
        "skudivision": {
          "description": "Sku division",
          "type": "string",
          "maxLength": 50
        },
        "skuvalidfrom": {
          "description": "Sku valid from",
          "type": "string",
          "maxLength": 50
        },
        "skupurgroup": {
          "description": "Sku group",
          "type": "string",
          "maxLength": 50
        },
        "skuvalueclass": {
          "description": "Sku value class",
          "type": "string",
          "maxLength": 50
        },
        "skudescription": {
          "description": "Stock Keeping Unit Description",
          "maxLength": 200,
          "type": "string"
        },
        "skulanguage": {
          "description": "Sku Language",
          "type": "string",
          "maxLength": 5
        },
        "skueanupc": {
          "description": "Sku eanupc",
          "type": "string",
          "maxLength": 50
        },
        "skuunit": {
          "description": "Sku unit",
          "type": "string",
          "maxLength": 50
        },
        "skufunction": {
          "description": "CREATE/CHANGE/DELETE",
          "type": "string",
          "pattern": "[a-zA-Z]{0,14}",
          "enum": ["CREATE", "CHANGE","DELETE"]
        },
        "skucharacterlist": {
          "type": "array",
          "description": "List of Sku Masters character",
          "items": {
            "type": "object",
            "description": "Line Items",
            "properties": {
              "skufunction": {
                "type": "string",
				"description":"sku function",
				"example":"CREATE"
              },
              "key": {
                "type": "string",
				"description":"The key or name of the characteristic of the sku",
				"example":"MAT_OEM"
              },
              "value": {    "type": "string",
				"description":"The value of the characteristic of the sku",
				"example":"MAT_OEM"}
            }
          }
        },
        "skuuomlist": {
          "type": "array",
          "description": "List of Sku Masters unit of measure",
          "items": {
            "type": "object",
            "description": "Line Items",
            "properties": {
              "skualtunit": {
                "type": "string",
				"description":"sku alt unit"
              },
              "skualtunitiso": {
                "type": "string",
				"description":"sku alt unit iso"
              },
              "skuunitconvfrom": {
                "type": "string",
				"description":"sku unit conv from"
              },
              "skuunitconvto": {
                "type": "string",
				"description":"sku unit conv to"
              },
              "skulength": {
                "type": "string",
				"description":"sku length"
              },
              "skuwidth": {
                "type": "string",
				"description":"sku width"
              },
              "skuheight": {
                "type": "string",
				"description":"sku height"
              },
              "skudimunit": {
                "type": "string",
				"description":"sku dim unit"
              },
              "skuweight": {
                "type": "string",
				"description":"sku weight"
              },
              "skuweightunit": {
                "type": "string",
				"description":"sku weight unit"
              }
            }
          }
        },
        "interfaceName": {
          "type": "string",
          "maxLength": 40,
          "description": "This field will show interfaceName"
        },
        "sessionId": {
          "type": "string",
          "maxLength": 100,
          "description": "This field will show session-id"
        },
        "status": {
          "type": "string",
          "maxLength": 25,
          "description": "This field will show API status"
        },
        "statusCode": {
          "type": "integer",
          "format": "int32",
          "description": "This field will show Status Code"
        },
        "statusText": {
          "type": "string",
          "maxLength": 100,
          "description": "This field will show Status text specific to this transaction"
        },
        "transconfirmNumber": {
          "type": "string",
          "maxLength": 50,
          "description": "Transaction Confirmation Number"
        },
        "transactionRefNo": {
          "type": "string",
		  "format":"string",
          "maxLength": 40,
          "description": "A reference number is a unique identifier assigned to each transaction, It helps to distinctively identify transactions in records and monitor transactions",
		  "example":"123456"
        }
      },
      "example": {
        "skuid": "190198786432",
        "skutype": "ZFIN",
        "skugroup": "DATK ",
        "skucategory": "00",
        "skucreationdate": "20180912",
        "skucreatedby": "EWENDT",
        "skulastchange": "20180912",
        "skubaseuom": "EA",
        "skudivision": "01",
        "skuvalidfrom": "20180912",
        "skupurgroup": "005",
        "skuvalueclass": "3005",
        "skudescription": "APL IPHONE XS MAX 256G GLD KIT ",
        "skulanguage": "EN",
        "skueanupc": "190198786432",
        "skuunit": "EA",
        "skufunction": "CREATE",
        "transactionRefNo": "111111111111111111111111111111",
        "transconfirmNumber": "2222222222222222222222222222",
        "skucharacterlist": {
          "items": {
            "key" :"MAT_OEM",
            "value" : "Apple",
            "skufunction" :"CREATE"
          }
        },
        "skuuomlist": {
          "items": {
            "skualtunit": "Unit",
            "skualtunitiso": "string",
            "skuunitconvfrom": "string",
            "skuunitconvto": "string",
            "skulength": "string",
            "skuwidth": "string",
            "skuheight": "string",
            "skudimunit": "string",
            "skuweight": "string",
            "skuweightunit": "string"
          }
        }
      }
    },
		"SkuMasterGetApiResponse": {
      "description": " Sku Master Data",
      "type": "object",
      "required": [
        "skuid",
        "skutype",
        "skugroup",
        "skufunction",
        "transactionRefNo"
      ],
      "properties": {
        "skuid": {
          "description": "Stock Keeping Unit Id",
          "type": "string",
          "maxLength": 50,
		  "format" : "string",
		  "example":"123"
        },
        "skutype": {
          "description": "Sku Type ",
          "type": "string",
		  "format":"string",
		  "example":"ABC123"
        },
        "skugroup": {
          "description": "Sku Group",
          "type": "string",
          "maxLength": 50,
		  "example":"Group",
		  "format":"string"
        },
        "skucategory": {
          "description": "Sku Category",
          "type": "string",
          "maxLength": 50,
		  "example":"Category"
        },
        "skucreationDate": {
          "type": "string",
          "format": "date",
          "example": "yyyy-mm-dd",
          "description": "Date of Creation"
        },
        "skucreatedby": {
          "description": "Sku Created by",
          "type": "string",
          "maxLength": 50,
		  "example":"NTID123"
        },
        "skulastchange": {
          "description": "Sku last change",
          "type": "string",
          "maxLength": 50
        },
        "skubaseuom": {
          "description": "Sku Category",
          "type": "string",
          "maxLength": 50
        },
        "skudivision": {
          "description": "Sku division",
          "type": "string",
          "maxLength": 50
        },
        "skuvalidfrom": {
          "description": "Sku valid from",
          "type": "string",
          "maxLength": 50
        },
        "skupurgroup": {
          "description": "Sku group",
          "type": "string",
          "maxLength": 50
        },
        "skuvalueclass": {
          "description": "Sku value class",
          "type": "string",
          "maxLength": 50
        },
        "skudescription": {
          "description": "Stock Keeping Unit Description",
          "maxLength": 200,
          "type": "string"
        },
        "skulanguage": {
          "description": "Sku Language",
          "type": "string",
          "maxLength": 5
        },
        "skueanupc": {
          "description": "Sku eanupc",
          "type": "string",
          "maxLength": 50
        },
        "skuunit": {
          "description": "Sku unit",
          "type": "string",
          "maxLength": 50
        },
        "skufunction": {
          "description": "CREATE/CHANGE/DELETE",
          "type": "string",
          "pattern": "[a-zA-Z]{0,14}",
          "enum": [
            "CREATE",
            "CHANGE",
            "DELETE"
          ],
		  "example":"CREATE"
        },
        "skucharacterlist": {
          "type": "array",
          "description": "List of Sku Masters character",
          "items": {
            "type": "object",
            "description": "Line Items",
            "properties": {
              "skufunction": {
                "type": "string",
				"example":"CREATE",
				"description":"The function for the characteristic item"
              },
              "key": {
                "type": "string",
				"example":"MAT_OEM",
				"description":"The key or name for the characteristic item"
              },
              "value": {                
			  "type": "string",
				"example":"Apple",
				"description":"The value for the characteristic item"}
            }
          }
        },
        "skuuomlist": {
          "type": "array",
          "description": "List of Sku Masters unit of measure",
          "items": {
            "type": "object",
            "description": "Line Items",
            "properties": {
              "skualtunit": {
                "type": "string",
				"description" : "sku alt unit"
              },
              "skualtunitiso": {
                "type": "string",
				"description" : "sku alt unit iso"
              },
              "skuunitconvfrom": {
                "type": "string",
				"description" : "sku unit conv from"
              },
              "skuunitconvto": {
                "type": "string",
				"description" : "sku unit conv to"
              },
              "skulength": {
                "type": "string",
				"description" : "sku length"
              },
              "skuwidth": {
                "type": "string",
				"description":"sku width"
              },
              "skuheight": {
                "type": "string",
				"description":"sku height"
              },
              "skudimunit": {
                "type": "string",
				"description":"sku dim unit"
              },
              "skuweight": {
                "type": "string",
				"description" : "sku weight"
              },
              "skuweightunit": {
                "type": "string",
				"description":"sku weight unit"
              }
            }
          }
        },
        "interfaceName": {
          "type": "string",
          "maxLength": 40,
          "description": "This field will show interfaceName"
        },
        "sessionId": {
          "type": "string",
          "maxLength": 100,
          "description": "This field will show session-id"
        },
        "status": {
          "type": "string",
          "maxLength": 25,
          "description": "This field will show API status"
        },
        "statusCode": {
          "type": "integer",
          "format": "int32",
          "description": "This field will show Status Code"
        },
        "statusText": {
          "type": "string",
          "maxLength": 100,
          "description": "This field will show Status text specific to this transaction"
        },
        "transconfirmNumber": {
          "type": "string",
          "maxLength": 50,
          "description": "Transaction Confirmation Number"
        },
        "transactionRefNo": {
          "type": "string",
          "maxLength": 40,
          "description": "A reference number is a unique identifier assigned to each transaction, It helps to distinctively identify transactions in records and monitor transactions",
		  "example":"Number1234",
		  "format":"string"
        }
      },
      "example": {
        "example": null,
        "skuid": "190198786432",
        "skutype": "ZFIN",
        "skugroup": "DATK ",
        "skucategory": "00",
        "skucreationdate": "20180912",
        "skucreatedby": "EWENDT",
        "skulastchange": "20180912",
        "skubaseuom": "EA",
        "skudivision": "01",
        "skuvalidfrom": "20180912",
        "skupurgroup": "005",
        "skuvalueclass": "3005",
        "skudescription": "APL IPHONE XS MAX 256G GLD KIT ",
        "skulanguage": "EN",
        "skueanupc": "190198786432",
        "skuunit": "EA",
        "skufunction": "CREATE",
        "transactionRefNo": "111111111111111111111111111111",
        "transconfirmNumber": "2222222222222222222222222222",
        "skucharacterlist": {
          "items": {
            "key": "MAT_OEM",
            "value": "Apple",
            "skufunction": "CREATE"
          }
        },
        "skuuomlist": {
          "items": {
            "skualtunit": "Unit",
            "skualtunitiso": "string",
            "skuunitconvfrom": "string",
            "skuunitconvto": "string",
            "skulength": "string",
            "skuwidth": "string",
            "skuheight": "string",
            "skudimunit": "string",
            "skuweight": "string",
            "skuweightunit": "string"
          }
        }
      }
		},
		"SkuMasterPostApiResponse": {
			"type": "object",
			"description": "Response from sku Master API",
			"required": [
				"transconfirmNumber"
			],
			"properties": {
				"transconfirmNumber": {
					"type": "string",
					"maxLength": 50,
					"description": "A unique identifier assigned to each transaction by target system, It helps to distinctively identify transactions in records and monitor transactions",
					"pattern": "[a-zA-Z0-9\\-]{0,49}",
					"example": "023ec153-9599-4cb6-9374-00ba6e0b23c5"
				},
				"interfaceName": {
					"type": "string",
					"maxLength": 30,
					"description": "This field will show interfaceName",
					"pattern": "[a-zA-Z0-9]{0,29}",
					"example": "API Name - skuMaster"
				},
				"sessionId": {
					"type": "string",
					"maxLength": 100,
					"description": "This field will show session-id",
					"pattern": "[a-zA-Z0-9]{0,99}",
					"example": "SessionId"
				},
				"statusText": {
					"type": "string",
					"maxLength": 100,
					"description": "This field will show Status text specific to this transaction",
					"pattern": "[a-zA-Z0-9]{0,99}",
					"example": "Sku Master record created successfully"
				},
				"transactionRefNo": {
					"type": "string",
					"maxLength": 40,
					"pattern": "[a-zA-Z0-9]{0,39}",
					"description": "A reference number is a unique identifier assigned to each transaction, It helps to distinctively identify transactions in records and monitor transactions",
					"example": "23456678"
				}
			}
		},
		"Errors": {
			"description": "a collection of errors",
			"type": "array",
			"items": {
				"$ref": "#/definitions/ResponseError"
			}
		},

		"ResponseError": {
			"description": "Used to pass error information in a response.",
			"type": "object",
			"properties": {
				"code": {
					"description": "Used to pass error codes",
					"type": "string"
				},
				"userMessage": {
					"description": "Use to pass human friendly information to the user.",
					"type": "string"
				},
				"systemMessage": {
					"description": "Used to pass system information.",
					"type": "string"
				}
			}
		},
		    "badRequest": {
      "required": [
        "errorCode",
        "errorDescription",
        "transactionStatus"
      ],
	  "type": "object",
      "properties": {
        "transactionStatus": {
          "type": "string",
          "example": "91",
          "description": "Transaction Status",
          "maxLength": 50,
		  "format": "String"
        },
        "errorCode": {
          "type": "string",
          "example": "400",
          "description": "Error Code",
          "maxLength": 50,
		  "format": "String"		  
        },
        "errorDescription": {
          "type": "string",
          "example": "Bad Request,InvalidToken",
          "description": "Error Text",
          "maxLength": 50,
		  "format": "String"
        },
        "transactionReferenceNo": {
          "type": "string",
          "example": "231232",
          "description": "Transaction Reference Number",
          "maxLength": 50,
		  "format": "String"
        }
      },
      "example": "[{\"status\":\"Failed\",\"statusCode\":\"91\",\"statusText\":\"BadRequest\",\"transactionReferenceNo\":\"506720\"},{\"status\":\"Failed\",\"statusCode\":\"91\",\"statusText\":\"TokenExpired\"},{\"status\":\"Failed\",\"statusCode\":\"91\",\"statusText\":\"Invalid AccessToken\"}]"
    }
	},

	"parameters": {
		"If-MatchParam": {
			"name": "If-Match",
			"in": "header",
			"description": "The If-Match header specifies an ETag-related condition to be placed on the execution of a request.  The request will only be executed if the ETag of the requested resource matches one of those specified in the If-Match header.  Because the behavior is complex, the details for usage are provided at http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html .",
			"required": false,
			"type": "string",
			"x-example": "xxyyzz"
		},
		"If-None-MatchParam": {
			"name": "If-None-Match",
			"in": "header",
			"description": "The If-None-Match header specifies an ETag-related condition to be placed on the execution of a request.  The request will NOT be executed if the ETag of the requested resource matches one of those specified in the If-None-Match header.  Because the behavior is complex, the details for usage are provided at http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html",
			"required": false,
			"type": "string",
			"x-example": "xxyyzz"
		}
	},
	"securityDefinitions": {
		"Oauth": {
			"type": "oauth2",
			"description": "When you sign up for an account, you are given your first API key.\nTo do so please follow this link: https://www.t-mobile.com/site/signup/\nAlso you can generate additional API keys, and delete API keys (as you may\nneed to rotate your keys in the future).\n",
			"tokenUrl": "https://tmobileb-sb01.apigee.net/v1/oauth2/accesstoken?grant_type=client_credentials",
			"flow": "application",
			"scopes": {
				"read:Foundation": "Foundation - skuMaster."
			}
		}
	}
}