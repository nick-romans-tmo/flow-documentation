{
  "swagger": "2.0",
  "info": {
    "title": "UsageDetails",
    "description": "This service return the Total usage and subscriber level  usage details from Backend. BillPeriod in the input needs to be set to retrieve the usage.",
    "version": "0.0.1",
    "x-createdBy": {
      "dateCreated": "2/22/18 21:55:36",
      "createdBy": "Vijay Maddipati",
      "application": "Swagger tool",
      "appVersion": 2
    }
  },
  "host": "dev.t-mobilapis.com",
  "basePath": "/tmoapp/v1/cards",
  "schemes": [
    "https"
  ],
  "paths": {
    "/line-usage": {
      "post": {
        "tags": [
          "getUsage"
        ],
        "operationId": "getUsage",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "usageRequest",
            "in": "body",
            "description": "User define object to capture account data input",
            "required": true,
            "schema": {
              "$ref": "#/definitions/dataUsageRequest"
            }
          },
          {
            "$ref": "#/parameters/AuthorizationParam"
          },
          {
            "$ref": "#/parameters/XAuthorizationParam"
          },
          {
            "$ref": "#/parameters/sessionidParam"
          },
          {
            "$ref": "#/parameters/interactionidParam"
          },
          {
            "$ref": "#/parameters/activityidParam"
          },
          {
            "$ref": "#/parameters/workflowidParam"
          },
          {
            "$ref": "#/parameters/servicetransactionidParam"
          }
        ],
        "responses": {
          "200": {
            "description": "Ok",
            "schema": {
              "$ref": "#/definitions/usageData"
            },
            "headers": {
              "Access-Control-Allow-Headers": {
                "type": "string"
              },
              "Access-Control-Allow-Methods": {
                "type": "string"
              },
              "Access-Control-Allow-Origin": {
                "type": "string"
              },
              "Cache-Control": {
                "type": "string"
              },
              "Content-Length": {
                "type": "string"
              },
              "Content-Type": {
                "type": "string"
              },
              "ETag": {
                "type": "string"
              },
              "Expires": {
                "type": "string"
              },
              "Location": {
                "type": "string"
              },
              "servicetransactionid": {
                "type": "string"
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "401": {
            "description": "Client not authorized",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "403": {
            "description": "Client access denied",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "404": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "409": {
            "description": "Invalid data",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "502": {
            "description": "Backend system problem",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "503": {
            "description": "Service unavailable",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "dataUsageRequest": {
      "type": "object",
      "description": "User define object to capture account data input",
      "properties": {
        "accountNumber": {
          "description": "Billing Account number",
          "type": "string"
        },
        "phoneNumbers": {
          "description": "List of Mobile Number",
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "roleBasedProfile": {
          "description": "Set to true for Filtered BAN, Set to false for Individual Lines",
          "type": "boolean"
        },
        "globalPassOnlyUsage": {
          "description": "Set to true returning global data pass only usage.Other Usages will not be returned",
          "type": "boolean"
        },
        "isHotspotUsage": {
          "description": "Set to true returning Hotspot pass  usage.Other Usages will not be returned",
          "type": "boolean"
        }
      }
    },
    "usageData": {
      "type": "object",
      "description": "Usage data complex type",
      "properties": {
        "accountInfo": {
          "$ref": "#/definitions/accountInfo"
        },
        "subscriberInfo": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/subscriberInfo"
          }
        }
      }
    },
    "accountInfo": {
      "type": "object",
      "properties": {
        "metadata": {
          "$ref": "#/definitions/metaData"
        },
        "subfacts": {
          "$ref": "#/definitions/accountSubData"
        }
      }
    },
    "accountSubData": {
      "type": "object",
      "properties": {
        "accountNumber": {
          "description": "Billing Account Number",
          "type": "string"
        },
        "numberofLines": {
          "description": "Number of lines on the account",
          "type": "string"
        },
        "usageTotals": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/usageTotals"
          }
        }
      }
    },
    "metaData": {
      "type": "object",
      "properties": {
        "lastUpdateDate": {
          "description": "last update date",
          "type": "string"
        },
        "sensitivity": {
          "description": "Sensitivity of the data",
          "type": "string"
        },
        "source": {
          "description": "source of data",
          "type": "string"
        }
      }
    },
    "subscriberInfo": {
      "type": "object",
      "properties": {
        "metadata": {
          "$ref": "#/definitions/metaData"
        },
        "subfacts": {
          "$ref": "#/definitions/subscriberSubData"
        }
      }
    },
    "subscriberSubData": {
      "type": "object",
      "description": "subscriber information",
      "properties": {
        "phoneNumber": {
          "description": "Phone Number",
          "type": "string"
        },
        "firstName": {
          "description": "Subscriber first name",
          "type": "string"
        },
        "lastName": {
          "description": "Subscriber last name",
          "type": "string"
        },
        "dataPassDetails": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/dataPasses"
          }
        },
        "usageDetails": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/usageInfo"
          }
        },
        "overageDetails": {
          "$ref": "#/definitions/overageInfo"
        }
      }
    },
    "dataPasses": {
      "description": "Data Pass details",
      "type": "object",
      "properties": {
        "passId": {
          "description": "Data Pass ID",
          "type": "string",
          "example": "SNPASS24"
        },
        "expirationDate": {
          "description": "Pass Expiration Date",
          "type": "string",
          "example": "2017-08-11"
        }
      }
    },
    "usageTotals": {
      "description": "usage totals of all the lines.",
      "type": "object",
      "properties": {
        "bucketType": {
          "description": "Type of bucket",
          "type": "string"
        },
        "totalUsed": {
          "description": "Total usage of the bucket",
          "type": "string"
        }
      }
    },
    "usageInfo": {
      "description": "returns the data usage information of the subscriber",
      "type": "object",
      "properties": {
        "bucketType": {
          "description": "Buket Type like SMS, Voice etc",
          "type": "string"
        },
        "bucketUsed": {
          "description": "Quantity used like in bytes(Data), minutes(voice) etc",
          "type": "string"
        },
        "bucketLimit": {
          "description": "Total limit for the billing cycle",
          "type": "string"
        },
        "bucketIsUnLimited": {
          "description": "Flag indicates whether the bucket is limited or unlimited",
          "type": "boolean"
        },
        "usedPercentage": {
          "description": "Percentage of usage. Returned if hotspot indicator is set to true",
          "type": "string"
        }
      }
    },
    "overageInfo": {
      "description": "returns the overage information of the subscriber",
      "type": "object",
      "properties": {
        "dataVolume": {
          "description": "Overage data",
          "type": "integer"
        },
        "overageMessages": {
          "description": "Overage messages",
          "type": "integer"
        },
        "overageMinutes": {
          "description": "Overage messages",
          "type": "integer"
        },
        "premiumCharge": {
          "type": "integer"
        },
        "roamCharge": {
          "type": "integer"
        },
        "IdTollIntCharge": {
          "type": "integer"
        }
      }
    },
    "errors": {
      "description": "a collection of errors",
      "type": "array",
      "items": {
        "$ref": "#/definitions/responseerror"
      }
    },
    "responseerror": {
      "description": "Used to pass error information in a response.",
      "type": "object",
      "properties": {
        "code": {
          "description": "Used to pass error codes",
          "type": "string"
        },
        "usermessage": {
          "description": "Use to pass human friendly information to the user.",
          "type": "string"
        },
        "systemmessage": {
          "description": "Used to pass system information.",
          "type": "string"
        }
      }
    }
  },
  "parameters": {
    "AuthorizationParam": {
      "name": "Authorization",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "XAuthorizationParam": {
      "name": "X-Authorization",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "sessionidParam": {
      "name": "sessionid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "interactionidParam": {
      "name": "interactionid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "activityidParam": {
      "name": "activityid",
      "in": "header",
      "required": true,
      "type": "string"
    },
    "workflowidParam": {
      "name": "workflowid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "servicetransactionidParam": {
      "name": "servicetransactionid",
      "in": "header",
      "required": false,
      "type": "string"
    }
  }
}