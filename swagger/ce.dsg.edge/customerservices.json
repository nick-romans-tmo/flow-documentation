{
  "swagger": "2.0",
  "info": {
    "title": "customerservicedetails",
    "description": "This service returns the customer and service details from edge. The API accepts BAN/ MSISDN in input and returns the data found in edge. - TEST",
    "version": "0.0.1",
    "x-createdBy": {
      "dateCreated": "2/15/17 12:32:32",
      "createdBy": "Manoj Sukumaran",
      "application": "Swagger Tool",
      "appVersion": 2
    }
  },
  "host": "dev.t-mobileapis.com",
  "basePath": "/commerce/v1/customers",
  "schemes": [
    "https"
  ],
  "paths": {
    "/v1/dcp/account": {
      "post": {
        "tags": [
          "getCustomerServicesData"
        ],
        "consumes": [
          "applicaiton/json"
        ],
        "produces": [
          "application/json"
        ],
        "description": "This resource gets customer,subscriber information from Edge Cache",
        "operationId": "searchCustomer",
        "parameters": [
          {
            "name": "dataRequest",
            "in": "body",
            "description": "Data request to query the customer and Services details. This API accepts BAN/ MSISDN.  If queried based on BAN then all the MSISDNs under the BAN will be returned. If queried based on BAN - MSISDN in input then the details for that MSISDN along with BAN details will be returned. standalone MSISDN is not accepted in input.",
            "required": true,
            "schema": {
              "$ref": "#/definitions/customerDataRequest"
            }
          },
          {
            "$ref": "#/parameters/AuthorizationParam"
          },
          {
            "$ref": "#/parameters/XAuthorizationParam"
          },
          {
            "$ref": "#/parameters/sessionidParam"
          },
          {
            "$ref": "#/parameters/interactionidParam"
          },
          {
            "$ref": "#/parameters/activityidParam"
          },
          {
            "$ref": "#/parameters/workflowidParam"
          },
          {
            "$ref": "#/parameters/servicetransactionidParam"
          }
        ],
        "responses": {
          "200": {
            "description": "oK",
            "schema": {
              "$ref": "#/definitions/customerDataResponse"
            },
            "headers": {
              "Access-Control-Allow-Headers": {
                "type": "string"
              },
              "Access-Control-Allow-Methods": {
                "type": "string"
              },
              "Access-Control-Allow-Origin": {
                "type": "string"
              },
              "Cache-Control": {
                "type": "string"
              },
              "Content-Length": {
                "type": "string"
              },
              "Content-Type": {
                "type": "string"
              },
              "ETag": {
                "type": "string"
              },
              "Expires": {
                "type": "string"
              },
              "Location": {
                "type": "string"
              },
              "servicetransactionid": {
                "type": "string"
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "401": {
            "description": "Client not authorized",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "403": {
            "description": "Client access denied",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "404": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "409": {
            "description": "Invalid data",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "502": {
            "description": "Backend system problem",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "503": {
            "description": "Service unavailable",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "customerDataRequest": {
      "description": "Input requests",
      "type": "object",
      "properties": {
        "accountNumber": {
          "description": "Billing Account Number",
          "type": "string"
        },
        "phoneNumber": {
          "description": "MSISDN - Phone Number",
          "type": "string"
        }
      }
    },
    "customerDataResponse": {
      "description": "Response requests",
      "type": "object",
      "properties": {
        "accountNumber": {
          "description": "Billing Account Number",
          "type": "string"
        },
        "status": {
          "description": "status of the customer",
          "type": "string"
        },
        "accountType": {
          "description": "Account Type of the Customer",
          "type": "string"
        },
        "accountSubType": {
          "description": "Account Sub Type of the customer",
          "type": "string"
        },
        "creditclass": {
          "description": "Credit class of customer",
          "type": "string"
        },
        "creditRiskProfileId": {
          "description": "Credit Risk Profile Indicator",
          "type": "string"
        },
        "customerTenure": {
          "description": "Credit Tenure with T-Mobile in days",
          "type": "string"
        },
        "billCycleCode": {
          "description": "BillCycle Code of the BAN",
          "type": "string",
          "example": 21
        },
        "subMarketCode": {
          "description": "sub market code of BAN",
          "type": "string",
          "example": "SEW"
        },
        "taxTreatment": {
          "description": "tax Treatment possible values TI/TE",
          "type": "string",
          "example": "TI"
        },   
        "affiliationType": {
          "description": "Customer Segement or affiliation Type",
          "type": "string",
          "example": "MILITARY"
        },             
        "billingDetails": {
          "$ref": "#/definitions/nameAddress"
        },
        "accountSubscriptions": {
          "$ref": "#/definitions/subscriptions"
        },
        "susbcribersData": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/subscriberData"
          }
        }
      }
    },
    "subscriberData": {
      "type": "object",
      "properties": {
        "phoneNumber": {
          "description": "MSISDN or phone number",
          "type": "string"
        },
        "status": {
          "description": "status of the subscriber",
          "type": "string"
        },
        "lineType": {
          "description": "line Type of the subscriber, possible values VOICE, DATA, DUPLICATE,VIRTUAL, FAX, IVR_AND_CONFERENCE",
          "type": "string",
          "example": "DUPLICATE"
        },
        "pairedPrimary": {
          "description": "paired primary line of duplicate line",
          "type": "string",
          "example": "4251021022"
        },                
        "subscriptions": {
          "$ref": "#/definitions/subscriptions"
        }
      }
    },
    "subscriptions": {
      "description": "subscriptions (features) of BAN/MSISDN",
      "type": "object",
      "properties": {
        "rateplanId": {
          "description": "rateplan ID",
          "type": "string"
        },
        "displayName": {
          "description": "display name of the rateplan",
          "type": "string"
        },
        "category": {
          "description": "Rateplan Indicator",
          "type": "string"
        },
        "productType": {
          "description": "product Type",
          "type": "string"
        },
        "effectiveDate": {
          "description": "effective Date of Rateplan",
          "type": "string"
        },
        "features": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/features"
          }
        }
      }
    },
    "features": {
      "description": "feature list",
      "type": "object",
      "properties": {
        "code": {
          "description": "feature code",
          "type": "string"
        },
        "displayName": {
          "description": "Display Name",
          "type": "string"
        },
        "category": {
          "description": "SOC indicator",
          "type": "string"
        },
        "effectiveDate": {
          "description": "Effective Date of Feature",
          "type": "string"
        }
      }
    },
    "nameAddress": {
      "description": "Name and Address",
      "type": "object",
      "properties": {
        "firstName": {
          "description": "First Name",
          "type": "string"
        },
        "familyName": {
          "description": "Family Name",
          "type": "string"
        },
        "addressLine1": {
          "description": "Address Line1",
          "type": "string"
        },
        "addressLine2": {
          "description": "Address Line2",
          "type": "string"
        },
        "addressLine3": {
          "description": "Address Line3",
          "type": "string"
        },
        "addressLine4": {
          "description": "Address Line4",
          "type": "string"
        },
        "cityName": {
          "description": "city Name",
          "type": "string"
        },
        "stateCode": {
          "description": "State Code",
          "type": "string"
        },
        "postalCode": {
          "description": "Postal Code",
          "type": "string"
        }
      }
    },
    "errors": {
      "description": "a collection of errors",
      "type": "array",
      "items": {
        "$ref": "#/definitions/responseerror"
      }
    },
    "responseerror": {
      "description": "Used to pass error information in a response.",
      "type": "object",
      "properties": {
        "code": {
          "description": "Used to pass error codes",
          "type": "string"
        },
        "usermessage": {
          "description": "Use to pass human friendly information to the user.",
          "type": "string"
        },
        "systemmessage": {
          "description": "Used to pass system information.",
          "type": "string"
        }
      }
    }
  },
  "parameters": {
    "AuthorizationParam": {
      "name": "Authorization",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "XAuthorizationParam": {
      "name": "X-Authorization",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "sessionidParam": {
      "name": "sessionid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "interactionidParam": {
      "name": "interactionid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "activityidParam": {
      "name": "activityid",
      "in": "header",
      "required": true,
      "type": "string"
    },
    "workflowidParam": {
      "name": "workflowid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "servicetransactionidParam": {
      "name": "servicetransactionid",
      "in": "header",
      "required": false,
      "type": "string"
    }
  }
}
