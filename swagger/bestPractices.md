# Swagger Best Practices

## Overview

This is a loose guide around Swagger best practices.  Specific standards are forthcoming, and those standards will begin to be codified in lint tools, but ahead of that rigor, this document captures the thought process to aid the author in making the best decisions.


## Decisioning Framework

1. Assume no other documentation is available to describe your API.  Can it be clearly understood by your client?
2. Have you considered the appropriate privacy and security implications of the API?  
3. Do you have a large number of optional fields as input?  Is that appropriate (i.e., filters/search criteria), or should additional APIs be considered, instead?
4. Remember that you cannot ever change your API once released, because that violates the core tenet of loose coupling (forces consumers to change, as well).  Think in terms of versions, you can add as many versions as you want over time (as long as you continue supporting the older versions).


## Guidelines

1. All fields should use the appropriate type.
2. Be as verbose as possible in your description fields.
3. Favor verbose description fields over comments in the YAML.
4. When using a `string` field, is there a suitable `format` available?  -> Formats defined [here](https://swagger.io/docs/specification/data-models/data-types/) 
5. When using a `string` field, always specify a maximum length.  Even if you don't have a hard boundary (i.e., database field size), this will aid in preventing some forms of fuzzing attacks by presenting a narrower surface.
6. When using a `string` field, try to include a `pattern` if possible so that the validation is present in the API spec itself.  RegEx is your friend.
7. Always include sample request/responses in your specification.
8. Be clear about the `Content-type` and `Accepts` mime types.  `application/json` is the norm, but others may be used, as appropriate.
9. Ensure the headers for your API call include enough detail to allow your API do perform authentication and authorization.  Apigee is only part of the equation, your service must re-validate.
10. ENUMs help with defining a clear API.  Adding ENUMs over time can be done without versioning the API, since this won't force a client to update.  Removing ENUMs breaks backwards compatibility, and requires a new version of the API.


## Additional Information

- There is guidance on [event swaggers](https://qwiki.internal.t-mobile.com/pages/viewpage.action?title=Event+Swagger+guidelines+and+best+practices&spaceKey=DIGEE).

