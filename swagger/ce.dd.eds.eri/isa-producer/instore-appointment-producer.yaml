swagger: '2.0'
info:
  description: |-
    The API publishes the InStrAppt requests data to MSP queue, the consumer processes this data and writes it to Selligent API. Clients can integrate with this API to publish the contact history of the campaigns to T-Mobile.  This document will briefly cover terminology and concepts of the API.  

     * Client consumes this API and publishes the contact history data in JSON format to MSP queues.  
     * The API validates the user name and password provided in the API request header, if authentication fails the service sends response back to client with status code 401 Unautherized.  
     * The API validates the JSON format and the required fields in the request.  If the validation of the data fails then the service sends response back to client with status code 400 Bad request.  
     * If the authentication and validation of the required fields are successful, it writes the message to the queue based on the routing key.  
     * Then the service sends response back to client with status code 200 Success after successfully writing the message to the queue.
  version: '1.0'
  title: InStroreAppointment Producer
  contact:
    email: BTSIteam@t-mobile.com
host: dev.instore-appointment-producer.eri.t-mobile.com
basePath: /
schemes:
  - https
paths:
  /publish:
    post:
      tags:
        - in-store-appointment-controller
      summary: returns status code
      description: Publish InStrAppt messages
      operationId: publishContactHistoryDataUsingPOST
      consumes:
        - application/json
      produces:
        - '*/*'
      parameters:
        - in: body
          name: model
          description: model
          required: true
          schema:
            $ref: '#/definitions/InStoreAppointmentModel'
        - name: Authorization
          in: header
          description: The HTTP Authorization request header contains the credentials to authenticate a user agent with a server.
          x-example: Bearer mF_9.B5f-4.1JqM
          required: true
          type: string
          minLength: 1
          maxLength: 256
          pattern: '^[\S ]+$'
      responses:
        '200':
          description: OK
          schema:
            type: string
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
securityDefinitions:
  Oauth2:
    type: oauth2
    description: oauth access token
    scopes: {}
    flow: application
    tokenUrl: 'https://tmobilea-sb02.apigee.net/v1/oauth2/accesstoken?grant_type=client_credentials'
definitions:
  InStoreAppointmentModel:
    type: object
    properties:
      appointmentEvents:
        type: array
        items:
          $ref: '#/definitions/InStoreAppointmentModelLineItem'
  InStoreAppointmentModelLineItem:
    type: object
    required:
      - appointmentDate
      - appointmentDayOfWeek
      - appointmentStartTime
      - appointmentStatus
      - appointmentYear
      - campaignId
      - cycleId
      - deliveryChannel
      - email
      - eventDateTime
      - firstName
      - keys
      - languageCode
      - lastName
      - purpose
      - responseTrackingCode
      - sourceChannel
      - sourceRecordId
      - sourceSystem
      - storeAddress
      - storeId
      - storePhoneNumber
      - treatmentId
      - url
      - vendorName
    example:
      appointmentDate: "October 30"
      appointmentDayOfWeek: "Friday"
      appointmentStartTime: "09:00AM"
      appointmentStatus: "NoShow_cart"
      appointmentYear: "2018"
      campaignId: 123
      cycleId: 1
      deliveryChannel: "email"
      email: "srikanthannadi@gmail.com"
      eventDateTime: "2017-07-28 14:34:25.000"
      firstName: "LeBron"
      keys: [{"name":"cart_Url","value":"http://www.google.com"},{"name":"cart_Type","value":"Retail"}]
      languageCode: "en"
      lastName: "James"
      purpose: "appointment"
      responseTrackingCode: "SFDC12345"
      sourceChannel: "web"
      sourceRecordId: "SFDCLEADID123"
      sourceSystem: "SFDC_B2B"
      storeAddress: "1527 6th Ave | Seattle, WA 98006"
      storeId: "12345"
      storePhoneNumber: "(123)-456-7899"
      treatmentId: 7
      url: "https://uatent.digital.t-mobile.com/store-locator/?appointment_Id=00Q2a000003IMcjEAG"
      vendorName: "Selligent"
    properties:
      appointmentDate:
        type: string
      appointmentDayOfWeek:
        type: string
      appointmentStartTime:
        type: string
        example: 'hh:mmaa'
      appointmentStatus:
        type: string
      appointmentYear:
        type: integer
        format: int32
      campaignId:
        type: integer
        format: int32
      cycleId:
        type: integer
        format: int32
      deliveryChannel:
        type: string
        pattern: "(email|sms|email_sms)"
      email:
        type: string
        description: Email address for contact.
        pattern: '^[a-zA-Z0-9.!#$%&''*+/=?^_`{|}~-]+@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'
      eventDateTime:
        type: string
        example: 'yyyy-MM-dd HH:mm:ss.SSS'
      firstName:
        type: string
      keys:
        type: array
        items:
          $ref: '#/definitions/Keys'
      languageCode:
        type: string
      lastName:
        type: string
      msisdn:
        type: string
        pattern:  "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$"
      purpose:
        type: string
        pattern: "(Confirmation|Modification|Reminder|Cancel)"
      responseTrackingCode:
        type: string
      sourceChannel:
        type: string
      sourceRecordId:
        type: string
      sourceSystem:
        type: string
      storeAddress:
        type: string
      storeId:
        type: string
      storePhoneNumber:
        type: string
      treatmentId:
        type: integer
        format: int32
      url:
        type: string
        pattern: "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"
      vendorName:
        type: string
  Keys:
    type: object
    properties:
      name:
        type: string
      value:
        type: string
