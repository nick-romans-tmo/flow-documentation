{
    "swagger": "2.0",
    "info": {
        "title": "dfs-lib-auditlog",
        "description": "This service will extract/transform request/response information with key/values cofigured. Initial version.",
        "version": "1.0.0",
        "contact": {
            "name": "Anil Kumar Mann",
            "email": "AnilKumar.Mann8@T-Mobile.com"
        },
        "x-createdBy": {
            "dateCreated": "Thu Oct 24 23:02:08 2019",
            "createdBy": "gshyand1",
            "application": "Recite",
            "appVersion": "1.2.1.3071",
            "documentId": "9c8fd6ac-5879-4fc3-bdd3-7129feca2a1d",
            "status": "Conceptual - Initial",
            "businessCapabilityModel": "5.0",
            "classification": "6.0 Customer Credit & Financing Management",
            "profile": "Core Business Capability Service",
            "serviceLayer": "Enterprise - CustomerManagement"
        }
    },
    "tags": [
        {
            "name": "CreateInCollection",
            "description": "The CreateInCollection design pattern as defined by T-Mobile Digital Architecture."
        }
    ],
    "basePath": "/device-finance/v2",
    "schemes": [
        "https"
    ],
    "definitions": {
        "AuditLogRequest": {
            "description": "Request object",
            "type": "object",
            "required": [
                "transactionId",
                "serviceName",
                "operationName",
                "eventType",
                "elapsedTime",
                "statusCode"
            ],
            "properties": {
                "transactionId": {
                    "type": "string",
                    "description": "TraceId/TransactionId/GUID, however, it should be same for services (including consumer/producers) during transaction."
                },
                "systemName": {
                    "type": "string",
                    "description": "This refer to the team name. Current system name in case service itself is logging request received or response sent. Backend System Name while calling backend service. EIP, FRONT_OFFICE, OFSLL_PROXY, OFSLL, SERVICING_UI",
                    "example": "DFS_SAGA"
                },
                "serviceName": {
                    "type": "string",
                    "description": "Service Name",
                    "example": "dfs-saga-loanestimate"
                },
                "operationName": {
                    "type": "string",
                    "description": "There might be one or multiple operation supported by service.",
                    "example": "loan-bundle-quotes"
                },
                "eventType": {
                    "type": "string",
                    "description": "This refer to ServiceRequest, ServiceResponse, OFSLLProxyRequest, OFSLLProxyResponse, OFSLLRequest, OFSLLResponse, UlidRequest, UlidResponse, ChubRequest, ChubResponse, SamsonRequest, SamsonResponse. This should standardized names and follow title case without space or underscore as provided in example",
                    "example": "ServiceRequest"
                },
                "url": {
                    "type": "string",
                    "description": "Current Server URL or Backend System URL"
                },
                "elapsedTime": {
                    "type": "integer",
                    "format": "int32",
                    "minimum": -2147483648,
                    "maximum": 2147483647,
                    "pattern": "^[\\-+]?[0-9]+$",
                    "description": "Elapsed time in milliseconds"
                },
                "statusCode": {
                    "type": "integer",
                    "format": "int32",
                    "minimum": 100,
                    "maximum": 599,
                    "pattern": "^[\\-+]?[0-9]+$",
                    "description": "Valid HTTP Status code"
                },
                "header": {
                    "type": "string",
                    "description": "Header values received by a server or sent to backend client"
                },
                "body": {
                    "type": "string",
                    "description": "Request Body received by server or sent to backend"
                },
                "timestamp": {
                    "type": "string",
                    "format": "date-time",
                    "description": "Request Body received by server or sent to backend"
                }
            }
        },
        "AuditLogResponse": {
            "description": "Response object",
            "type": "object",
            "properties": {
                "status": {
                    "type": "string",
                    "description": "Status of the operation."
                }
            }
        },
        "Error": {
            "description": "As defined in http://api-standards.apps.px-npe01.cf.t-mobile.com/http/status-codes/error-response-format/",
            "type": "object",
            "required": [
                "code",
                "userMessage"
            ],
            "properties": {
                "code": {
                    "type": "string",
                    "pattern": "^[\\S ]+$",
                    "description": "A succinct, domain-specific, human-readable text string to identify the type of error for the given status code",
                    "example": "ProductNotFound"
                },
                "userMessage": {
                    "type": "string",
                    "pattern": "^[\\S ]+$",
                    "description": "A human-readable message describing the error.",
                    "example": "Failed to find product."
                },
                "systemMessage": {
                    "type": "string",
                    "description": "Text that provides a more detailed technical explanation of the error",
                    "example": "PRODUCT_NOT_FOUND"
                },
                "detailLink": {
                    "type": "string",
                    "description": "link to custom information providing greater detail on error or errors",
                    "example": "http://aaa.bbb.ccc/"
                }
            }
        }
    },
    "paths": {
        "/auditlog": {
            "post": {
                "tags": [
                    "CreateInCollection"
                ],
                "x-api-pattern": "CreateInCollection",
                "summary": "Insert audit log information",
                "description": "Operation provides a way to insert in new audit log tables",
                "operationId": "log",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "auditLogRequest",
                        "in": "body",
                        "description": "Request object",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/AuditLogRequest"
                        }
                    },
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "The HTTP Authorization request header contains the credentials to authenticate a user agent with a server.",
                        "x-example": "Bearer mF_9.B5f-4.1JqM",
                        "required": true,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S ]+$"
                    },
                    {
                        "name": "Content-Type",
                        "in": "header",
                        "description": "The MIME type of this content",
                        "x-example": "text/xml",
                        "required": true,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[ \\S]+$"
                    },
                    {
                        "name": "x-auth-originator",
                        "in": "header",
                        "description": "API chain initiating callers access token",
                        "x-example": "c34e7acd-384b-4c22-8b02-ba3963682508",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "x-authorization",
                        "in": "header",
                        "description": "Contains Proof of Possession token generated by API caller",
                        "x-example": "c34e7acd-384b-4c22-8b02-ba3963682508",
                        "required": true,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S ]*$"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success Response",
                        "schema": {
                            "$ref": "#/definitions/AuditLogResponse"
                        }
                    },
                    "400": {
                        "description": "Not a valid Request.",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "400",
                                "userMessage": "Not a valid Request.",
                                "systemMessage": "Not a valid Request.",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "401": {
                        "description": "Unauthorized",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "401",
                                "userMessage": "Unauthorized",
                                "systemMessage": "Unauthorized",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "403": {
                        "description": "Client access denied",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "403",
                                "userMessage": "Client access denied",
                                "systemMessage": "Client access denied",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "404": {
                        "description": "Resource not found",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "404",
                                "userMessage": "Resource not found",
                                "systemMessage": "Resource not found",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "405": {
                        "description": "Method Not Allowed",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "405",
                                "userMessage": "Method Not Allowed",
                                "systemMessage": "Method Not Allowed",
                                "detailLink": "http://www.tmus.com"
                            }
                        },
                        "headers": {
                            "Allow": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 256,
                                "pattern": "^[\\S]*$",
                                "description": "list of supported methods for URI",
                                "x-example": "GET"
                            },
                            "service-transaction-id": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 256,
                                "pattern": "^[\\S]*$",
                                "description": "Request ID echoed back by server",
                                "x-example": "23409209723"
                            }
                        }
                    },
                    "406": {
                        "description": "Mismatching data format",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "406",
                                "userMessage": "Mismatching data format",
                                "systemMessage": "Mismatching data format",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "409": {
                        "description": "Invalid data",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "409",
                                "userMessage": "Invalid data",
                                "systemMessage": "Invalid data",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "415": {
                        "description": "Unsupported Media Type",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "415",
                                "userMessage": "Unsupported Media Type",
                                "systemMessage": "Unsupported Media Type",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "500": {
                        "description": "Error while storing data in database",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "500",
                                "userMessage": "Error while storing data in database",
                                "systemMessage": "Error while storing data in database",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "503": {
                        "description": "Service unavailable",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "503",
                                "userMessage": "Service unavailable",
                                "systemMessage": "Service unavailable",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    }
                },
                "security": [
                    {
                        "oauth2-application": [
                            "user",
                            "financial",
                            "read:customer",
                            "read:StoreLocation"
                        ]
                    }
                ]
            }
        }
    },
    "securityDefinitions": {
        "oauth2-application": {
            "type": "oauth2",
            "description": "When you sign up for an account, you are given your first API key. To do so please follow this link: https://www.t-mobile.com/site/signup/ Also you can generate additional API keys, and delete API keys (as you may need to rotate your keys in the future).",
            "tokenUrl": "https://tmobileb-sb01.apigee.net/v1/oauth2/accesstoken?grant_type=client_credentials",
            "flow": "application",
            "scopes": {
                "user": "Grants read/write access to profile info only.",
                "financial": "Grants read/write access to financial transactions",
                "read:customer": "read only access to customer",
                "read:StoreLocation": "Get store based on geo location or address"
            }
        }
    }
}