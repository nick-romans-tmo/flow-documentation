swagger: '2.0'
info:
  version: 2.0.0
  description: Service promotions FPD Eligibility API
  title: Service promotions FPD Eligibility API
  contact:
    name: Promo Service DevOps
    email: Amdocs_DevOps_SPE@T-Mobile.com
  license:
    name: Proprietary software
    url: "https://en.wikipedia.org/wiki/Proprietary_software"
  termsOfService: "https://www.t-mobile.com/templates/popup.aspx?PAsset=Ftr_Ftr_TermsAndConditions"
  x-createdBy:
    dateCreated: "Fri Dec 28 10:40:10 2018"
    createdBy: Promo service team
    status: Production - Active
    classification: 1.4 Customer Promotion Presentment
    profile: Core Business Capability Service
    serviceLayer: Resource - Other
x-servers:
  - url: "https://promo-service-fpd-elig-v1-apps.apps.px-npe01.cf.t-mobile.com/"
    description: Test Server 1

host: promo-service-fpd-elig-v1-apps.apps.px-npe01.cf.t-mobile.com
basePath: /product-delivery/v2/promotions/service
tags:
- name: promo-service-fpd-elig-v1
  description: Service promotions FPD Eligibility
schemes: 
  - https
paths:
  /fpd-eligibility:
    get:
      tags:
        - promo-service-fpd-elig-v1
      x-api-pattern: QueryCollection
      summary: >-
        This API returns service promotions eligibility for the given BAN.
      description: >-
        API will be invoked with all the mandatory fields to return eligible service promotions.
      operationId: getPromoEligibility
      security:
        - Oauth: []
      consumes:
      - application/json
      produces:
      - application/json
      parameters:
      - name: ban
        in: query
        description: >-
          Billing Account number for which eligibility needs to be checked. 
          This field will be provided in base64 format.
        required: true
        type: string
        x-example: OTUyMDI4NjY2
        minLength: 1
        maxLength: 256
        pattern: '^[\S]*$'
      - name: super-user-ind
        in: query
        description: >-
          Super User Indicator.If value is 'true' then user is considerd as super user otherwise permissioned user.
        required: true
        type: boolean
        x-example: true
      - name: grace-period
        in: query
        description: >-
          Grace period which needs to be considerd in days.
        required: true
        type: integer
        format: int32
        x-example: 10
        minimum: 0
        maximum: 2147483647
      - name: Accept
        in: header
        description: Content-Types that are acceptable including application/json or application/xml (default assumed application/json)
        required: false
        type: string
        minLength: 1
        maxLength: 256
        pattern: '^[ \S]+$'
        x-example: application/json
      - name: Accept-Charset
        in: header
        description: Character sets that are acceptable.
        x-example: "ISO-8859-1,utf-8;q=0.7,*;q=0.7"
        required: false
        type: string
        minLength: 1
        maxLength: 256
        pattern: '^[\S ]*$'
      - name: Accept-Encoding
        in: header
        description: List of acceptable encodings. See HTTP compression.
        x-example: "gzip,deflate"
        required: false
        type: string
        minLength: 1
        maxLength: 256
        pattern: '^[\S ]*$'
      - name: Accept-Language
        in: header
        description: List of acceptable human languages for response
        x-example: "en-us,en;q=0.5"
        required: false
        type: string
        minLength: 1
        maxLength: 256
        pattern: '^[\S ]*$'
      - name: Authorization
        in: header
        description: OAuth 2.0 access token with the authentication type set as Bearer.
        x-example: Bearer mF_9.B5f-4.1JqM
        required: true
        type: string
        minLength: 1
        maxLength: 256
        pattern: '^[\S ]+$'
      - name: Content-Length
        in: header
        description: Length of the response body in octets; not supported if Transfer-Endoding = chunked
        x-example: "9112300000003"
        required: false
        type: integer
        format: int64
        minimum: 0
        pattern: '^[\d]*$'
      - name: Date
        in: header
        description: The Date general HTTP header contains the date and time at which the message was originated.
        x-example: "Mon, 05 Mar 2018 16:38:08 GMT"
        required: false
        type: string
        format: date-time
        pattern: "^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), [0-3]{1}[0-9]{1} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [1-3][0-9]{3} [0-2][0-9]:[0-6][0-9]:[0-6][0-9] GMT$"
      - name: timestamp
        in: header
        description: ms since 1/1/70 adjusted to GMT
        x-example: "1524599536"
        required: false
        type: integer
        format: int64
        minimum: 0
        maximum: 999999999999
        pattern: '^[\d]*$'
      - name: workflow-id
        in: header
        description: Workflow name (NEW/ADD/EXCHANGE/RETURN)/OrderTypes _ ACTIVATION ADDALINE CHANGEDEVICE_JUMP CHANGEDEVICE
        required: false
        type: string
        x-example: ACTIVATION
        minLength: 1
        maxLength: 256
        pattern: '^[\S]*$'
      - name: activity-id
        in: header
        description: "Unique id alphanumeric value generated and sent by the partner to identify every the service request uniquely. Sample: XyzA:D1s5s1i-X6AFRvfiPBfs-1234  Pass as is to downstream calls."
        x-example: c34e7acd-384b-4c22-8b02-ba3963682508
        required: false
        type: string
        minLength: 1
        maxLength: 256
        pattern: '^[\S]*$'
      - name: interaction-id
        in: header
        description: "Alphanumeric value represent a common transaction id across all calls made from UI, while completing all business activity needs of a particular customer."
        x-example: cc688d54-50d1-49d4-8c5d-95459d1172e8
        required: false
        type: string
        minLength: 1
        maxLength: 256
        pattern: '^[\S]*$'
      - name: service-transaction-id
        in: header
        description: Request ID echoed back by server
        x-example: "23409209723"
        required: false
        type: string
        minLength: 1
        maxLength: 256
        pattern: '^[\S]*$'
      - name: session-id
        in: header
        description: 'A value populated by the sender used to track the transactions that occur during a session, a long-lasting interaction that ismaintained by the sender.'
        x-example: 350b91ec-4a64-4b10-a3f3-a78c8db3924a
        required: false
        type: string
        minLength: 1
        maxLength: 256
        pattern: '^[\S]*$'
      - name: x-auth-originator
        in: header
        description: API chain initiating callers access token
        x-example: c34e7acd-384b-4c22-8b02-ba3963682508
        required: false
        type: string
        minLength: 1
        maxLength: 256
        pattern: '^[\S]*$'
      - name: x-authorization
        in: header
        description: Contains Proof of Possession token generated by API caller
        x-example: c34e7acd-384b-4c22-8b02-ba3963682508
        required: false
        type: string
        minLength: 1
        maxLength: 256
        pattern: '^[\S ]*$'
      - name: application-user-id
        in: header
        description: >-
          NT id of the one who is managing the transaction from CARE/RETAIL/SAMSON
          channels.Submitter Id.
        required: true
        type: string
        minLength: 1
        maxLength: 256
        pattern: '^[\S ]*$'
        x-example: abcde1
      - name: channel-id
        in: header
        description: >-
          Identifies the business unit or sales channel for the Request.Below
          are the sample values: Care, TeleSales, FullService, BusinessDirect,
          FinancePartners, NonFinancePartners, SelfServiceAuthenticated,
          SelfServiceAnonymous, InteractiveVoiceResponse
        required: true
        type: string
        minLength: 1
        maxLength: 256
        pattern: '^[\S ]*$'
        x-example: Care
      responses:
        '200':
          description: >-
            API will return a list of promotion(s).
            Response can be empty as well.
          schema:
            $ref: '#/definitions/ServiceEligiblePromotion'
          examples:
            application/json:
              eligibilitySequenceNo: 779250805909
              promotions:
                - subscriber: '4043455678'
                  promoCode: DEVITO1
                  promoDesc: Buy One gt One Promotion.
                  promoStartDate: '2018-07-01T05:00:00.00Z'
                  promoEndDate: '2019-07-01T05:00:00.00Z'
                  promoType: B/I/F
                  boLineCount: 1
                  goLineCount: 2
              banPaidLines:
                - iotCount: 0
                  ban: 0
                  gsmCount: 0
                  miCount: 0
                  pureMiCount: 0
                  virtualCount: 0
                  duplicateCount: 0
                  autoAttendantCount: 0
                  faxCount: 0
                  conferenceCount: 0
                  totalLineCount: 0
                  promoCode: string
          headers:
            Cache-Control:
              description: Cache policy supported for the resource by the server
              x-example: no-cache
              type: string
              minLength: 1
              maxLength: 256
              pattern: '^[\S]*$'
            Content-Type:
              description: The MIME type of this content
              x-example: text/xml
              type: string
              minLength: 1
              maxLength: 256
              pattern: '^[ \S]+$'
            Date:
              description: The Date general HTTP header contains the date and time at which the message was originated.
              x-example: "Mon, 05 Mar 2018 16:38:08 GMT"
              type: string
              format: date-time
              pattern: "^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), [0-3]{1}[0-9]{1} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [1-3][0-9]{3} [0-2][0-9]:[0-6][0-9]:[0-6][0-9] GMT$"
            ETag:
              description: The ETag header specifies the unique entity tag value for the returned resource.
              x-example: "*"
              type: string
              minLength: 1
              maxLength: 256
              pattern: '^[\S]*$'
            Location:
              description: "The Location header specifies the URI of a created resource, or redirects the API consumer to an alternate resource location."
              x-example: "http://api.t-mo.com/order-mgmt/v1/orders/123xyz-0/status"
              type: string
              minLength: 1
              maxLength: 65536
              pattern: '^[\S]*$'
            Transfer-Encoding:
              description: "The form of encoding used to safely transfer the entity to the user. Currently defined methods are: chunked, compress, deflate, gzip, identity."
              x-example: chunked
              type: string
              minLength: 1
              maxLength: 15
              pattern: '^[\S]*$'

            service-transaction-id:
              description: Request ID echoed back by server
              x-example: "23409209723"
              type: string
              minLength: 1
              maxLength: 256
              pattern: '^[\S]*$'
            
        '400':
          description: Invalid request.
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: "400"
              userMessage: Bad Request
              systemMessage: Bad Request
              detailLink: "http://www.tmus.com"
          headers: {}
        '401':
          description: Unauthorized
          schema:
            $ref: "#/definitions/Error"
          examples:
            application/json:
              code: "401"
              userMessage: Unauthorized
              systemMessage: Unauthorized
              detailLink: "http://www.tmus.com"
          headers: {}
        '404':
          description: Resource not found
          schema:
            $ref: "#/definitions/Error"
          examples:
            application/json:
              code: "404"
              userMessage: Resource not found
              systemMessage: Resource not found
              detailLink: "http://www.tmus.com"
          headers: {}
        "405":
          description: Method Not Allowed
          schema:
            $ref: "#/definitions/Error"
          examples:
            application/json:
              code: "405"
              userMessage: Method Not Allowed
              systemMessage: Method Not Allowed
              detailLink: "http://www.tmus.com"
          headers:
            Allow:
              description: list of supported methods for URI
              x-example: GET
              type: string
              minLength: 1
              maxLength: 256
              pattern: '^[\S]*$'
            service-transaction-id:
              description: Request ID echoed back by server
              x-example: "23409209723"
              type: string
              minLength: 1
              maxLength: 256
              pattern: '^[\S]*$'
        "406":
          description: Mismatching data format
          schema:
            $ref: "#/definitions/Error"
          examples:
            application/json:
              code: "406"
              userMessage: Mismatching data format
              systemMessage: Mismatching data format
              detailLink: "http://www.tmus.com"
          headers: {}

        '500':
          description: The server encountered an unexpected condition which prevented it from fulfilling the request.
          schema:
            $ref: "#/definitions/Error"
          examples:
            application/json:
              code: "500"
              userMessage: System Error
              systemMessage: System Error
              detailLink: "http://www.tmus.com"
          headers: {}
        '503':
          description: The service is unavailable.
          schema:
            $ref: "#/definitions/Error"
          examples:
            application/json:
              code: "503"
              userMessage: Service unavailable
              systemMessage: Service unavailable
              detailLink: "http://www.tmus.com"
          headers: {}
definitions:
  ServiceEligiblePromotion:
    description: Promotion details
    type: object
    properties:
      eligibilitySequenceNo:
        type: integer
        format: int64
        example: 779250805909
        pattern: '[\-+]?[0-9]+'
        description: Sequence number of eligibility check.
      promotions:
        type: array
        description: list of Promotions
        items:
          $ref: "#/definitions/Promotion"
      banPaidLines:
        type: array
        description: list of paid lines of ban
        items:
          $ref: '#/definitions/BanPaidLine'
  Promotion:
    description: Promotions
    type: object
    required:
    - subscriber
    - promoCode
    - promoStartDate
    properties:
      subscriber:
        type: string
        example: '4043455678'
        description: MSISDN number.
        pattern: '^[\S ]+$'
        minLength: 1
        maxLength: 256
      promoCode:
        type: string
        example: 'DEVITO1'
        description: Promo code against the window in which subscriber is activated
        pattern: '^[\S ]+$'
        minLength: 1
        maxLength: 256
      promoDesc:
        type: string
        example: Buy One gt One Promotion.
        description: promotion description
        pattern: '^[\S ]+$'
        minLength: 1
        maxLength: 256
      promoStartDate:
        type: string
        format: date-time
        example: '2018-07-01T05:00:00.00Z'
        pattern: "^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), [0-3]{1}[0-9]{1} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [1-3][0-9]{3} [0-2][0-9]:[0-6][0-9]:[0-6][0-9] GMT$"
        description: promotion start datetime in UTC timezone.
      promoEndDate:
        type: string
        format: date-time
        example: '2018-07-01T05:00:00.00Z'
        pattern: "^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), [0-3]{1}[0-9]{1} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [1-3][0-9]{3} [0-2][0-9]:[0-6][0-9]:[0-6][0-9] GMT$"
        description: promotion end datetime in UTC timezone.
      promoType:
        type: string
        enum:
        - B
        - I
        - F
        example: B/I/F
        minLength: 1
        maxLength: 1
        description: type of promotions BOGO/Indivisual/Flexible.
      boLineCount:
        type: integer
        example: 1
        description: Buy one line count for given promotion.
        pattern: '[\-+]?[0-9]+'
        minimum: 0
        maximum: 999
      goLineCount:
        type: integer
        example: 2
        description: Get one line count for given promotion.
        pattern: '[\-+]?[0-9]+'
        minimum: 0
        maximum: 999
      actStoreId:
        type: string
        example: 'XYZ'
        description: Activation store ID.
        pattern: '^[\S ]+$'
        minLength: 1
        maxLength: 256
      correspondingBoGo:
        type: string
        example: '4043455680,4043455681'
        description: corresponding BO lines for lines eligible for BOGO promotion
    title: Promotion
  BanPaidLine:
    description: Number of paid lines per type
    type: object
    properties:
      ban:
        type: integer
        example: 1
        description: Buy one line count for given promotion.
        pattern: '[\-+]?[0-9]+'
        minimum: 0
        maximum: 999
      promoCode:
        type: string
        example: 'DEVITO1'
        description: Promo code against the window in which subscriber is activated
        pattern: '^[\S ]+$'
        minLength: 1
        maxLength: 256
      gsmCount:
        type: integer
        example: 1
        description: Buy one line count for given promotion.
        pattern: '[\-+]?[0-9]+'
        minimum: 0
        maximum: 999
      miCount:
        type: integer
        example: 1
        description: Buy one line count for given promotion.
        pattern: '[\-+]?[0-9]+'
        minimum: 0
        maximum: 999
      pureMiCount:
        type: integer
        example: 1
        description: Buy one line count for given promotion.
        pattern: '[\-+]?[0-9]+'
        minimum: 0
        maximum: 999
      virtualCount:
        type: integer
        example: 1
        description: Buy one line count for given promotion.
        pattern: '[\-+]?[0-9]+'
        minimum: 0
        maximum: 999
      duplicateCount:
        type: integer
        example: 1
        description: Buy one line count for given promotion.
        pattern: '[\-+]?[0-9]+'
        minimum: 0
        maximum: 999
      autoAttendantCount:
        type: integer
        example: 1
        description: Buy one line count for given promotion.
        pattern: '[\-+]?[0-9]+'
        minimum: 0
        maximum: 999
      faxCount:
        type: integer
        example: 1
        description: Buy one line count for given promotion.
        pattern: '[\-+]?[0-9]+'
        minimum: 0
        maximum: 999
      conferenceCount:
        type: integer
        example: 1
        description: Buy one line count for given promotion.
        pattern: '[\-+]?[0-9]+'
        minimum: 0
        maximum: 999
      iotCount:
        type: integer
        example: 1
        description: Buy one line count for given promotion.
        pattern: '[\-+]?[0-9]+'
        minimum: 0
        maximum: 999
      totalLineCount:
        type: integer
        example: 1
        description: Buy one line count for given promotion.
        pattern: '[\-+]?[0-9]+'
        minimum: 0
        maximum: 999
    title: BanPaidLine
  Error:
    type: object
    description: "As defined in http://api-standards.apps.px-npe01.cf.t-mobile.com/http/status-codes/error-response-format/"
    required:
      - code
      - userMessage
    properties:
      code:
        description: Used to pass error codes
        type: string
        example: 400
        pattern: '^[\S ]+$'
        minLength: 1
        maxLength: 256
      userMessage:
        description: Use to pass human friendly information to the user.
        type: string
        example: GO Line not found for subscriber
        pattern: '^[\S ]+$'
        minLength: 1
        maxLength: 256
      systemMessage:
        description: Used to pass system information.
        type: string
        example: GO_LINE_NOT_FOUND_FOR_BO_LINE
        pattern: '^[\S ]+$'
        minLength: 1
        maxLength: 256
    title: Error
securityDefinitions:
  Oauth:
    type: oauth2
    tokenUrl: 'https://host:env/v1/oauth2/accesstoken?grant_type=client_credentials'
    flow: application
    description: oauth2 Authentication through Apigee 
  
