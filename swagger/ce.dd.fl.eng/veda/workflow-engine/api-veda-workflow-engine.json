{
  "swagger": "2.0",
  "info": {
    "version": "1.0.0",
    "title": "Veda Workflow Engine",
    "description": "System of work items"
  },
  "host": "dev-veda.lab.t-mobile.com:3010",
  "basePath": "/workflow-engine",
  "schemes": [
    "https"
  ],
  "paths": {
    "/health-check": {
      "get": {
        "tags": [
          "workflow"
        ],
        "summary": "Check application",
        "description": "Check if application is alive and reachable",
        "operationId": "healthCheck",
        "produces": [
          "text/plain"
        ],
        "responses": {
          "200": {
            "description": "OK"
          }
        }
      }
    },
    "/work-item-history/{id}": {
      "get": {
        "tags": [
          "workflow"
        ],
        "summary": "Get work item history",
        "description": "Gives the set of successful state transitions that the work item has gone through.",
        "operationId": "getWorkItemHistory",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "type": "string",
            "description": "Work item ID",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          },
          "404": {
            "description": "Not Found"
          },
          "500": {
            "description": "Internal Server Error"
          }
        }
      }
    },
    "/work-item/{id}": {
      "get": {
        "tags": [
          "workflow"
        ],
        "summary": "Get work item",
        "description": "Get a single work item by its ID.",
        "operationId": "getWorkItem",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "type": "string",
            "description": "Work item ID",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          },
          "404": {
            "description": "Not Found"
          }
        }
      }
    },
    "/filter-work-items": {
      "post": {
        "tags": [
          "workflow"
        ],
        "summary": "Get work items",
        "description": "Return a set of work items determined by filter parameters.",
        "operationId": "getWorkItems",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "filter",
            "description": "Filter parameters",
            "required": true,
            "schema": {
              "type": "object"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          },
          "400": {
            "description": "Bad Request"
          },
          "500": {
            "description": "Internal Server Error"
          }
        }
      }
    },
    "/work-item": {
      "post": {
        "tags": [
          "workflow"
        ],
        "summary": "Create work item",
        "description": "Create a new work item based on the provided parameters.",
        "operationId": "createWorkItem",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "workItemToCreate",
            "description": "Work item paramters to create from",
            "required": true,
            "schema": {
              "$ref": "#/definitions/workItemToCreate"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          },
          "404": {
            "description": "Not Found"
          }
        }
      }
    },
    "/event": {
      "post": {
        "tags": [
          "workflow"
        ],
        "summary": "Create work item stage transition",
        "description": "Submit a stage transition to apply to a work item.",
        "operationId": "createWorkItemEvent",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "workItemEvent",
            "description": "Stage transition event",
            "required": true,
            "schema": {
              "$ref": "#/definitions/workItemEvent"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          },
          "404": {
            "description": "Not Found"
          }
        }
      }
    }
  },
  "definitions": {
    "workItemToCreate": {
      "type": "object",
      "description": "Work item paramters to create from",
      "properties": {
        "title": {
          "type": "string"
        },
        "customData": {
          "type": "string"
        },
        "initiatingSystem": {
          "type": "string"
        }
      }
    },
    "workItemEvent": {
      "type": "object",
      "description": "Stage transition event",
      "properties": {
        "initiatingSystem": {
          "type": "string"
        },
        "workItemId": {
          "type": "string"
        },
        "initiatingUser": {
          "type": "string"
        },
        "stageTransition": {
          "type": "object",
          "$ref": "#/definitions/stageTransition"
        }
      }
    },
    "stageTransition": {
      "type": "object",
      "description": "Stage transition start and end stages",
      "properties": {
        "startStage": {
          "type": "string"
        },
        "endStage": {
          "type": "string"
        }
      }
    }
  }
}