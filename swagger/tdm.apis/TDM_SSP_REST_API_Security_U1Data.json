{
  "swagger": "2.0",
  "info": {
    "title": "U1postpaid1-testdata-lookup",
    "description": "This API retrieves U1 post-paid test data",
    "version": "1.0.0",
    "contact": {
      "name": "Test Data Management",
      "url": "https://dftdm-ssp.apps.px-prd01.cf.t-mobile.com/login",
      "email": "Neha.Gandhi5@T-Mobile.com"
    },
    "x-createdBy": {
      "dateCreated": "Thursday June 11 10:10:02 2019",
      "createdBy": "Neha Gandhi",
      "application": "TDM SSP",
      "appVersion": "0.3.8.2201",
      "documentId": "39e22044-fe01-4396-a35e-228d375900f9",
      "status": "API Security - Initial",
      "classification": "Test Data Management.",
      "profile": "Application Integration Service",
      "serviceLayer": "Resource - Other"
    }
  },
  "host": "env.api.t-mobile.com",
  "basePath": "/tdm/v1/postpaid-data",
  "schemes": [
    "https"
  ],
  "x-servers": [
    {
      "url": "https://tmobileb-sb01.apigee.net/tdm/v1/postpaid-data/data",
      "description": "Sandbox Server"
    },
    {
      "url": "https://api.t-mobile.com/tdm/v1/data",
      "description": "Live Server"
    }
  ],
  "tags": [
    {
      "name": "U1 Postpaid API",
      "description": "U1 test data API scenarios."
    }
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/data": {
      "get": {
        "tags": [
          "data"
        ],
        "description": "This resource can be used to fetch U1 postpaid data based on the scenario, environment and count",
        "operationId": "data",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
		  {
            "name": "scenario",
            "in": "query",
            "description": "Scenario in the API request.",
            "required": true,
            "type": "string",
            "pattern": ".*$",
			"minLength": 0,
            "maxLength": 50,
            "x-example": "jump_upgrade_single"
          },
		  {
            "name": "env",
            "in": "query",
            "description": "Requested environment for the test data in the API request.",
            "required": true,
            "type": "string",
            "pattern": ".*$",
			"minLength": 0,
            "maxLength": 50,
                           "x-example": "QLAB01"
          },
		  {
            "name": "count",
            "in": "query",
            "description": "Requested number of count for the test data in the API request.",
            "required": true,
            "type": "integer",
            "pattern": ".*$",
			"minLength": 0,
            "maxLength": 50,
            "x-example": "1"
          },
		  {
            "name": "soc",
            "in": "query",
            "description": "Soc for the test data in the API request.",
            "required": false,
            "type": "string",
            "pattern": ".*$",
			"minLength": 0,
            "maxLength": 50,
            "x-example": ""
          },
          {
            "name": "Authorization",
            "in": "header",
            "description": "OAuth 2.0 access token with the authentication type set as Bearer.",
            "required": true,
            "type": "string",
            "pattern": "Bearer .*$",
			"minLength": 0,
            "maxLength": 50,
                           "x-example": "Bearer WoOhe88khLZ3rMVqujG9GGa4PRH2"
          },
		  {
            "name": "account_type",
            "in": "header",
            "description": "Account type in the API request.",
            "required": false,
            "type": "string",
            "pattern": ".*$",
			"minLength": 0,
            "maxLength": 2,
            "x-example": "I"
          },
		  {
            "name": "sub_type",
            "in": "header",
            "description": "Account Sub-type in the API request.",
            "required": false,
            "type": "string",
            "pattern": ".*$",
			"minLength": 0,
            "maxLength": 2,
            "x-example": "R"
          },
          {
            "name": "Accept",
            "in": "header",
            "description": "Content-Types that are acceptable",
            "required": false,
            "type": "string",
			"minLength": 0,
            "maxLength": 50,
            "x-example": "application/json"
          },
          {
            "name": "Accept-Encoding",
            "in": "header",
            "description": "The preferred encoding of response body for better performance.",
            "required": false,
            "type": "string",
			"minLength": 0,
            "maxLength": 50,
            "x-example": "gzip"
          },
          {
            "name": "Accept-Language",
            "in": "header",
            "description": "Represents the list of natural languages preferred in response.",
            "required": false,
            "type": "string",
			"minLength": 0,
            "maxLength": 50,
            "x-example": "en"
          },
          {
            "name": "Content-Length",
            "in": "header",
            "description": "Represents the length of request body in octets.",
            "required": false,
            "type": "string",
			"minLength": 0,
            "maxLength": 50,
            "x-example": "1024"
          }
        ],
        "responses": {
          "200": {
            "description": "Ok",
            "headers": {
              "Content-Encoding": {
                "description": "The type of encoding used on the data.",
                "type": "string",
                "x-example": "gzip",
				"minLength": 0,
                "maxLength": 50,
                "pattern": ".*$"
              },
              "Content-Language": {
                "description": "The natural language or languages of the intended audience for the enclosed content.",
                "type": "string",
                "x-example": "en",
				"minLength": 0,
                "maxLength": 50,
                "pattern": ".*$"
              },
              "Content-Length": {
                "description": "The Content-Length header specifies the actual length of the returned payload.",
                "x-example": "1024",
                "type": "string",
				"minLength": 0,
                "maxLength": 50,
                "pattern": ".*$"
              },
              "Date": {
                "description": "The date and time of the request based on the clock on server side.",
                "x-example": "Date: Mon, 05 Mar 2018 16:38:08 GMT",
                "type": "string",
                "format": "date-time",
				"minLength": 0,
                "maxLength": 50,
                "pattern": ".*$"
              },
              "Transfer-Encoding": {
                "description": "Indicates that data is streamed as a series of chunks with the final zero-length chunk indicating completion.",
                "x-example": "chunked",
                "type": "string",
				"minLength": 0,
                "maxLength": 50,
                "pattern": ".*$"
              },
              "Expires": {
                "description": "The Expires response header contains the date/time after which the response is considered stale.",
                "x-example": "Mon, 05 Mar 2018 16:38:08 GMT",
                "type": "string",
				"minLength": 0,
                "maxLength": 50,
                "pattern": ".*$"
              },
              "Cache-Control": {
                "description": "Cache policy supported for the resource by the server, max-age indicates the number of seconds, x, after which the response is stale, no-cache means the client or intermediate cache must not re-use the representation without checking the ETag with the server: if possible, it is preferable to use a low value for max-age instead, no-store means the client is not allowed to cache the representation.",
                "x-example": "max-age=3600",
                "type": "string",
				"minLength": 0,
                "maxLength": 50,
                "pattern": ".*$"
              }
            },
			"examples": {
				"application/json": {
				  "dataset": {
					"data": {
					  "record": [
						{
						  "MSISDN": "9093194779",
                          "BAN": "958871846"
						}
					  ]
					}
				  }
				}
			  }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
        
            },
            "examples": {
              "application/json": {
                "code": "TDM-0400",
                "userMessage": "Bad Request",
                "systemMessage": "Missing request header 'Authorization' for method parameter of type String",
                "detailLink": "https://dev01.api.t-mobile.com/tdm/v1/errors/21b7c4d3"
              }
            }
          },
		  "401": {
            "description": "Unauthorized",
            "schema": {
         
            },
            "examples": {
              "application/json": {
                "code": "MD-0401",
                "userMessage": "Unauthorized",
                "systemMessage": "Unauthorized"
              }
            }
          },
          "404": {
            "description": "Resource not found",
            "schema": {
        
            },
            "examples": {
              "application/json": {
                "code": "TDM-0404",
                "userMessage": "Resource not found",
                "systemMessage": "Resource not found",
                "detailLink": "https://dev01.api.t-mobile.com/tdm/v1/errors/21b7c4d3"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
         
            },
            "examples": {
              "application/json": {
                "code": "TDM-0500",
                "userMessage": "System Error",
                "systemMessage": "System Error",
                "detailLink": "https://dev01.api.t-mobile.com/tdm/v1/errors/21b7c4d3"
              }
            }
          }
        },
        "summary": "Get U1 postpaid test data",
        "security": [
          {
            "Oauth": [
              "get:data"
            ]
          }
        ]
	  }
     }
	},
  "securityDefinitions": {
    "Oauth": {
      "type": "oauth2",
      "description": "When you sign up for an account, you are given your first API key.\nTo do so please follow this link: https://www.t-mobile.com/site/signup/\nAlso you can generate additional API keys, and delete API keys (as you may\nneed to rotate your keys in the future).\nToken can change to a JwT and ID and PoP tokens.\n",
      "tokenUrl": "https://host:env/v1/oauth2/accesstoken?grant_type=client_credentials",
      "flow": "application",
      "scopes": {
        "get:data": "Get U1 test data"
      }
    }
  }
}