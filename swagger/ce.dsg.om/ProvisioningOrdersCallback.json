{
    "swagger": "2.0",
    "info": {
        "title": "ProvisioningOrdersCallback",
        "description": "Provisioning Order-Callback API is used to capture the Network Provistioning Engine statuses and update them into OM. This API is protected with JWT security and a valid client pop token credentials are required to generate the bearer access token and try it out live. ",
        "version": "1.0.0",
        "contact": {
            "name": "OM Provisioning API",
            "url": "http://www.t-mobile.com",
            "email": "P&TOrderManagementFulfillment@T-Mobile.com"
        },
        "license": {
            "name": "Proprietary software",
            "url": "https://en.wikipedia.org/wiki/Proprietary_software"
        },
        "termsOfService": "https://www.t-mobile.com/templates/popup.aspx?PAsset=Ftr_Ftr_TermsAndConditions",
        "x-createdBy": {
            "dateCreated": "Wed Feb  6 17:54:03 2019",
            "createdBy": "bpunreddy",
            "application": "Recite",
            "appVersion": "0.4.3.2801",
            "documentId": "890acaba-b961-4a1f-a4b0-00283d26398e",
            "status": "Conceptual - Initial",
            "classification": "4.5 Order Fulfillment Mgmt.",
            "profile": "Core Business Capability Service",
            "serviceLayer": "Enterprise - OrderManagement"
        }
    },
    "tags": [
        {
            "name": "SubmitProvisioningOrderCallback",
            "description": ""
        }
    ],
    "host": "localhost:8080",
    "basePath": "/ordermanagement/v1/provisioning-orders-callback",
    "schemes": [
        "https"
    ],
    "paths": {
        "/{order-id}": {
            "post": {
                "tags": [
                    "SubmitProvisioningOrderCallback"
                ],
                "x-api-pattern": "CreateInCollection",
                "summary": "Put operation to submit the network provisioning order callback request to OM.",
                "description": "Submit a network provisioning order callback request. This API shall be used by NPE to update the network provisioning order status back to OM.",
                "operationId": "post-provisioning-notification-messages",
                "consumes": [
                    "application/json",
                    "application/xml"
                ],
                "produces": [
                    "application/json",
                    "application/xml"
                ],
                "parameters": [
                    {
                        "name": "EDANotification",
                        "in": "body",
                        "description": "EDA success or error notification",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/EDANotification"
                        },
                        "x-example": {
                            "neStatus": {
                                "neName": "CS",
                                "errors": {
                                    "errorDesc": "AIR - Invalid combination of values.. External error. Error occurred when provisioning AIR. - [Processed by PG Node: veccedal006-1]",
                                    "errorCode": "17352"
                                }
                            },
                            "provisioningStatusCode": "3",
                            "source": "METRO",
                            "notificationType": "FINAL",
                            "provisioningStatusMsg": "CS transaction failed"
                        }
                    },
                    {
                        "name": "order-id",
                        "in": "path",
                        "description": "OrderId_StepId",
                        "required": true,
                        "type": "string",
                        "x-example": "orderid{orderid:string}"
                    },
                    {
                        "name": "Accept",
                        "in": "header",
                        "description": "Preferred content type _ default to application/json",
                        "x-example": "application/json",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[ \\S]+$"
                    },
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "The HTTP Authorization request header contains the credentials to authenticate a user agent with a server.",
                        "x-example": "Bearer mF_9.B5f-4.1JqM",
                        "required": true,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S ]+$"
                    },
                    {
                        "name": "Content-Type",
                        "in": "header",
                        "description": "The MIME type of this content",
                        "x-example": "text/xml",
                        "required": true,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[ \\S]+$"
                    },
                    {
                        "name": "Date",
                        "in": "header",
                        "description": "The Date general HTTP header contains the date and time at which the message was originated.",
                        "x-example": "Mon, 05 Mar 2018 16:38:08 GMT",
                        "required": false,
                        "type": "string",
                        "format": "date-time",
                        "pattern": "^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), [0-3]{1}[0-9]{1} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [1-3][0-9]{3} [0-2][0-9]:[0-6][0-9]:[0-6][0-9] GMT$"
                    },
                    {
                        "name": "If-None-Match",
                        "in": "header",
                        "description": "The If-None-Match header specifies an ETag-related condition to be placed on the execution of a request.  The request will NOT be executed if the ETag of the requested resource matches one of those specified in the If-None-Match header.  Because the behavior is complex, the details for usage are provided at http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html",
                        "x-example": "*",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "activity-id",
                        "in": "header",
                        "description": "Unique id alphanumeric value generated and sent by the partner to identify every the service request uniquely. Sample: XyzA:D1s5s1i-X6AFRvfiPBfs-1234  Pass as is to downstream calls.",
                        "x-example": "c34e7acd-384b-4c22-8b02-ba3963682508",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "interaction-id",
                        "in": "header",
                        "description": "Alphanumeric value represent a common transaction id across all calls made from UI, while completing all business activity needs of a particular customer.",
                        "x-example": "cc688d54-50d1-49d4-8c5d-95459d1172e8",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "service-transaction-id",
                        "in": "header",
                        "description": "Request ID echoed back by server",
                        "x-example": "23409209723",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "session-id",
                        "in": "header",
                        "description": "A value populated by the sender used to track the transactions that occur during a session, a long-lasting interaction that ismaintained by the sender.",
                        "x-example": "350b91ec-4a64-4b10-a3f3-a78c8db3924a",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "timestamp",
                        "in": "header",
                        "description": "ms since 1/1/70 adjusted to GMT",
                        "x-example": "1524599536",
                        "required": false,
                        "type": "integer",
                        "format": "int64",
                        "minimum": 0,
                        "maximum": 999999999999,
                        "pattern": "^[\\d]*$"
                    },
                    {
                        "name": "workflow-id",
                        "in": "header",
                        "description": "Business Use Case",
                        "x-example": "ACTIVATION",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "x-auth-originator",
                        "in": "header",
                        "description": "API chain initiating callers access token",
                        "x-example": "QVdvYWZBdnA4VEFYcXY2UkJoSjg5dmlpOENWVUpaRGYgOms4c0hLNXhDWE9PS2lZUEg=",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "x-authorization",
                        "in": "header",
                        "description": "Contains Proof of Possession token generated by API caller",
                        "x-example": "QVdvYWZBdnA4VEFYcXY2UkJoSjg5dmlpOENWVUpaRGYgOms4c0hLNXhDWE9PS2lZUEg=",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 4096,
                        "pattern": "^[\\S ]*$"
                    }
                ],
                "responses": {
                    "201": {
                        "description": "Created",
                        "headers": {
                            "Date": {
                                "description": "The Date general HTTP header contains the date and time at which the message was originated.",
                                "x-example": "Mon, 05 Mar 2018 16:38:08 GMT",
                                "type": "string",
                                "format": "date-time",
                                "pattern": "^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), [0-3]{1}[0-9]{1} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [1-3][0-9]{3} [0-2][0-9]:[0-6][0-9]:[0-6][0-9] GMT$"
                            },
                            "Location": {
                                "description": "The Location header specifies the URI of a created resource, or redirects the API consumer to an alternate resource location.",
                                "x-example": "http://api.t-mo.com/order-mgmt/v1/orders/123xyz-0/status",
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 65536,
                                "pattern": "^[\\S]*$"
                            },
                            "service-transaction-id": {
                                "description": "Request ID echoed back by server",
                                "x-example": "23409209723",
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 256,
                                "pattern": "^[\\S]*$"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "400",
                                "userMessage": "Bad Request",
                                "systemMessage": "Bad Request",
                                "detailLink": "http://www.tmus.com"
                            }
                        },
                        "headers": {}
                    },
                    "401": {
                        "description": "Unauthorized",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "401",
                                "userMessage": "Unauthorized",
                                "systemMessage": "Unauthorized",
                                "detailLink": "http://www.tmus.com"
                            }
                        },
                        "headers": {}
                    },
                    "403": {
                        "description": "Client access denied",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "403",
                                "userMessage": "Client access denied",
                                "systemMessage": "Client access denied",
                                "detailLink": "http://www.tmus.com"
                            }
                        },
                        "headers": {}
                    },
                    "404": {
                        "description": "Resource not found",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "404",
                                "userMessage": "Resource not found",
                                "systemMessage": "Resource not found",
                                "detailLink": "http://www.tmus.com"
                            }
                        },
                        "headers": {}
                    },
                    "405": {
                        "description": "Method Not Allowed",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "405",
                                "userMessage": "Method Not Allowed",
                                "systemMessage": "Method Not Allowed",
                                "detailLink": "http://www.tmus.com"
                            }
                        },
                        "headers": {
                            "Allow": {
                                "description": "list of supported methods for URI",
                                "x-example": "GET",
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 256,
                                "pattern": "^[\\S]*$"
                            },
                            "service-transaction-id": {
                                "description": "Request ID echoed back by server",
                                "x-example": "23409209723",
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 256,
                                "pattern": "^[\\S]*$"
                            }
                        }
                    },
                    "406": {
                        "description": "Mismatching data format",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "406",
                                "userMessage": "Mismatching data format",
                                "systemMessage": "Mismatching data format",
                                "detailLink": "http://www.tmus.com"
                            }
                        },
                        "headers": {}
                    },
                    "409": {
                        "description": "Invalid data",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "409",
                                "userMessage": "Invalid data",
                                "systemMessage": "Invalid data",
                                "detailLink": "http://www.tmus.com"
                            }
                        },
                        "headers": {}
                    },
                    "415": {
                        "description": "Unsupported Media Type",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "415",
                                "userMessage": "Unsupported Media Type",
                                "systemMessage": "Unsupported Media Type",
                                "detailLink": "http://www.tmus.com"
                            }
                        },
                        "headers": {}
                    },
                    "500": {
                        "description": "System Error",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "500",
                                "userMessage": "System Error",
                                "systemMessage": "System Error",
                                "detailLink": "http://www.tmus.com"
                            }
                        },
                        "headers": {}
                    },
                    "503": {
                        "description": "Service unavailable",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "503",
                                "userMessage": "Service unavailable",
                                "systemMessage": "Service unavailable",
                                "detailLink": "http://www.tmus.com"
                            }
                        },
                        "headers": {}
                    }
                },
                "security": [
                    {
                        "JWT": [
                            "write:orders:",
                            "read:orders:"
                        ]
                    }
                ]
            }
        }
    },
    "definitions": {
        "EDANotification": {
            "type": "object",
            "description": "EDANotification",
            "properties": {
                "provisioningStatusCode": {
                    "description": "0-7 numbers that define the type of notigfication",
                    "type": "integer",
                    "format": "int32",
                    "pattern": "[\\-+]?[0-9]+"
                },
                "source": {
                    "description": "source",
                    "type": "string"
                },
                "notificationType": {
                    "description": "defines if the notification is core or final",
                    "type": "string"
                },
                "provisioningStatusMsg": {
                    "description": "defines the reason for the success or failure",
                    "type": "string"
                },
                "neStatus": {
                    "$ref": "#/definitions/NeStatus"
                }
            }
        },
        "Error": {
            "type": "object",
            "description": "As defined in http://api-standards.apps.px-npe01.cf.t-mobile.com/http/status-codes/error-response-format/",
            "required": [
                "code",
                "userMessage"
            ],
            "properties": {
                "code": {
                    "description": "A succinct, domain-specific, human-readable text string to identify the type of error for the given status code",
                    "example": "ProductNotFound",
                    "type": "string",
                    "pattern": "^[\\S ]+$"
                },
                "userMessage": {
                    "description": "A human-readable message describing the error.",
                    "example": "Failed to find product.",
                    "type": "string",
                    "pattern": "^[\\S ]+$"
                },
                "systemMessage": {
                    "description": "Text that provides a more detailed technical explanation of the error",
                    "example": "PRODUCT_NOT_FOUND",
                    "type": "string"
                },
                "detailLink": {
                    "description": "link to custom information providing greater detail on error or errors",
                    "example": "http://aaa.bbb.ccc/",
                    "type": "string"
                }
            }
        },
        "NeStatus": {
            "type": "object",
            "description": "neStatus",
            "properties": {
                "neName": {
                    "description": "neName",
                    "type": "string"
                },
                "errors": {
                    "$ref": "#/definitions/NeStatusErrors"
                }
            }
        },
        "NeStatusErrors": {
            "type": "object",
            "properties": {
                "userMessage": {
                    "description": "userMessage",
                    "type": "string"
                },
                "systemMessage": {
                    "description": "systemMessage",
                    "type": "string"
                },
                "code": {
                    "description": "code",
                    "type": "string"
                }
            }
        }
    },
    "securityDefinitions": {
        "JWT": {
            "type": "oauth2",
            "description": "Standard security scheme to access the APIs is accessCode, for channels where customer is accessing the UI. Tokens issued are JWT signed tokens (JWE in future) using public/private key pair. PoP JWT are needed for Public APIs. Other flow types like - application may be supported - if reviewed and approved by Security teams at T-Mobile.",
            "authorizationUrl": "https://host-env:port/ras/v3/code",
            "tokenUrl": "https://host-env:port/oauth2/v4/tokens",
            "flow": "accessCode",
            "scopes": {
                "write:orders:": "create new orders",
                "read:orders:": "read existing orders"
            }
        }
    }
}