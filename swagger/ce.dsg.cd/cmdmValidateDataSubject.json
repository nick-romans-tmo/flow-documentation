{
  "swagger": "2.0",
  "info": {
    "title": "cmdmValidateDataSubject v1.0",
    "description": "CMDM ValidateDataSubjectService to validate DSR request and enrich the DSR request for current/former customers",
    "version": "1.0.0",
    "contact": {
      "name": "Jose Antony",
      "email": "jose.antony2@t-mobile.com",
      "x-Organization": "Customer Info Domain",
      "x-OrganizationEmail": "EIT_CM_AnalysisArch@TMobileUSA.onmicrosoft.com"
    },
    "x-createdBy": {
      "dateCreated": "Wed Oct 02 14:04:51 2019",
      "modifiedBy": "jantony2",
      "classification": "5.2 Customer Profile Data Mgmt",
      "profile": "Core Business Capability Service - CCPA Validate Data Subject",
      "serviceLayer": "Enterprise - CustomerManagement"
    }
  },
  "tags": [
    {
      "name": "validateDataSubjectService",
      "description": "Validate the DSR request for current/former customers and enrich with Account/Subscriber identifiers"
    }
  ],
  "host": "dit01.api.t-mobile.com",
  "basePath": "/customer-profile/v1/consumer-rights",
  "x-servers": [
    {
      "url": "http://127.0.0.1/",
      "description": "Sandbox"
    },
    {
      "url": "http://127.0.0.1/",
      "description": "Production"
    }
  ],
  "schemes": [
    "https"
  ],
  "paths": {
    "/validate-data-subject": {
      "post": {
        "tags": [
          "validateDataSubjectService"
        ],
        "summary": "Method to search for Current and Former customers and enrich with Account/Subscriber identifiers",
        "description": "Search for Current and Former customers based on the identifiers provided across all brands and return enriched resultset",
        "operationId": "validateDataSubject",
        "x-api-pattern": "ExecuteFunction",
        "consumes": [
          "application/json",
          "application/xml"
        ],
        "produces": [
          "application/json",
          "application/xml"
        ],
        "parameters": [
          {
            "name": "validateDataSubjectRequest",
            "in": "body",
            "description": "Validate DataSubject Request",
            "required": true,
            "schema": {
              "$ref": "#/definitions/DataSubjectRequest"
            }
          },
          {
            "name": "Authorization",
            "in": "header",
            "description": "JWT access token",
            "required": true,
            "type": "string",
            "x-example": "Bearer eyJhbGciOi.eyJpYXQiOjE1NjE1OD.qGyk_vFpujoLt64",
            "pattern": "^.*$",
            "minLength": 0,
            "maxLength": 1000
          },
          {
            "name": "Content-Type",
            "in": "header",
            "description": "content-type of request body if exists, application/json expected, except for PATCH",
            "required": true,
            "type": "string",
            "x-example": "application/json; charset=UTF-8",
            "pattern": "^.*$",
            "minLength": 0,
            "maxLength": 100
          },
          {
            "name": "x-authorization",
            "in": "header",
            "description": "JWT PoP token",
            "required": false,
            "type": "string",
            "format": "string",
            "x-example": "eyJhbGciOi.eyJpYXQiOjE1NjE1OD.qGyk_vFpujoLt64",
            "pattern": "^.*$",
            "minLength": 0,
            "maxLength": 1000
          },
          {
            "name": "x-auth-originator",
            "in": "header",
            "description": "JWT id token",
            "required": false,
            "type": "string",
            "format": "string",
            "x-example": "eyJhbGciOi.eyJpYXQiOjE1NjE1OD.qGyk_vFpujoLt64",
            "pattern": "^.*$",
            "minLength": 0,
            "maxLength": 1000
          },
          {
            "name": "timestamp",
            "in": "header",
            "description": "timestamp",
            "required": false,
            "type": "string",
            "pattern": "^.*$",
            "x-example": "1524599536",
            "minLength": 0,
            "maxLength": 100
          },
          {
            "name": "workflow-id",
            "in": "header",
            "description": "workflow-id",
            "required": false,
            "type": "string",
            "x-example": "ACTIVATION",
            "pattern": "^.*$",
            "minLength": 0,
            "maxLength": 100
          },
          {
            "name": "activity-id",
            "in": "header",
            "description": "activity-id - global GUID of transaction for auditing sent from calling app",
            "required": false,
            "type": "string",
            "x-example": "c34e7acd-384b-4c22-8b02-ba3963682508",
            "pattern": "'^[a-zA-Z0-9-]*$'",
            "minLength": 1,
            "maxLength": 100
          },
          {
            "name": "session-id",
            "in": "header",
            "description": "session-id",
            "required": false,
            "type": "string",
            "x-example": "350b91ec-4a64-4b10-a3f3-a78c8db3924a",
            "pattern": "'^[a-zA-Z0-9-]*$'",
            "minLength": 1,
            "maxLength": 100
          },
          {
            "name": "interaction-id",
            "in": "header",
            "description": "interaction-id",
            "required": false,
            "type": "string",
            "x-example": "cc688d54-50d1-49d4-8c5d-95459d1172e8",
            "pattern": "'^[a-zA-Z0-9-]*$'",
            "minLength": 1,
            "maxLength": 100
          },
          {
            "name": "service-transaction-id",
            "in": "header",
            "description": "service-transaction-id",
            "required": false,
            "type": "string",
            "x-example": "cc688d54-50d1-49d4-8c5d-95459d1172e8",
            "pattern": "'^[a-zA-Z0-9-]*$'",
            "minLength": 1,
            "maxLength": 100
          }
        ],
        "responses": {
		   "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/DataSubjectResponse"
            },
            "headers": {
              "Cache-Control": {
                "description": "No cache indicators supported at this time. Samples: no-cache",
                "type": "string",
                "x-example": "no-cache",
                "pattern": "^.*$",
                "minLength": 0,
                "maxLength": 50
              },
              "Content-Encoding": {
                "description": "The type of encoding used on the data",
                "type": "string",
                "x-example": "gzip,deflate",
                "pattern": "^.*$",
                "minLength": 0,
                "maxLength": 50
              },
              "Content-Language": {
                "description": "The natural language or languages of the intended audience for the enclosed content",
                "type": "string",
                "x-example": "en",
                "pattern": "^.*$",
                "minLength": 0,
                "maxLength": 50
              },
              "Content-Length": {
                "description": "The Content-Length header specifies the actual length of the returned payload.",
                "type": "string",
                "x-example": "9112300000003",
                "pattern": "^.*$",
                "minLength": 0,
                "maxLength": 50
              },
              "Content-Type": {
                "description": "The Content-Length header specifies the actual length of the returned payload.",
                "type": "string",
                "x-example": "application/json",
                "pattern": "^.*$",
                "minLength": 0,
                "maxLength": 50
              },
              "Date": {
                "description": "The date and time that the message was originated (in HTTP-date format as defined by RFC 7231 Date/Time Formats).",
                "type": "string",
                "format": "date-time",
                "x-example": "Mon, 05 Mar 2018 16:38:08 GMT",
                "minLength": 0,
                "maxLength": 50
              },
              "ETag": {
                "description": "The ETag header specifies the unique entity tag value for the returned resource.",
                "type": "string",
                "x-example": "*",
                "pattern": "^.*$",
                "minLength": 0,
                "maxLength": 50
              },
              "Last-Modified": {
                "description": "The Last-Modified entity-header field indicates the date and time at which the origin server believes the variant was last modified.",
                "type": "string",
                "format": "date-time",
                "x-example": "Mon, 05 Mar 2018 16:38:08 GMT",
                "minLength": 0,
                "maxLength": 50
              },
              "Transfer-Encoding": {
                "description": "The form of encoding used to safely transfer the entity to the user. Currently defined methods are: chunked, compress, deflate, gzip, identity.",
                "type": "string",
                "x-example": "chunked",
                "pattern": "^.*$",
                "minLength": 0,
                "maxLength": 50
              },
              "service-transaction-id": {
                "description": "Custom TMO Header.",
                "type": "string",
                "x-example": "test",
                "pattern": "^.*$",
                "minLength": 0,
                "maxLength": 50
              }
            },
            "examples": {
              "application/json": {
					  "requestId": "4237583948575",		
					  "subTaskId": "83948575"
			    }
             }
          },
          "400": {
            "description": "BAD Request",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "400",
                "userMessage": "Bad Request",
                "systemMessage": "Bad Request",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "401",
                "userMessage": "Unauthorized",
                "systemMessage": "Unauthorized",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "404": {
            "description": "Resource not Found",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "404",
                "userMessage": "Resource not Found",
                "systemMessage": "Resource not Found",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "405": {
            "description": "Method Not Allowed",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "405",
                "userMessage": "Method Not Allowed",
                "systemMessage": "Method Not Allowed",
                "detailLink": "http://www.tmus.com"
              }
            },
            "headers": {
              "Allow": {
                "description": "The comma-separated list of allowed HTTP request methods.",
                "type": "string",
                "x-example": "POST",
                "pattern": "^.*$",
                "minLength": 0,
                "maxLength": 100
              },
              "service-transaction-id": {
                "description": "Custom TMO Header.",
                "type": "string",
                "x-example": "test",
                "pattern": "^.*$",
                "minLength": 0,
                "maxLength": 100
              }
            }
          },
          "409": {
            "description": "Invalid data",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "409",
                "userMessage": "Invalid data",
                "systemMessage": "Invalid data",
                "detailLink": "http://www.tmus.com"
              }
            },
            "headers": {}
          },
          "415": {
            "description": "Unsupported Media Type",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "415",
                "userMessage": "Unsupported Media Type",
                "systemMessage": "Unsupported Media Type",
                "detailLink": "http://www.tmus.com"
              }
            },
            "headers": {}
          },
          "500": {
            "description": "Internal Service Error",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "500",
                "userMessage": "System Error",
                "systemMessage": "System Error",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "503": {
            "description": "Backend Service error",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "503",
                "userMessage": "Service unavailable",
                "systemMessage": "Service unavailable",
                "detailLink": "http://www.tmus.com"
              }
            }
          }
        },
        "security": [
          {
            "JWT": []
          }
        ]
      }
   }
  },
  "definitions": {
    "DataSubjectRequest": {
      "type": "object",
      "description": "Data Subject Request for Access or Delete",
      "required": [
        "requestId",
		"subTaskId",
        "requestType",
        "requestSubmittedDate",  
        "requestDueDate",
		"idFirstName",		
		"idLastName",		
		"dataSubjectType",
		"communicationEmail",
		"dataSubject"
      ],
      "properties": {
		"requestId": {
          "description": "Unique DSR request ID",
          "type": "string",
          "pattern": "^.*$",  
          "example": "4237583948575",
          "minLength": 0,
          "maxLength": 100
        },
		"subTaskId": {
          "description": "Unique Sub Task ID for the DSR request",
          "type": "string",
          "pattern": "^.*$",
          "example": "83948575",
          "minLength": 0,
          "maxLength": 100
        },
        "requestType": {
          "description": "Type of DSR Request - Access or Delete",
          "type": "string",          
          "pattern": "^[a-zA-Z]*$",
		  "enum": [
            "Access",
            "Delete"
          ],
          "example": "Access",
          "minLength": 0,
          "maxLength": 10
        },
        "requestSubmittedDate": {         
          "description": "Date and Timestamp of DSR Submit Request in UTC format",
          "type": "string",
          "format": "date-time",
          "example": "2019-07-19T22:06:02.419Z",
          "pattern": "^.*$",
          "minLength": 0,
          "maxLength": 50
        },
		"requestDueDate": {         
          "description": "Date and Timestamp of due date of DSR Request in UTC format",
          "type": "string",
          "format": "date-time",
          "example": "2019-09-19T22:06:02.419Z",
          "pattern": "^.*$",
          "minLength": 0,
          "maxLength": 50
        },
        "idFirstName": {
          "description": "The first name of the individual making the request",
          "type": "string",
          "format": "encrypted",
          "x-tmo-cipher-format": "string",
          "pattern": "^.*$",
          "example": "Jane",
          "minLength": 0,
          "maxLength": 100
        },
        "idMiddleName": {
          "description": "This is the middle name of the individual making the request.",
          "type": "string",
          "format": "encrypted",
          "x-tmo-cipher-format": "string",
          "pattern": "^.*$",
          "example": "A",
          "minLength": 0,
          "maxLength": 100
        },
        "idLastName": {
          "description": "The last name of the individual making the request",
         "type": "string",
          "format": "encrypted",
          "x-tmo-cipher-format": "string",
          "pattern": "^.*$",
          "example": "Doe",
          "minLength": 0,
          "maxLength": 100
        },
		"idDob": {
          "description": "Date of birth",
          "type": "string",
          "format": "encrypted",
          "x-tmo-cipher-format": "string",
          "pattern": "^.*$",
          "example": "01/01/1980",
          "minLength": 0,
          "maxLength": 10
        },
        "govtIdNumber": {
          "description": "Drivers license or Passport Number",
          "type": "string",
          "format": "encrypted",
          "x-tmo-cipher-format": "string",
          "pattern": "^.*$",
          "example": "567584984*UHDR",
          "minLength": 0,
          "maxLength": 100
        },
		"govtAddress": {
          "$ref": "#/definitions/Address"
        },
        "communicationEmail": {
          "description": "The email address provided for communications purposes only",
          "type": "string",
          "pattern": "^.*$",          
          "example": "jane.doe@gmail.com",
          "minLength": 0,
          "maxLength": 200
        },
		"communicationMailingAddress": {
          "$ref": "#/definitions/Address"
        },
        "tmoId": {
          "description": "TMOID",
          "type": "string",
          "format": "encrypted",
          "x-tmo-cipher-format": "string",
          "pattern": "^.*$",
          "example": "U-5d4627da-7ghf-4331-b091-3ert657jthy427",
          "minLength": 0,
          "maxLength": 100
        },
		"dataSubjectType": {
          "description": "Data Subject Type",
          "type": "string",          
          "pattern": "^[a-zA-Z]*$",
		  "enum": [
            "Current",
            "Former",
			"Prospect"
          ],
          "example": "Current",
          "minLength": 0,
          "maxLength": 10
        },
		"geographicLocation": {
          "description": "Data Subject's Geographic Location(State)",
          "type": "string",          
          "pattern": "^[A-Z]*$",		  
          "example": "CA",
          "minLength": 0,
          "maxLength": 2
        },
        "dataSubject": {
          "description": "Data Subject Identifiers",
          "type": "array",
          "items": {
            "$ref": "#/definitions/DataSubjectIdentifiers"
          },
          "example": {
            "brandName": "MbyT",
            "accountNumber": "6758686860",
            "msisdn": "4086789867"
          }
        }
      }
    }, 
	"DataSubjectIdentifiers": {
      "type": "object",
      "description": "DataSubject Identifiers. The DataSubject idendifier accountEmail or msisdn is required and will be vaidated in the code.",
	  "required": [
        "accountEmail"
      ],      
      "properties": {              
        "accountNumber": {
          "description": "Customer Account Number like BAN/FAN or TVision AccountID",
          "type": "string",
          "pattern": "^.*$",
          "example": "986756742",
          "minLength": 0,
          "maxLength": 50
        },
        "accountEmail": {
          "description": "Customer Account Primary Email",
          "type": "string",
          "pattern": "^.*$",
          "example": "janedoe@gmail.com",
          "minLength": 0,
          "maxLength": 255
        },
        "msisdn": {
          "description": "Customer MSISDN",
		  "type": "string",
          "pattern": "^[0-9]*$",
          "minLength": 0,
          "maxLength": 10,
          "example": "2068795647"
        },
		"accountZipCode": {
          "description": "The zip code that may be used on ​T-Mobile paper-mail communications",
		  "type": "string",
          "pattern": "^[0-9]*$",
          "minLength": 0,
          "maxLength": 5,
          "example": "98021"
        }
      }
    },
    "DataSubjectResponse": {
      "type": "object",
      "description": "Data Subject Request acknowledgement",
      "required": [
        "requestId",
		"subTaskId"
      ],
      "properties": {
		"requestId": {
          "description": "Unique DSR request ID",
          "type": "string",
          "pattern": "^.*$",  
          "example": "4237583948575",
          "minLength": 0,
          "maxLength": 100
        },
		"subTaskId": {
          "description": "Unique Sub Task ID for the DSR request",
          "type": "string",
          "pattern": "^.*$",
          "example": "83948575",
          "minLength": 0,
          "maxLength": 100
        }
	  }
    },	
	"Address": {
      "type": "object",
      "description": "Address",
      "required": [
        "addressLine1",
		"city",
		"state"
      ],
      "properties": {
	   "addressLine1": {
          "description": "Address Line 1",
          "type": "string",
          "pattern": "^.*$",
          "minLength": 0,
          "maxLength": 200,
          "example": "1234 Main Street"
        },
		"addressLine2": {
          "description": "Address Line 2",
          "type": "string",
          "pattern": "^.*$",
          "minLength": 0,
          "maxLength": 200,
          "example": "Suite 204"
        },
		"city": {
          "description": "City",
          "type": "string",
          "pattern": "^.*$",
          "example": "Bellevue",
          "minLength": 0,
          "maxLength": 50
        },
		"state": {
          "description": "State",
          "type": "string",
          "pattern": "^[A-Z]*$",
          "example": "WA",
          "minLength": 0,
          "maxLength": 2
        },
		"zipCode": {
          "description": "Zip code",
          "type": "string",
          "pattern": "^[0-9]*$",
          "example": "98006",
          "minLength": 0,
          "maxLength": 5
        }
      }
    },
    "Error": {
      "description": "Error object. http://api-standards.apps.px-npe01.cf.t-mobile.com/http/status-codes/error-response-format/",
      "type": "object",
      "properties": {
        "code": {
          "description": "A specific T-Mobile error code.",
          "type": "string",
          "pattern": "^.*$",
          "minLength": 0,
          "maxLength": 20
        },
        "userMessage": {
          "description": "A human-readable message describing the error.",
          "type": "string",
          "pattern": "^.*$",
          "minLength": 0,
          "maxLength": 100
        },
        "systemMessage": {
          "description": "Backend system error message.",
          "type": "string",
          "pattern": "^.*$",
          "minLength": 0,
          "maxLength": 100
        },
        "detailLink": {
          "description": "Optional link to error details",
          "type": "string",
          "pattern": "^.*$",
          "minLength": 0,
          "maxLength": 200
        }
      }
    }
  },
  "securityDefinitions": {
    "JWT": {
      "type": "apiKey",
      "description": "Must be an ID or Access token issued by IAM/Brass. JWT is a means of representing claims to be transferred between two parties. The claims in a JWT are encoded as a JSON object that is digitally signed using JSON Web Signature (JWS) and/or encrypted using JSON Web Encryption (JWE).",
      "name": "Authorization",
      "in": "header"
    }
  }
}