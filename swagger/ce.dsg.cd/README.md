# Clearwater Index for Customer Information Domain (CD)

## OpenAPI specifications (swaggers)

* [The status of the CD API prod vs nonProd is tracked here](https://qwiki.internal.t-mobile.com/display/CIM/CustomerInfo+Domain+Interfaces+Status)
* [Swaggers that are no longer needed] (https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.cd/archive)
* [Swaggers that are for internal CD use - not published to devportal] (https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.cd/internal_dont_publish)
