swagger: '2.0'
info:
  title: Metro SIGMA Order Management Service
  description: Micro services for managing Metro web orders
  version: 1.0.0
  contact:
    name: Metro Dev Team
    email: DTD_SelfService_MetroWeb@T-Mobile.com
host: sigma.corporate.tmobile.com
tags:
- name: order
  description: retrive and update order details.
- name: shipment
  description: retrive and update shipping details.
- name: activate-subscriber
  description: Operations that support subscriber activation.
schemes:
  - https
basePath: /v1/oms
parameters:
  Authorization:
    name: Authorization
    in: header
    required: true
    type: string
    description: Jwt token
    pattern: Bearer .*$
    x-example: Bearer xyz
  Rm-Token:
    name: Rm-Token
    in: header
    required: true
    type: string
    description: RM Token
    pattern: ^.*$
    x-example: xyz
  mdn:
    name: mdn
    in: header
    description: phone number
    required: true
    type: string
    pattern: ^\d{10}$
    x-example: 2146946292
  content-type:
    name: Content-Type
    in: header
    description: content type
    required: true
    type: string
    pattern: '^.*$'
    default: 'application/json;charset=UTF-8'
    x-example: application/json;charset=UTF-8
paths:
  /order:
    get:
      summary: get the order details
      description: get 
      tags:
       - order
      operationId: order
      produces:
       - application/json
      security:
       - Bearer: []
      responses:
        '200':
          description: Order Object
          schema:
            $ref: '#/definitions/Order'
          examples:
            data: { "orderId": "116782" }
        '400':
          description: Bad Request.
          examples:
            data: {"code": "400", "userMessage": "Transaction failed", "systemMessage": "bad request" }
          schema:
            $ref: '#/definitions/Error'
        '403':
          description: forbidden
          examples:
            data: {"code": "500000231", "userMessage": "Transaction failed", "systemMessage": "Backend system error" }
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server  error
          examples:
            data: {"code": "500000231", "userMessage": "Transaction failed", "systemMessage": "Backend system error" }
          schema:
            $ref: '#/definitions/Error'
  /get-coupon-promo:
    post:
      x-api-pattern: ExecuteFunction
      summary: validate promo code and get additional promos
      description: validate promo code entered by user and get additional valid promos 
      tags:
      - order
      operationId: "get-coupon-promo"
      produces:
      - application/json
      security:
        - Bearer: []
      parameters:
        - $ref: '#/parameters/content-type'
        - $ref: '#/parameters/Authorization'
        - name: getCouponPromoRequest
          in: body
          required: true
          description: serialized coupon details.
          schema:
            $ref: '#/definitions/GetCouponPromoRequest'
      responses:
        '200':
          description: CouponPromo Response Object
          headers:
            Content-Type:
              type: string
              description: response content type
              default:  'application/json;charset=UTF-8'
              x-example: application/json;charset=UTF-8
            Date:
              type: string
              description: response generated time
              x-example: Wed, 07 Nov 2018 09:38:15 GMT
          examples:
            application/json:
              {
                "promoDetailsList": [
                  {
                    "applyAfterCycles": 0,
                    "campaignCode": "string",
                    "saleEffective": true,
                    "priceSheetCode": "string",
                    "priority": 0,
                    "promoLongDescription": "string",
                    "promoShortDescription": "string",
                    "promotionCode": "string",
                    "promotionType": "string"
                  }
                ]
              }
          schema:
            $ref: '#/definitions/GetCouponPromoResponse'
        '400':
          description: Bad Request.
          examples:
            data: {"code": "400", "userMessage": "Transaction failed", "systemMessage": "bad request" }
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized.
          examples:
            data: {"code": "401", "userMessage": "Unauthorized", "systemMessage": "Unauthorized" }
          schema:
            $ref: '#/definitions/Error'
        '404':
          description: Not Found
          examples:
            data: {"code": "404", "userMessage": "Not Found", "systemMessage": "Not Found" }
          schema:
            $ref: '#/definitions/Error'
        '405':
          description: method not allowed
          headers:
            Allow:
              type: string
              description: Allowed methods
              default: 'POST'
              x-example: POST
          schema:
            $ref: '#/definitions/Error'
          examples:
           data: {"code": "405", "userMessage": "Transaction failed", "systemMessage": "method not allowed" }
        '409':
          description: Conflict
          examples:
            data: {"code": "409", "userMessage": "Conflict", "systemMessage": "Conflict" }
          schema:
            $ref: '#/definitions/Error'
        '415':
          description: Unsupported Media Type
          examples:
            data: {"code": "415", "userMessage": "Unsupported Media Type", "systemMessage": "Unsupported Media Type" }
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server  error
          examples:
            data: {"code": "500000231", "userMessage": "Transaction failed", "systemMessage": "Backend system error" }
          schema:
            $ref: '#/definitions/Error'
        '503':
          description: Service Unavailable
          examples:
            data: {"code": "404", "userMessage": "Service Unavailable", "systemMessage": "Service Unavailable" }
          schema:
            $ref: '#/definitions/Error'
  /shipment:
    get:
      summary: get the shipment details
      description: get 
      tags:
        - shipment
      operationId: shipment
      produces:
        - application/json
      security:
        - Bearer: []
      responses:
        '200':
          description: shipment Object
          examples: 
            data: {"shipmentId": "123456"}
          schema:
            $ref: '#/definitions/Shipment'
        '400':
          description: Bad Request.
          examples:
            data: {"code": "400", "userMessage": "Transaction failed", "systemMessage": "bad request" }
          schema:
            $ref: '#/definitions/Error'
        '403':
          description: forbidden
          examples:
            data: {"code": "500000231", "userMessage": "Transaction failed", "systemMessage": "Backend system error" }
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server  error
          examples:
            data: {"code": "500000231", "userMessage": "Transaction failed", "systemMessage": "Backend system error" }
          schema:
            $ref: '#/definitions/Error'
  /reserve-subscriber-on-new-account:
    post:
      summary: submit bundled cart order to Amdocs
      x-api-pattern: ExecuteFunction
      description: Will create an account and a reserved subscriber with a dummy IMEI and SIM..
      tags:
        - activate-subscriber
      operationId: reserveSubscriber
      produces:
        - application/json
      security:
        - Bearer: []
      parameters:
        - $ref: '#/parameters/content-type'
        - name: reserveSubscriberRequest
          in: body
          required: true
          description: Reserve subscriber on new account request.
          schema:
            $ref: '#/definitions/ReserveSubscriberRequest'
      responses:
        '200':
          description: Successful coverage response
          headers:
            Content-Type:
              type: string
              description: response content type
              default:  'application/json;charset=UTF-8'
              x-example: application/json;charset=UTF-8
            Date:
              type: string
              description: response generated time
              x-example: Wed, 07 Nov 2018 09:38:15 GMT
          examples:
            {
              "subscriberId": "98531876539",
              "mdn": "4086259547",
              "accountNumber": 940380023,
              "csmOrderNumber": 182434048
            }
          schema:
            $ref: '#/definitions/ReserveSubscriberResponse'
        '400':
          description: Bad Request. Make sure required parameters are provided.
          schema:
            $ref: '#/definitions/Error'
          examples:
            data: {"code": "400", "userMessage": "Transaction failed", "systemMessage": "bad request" }
        '401':
          description: unauthorized request
          schema:
            $ref: '#/definitions/Error'
          examples:
            data: {"code": "401", "userMessage": "Transaction failed", "systemMessage": "unauthorized request" }
        '404':
          description: not found
          schema:
            $ref: '#/definitions/Error'
          examples:
            data: {"code": "404", "userMessage": "Transaction failed", "systemMessage": "not found" }
        '405':
          headers:
            Allow:
              type: string
              description: Allowed methods
              default: 'POST'
              x-example: POST
          description: method not allowed
          schema:
            $ref: '#/definitions/Error'
          examples:
            data: {"code": "405", "userMessage": "Transaction failed", "systemMessage": "method not allowed" }
        '409':
          description: conflict
          schema:
            $ref: '#/definitions/Error'
          examples:
            data: {"code": "409", "userMessage": "Transaction failed", "systemMessage": "conflict" }
        '415':
          description: Unsupported media type
          schema:
            $ref: '#/definitions/Error'
          examples:
            data: {"code": "415", "userMessage": "Transaction failed", "systemMessage": "Unsupported media type" }
        '500':
          description: Internal Server  error
          schema:
            $ref: '#/definitions/Error'
          examples:
            data: {"code": "500", "userMessage": "Transaction failed", "systemMessage": "Internal Server  error" }
        '503':
          description: service unavailable
          schema:
            $ref: '#/definitions/Error'
          examples:
            data: {"code": "503", "userMessage": "Transaction failed", "systemMessage": "service unavailable" }
            
  /inventory-check:
    post:
      summary: validate inventory for SAP,avaliable or Not
      x-api-pattern: ExecuteFunction
      description: validate the availability of the product 
      tags:
      - inventory
      operationId: "inventoryCheck"
      produces:
      - application/json
      security:
        - Bearer: []
      parameters:
        - $ref: '#/parameters/content-type'
        - name: inventoryRequest
          in: body
          required: true
          description: serialized inventory details.
          schema:
            $ref: '#/definitions/InventoryRequest'
      responses:
        '200':
          description: Get Inventory Response Object
          headers:
            Content-Type:
              type: string
              description: response content type
              default:  'application/json;charset=UTF-8'
              x-example: application/json;charset=UTF-8
            Date:
              type: string
              description: response generated time
              x-example: Wed, 07 Jul 2019 09:38:15 GMT
          examples:
            application/json:
              {
                "List": [
                  {
                    "materialNumber": "string",
                    "package": 1,
                    "group": 1,
                    "requiredQuantity": 1.0,
                    "EstShipmentDt": {
                              "availability": "string",
                              "preAuth": "string",
                              "availableQuantity": 1,
                              "ESDFrom": "string",
                              "ESDTo": "string"
                            }
                  }
                ]
              }
          schema:
            $ref: '#/definitions/GetInventoryResponse'
        '400':
          description: Bad Request.
          examples:
            data: {"code": "400", "userMessage": "Transaction failed", "systemMessage": "bad request" }
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized.
          examples:
            data: {"code": "401", "userMessage": "Unauthorized", "systemMessage": "Unauthorized" }
          schema:
            $ref: '#/definitions/Error'
        '404':
          description: Not Found
          examples:
            data: {"code": "404", "userMessage": "Not Found", "systemMessage": "Not Found" }
          schema:
            $ref: '#/definitions/Error'
        '405':
          description: method not allowed
          headers:
            Allow:
              type: string
              description: Allowed methods
              default: 'POST'
              x-example: POST
          schema:
            $ref: '#/definitions/Error'
          examples:
           data: {"code": "405", "userMessage": "Transaction failed", "systemMessage": "method not allowed" }
        '409':
          description: Conflict
          examples:
            data: {"code": "409", "userMessage": "Conflict", "systemMessage": "Conflict" }
          schema:
            $ref: '#/definitions/Error'
        '415':
          description: Unsupported Media Type
          examples:
            data: {"code": "415", "userMessage": "Unsupported Media Type", "systemMessage": "Unsupported Media Type" }
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server  error
          examples:
            data: {"code": "500000231", "userMessage": "Transaction failed", "systemMessage": "Backend system error" }
          schema:
            $ref: '#/definitions/Error'
        '503':
          description: Service Unavailable
          examples:
            data: {"code": "404", "userMessage": "Service Unavailable", "systemMessage": "Service Unavailable" }
          schema:
            $ref: '#/definitions/Error'  
securityDefinitions:
  Bearer:
    name: BearerToken
    in: header
    type: apiKey
    description: Format "Bearer <<token>>"
definitions:
  Error:
    type: object
    description: error details
    properties:
      code:
        type: string
        description: status code
      userMessage:
        type: string
        description: error message
      systemMessage:
        type: string
        description: error details
    example:
      {"code":"500000000","userMessage":"Transaction failed","systemMessage":"Backend system error"}
  Acknowledgement:
    type: object
    description: Acknowledgement for transaction success
    properties:
      message:
        type: string
        description: Acknowledgement for transaction success
  Order:
    type: object
    properties:
      orderId:
        type: number
        format: double
        description: Displays the Order Id.
  Shipment:
    type: object
    properties:
      shipmentId:
        type: number
        format: double
        description: Displays the shipment Id.
  GetCouponPromoRequest:
    type: object
    description: Get coupon promo request object.
    properties:
      addOns: 
        type: array
        description: usage details for subscriber.
        items:
          $ref: '#/definitions/AddOn'
      couponCode:
        type: string
        description: coupon code.
        pattern: ^.{6,16}$
        example: "NEUBYO"
      itemId:
        type: string
        description: itemId
        pattern: '^.*$'
        example: "610214637673"
      pricePlanCode:
        type: string
        description: Price plan code.
        example: '20180510'
      promoList:
        type: array
        description: usage details for subscriber.
        items:
          $ref: '#/definitions/Promo'
      submarket:
        type: string
        description: Submarket of the corresponding NPA-NXX
        example: "060"
      zipCode:
        type: string
        description: zip code
        pattern: '^\d{5}$'
        example: 75082
  GetCouponPromoResponse:
    type: object
    description: coupon promo response
    properties:
      promoDetailsList:
        type: array
        description: list of promotions.
        items:
          $ref: '#/definitions/PromoDetails'
  AddOn:
    type: object
    description: Addon services
    properties:
      soc:
        type: string
        pattern: '^.*$'
        description: soc
        example: "50UNLMPH"
  Promo:
    type: object
    description: Addon services
    properties:
      promotionCode:
        type: string
        pattern: '^.*$'
        description: promotion code
        example: "SAVE20"
  PromoDetails:
    type: object
    description: details of promotion
    properties:
      applyAfterCycles:
        type: integer
        format: int32
        pattern: '^\d+$'
        description: Number of cycles after which promo to be applied.
        example: 2
      campaignCode:
        type: string
        pattern: '^.*$'
        description: promotion code
        example: "SAVE20"
      saleEffective:
        type: boolean
        example: false
        description: date when the promo is effective.
      priceSheetCode:
        type: string
        pattern: '^.*$'
        description: priceSheet code
        example: "UATALEFSS"
      priority:
        type: integer
        format: int32
        description: Priority of promo
        example: 1
      promoLongDescription:
        type: string
        pattern: '^.*$'
        description: long description of code
        example: "Value SOC free for 2 Months"
      promoShortDescription:
        type: string
        pattern: '^.*$'
        description: short description of code
        example: "Value SOC free for 2 Months"
      promotionCode:
        type: string
        pattern: '^.*$'
        description: promotion code
        example: "UATALEFSS"
      promotionType:
        type: string
        pattern: '^.*$'
        description: promotion code
        example: "FREESOC"
  ReserveSubscriberRequest:
     type: object
     description: Reserve subscriber on new account request object. 
     properties:
      webOrderNumber:
        type: integer
        format: int32
        example: 12345678
        description: Web order number.
      accountDetails:
        $ref: "#/definitions/ReserveSubscriberAccountDetails"
      subscriberDetails:
        $ref: "#/definitions/ReserveSubscriberDetails"
  ReserveSubscriberAccountDetails:
    type: object
    description: Account details.
    required:
    - firstName
    - lastName
    - accountType
    - accountSubtype
    - addressType
    - streetAddress
    - city
    - stateCode
    - zipCode
    - zipGeoCode
    - securityPinCode
    - securityQuestionCode
    - securityAnswer
    properties:
      firstName:
        type: string
        description: Customer's first name.
        minLength: 1
        maxLength: 30
        pattern: ^.*$
        example: John
        x-notEmpty: true
        x-notBlank: true
      lastName:
        type: string
        description: Customer's last name.
        minLength: 1
        maxLength: 30
        pattern: ^.*$
        example: John
        x-notEmpty: true
        x-notBlank: true
      emailAddress:
        type: string
        pattern: ^$|^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$
        maxLength: 100
        example: abc@amdocs.com
        description: Email address.
      accountType:
        type: string
        pattern: ^.*$
        minLength: 1
        maxLength: 1
        default: I
        example: I
        x-notEmpty: true
        description: The accountType defines specific definitions that apply to this account (E.g., for the individual account type the value 'I' is returned).
      accountSubtype:
        type: string
        pattern: ^.*$
        minLength: 1
        maxLength: 1
        default: 1
        example: 1
        x-notEmpty: true
        description: Account sub type. Consult solution lead to retrieve additional values of account sub types from the following Table and column.
      addressType:
        type: string
        description: Address Type of the Account.
        enum:
          - R
          - M
          - S
          - P
        example: S
        x-notEmpty: true
        x-validStringValues: {"R","M","S","P"}
      streetAddress:
        type: string
        pattern: ^.*$
        minLength: 1
        example: 71 Pilgrim Avenue
        x-notEmpty: true
        description: Street address.
      city:
        type: string
        pattern: ^.*$
        minLength: 2
        example: NEW YORK
        x-notEmpty: true
        description: city
      stateCode:
        description: To indicate the state code.
        type: string
        minLength: 2
        maxLength: 2
        pattern: '^[A-Za-z]{2}$'
        example: NY
        x-notEmpty: true
      zipCode:
        type: string
        minLength: 5
        maxLength: 5
        pattern: '^\d{5}$'
        example: 10003
        x-notEmpty: true
        description: Zip code. Must be five digits.
      zipGeoCode:
        type:   string
        minLength: 9
        maxLength: 9
        pattern: '^\d{9}$'
        example: 330612010
        x-notEmpty: true
        description: Zip geo code. Must be nine digits. Validated from code1 as part of the address validation performed by Web on their side.
      securityPinCode:
        type: string
        minLength: 8
        maxLength: 8
        pattern: '^\d{8}$'
        example: 12121212
        x-notEmpty: true
        description: Security Pin Code. Must be eight digits.
      securityQuestionCode:
        type: string
        pattern: ^.*$
        minLength: 1
        example: Q11
        x-notEmpty: true
        description: Account authentication security question code.
      securityAnswer:
        type: string
        pattern: ^.*$
        minLength: 2
        maxLength: 20
        example: CAT
        x-notEmpty: true
        description: The security answer for the security question.
  ReserveSubscriberDetails:
    type: object
    description: Subscriber details.
    required:
      - firstName
      - lastName
      - emailAddress
      - emailMarketingPreferences
      - addressType
      - streetAddress
      - city
      - stateCode
      - zipCode
      - zipGeoCode
      - itemId
      - planDetails
    properties:
      firstName:
        type: string
        description: Customer's first name.
        minLength: 1
        maxLength: 30
        pattern: ^.*$
        example: John
        x-notEmpty: true
        x-notBlank: true
      lastName:
        type: string
        description: Customer's last name.
        minLength: 1
        maxLength: 30
        pattern: ^.*$
        example: John
        x-notEmpty: true
        x-notBlank: true
      emailAddress:
        type: string
        pattern: ^$|^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$
        maxLength: 100
        example: abc@amdocs.com
        description: Email address.
      emailMarketingPreferences:
        type: boolean
        example: example
        description: Email marketing preferences indicator.
      addressType:
        description: Address Type of account.
        type: string
        enum:
          - R
          - M
          - S
          - P
        example: S
        x-notEmpty: true
        x-validStringValues: {"R","M","S","P"}
      streetAddress:
        type: string
        pattern: ^.*$
        minLength: 1
        example: 71 Pilgrim Avenue
        x-notEmpty: true
        description: Street address.
      city:
        type: string
        pattern: ^.*$
        minLength: 1
        example: NEW YORK
        x-notEmpty: true
        description: city
      stateCode:
        description: To indicate the state code.
        type: string
        pattern: '^[A-Za-z]{2}$'
        example: NY
        x-notEmpty: true
      zipCode:
        type: string
        minLength: 5
        maxLength: 5
        pattern: '^\d{5}$'
        example: 10003
        x-notEmpty: true
        description: Zip code. Must be five digits.
      zipGeoCode:
        type: string
        minLength: 9
        maxLength: 9
        pattern: '^\d{9}$'
        example: 330612010
        x-notEmpty: true
        description: Zip geo code. Must be nine digits. Validated from code1 as part of the address validation performed by Web on their side.
      itemId:
        type: string
        pattern: ^.*$
        example: 5TRADEIN
        x-notEmpty: true
        description: Unique identification of the device model.
      portInMdn:
        type: string
        pattern: ^.*$
        minLength: 10
        maxLength: 10
        example: 3374433064
        description: Port in subscriber's phone number. Mandatory only in the event of a port-in. In this phase, the port-in MDN is not expected and will not be saved if populated.
      planDetails:
        $ref: '#/definitions/ReserveSubscriberPlanDetails'
      addOnDetails:
        $ref: '#/definitions/ReserveSubscriberAddOnDetails'
      promotionDetails:
        $ref: '#/definitions/ReserveSubscriberPromotionDetails'
      couponDetails:
        $ref: '#/definitions/ReserveSubscriberCouponDetails'
  ReserveSubscriberPlanDetails:
    type: object
    required:
      - pricePlanCode
    description: Plan details.
    properties:
      offerCode:
        type: string
        pattern: ^.*$
        example: PCFTR40F
        x-notEmpty: true
        description: Offer code.
      pricePlanCode:
        type: string
        pattern: ^.*$
        description: Price plan code.
        example: 3F90CFTR
        x-notEmpty: true
      priceSheetCode:
        type: string
        pattern: ^.*$
        example: PCFTR40F
        x-notEmpty: true
        description: Price sheet code.
  ReserveSubscriberAddOnDetails:
    type: object
    description: Add-On details.
    properties:
      addOnCode:
        type: string
        description: Addon code.
        example: AIRG3
        x-notEmpty: true
  ReserveSubscriberPromotionDetails:
    type: object
    description: Promotion details.
    properties:
      promotionCode:
        type: string
        example: COUP_NEUAM6
        x-notEmpty: true
        description: Promotion code.
      promotionPriceSheetCode:
        type: string
        example: UPGAAL
        x-notEmpty: true
        description: Promotion price sheet code.
      promotionType:
        type: string
        example: DOFFPP
        x-notEmpty: true
        description: Promotion type.
  ReserveSubscriberCouponDetails:
    type: object
    description: Coupon details.
    properties:
      couponCode:
        type: string
        example: CPDUPPH2X3WN63TP
        description: Coupon code. 
  ReserveSubscriberResponse:
    type: object
    description: Reserve subscriber on new account response.
    properties:
      subscriberId:
        type: string
        example: 98531876539
        description: Subscriber id.
      mdn:
        description: phone number
        type: string
        pattern: ^\d{10}$
        x-example: 2146946292
      accountNumber:
        type: integer
        format: int32
        example: 940380023
        description: Account number.
      orderNumber:
        type: integer
        format: int32
        example: 940380023
        description: CSM order number.
  InventoryRequest:
    type: object
    properties:
      list: 
        description: Line Item Information for Inventory.
        type: array
        items:
          $ref: '#/definitions/ProductList'
  ProductList:
    type: object
    properties:
      materialNumber:
        description: The sku/productCode of the device offer variant.
        minLength: 3
        maxLength: 30
        type: string
        pattern: '^.*$'
        example: "SUPM50907"
      requiredQuantity:
        description: The quantity of the product being ordered on this line.
        type: string
        pattern: '^[0-9]*$'
        example: "1"
  GetInventoryResponse:
    type: object
    properties:
      inventoryDetailsList:
        description: Line Item Availibility Information for Product.
        type: array
        items:
          $ref: '#/definitions/InventoryDetailsList'
  InventoryDetailsList:
    type: object
    properties:
      materialNumber:
        description: The sku/productCode of the device offer variant.
        minLength: 3
        maxLength: 30
        type: string
        pattern: ^.*$
        example: "SUPM50907"
      package:
        description: Product belong to package .
        type: string
        pattern: ^.*$
        example: "1"
      group:
        description: Product belong to group .
        type: string
        pattern: ^.*$
        example: "1"
      requiredQuantity:
        description: The quantity of the product being ordered on this line.
        type: string
        pattern: ^.*$
        example: "1.0"
      estimatedShipmentDate:
        description: Estimated Shimpment date
        type: array
        items:
          $ref: '#/definitions/EstimatedShipmentDate'
  EstimatedShipmentDate:
   type: object
   properties:
      availability:
        description: product availability indicator.
        type: string
        minLength: 1
        maxLength: 1
        enum:
          - Y
          - N
        example: "Y"
      preAuth:
        description: preAuth indicator.
        type: string
        minLength: 1
        maxLength: 1
        enum:
          - Y
          - N
        example: "N"
      availableQuantity:
        description: The quantity of the available product 
        type: string
        minLength: 0
        maxLength: 20
        pattern: '^[0-9]*$'
        example: "1"
      estimatedDateFrom:
        description: Estimated From date
        type: string
        pattern: ^[0-9]{4}-[0-9]{2}-[0-9]{2}$
        minLength: 0
        maxLength: 10
        example: "2019-07-09"
      estimatedDateTo:
        description: Estimated To date
        type: string
        pattern: ^[0-9]{4}-[0-9]{2}-[0-9]{2}$
        minLength: 0
        maxLength: 10
        example: "2019-07-11" 
