{
  "swagger": "2.0",
  "info": {
    "version": "2.0.0",
    "title": "Anonymous Token Management Service",
    "description": "A micro service responsible for returning ID_token",
    "contact": {
      "name": "SecureID On-call Support",
      "email": "EUIDevelopment@T-Mobile.com"
    }
  },
  "host": "brass.account.t-mobile.com",
  "x-servers": "brass.account.t-mobile.com",
  "basePath": "/ats/v1",
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "tags": [
    {
      "name": "ATMS",
      "description": "Anonymous Token Management Service"
    }
  ],
  "paths": {
    "/healthcheck": {
      "get": {
        "x-api-pattern": "ExecuteFunction",
        "operationId": "requestGetHealthcheck",
        "description": "Attemts to check if healthcheck up and running",
        "tags": [
          "ATMS"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "requestBody",
            "description": "Request JSON body for healthcheck",
            "schema": {
              "$ref": "#/definitions/healthcheckRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "JSON response with user id_token",
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/healthcheckResponse"
                }
              ]
            },
            "examples": {
              "access_token": "01.USR.EG7ZSusQzR4wrXTWj",
              "access_token_type": "Bearer",
              "access_token_ttl": "3600",
              "scope": "TMO_ID_profile associated_lines extended_lines",
              "tmobileid": "660a9323b8071a2dd159c603045cf0c3464f9e796d019936c772d426aca5cacb",
              "user_id": "6cf1b969e1a538367b59b6589a3c260130dd2e5c65e617b5121485f4f915134a",
              "id_tokens": [
                {
                  "id_token.basic": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI"
                }
              ]
            }
          }
        }
      }
    },
    "/usertoken": {
      "post": {
        "x-api-pattern": "CreateInCollection",
        "operationId": "requestPostUserToken",
        "description": "Attempts to retrive ID Token",
        "tags": [
          "ATMS"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "$ref": "#/parameters/contentTypeHeader"
          },
          {
            "$ref": "#/parameters/authorizationHeader"
          },
          {
            "$ref": "#/parameters/xauthorizationHeader"
          },
          {
            "in": "body",
            "name": "requestBody",
            "description": "Request JSON body for User Token resource",
            "schema": {
              "$ref": "#/definitions/userTokenPostRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "JSON response with user id_token",
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/userTokenPostResponse"
                }
              ]
            },
            "examples": {
              "access_token": "01.USR.EG7ZSusQzR4wrXTWj",
              "access_token_type": "Bearer",
              "access_token_ttl": "3600",
              "scope": "TMO_ID_profile associated_lines extended_lines",
              "tmobileid": "660a9323b8071a2dd159c603045cf0c3464f9e796d019936c772d426aca5cacb",
              "user_id": "6cf1b969e1a538367b59b6589a3c260130dd2e5c65e617b5121485f4f915134a",
              "id_tokens": [
                {
                  "id_token.basic": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI"
                }
              ]
            }
          },
          "401": {
            "$ref": "#/responses/UnauthorizedRequest"
          },
          "403": {
            "$ref": "#/responses/InvalidRequest"
          },
          "500": {
            "$ref": "#/responses/ServerError"
          }
        }
      },
      "put": {
        "x-api-pattern": "ExecuteFunction",
        "operationId": "requestPutUserToken",
        "description": "Attempts to update ID Token",
        "x-api-pattern": "",
        "tags": [
          "ATMS"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "$ref": "#/parameters/contentTypeHeader"
          },
          {
            "$ref": "#/parameters/authorizationHeader"
          },
          {
            "$ref": "#/parameters/xauthorizationHeader"
          },
          {
            "in": "body",
            "name": "requestBody",
            "description": "Request JSON body for User Token resource",
            "schema": {
              "$ref": "#/definitions/userTokenPutRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/SuccessResponse"
          },
          "401": {
            "$ref": "#/responses/UnauthorizedRequest"
          },
          "403": {
            "$ref": "#/responses/InvalidRequest"
          },
          "500": {
            "$ref": "#/responses/ServerError"
          }
        }
      }
    }
  },
  "securityDefinitions": {
    "BasicAuth": {
      "type": "basic",
      "description": "Standard basic authentication"
    },
    "DatToken": {
      "description": "Token for authorizing the client, checked by middleware for any not whitelisted connections",
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    },
    "PopToken": {
      "description": "Token used to validate the DatToken, checked by middleware for any not whitelisted connections",
      "type": "apiKey",
      "name": "x-authorization",
      "in": "header"
    }
  },
  "definitions": {
    "healthcheckRequest": {},
    "healthcheckResponse": {
      "type": "object",
      "properties": {
        "serverState": {
          "type": "string",
          "format": "byte",
          "description": "ATMS Node server is up and running"
        }
      }
    },
    "userTokenPostRequest": {
      "type": "object",
      "description": "Request body to retrive id token",
      "required": [
        "id",
        "client_id",
        "pub_key",
        "trans_id",
        "usn"
      ],
      "properties": {
        "id": {
          "type": "string",
          "description": "Calling application authorization secret client_id",
          "example": "TestingAtms",
          "format": "string"
        },
        "client_id": {
          "type": "string",
          "description": "Calling application authorization secret client_id",
          "example": "TMOAppNative",
          "format": "string"
        },
        "trans_id": {
          "type": "string",
          "description": "Unique identifier for auditing",
          "example": "123321112",
          "format": "byte"
        },
        "usn": {
          "type": "string",
          "description": "Unique session number",
          "example": "test1234",
          "format": "byte"
        },
        "interaction_id": {
          "type": "string",
          "description": "client partner can be able to track the user session",
          "example": "test1234",
          "format": "byte"
        },
        "pub_key": {
          "type": "object",
          "required": [
            "code"
          ],
          "example": "Eydmndsmvshbvskdjvnsvdjsksdfsdvs...",
          "properties": {
            "Key_name": {
              "type": "string",
              "description": "In case of DAT not available in request header then this parameter becomes required to populate CNF value in id_tokens",
              "format": "string",
              "example": "Eydmndsmvshbvskdjvnsvdjsksdfsdvs...."
            }
          }
        }
      }
    },
    "userTokenPostResponse": {
      "type": "object",
      "properties": {
        "id_token": {
          "type": "string",
          "format": "byte",
          "description": "Salted IAM client identifier"
        }
      }
    },
    "userTokenPutRequest": {
      "type": "object",
      "required": [
        "client_id",
        "id_token",
        "pub_key",
        "trans_id"
      ],
      "properties": {
        "trans_id": {
          "type": "string",
          "description": "Unique identifier for auditing",
          "example": "ghjk677",
          "format": "byte"
        },
        "client_id": {
          "type": "string",
          "description": "Calling application authorization secret client_id",
          "example": "TMOAppNative",
          "format": "string"
        },
        "pub_key": {
          "type": "object",
          "description": "In case of DAT not available in request header then this parameter becomes required to populate CNF value in id_tokens",
          "format": "byte",
          "example": "Eydmndsmvshbvskdjvnsvdjsksdfsdvs....",
          "required": [
            "code"
          ],
          "properties": {
            "Key_name": {
              "type": "string",
              "description": "In case of DAT not available in request header then this parameter becomes required to populate CNF value in id_tokens",
              "format": "byte",
              "example": "Eydmndsmvshbvskdjvnsvdjsksdfsdvs...."
            }
          }
        },
        "id_token": {
          "type": "string",
          "description": "If you pass id_token will get new id_token for new client id",
          "format": "byte",
          "example": "Eydmndsmvshbvskdjvnsvdjsksdfsdvs...."
        }
      }
    },
    "userTokenPutResponse": {
      "type": "object",
      "properties": {
        "id_token": {
          "type": "string",
          "format": "byte",
          "description": "Salted IAM client identifier"
        }
      }
    },
    "ErrorModel": {
      "type": "object",
      "properties": {
        "error": {
          "type": "string",
          "format": "byte",
          "description": "invalid_request",
          "example": "Invalid Request"
        },
        "error_description": {
          "type": "string",
          "format": "byte",
          "description": "Invalid oauth parameters",
          "example": "Invalid oauth parameters"
        }
      },
      "required": [
        "error",
        "error_description"
      ]
    }
  },
  "parameters": {
    "contentTypeHeader": {
      "in": "header",
      "name": "content-type",
      "type": "string",
      "description": "Type of the returned content",
      "required": true,
      "format": "application/json",
      "x-example": "application/json"
    },
    "authorizationHeader": {
      "in": "header",
      "name": "authorization",
      "type": "string",
      "description": "JWT token returned by the authorization service",
      "required": true,
      "format": "authorization",
      "x-example": "Bearer asdasdasd. . ."
    },
    "xauthorizationHeader": {
      "in": "header",
      "name": "x-authorization",
      "type": "string",
      "description": "JWT token returned by the authorization service",
      "required": false,
      "format": "x-authorization",
      "x-example": "Bearer asdasdasd. . ."
    }
  },
  "responses": {
    "SuccessResponse": {
      "description": "JSON response with user id Tokens",
      "schema": {
        "$ref": "#/definitions/userTokenPutResponse"
      },
      "examples": {
        "id_tokens": [
          {
            "id_token.basic": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI"
          }
        ]
      }
    },
    "UnauthorizedRequest": {
      "description": "incoming request doesn't have authorization header or invalid apigee token",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "error": "Unauthorized",
        "error_description": "Unauthorized client"
      }
    },
    "InvalidRequest": {
      "description": "incoming request doesn't have require parameters",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "error": "invalid_request",
        "error_description": "Invalid oauth parameters"
      }
    },
    "ServerError": {
      "description": "JSON response in case of internal system error, in the event of downstream systems unavailability or uncaught exceptions",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "error": "internal_server_error",
        "error_description": "Sorry, we are currently experiencing problems with our server. Please try again later."
      }
    }
  }
}
