{
  "swagger": "2.0",
  "info": {
    "version": "2.0.0",
    "title": "Token Management Service",
    "description": "A micro service responsible for returning user JWT and information about line and user profiles",
    "contact": {
      "name": "SecureID On-call Support",
      "email": "ajit.ravindran2@t-mobile.com"
    }
  },
  "host": "brass.account.t-mobile.com",
  "x-servers": "brass.account.t-mobile.com",
  "basePath": "/tms/v2",
  "schemes": ["https"],
  "consumes": ["application/json"],
  "produces": ["application/json"],
  "tags": [
    {
      "name": "TMS",
      "description": "Token Management Service"
    }
  ],
  "paths": {
    "/usertoken": {
      "post": {
        "x-api-pattern": "ExecuteFunction",
        "operationId": "requestUserToken",
        "tags": ["TMS"],
        "security": [
          {},
          {
            "BasicAuth": []
          },
          {
            "DatToken": []
          },
          {
            "PopToken": []
          }
        ],
        "description": "Returns user JWT, line and user profile information",
        "summary": "Get authorization token for user",
        "parameters": [
          {
            "$ref": "#/parameters/authorizationHeader"
          },
          {
            "$ref": "#/parameters/contentTypeHeader"
          },
          {
            "in": "body",
            "name": "requestBody",
            "description": "Request JSON body for User Token resource",
            "schema": {
              "$ref": "#/definitions/UserTokenRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "JSON response with user JWT, response elements from IAM API(s) /token and /userinfo",
            "headers": {
              "date": {
                "type": "string",
                "format": "date-time",
                "description": "date of transaction",
                "x-example": "2002-09-24+06:00"
              },
              "content-type": {
                "type": "string",
                "format": "application/json",
                "description": "type of the returned content",
                "x-example": "application/json"
              },
              "cache-control": {
                "type": "string",
                "description": "date of transaction",
                "x-example": "public"
              }
            },
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/AccessTokenPartial"
                },
                {
                  "$ref": "#/definitions/UserToken"
                },
                {
                  "$ref": "#/definitions/IamProfile"
                },
                {
                  "$ref": "#/definitions/ExtendedLine"
                }
              ]
            },
            "examples": {
              "accessToken": "JWTTOKEN.THINGS=",
              "accessTokenType": "online",
              "accessTokenExpires": "600",
              "userToken": "BAS64ENCODEDCREDENTIAL"
            }
          },
          "206": {
            "description": "Partial response in the event of failure to return ID token and/or user info (if requested)",
            "schema": {
              "$ref": "#/definitions/AccessTokenPartial"
            },
            "examples": {
              "accessToken": "JWTTOKEN.THINGS=",
              "accessTokenType": "online",
              "accessTokenExpires": "600"
            }
          },
          "400": {
            "$ref": "#/responses/BadRequest"
          },
          "401": {
            "$ref": "#/responses/Unauthenticated"
          },
          "403": {
            "$ref": "#/responses/Unauthorized"
          },
          "404": {
            "$ref": "#/responses/NotFound"
          },
          "405": {
            "$ref": "#/responses/MethodNotAllowed"
          },
          "409": {
            "$ref": "#/responses/Conflict"
          },
          "415": {
            "$ref": "#/responses/UnsupportedMediaType"
          },
          "500": {
            "$ref": "#/responses/ServerError"
          },
          "503": {
            "$ref": "#/responses/ServiceUnavailable"
          }
        }
      }
    }
  },
  "securityDefinitions": {
    "BasicAuth": {
      "type": "basic",
      "description": "Standard basic authentication"
    },
    "DatToken": {
      "description": "Token for authorizing the client, checked by middleware for any not whitelisted connections",
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    },
    "PopToken": {
      "description": "Token used to validate the DatToken, checked by middleware for any not whitelisted connections",
      "type": "apiKey",
      "name": "x-authorization",
      "in": "header"
    }
  },
  "definitions": {
    "UserTokenRequest": {
      "type": "object",
      "required": ["code", "clientId", "redirectUri"],
      "properties": {
        "transId": {
          "type": "string",
          "description": "Unique identifier for auditing",
          "example": "ghjk677"
        },
        "code": {
          "type": "string",
          "description": "Access code for authorizing user token request",
          "format": "byte",
          "example": "02.tvEAH6qNps0JHRyAp"
        },
        "clientId": {
          "type": "string",
          "description": "Calling application identifier",
          "example": "TMOAppNative",
          "format": "string"
        },
        "clientSecret": {
          "type": "string",
          "description": "Calling application authorization secret",
          "format": "byte",
          "example": "TMOAppNativeSecret"
        },
        "redirectUri": {
          "type": "string",
          "description": "Client application URI",
          "format": "uri",
          "example": "https://localhost"
        },
        "isUserInfoRequired": {
          "type": "boolean",
          "description": "Should the user info be returned in response",
          "example": "false"
        },
        "scope": {
          "type": "string",
          "description": "Processes the request has claim to in IAM",
          "example": "associatedLines billingInformation TMOIDProfile openid"
        }
      }
    },
    "AccessTokenPartial": {
      "type": "object",
      "properties": {
        "accessToken": {
          "type": "string",
          "format": "bit",
          "description": "JWT token returned by the authorization service"
        },
        "accessTokenType": {
          "type": "string",
          "enum": ["online", "offline"],
          "description": "Online or Offline access type of client"
        },
        "accessTokenExpires": {
          "type": "string",
          "format": "number",
          "description": "Expiration time of the token"
        },
        "refreshToken": {
          "type": "string",
          "format": "byte",
          "description": "Provided if token type is offline; used to obtain new access token"
        },
        "scope": {
          "type": "string",
          "description": "Processes the request has claim to in IAM"
        },
        "tmobileId": {
          "type": "string",
          "format": "byte",
          "description": "Salted IAM client identifier"
        }
      }
    },
    "UserToken": {
      "type": "object",
      "properties": {
        "userToken": {
          "type": "string",
          "format": "byte",
          "description": "OpenID security token"
        }
      }
    },
    "UserInfo": {
      "type": "object",
      "properties": {
        "iamProfile": {
          "$ref": "#/definitions/IamProfile"
        },
        "extendedLine": {
          "$ref": "#/definitions/ExtendedLine"
        }
      }
    },
    "IamProfile": {
      "type": "object",
      "properties": {
        "userId": {
          "type": "string",
          "description": "Internal identifier of user"
        },
        "firstName": {
          "type": "string",
          "description": "User's first name"
        },
        "lastName": {
          "type": "string",
          "description": "User's last name"
        }
      }
    },
    "ExtendedLine": {
      "type": "object",
      "properties": {
        "billingAccountCode": {
          "type": "string",
          "description": "User billing account code"
        },
        "customers": {
          "type": "array",
          "description": "User's associated with billing account",
          "items": {
            "$ref": "#/definitions/Customer"
          }
        }
      }
    },
    "Customer": {
      "type": "object",
      "properties": {
        "billingAccountNumber": {
          "type": "string",
          "description": "User billing account identifier"
        }
      }
    },
    "ErrorModel": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string",
          "description": "succinct, domain-specific, human-readable text string to identify the type of error for the given status code",
          "example": "Resource not found",
          "format": "^[a-zA-Z\\s\\-\\']$"
        },
        "userMessage": {
          "type": "string",
          "description": "human readable text that describes the nature of the error",
          "example": "We could not authenticate that user.",
          "format": "^[a-zA-Z\\s\\.\\-\\']+$"
        },
        "systemMessage": {
          "type": "string",
          "description": "text that provides a more detailed technical explanation of the error",
          "example": "Authentication failed"
        },
        "detailLink": {
          "type": "string",
          "description": "link to custom information providing greater detail on error or errors",
          "example": "audit.log.for.events.url"
        }
      },
      "required": ["code", "userMessage"]
    }
  },
  "parameters": {
    "dateHeader": {
      "in": "header",
      "name": "date",
      "type": "string",
      "description": "datetime of transaction",
      "required": false,
      "format": "date"
    },
    "contentTypeHeader": {
      "in": "header",
      "name": "content-type",
      "type": "string",
      "description": "Type of the returned content",
      "required": true,
      "format": "application/json",
      "x-example": "application/json"
    },
    "cacheControlResponseHeader": {
      "in": "header",
      "name": "cache-control",
      "type": "string",
      "description": "Cache control response directive",
      "required": false,
      "format": "string"
    },
    "authorizationHeader": {
      "in": "header",
      "name": "authorization",
      "type": "string",
      "description": "Authorization will be in form of DAT and POP token, and is optional if connection is coming from within local network; authentication is still done with client bearer token, or clientId and clientSecret pair.",
      "required": false,
      "format": "byte",
      "x-example": "basic"
    }
  },
  "responses": {
    "BadRequest": {
      "description": "JSON response in case of missing required inbound parameters",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "code": "Bad Request",
        "userMessage": "ClientId not present in request"
      }
    },
    "Unauthenticated": {
      "description": "JSON response for invalid DAT and/or PoP tokens, untrusted sources trying to access API directly",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "code": "Unauthenticated",
        "userMessage": "Invalid authentication"
      }
    },
    "Unauthorized": {
      "description": "JSON response in case of authorization fails",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "code": "Unauthorized",
        "userMessage": "Invalid ClientId"
      }
    },
    "NotFound": {
      "description": "JSON response in case requested data cannot be found",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "code": "Not Found",
        "userMessage": "That ClientId was not found"
      }
    },
    "MethodNotAllowed": {
      "description": "JSON response in case REST method not permitted on resource",
      "headers": {
        "allow": {
          "type": "string",
          "format": "string",
          "description": "Allowed operation methods",
          "x-example": "get"
        }
      },
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "code": "Method Not Allowed",
        "userMessage": "The method received in the request-line is known by the origin server but not supported by the target resource."
      }
    },
    "NotAcceptable": {
      "description": "JSON response in case response content type will not be accepted by the consumer",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "code": "Not Acceptable",
        "userMessage": "The target resource does not have a current representation that would be acceptable to the user agent, according to the proactive negotiation header fields received in the request, and the server is unwilling to supply a default representation."
      }
    },
    "Conflict": {
      "description": "JSON response that indicates the request could not be processed because of conflict in the request, such as an edit conflict.",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "code": "Conflict",
        "userMessage": "The request could not be completed due to a conflict with the current state of the resource."
      }
    },
    "UnsupportedMediaType": {
      "description": "JSON response that indicates the request entity has a media type which the server or resource does not support.",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "code": "Unsupported Media Type",
        "userMessage": "The server is refusing to service the request because the entity of the request is in a format not supported by the requested resource for the requested method."
      }
    },
    "ServerError": {
      "description": "JSON response in case of internal system error, in the event of downstream systems unavailability or uncaught exceptions",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "code": "Server Error",
        "userMessage": "The server encountered an unexpected condition that prevented it from fulfilling the request."
      }
    },
    "ServiceUnavailable": {
      "description": "JSON response in case the requested service is unavailable",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "code": "Service Unavailable",
        "userMessage": "The server is currently unable to handle the request due to a temporary overload or scheduled maintenance, which will likely be alleviated after some delay."
      }
    }
  }
}
