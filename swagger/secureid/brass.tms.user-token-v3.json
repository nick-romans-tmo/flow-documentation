{
  "swagger": "2.0",
  "info": {
    "version": "2.0.0",
    "title": "Token Management Service",
    "description": "A micro service responsible for returning access_token and information about line and user profiles",
    "contact": {
      "name": "SecureID On-call Support",
      "email": "EUIDevelopment@T-Mobile.com"
    }
  },
  "host": "brass.account.t-mobile.com",
  "x-servers": "brass.account.t-mobile.com",
  "basePath": "/tms/v3",
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "tags": [
    {
      "name": "TMS",
      "description": "Token Management Service"
    }
  ],
  "paths": {
    "/usertoken": {
      "post": {
        "x-api-pattern": "ExecuteFunction",
        "operationId": "requestUserToken",
        "description": "Attempts to retrive ID Token",
        "tags": [
          "TMS"
        ],
        "parameters": [
          {
            "$ref": "#/parameters/contentTypeHeader"
          },
          {
            "in": "body",
            "name": "requestBody",
            "description": "Request JSON body for User Token resource",
            "schema": {
              "$ref": "#/definitions/UserTokenRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "JSON response with user access_token, response elements from IAM API(s) /token and /userinfo",
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/AccessToken"
                },
                {
                  "$ref": "#/definitions/UserInfo"
                }
              ]
            },
            "examples": {
              "access_token": "01.USR.EG7ZSusQzR4wrXTWj",
              "access_token_type": "Bearer",
              "access_token_ttl": "600",
              "scope": "TMO_ID_profile associated_lines extended_lines",
              "tmobileid": "660a9323b8071a2dd159c603045cf0c3464f9e796d019936c772d426aca5cacb",
              "user_id": "6cf1b969e1a538367b59b6589a3c260130dd2e5c65e617b5121485f4f915134a",
              "id_tokens": [
                {
                  "id_token.basic": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI"
                }
              ],
              "userInfo": {
                "iamProfile": {
                  "email": "malik143@yopmail.com"
                },
                "associatedLines": [
                  {
                    "linkTypes": [
                      "direct"
                    ]
                  }
                ],
                "extendedLines": [
                  {
                    "customers": [
                      {
                        "userId": "U-abfc7ee7-c664-4d00-95de-e58501200d71"
                      }
                    ],
                    "contracts": [
                      {
                        "msisdn": "7142341535"
                      }
                    ]
                  }
                ]
              }
            }
          },
          "206": {
            "description": "Partial response in the event of failure to return ID token and/or user info (if requested)",
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/AccessTokenPartial"
                },
                {
                  "$ref": "#/definitions/UserInfo"
                }
              ]
            },
            "examples": {
              "access_token": "01.USR.EG7ZSusQzR4wrXTWj",
              "access_token_type": "Bearer",
              "access_token_ttl": "600",
              "scope": "TMO_ID_profile associated_lines extended_lines",
              "tmobileid": "660a9323b8071a2dd159c603045cf0c3464f9e796d019936c772d426aca5cacb",
              "error": "Not provisioned to receive id_token or problems while retrieving id_token(s)",
              "userInfo": {
                "iamProfile": {
                  "email": "malik143@yopmail.com"
                },
                "associatedLines": [
                  {
                    "linkTypes": [
                      "direct"
                    ]
                  }
                ],
                "extendedLines": [
                  {
                    "customers": [
                      {
                        "userId": "U-abfc7ee7-c664-4d00-95de-e58501200d71"
                      }
                    ],
                    "contracts": [
                      {
                        "msisdn": "7142341535"
                      }
                    ]
                  }
                ]
              }
            }
          },
          "403": {
            "$ref": "#/responses/InvalidRequest"
          },
          "500": {
            "$ref": "#/responses/ServerError"
          }
        }
      }
    }
  },
  "definitions": {
    "UserTokenRequest": {
      "type": "object",
      "required": [
        "code",
        "client_id",
        "client_secret",
        "redirect_uri",
        "trans_id"
      ],
      "properties": {
        "trans_id": {
          "type": "string",
          "description": "Unique identifier for auditing",
          "example": "ghjk677"
        },
        "code": {
          "type": "string",
          "description": "Access code for authorizing user token request",
          "format": "byte",
          "example": "01.GzsAyyLLy21SGETwE"
        },
        "client_id": {
          "type": "string",
          "description": "Calling application authorization secret client_id",
          "example": "TMOAppNative",
          "format": "string"
        },
        "client_secret": {
          "type": "string",
          "description": "Calling application authorization client_secret",
          "format": "byte",
          "example": "TMOAppNativeSecret"
        },
        "redirect_uri": {
          "type": "string",
          "description": "Client application URI",
          "format": "uri",
          "example": "https://localhost"
        },
        "scope": {
          "type": "string",
          "description": "Processes the request has claim to in IAM",
          "example": "associatedLines billingInformation TMOIDProfile openid"
        },
        "response_selection": {
          "type": "string",
          "description": "if you pass response_selection will get id_tokens, if you pass userinfo in response_selection will get user profile",
          "example": "id_token.basic userinfo"
        }
      }
    },
    "AccessToken": {
      "type": "object",
      "properties": {
        "access_token": {
          "type": "string",
          "format": "bit",
          "description": "access_token returned by the authorization service"
        },
        "access_token_type": {
          "type": "string",
          "format": "byte",
          "description": "Access type of client"
        },
        "access_token_ttl": {
          "type": "string",
          "format": "number",
          "description": "Expiration time of the token"
        },
        "scope": {
          "type": "string",
          "description": "Processes the request has claim to in IAM"
        },
        "tmobileid": {
          "type": "string",
          "format": "byte",
          "description": "Salted IAM client identifier"
        },
        "user_id": {
          "type": "string",
          "format": "byte",
          "description": "user_id for the user"
        },
        "id_tokens": {
          "type": "array",
          "description": "id_token for the user",
          "items": {
            "type": "object",
            "properties": {
              "id_token.basic": {
                "type": "string",
                "format": "bit",
                "description": "JWT token returned by the authorization service"
              }
            }
          }
        }
      }
    },
    "AccessTokenPartial": {
      "type": "object",
      "properties": {
        "access_token": {
          "type": "string",
          "format": "bit",
          "description": "access_token returned by the authorization service"
        },
        "access_token_type": {
          "type": "string",
          "format": "byte",
          "description": "Access type of client"
        },
        "access_token_ttl": {
          "type": "string",
          "format": "number",
          "description": "Expiration time of the token"
        },
        "scope": {
          "type": "string",
          "description": "Processes the request has claim to in IAM"
        },
        "tmobileid": {
          "type": "string",
          "format": "byte",
          "description": "Salted IAM client identifier"
        },
        "error": {
          "type": "string",
          "format": "byte",
          "description": "Not provisioned to receive id_token or problems while retrieving id_token(s)"
        }
      }
    },
    "UserInfo": {
      "type": "object",
      "properties": {
        "userInfo": {
          "type": "object",
          "properties": {
            "iamProfile": {
              "$ref": "#/definitions/IamProfile"
            },
            "associatedLines": {
              "$ref": "#/definitions/AssociatedLines"
            },
            "extendedLines": {
              "$ref": "#/definitions/ExtendedLines"
            }
          }
        }
      }
    },
    "IamProfile": {
      "type": "object",
      "properties": {
        "email": {
          "type": "string",
          "description": "User's email"
        },
        "userId": {
          "type": "string",
          "description": "Internal identifier of user"
        },
        "firstName": {
          "type": "string",
          "description": "User's first name"
        },
        "lastName": {
          "type": "string",
          "description": "User's last name"
        }
      }
    },
    "AssociatedLines": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "linkTypes": {
            "type": "array",
            "items": {
              "type": "string"
            }
          }
        }
      }
    },
    "ExtendedLines": {
      "type": "object",
      "properties": {
        "billingAccountCode": {
          "type": "string",
          "description": "User billing account code"
        },
        "customers": {
          "type": "array",
          "description": "User's associated with billing account",
          "items": {
            "$ref": "#/definitions/Customers"
          }
        },
        "contracts": {
          "type": "array",
          "description": "User's associated with billing account",
          "items": {
            "$ref": "#/definitions/Contracts"
          }
        }
      }
    },
    "Customers": {
      "type": "object",
      "properties": {
        "billingAccountNumber": {
          "type": "string",
          "description": "User billing account identifier"
        }
      }
    },
    "Contracts": {
      "type": "object",
      "properties": {
        "msisdn": {
          "type": "string",
          "description": "User billing account identifier"
        }
      }
    },
    "ErrorModel": {
      "type": "object",
      "properties": {
        "error": {
          "type": "string",
          "description": "invalid_request"
        },
        "error_description": {
          "type": "string",
          "description": "Invalid oauth parameters"
        }
      },
      "required": [
        "error",
        "error_description"
      ]
    }
  },
  "parameters": {
    "contentTypeHeader": {
      "in": "header",
      "name": "content-type",
      "type": "string",
      "description": "Type of the returned content",
      "required": true,
      "format": "application/json",
      "x-example": "application/json"
    }
  },
  "responses": {
    "InvalidRequest": {
      "description": "incoming request doesn't have require parameters",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "error": "invalid_request",
        "error_description": "Invalid oauth parameters"
      }
    },
    "ServerError": {
      "description": "JSON response in case of internal system error, in the event of downstream systems unavailability or uncaught exceptions",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "error": "internal_server_error",
        "error_description": "Sorry, we are currently experiencing problems with our server. Please try again later."
      }
    }
  }
}
