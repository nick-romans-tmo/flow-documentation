{
  "swagger": "2.0",
  "info": {
    "version": "1.0.0",
    "title": "BRASS Authentication Facade Service",
    "description": "A micro service for simplifying authentication methods. ",
    "contact": {
      "name": "SecureID Support",
      "email": "SecureID_Support@T-Mobile.com"
    }
  },
  "host": "brass.account.t-mobile.com",
  "x-servers": [
    {
      "url": "https://brass.account.t-mobile.com"
    },
    {
      "url": "https://{Environment}brass.account.t-mobile.com",
      "variables": {
        "Environment": {
          "enum": [
            "dev.",
            "qat.",
            "uat.",
            "plab.",
            "ppd."
          ],
          "default": "uat."
        }
      }
    }
  ],
  "basePath": "/bps/apigee/auth/v1/facade",
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "tags": [
    {
      "name": "First Factor Facade",
      "description": "Completes first factor authentication"
    },
    {
      "name": "Second Factor Facade",
      "description": "Completes second factor authentication"
    }
  ],
  "paths": {
    "/create": {
      "post": {
        "x-api-pattern": "ExecuteFunction",
        "summary": "Generates an id token that fulfills first factor authentication",
        "operationId": "create_facade",
        "description": "Generates an id token that fulfills first factor authentication",
        "tags": [
          "First Factor Facade"
        ],
        "security": [
          {
            "Basic": []
          }
        ],
        "parameters": [
          {
            "in": "header",
            "name": "Authorization",
            "type": "string",
            "description": "APIGEE Access Token",
            "required": true,
            "format": "byte",
            "x-example": "Basic mF_9.B5f-4.1JqM"
          },
          {
            "in": "header",
            "name": "Content-type",
            "type": "string",
            "description": "Request content type",
            "required": true,
            "format": "application/json",
            "x-example": "application/json"
          },
          {
            "in": "header",
            "name": "Accept",
            "type": "string",
            "description": "Acceptable media type.  Currently, only application/json is supported.",
            "required": false,
            "format": "application/json",
            "x-example": "application/json"
          },
          {
            "in": "header",
            "name": "Accept-Language",
            "type": "string",
            "description": "Acceptable Language.  Currently, only en-US is be accepted.",
            "required": false,
            "format": "application/json",
            "x-example": "en-US"
          },
          {
            "in": "body",
            "name": "requestBody",
            "description": "Request JSON body for the authenticate request.  Unless noted, request values shall be in plaintext",
            "schema": {
              "$ref": "#/definitions/createFacadeRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful response with id token attached to response.",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/idTokenResponse"
            },
            "examples": {
              "idTokens": [
                {
                  "basic": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlZVRlVSRUZVTFRJd01Uaz0ifQ.eyJpYXQiOjE1NTcxNjgxNjQsImV4cCI6MTU1NzE3MTc2MywiaXNzIjoiaHR0cHM6Ly91YXQuYnJhc3MuYWNjb3VudC50LW1vYmlsZS5jb20iLCJhdWQiOiJUTU9BcHBOYXRpdmUiLCJBVCI6IjAxLlVTUi5WUWNkNzg4c1hMdWs5SHplMyIsInN1YiI6IlUtMjNlZWE5MTMtNDkwMS00YTM5LTk2NDUtMDk0MmJmYzQ1MjMzIiwiYWNyIjoibG9hMiIsImFtciI6WyJwYXNzd29yZCJdLCJ1c24iOiI4MmI0MDJiYzQ4OTkzYmI2IiwiZW50X3R5cGUiOiJiYXNpYyIsImVudCI6eyJhY2N0IjpbeyJyIjoiQU8iLCJpZCI6IjgxMjM0NjY5IiwidHN0IjoiSVIiLCJsaW5lX2NvdW50IjoxLCJsaW5lcyI6W3sicGhudW0iOiI4MDgxMjM0NjY5IiwiciI6IkQifV19XX19.dWLfRouAejO3xKtDGLkacXBJpv0qipA2NUK8Ubjr_nx9PepZxendTxeg_VDGxnJnFAR7JGUIfpjjytSm1gOtDrFDsX4MO_8xwu0Sg7Z7WeKeQtPcLNc2Wh44BxGEhO4ekb5pWrHlJKP7vqlbuaumXNx_sbNjZBvodIOXGbfB_D2wmglcOo8U37R0NvwcxF_rUb7b8xKWEaEGa90SgkxajElwQrHv5jEFVXOZ1xoTS0zklmbaZJjDpWexhXw6k4D3wGRAF3WPVCiZA60pjl1a8U8ed8XoMO8dFUjfRx2R91J8v9bTj2t9paKE2vE53JhIjk-tx8Sv5_22YtQ_D32Pqg"
                }
              ]
            }
          },
          "202": {
            "description": "The request has been accepted for processing, but the processing has not been completed.",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            }
          },
          "400": {
            "description": "Bad Request (ie missing required parameters).",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpBadRequestResponse"
            },
            "examples": [
              {
                "code": "400",
                "systemMessage": "Bad Request",
                "userMessage": "The request is missing required parameters."
              },
              {
                "code": "400",
                "systemMessage": "invalid_request",
                "userMessage": "Uh-oh, it looks like we have our wires crossed. Please try again later."
              },
              {
                "code": "400",
                "systemMessage": "iam_account_hard_locked",
                "userMessage": "We’ve temporarily locked your account to protect your information. Please call Customer Care at 1-800-937-8997 or wait 24 hours to regain account access."
              },
              {
                "code": "400",
                "systemMessage": "iam_account_soft_locked",
                "userMessage": "To protect your security, we have temporarily locked your account. To access your account, wait 24 hours or Reset your password now."
              },
              {
                "code": "400",
                "systemMessage": "invalid_scope",
                "userMessage": "The requested scope is invalid, unknown, or malformed."
              },
              {
                "code": "400",
                "systemMessage": "user_not_match",
                "userMessage": "The user id in the access token doesn’t match the user in the session or user_id parameter"
              },
              {
                "code": "400",
                "systemMessage": "no_available_authentication_methods",
                "userMessage": "Please try again later, or call your Team of Experts at 1-800-937-8997 if you’d like help managing your account."
              }
            ]
          },
          "401": {
            "description": "Unauthorized Request (ie authorization token could not be validated).",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpAuthErrorResponse"
            },
            "examples": {
              "code": "400",
              "systemMessage": "Unauthorized",
              "userMessage": "You request could not be properly validated."
            }
          },
          "404": {
            "description": "The provided msisdn or BAN was not found",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpAccountNotFoundErrorResponse"
            },
            "examples": [
              {
                "code": "404",
                "systemMessage": "Not Found",
                "userMessage": "The provided msisdn or BAN was not found"
              },
              {
                "code": "404",
                "systemMessage": "iam_account_not_found",
                "userMessage": "Make sure your 10 digit phone number is correct."
              }
            ]
          },
          "405": {
            "description": "Method not Allowed",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              },
              "Allow": {
                "type": "string",
                "format": "application/json",
                "description": "Acceptable MIME-type",
                "x-example": "POST"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpErrorResponse"
            },
            "examples": {
              "code": "405",
              "systemMessage": "Method not Allowed",
              "userMessage": "Allow: POST"
            }
          },
          "409": {
            "description": "Conflict",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpErrorResponse"
            },
            "examples": {
              "code": "405",
              "systemMessage": "Method not Allowed",
              "userMessage": "Allow: POST"
            }
          },
          "415": {
            "description": "Unsupported Media Type",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpErrorResponse"
            },
            "examples": {
              "code": "415",
              "systemMessage": "Unsupported Media type",
              "userMessage": "The Content-type provided is incorrect."
            }
          },
          "500": {
            "description": "Internal Server Error",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpErrorResponse"
            },
            "examples": {
              "code": "500",
              "systemMessage": "Internal Server Error",
              "userMessage": "The server encountered an unexpected condition that prevented it from fulfilling the request"
            }
          },
          "503": {
            "description": "Service Unavailable",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpErrorResponse"
            },
            "examples": {
              "code": "503",
              "systemMessage": "Service Unavailable",
              "userMessage": "The server is currently unable to handle the request due to a temporary overload or scheduled maintenance"
            }
          }
        }
      }
    },
    "/update": {
      "post": {
        "x-api-pattern": "ExecuteFunction",
        "summary": "Generates an id token that fulfills second factor authentication",
        "operationId": "update_facade",
        "description": "Generates an id token that fulfills second factor authentication",
        "tags": [
          "Second Factor Facade"
        ],
        "security": [
          {
            "Basic": []
          }
        ],
        "parameters": [
          {
            "in": "header",
            "name": "authorization",
            "type": "string",
            "description": "APIGEE Access Token",
            "required": true,
            "format": "byte",
            "x-example": "Basic mF_9.B5f-4.1JqM"
          },
          {
            "in": "header",
            "name": "Content-type",
            "type": "string",
            "description": "Request content type",
            "required": true,
            "format": "application/json",
            "x-example": "application/json"
          },
          {
            "in": "header",
            "name": "Accept",
            "type": "string",
            "description": "Acceptable media type.  Currently, only application/json is supported.",
            "required": false,
            "format": "application/json",
            "x-example": "application/json"
          },
          {
            "in": "header",
            "name": "Accept-Language",
            "type": "string",
            "description": "Acceptable Language.  Currently, only en-US is be accepted.",
            "required": false,
            "format": "application/json",
            "x-example": "en-US"
          },
          {
            "in": "body",
            "name": "requestBody",
            "description": "Request JSON body for the authenticate request.  Unless noted, request values shall be in plaintext",
            "schema": {
              "$ref": "#/definitions/updateFacadeRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful response with id token attached to response.",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/idTokenResponse"
            },
            "examples": {
              "idTokens": [
                {
                  "basic": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlZVRlVSRUZVTFRJd01Uaz0ifQ.eyJpYXQiOjE1NTcxNjgxNjQsImV4cCI6MTU1NzE3MTc2MywiaXNzIjoiaHR0cHM6Ly91YXQuYnJhc3MuYWNjb3VudC50LW1vYmlsZS5jb20iLCJhdWQiOiJUTU9BcHBOYXRpdmUiLCJBVCI6IjAxLlVTUi5WUWNkNzg4c1hMdWs5SHplMyIsInN1YiI6IlUtMjNlZWE5MTMtNDkwMS00YTM5LTk2NDUtMDk0MmJmYzQ1MjMzIiwiYWNyIjoibG9hMiIsImFtciI6WyJwYXNzd29yZCJdLCJ1c24iOiI4MmI0MDJiYzQ4OTkzYmI2IiwiZW50X3R5cGUiOiJiYXNpYyIsImVudCI6eyJhY2N0IjpbeyJyIjoiQU8iLCJpZCI6IjgxMjM0NjY5IiwidHN0IjoiSVIiLCJsaW5lX2NvdW50IjoxLCJsaW5lcyI6W3sicGhudW0iOiI4MDgxMjM0NjY5IiwiciI6IkQifV19XX19.dWLfRouAejO3xKtDGLkacXBJpv0qipA2NUK8Ubjr_nx9PepZxendTxeg_VDGxnJnFAR7JGUIfpjjytSm1gOtDrFDsX4MO_8xwu0Sg7Z7WeKeQtPcLNc2Wh44BxGEhO4ekb5pWrHlJKP7vqlbuaumXNx_sbNjZBvodIOXGbfB_D2wmglcOo8U37R0NvwcxF_rUb7b8xKWEaEGa90SgkxajElwQrHv5jEFVXOZ1xoTS0zklmbaZJjDpWexhXw6k4D3wGRAF3WPVCiZA60pjl1a8U8ed8XoMO8dFUjfRx2R91J8v9bTj2t9paKE2vE53JhIjk-tx8Sv5_22YtQ_D32Pqg"
                }
              ]
            }
          },
          "202": {
            "description": "The request has been accepted for processing, but the processing has not been completed.",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            }
          },
          "400": {
            "description": "Bad Request (ie missing required parameters).",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpBadRequestResponse"
            },
            "examples": [
              {
                "code": "400",
                "systemMessage": "Bad Request",
                "userMessage": "The request is missing required parameters."
              },
              {
                "code": "400",
                "systemMessage": "iam_account_hard_locked",
                "userMessage": "We’ve temporarily locked your account to protect your information. Please call Customer Care at 1-800-937-8997 or wait 24 hours to regain account access."
              },
              {
                "code": "400",
                "systemMessage": "iam_account_soft_locked",
                "userMessage": "To protect your security, we have temporarily locked your account. To access your account, wait 24 hours or Reset your password now."
              },
              {
                "code": "400",
                "systemMessage": "invalid_scope",
                "userMessage": "The requested scope is invalid, unknown, or malformed."
              },
              {
                "code": "400",
                "systemMessage": "user_not_match",
                "userMessage": "The user id in the access token doesn’t match the user in the session or user_id parameter"
              }
            ]
          },
          "401": {
            "description": "Unauthorized Request (ie authorization token could not be validated).",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpAuthErrorResponse"
            },
            "examples": {
              "code": "401",
              "systemMessage": "Unauthorized",
              "userMessage": "You request could not be properly validated."
            }
          },
          "404": {
            "description": "The provided msisdn or BAN was not found",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpAccountNotFoundErrorResponse"
            },
            "examples": [
              {
                "code": "404",
                "systemMessage": "Not Found",
                "userMessage": "The provided msisdn or BAN was not found"
              },
              {
                "code": "404",
                "systemMessage": "iam_account_not_found",
                "userMessage": "Make sure your 10 digit phone number is correct."
              }
            ]
          },
          "405": {
            "description": "Method not Allowed",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              },
              "Allow": {
                "type": "string",
                "format": "application/json",
                "description": "Acceptable MIME-type",
                "x-example": "POST"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpErrorResponse"
            },
            "examples": {
              "code": "405",
              "systemMessage": "Method not Allowed",
              "userMessage": "Allow: POST"
            }
          },
          "409": {
            "description": "Conflict",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpErrorResponse"
            },
            "examples": {
              "code": "405",
              "systemMessage": "Method not Allowed",
              "userMessage": "Allow: POST"
            }
          },
          "415": {
            "description": "Unsupported Media Type",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpErrorResponse"
            },
            "examples": {
              "code": "415",
              "systemMessage": "Unsupported Media type",
              "userMessage": "The Content-type provided is incorrect."
            }
          },
          "500": {
            "description": "Internal Server Error",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpErrorResponse"
            },
            "examples": {
              "code": "500",
              "systemMessage": "Internal Server Error",
              "userMessage": "The server encountered an unexpected condition that prevented it from fulfilling the request"
            }
          },
          "503": {
            "description": "Service Unavailable",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpErrorResponse"
            },
            "examples": {
              "code": "503",
              "systemMessage": "Service Unavailable",
              "userMessage": "The server is currently unable to handle the request due to a temporary overload or scheduled maintenance"
            }
          }
        }
      }
    }
  },
  "securityDefinitions": {
    "Basic": {
      "type": "basic",
      "description": "Standard basic authentication"
    }
  },
  "definitions": {
    "createFacadeRequest": {
      "description": "The request format for retrieving an id token",
      "type": "object",
      "required": [
        "msisdn",
        "ban",
        "clientId",
        "responseSelection"
      ],
      "properties": {
        "msisdn": {
          "type": "string",
          "format": "string",
          "description": "The MSISDN",
          "example": "8085557890"
        },
        "ban": {
          "type": "string",
          "format": "string",
          "description": "The Billing Account number, used for dynamically linking MSISDN to an IAM profile with the same BAN.",
          "example": "678345123"
        },
        "clientId": {
          "type": "string",
          "format": "string",
          "description": "IAM-issued client id.  Please onboard a client id with IAM prior to making this API call.",
          "example": "metro-edge"
        },
        "responseSelection": {
          "type": "string",
          "format": "string",
          "description": "Space-delimited list of id tokens requested by client. Valid values: id_token.basic",
          "example": "id_token.basic"
        }
      }
    },
    "updateFacadeRequest": {
      "description": "Request for generating second factor authentication id token",
      "type": "object",
      "required": [
        "msisdn",
        "ban",
        "clientId",
        "responseSelection"
      ],
      "properties": {
        "msisdn": {
          "type": "string",
          "format": "string",
          "description": "The msisdn to verify answers against.",
          "example": "8085551234"
        },
        "ban": {
          "type": "string",
          "format": "string",
          "description": "The billing account number.",
          "example": "678345123"
        },
        "idToken": {
          "type": "string",
          "format": "string",
          "description": "The id token.  Must be valid and tied to the MSISDN provided.",
          "example": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlZVRlVSRUZVTFRJd01Uaz0ifQ.eyJpYXQiOjE1NTcxNjgxNjQsImV4cCI6MTU1NzE3MTc2MywiaXNzIjoiaHR0cHM6Ly91YXQuYnJhc3MuYWNjb3VudC50LW1vYmlsZS5jb20iLCJhdWQiOiJUTU9BcHBOYXRpdmUiLCJBVCI6IjAxLlVTUi5WUWNkNzg4c1hMdWs5SHplMyIsInN1YiI6IlUtMjNlZWE5MTMtNDkwMS00YTM5LTk2NDUtMDk0MmJmYzQ1MjMzIiwiYWNyIjoibG9hMiIsImFtciI6WyJwYXNzd29yZCJdLCJ1c24iOiI4MmI0MDJiYzQ4OTkzYmI2IiwiZW50X3R5cGUiOiJiYXNpYyIsImVudCI6eyJhY2N0IjpbeyJyIjoiQU8iLCJpZCI6IjgxMjM0NjY5IiwidHN0IjoiSVIiLCJsaW5lX2NvdW50IjoxLCJsaW5lcyI6W3sicGhudW0iOiI4MDgxMjM0NjY5IiwiciI6IkQifV19XX19.dWLfRouAejO3xKtDGLkacXBJpv0qipA2NUK8Ubjr_nx9PepZxendTxeg_VDGxnJnFAR7JGUIfpjjytSm1gOtDrFDsX4MO_8xwu0Sg7Z7WeKeQtPcLNc2Wh44BxGEhO4ekb5pWrHlJKP7vqlbuaumXNx_sbNjZBvodIOXGbfB_D2wmglcOo8U37R0NvwcxF_rUb7b8xKWEaEGa90SgkxajElwQrHv5jEFVXOZ1xoTS0zklmbaZJjDpWexhXw6k4D3wGRAF3WPVCiZA60pjl1a8U8ed8XoMO8dFUjfRx2R91J8v9bTj2t9paKE2vE53JhIjk-tx8Sv5_22YtQ_D32Pqg"
        },
        "clientId": {
          "type": "string",
          "format": "string",
          "description": "The IAM-issued client id.  Please ensure you have properly onboarded with IAM to receive a client id.",
          "example": "metro-edge"
        },
        "responseSelection": {
          "type": "string",
          "format": "string",
          "description": "Space-delimited list of id tokens requested by client. Valid values: id_token.basic",
          "example": "id_token.basic"
        }
      }
    },
    "idTokenResponse": {
      "description": "Id token(s) generated ",
      "type": "object",
      "properties": {
        "id_tokens": {
          "type": "array",
          "description": "Array of id tokens",
          "items": {
            "type": "object",
            "properties": {
              "id_token.basic": {
                "type": "string",
                "description": "A basic id token.",
                "example": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlZVRlVSRUZVTFRJd01Uaz0ifQ.eyJpYXQiOjE1NTcxNjgxNjQsImV4cCI6MTU1NzE3MTc2MywiaXNzIjoiaHR0cHM6Ly91YXQuYnJhc3MuYWNjb3VudC50LW1vYmlsZS5jb20iLCJhdWQiOiJUTU9BcHBOYXRpdmUiLCJBVCI6IjAxLlVTUi5WUWNkNzg4c1hMdWs5SHplMyIsInN1YiI6IlUtMjNlZWE5MTMtNDkwMS00YTM5LTk2NDUtMDk0MmJmYzQ1MjMzIiwiYWNyIjoibG9hMiIsImFtciI6WyJwYXNzd29yZCJdLCJ1c24iOiI4MmI0MDJiYzQ4OTkzYmI2IiwiZW50X3R5cGUiOiJiYXNpYyIsImVudCI6eyJhY2N0IjpbeyJyIjoiQU8iLCJpZCI6IjgxMjM0NjY5IiwidHN0IjoiSVIiLCJsaW5lX2NvdW50IjoxLCJsaW5lcyI6W3sicGhudW0iOiI4MDgxMjM0NjY5IiwiciI6IkQifV19XX19.dWLfRouAejO3xKtDGLkacXBJpv0qipA2NUK8Ubjr_nx9PepZxendTxeg_VDGxnJnFAR7JGUIfpjjytSm1gOtDrFDsX4MO_8xwu0Sg7Z7WeKeQtPcLNc2Wh44BxGEhO4ekb5pWrHlJKP7vqlbuaumXNx_sbNjZBvodIOXGbfB_D2wmglcOo8U37R0NvwcxF_rUb7b8xKWEaEGa90SgkxajElwQrHv5jEFVXOZ1xoTS0zklmbaZJjDpWexhXw6k4D3wGRAF3WPVCiZA60pjl1a8U8ed8XoMO8dFUjfRx2R91J8v9bTj2t9paKE2vE53JhIjk-tx8Sv5_22YtQ_D32Pqg"
              }
            }
          },
          "example": [
            {
              "id_token.basic": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlZVRlVSRUZVTFRJd01Uaz0ifQ.eyJpYXQiOjE1NTcxNjgxNjQsImV4cCI6MTU1NzE3MTc2MywiaXNzIjoiaHR0cHM6Ly91YXQuYnJhc3MuYWNjb3VudC50LW1vYmlsZS5jb20iLCJhdWQiOiJUTU9BcHBOYXRpdmUiLCJBVCI6IjAxLlVTUi5WUWNkNzg4c1hMdWs5SHplMyIsInN1YiI6IlUtMjNlZWE5MTMtNDkwMS00YTM5LTk2NDUtMDk0MmJmYzQ1MjMzIiwiYWNyIjoibG9hMiIsImFtciI6WyJwYXNzd29yZCJdLCJ1c24iOiI4MmI0MDJiYzQ4OTkzYmI2IiwiZW50X3R5cGUiOiJiYXNpYyIsImVudCI6eyJhY2N0IjpbeyJyIjoiQU8iLCJpZCI6IjgxMjM0NjY5IiwidHN0IjoiSVIiLCJsaW5lX2NvdW50IjoxLCJsaW5lcyI6W3sicGhudW0iOiI4MDgxMjM0NjY5IiwiciI6IkQifV19XX19.dWLfRouAejO3xKtDGLkacXBJpv0qipA2NUK8Ubjr_nx9PepZxendTxeg_VDGxnJnFAR7JGUIfpjjytSm1gOtDrFDsX4MO_8xwu0Sg7Z7WeKeQtPcLNc2Wh44BxGEhO4ekb5pWrHlJKP7vqlbuaumXNx_sbNjZBvodIOXGbfB_D2wmglcOo8U37R0NvwcxF_rUb7b8xKWEaEGa90SgkxajElwQrHv5jEFVXOZ1xoTS0zklmbaZJjDpWexhXw6k4D3wGRAF3WPVCiZA60pjl1a8U8ed8XoMO8dFUjfRx2R91J8v9bTj2t9paKE2vE53JhIjk-tx8Sv5_22YtQ_D32Pqg"
            }
          ]
        }
      }
    },
    "httpErrorResponse": {
      "description": "Common Error response format",
      "type": "object",
      "required": [
        "code",
        "userMessage"
      ],
      "properties": {
        "code": {
          "type": "string",
          "format": "string",
          "description": "The error code.",
          "example": "500"
        },
        "systemMessage": {
          "type": "string",
          "format": "string",
          "description": "The error code description.",
          "example": "defaultError"
        },
        "userMessage": {
          "type": "string",
          "format": "string",
          "description": "Human readable error message",
          "example": "Uh-oh, it looks like we have our wires crossed. Please try again later."
        }
      }
    },
    "httpBadRequestResponse": {
      "description": "Common Error response format",
      "type": "object",
      "required": [
        "code",
        "userMessage"
      ],
      "properties": {
        "code": {
          "type": "string",
          "format": "string",
          "description": "The error code.",
          "example": "400"
        },
        "systemMessage": {
          "type": "string",
          "format": "string",
          "description": "The error code description.",
          "example": "Bad Request"
        },
        "userMessage": {
          "type": "string",
          "format": "string",
          "description": "Human readable error message",
          "example": "The request is missing required parameters."
        }
      }
    },
    "httpAuthErrorResponse": {
      "description": "Common Error response format",
      "type": "object",
      "required": [
        "code",
        "userMessage"
      ],
      "properties": {
        "code": {
          "type": "string",
          "format": "string",
          "description": "The error code.",
          "example": "401"
        },
        "systemMessage": {
          "type": "string",
          "format": "string",
          "description": "The error code description.",
          "example": "Unauthorized"
        },
        "userMessage": {
          "type": "string",
          "format": "string",
          "description": "Human readable error message",
          "example": "Your request could not be properly validated."
        }
      }
    },
    "httpAccountNotFoundErrorResponse": {
      "description": "Common Error response format",
      "type": "object",
      "required": [
        "code",
        "userMessage"
      ],
      "properties": {
        "code": {
          "type": "string",
          "format": "string",
          "description": "The error code.",
          "example": "404"
        },
        "systemMessage": {
          "type": "string",
          "format": "string",
          "description": "The error code description.",
          "example": "iamAccountNotFound"
        },
        "userMessage": {
          "type": "string",
          "format": "string",
          "description": "Human readable error message",
          "example": "IAM Account Not Found from IAM authorize response"
        }
      }
    }
  }
}
