{
  "swagger": "2.0",
  "info": {
    "version": "1.0.0",
    "title": "EUI2.0",
    "description": "A micro service for getting the application information based on client ID",
    "contact": {
      "name": "SecureID Support",
      "email": "SecureID_Support@T-Mobile.com"
    }
  },
  "host": "ws14.iam.msg.lab.t-mobile.com",
  "x-servers": [
    {
      "url": "https://ws14.iam.msg.lab.t-mobile.com"
    },
    {
      "url": "https://{Environment}ws14.iam.msg.lab.t-mobile.com",
      "variables": {
        "Environment": {
          "enum": [
            "dev.",
            "qat.",
            "uat.",
            "plab.",
            "ppd."
          ],
          "default": "uat."
        }
      }
    }
  ],
  "basePath": "/svr/getAppInfo",
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "tags": [
    {
      "name": "getAppInfo",
      "description": "get application information of a client_id."
    }
  ],
  "paths": {
    "/getAppInfo": {
      "get": {
        "x-api-pattern": "ExecuteFunction",
        "summary": "Sends application information of provided CLIENT_ID",
        "operationId": "getAppInfo",
        "description": "Sends application information of provided CLIENT_ID",
        "tags": [
          "getAppInfo"
        ],
        "security": [
          {
            "Basic": []
          }
        ],
        "parameters": [
          {
            "in": "header",
            "name": "Authorization",
            "type": "string",
            "description": "Access Token",
            "required": true,
            "format": "byte",
            "x-example": "Bearer mF_9.B5f-4.1JqM"
          },
          {
            "in": "header",
            "name": "Content-type",
            "type": "string",
            "description": "Request content type",
            "required": true,
            "format": "application/json",
            "x-example": "application/json"
          },
          {
            "in": "header",
            "name": "Accept",
            "type": "string",
            "description": "Acceptable media type.  Currently, only application/json is supported.",
            "required": false,
            "format": "application/json",
            "x-example": "application/json"
          },
          {
            "in": "header",
            "name": "Accept-Language",
            "type": "string",
            "description": "Acceptable Language.  Currently, only en-US is be accepted.",
            "required": false,
            "format": "application/json",
            "x-example": "en-US"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful got the Application informatio for a given client Id",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/sendAppInfo"
            }
          },
          "400": {
            "description": "Bad Request (ie missing required parameters).",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpBadRequestResponse"
            }
          },
          "401": {
            "description": "Unauthorized Request (ie Authorization token could not be validated).",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpErrorResponse"
            },
            "examples": {
              "code": "401",
              "systemMessage": "Unauthorized",
              "userMessage": "You request could not be properly validated."
            }
          },
          "404": {
            "description": "The provided client Id was not found",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpClientIdNotFoundErrorResponse"
            }
          },
          "405": {
            "description": "Method not Allowed",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              },
              "Allow": {
                "type": "string",
                "format": "application/json",
                "description": "Acceptable MIME-type",
                "x-example": "POST"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpMethodNotFoundResponse"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpMethodNotFoundResponse"
            }
          },
          "503": {
            "description": "Service Unavailable",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpMethodNotFoundResponse"
            }
          }
        }
      }
    }
  },
  "securityDefinitions": {
    "Basic": {
      "type": "basic",
      "description": "Standard basic authentication"
    }
  },
  "definitions": {
    "sendAppInfo": {
      "description": "Application info",
      "type": "object",
      "properties": {
        "ApplicationId": {
          "type": "string",
          "description": "Application id",
          "example": "EUI"
        },
        "Name": {
          "type": "array",
          "description": "An array containing language code and client ID",
          "items": {
            "type": "object",
            "properties": {
              "langCode": {
                "type": "string",
                "description": "Language of question.  ",
                "example": "en"
              },
              "value": {
                "type": "string",
                "description": "Client ID",
                "example": "EUI Client"
              }
            }
          }
        },
        "Status": {
          "type": "string",
          "description": "Status",
          "example": "active"
        },
        "Salt": {
          "type": "string",
          "description": "Salt",
          "example": "xMur2I"
        },
        "Desc": {
          "type": "array",
          "description": "An array containing language code and client ID",
          "items": {
            "type": "object",
            "properties": {
              "langCode": {
                "type": "string",
                "description": "Language of question.  ",
                "example": "en"
              },
              "value": {
                "type": "string",
                "description": "Client ID",
                "example": "EUI Client"
              }
            }
          }
        },
        "ClientType": {
          "type": "string",
          "description": "client Type",
          "example": "WEBAPP"
        },
        "TrustedType": {
          "type": "string",
          "description": "Trusted types",
          "example": "CORE"
        },
        "AccessTokenTTL": {
          "type": "number",
          "description": "Access token ttl",
          "example": 36000
        },
        "AllowedGrantTypes": {
          "type": "string",
          "description": "Allowed grant types",
          "example": "authorization_code,client_credentials"
        },
        "LifeCycleConfig": {
          "type": "string",
          "description": "Life cycle config",
          "example": "ONETIME"
        },
        "RedirectUrls": {
          "type": "string",
          "description": "Redirect Urls",
          "example": "https://dev2.account.t-mobile.com,https://qat2.account.t-mobile.com,https://uat2.account.t-mobile.com,"
        },
        "AllowOfflineAccess": {
          "type": "boolean",
          "description": "Allow offline access",
          "example": true
        },
        "AllowedScopes": {
          "type": "string",
          "description": "Allowed scopes",
          "example": "{\"billing_information\":[\"paymentType\",\"paymentAccount\",\"billingPreference\"],\"address\":[\"country\"],\"credential_management\":[],\"federated_authn\":[],\"token_validation\":[\"client_id\",\"scope\",\"expires_in\",\"owner_id\"],\"associated_lines\":[\"lineLinkedTimestamp\",\"multiLineIndicator\",\"customerType\",\"OperatorId\",\"contractCode\",\"lineUserId\",\"msisdn\",\"operatorId\",\"uniqueContractCode\",\"primaryBillingAccountCode\",\"lineType\",\"subscriberType\",\"multiLineCustomerType\",\"multiLinesPAH\",\"multilineHasDefaultPermissions\",\"multilinePermissions\",\"lineEmail\",\"isMultiLinePAH\",\"multiLinesOnBA\"],\"associated_billing_accounts\":[\"county\",\"city\",\"streetAddress\",\"state\",\"country\",\"primaryLine\",\"fraudlock\",\"billingAccountCountry\",\"billingAccountState\",\"billingAccountCounty\",\"billingAccountCode\",\"billingAccountCustomers\",\"billingAccountContractCodes\",\"businessLine\",\"billingAccountStatus\",\"billingAccountStatusDate\",\"paymentOption\",\"billingStatementLanguage\",\"billingAccountCity\",\"billingAccountStreetAddress\",\"billingAccountZipCode\",\"billingAccountbillCyclePeriod\",\"billingAccountBillCyclePeriod\"],\"openid\":[],\"auto_generated_flag_write\":[],\"permission\":[],\"associated_customers\":[\"isIdVerified\",\"customer_addresses\",\"customerNumber\",\"customerUserId\",\"ssn\",\"customerDateOfBirth\",\"gender\",\"customerContracts\",\"customerBillingAccounts\",\"customerCustomers\",\"businessUnit\",\"customerStatus\",\"customerStatusDate\",\"customerCreationDate\",\"customerGroup\",\"parentCustomerNumber\",\"customerFirstName\",\"customerLastName\",\"middleName\",\"employeeNumber\",\"preferredCommunicationMethod\",\"websiteLanguage\",\"isCreditVerified\"],\"biometric_authn\":[],\"counter_w\":[],\"extended_lines\":[\"exCoUniqueContractCode\",\"exCoBillingAccountCode\",\"exCoMsisdn\",\"exCoLineType\",\"exBaBillingAccountType\",\"exBaStatus\",\"exCuCustomerNumber\",\"exCuNickname\",\"exCuUserId\",\"exCuStatus\",\"exCoUserId\",\"exCoContractCode\",\"exCoSubscriberType\",\"exCoMultiLineCustomerType\",\"exCoBillingFirstname\",\"exCoBillingLastname\",\"exCoMultilineHasDefaultPermissions\",\"exCoMultilinePermissions\",\"exCoIsPrimary\",\"exCoOperatorId\",\"exCoIsMultiLinePAH\",\"exCoMultiLinesOnBA\",\"exCoCustomers\"],\"biometrics\":[],\"TMO_ID_profile\":[\"securityQ1\",\"securityQ2\",\"email\",\"securityQ3\",\"userName\",\"username\",\"firstname\",\"lastname\",\"creationTimestamp\",\"userId\",\"autoGeneratedFlag\",\"pin\"],\"session_w\":[],\"unified_idp_api\":[],\"client\":[],\"email\":[\"email\"]}"
        }
      }
    },
    "httpErrorResponse": {
      "description": "Common Error response format",
      "type": "object",
      "required": [
        "code",
        "userMessage"
      ],
      "properties": {
        "code": {
          "type": "string",
          "format": "string",
          "description": "The error code.",
          "example": "400"
        },
        "systemMessage": {
          "type": "string",
          "format": "string",
          "description": "The error code description.",
          "example": "Unauthorized"
        },
        "userMessage": {
          "type": "string",
          "format": "string",
          "description": "Human readable error message",
          "example": "The server failed unexpectedly."
        }
      }
    },
    "httpBadRequestResponse": {
      "description": "Common Error response format",
      "type": "object",
      "required": [
        "code",
        "userMessage"
      ],
      "properties": {
        "code": {
          "type": "string",
          "format": "string",
          "description": "The error code.",
          "example": "400"
        },
        "systemMessage": {
          "type": "string",
          "format": "string",
          "description": "The error code description.",
          "example": "Bad Request"
        },
        "userMessage": {
          "type": "string",
          "format": "string",
          "description": "Human readable error message",
          "example": "The request is missing required parameters."
        }
      }
    },
    "httpClientIdNotFoundErrorResponse": {
      "description": "Common Error response format",
      "type": "object",
      "required": [
        "code",
        "userMessage"
      ],
      "properties": {
        "code": {
          "type": "string",
          "format": "string",
          "description": "The error code.",
          "example": "404"
        },
        "systemMessage": {
          "type": "string",
          "format": "string",
          "description": "The error code description.",
          "example": "client_id not found"
        },
        "userMessage": {
          "type": "string",
          "format": "string",
          "description": "Human readable error message",
          "example": "client_id not found"
        }
      }
    },
    "httpMethodNotFoundResponse": {
      "description": "Common Error response format",
      "type": "object",
      "required": [
        "code",
        "userMessage"
      ],
      "properties": {
        "code": {
          "type": "string",
          "format": "string",
          "description": "The error code.",
          "example": "405"
        },
        "systemMessage": {
          "type": "string",
          "format": "string",
          "description": "The error code description.",
          "example": "defaultError"
        },
        "userMessage": {
          "type": "string",
          "format": "string",
          "description": "Human readable error message",
          "example": "Uh-oh, it looks like we have our wires crossed. Please try again later."
        }
      }
    }
  }
}