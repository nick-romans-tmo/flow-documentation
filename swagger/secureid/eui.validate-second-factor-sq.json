{
  "swagger": "2.0",
  "info": {
    "description": "This is a collection of a several SecureID API endpoints.",
    "version": "1.0.0",
    "title": "SecureID API endpoints",
    "contact": {
      "name": "secure ID",
      "email": "apiteam@swagger.io"
    }
  },
  "host": "account.t-mobile.com",
  "x-servers": "account.t-mobile.com",
  "basePath": "/svr/validateSecondFactorSQ",
  "tags": [
    {
      "name": "validateSecondFactorSQ  ",
      "description": "validateSecondFactorSQ API is used to validate second factor SQ"
    }
  ],
  "schemes": [
    "https"
  ],
  "paths": {
    "/validateSecondFactorSQ": {
      "post": {
        "tags": [
          "validateSecondFactorSQ"
        ],
        "summary": "validateSecondFactorSQ API is used to validate second factor SQ",
        "description": "validateSecondFactorSQ API is used to validate second factor SQ",
        "operationId": "validateSecondFactorSQ",
        "x-api-pattern": "CreateInCollection",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "text/plain"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "request object required for resending the code",
            "required": true,
            "schema": {
              "$ref": "#/definitions/validateSecondFactorSQ"
            }
          },
          {
            "name": "Content-Type",
            "x-example": "string",
            "in": "header",
            "required": true,
            "format": "string",
            "type": "string",
            "description": "Application/Json"
          },
          {
            "name": "Origin",
            "in": "header",
            "x-example": "string",
            "format": "string",
            "type": "string",
            "required": true,
            "description": "https://account.t-mobile.com"
          }
        ],
        "responses": {
          "200":{
            "$ref": "#/responses/validateSecondFactorSQ"
          },
          "400": {
            "$ref": "#/responses/BadRequest"
          },
           "403":{
            "$ref": "#/responses/Forbidden"
          },
          "500": {
            "$ref": "#/responses/ServerError"
          },
          "503": {
            "$ref": "#/responses/ServiceUnavailable"
          }
        },
        "security": [
          {
            "xsrfCsutomToken": []
          },
          {
            "secSessionToken": []
          },
          {
            "authorization": []
          }
        ]
      }
    }
  },
  "securityDefinitions": {
    "xsrfCsutomToken": {
      "name": "xsrf-custom-token",
      "in": "header",
      "type": "apiKey",
      "description": "Cross-Site Request Forgery"
    },
    "secSessionToken": {
      "name": "sec_session_token",
      "in": "header",
      "type": "apiKey",
      "description": "checking for session validation"
    },
    "authorization": {
      "name": "Authorization",
      "in": "header",
      "type": "apiKey",
      "description": "Authorization Bearer token"
    }
  },
  "definitions": {
    "validateSecondFactorSQ": {
      "type": "object",
      "description": "request Object",
      "properties": {
        "securityInformation": {
          "type": "array",
          "description": "Describes security information array",
          "items":{
            "type": "object",
            "properties":{
                "sq_id": {
                        "type": "string",
                        "format": "^[a-zA-Z\\s\\.\\-\\']+$",
                        "description": "Describes security question id",
                        "example": "12"
                  },
                  "answer": {
                        "type": "string",
                        "format": "^[a-zA-Z\\s\\.\\-\\']+$",
                        "description": "Describes security question answer",
                        "example": "test"
                  }
                }
            }
                    },
        "msisdn": {
          "type": "string",
          "format": "^[a-zA-Z\\s\\.\\-\\']+$",
          "description": "Describes user msisdn",
          "example": "4256002152"
        },
        "context": {
          "type": "string",
          "format": "^[a-zA-Z\\s\\.\\-\\']+$",
          "description": "Describes user msisdn",
          "example": "authorize"
        },
        "userPreferred2FVMethods": {
          "type": "string",
          "format": "^[a-zA-Z\\s\\.\\-\\']+$",
          "description": "Describes users preferred 2FV methods",
          "example": "security_question"
        },
        "uuid": {
          "type": "string",
          "format": "^[a-zA-Z\\s\\.\\-\\']+$",
          "description": "Describes user uuid",
          "example": "U-e7d95443-9e26-4726-a848-3a1a7e16db8c"
        },
        "euiFlow": {
          "type": "string",
          "format": "^[a-zA-Z\\s\\.\\-\\']+$",
          "description": "Describes user flow",
          "example": "LOGIN"
        },
        "remTransId": {
          "type": "string",
          "format": "^[a-zA-Z\\s\\.\\-\\']+$",
          "example": "formSubmit-redirect-error",
          "description": "rem transaction ID"
        }
      }
    },
    "validateSecondFactorSQModel": {
      "type": "object",
      "description": "Describes success validateSecondFactorSQ model",
      "properties": {
        "user_id": {
          "type": "string",
          "description": "user id",
          "example": "U-e7d95443-9e26-4726-a848-3a1a7e16db8c",
          "format": "^[a-zA-Z\\s\\-\\']$"
        },
        "authorization_code": {
          "type": "string",
          "description": "authorization code",
          "example": "14.5yETTAntnptOBvuse",
          "format": "^[a-zA-Z\\s\\-\\']$"
        }
      }
    },
    "ServerErrorModel": {
      "type": "object",
      "description": "Describes server error model",
      "properties": {
        "code": {
          "type": "string",
          "description": "succinct, domain-specific, human-readable text string to identify the type of error for the given status code",
          "example": "Resource not found",
          "format": "^[a-zA-Z\\s\\-\\']$"
        },
        "userMessage": {
          "type": "string",
          "description": "human readable text that describes the nature of the error",
          "example": "We could not authenticate that user.",
          "format": "^[a-zA-Z\\s\\.\\-\\']+$"
        },
        "systemMessage": {
          "type": "string",
          "description": "text that provides a more detailed technical explanation of the error",
          "example": "Authentication failed"
        },
        "detailLink": {
          "type": "string",
          "description": "link to custom information providing greater detail on error or errors",
          "example": "audit.log.for.events.url"
        }
      }
    },
    "BadRequestModel": {
      "type": "object",
      "description": "Describes bad request model",
      "properties": {
        "error": {
          "type": "string",
          "description": "human readable text that describes the nature of the error",
          "example": "invalid_request",
          "format": "^[a-zA-Z\\s\\-\\']$"
        },
        "error_description": {
          "type": "string",
          "description": "uman readable text that describes the nature of the error",
          "example": "Invalid request",
          "format": "^[a-zA-Z\\s\\.\\-\\']+$"
        }
      }
    },
    "ErrorModel": {
      "type": "object",
      "description": "Describes error model",
      "properties": {
        "code": {
          "type": "string",
          "description": "succinct, domain-specific, human-readable text string to identify the type of error for the given status code",
          "example": "Resource not found",
          "format": "^[a-zA-Z\\s\\-\\']$"
        },
        "userMessage": {
          "type": "string",
          "description": "human readable text that describes the nature of the error",
          "example": "We could not authenticate that user.",
          "format": "^[a-zA-Z\\s\\.\\-\\']+$"
        },
        "systemMessage": {
          "type": "string",
          "description": "text that provides a more detailed technical explanation of the error",
          "example": "Authentication failed"
        },
        "detailLink": {
          "type": "string",
          "description": "link to custom information providing greater detail on error or errors",
          "example": "audit.log.for.events.url"
        }
      }
    }
  },
  "responses": {
    "Forbidden": {
      "description": "JSON response in case of invalid request fails",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "statuscode": 403,
        "error": "invalid_request",
        "error_description": "Invalid request body"
      }
    },
    "ServerError": {
      "description": "JSON response in case of internal system error, in the event of downstream systems unavailability or uncaught exceptions",
      "schema": {
        "$ref": "#/definitions/ServerErrorModel"
      },
      "examples": {
        "code": "Server Error",
        "userMessage": "The server encountered an unexpected condition that prevented it from fulfilling the request."
      }
    },
    "BadRequest": {
      "description": "JSON response in case of missing required inbound parameters",
      "schema": {
        "$ref": "#/definitions/BadRequestModel"
      },
      "examples": {
        "error": "invalid_request",
        "error_description": "Invalid request body"
      }
    },
    "validateSecondFactorSQ": {
      "description": "JSON response for that describes the status of the request.",
      "schema": {
        "$ref": "#/definitions/validateSecondFactorSQModel"
      },
      "examples": {
        "user_id":"uid:U-1b073f55-8e5b-4d9c-922e-0579e32c28a3",
        "authorization_code":"14.5yETTAntnptOBvuse"
      }
    },
    "ServiceUnavailable": {
      "description": "JSON response in case the requested service is unavailable",
      "schema": {
        "$ref": "#/definitions/ServerErrorModel"
      },
      "examples": {
        "code": "Service Unavailable",
        "userMessage": "The server is currently unable to handle the request due to a temporary overload or scheduled maintenance, which will likely be alleviated after some delay."
      }
    }
  }
}