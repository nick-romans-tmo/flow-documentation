{
  "swagger" : "2.0",
  "info" : {
    "description" : "A micro service responsible for authenticating and authorizing user and generates authorization code or access token.",
    "version" : "1.0.0",
    "title" : "Restful Authentication Service",
    "contact" : {
      "name" : "SecureID - Development Team",
      "email" : "EUIDevelopment@T-Mobile.com"
    }
  },
  "host" : "brass.account.t-mobile.com",
  "basePath" : "/ras/v1",
  "tags" : [ {
    "name" : "RAS",
    "description" : "Restful Authentication Service"
  } ],
  "schemes" : [ "https" ],
  "consumes" : [ "application/json" ],
  "produces" : [ "application/json" ],
  "paths" : {
    "/fetchTokenWithCode" : {
      "post" : {
        "tags" : [ "RAS" ],
        "summary" : "Get authorization code for user",
        "description" : "Returns Authorization_code and Session details.",
        "operationId" : "requestAuthCode",
        "parameters" : [ {
          "name" : "authorization",
          "in" : "header",
          "description" : "Authorization will be in form of DAT and POP token, and is optional if connection is coming from within local network; authentication is still done with client bearer token, or clientId and clientSecret pair.",
          "required" : true,
          "type" : "string",
          "format" : "byte",
          "x-example" : "basic"
        }, {
          "name" : "content-type",
          "in" : "header",
          "description" : "Type of the returned content",
          "required" : true,
          "type" : "string",
          "format" : "application/json",
          "x-example" : "application/json"
        }, {
          "in" : "body",
          "name" : "requestBody",
          "description" : "Request JSON body for auth code",
          "required" : false,
          "schema" : {
            "allOf" : [ {
              "$ref" : "#/definitions/CodeRequest"
            }, { } ]
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "JSON response with access_token, access_token_type, access_token_ttl, scope and tmobileid",
            "headers" : {
              "date" : {
                "type" : "string",
                "format" : "date-time",
                "description" : "date of transaction",
                "x-example" : "2002-09-24+06:00"
              },
              "content-type" : {
                "type" : "string",
                "format" : "application/json",
                "description" : "type of the returned content",
                "x-example" : "application/json"
              },
              "cache-control" : {
                "type" : "string",
                "description" : "date of transaction",
                "x-example" : "public"
              }
            },
            "schema" : {
              "$ref" : "#/definitions/inline_response_200"
            }
          },
          "400" : {
            "description" : "JSON response in case of missing required inbound parameters",
            "examples" : {
              "error" : "Bad Request",
              "error_description" : "ClientId not present in request"
            },
            "schema" : {
              "$ref" : "#/definitions/ErrorModel"
            }
          },
          "401" : {
            "description" : "JSON response in case of authorization fails",
            "examples" : {
              "error" : "Unauthorized",
              "error_description" : "Unauthorized client"
            },
            "schema" : {
              "$ref" : "#/definitions/ErrorModel"
            }
          },
          "403" : {
            "description" : "JSON response in case of authorization fails",
            "examples" : {
              "error" : "Forbidden",
              "error_description" : "Invalid oauth parameters"
            },
            "schema" : {
              "$ref" : "#/definitions/ErrorModel"
            }
          },
          "404" : {
            "description" : "JSON response in case requested data cannot be found",
            "examples" : {
              "error" : "Not Found",
              "error_description" : "That ClientId was not found"
            },
            "schema" : {
              "$ref" : "#/definitions/ErrorModel"
            }
          },
          "500" : {
            "description" : "JSON response in case of internal system error, in the event of downstream systems unavailability or uncaught exceptions",
            "examples" : {
              "error" : "server_error",
              "error_description" : "Sorry, we are currently experiencing problems with our server. Please try again later."
            },
            "schema" : {
              "$ref" : "#/definitions/ErrorModel"
            }
          },
          "503" : {
            "description" : "JSON response in case the requested service is unavailable"
          }
        },
        "security" : [ { }, {
          "BasicAuth" : [ ]
        }, {
          "DatToken" : [ ]
        }, {
          "PopToken" : [ ]
        } ],
        "x-api-pattern" : "ExecuteFunction"
      }
    }
  },
  "securityDefinitions" : {
    "BasicAuth" : {
      "description" : "Standard basic authentication",
      "type" : "basic"
    },
    "DatToken" : {
      "description" : "Token for authorizing the client, checked by middleware for any not whitelisted connections",
      "type" : "apiKey",
      "name" : "Authorization",
      "in" : "header"
    },
    "PopToken" : {
      "description" : "Token used to validate the DatToken, checked by middleware for any not whitelisted connections",
      "type" : "apiKey",
      "name" : "x-authorization",
      "in" : "header"
    }
  },
  "definitions" : {
    "CodeRequest" : {
      "type" : "object",
      "required" : [ "client_id", "code", "redirect_uri", "trans_id" ],
      "properties" : {
        "trans_id" : {
          "type" : "string",
          "format" : "string",
          "example" : "abcd_23pqr78_1244",
          "description" : "Unique identifier for auditing"
        },
        "client_id" : {
          "type" : "string",
          "format" : "string",
          "example" : "TMOAppNative",
          "description" : "OAUTH parameter clients can pass to receive code"
        },
        "redirect_uri" : {
          "type" : "string",
          "format" : "uri",
          "example" : "https://localhost",
          "description" : "Client application URI"
        },
        "code" : {
          "type" : "string",
          "format" : "string",
          "example" : "01.95q0LMkZvojUQW8Vz",
          "description" : "Auth code"
        },
        "scope" : {
          "type" : "string",
          "format" : "string",
          "example" : "TMO_ID_profile associated_lines extended_lines openid",
          "description" : "Scope of code"
        }
      }
    },
    "CodeResponse" : {
      "type" : "object",
      "properties" : {
        "access_token" : {
          "type" : "string",
          "format" : "string",
          "description" : "Authorization code"
        },
        "access_token_type" : {
          "type" : "string",
          "format" : "string",
          "description" : "type of access_token"
        },
        "access_token_ttl" : {
          "type" : "string",
          "format" : "string",
          "description" : "Expiry time of access_token"
        },
        "tmobileid" : {
          "type" : "string",
          "format" : "string",
          "description" : "Unique id of user"
        },
        "scope" : {
          "type" : "string",
          "format" : "string",
          "description" : "Scope associated with code"
        }
      }
    },
    "ErrorModel" : {
      "type" : "object",
      "required" : [ "error", "error_description" ],
      "properties" : {
        "error" : {
          "type" : "string",
          "format" : "string",
          "example" : "authentication_failure",
          "description" : "succinct, domain-specific, human-readable text string to identify the type of error for the given status code"
        },
        "error_description" : {
          "type" : "string",
          "format" : "string",
          "example" : "We could not authenticate that user.",
          "description" : "human readable text that describes the nature of the error"
        }
      }
    },
    "inline_response_200" : {
      "type" : "object",
      "properties" : {
        "CodeResponse" : {
          "$ref" : "#/definitions/CodeResponse"
        }
      },
      "example" : "{access_token=1.asgdv34ndkk, access_token_type=Bearer, tmobileid=d4a09fa033ec2c759f20f37c12d9e9db1, scope=TMO_ID_profile associated_lines extended_lines openid, access_token_ttl=3600}"
    }
  },
  "parameters" : {
    "dateHeader" : {
      "name" : "date",
      "in" : "header",
      "description" : "datetime of transaction",
      "required" : false,
      "type" : "string",
      "format" : "date"
    },
    "contentTypeHeader" : {
      "name" : "content-type",
      "in" : "header",
      "description" : "Type of the returned content",
      "required" : true,
      "type" : "string",
      "format" : "application/json",
      "x-example" : "application/json"
    },
    "cacheControlResponseHeader" : {
      "name" : "cache-control",
      "in" : "header",
      "description" : "Cache control response directive",
      "required" : false,
      "type" : "string",
      "format" : "string"
    },
    "authorizationHeader" : {
      "name" : "authorization",
      "in" : "header",
      "description" : "Authorization will be in form of DAT and POP token, and is optional if connection is coming from within local network; authentication is still done with client bearer token, or clientId and clientSecret pair.",
      "required" : true,
      "type" : "string",
      "format" : "byte",
      "x-example" : "basic"
    }
  },
  "responses" : {
    "BadRequest" : {
      "description" : "JSON response in case of missing required inbound parameters",
      "examples" : {
        "error" : "Bad Request",
        "error_description" : "ClientId not present in request"
      },
      "schema" : {
        "$ref" : "#/definitions/ErrorModel"
      }
    },
    "Unauthenticated" : {
      "description" : "JSON response for invalid DAT and/or PoP tokens, untrusted sources trying to access API directly",
      "examples" : {
        "error" : "authentication_failure",
        "error_description" : "The login information you provided is incorrect. Please try again."
      },
      "schema" : {
        "$ref" : "#/definitions/ErrorModel"
      }
    },
    "Forbidden" : {
      "description" : "JSON response in case of authorization fails",
      "examples" : {
        "error" : "Forbidden",
        "error_description" : "Invalid oauth parameters"
      },
      "schema" : {
        "$ref" : "#/definitions/ErrorModel"
      }
    },
    "Unauthorized" : {
      "description" : "JSON response in case of authorization fails",
      "examples" : {
        "error" : "Unauthorized",
        "error_description" : "Unauthorized client"
      },
      "schema" : {
        "$ref" : "#/definitions/ErrorModel"
      }
    },
    "NotFound" : {
      "description" : "JSON response in case requested data cannot be found",
      "examples" : {
        "error" : "Not Found",
        "error_description" : "That ClientId was not found"
      },
      "schema" : {
        "$ref" : "#/definitions/ErrorModel"
      }
    },
    "ServerError" : {
      "description" : "JSON response in case of internal system error, in the event of downstream systems unavailability or uncaught exceptions",
      "examples" : {
        "error" : "server_error",
        "error_description" : "Sorry, we are currently experiencing problems with our server. Please try again later."
      },
      "schema" : {
        "$ref" : "#/definitions/ErrorModel"
      }
    },
    "ServiceUnavailable" : {
      "description" : "JSON response in case the requested service is unavailable"
    }
  },
  "x-servers" : "brass.account.t-mobile.com"
}