{
  "swagger": "2.0",
  "info": {
    "description": "A End Point used to fetch the user Profile by Code and token",
    "version": "0.0.1",
    "title": "fetchprofile",
    "contact": {
      "name": "SecureID - Development Team",
      "email": "EUIDevelopment@T-Mobile.com"
    }
  },
  "host": "eui9.account.t-mobile.com",
  "basePath": "/svr/fetchprofile",
  "tags": [
    {
      "name": "fetchprofile",
      "description": "Fetch Profile"
    }
  ],
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/fetchprofile": {
      "post": {
        "tags": [
          "fetchprofile"
        ],
        "summary": "fetch user profile of the user",
        "description": "Returns user Profile.",
        "operationId": "user profile",
        "parameters": [
          {
            "name": "sessionId",
            "in": "header",
            "description": "session id will expect the encrypted string with basic.",
            "required": true,
            "type": "string",
            "format": "byte",
            "x-example": "Basic-0x595fe0ee25000000=="
          },
          {
            "name": "content-type",
            "in": "header",
            "description": "Type of the returned content",
            "required": true,
            "type": "string",
            "format": "application/json",
            "x-example": "application/json"
          },
          {
            "in": "body",
            "name": "requestBody",
            "description": "Request JSON body for auth code",
            "required": false,
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/CodeRequest"
                },
                {}
              ]
            }
          }
        ],
        "responses": {
          "200": {
            "description": "JSON response with code , sso_session_id, sso_session_ttl, user_id and session_number",
            "headers": {
              "date": {
                "type": "string",
                "format": "date-time",
                "description": "date of transaction",
                "x-example": "2002-09-24+06:00"
              },
              "content-type": {
                "type": "string",
                "format": "application/json",
                "description": "type of the returned content",
                "x-example": "application/json"
              },
              "cache-control": {
                "type": "string",
                "description": "date of transaction",
                "x-example": "public"
              }
            },
            "schema": {
              "$ref": "#/definitions/inline_response_200"
            }
          },
          "400": {
            "description": "The access token in the request is not matched within defined pattern.",
            "examples": {
              "error": "invalid_request",
              "error_description": "The access token in the request is not matched within defined pattern."
            },
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "401": {
            "description": "JSON response in case of authorization fails",
            "examples": {
              "error": "invalid_token",
              "error_description": "Access token is invalid."
            },
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "500": {
            "description": "JSON response in case of internal system error, in the event of downstream systems unavailability or uncaught exceptions",
            "examples": {
              "error": "server_error",
              "error_description": "Sorry, we are currently experiencing problems with our server. Please try again later."
            },
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            }
          }
        },
        "security": [
          {},
          {
            "BasicAuth": []
          }
        ],
        "x-api-pattern": "ExecuteFunction"
      }
    }
  },
  "securityDefinitions": {
    "BasicAuth": {
      "description": "Standard basic authentication",
      "type": "basic"
    },
    "DatToken": {
      "description": "Token for authorizing the client, checked by middleware for any not whitelisted connections",
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    },
    "PopToken": {
      "description": "Token used to validate the DatToken, checked by middleware for any not whitelisted connections",
      "type": "apiKey",
      "name": "x-authorization",
      "in": "header"
    }
  },
  "definitions": {
    "CodeRequest": {
      "type": "object",
      "required": [
        "code",
        "transId",
        "clientId",
        "redirectUri",
        "responseType",
        "scope",
        "accessType"
      ],
      "properties": {
        "headers": {
          "type": "object",
          "format": "string",
          "example": "sessionId",
          "description": "sessionId",
          "properties": {
            "sessionId": {
              "type": "string",
              "format": "string",
              "example": "Basic-0x595fe0ee25000000==",
              "description": "encrypted code"
            }
          }
        },
        "code": {
          "type": "string",
          "format": "string",
          "example": "01.ZoF6arl0DCMjtLGtD",
          "description": "encrypted code"
        },
        "transId": {
          "type": "string",
          "format": "string",
          "example": "abcd_23pqr78_1244",
          "description": "Unique identifier for auditing"
        },
        "clientId": {
          "type": "string",
          "format": "string",
          "example": "EUI",
          "description": "OAUTH parameter clients can pass to receive code"
        },
        "redirectUri": {
          "type": "string",
          "format": "uri",
          "example": "https://localhost",
          "description": "Client application URI"
        },
        "scope": {
          "type": "string",
          "format": "string",
          "example": "associatedLines billingInformation TMOIDProfile openid",
          "description": "This parameter will decide"
        },
        "responseType": {
          "type": "string",
          "format": "string",
          "example": "Allowed value 'code'",
          "description": "code"
        },
        "accessType": {
          "type": "string",
          "format": "string",
          "example": "offline or online",
          "description": "OAUTH parameter clients can pass to receive a refresh token for offline access."
        }
      }
    },
    "CodeResponse": {
      "type": "object",
      "properties": {
        "iamProfile": {
          "type": "string",
          "format": "string",
          "description": "Authorization code"
        },
        "userId": {
          "type": "string",
          "format": "string",
          "description": "Unique id of user"
        },
        "sessionNumber": {
          "type": "string",
          "format": "string",
          "description": "Session number to track session activities"
        }
      },
      "example": "{iamProfile:{email:4258292127cc@yopmail.com, userId:U-27d7c009-9d50-41cb-847c-50861e395100,username:metro_944734851,firstname:test,lastname:test,creationTimestamp:2019-11-19T14:25:32Z,autoGeneratedFlag:false,pin:AQD0AnQvbX9Yd2b3Fc6EKoDe}, associatedLines:[{multiLineCustomerType:NSRP, contractCode:3102672397,uniqueContractCode:3102672397,primaryBillingAccountCode:944734851,userId:U-27d7c009-9d50-41cb-847c-50861e395100,msisdn:2067792432,lineType:Legacy,subscriberType:POSTPAID,operatorId:4000,linkTypes:[direct, indirect],lineLinkedTimestamp:2019-11-22T12:38:21Z},{multiLineCustomerType:NSRP,contractCode:3102672398,uniqueContractCode:3102672398,primaryBillingAccountCode:944734851,userId:U-27d7c009-9d50-41cb-847c-50861e395100,msisdn:4258292127,lineType:Legacy,subscriberType:POSTPAID,email:4258292127cc@yopmail.com,operatorId:4000,}]}"
    },
    "ErrorModel": {
      "type": "object",
      "required": [
        "error",
        "error_description"
      ],
      "description": "Error Modal Defines error and error description",
      "properties": {
        "error": {
          "type": "string",
          "format": "string",
          "example": "invalid_request",
          "description": "The incomming request is not valid"
        },
        "error_description": {
          "type": "string",
          "format": "string",
          "example": "The access token in the request is not matched within defined pattern.",
          "description": "The access token in the request is not matched within defined pattern."
        }
      }
    },
    "ErrorResponse": {
      "type": "object",
      "format": "Object",
      "description": "error description",
      "properties": {
        "action": {
          "type": "string",
          "description": "Internal server error."
        },
        "deviceInformation": {
          "type": "object",
          "properties": {
            "device_is_mobile": {
              "type": "boolean",
              "description": "Checks if the device is mobile or desktop"
            }
          }
        }
      }
    },
    "inline_response_200": {
      "type": "object",
      "properties": {
        "CodeResponse": {
          "$ref": "#/definitions/CodeResponse"
        }
      },
      "example": "{iamProfile:{email:4258292127cc@yopmail.com, userId:U-27d7c009-9d50-41cb-847c-50861e395100,username:metro_944734851,firstname:test,lastname:test,creationTimestamp:2019-11-19T14:25:32Z,autoGeneratedFlag:false,pin:AQD0AnQvbX9Yd2b3Fc6EKoDe}, associatedLines:[{multiLineCustomerType:NSRP, contractCode:3102672397,uniqueContractCode:3102672397,primaryBillingAccountCode:944734851,userId:U-27d7c009-9d50-41cb-847c-50861e395100,msisdn:2067792432,lineType:Legacy,subscriberType:POSTPAID,operatorId:4000,linkTypes:[direct, indirect],lineLinkedTimestamp:2019-11-22T12:38:21Z},{multiLineCustomerType:NSRP,contractCode:3102672398,uniqueContractCode:3102672398,primaryBillingAccountCode:944734851,userId:U-27d7c009-9d50-41cb-847c-50861e395100,msisdn:4258292127,lineType:Legacy,subscriberType:POSTPAID,email:4258292127cc@yopmail.com,operatorId:4000,}]}"
    }
  },
  "responses": {
    "BadRequest": {
      "description": "JSON response in case of missing required inbound parameters",
      "examples": {
        "error": "Bad Request",
        "error_description": "ClientId not present in request"
      },
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      }
    },
    "Unauthenticated": {
      "description": "JSON response for invalid DAT and/or PoP tokens, untrusted sources trying to access API directly",
      "examples": {
        "error": "authentication_failure",
        "error_description": "The login information you provided is incorrect. Please try again."
      },
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      }
    },
    "Forbidden": {
      "description": "JSON response in case of authorization fails",
      "examples": {
        "error": "Forbidden",
        "error_description": "Invalid oauth parameters"
      },
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      }
    },
    "Unauthorized": {
      "description": "JSON response in case of authorization fails",
      "examples": {
        "error": "Unauthorized",
        "error_description": "Unauthorized client"
      },
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      }
    },
    "NotFound": {
      "description": "JSON response in case requested data cannot be found",
      "examples": {
        "error": "Not Found",
        "error_description": "That ClientId was not found"
      },
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      }
    },
    "ServerError": {
      "description": "JSON response in case of internal system error, in the event of downstream systems unavailability or uncaught exceptions",
      "examples": {
        "error": "server_error",
        "error_description": "Sorry, we are currently experiencing problems with our server. Please try again later."
      },
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      }
    },
    "ServiceUnavailable": {
      "description": "JSON response in case the requested service is unavailable"
    }
  },
  "x-servers": "brass.account.t-mobile.com"
}