{
  "swagger": "2.0",
  "info": {
    "version": "0.0.1",
    "title": "NET - Save Network Details",
    "description": "This API will save network details",
    "contact": {
      "name": "SecureID - Development Team",
      "email": "EUIDevelopment@T-Mobile.com"
    }
  },
  "host": "brass.account.t-mobile.com",
  "x-servers": [
    {
      "url": "https://{Environment}brass.account.t-mobile.com",
      "variables": {
        "Environment": {
          "enum": [
            "dev.",
            "qat.",
            "uat.",
            "plab.",
            "ppd."
          ],
          "default": "qat."
        }
      }
    }
  ],
  "basePath": "/net/v1",
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "tags": [
    {
      "name": "NET",
      "description": "API that Saves Network Details"
    }
  ],
  "paths": {
    "/saveNetworkDetails": {
      "post": {
        "x-api-pattern": "ExecuteFunction",
        "operationId": "saveNetworkDetails",
        "tags": [
          "NET"
        ],
        "security": [
          {}
        ],
        "description": "API that saves Network Details.",
        "summary": "Saving Network Details",
        "parameters": [
          {
            "in": "body",
            "name": "requestBody",
            "description": "Network details payload",
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/getRequestPayload"
                }
              ]
            }
          }
        ],
        "responses": {
          "200": {
            "description": "OK. Operation completed",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "type": "object",
              "required": [
                "Status"
              ],
              "properties": {
                "Status": {
                  "type": "number",
                  "format": "number",
                  "description": "Success response code",
                  "example": 200
                }
              }
            },
            "examples": {
              "required": {
                "value": {
                  "Device_PK": "DevicePublicKey,",
                  "Device_Attributes": "String"
                }
              },
              "full": {
                "value": {
                  "Status": {
                    "type": "number",
                    "enum": 200
                  }
                }
              }
            }
          },
          "401": {
            "description": "JSON response for invalid tokens - DAT/POP/id_token",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/errorResponse"
            },
            "examples": {
              "RECORD_NOT_FOUND": {
                "value": {
                  "error_code": "RECORD_NOT_FOUND",
                  "error_message": "No record for XwK4aTCtsdOGttxr4X792IzOP6gfOH7h1yx2zEXcqecMj+YNt6YuAipFIM9jpCnIsE10WggRUOdL2755WiehzVsyKTLTegqWaLKMVGSi+rRyK/Bx/8PDJ9GdQlnwJSprw6DW4hS7DaESR6nqjqBNibDUIXDmNWL+ROctwkwGKYjK/XarpXIGKKn5FdJbEXodnMvSBe8ajJz9M4LB2Hy98+9k/xBpOduYYn2Q3AI/EJUQ9NT6uS2hOz+pr+PGEQsFC7sorNJyj4XywYLMDCIV6kCLwLVtxH9xRgaAH/+zAO/XxaRMNo2XoA67SoJPNlQohtw4r0gDC4SG5Oja8V/nQ=="
                }
              },
              "invalid_request": {
                "value": {
                  "error_code": "invalid_request",
                  "error_message": "Request does not have mandatory param trans_id"
                }
              },
              "Missing_Device_Attributes": {
                "value": {
                  "error_code": "Missing_Device_Attributes",
                  "error_message": "Missing device attributes"
                }
              },
              "Missing_Device_PK": {
                "value": {
                  "error_code": "Missing_Device_PK",
                  "error_message": "Missing device public key"
                }
              },
              "Missing_IAM_Details": {
                "value": {
                  "error_code": "Missing_IAM_Details",
                  "error_description": "Missing required parameter iam"
                }
              },
              "DECRYPT_ERROR": {
                "value": {
                  "error_code": "DECRYPT_ERROR",
                  "error_description": "Error while decrypting device attributes"
                }
              },
              "Missing_Client_ID": {
                "value": {
                  "error_code": "Missing_Client_ID",
                  "error_description": "Missing required parameter client_id"
                }
              },
              "DB_RETRIEVE_ERROR": {
                "value": {
                  "error_code": "DB_RETRIEVE_ERROR",
                  "error_description": "Unable to retrieve record from DAT DB"
                }
              }
            }
          }
        }
      }
    }
  },
  "securityDefinitions": {
    "BasicAuth": {
      "type": "basic",
      "description": "Standard basic authentication"
    }
  },
  "definitions": {
    "getRequestPayload": {
      "type": "object",
      "description": "This object will contain network details request payload",
      "required": [
        "Device_PK",
        "Device_Attributes",
        "trans_id",
        "iam"
      ],
      "properties": {
        "Device_PK": {
          "type": "string",
          "format": "string",
          "description": "Device_PK",
          "example": "IJ1ON7njFvbc2P53p9oUVvaVjYcybkCHbNthTtzTLbsK0N40sxMeNcl5Djo9tr7UXUdYFEAvmhOiwb0h5OzFGJJJF2m3jpT"
        },
        "trans_id": {
          "type": "string",
          "format": "string",
          "description": "transaction id",
          "example": "2b0ab7bb-1f20-410d-b94c-7c4acbcb70dc"
        },
        "Device_Attributes": {
          "type": "string",
          "format": "string",
          "description": "Device_Attributes",
          "example": "8gJwzAaQNFUL0XEb+3nxXRCtPu1Vk5RalWbKb4+xB3Ku2g7b0L2H9YYX/OeiLc1QoJwYm7U3uXxaV626miSrM2eCA5+BcJVnEgSui9WjGGHfMn/U8IFPAJq0nCeoB6bL"
        },
        "iam": {
          "type": "object",
          "description": "iam client_id",
          "required": [
            "client_id"
          ],
          "properties": {
            "client_id": {
              "type": "string",
              "format": "string",
              "description": "client_id",
              "example": "TMOAppNative"
            }
          },
          "example": "TMOAppNative"
        }
      }
    },
    "errorResponse": {
      "type": "object",
      "required": [
        "statusCode"
      ],
      "properties": {
        "statusCode": {
          "type": "string",
          "format": "string",
          "description": "The status code.",
          "example": "401"
        },
        "error_code": {
          "type": "string",
          "format": "string",
          "description": "The error code.",
          "example": "Unauthorized"
        },
        "error_description": {
          "type": "string",
          "format": "string",
          "description": "The error code description.",
          "example": "Unauthorized"
        }
      }
    }
  }
}