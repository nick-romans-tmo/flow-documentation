{
  "swagger": "2.0",
  "info": {
    "version": "2.0.0",
    "title": "Token Management Service",
    "description": "A micro service responsible for returning access_token,id_tokens and information about line and user profiles",
    "contact": {
      "name": "SecureID On-call Support",
      "email": "EUIDevelopment@T-Mobile.com"
    }
  },
  "host": "brass.account.t-mobile.com",
  "x-servers": "brass.account.t-mobile.com",
  "basePath": "/tms",
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "tags": [
    {
      "name": "TMS",
      "description": "Token Management Service"
    }
  ],
  "paths": {
    "/v1/updateUsertoken": {
      "post": {
        "x-api-pattern": "ExecuteFunction",
        "operationId": "requestUserToken",
        "tags": [
          "TMS"
        ],
        "parameters": [
          {
            "$ref": "#/parameters/contentTypeHeader"
          },
          {
            "in": "body",
            "name": "requestBody",
            "description": "Request JSON body for User Token resource",
            "schema": {
              "$ref": "#/definitions/UpdateUserTokenRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/SuccessResponse"
          },
          "401": {
            "$ref": "#/responses/UnauthorizedRequest"
          },
          "403": {
            "$ref": "#/responses/InvalidRequest"
          },
          "500": {
            "$ref": "#/responses/ServerError"
          }
        }
      }
    }
  },
  "definitions": {
    "UpdateUserTokenRequest": {
      "type": "object",
      "required": [
        "code",
        "client_id",
        "client_secret",
        "redirect_uri",
        "trans_id",
        "id_token"
      ],
      "properties": {
        "trans_id": {
          "type": "string",
          "description": "Unique identifier for auditing",
          "example": "ghjk677"
        },
        "code": {
          "type": "string",
          "description": "Access code for authorizing user token request",
          "format": "byte",
          "example": "01.GzsAyyLLy21SGETwE"
        },
        "client_id": {
          "type": "string",
          "description": "Calling application authorization secret client_id",
          "example": "TMOAppNative",
          "format": "string"
        },
        "client_secret": {
          "type": "string",
          "description": "Calling application authorization client_secret",
          "format": "byte",
          "example": "TMOAppNativeSecret"
        },
        "redirect_uri": {
          "type": "string",
          "description": "Client application URI",
          "format": "uri",
          "example": "https://localhost"
        },
        "scope": {
          "type": "string",
          "description": "Processes the request has claim to in IAM",
          "example": "associatedLines billingInformation TMOIDProfile openid"
        },
        "response_selection": {
          "type": "string",
          "description": "if you pass response_selection will get id_tokens, if you pass userinfo in response_selection will get user profile",
          "example": "id_token.basic userinfo"
        },
        "pub_key": {
          "type": "string",
          "description": "In case of DAT not available in request header then this parameter becomes required to populate CNF value in id_tokens",
          "example": "Eydmndsmvshbvskdjvnsvdjsksdfsdvs...."
        },
        "id_token": {
          "type": "string",
          "description": "If you pass id_token will get new id_token for new client id",
          "example": "Eydmndsmvshbvskdjvnsvdjsksdfsdvs...."
        }
      }
    },
    "SuccessModel": {
      "type": "object",
      "properties": {
        "id_tokens": {
          "type": "array",
          "description": "id_token for the user",
          "items": {
            "type": "object",
            "properties": {
              "id_token.basic": {
                "type": "string",
                "format": "bit",
                "description": "JWT token returned by the authorization service"
              }
            }
          }
        }
      }
    },
    "ErrorModel": {
      "type": "object",
      "properties": {
        "error": {
          "type": "string",
          "description": "invalid_request"
        },
        "error_description": {
          "type": "string",
          "description": "Invalid oauth parameters"
        }
      },
      "required": [
        "error",
        "error_description"
      ]
    }
  },
  "parameters": {
    "contentTypeHeader": {
      "in": "header",
      "name": "content-type",
      "type": "string",
      "description": "Type of the returned content",
      "required": true,
      "format": "application/json",
      "x-example": "application/json"
    }
  },
  "responses": {
    "SuccessResponse": {
      "description": "JSON response with user id Tokens",
      "schema": {
        "$ref": "#/definitions/SuccessModel"
      },
      "examples": {
        "id_tokens": [
          {
            "id_token.basic": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI"
          }
        ]
      }
    },
    "UnauthorizedRequest": {
      "description": "incoming request doesn't have authorization header or invalid apigee token",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "error": "Unauthorized",
        "error_description": "Unauthorized client"
      }
    },
    "InvalidRequest": {
      "description": "incoming request doesn't have require parameters",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "error": "invalid_request",
        "error_description": "Invalid oauth parameters"
      }
    },
    "ServerError": {
      "description": "JSON response in case of internal system error, in the event of downstream systems unavailability or uncaught exceptions",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "error": "internal_server_error",
        "error_description": "Sorry, we are currently experiencing problems with our server. Please try again later."
      }
    }
  }
}