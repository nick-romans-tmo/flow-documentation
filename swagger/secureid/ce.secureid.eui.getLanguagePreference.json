{
  "swagger": "2.0",
  "info": {
    "description": "This api will gives Language Preference.",
    "version": "0.0.1",
    "title": "getLanguagePreference",
    "contact": {
      "name": "SecureID - Development Team",
      "email": "EUIDevelopment@T-Mobile.com"
    }
  },
  "host": "eui9.account.t-mobile.com",
  "basePath": "/getLanguagePreference",
  "tags": [
    {
      "name": "getLanguagePreference",
      "description": "This api will gives Language Preference"
    }
  ],
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/getLanguagePreference": {
      "post": {
        "tags": [
          "getLanguagePreference"
        ],
        "summary": "getLanguagePreference",
        "description": "Returns prefered Language en/es.",
        "operationId": "getLanguagePreference",
        "parameters": [
          {
            "name": "authorization",
            "in": "header",
            "description": "Authorization will be in form of DAT and POP token, and is optional if connection is coming from within local network; authentication is still done with client bearer token, or clientId and clientSecret pair.",
            "required": true,
            "type": "string",
            "format": "byte",
            "x-example": "basic"
          },
          {
            "name": "content-type",
            "in": "header",
            "description": "Type of the returned content",
            "required": true,
            "type": "string",
            "format": "application/json",
            "x-example": "application/json"
          },
          {
            "in": "body",
            "name": "requestBody",
            "description": "Request JSON body for auth code",
            "required": true,
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/codeRequest"
                },
                {}
              ]
            }
          }
        ],
        "responses": {
          "200": {
            "description": "JSON response email",
            "headers": {
              "date": {
                "type": "string",
                "format": "date-time",
                "description": "date of transaction",
                "x-example": "2002-09-24+06:00"
              },
              "content-type": {
                "type": "string",
                "format": "application/json",
                "description": "type of the returned content",
                "x-example": "application/json"
              },
              "cache-control": {
                "type": "string",
                "description": "date of transaction",
                "x-example": "public"
              }
            },
            "schema": {
              "$ref": "#/definitions/successResponse"
            }
          },
          "400": {
            "description": "JSON response in case of missing required inbound parameters",
            "schema": {
              "$ref": "#/definitions/badRequestErrorModel"
            }
          },
          "403": {
            "description": "JSON response in case of request does not have a csrf token",
            "examples": {
              "statusCode":403,
              "error": "invalid_request",
              "error_description": "Invalid request, cannot complete processing"
            },
            "schema": {
              "$ref": "#/definitions/invalidRequestModel"
            }
          },
          "404": {
            "description": "JSON response in case requested data cannot be found",
            "examples": {
              "error": "Not Found",
              "error_description": "That ClientId was not found"
            },
            "schema": {
              "$ref": "#/definitions/errorModel"
            }
          },
          "500": {
            "description": "JSON response in case of internal system error, in the event of downstream systems unavailability or uncaught exceptions",
            "examples": {
              "error": "server_error",
              "statusCode": "statusCode"
            },
            "schema": {
              "$ref": "#/definitions/serverErrorModel"
            }
          }
        },
        "security": [
          {},
          {
            "BasicAuth": []
          },
          {
            "DatToken": []
          },
          {
            "PopToken": []
          }
        ]      }
    }
  },
  "securityDefinitions": {
    "BasicAuth": {
      "description": "Standard basic authentication",
      "type": "basic"
    },
    "DatToken": {
      "description": "Token for authorizing the client, checked by middleware for any not whitelisted connections",
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    },
    "PopToken": {
      "description": "Token used to validate the DatToken, checked by middleware for any not whitelisted connections",
      "type": "apiKey",
      "name": "x-authorization",
      "in": "header"
    }
  },
  "definitions": {
    "codeRequest": {
      "type": "object",
      "required": [
        "data"
      ],
		"description": "code request",

      "properties": {
        "data": {
          "type": "string",
          "format": "string",
          "example": "BQBtjgkUSaI dVsHlN3 YxqvYy yj/pvnopaahK2bAoNZp628PBnX9An8kd/0CXRJaROUDnWzD28z3 yOomPQuTFnri30WOysKZOApzSYLncp45dyHBNjg/hE5tiza4OYz4I0aQlqdhNHMG20N916MP4U0mSIUaOvohC81yakpAbBU0LNM2iwRCzWKewKFui88ACNC y7T/ XpZ8ato8Em3f4AW/1NKhodXz4HVArnPRAjby/L9rd2p8HHD/I H6m4ZRZAIspjQBElyjXN/85 2KBBXXb827aUjnObKrtWlW1 MP p3yqHM3Rlv7VWxKSUAv/4tkm9M0kZ1lGJOh0V HYRL1B/NbgC/IUv9Puu6zIYaVou YMJfwjHKSqzcqwa4=",
          "description": "fetch language preference from encrypted pwd reset data string"
        }
      }
    },
    "codeResponse": {
      "type": "object",
	  "description": "code response",
      "properties": {
        "language":{
          "type": "string",
          "format": "string",
          "example": "en",
          "description": "language"
        },
        "statusCode":{
          "type": "number",
          "format": "number",
          "example": 200,
          "description": "status code"
        }
      }
    },
    "invalidRequestModel": {
      "type": "object",
	  "description": "invalidRequestModel",
      "required": [
        "error"
      ],
      "properties": {
        "statusCode": {
          "type": "number",
          "format": "string",
          "example": 403,
          "description": "statusCode"
        },
        "error": {
          "type": "string",
          "format": "string",
          "example": "invalid_request",
          "description": "invalid_request"
        },
        "error_description": {
          "type": "string",
          "format": "string",
          "example": "Invalid request, cannot complete processing",
          "description": "Invalid request, cannot complete processing"
        }
      }
      
    },
    "serverErrorModel": {
      "type": "object",
      "required": [
        "error"
      ],
      "description":"server_error",
      "properties": {
        "error": {
          "type": "string",
          "format": "string",
          "example": "server_error",
          "description": "server_error"
        },
        "statusCode": {
          "type": "number",
          "format": "number",
          "example": 500,
          "description": "statusCode"
        }
      }
      
    },
    "badRequestErrorModel": {
      "type": "object",
      "required": [
        "error"
      ],
      "description":"invalid_request",
      "properties": {
        "error": {
          "type": "string",
          "format": "string",
          "example": "invalid_request",
          "description": "invalid_request"
        },
        "statusCode": {
          "type": "number",
          "format": "string",
          "example": 400,
          "description": "statusCode"
        }
      }
    },
    "errorModel": {
      "type": "object",
      "required": [
        "error"
      ],
      "description":"invalid_request",
      "properties": {
        "error": {
          "type": "string",
          "format": "string",
          "example": "invalid_request",
          "description": "invalid_request"
        },
        "statusCode": {
          "type": "number",
          "format": "string",
          "example": 400,
          "description": "statusCode"
        }
      }
    },
    "successResponse": {
      "type": "object",
      "description":"successResponse",
      "properties": {
        "codeResponse": {
          "$ref": "#/definitions/codeResponse"
        }
      },
      "example": {"language":"en","statusCode":200}
    }
  },
  "parameters": {
    "dateHeader": {
      "name": "date",
      "in": "header",
      "description": "datetime of transaction",
      "required": false,
      "type": "string",
      "format": "date"
    },
    "contentTypeHeader": {
      "name": "content-type",
      "in": "header",
      "description": "Type of the returned content",
      "required": true,
      "type": "string",
      "format": "application/json",
      "x-example": "application/json"
    },
    "cacheControlResponseHeader": {
      "name": "cache-control",
      "in": "header",
      "description": "Cache control response directive",
      "required": false,
      "type": "string",
      "format": "string"
    },
    "authorizationHeader": {
      "name": "authorization",
      "in": "header",
      "description": "Authorization will be in form of DAT and POP token, and is optional if connection is coming from within local network; authentication is still done with client bearer token, or clientId and clientSecret pair.",
      "required": true,
      "type": "string",
      "format": "byte",
      "x-example": "basic"
    }
  },
  "responses": {
    "BadRequest": {
      "description": "JSON response in case of missing required inbound parameters",
      "examples": {
        "error": "invalid_request",
        "error_description": "invalid_request"
      },
      "schema": {
        "$ref": "#/definitions/errorModel"
      }
    },
    "Forbidden": {
      "description": "JSON response in case of authorization fails",
      "examples": {
        "error": "Forbidden",
        "error_description": "Invalid oauth parameters"
      },
      "schema": {
        "$ref": "#/definitions/errorModel"
      }
    },
    "Unauthorized": {
      "description": "JSON response in case of authorization fails",
      "examples": {
        "error": "Unauthorized",
        "error_description": "Unauthorized client"
      },
      "schema": {
        "$ref": "#/definitions/errorModel"
      }
    },
    "NotFound": {
      "description": "JSON response in case requested data cannot be found",
      "examples": {
        "error": "Not Found",
        "error_description": "That ClientId was not found"
      },
      "schema": {
        "$ref": "#/definitions/errorModel"
      }
    },
    "ServerError": {
      "description": "JSON response in case of internal system error, in the event of downstream systems unavailability or uncaught exceptions",
      "examples": {
        "error": "server_error",
        "error_description": "Sorry, we are currently experiencing problems with our server. Please try again later."
      },
      "schema": {
        "$ref": "#/definitions/errorModel"
      }
    },
    "ServiceUnavailable": {
      "description": "JSON response in case the requested service is unavailable"
    }
  },
  "x-servers": "eui9.account.t-mobile.com"
}