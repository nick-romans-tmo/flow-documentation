{
  "swagger": "2.0",
  "info": {
    "version": "1.0.0",
    "title": "BRASS - RAS",
    "description": "A micro service with Restful and extensible native extensions for use by the T-Mobile APP.",
    "contact": {
      "name": "SecureID Support",
      "email": "SecureID_Support@T-Mobile.com"
    }
  },
  "host": "brass.account.t-mobile.com",
  "x-servers": [
    {
      "url": "https://{Environment}.brass.account.t-mobile.com",
      "variables": {
        "Environment": {
          "enum": [
            "dev",
            "qat",
            "uat",
            "plab",
            "ppd"
          ],
          "default": "uat"
        }
      }
    }
  ],
  "basePath": "/ras/v2",
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "tags": [
    {
      "name": "Get access token",
      "description": "API that returns access token response and user token (JWT) and user info for given user_id, password and scope parameters."
    }
  ],
  "paths": {
    "/token": {
      "post": {
        "x-api-pattern": "ExecuteFunction",
        "summary": "Retrieves the access token",
        "operationId": "query_access_token",
        "description": "Generates the access token. This is a POST rather than a GET because an username and password must be passed.",
        "tags": [
          "Get Access token"
        ],
        "security": [
          {
            "Basic": []
          }
        ],
        "parameters": [
          {
            "in": "header",
            "name": "Content-type",
            "type": "string",
            "description": "Request content type",
            "required": true,
            "format": "application/json",
            "x-example": "application/json"
          },
          {
            "in": "header",
            "name": "Accept-language",
            "type": "string",
            "description": "Request language",
            "required": false,
            "format": "en-US",
            "x-example": "en-US"
          },
          {
            "in": "header",
            "name": "Authorization",
            "type": "string",
            "description": "Authorization will be in form of DAT token, and is optional if connection is coming from within local network; authentication is still done with client bearer token, or clientId and clientSecret pair.",
            "required": true,
            "format": "byte",
            "x-example": "basic"
          },
          {
            "in": "header",
            "name": "x-authorization",
            "type": "string",
            "description": "X-Authorization will be in form of POP token, and is optional if connection is coming from within local network; authentication is still done with client bearer token, or clientId and clientSecret pair.",
            "required": true,
            "format": "byte",
            "x-example": "basic"
          },
          {
            "in": "body",
            "name": "requestBody",
            "required": true,
            "description": "Request JSON body containing additional details needed to retrieve the access token.",
            "schema": {
              "$ref": "#/definitions/getTokenRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful retreival of access token/code",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              },
              "Cache-Control": {
                "type": "string",
                "format": "string",
                "description": "cache control",
                "x-example": "no-cache, no-store, must-revalidate"
              },
              "Pragma": {
                "type": "string",
                "format": "string",
                "description": "Pragma",
                "x-example": "no-cache"
              },
              "Server": {
                "type": "string",
                "format": "string",
                "description": "Server",
                "x-example": "https://dev.brass.account.t-mobile.com"
              },
              "Last-Modified": {
                "type": "string",
                "format": "string",
                "description": "Last modified",
                "x-example": "Mon 25 Oct 2016 10:00:14  GMT"
              },
              "Expires": {
                "type": "string",
                "format": "string",
                "description": "Expires",
                "x-example": 0
              },
              "Status": {
                "type": "string",
                "format": "string",
                "description": "Status of the the transaction",
                "x-example": 200
              }
            },
            "schema": {
              "$ref": "#/definitions/getTokenResponse"
            },
            "examples": {
              "access_token": "abc",
              "token_type": "Bearer",
              "access_token_ttl": 3600,
              "code": "1.0Hyertyy",
              "sso_session_id": "xyz",
              "sso_session_ttl": "Mon 25 Oct 2016 10:18:14  GMT",
              "refresh_token": "tGzv3JOkF0XG5Qx2TlKWIA",
              "scope": "TMO_ID_profile",
              "userid": "2rqwras0912lkjqfu2",
              "tmobileid": "ase32432dsd"
            }
          },
          "400": {
            "description": "Bad request.",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              },
              "Cache-Control": {
                "type": "string",
                "format": "string",
                "description": "cache control",
                "x-example": "no-cache, no-store, must-revalidate"
              },
              "Pragma": {
                "type": "string",
                "format": "string",
                "description": "Pragma",
                "x-example": "no-cache"
              },
              "Server": {
                "type": "string",
                "format": "string",
                "description": "Server",
                "x-example": "https://dev.brass.account.t-mobile.com"
              },
              "Last-Modified": {
                "type": "string",
                "format": "string",
                "description": "Last modified",
                "x-example": "Mon 25 Oct 2016 10:00:14  GMT"
              },
              "Expires": {
                "type": "string",
                "format": "string",
                "description": "Expires",
                "x-example": 0
              },
              "Status": {
                "type": "string",
                "format": "string",
                "description": "Status of the the transaction",
                "x-example": 400
              }
            },
            "schema": {
              "$ref": "#/definitions/badRequest"
            },
            "examples": {
              "error": "invalid request.",
              "error_description": "Unable to generate access token using information provided.",
              "disable_login_button": false,
              "hide_signup_button": false
            }
          },
          "401": {
            "description": "Unauthorized Request (ie Authorization token could not be validated).",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              },
              "Cache-Control": {
                "type": "string",
                "format": "string",
                "description": "cache control",
                "x-example": "no-cache, no-store, must-revalidate"
              },
              "Pragma": {
                "type": "string",
                "format": "string",
                "description": "Pragma",
                "x-example": "no-cache"
              },
              "Server": {
                "type": "string",
                "format": "string",
                "description": "Server",
                "x-example": "https://dev.brass.account.t-mobile.com"
              },
              "Last-Modified": {
                "type": "string",
                "format": "string",
                "description": "Last modified",
                "x-example": "Mon 25 Oct 2016 10:00:14  GMT"
              },
              "Expires": {
                "type": "string",
                "format": "string",
                "description": "Expires",
                "x-example": 0
              },
              "Status": {
                "type": "string",
                "format": "string",
                "description": "Status of the the transaction",
                "x-example": 401
              }
            },
            "schema": {
              "$ref": "#/definitions/unauthorizedErrorResponse"
            },
            "examples": {
              "error": "Unauthorized",
              "error_description": "Not an authorized client"
            }
          },
          "403": {
            "description": "Forbidden",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              },
              "Cache-Control": {
                "type": "string",
                "format": "string",
                "description": "cache control",
                "x-example": "no-cache, no-store, must-revalidate"
              },
              "Pragma": {
                "type": "string",
                "format": "string",
                "description": "Pragma",
                "x-example": "no-cache"
              },
              "Server": {
                "type": "string",
                "format": "string",
                "description": "Server",
                "x-example": "https://dev.brass.account.t-mobile.com"
              },
              "Last-Modified": {
                "type": "string",
                "format": "string",
                "description": "Last modified",
                "x-example": "Mon 25 Oct 2016 10:00:14  GMT"
              },
              "Expires": {
                "type": "string",
                "format": "string",
                "description": "Expires",
                "x-example": 0
              },
              "Status": {
                "type": "string",
                "format": "string",
                "description": "Status of the the transaction",
                "x-example": 401
              }
            },
            "schema": {
              "$ref": "#/definitions/forbiddenErrorResponse"
            },
            "examples": {
              "error": "Forbidden",
              "error_description": "Invalid authorization header/invalid oauth params/ no trans_id"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "schema": {
              "$ref": "#/definitions/httpServerErrorResponse"
            },
            "examples": {
              "systemMessage": "Internal Server Error",
              "userMessage": "The server encountered an unexpected condition that prevented it from fulfilling the request"
            }
          }
        }
      }
    }
  },
  "securityDefinitions": {
    "Basic": {
      "type": "basic",
      "description": "Standard basic authentication"
    }
  },
  "definitions": {
    "getTokenRequest": {
      "description": "The request format for retrieving access token for a given username and password.",
      "type": "object",
      "required": [
        "trans_id",
        "client_id",
        "scope",
        "redirect_uri",
        "user_id",
        "password",
        "response_type"
      ],
      "properties": {
        "user_id": {
          "type": "string",
          "format": "string",
          "description": "Email/ MSISDN/ username of the TMO account.",
          "example": "1234567890|email@domain.com|name"
        },
        "password": {
          "type": "string",
          "format": "string",
          "description": "The password of the TMO account.",
          "example": "password"
        },
        "trans_id": {
          "type": "string",
          "format": "string",
          "description": "The trans_id will be unique.",
          "example": "remTransId_1234"
        },
        "client_id": {
          "type": "string",
          "format": "string",
          "description": "The Client_Id will be unique for each client",
          "example": "TMOAppNative"
        },
        "redirect_uri": {
          "type": "string",
          "format": "string",
          "description": "OAUTH parameter clients can pass to receive code and access token for.This should be one of the URLs registered with IAM during client id creation.",
          "example": "https://dev2.account.t-mobile.com"
        },
        "scope": {
          "type": "string",
          "format": "string",
          "description": "This defines level of details returned by IAM when requested for profile using .",
          "example": "TMOID_profile"
        },
        "response_type": {
          "type": "string",
          "format": "string",
          "description": "Accepted value ‘code’",
          "example": "code"
        },
        "app_version": {
          "type": "string",
          "format": "string",
          "description": "Version of T-Mobile APP. Helps in sending supported responses",
          "example": "1.2.3"
        },
        "platform": {
          "type": "string",
          "format": "string",
          "description": "Device OS version.Required for metrics and report generation",
          "example": "IOS"
        },
        "sso_session_id": {
          "type": "string",
          "format": "string",
          "description": "The client shall pass ANY SSO Session ID the client has. If an SSO session id is not provided then Username & password must be provided",
          "example": "skdfm_sefne_skrejn"
        },
        "lang": {
          "type": "string",
          "format": "string",
          "description": "Supported values ‘en’ and ‘es’.Default value is ‘en’.If any unsupported value sent default will be used.",
          "example": "en"
        },
        "keep_me_logged_in": {
          "type": "boolean",
          "format": "boolean",
          "description": "If true then sso_session_id is valid for 90 days.If false then sso_session_id is valid for 30 minutes.Default value is false",
          "example": true
        },
        "customer_ip": {
          "type": "string",
          "format": "string",
          "description": "Highly recommend to send this.This helps in auditing and tracing location of request origin",
          "example": "10.0.0.1"
        },
        "re_auth": {
          "type": "string",
          "format": "string",
          "description": "OAUTH parameter clients can pass to receive code and access token for.",
          "example": "code"
        },
        "approval_prompt": {
          "type": "string",
          "format": "string",
          "description": "OAUTH parameter clients can pass to have users’ permission to access profile and other things.Possible values are ‘auto’, ‘force’",
          "example": "auto"
        },
        "access_type": {
          "type": "string",
          "format": "string",
          "description": "OAUTH parameter clients can pass to receive a refresh token for offline access",
          "example": "ONLINE"
        },
        "is_user_jwt_required": {
          "type": "boolean",
          "format": "boolean",
          "description": "Possible values are ‘true’ or ‘false’",
          "example": true
        },
        "entitlements_type": {
          "type": "string",
          "format": "string",
          "description": "Possible values are ‘small’ or ‘large’",
          "example": "small"
        },
        "user_info_scopes": {
          "type": "string",
          "format": "string",
          "description": "Values passed in should be scope names defined by IAM",
          "example": "TMO_ID_profile"
        }
      }
    },
    "getTokenResponse": {
      "description": "successfully retrieved access token/code",
      "type": "object",
      "properties": {
        "access_token": {
          "type": "string",
          "format": "string",
          "description": "access token.",
          "example": "abc"
        },
        "token_type": {
          "type": "string",
          "format": "string",
          "description": "Token type.",
          "example": "Bearer"
        },
        "access_token_ttl": {
          "type": "string",
          "format": "string",
          "description": "Access token expiry time. access_token_ttl is in seconds",
          "example": 3600
        },
        "code": {
          "type": "string",
          "format": "string",
          "description": "code",
          "example": "1.0Hyertyy"
        },
        "sso_session_id": {
          "type": "string",
          "format": "string",
          "description": "Session ID",
          "example": "xyz"
        },
        "sso_session_ttl": {
          "type": "string",
          "format": "string",
          "description": "sso_session_ttl is in format “Mon, 25 Oct 2016 10:18:14 GMT”",
          "example": "Mon 25 Oct 2016 10:18:14  GMT"
        },
        "refresh_token": {
          "type": "string",
          "format": "string",
          "description": "refresh_token using this token received can be refreshed",
          "example": "tGzv3JOkF0XG5Qx2TlKWIA"
        },
        "scope": {
          "type": "string",
          "format": "string",
          "description": "Scope",
          "example": "TMO_ID_profile"
        },
        "userid": {
          "type": "string",
          "format": "string",
          "description": "user id of the TMO account",
          "example": "2rqwras0912lkjqfu2"
        },
        "tmobileid": {
          "type": "string",
          "format": "string",
          "description": "TMO ID of the account",
          "example": "ase32432dsd"
        },
        "user_token": {
          "type": "string",
          "format": "string",
          "description": "JWT that is issued and signed by BRASS",
          "example": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0OTc4OTk1MTAyNTQsImV4cCI6MTQ5NzkwMz"
        }
      }
    },
    "badRequest": {
      "description": "Common Error response format",
      "type": "object",
      "properties": {
        "error": {
          "type": "string",
          "format": "string",
          "description": "invalid request.",
          "example": "invalid_request"
        },
        "error_description": {
          "type": "string",
          "format": "string",
          "description": "Human readable error message",
          "example": "Incoming request is invalid."
        },
        "disable_login_button": {
          "type": "boolean",
          "format": "boolean",
          "description": "If true ‘Login’ button on Native App’s Login screen should be disabled.If false ‘Login’ button should be enabled.",
          "example": true
        },
        "hide_signup_button": {
          "type": "boolean",
          "format": "boolean",
          "description": "If true ‘Signup’ button on Native App’s Login screen should be disabled.If false ‘Signup’ button should be enabled",
          "example": true
        },
        "url_value": {
          "type": "string",
          "format": "string",
          "description": "URL value that needs to be opened up in web view on user click of the link",
          "example": "https://ppd.account.t-mobile.com/oauth2/v1/forgotpassword"
        }
      }
    },
    "unauthorizedErrorResponse": {
      "description": "Common Error response format",
      "type": "object",
      "required": [
        "statusCode",
        "error_description"
      ],
      "properties": {
        "error": {
          "type": "string",
          "format": "string",
          "description": "The error code description.",
          "example": "Unauthorized"
        },
        "error_description": {
          "type": "string",
          "format": "string",
          "description": "Human readable error message",
          "example": "Your request could not be properly validated."
        }
      }
    },
    "forbiddenErrorResponse": {
      "description": "Common Error response format",
      "type": "object",
      "required": [
        "statusCode",
        "error_description"
      ],
      "properties": {
        "error": {
          "type": "string",
          "format": "string",
          "description": "The error code description.",
          "example": "Unauthorized"
        },
        "error_description": {
          "type": "string",
          "format": "string",
          "description": "Human readable error message",
          "example": "Your request could not be properly validated."
        }
      }
    },
    "httpServerErrorResponse": {
      "description": "Common Error response format",
      "type": "object",
      "required": [
        "statusCode",
        "error_description"
      ],
      "properties": {
        "error": {
          "type": "string",
          "format": "string",
          "description": "internal_server_error",
          "example": "internal_server_error"
        },
        "error_description": {
          "type": "string",
          "format": "string",
          "description": "Sorry, we are currently experiencing problems with our server. Please try again later.",
          "example": "Sorry, we are currently experiencing problems with our server. Please try again later."
        }
      }
    }
  }
}
