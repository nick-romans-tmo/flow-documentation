{
  "swagger": "2.0",
  "info": {
    "description": "A service responsible for unlink the msisdn.",
    "version": "0.0.1",
    "title": "unlinkMsisdn",
    "contact": {
      "name": "SecureID - Development Team",
      "email": "EUIDevelopment@T-Mobile.com"
    }
  },
  "host": "eui9.account.t-mobile.com",
  "basePath": "/unlinkMsisdn",
  "tags": [
    {
      "name": "unlinkMsisdn",
      "description": "This api will unlink the msisdn when we merge the account in profile"
    }
  ],
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/unlinkMsisdn": {
      "post": {
        "tags": [
          "unlinkMsisdn"
        ],
        "summary": "UnlinkMsisdn",
        "description": "Returns email and status code.",
        "operationId": "requestEmail",
        "parameters": [
          {
            "name": "authorization",
            "in": "header",
            "description": "Authorization will be in form of DAT and POP token, and is optional if connection is coming from within local network; authentication is still done with client bearer token, or clientId and clientSecret pair.",
            "required": true,
            "type": "string",
            "format": "byte",
            "x-example": "basic"
          },
          {
            "name": "content-type",
            "in": "header",
            "description": "Type of the returned content",
            "required": true,
            "type": "string",
            "format": "application/json",
            "x-example": "application/json"
          },
          {
            "in": "body",
            "name": "requestBody",
            "description": "Request JSON body for auth code",
            "required": true,
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/CodeRequest"
                },
                {}
              ]
            }
          }
        ],
        "responses": {
          "200": {
            "description": "JSON response email",
            "headers": {
              "date": {
                "type": "string",
                "format": "date-time",
                "description": "date of transaction",
                "x-example": "2002-09-24+06:00"
              },
              "content-type": {
                "type": "string",
                "format": "application/json",
                "description": "type of the returned content",
                "x-example": "application/json"
              },
              "cache-control": {
                "type": "string",
                "description": "date of transaction",
                "x-example": "public"
              }
            },
            "schema": {
              "$ref": "#/definitions/inline_response_200"
            }
          },
          "400": {
            "description": "JSON response in case of missing required inbound parameters",
            "examples": {
              "error": "Bad Request",
              "error_description": "ClientId not present in request"
            },
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "401": {
            "description": "JSON response in case of authorization fails",
            "examples": {
              "error": "Unauthorized",
              "error_description": "Unauthorized client"
            },
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "403": {
            "description": "JSON response in case of authorization fails",
            "examples": {
              "error": "Forbidden",
              "error_description": "Invalid oauth parameters"
            },
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "404": {
            "description": "JSON response in case requested data cannot be found",
            "examples": {
              "error": "Not Found",
              "error_description": "That ClientId was not found"
            },
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "500": {
            "description": "JSON response in case of internal system error, in the event of downstream systems unavailability or uncaught exceptions",
            "examples": {
              "error": "server_error",
              "error_description": "Sorry, we are currently experiencing problems with our server. Please try again later."
            },
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          }
        },
        "security": [
          {},
          {
            "BasicAuth": []
          },
          {
            "DatToken": []
          },
          {
            "PopToken": []
          }
        ],
        "x-api-pattern": "ExecuteFunction"
      }
    }
  },
  "securityDefinitions": {
    "BasicAuth": {
      "description": "Standard basic authentication",
      "type": "basic"
    },
    "DatToken": {
      "description": "Token for authorizing the client, checked by middleware for any not whitelisted connections",
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    },
    "PopToken": {
      "description": "Token used to validate the DatToken, checked by middleware for any not whitelisted connections",
      "type": "apiKey",
      "name": "x-authorization",
      "in": "header"
    }
  },
  "definitions": {
    "CodeRequest": {
      "type": "object",
      "required": [
        "user_id",
        "msisdn"
      ],
      "properties": {
        "user_id": {
          "type": "string",
          "format": "string",
          "example": "uid:U-bb642d20-2ee2-4533-b0e7-671ceea47157",
          "description": "user_id must be passed If SSO_SESSION_ID exists then this becomes optional"
        },
        "msisdn": {
          "type": "string",
          "format": "string",
          "example": "6625653445",
          "description": "user msisdn"
        }
      }
    },
    "CodeResponse": {
      "type": "object",
      "properties": {}
    },
    "ErrorModel": {
      "type": "object",
      "required": [
        "error",
        "error_description"
      ],
      "properties": {
        "error": {
          "type": "string",
          "format": "string",
          "example": "user_not_found",
          "description": "succinct, domain-specific, human-readable text string to identify the type of error for the given status code"
        },
        "error_description": {
          "type": "string",
          "format": "string",
          "example": "The provided user_id is not found.",
          "description": "human readable text that describes the nature of the error"
        }
      }
    },
    "inline_response_200": {
      "type": "object",
      "properties": {
        "CodeResponse": {
          "$ref": "#/definitions/CodeResponse"
        }
      },
      "example": "{}"
    }
  },
  "parameters": {
    "dateHeader": {
      "name": "date",
      "in": "header",
      "description": "datetime of transaction",
      "required": false,
      "type": "string",
      "format": "date"
    },
    "contentTypeHeader": {
      "name": "content-type",
      "in": "header",
      "description": "Type of the returned content",
      "required": true,
      "type": "string",
      "format": "application/json",
      "x-example": "application/json"
    },
    "cacheControlResponseHeader": {
      "name": "cache-control",
      "in": "header",
      "description": "Cache control response directive",
      "required": false,
      "type": "string",
      "format": "string"
    },
    "authorizationHeader": {
      "name": "authorization",
      "in": "header",
      "description": "Authorization will be in form of DAT and POP token, and is optional if connection is coming from within local network; authentication is still done with client bearer token, or clientId and clientSecret pair.",
      "required": true,
      "type": "string",
      "format": "byte",
      "x-example": "basic"
    }
  },
  "responses": {
    "BadRequest": {
      "description": "JSON response in case of missing required inbound parameters",
      "examples": {
        "error": "Bad Request",
        "error_description": "ClientId not present in request"
      },
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      }
    },
    "Forbidden": {
      "description": "JSON response in case of authorization fails",
      "examples": {
        "error": "Forbidden",
        "error_description": "Invalid oauth parameters"
      },
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      }
    },
    "Unauthorized": {
      "description": "JSON response in case of authorization fails",
      "examples": {
        "error": "Unauthorized",
        "error_description": "Unauthorized client"
      },
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      }
    },
    "NotFound": {
      "description": "JSON response in case requested data cannot be found",
      "examples": {
        "error": "Not Found",
        "error_description": "That ClientId was not found"
      },
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      }
    },
    "ServerError": {
      "description": "JSON response in case of internal system error, in the event of downstream systems unavailability or uncaught exceptions",
      "examples": {
        "error": "server_error",
        "error_description": "Sorry, we are currently experiencing problems with our server. Please try again later."
      },
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      }
    },
    "ServiceUnavailable": {
      "description": "JSON response in case the requested service is unavailable"
    }
  },
  "x-servers": "eui9.account.t-mobile.com"
}