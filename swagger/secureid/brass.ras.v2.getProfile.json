{
  "swagger": "2.0",
  "info": {
    "version": "1.0.0",
    "title": "BRASS - RAS",
    "description": "A micro service with Restful and extensible native extensions for use by the T-Mobile APP.",
    "contact": {
      "name": "SecureID Support",
      "email": "SecureID_Support@T-Mobile.com"
    }
  },
  "host": "brass.account.t-mobile.com",
  "x-servers": [
    {
      "url": "https://{Environment}brass.account.t-mobile.com",
      "variables": {
        "Environment": {
          "enum": [
            "dev.",
            "qat.",
            "uat.",
            "plab.",
            "ppd."
          ],
          "default": "uat."
        }
      }
    }
  ],
  "basePath": "/ras/v2",
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "tags": [
    {
      "name": "Get access token and get profile",
      "description": "API that returns access token response and user token (JWT) and user info for given user_id, password and scope parameters."
    }
  ],
  "paths": {
    "/getProfile": {
      "post": {
        "x-api-pattern": "ExecuteFunction",
        "summary": "gets the profile information",
        "operationId": "getProfile",
        "description": "Returns user profile by taking access token from id_token",
        "tags": [
          "profile"
        ],
        "security": [
          {},
          {
            "BasicAuth": []
          },
          {
            "DatToken": []
          },
          {
            "PopToken": []
          }
        ],
        "parameters": [
          {
            "$ref": "#/parameters/authorizationHeader"
          },
          {
            "$ref": "#/parameters/contentTypeHeader"
          },
          {
            "$ref": "#/parameters/acceptLangaugeHeader"
          },
          {
            "$ref": "#/parameters/x-AuthorizationHeader"
          },
          {
            "in": "body",
            "name": "getProfileV2Request",
            "description": "Network details payload",
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/getProfileRequest"
                }
              ]
            }
          }
        ],
        "responses": {
          "200": {
            "description": "JSON response with user profile details returned as-is from IAM. Filtering of response will happen at IAM side.",
            "schema": {
              "$ref": "#/definitions/ProfileResponse"
            },
            "headers": {
              "Date": {
                "type": "string",
                "format": "date-time",
                "description": "Date of Transaction",
                "x-example": "2002-09-24+06:00"
              },
              "Content-Type": {
                "type": "string",
                "format": "application/json",
                "description": "Type of returned content",
                "x-example": "application/json"
              }
            },
            "examples": {
              "application/json": {
                "iamProfile": {
                  "email": "ckent@t-mobile.com",
                  "userId": "U-2b0ab7bb-1f20-410d-b94c-7c4acbcb70dc",
                  "firstName": "Clark",
                  "lastName": "Kent"
                },
                "associatedLines": [
                  {
                    "controlledPlanType": "CLS"
                  }
                ],
                "extendedLines": [
                  {
                    "lineType": "CLS"
                  }
                ]
              }
            }
          },
          "400": {
            "description": "JSON response for bad input, instances like invalid access token",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "status": 400,
                "error": "Unable to return user information",
                "error_description": "Unable to return user information"
              }
            }
          },
          "401": {
            "description": "JSON response for invalid tokens - DAT/POP/id_token",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "error": "Unauthorized",
                "error_description": "Unauthorized Client"
              }
            }
          },
          "500": {
            "description": "JSON response in case of internal system error, in the event of downstream systems unavailability or uncaught exceptions",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "error_code": "internal_server_error",
                "error_description": "Sorry, we are currently experiencing problems with our server. Please try again later."
              }
            }
          }
        }
      }
    }
  },
  "securityDefinitions": {
    "BasicAuth": {
      "type": "basic",
      "description": "Standard basic authentication"
    },
    "DatToken": {
      "description": "Token for authorizing the client, checked by middleware for any not whitelisted connections",
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    },
    "PopToken": {
      "description": "Token used to validate the DatToken, checked by middleware for any not whitelisted connections",
      "type": "apiKey",
      "name": "x-authorization",
      "in": "header"
    }
  },
  "definitions": {
    "getProfileRequest": {
      "type": "object",
      "description": "This object will contain profile request information",
      "required": [
        "access_token",
        "trans_id",
        "id_token"
      ],
      "properties": {
        "access_token": {
          "type": "string",
          "format": "string",
          "description": "access token",
          "example": "01.USR.N2oF2UZnKoy4ExNYY"
        },
        "trans_id": {
          "type": "string",
          "format": "string",
          "description": "transaction id",
          "example": "2b0ab7bb-1f20-410d-b94c-7c4acbcb70dc"
        },
        "id_token": {
          "type": "string",
          "format": "string",
          "description": "User ID Token (JWT)",
          "example": "eyJ0eXAiOiJKV0UiLCJhbGciOiJSU0ExXzUiLCJlbmMiOiJBMjU2Q0JDK0hTNTEyIn0.POUx6l4e3cGNHjwCEv--8WGHRwo9L-byMdCLLKFp55nooZcIEQ-4YN4yGFawr8_MmOiOBKHSqtzyfFwttJIjAgTzUAyN4ZQjMMMxder8oKH9wJ_q1qSeXbGUG9GsCS7j4iW8nU4NEmERqAc8gpZ7QJHOdrZQcjgaaMut4JFs642wY48K0Lnu_1YWx8tR09fWOPgVFBqxbI7xWvDcIbCdIHnP7f_OIl_LJc1n5-Qh2K4erlYz19GjZ4pgem6Ue1G5f5KkjMQ0jyTEwwpWq1WhVMkbOjVMY-wSb_nKsFbWRSLJsI7lV0CQRRnyoEvxsIpKYrBV3gO8LILbOm5H8_2hDQ.HMWRw824lfzTtc0DfZ-DKw.L9rDMXlDY_-iFjYpQJ-cBQDAR1B5rnC5BgnA7Asr4LEr5Ik7F3Vhvu6W2jiA7YIinqpxXnlBBVPcAj4efY5bRw.LjYoRZjFM0zXq11sS7516pS3lv0pzg0vMd_TK9-LJbKzB27fmR84dQhAhoYUNzENgXTfCd3poVJkij-9TY1HHg"
        }
      }
    },
    "ProfileResponse": {
      "type": "object",
      "description": "This object will contain user profile information",
      "properties": {
        "iamProfile": {
          "type": "object",
          "description": "This object will contain IAM profile information",
          "properties": {
            "email": {
              "type": "string",
              "format": "string",
              "x-example": "test@t-mobile.com",
              "description": "Email"
            },
            "userId": {
              "type": "string",
              "format": "string",
              "x-example": "U-2b0ab7bb-1f20-410d-b94c-7c4acbcb70dc",
              "description": "User id"
            },
            "firstName": {
              "type": "string",
              "format": "string",
              "x-example": "test",
              "description": "First name"
            },
            "lastName": {
              "type": "string",
              "format": "string",
              "x-example": "test",
              "description": "Last name"
            }
          }
        },
        "associated_lines": {
          "type": "array",
          "description": "This object contains the associated lines information",
          "items": {
            "type": "object",
            "properties": {
              "controlledPlanType": {
                "type": "string",
                "format": "string",
                "x-example": "CLS",
                "description": "controlledPlanType"
              }
            }
          }
        },
        "extendedLines": {
          "type": "array",
          "description": "This object contains the extended lines information",
          "items": {
            "type": "object",
            "properties": {
              "lineType": {
                "type": "string",
                "format": "string",
                "x-example": "Legacy",
                "description": "lineType"
              }
            }
          }
        }
      }
    },
    "ErrorResponse": {
      "type": "object",
      "properties": {
        "error": {
          "type": "string",
          "description": "Error code"
        },
        "error_description": {
          "type": "string",
          "description": "Error description"
        }
      }
    }
  },
  "parameters": {
    "contentTypeHeader": {
      "in": "header",
      "name": "content-type",
      "type": "string",
      "description": "Type of the returned content",
      "format": "application/json",
      "x-example": "application/json",
      "required": true
    },
    "authorizationHeader": {
      "name": "Authorization",
      "in": "header",
      "type": "string",
      "required": true,
      "description": "DAT token",
      "x-example": "eyJ0eXAiOiJKV0UiLCJhbGciOiJSU0E"
    },
    "acceptLangaugeHeader": {
      "name": "Accept-Language",
      "in": "header",
      "type": "string",
      "description": "Accept language",
      "x-example": "*"
    },
    "x-AuthorizationHeader": {
      "name": "x-Authorization",
      "in": "header",
      "type": "string",
      "required": true,
      "description": "POP token",
      "x-example": "eyJ0eXAiOiJKV0UiLCJhbGciOiJSU0E"
    }
  }
}