{
  "swagger": "2.0",
  "info": {
    "version": "2.0.0",
    "title": "Token Management Service",
    "description": "A micro service responsible for returning access_token,id_tokens and information about line and user profiles",
    "contact": {
      "name": "SecureID On-call Support",
      "email": "EUIDevelopment@T-Mobile.com"
    }
  },
  "host": "brass.account.t-mobile.com",
  "x-servers": "brass.account.t-mobile.com",
  "basePath": "/tms",
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "tags": [
    {
      "name": "TMS",
      "description": "Token Management Service"
    }
  ],
  "paths": {
    "/v1/healthcheck": {
      "get": {
        "x-api-pattern": "ExecuteFunction",
        "operationId": "requestGetHealthcheck",
        "description": "Attemts to check if healthcheck up and running",
        "tags": [
          "TMS"
        ],
        "security": [
          {},
          {
            "BasicAuth": []
          },
          {
            "DatToken": []
          },
          {
            "PopToken": []
          }
        ],
        "parameters": [
          {
            "in": "body",
            "name": "requestBody",
            "description": "Request JSON body for healthcheck",
            "schema": {
              "$ref": "#/definitions/HealthCheckRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "JSON response with user id_token",
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/HealthCheckResponse"
                }
              ]
            },
            "examples": {
              "access_token": "01.USR.EG7ZSusQzR4wrXTWj",
              "access_token_type": "Bearer",
              "access_token_ttl": "3600",
              "scope": "TMO_ID_profile associated_lines extended_lines",
              "tmobileid": "660a9323b8071a2dd159c603045cf0c3464f9e796d019936c772d426aca5cacb",
              "user_id": "6cf1b969e1a538367b59b6589a3c260130dd2e5c65e617b5121485f4f915134a",
              "id_tokens": [
                {
                  "id_token.basic": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI"
                }
              ],
              "userInfo": {
                "iamProfile": {
                  "email": "malik143@yopmail.com"
                },
                "associatedLines": [
                  {
                    "linkTypes": [
                      "direct"
                    ]
                  }
                ],
                "extendedLines": [
                  {
                    "customers": [
                      {
                        "userId": "U-abfc7ee7-c664-4d00-95de-e58501200d71"
                      }
                    ],
                    "contracts": [
                      {
                        "msisdn": "7142341535"
                      }
                    ]
                  }
                ]
              }
            }
          }
        }
      }
    },
    "/v1/usertoken": {
      "post": {
        "x-api-pattern": "ExecuteFunction",
        "operationId": "requestVOnePostUserToken",
        "tags": [
          "TMS"
        ],
        "security": [
          {},
          {
            "BasicAuth": []
          },
          {
            "DatToken": []
          },
          {
            "PopToken": []
          }
        ],
        "description": "Returns Access token and Session details",
        "parameters": [
          {
            "$ref": "#/parameters/contentTypeHeader"
          },
          {
            "$ref": "#/parameters/authorizationHeader"
          },
          {
            "$ref": "#/parameters/xauthorizationHeader"
          },
          {
            "in": "body",
            "name": "requestBody",
            "description": "Request JSON body for User Token resource",
            "schema": {
              "$ref": "#/definitions/UserTokenOneRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "JSON response with user JWT, response elements from IAM API(s) /token and /userinfo",
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/AccessToken"
                },
                {
                  "$ref": "#/definitions/IamProfile"
                },
                {
                  "$ref": "#/definitions/ExtendedLines"
                }
              ]
            }
          },
          "206": {
            "description": "Partial response in the event of failure to return ID token and/or user info (if requested)",
            "schema": {
              "$ref": "#/definitions/PartialResponseUserInfo"
            }
          },
          "400": {
            "description": "JSON response in case of missing required inbound parameters",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "401": {
            "description": "JSON response for invalid DAT and/or PoP tokens, untrusted sources trying to access API directly",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "403": {
            "description": "JSON response in case of authorization failes. Example missing/invalid client_id, redirect_uri",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "500": {
            "description": "JSOn response in case of internal system error, in the event of downstream systems unavailability or uncaught exceptions",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          }
        }
      }
    },
    "/v2/usertoken": {
      "post": {
        "x-api-pattern": "ExecuteFunction",
        "operationId": "requestVTwoPostUserToken",
        "tags": [
          "TMS"
        ],
        "security": [
          {},
          {
            "BasicAuth": []
          },
          {
            "DatToken": []
          },
          {
            "PopToken": []
          }
        ],
        "description": "Returns Access token and Session details",
        "parameters": [
          {
            "$ref": "#/parameters/contentTypeHeader"
          },
          {
            "$ref": "#/parameters/authorizationHeader"
          },
          {
            "$ref": "#/parameters/xauthorizationHeader"
          },
          {
            "in": "body",
            "name": "requestBody",
            "description": "Request JSON body for User Token resource",
            "schema": {
              "$ref": "#/definitions/UserTokenOneRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "JSON response with user JWT, response elements from IAM API(s) /token and /userinfo",
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/AccessToken"
                },
                {
                  "$ref": "#/definitions/IamProfile"
                },
                {
                  "$ref": "#/definitions/ExtendedLines"
                }
              ]
            }
          },
          "206": {
            "description": "Partial response in the event of failure to return ID token and/or user info (if requested)",
            "schema": {
              "$ref": "#/definitions/PartialResponseUserInfo"
            }
          },
          "400": {
            "description": "JSON response in case of missing required inbound parameters",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "401": {
            "description": "JSON response for invalid DAT and/or PoP tokens, untrusted sources trying to access API directly",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "403": {
            "description": "JSON response in case of authorization failes. Example missing/invalid client_id, redirect_uri",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "500": {
            "description": "JSOn response in case of internal system error, in the event of downstream systems unavailability or uncaught exceptions",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          }
        }
      }
    },
    "/v3/usertoken": {
      "post": {
        "x-api-pattern": "ExecuteFunction",
        "operationId": "requestPostUserToken",
        "tags": [
          "TMS"
        ],
        "security": [
          {},
          {
            "BasicAuth": []
          },
          {
            "DatToken": []
          },
          {
            "PopToken": []
          }
        ],
        "description": "Returns Access token and Session details",
        "parameters": [
          {
            "$ref": "#/parameters/contentTypeHeader"
          },
          {
            "$ref": "#/parameters/authorizationHeader"
          },
          {
            "$ref": "#/parameters/xauthorizationHeader"
          },
          {
            "in": "body",
            "name": "requestBody",
            "description": "Request JSON body for User Token resource",
            "schema": {
              "$ref": "#/definitions/UserTokenPostRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "JSON response with user access_token, response elements from IAM API(s) /token and /userinfo",
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/AccessToken"
                },
                {
                  "$ref": "#/definitions/UserInfo"
                }
              ]
            },
            "examples": {
              "access_token": "01.USR.EG7ZSusQzR4wrXTWj",
              "access_token_type": "Bearer",
              "access_token_ttl": "3600",
              "scope": "TMO_ID_profile associated_lines extended_lines",
              "tmobileid": "660a9323b8071a2dd159c603045cf0c3464f9e796d019936c772d426aca5cacb",
              "user_id": "6cf1b969e1a538367b59b6589a3c260130dd2e5c65e617b5121485f4f915134a",
              "id_tokens": [
                {
                  "id_token.basic": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI"
                }
              ],
              "userInfo": {
                "iamProfile": {
                  "email": "malik143@yopmail.com"
                },
                "associatedLines": [
                  {
                    "linkTypes": [
                      "direct"
                    ]
                  }
                ],
                "extendedLines": [
                  {
                    "customers": [
                      {
                        "userId": "U-abfc7ee7-c664-4d00-95de-e58501200d71"
                      }
                    ],
                    "contracts": [
                      {
                        "msisdn": "7142341535"
                      }
                    ]
                  }
                ]
              }
            }
          },
          "206": {
            "description": "Partial response in the event of failure to return ID token and/or user info (if requested)",
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/AccessTokenPartial"
                },
                {
                  "$ref": "#/definitions/UserInfo"
                }
              ]
            },
            "examples": {
              "access_token": "01.USR.EG7ZSusQzR4wrXTWj",
              "access_token_type": "Bearer",
              "access_token_ttl": "3600",
              "scope": "TMO_ID_profile associated_lines extended_lines",
              "tmobileid": "660a9323b8071a2dd159c603045cf0c3464f9e796d019936c772d426aca5cacb",
              "error": "Not provisioned to receive id_token or problems while retrieving id_token(s)",
              "userInfo": {
                "iamProfile": {
                  "email": "malik143@yopmail.com"
                },
                "associatedLines": [
                  {
                    "linkTypes": [
                      "direct"
                    ]
                  }
                ],
                "extendedLines": [
                  {
                    "customers": [
                      {
                        "userId": "U-abfc7ee7-c664-4d00-95de-e58501200d71"
                      }
                    ],
                    "contracts": [
                      {
                        "msisdn": "7142341535"
                      }
                    ]
                  }
                ]
              }
            }
          },
          "403": {
            "$ref": "#/responses/InvalidRequest"
          },
          "500": {
            "$ref": "#/responses/ServerError"
          }
        }
      }
    },
    "/v4/usertoken": {
      "post": {
        "x-api-pattern": "ExecuteFunction",
        "operationId": "requestFourPostUserToken",
        "tags": [
          "TMS"
        ],
        "security": [
          {},
          {
            "BasicAuth": []
          },
          {
            "DatToken": []
          },
          {
            "PopToken": []
          }
        ],
        "description": "Returns Access token and Session details",
        "parameters": [
          {
            "$ref": "#/parameters/contentTypeHeader"
          },
          {
            "$ref": "#/parameters/authorizationHeader"
          },
          {
            "$ref": "#/parameters/xauthorizationHeader"
          },
          {
            "in": "body",
            "name": "requestBody",
            "description": "Request JSON body for User Token resource",
            "schema": {
              "$ref": "#/definitions/UserTokenPostRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "JSON response with user access_token, response elements from IAM API(s) /token and /userinfo",
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/AccessToken"
                },
                {
                  "$ref": "#/definitions/UserInfo"
                }
              ]
            },
            "examples": {
              "access_token": "01.USR.EG7ZSusQzR4wrXTWj",
              "access_token_type": "Bearer",
              "access_token_ttl": "3600",
              "scope": "TMO_ID_profile associated_lines extended_lines",
              "tmobileid": "660a9323b8071a2dd159c603045cf0c3464f9e796d019936c772d426aca5cacb",
              "user_id": "6cf1b969e1a538367b59b6589a3c260130dd2e5c65e617b5121485f4f915134a",
              "id_tokens": [
                {
                  "id_token.basic": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI"
                }
              ],
              "userInfo": {
                "iamProfile": {
                  "email": "malik143@yopmail.com"
                },
                "associatedLines": [
                  {
                    "linkTypes": [
                      "direct"
                    ]
                  }
                ],
                "extendedLines": [
                  {
                    "customers": [
                      {
                        "userId": "U-abfc7ee7-c664-4d00-95de-e58501200d71"
                      }
                    ],
                    "contracts": [
                      {
                        "msisdn": "7142341535"
                      }
                    ]
                  }
                ]
              }
            }
          },
          "206": {
            "description": "Partial response in the event of failure to return ID token and/or user info (if requested)",
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/AccessTokenPartial"
                },
                {
                  "$ref": "#/definitions/UserInfo"
                }
              ]
            },
            "examples": {
              "access_token": "01.USR.EG7ZSusQzR4wrXTWj",
              "access_token_type": "Bearer",
              "access_token_ttl": "3600",
              "scope": "TMO_ID_profile associated_lines extended_lines",
              "tmobileid": "660a9323b8071a2dd159c603045cf0c3464f9e796d019936c772d426aca5cacb",
              "error": "Not provisioned to receive id_token or problems while retrieving id_token(s)",
              "userInfo": {
                "iamProfile": {
                  "email": "malik143@yopmail.com"
                },
                "associatedLines": [
                  {
                    "linkTypes": [
                      "direct"
                    ]
                  }
                ],
                "extendedLines": [
                  {
                    "customers": [
                      {
                        "userId": "U-abfc7ee7-c664-4d00-95de-e58501200d71"
                      }
                    ],
                    "contracts": [
                      {
                        "msisdn": "7142341535"
                      }
                    ]
                  }
                ]
              }
            }
          },
          "403": {
            "$ref": "#/responses/InvalidRequest"
          },
          "500": {
            "$ref": "#/responses/ServerError"
          }
        }
      },
      "put": {
        "x-api-pattern": "ExecuteFunction",
        "operationId": "requestPutUserToken",
        "tags": [
          "TMS"
        ],
        "security": [
          {},
          {
            "BasicAuth": []
          },
          {
            "DatToken": []
          },
          {
            "PopToken": []
          }
        ],
        "parameters": [
          {
            "$ref": "#/parameters/contentTypeHeader"
          },
          {
            "$ref": "#/parameters/authorizationHeader"
          },
          {
            "$ref": "#/parameters/xauthorizationHeader"
          },
          {
            "in": "body",
            "name": "requestBody",
            "description": "Request JSON body for User Token resource",
            "schema": {
              "$ref": "#/definitions/UserTokenPutRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/SuccessResponse"
          },
          "401": {
            "$ref": "#/responses/UnauthorizedRequest"
          },
          "403": {
            "$ref": "#/responses/InvalidRequest"
          },
          "500": {
            "$ref": "#/responses/ServerError"
          }
        }
      }
    }
  },
  "securityDefinitions": {
    "BasicAuth": {
      "type": "basic",
      "description": "Standard basic authentication"
    },
    "DatToken": {
      "description": "Token for authorizing the client, checked by middleware for any not whitelisted connections",
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    },
    "PopToken": {
      "description": "Token used to validate the DatToken, checked by middleware for any not whitelisted connections",
      "type": "apiKey",
      "name": "x-authorization",
      "in": "header"
    }
  },
  "definitions": {
    "HealthCheckRequest": {},
    "HealthCheckResponse": {
      "type": "object",
      "properties": {
        "serverState": {
          "type": "string",
          "format": "byte",
          "description": "TMS Node server is up and running"
        }
      }
    },
    "UserTokenOneRequest": {
      "type": "object",
      "properties": {
        "trans_id": {
          "type": "string"
        },
        "code": {
          "type": "string"
        },
        "client_id": {
          "type": "string"
        },
        "client_secret": {
          "type": "string"
        },
        "redirect_uri": {
          "type": "string"
        },
        "is_user_info_required": {
          "type": "boolean"
        },
        "interaction_id": {
          "type": "string"
        },
        "scopes": {
          "type": "string"
        }
      }
    },
    "PartialResponseUserInfo": {
      "type": "object",
      "properties": {
        "access_token": {
          "type": "string"
        },
        "access_token_type": {
          "type": "string"
        },
        "access_token_ttl": {
          "type": "string"
        },
        "refresh_token": {
          "type": "string"
        },
        "scope": {
          "type": "string"
        },
        "tmobileid": {
          "type": "string"
        }
      }
    },
    "UserTokenPostRequest": {
      "type": "object",
      "required": [
        "code",
        "client_id",
        "client_secret",
        "redirect_uri",
        "trans_id",
        "id_token"
      ],
      "properties": {
        "trans_id": {
          "type": "string",
          "description": "Unique identifier for auditing",
          "example": "ghjk677"
        },
        "code": {
          "type": "string",
          "description": "Access code for authorizing user token request",
          "format": "byte",
          "example": "01.GzsAyyLLy21SGETwE"
        },
        "interaction_id": {
          "type": "string",
          "description": "able to track user session",
          "format": "byte",
          "example": "test123"
        },
        "client_id": {
          "type": "string",
          "description": "Calling application authorization secret client_id",
          "example": "TMOAppNative",
          "format": "string"
        },
        "client_secret": {
          "type": "string",
          "description": "Calling application authorization client_secret",
          "format": "byte",
          "example": "TMOAppNativeSecret"
        },
        "redirect_uri": {
          "type": "string",
          "description": "Client application URI",
          "format": "uri",
          "example": "https://localhost"
        },
        "response_selection": {
          "type": "string",
          "description": "if you pass response_selection will get id_tokens, if you pass userinfo in response_selection will get user profile",
          "example": "id_token.basic userinfo"
        },
        "pub_key": {
          "type": "object",
          "required": [
            "code"
          ],
          "properties": {
            "keyName": {
              "type": "string",
              "description": "In case of DAT not available in request header then this parameter becomes required to populate CNF value in id_tokens",
              "example": "Eydmndsmvshbvskdjvnsvdjsksdfsdvs...."
            }
          }
        }
      }
    },
    "UserTokenPutRequest": {
      "type": "object",
      "required": [
        "code",
        "client_id",
        "client_secret",
        "redirect_uri",
        "trans_id",
        "id_token"
      ],
      "properties": {
        "trans_id": {
          "type": "string",
          "description": "Unique identifier for auditing",
          "example": "ghjk677"
        },
        "code": {
          "type": "string",
          "description": "Access code for authorizing user token request",
          "format": "byte",
          "example": "01.GzsAyyLLy21SGETwE"
        },
        "interaction_id": {
          "type": "string",
          "description": "able to track user session",
          "format": "byte",
          "example": "test123"
        },
        "client_id": {
          "type": "string",
          "description": "Calling application authorization secret client_id",
          "example": "TMOAppNative",
          "format": "string"
        },
        "client_secret": {
          "type": "string",
          "description": "Calling application authorization client_secret",
          "format": "byte",
          "example": "TMOAppNativeSecret"
        },
        "redirect_uri": {
          "type": "string",
          "description": "Client application URI",
          "format": "uri",
          "example": "https://localhost"
        },
        "scope": {
          "type": "string",
          "description": "Processes the request has claim to in IAM",
          "example": "associatedLines TMOIDProfile"
        },
        "response_selection": {
          "type": "string",
          "description": "if you pass response_selection will get id_tokens, if you pass userinfo in response_selection will get user profile",
          "example": "id_token.basic userinfo"
        },
        "pub_key": {
          "type": "object",
          "required": [
            "code"
          ],
          "properties": {
            "keyName": {
              "type": "string",
              "description": "In case of DAT not available in request header then this parameter becomes required to populate CNF value in id_tokens",
              "example": "Eydmndsmvshbvskdjvnsvdjsksdfsdvs...."
            }
          }
        },
        "id_token": {
          "type": "string",
          "description": "If you pass id_token will get new id_token for new client id",
          "example": "Eydmndsmvshbvskdjvnsvdjsksdfsdvs...."
        }
      }
    },
    "AccessToken": {
      "type": "object",
      "properties": {
        "access_token": {
          "type": "string",
          "format": "bit",
          "description": "access_token returned by the authorization service"
        },
        "access_token_type": {
          "type": "string",
          "format": "byte",
          "description": "Access type of client"
        },
        "access_token_ttl": {
          "type": "string",
          "format": "number",
          "description": "Expiration time of the token"
        },
        "scope": {
          "type": "string",
          "description": "Processes the request has claim to in IAM"
        },
        "tmobileid": {
          "type": "string",
          "format": "byte",
          "description": "Salted IAM client identifier"
        },
        "user_id": {
          "type": "string",
          "format": "byte",
          "description": "user_id for the user"
        },
        "id_tokens": {
          "type": "array",
          "description": "id_token for the user",
          "items": {
            "type": "object",
            "properties": {
              "id_token.basic": {
                "type": "string",
                "format": "bit",
                "description": "JWT token returned by the authorization service"
              }
            }
          }
        }
      }
    },
    "AccessTokenPartial": {
      "type": "object",
      "properties": {
        "access_token": {
          "type": "string",
          "format": "bit",
          "description": "access_token returned by the authorization service"
        },
        "access_token_type": {
          "type": "string",
          "format": "byte",
          "description": "Access type of client"
        },
        "access_token_ttl": {
          "type": "string",
          "format": "number",
          "description": "Expiration time of the token"
        },
        "scope": {
          "type": "string",
          "description": "Processes the request has claim to in IAM"
        },
        "tmobileid": {
          "type": "string",
          "format": "byte",
          "description": "Salted IAM client identifier"
        },
        "error": {
          "type": "string",
          "format": "byte",
          "description": "Not provisioned to receive id_token or problems while retrieving id_token(s)"
        }
      }
    },
    "UserInfo": {
      "type": "object",
      "properties": {
        "userInfo": {
          "type": "object",
          "properties": {
            "iamProfile": {
              "$ref": "#/definitions/IamProfile"
            },
            "associatedLines": {
              "$ref": "#/definitions/AssociatedLines"
            },
            "extendedLines": {
              "$ref": "#/definitions/ExtendedLines"
            }
          }
        }
      }
    },
    "IamProfile": {
      "type": "object",
      "properties": {
        "email": {
          "type": "string",
          "description": "User's email"
        },
        "userId": {
          "type": "string",
          "description": "Internal identifier of user"
        },
        "firstName": {
          "type": "string",
          "description": "User's first name"
        },
        "lastName": {
          "type": "string",
          "description": "User's last name"
        }
      }
    },
    "AssociatedLines": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "linkTypes": {
            "type": "array",
            "items": {
              "type": "string"
            }
          }
        }
      }
    },
    "ExtendedLines": {
      "type": "object",
      "properties": {
        "billingAccountCode": {
          "type": "string",
          "description": "User billing account code"
        },
        "customers": {
          "type": "array",
          "description": "User's associated with billing account",
          "items": {
            "$ref": "#/definitions/Customers"
          }
        },
        "contracts": {
          "type": "array",
          "description": "User's associated with billing account",
          "items": {
            "$ref": "#/definitions/Contracts"
          }
        }
      }
    },
    "Customers": {
      "type": "object",
      "properties": {
        "billingAccountNumber": {
          "type": "string",
          "description": "User billing account identifier"
        }
      }
    },
    "Contracts": {
      "type": "object",
      "properties": {
        "msisdn": {
          "type": "string",
          "description": "User billing account identifier"
        }
      }
    },
    "SuccessModel": {
      "type": "object",
      "properties": {
        "id_tokens": {
          "type": "array",
          "description": "id_token for the user",
          "items": {
            "type": "object",
            "properties": {
              "id_token.basic": {
                "type": "string",
                "format": "bit",
                "description": "JWT token returned by the authorization service"
              }
            }
          }
        }
      }
    },
    "ErrorModel": {
      "type": "object",
      "properties": {
        "error": {
          "type": "string",
          "description": "invalid_request"
        },
        "error_description": {
          "type": "string",
          "description": "Invalid oauth parameters"
        }
      },
      "required": [
        "error",
        "error_description"
      ]
    }
  },
  "parameters": {
    "contentTypeHeader": {
      "in": "header",
      "name": "content-type",
      "type": "string",
      "description": "Type of the returned content",
      "required": true,
      "format": "application/json",
      "x-example": "application/json"
    },
    "authorizationHeader": {
      "in": "header",
      "name": "authorization",
      "type": "string",
      "description": "JWT token returned by the authorization service",
      "required": false,
      "format": "authorization",
      "x-example": "Bearer asdasdasd. . ."
    },
    "xauthorizationHeader": {
      "in": "header",
      "name": "x-authorization",
      "type": "string",
      "description": "JWT token returned by the authorization service",
      "required": false,
      "format": "x-authorization",
      "x-example": "Bearer asdasdasd. . ."
    }
  },
  "responses": {
    "SuccessResponse": {
      "description": "JSON response with user id Tokens",
      "schema": {
        "$ref": "#/definitions/SuccessModel"
      },
      "examples": {
        "id_tokens": [
          {
            "id_token.basic": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI"
          }
        ]
      }
    },
    "UnauthorizedRequest": {
      "description": "incoming request doesn't have authorization header or invalid apigee token",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "error": "Unauthorized",
        "error_description": "Unauthorized client"
      }
    },
    "InvalidRequest": {
      "description": "incoming request doesn't have require parameters",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "error": "invalid_request",
        "error_description": "Invalid oauth parameters"
      }
    },
    "ServerError": {
      "description": "JSON response in case of internal system error, in the event of downstream systems unavailability or uncaught exceptions",
      "schema": {
        "$ref": "#/definitions/ErrorModel"
      },
      "examples": {
        "error": "internal_server_error",
        "error_description": "Sorry, we are currently experiencing problems with our server. Please try again later."
      }
    }
  }
}