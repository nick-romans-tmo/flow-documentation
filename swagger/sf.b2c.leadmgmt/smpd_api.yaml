swagger: '2.0'
info:
  description: >
    SMPD SMSC/MMSC API for connecting to T-Mobile's text messaging services.
    Please contact the SMPD to create an account for this API. We use oAuth2 for
    sending messages and a webhook/token combination of your specification for
    notifications.


    The SMPD group provides access to sending text messages through the SMSC and
    MMSC.  The API grants access through a standard oAuth2 interface.  Please
    contact T-Mobile SMPD Engineering to obtain access credentials.  When
    contacting the SMPD group, you will need to provide the location of your
    webhook for inbound messages.

    _____________________________________

    # Message Flow

    First off, begin by configuring your webhook.  Your webhook is an HTTPS URL
    through which you will receive message notifications from the SMPD API.  The
    API sends messages to this webhook as soon as the SMPD service receives
    notification and the conversation is deemed pertinent to your account. 
    “Inbound” messages (messages coming to you) will be easily distinguishable
    as text- vs. media-type messages.  Furthermore, these messages will maintain
    both a conversation ID and a message ID to provide tracking information
    pertinent to the message.


    Media-type message notifications will come as a JSON package with media IDs
    therein.  Your system will be required, then, to obtain the file data
    through the API endpoint provided.


    If you feel you are not receiving the traffic your account should receive,
    please contact the SMPD Engineering Group to ensure your account has the
    correct permissions granted it.


    ## Conversations, MSISDNs, and Sources

    The API considers the combination of a MSISDN and source as the identifier
    for a conversation.  When receiving notifications from the API, the
    conversation ID can be retained for quicker messaging through the API and
    should be sent along with “Send Message” requests.  If the conversation ID
    is unknown, you will need to specify both the MSISDN and source properties
    for your outbound message.  In this case, the API will then perform a lookup
    for the conversation ID before the message is sent.  Either way, the
    response to your send request will contain both the conversation ID and the
    message ID for the message.


    ## Sending Standard Text Messages

    Sending a standard text message simply requires a conversation ID and text. 
    You will send your notification and receive confirmation of its delivery to
    the internal T-Mobile messaging services.


    ## Sending Media Files

    Sending media files is a two-step process.  Begin by uploading your media
    files and receiving a media ID in response.  Then, create your media-type
    message with one or more media IDs referenced.  Again, you will receive
    confirmation of its delivery to the internal T-Mobile messaging services.

    _____________________________________
  version: 1.0.0
  title: SMPD SMSC/MMSC API
  termsOfService: 'http://api.message.corporate.t-mobile.com/v1/text/terms'
  contact:
    email: david.hoffman16@t-mobile.com
  license:
    name: Apache 2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
host: api.message.corporate.t-mobile.com
basePath: /v1/text
tags:
  - name: webhook
    description: Receive messages from T-Mobile
  - name: send
    description: Send a message to a MSISDN
  - name: media
    description: Upload and access media files for media-type messages
schemes:
  - https
paths:
  /webhook/ping:
    get:
      tags:
        - webhook
      summary: Receive a ping on your account's webhook
      description: >-
        When you want a test ping to your webhook, call this endpoint with your
        oAuth2 credentials and a ping will be sent to your webhook endpoint.
      operationId: webhookPing
      produces:
        - application/json
      responses:
        '200':
          description: Ping sent to your webhook
          schema:
            $ref: '#/definitions/SuccessResponse'
        '201':
          description: >-
            Not really a 201 - Definition of callback with ping to your webhook,
            will maintain a header 'token' with the token you specified for your
            webhook
          schema:
            $ref: '#/definitions/WebhookPing'
  /webhook/start:
    get:
      tags:
        - webhook
      summary: Start traffic to your account's webhook
      description: >-
        When you want a test ping to your webhook, call this endpoint with your
        oAuth2 credentials and a ping will be sent to your webhook endpoint.
      operationId: webhookStart
      produces:
        - application/json
      responses:
        '200':
          description: Traffic to your webhook has started
          schema:
            $ref: '#/definitions/WebhookStatus'
        '201':
          description: >-
            Not really a 201 - Definition of callback with message notifications
            to your webhook, will maintain a header 'token' with the token you
            specified for your webhook
          schema:
            $ref: '#/definitions/MessageNotification'
      security:
        - OAuth2:
            - read
  /webhook/stop:
    get:
      tags:
        - webhook
      summary: Stop traffic to your account's webhook
      description: >-
        When you want a test ping to your webhook, call this endpoint with your
        oAuth2 credentials and a ping will be sent to your webhook endpoint.
      operationId: webhookStop
      produces:
        - application/json
      responses:
        '200':
          description: Traffic to your webhook has stopped
          schema:
            $ref: '#/definitions/WebhookStatus'
      security:
        - OAuth2:
            - read
  /send:
    post:
      tags:
        - send
      summary: Send a message
      description: >-
        If you know the conversation ID that you are posting to, that is best.
        Otherwise, you must provide both a MSISDN and source so we can
        find/generate the conversation ID for the interaction with the user. 
        Sending a message requires that your MSISDN and source map to a
        conversation your account has privileges to.
      operationId: sendMessage
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: body
          name: body
          description: Message object to be sent
          required: true
          schema:
            $ref: '#/definitions/Message'
      responses:
        '200':
          description: Message sent
          schema:
            $ref: '#/definitions/MessageResponse'
        '405':
          description: Invalid input
          schema:
            $ref: '#/definitions/ErrorResponse'
      security:
        - OAuth2:
            - send
  '/media/{mediaId}':
    get:
      tags:
        - media
      summary: Retrieve a media file
      description: >-
        When receiving a media-type message, one uses this endpoint to retrieve
        the files associated with the message.
      operationId: retrieveMedia
      produces:
        - file data
      parameters:
        - in: path
          name: mediaId
          type: string
          description: ID for media file you are retrieving
          required: true
      responses:
        '200':
          description: Media file
      security:
        - OAuth2:
            - read
  /media/upload:
    post:
      tags:
        - media
      summary: Upload a media file
      description: >-
        When sending a text with one or more media files, you must first upload
        the files to retrieve media IDs for them.
      operationId: uploadMedia
      consumes:
        - multipart/form-data
      produces:
        - application/json
      parameters:
        - in: formData
          name: file
          type: file
          description: The file to upload.
          required: true
      responses:
        '200':
          description: File uploaded
          schema:
            $ref: '#/definitions/UploadMediaResponse'
        '405':
          description: Invalid input
          schema:
            $ref: '#/definitions/ErrorResponse'
      security:
        - OAuth2:
            - send
securityDefinitions:
  OAuth2:
    type: oauth2
    flow: accessCode
    authorizationUrl: 'https://example.com/oauth/authorize'
    tokenUrl: 'https://example.com/oauth/token'
    scopes:
      send: send messages to conversations
      start: start a conversation with a MSISDN/Source
      read: receive messages and download media
definitions:
  Message:
    type: object
    description: >-
      The Message you would like to send.  If conversationId is available to
      you, please send that as the identifier.  Otherwise, you must send a valid
      MSISDN/source combination.
    required:
      - type
    properties:
      type:
        type: string
        description: Type of message
        enum:
          - text
          - media
      conversationId:
        type: string
        description: Conversation ID
      msisdn:
        type: integer
        format: int64
        description: MSISDN for message
      source:
        type: string
        description: Source ID/number for message
      text:
        type: string
        description: Text for message / label for media list
      media:
        type: array
        items:
          $ref: '#/definitions/MessageMedia'
  MessageMedia:
    type: object
    description: The media ids you would like to send with the message
    required:
      - id
    properties:
      id:
        type: string
        description: Media ID from uploaded media
  MessageResponse:
    type: object
    required:
      - status
      - code
      - message
      - conversationId
      - messageId
    properties:
      status:
        type: string
        enum:
          - success
      code:
        type: integer
        format: int64
        description: Success code generated
      message:
        type: string
        description: Plain-text success message
      conversationId:
        type: string
        description: Conversation ID for message
      messageId:
        type: integer
        format: int64
        description: Order of message in conversation
  UploadMediaResponse:
    type: object
    required:
      - status
      - code
      - message
      - mediaId
    properties:
      status:
        type: string
        enum:
          - success
      code:
        type: integer
        format: int64
        description: Success code generated
      message:
        type: string
        description: Plain-text success message
      mediaId:
        type: string
        description: Media ID for media-type message
  SuccessResponse:
    type: object
    required:
      - status
      - code
      - message
    properties:
      status:
        type: string
        enum:
          - success
      code:
        type: integer
        format: int64
        description: Success code generated
      message:
        type: string
        description: Plain-text success message
  ErrorResponse:
    type: object
    required:
      - status
      - code
      - message
    properties:
      status:
        type: string
        enum:
          - error
      code:
        type: integer
        format: int64
        description: Error code generated
      message:
        type: string
        description: Plain-text error message
  WebhookPing:
    type: object
    required:
      - ping
    properties:
      ping:
        type: string
        description: Ping message
  WebhookStatus:
    type: object
    required:
      - status
      - code
      - message
      - 'on'
    properties:
      status:
        type: string
        enum:
          - success
      code:
        type: integer
        format: int64
        description: Success code generated
      message:
        type: string
        description: Plain-text success message
      'on':
        type: boolean
        description: Is the webhook channel on or off
  MessageNotification:
    type: object
    required:
      - type
      - conversationId
      - messageId
      - msisdn
      - source
    properties:
      type:
        type: string
        description: Type of message
        enum:
          - text
          - media
      conversationId:
        type: string
        description: Conversation ID
      messageId:
        type: integer
        format: int64
        description: Sequence of message in conversation
      msisdn:
        type: integer
        format: int64
        description: MSISDN for user in conversation
      source:
        type: string
        description: T-Mobile source for conversation
      text:
        type: string
        description: Text for message / label for media list
      media:
        type: array
        items:
          $ref: '#/definitions/MessageNotificationMedia'
  MessageNotificationMedia:
    type: object
    required:
      - id
      - size
      - type
    properties:
      id:
        type: string
        description: Media ID from uploaded media
      size:
        type: integer
        format: int64
        description: Size of the file in bytes
      type:
        type: string
        description: MIME-type of file
