{
  "swagger": "2.0",
  "info": {
    "description": "An API developed in Node.JS on Heroku for the Live Engage team to use to allow for creation of Salesforce Leads, Engagements, and lead searching without fully consuming Salesforce APIs. To use this API before any call is made, you must first make a call to the oauth/token route and receive a Bearer token. From then on you may call any of the other APIs as described with the Bearer token in the authorization header.",
    "version": "1.0.0",
    "title": "Lead Services",
    "contact": {
      "email": "jay.mcdoniel1@t-mobile.com"
    }
  },
  "basePath": "/",
  "tags": [
    {
      "name": "Auth",
      "description": "Authentication for the application"
    },
    {
      "name": "Engagement",
      "description": "APIs dealing with the engagement object"
    },
    {
      "name": "Lead",
      "description": "APIs dealing with the lead"
    },
    {
      "name": "Person Account",
      "description": "APIs dealing with the Person Account Object. Part of Home Internet."
    }
  ],
  "schemes": [
    "https"
  ],
  "securityDefinitions": {
    "bearer": {
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    }
  },
  "host": "tmobile-heroku-csf-dev.herokuapp.com",
  "paths": {
    "/oauth/token": {
      "post": {
        "summary": "Token Request",
        "description": "Endpoint for sending the grant_type, client_id, client_secret, and audience to get an Authentication token.",
        "parameters": [
          {
            "name": "AuthReq",
            "required": true,
            "in": "body",
            "schema": {
              "$ref": "#/definitions/AuthReq"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/AuthRes"
            }
          }
        },
        "tags": [
          "Auth"
        ],
        "produces": [
          "application/json"
        ],
        "consumes": [
          "application/json"
        ]
      }
    },
    "/service/lead/search": {
      "post": {
        "summary": "search for lead",
        "description": "The criteria used to search for a lead. Matching criteria is currently one of the following in the following order:\n1. email only\n2. phone only\n3. first name and last name\n4. last name only",
        "parameters": [
          {
            "name": "LeadSearchRequest",
            "required": true,
            "in": "body",
            "schema": {
              "$ref": "#/definitions/LeadSearchRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/LeadSearchResult"
            }
          }
        },
        "tags": [
          "Lead"
        ],
        "security": [
          {
            "bearer": []
          }
        ],
        "produces": [
          "application/json"
        ],
        "consumes": [
          "application/json"
        ]
      }
    },
    "/service/lead/upsert": {
      "post": {
        "summary": "Upsert a lead to Salesforce",
        "description": "The body used to upsert the lead. Criteria to check if the lead exists are as follows in the following order:\n1. email\n2. social id\n3. phone and first name and last name\n4. salesforce id (leadId)",
        "parameters": [
          {
            "name": "LeadUpsertRequest",
            "required": true,
            "in": "body",
            "schema": {
              "$ref": "#/definitions/LeadUpsertRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/LeadUpsertResult"
            }
          }
        },
        "tags": [
          "Lead"
        ],
        "security": [
          {
            "bearer": []
          }
        ],
        "produces": [
          "application/json"
        ],
        "consumes": [
          "application/json"
        ]
      }
    },
    "/service/engagement/upsert": {
      "post": {
        "summary": "Upsert Engagement",
        "description": "The body is used to update the engagement. If the body of the request has an engagementId, it is assumed that this id is correctly formatted to Salesforce standards and is a Salesforce Engagement Id. This id is used for updating the existing engagement.",
        "parameters": [
          {
            "name": "EngagementRequest",
            "required": true,
            "in": "body",
            "schema": {
              "$ref": "#/definitions/EngagementRequest"
            }
          },
          {
            "type": "string",
            "name": "channel",
            "required": true,
            "in": "header"
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/EngagementResult"
            }
          }
        },
        "tags": [
          "Engagement"
        ],
        "security": [
          {
            "bearer": []
          }
        ],
        "produces": [
          "application/json"
        ],
        "consumes": [
          "application/json"
        ]
      }
    },
    "/service/person-account/upsert": {
      "post": {
        "summary": "Upsert Person Account",
        "description": "Api to upsert the person account. If it is a new person account, externalId, accountSource, msisdn, recordType, firstName, and lastName are required.\nIf it is a person account to update, the sfdcId, email, or firstName, lastName, and msisdn is required.\nThe return will always be a returnCode an errorList, and the sfdcId array. If the returnCode is non-zero, there is an error and the errorList should be referred to.",
        "parameters": [
          {
            "name": "PersonAccounts",
            "required": true,
            "in": "body",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/PersonAccountReqDTO"
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/PersonAccountResDTO"
            }
          }
        },
        "tags": [
          "Person Account"
        ],
        "security": [
          {
            "bearer": []
          }
        ],
        "produces": [
          "application/json"
        ],
        "consumes": [
          "application/json"
        ]
      }
    }
  },
  "definitions": {
    "AuthReq": {
      "type": "object",
      "properties": {
        "grant_type": {
          "type": "string",
          "example": "client_credentials"
        },
        "client_id": {
          "type": "string",
          "example": "publicKnowledgeId"
        },
        "client_secret": {
          "type": "string",
          "example": "verySecureSecret"
        },
        "audience": {
          "type": "string",
          "example": "https://live-engage.tmobile.com"
        }
      },
      "required": [
        "grant_type",
        "client_id",
        "client_secret",
        "audience"
      ]
    },
    "Token": {
      "type": "object",
      "properties": {
        "access_token": {
          "type": "string",
          "example": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IlJETTJOVEZFTTBNeU9ESTNNMFV3TVRFeVF6RkJNa013TVRKRk9EZENNek5CUXpjeE1qQkZOQSJ9.eyJpc3MiOiJodHRwczovL3QtbW9iaWxlLmF1dGgwLmNvbS8iLCJzdWIiOiJ3SzFFc1o2TXhBczF1TGdCeG05S0FneTdOd2gxTWN1UEBjbGllbnRzIiwiYXVkIjoiaHR0cHM6Ly9saXZlLWVuZ2FnZS50bW9iaWxlLmNvbSIsImlhdCI6MTU0NTA3MTk4MiwiZXhwIjoxNTQ1MTU4MzgyLCJhenAiOiJ3SzFFc1o2TXhBczF1TGdCeG05S0FneTdOd2gxTWN1UCIsImd0eSI6ImNsaWVudC1jcmVkZW50aWFscyJ9.JLL_1S2c8cKpM3fzWFzzvrFi0EHplgFZX1SWMUx3w16h9GL7mZoLdbdvCFGUdCTHy9nGyxkT9gewBaq8mivuWz1yLjRTIIaV3akcTu8KxpRMI8pVdGuHUx85uPqliMyB23-0IiwiWWyy_yaxikRmFP1UQSmcVqwCK6sMevVTVQIppacQr2Gxp0mbYuwpcpqFddyIIKxtdHEX1bVeaKrOJkpFmG56FwoE8p4XTw4jbt3o4uZNJsd6ZyQhSuy7mm-NsXQmwj9rray8C4AwPtM3PX-T0PfkWcVwl_QUrV_IIKKq5EsO1-p83DKKF9NpAbRd_DdPB0QQwEkwxsNfSU4pfQ"
        },
        "expires_in": {
          "type": "number",
          "example": 86400
        },
        "token_type": {
          "type": "string",
          "example": "Bearer"
        }
      },
      "required": [
        "access_token",
        "expires_in",
        "token_type"
      ]
    },
    "AuthRes": {
      "type": "object",
      "properties": {
        "returnCode": {
          "type": "number",
          "example": 0,
          "default": 0,
          "description": "The return code for the API. Non-Zero codes mean there was some sort of failure and the errorList should be referenced"
        },
        "errorList": {
          "type": "array",
          "example": [],
          "description": "The message of the error that happened. Will only be filled if returnCode is non-zero.",
          "default": [],
          "items": {
            "type": "string"
          }
        },
        "auth": {
          "title": "Token",
          "allOf": [
            {
              "$ref": "#/definitions/Token"
            },
            {
              "example": {
                "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IlJETTJOVEZFTTBNeU9ESTNNMFV3TVRFeVF6RkJNa013TVRKRk9EZENNek5CUXpjeE1qQkZOQSJ9.eyJpc3MiOiJodHRwczovL3QtbW9iaWxlLmF1dGgwLmNvbS8iLCJzdWIiOiJ3SzFFc1o2TXhBczF1TGdCeG05S0FneTdOd2gxTWN1UEBjbGllbnRzIiwiYXVkIjoiaHR0cHM6Ly9saXZlLWVuZ2FnZS50bW9iaWxlLmNvbSIsImlhdCI6MTU0NTA3MTk4MiwiZXhwIjoxNTQ1MTU4MzgyLCJhenAiOiJ3SzFFc1o2TXhBczF1TGdCeG05S0FneTdOd2gxTWN1UCIsImd0eSI6ImNsaWVudC1jcmVkZW50aWFscyJ9.JLL_1S2c8cKpM3fzWFzzvrFi0EHplgFZX1SWMUx3w16h9GL7mZoLdbdvCFGUdCTHy9nGyxkT9gewBaq8mivuWz1yLjRTIIaV3akcTu8KxpRMI8pVdGuHUx85uPqliMyB23-0IiwiWWyy_yaxikRmFP1UQSmcVqwCK6sMevVTVQIppacQr2Gxp0mbYuwpcpqFddyIIKxtdHEX1bVeaKrOJkpFmG56FwoE8p4XTw4jbt3o4uZNJsd6ZyQhSuy7mm-NsXQmwj9rray8C4AwPtM3PX-T0PfkWcVwl_QUrV_IIKKq5EsO1-p83DKKF9NpAbRd_DdPB0QQwEkwxsNfSU4pfQ",
                "expires_in": 86400,
                "token_type": "Bearer"
              }
            }
          ]
        }
      },
      "required": [
        "returnCode",
        "errorList",
        "auth"
      ]
    },
    "LeadSearchRequest": {
      "type": "object",
      "properties": {
        "firstName": {
          "type": "string",
          "example": "Bob",
          "description": "The first name of the lead to search. This needs to be used *with* the lastName field.\nCase insensitive partial matching will be used.\ni.e. Anna will match Anna, Annabelle, and Hannah"
        },
        "lastName": {
          "type": "string",
          "example": "Smith",
          "description": "The last name of the lead to search. This _can_ be used without the firstName field.\nCase insensitive partial matching will be used.\ni.e. Bert will match Robertson, Bertrand, and Bert"
        },
        "phone": {
          "type": "string",
          "format": "8008008000",
          "example": "1234567890",
          "description": "The phone number used to search for the lead. *Exact* matches only"
        },
        "email": {
          "type": "string",
          "format": "email@provider.com",
          "example": "bob.smith@t-mobile.com",
          "description": "The email used to search for the lead.\nMatches are made with case insensitive partial searches.\ni.e. mail will match email@test.com, test@email.com something.whatever@gmail.com, etc."
        }
      }
    },
    "Lead": {
      "type": "object",
      "properties": {
        "sfid": {
          "type": "string",
          "example": "00Q8d6cmJu2neRs"
        },
        "phone": {
          "type": "string",
          "example": "(123)-456-7890",
          "format": "(800)-800-8000"
        },
        "firstname": {
          "type": "string",
          "example": "Bob"
        },
        "lastname": {
          "type": "string",
          "example": "Smith"
        },
        "customer_type__c": {
          "type": "string",
          "example": "Existing Custoemr"
        },
        "email": {
          "type": "string",
          "example": "bob.smith@email.com"
        }
      },
      "required": [
        "sfid",
        "phone",
        "firstname",
        "lastname",
        "customer_type__c",
        "email"
      ]
    },
    "LeadSearchResult": {
      "type": "object",
      "properties": {
        "returnCode": {
          "type": "number",
          "example": 0,
          "default": 0,
          "description": "The return code for the API. Non-Zero codes mean there was some sort of failure and the errorList should be referenced"
        },
        "errorList": {
          "type": "array",
          "example": [],
          "description": "The message of the error that happened. Will only be filled if returnCode is non-zero.",
          "default": [],
          "items": {
            "type": "string"
          }
        },
        "leadList": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Lead"
          }
        }
      },
      "required": [
        "returnCode",
        "errorList",
        "leadList"
      ]
    },
    "LeadUpsertRequest": {
      "type": "object",
      "properties": {
        "email": {
          "type": "string",
          "example": "bob.smith@email.com",
          "format": "email@provider.com",
          "description": "The email of the lead. One of the fields possible to use for updating a lead."
        },
        "storeId": {
          "type": "string",
          "example": 9714,
          "description": "The SAP number of a store to easily relate the customer to a region."
        },
        "updateRecord": {
          "type": "boolean",
          "example": true,
          "description": "Boolean field to determine if a lead should be updated in Salesforce or not."
        },
        "firstName": {
          "type": "string",
          "example": "Bob",
          "description": "The first name of the lead. Used *with* lastName and phone to find a lead to update."
        },
        "lastName": {
          "type": "string",
          "example": "Smith",
          "description": "The last name of the lead. Used *with* firstName nad phone to find a lead to update."
        },
        "phone": {
          "type": "string",
          "example": "1234567890",
          "format": "8008008000",
          "description": "The phone of the lead. Used *with* firstName and lastName to find a lead to update."
        },
        "channel": {
          "type": "string",
          "example": "LiveEngage for liveengage or Home Internet for Home internet",
          "description": "The channel that the lead is coming through."
        },
        "phoneType": {
          "type": "string",
          "example": "Home",
          "description": "The type of phone the lead has."
        },
        "phoneColor": {
          "type": "string",
          "example": "Blue",
          "description": "The color of the phone that the lead has."
        },
        "phoneMake": {
          "type": "string",
          "example": "Samsung",
          "description": "The maker of the phone that the lead has."
        },
        "phoneModel": {
          "type": "string",
          "example": "Galaxy 8",
          "description": "The model of the phone that the lead has."
        },
        "phoneOS": {
          "type": "string",
          "example": "Android",
          "description": "The operating system on the phone that the lead has."
        },
        "timezone": {
          "type": "string",
          "example": "-5 or Mountain",
          "description": "The timezone of the lead. Can be sent in as UTC offset or string name."
        },
        "locale": {
          "type": "string",
          "example": "en_US",
          "description": "The locale of the lead. Defaulted to en_US.",
          "default": "en_US"
        },
        "gender": {
          "type": "string",
          "example": "male",
          "description": "The gender of the lead."
        },
        "notes": {
          "type": "string",
          "example": "Some notes",
          "description": "Any notes about the lead that is being upserted."
        },
        "numberOfLines": {
          "type": "number",
          "example": 4,
          "description": "How many lines the lead has."
        },
        "existingCustomer": {
          "type": "boolean",
          "example": true,
          "description": "A boolean describing if the lead is already a customer or not."
        },
        "leadSource": {
          "type": "string",
          "example": "Chat for Live engage or ICA for Home internet",
          "description": "The leadSource that the lead is coming through."
        },
        "street": {
          "type": "string",
          "description": "The shipping/billing street associated with the Account. This is a full street address.",
          "example": "1234 Example St W Apt W211"
        },
        "city": {
          "type": "string",
          "description": "The shipping/billing city associated with the Account.",
          "example": "Bellevue"
        },
        "state": {
          "type": "string",
          "description": "The shipping/billing state associated with the Account.",
          "example": "Washington"
        },
        "zip": {
          "type": "string",
          "description": "The shipping/billing postal code associated with the Lea.",
          "example": "98009"
        },
        "latitude": {
          "type": "string",
          "description": "The shipping/billing latitude portion of the geolocation associated with the Lead.",
          "example": "40.74726223",
          "format": "[-+]?d{1,2}([.]d+)?"
        },
        "longitude": {
          "type": "string",
          "description": "The shipping/billing longitude portion of the geolocation associated with the Lead.",
          "example": "-121.9929824",
          "format": "[-+]?d{1,3}([.]d+)?"
        },
        "campaignId": {
          "type": "string",
          "description": "The Salesforce Campaign Id relating to the Home Internet service. Not needed for Live Engage"
        },
        "ban": {
          "type": "string",
          "description": "The BAN related to the person account for the Home Internet Service. Not needed for Live Engage"
        },
        "leadId": {
          "type": "string",
          "example": "00QV00000086gFTMAY",
          "description": "SFID for Lead"
        },
        "cbsScore": {
          "type": "string",
          "example": 12345,
          "description": "CBS score for Lead"
        },
        "dealerCode": {
          "type": "string",
          "example": 12345,
          "description": "dealer code"
        }
      },
      "required": [
        "email",
        "storeId",
        "updateRecord",
        "firstName",
        "lastName",
        "phone"
      ]
    },
    "LeadUpsertResult": {
      "type": "object",
      "properties": {
        "returnCode": {
          "type": "number",
          "example": 0,
          "default": 0,
          "description": "The return code for the API. Non-Zero codes mean there was some sort of failure and the errorList should be referenced"
        },
        "errorList": {
          "type": "array",
          "example": [],
          "description": "The message of the error that happened. Will only be filled if returnCode is non-zero.",
          "default": [],
          "items": {
            "type": "string"
          }
        },
        "leadId": {
          "type": "string",
          "example": "00Q8d6cmJu2neRs"
        },
        "autoLeadId": {
          "type": "string",
          "example": "00Q8d6cmJu2neRs"
        }
      },
      "required": [
        "returnCode",
        "errorList",
        "leadId"
      ]
    },
    "Disposition": {
      "type": "object",
      "properties": {
        "dispositionType": {
          "type": "string",
          "description": "The dispostion that was determined from the engagement",
          "enum": [
            "No-Sale",
            "Sale",
            "Follow Up",
            "Care",
            "Misdial/Ghost Call",
            "Referred to Retail",
            "Rejected for Verification",
            "Sales Transfer",
            "No Contact(No Answer)",
            "No Contact(Wrong Number)",
            "Work in Progress",
            "Retail In-Store Appointment"
          ],
          "example": "Follow Up"
        },
        "followupDate": {
          "type": "string",
          "description": "The follow up date if the disposition is \"Follow Up\"",
          "format": "MM/DD/YYYY",
          "pattern": "^d{1,2}/d{1,2}/d{4}$",
          "example": "08/13/2019"
        },
        "followupTime": {
          "type": "string",
          "description": "The follow up time if the disposition is \"Follow Up\"",
          "format": "HH:MM (A|P)M",
          "pattern": "^d{1,2}:d{1,2} (A|P)M$",
          "example": "3:45 PM or 8:15 AM or 11:37 AM"
        },
        "followupPhoneNumber": {
          "type": "string",
          "description": "The follow up phone number if the disposition if \"Follow Up\"",
          "format": "1234567890",
          "pattern": "d{10}",
          "example": "80080080000"
        },
        "followupZipcode": {
          "type": "string",
          "description": "The follow up zip code if the disposition is \"Follow Up\"",
          "format": "12345",
          "pattern": "d{5}",
          "example": "98037"
        }
      },
      "required": [
        "dispositionType"
      ]
    },
    "EngagementRequest": {
      "type": "object",
      "description": "This API is used by multiple parties, so some fields may or may not be used in overlap.\nIf a field does not appear on either list, but is defined in the mode it is used for all teams. The fields are separated as follows.\n\n## Live Engage\n\n* reference\n* details\n* notes\n* orderNumber\n* ntId\n* status\n* statusDetails\n* disposition\n\n## Home Internet\n\n* ispStreet\n* ispCity\n* ispState\n* ispPostalCode\n* icaStatus\n* ispLongitude\n* ispLatitude\n* icaStreet\n* icaCity\n* icaState\n* icaPostalCode\n* icaStatus\n* icaLongitude\n* icaLatitude\n* placementDirection\n* consent\n* campaignId\n* ban",
      "properties": {
        "leadId": {
          "type": "string",
          "minLength": 15,
          "maxLength": 18,
          "example": "00QV00000086gFTMAY",
          "description": "SFID for the person account (Lead or Contact or Person Account)"
        },
        "channel": {
          "type": "string",
          "description": "This field value will be maped whatever send in the request. Example \"LiveEngage\""
        },
        "reference": {
          "type": "string",
          "maxLength": 40,
          "description": "UUID sent from SMPD. Not needed for Home Internet"
        },
        "details": {
          "type": "string",
          "description": "Details about the engagement. Not needed for Home Internet",
          "example": "Here are some details"
        },
        "notes": {
          "type": "string",
          "description": "Notes about the engagement. Not needed for Home Internet",
          "example": "Here are some notes"
        },
        "engagementId": {
          "type": "string",
          "description": "SFID of the engagement object. Used for updating. If null will create new record.",
          "example": "a2iV0000001tTROIA2",
          "maxLength": 18,
          "minLength": 15
        },
        "orderNumber": {
          "type": "string",
          "description": "Order number if there is a sale. Not needed for Home Internet",
          "example": "11111111"
        },
        "ntId": {
          "type": "string",
          "description": "NTID of the representative who helped the customer. Not needed for Home Internet",
          "example": "bsmith12"
        },
        "status": {
          "type": "string",
          "description": "Status of the engagement.\nICA and Retail Outbound will both use this field."
        },
        "statusDetails": {
          "type": "string",
          "description": "Status from SMPD. Not needed for Home Internet"
        },
        "disposition": {
          "title": "Disposition",
          "allOf": [
            {
              "$ref": "#/definitions/Disposition"
            },
            {
              "description": "The disposition of the engagement. Not needed for Home Internet",
              "minProperties": 1,
              "maxProperties": 5,
              "example": {
                "dispositionType": "Follow Up",
                "followupDate": "12/20/2018",
                "followupTime": "1:30 PM",
                "followupPhoneNumber": "8009008000",
                "followupZipcode": "98037"
              }
            }
          ]
        },
        "icaStatus": {
          "type": "string",
          "description": "The status sent from ICA. Not needed for Live Engage"
        },
        "icaStreet": {
          "type": "string",
          "description": "The street provided by ICA that is connected with the Account. Again, this is a full street address.",
          "example": "501 Test Ave"
        },
        "icaCity": {
          "type": "string",
          "description": "The city provided by ICA that is connected with the Account.",
          "example": "Bothell"
        },
        "icaState": {
          "type": "string",
          "description": "The state provided by ICA that is connected with the Account.",
          "example": "Washington"
        },
        "icaPostalCode": {
          "type": "string",
          "description": "The postcal code provided by ICA that is connected with the Account.",
          "example": "98011"
        },
        "icaLatitude": {
          "type": "string",
          "description": "The latitude portion of the geolocation provided by ICA that is connected with the Account.",
          "example": "40.74726223",
          "format": "[-+]?d{1,2}([.]d+)?"
        },
        "icaLongitude": {
          "type": "string",
          "description": "The longitude portion of the geolocation provided by ICA that is connected with the Account.",
          "example": "-121.9929824",
          "format": "[-+]?d{1,3}([.]d+)?"
        },
        "placementDirection": {
          "type": "string",
          "description": "The region in which the device will be placed.",
          "example": "SE",
          "enum": [
            "SE",
            "SW",
            "NW",
            "NE",
            "N",
            "E",
            "W",
            "S"
          ]
        },
        "ispStreet": {
          "type": "string",
          "description": "The street the customer inputs for Home Internet service. Not needed for Live Engage"
        },
        "ispCity": {
          "type": "string",
          "description": "The city the customer inputs for Home Internet service. Not needed for Live Engage"
        },
        "ispState": {
          "type": "string",
          "description": "The state the customer inputs for Home Internet service. Not needed for Live Engage"
        },
        "ispPostalCode": {
          "type": "string",
          "description": "The zip code the customer inputs for Home Internet service. Not needed for Live Engage"
        },
        "ispLatitude": {
          "type": "string",
          "description": "The latitude calculated for Home Internet service. Not needed for Live Engage"
        },
        "ispLongitude": {
          "type": "string",
          "description": "The  longitude calculated inputs for Home Internet service. Not needed for Live Engage"
        },
        "consent": {
          "type": "boolean",
          "default": false,
          "description": "Consent if the customer wants to be contacted about service. Not needed for Live Engage"
        },
        "campaignId": {
          "type": "string",
          "description": "The Salesforce Campaign Id relating to the Home Internet service. Not needed for Live Engage"
        },
        "ban": {
          "type": "string",
          "description": "The BAN related to the person account for the Home Internet Service. Not needed for Live Engage"
        },
        "msisdn": {
          "type": "string",
          "description": "The phone number tied to the engagement for Home Internet. Not needed for Live Engage"
        },
        "cbsScore": {
          "type": "number",
          "description": "The CBS Score needed for Home internet and will get it in ICA request"
        },
        "externalId": {
          "type": "string",
          "description": "The id of the lead in the external system"
        }
      },
      "required": [
        "leadId",
        "reference",
        "disposition",
        "icaStatus",
        "placementDirection",
        "campaignId"
      ]
    },
    "EngagementResult": {
      "type": "object",
      "properties": {
        "returnCode": {
          "type": "number",
          "example": 0,
          "default": 0,
          "description": "The return code for the API. Non-Zero codes mean there was some sort of failure and the errorList should be referenced"
        },
        "errorList": {
          "type": "array",
          "example": [],
          "description": "The message of the error that happened. Will only be filled if returnCode is non-zero.",
          "default": [],
          "items": {
            "type": "string"
          }
        },
        "id": {
          "type": "string",
          "example": "ENGe8afef6e-9f86-4980-b925-5f47e33c9f73"
        }
      },
      "required": [
        "returnCode",
        "errorList",
        "id"
      ]
    },
    "Social": {
      "type": "object",
      "properties": {
        "socialChannel": {
          "type": "string",
          "description": "The social channel of the customer.",
          "enum": [
            "Facebook",
            "Twitter"
          ]
        },
        "socialHandle": {
          "type": "string",
          "maxLength": 255,
          "description": "The handle for the social channel.",
          "format": "/^@?w{1,255}$/"
        },
        "socialConsent": {
          "type": "boolean",
          "description": "THe consent of the customer to contact them via the chosen social channel."
        }
      },
      "required": [
        "socialChannel",
        "socialHandle",
        "socialConsent"
      ]
    },
    "PersonAccountReqDTO": {
      "type": "object",
      "properties": {
        "externalId": {
          "type": "string",
          "description": "The id of the person-account in the external system"
        },
        "accountSource": {
          "type": "string",
          "example": "Home Internet",
          "description": "Where the person-account is coming from"
        },
        "sfdcId": {
          "type": "string",
          "description": "The salesforce id of the person-account, if it already exists.\nUseful for running updates, but not necessary.",
          "minLength": 15,
          "maxLength": 18,
          "example": ""
        },
        "msisdn": {
          "type": "string",
          "description": "The phone number of the person-account. Used for helping find if the person-account already exists",
          "example": "1234567890",
          "format": "8008008000"
        },
        "ban": {
          "type": "string",
          "description": "The ban of the person-account. Will be used to create new BAN object if it does not already exist",
          "example": "123456789",
          "format": "999999999"
        },
        "firstName": {
          "type": "string",
          "description": "The first name of the person-account holder",
          "example": "Bob"
        },
        "lastName": {
          "type": "string",
          "description": "The last name of the person-account holder.",
          "example": "Smith"
        },
        "billingStreet": {
          "type": "string",
          "description": "The shipping/billing street associated with the Account. This is a full street address.",
          "example": "1234 Example St W Apt W211"
        },
        "billingCity": {
          "type": "string",
          "description": "The shipping/billing city associated with the Account.",
          "example": "Bellevue"
        },
        "billingState": {
          "type": "string",
          "description": "The shipping/billing state associated with the Account.",
          "example": "Washington"
        },
        "billingPostalCode": {
          "type": "string",
          "description": "The shipping/billing postal code associated with the Account.",
          "example": "98009"
        },
        "billingLatitude": {
          "type": "string",
          "description": "The shipping/billing latitude portion of the geolocation associated with the Account.",
          "example": "40.74726223",
          "format": "[-+]?d{1,2}([.]d+)?"
        },
        "billingLongitude": {
          "type": "string",
          "description": "The shipping/billing longitude portion of the geolocation associated with the Account.",
          "example": "-121.9929824",
          "format": "[-+]?d{1,3}([.]d+)?"
        },
        "language": {
          "type": "string",
          "description": "The language the customer prefers",
          "example": "en_US"
        },
        "email": {
          "type": "string",
          "description": "The email the account uses.",
          "example": "test@test.email"
        },
        "campaignId": {
          "type": "string",
          "description": "The campaign that the person-account was brought in with. This is a salesforce id.",
          "example": "70122000000DJw9AAG",
          "minLength": 15,
          "maxLength": 18,
          "format": "701xxxxxxxxxxxx"
        },
        "preferredChannel": {
          "type": "string",
          "description": "The preferred method of social contact for the person account. *This is required when working with socialChannels.*",
          "example": "Facebook",
          "enum": [
            "Facebook",
            "Twitter"
          ]
        },
        "socialChannels": {
          "type": "array",
          "description": "The social channels provided by the customer.",
          "items": {
            "$ref": "#/definitions/Social"
          }
        }
      },
      "required": [
        "externalId",
        "accountSource",
        "msisdn",
        "firstName",
        "lastName"
      ]
    },
    "PersonAccountResDTO": {
      "type": "object",
      "properties": {
        "returnCode": {
          "type": "number",
          "example": 0,
          "default": 0,
          "description": "The return code for the API. Non-Zero codes mean there was some sort of failure and the errorList should be referenced"
        },
        "errorList": {
          "type": "array",
          "example": [],
          "description": "The message of the error that happened. Will only be filled if returnCode is non-zero.",
          "default": [],
          "items": {
            "type": "string"
          }
        },
        "sfdcId": {
          "type": "array",
          "example": [
            "id goes here",
            "next id here",
            "another id here"
          ],
          "items": {
            "type": "string"
          }
        }
      },
      "required": [
        "returnCode",
        "errorList",
        "sfdcId"
      ]
    }
  }
}