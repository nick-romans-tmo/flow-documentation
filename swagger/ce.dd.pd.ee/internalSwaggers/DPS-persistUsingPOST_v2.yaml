swagger: '2.0'
info:
  description: 'Release: 1.0.0'
  version: 1.0.0
  title: Decision Persistence Service
  contact:
    name: Promo Platform Dev Ops
    url: https://tmobileusa.sharepoint.com/:w:/r/teams/eitpromo/Shared%20Documents/Amdocs%20Dev%20Ops/02%20DevOps%20Digital%20for%20EFPE%20and%20SPE/DIGITAL%20Team-Branch/technical%20docs/Decision%20Persistance_V4.docx?d=w61328ebdaae348b0a9c7736aa2b1fd02&csf=1
    email: PromoPlatformDevOps@T-Mobile.com
  license:
    name: Proprietary software
    url: https://en.wikipedia.org/wiki/Proprietary_software
  termsOfService: https://www.t-mobile.com/templates/popup.aspx?PAsset=Ftr_Ftr_TermsAndConditions
  x-createdBy:
    dateCreated: Mon Mar 12 14:11:13 2019
    createdBy: shivankp
    application: None
    appVersion: 0.0.0.0
    documentId: c709abc4-af25-4703-856d-820dd7b11035
    status: Production - Inactive
    classification: 1.4 Customer Promotion Presentment
    profile: Core Business Capability Service
    serviceLayer: Resource - Other
x-servers:
  - url: http://decision-persistence-service-dlab02.apps.px-npe01b.cf.t-mobile.com/
    description: Test Server 1
tags:
  - name: Decision persistence service APIs
    description: Decision persistence service APIs for persisting and retreiving promotion decisions
host: decision-persistence-service-dlab02.apps.px-npe01b.cf.t-mobile.com
basePath: /decision-persistence/v1
schemes:
  - https
paths:
  /:
    post:
      tags:
        - Decision persistence service APIs
      x-api-pattern: CreateInCollection
      summary: Persist promotion decisions
      description: Persist promotion decisions
      operationId: persistUsingPOST
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: traceId
          in: header
          description: Trace Id
          required: true
          type: string
          x-example: 123455
          pattern: '^[\S]*$'
        - in: header
          name: Authorization
          description: Authorization header for the request.
          type: string
          required: true
          pattern: Bearer .*$
          x-example: Bearer WoOhe88khLZ3rMVqujG9GGa4PRH2
        - in: body
          name: payload
          description: Payload to persist
          required: true
          schema:
            $ref: '#/definitions/DpsPayload'
          x-example:
            devicePromoEvaluations:
            - customerProductIdentifier:
                ban: 111670685
                previousBan: 111670685
                deviceId: 7000167
                enrollmentType: N
                msisdn: 6125321552
                previousMsisdn: 6125321552
              decisions:
              - controlFields:
                  eventCreatedBy: XYZ
                  eventProcessingDate: '2018-12-28T00:40:00.000-0800'
                decision:
                  reminderNotificationDate: '2018-12-27T14:40:00.000-0800'
                  financeAgreementId: 1315
                  promoEnrollmentDeadlineDate: '2018-12-27T14:40:00.000-0800'
                  benefit:
                    amount: 21
                    description: BAN tenure
                    discountCode: AMC01
                    frequency: Billing Cycle
                    numberOfOccurrences: 1
                  reason:
                    care: Added as valid.
                    system: promo code PC001 was eligible.  
                  fulfillConditionalParams:
                  - criteriaName: SOC
                    eventValues:
                    - attribute1=value2
                    - attribute2=value2
                    match: SUCCESS
                    ruleType: CANDIDATE
                  manualOverride:
                    ind: Y
                    userId: customer-111
                    manualActionChannelId: care
                    maintenanceCheckInd: Y
                  matrix:
                  - criteriaName: SOC
                    eventValues:
                    - attribute1=value2
                    - attribute2=value2
                    match: SUCCESS
                    ruleType: CANDIDATE
                  pendingAction:
                    system:
                    - criteriaName: SOC
                      eventValues:
                      - attribute1=value2
                      - attribute2=value2
                      match: SUCCESS
                      ruleType: CANDIDATE
                  relations:
                  - products:
                    - 23423
                    type: BO
                  showMatrix: false
                  state: ELIGIBLE
                promoDefinition:
                  code: RGST01
                  description: Business 2018 Samsung Note9 and Tab E
                  configId: 23423
                  configVersion: 1
                  subType: STANDARD
                  type: DEVICE_FINANCE
                productAction: MOVE_MSISDN
            lastPromoBanEvalStamp: uuid_value
      responses:
        '200':
          description: OK
          schema:
            type: array
            description: 200 response
            items:
              $ref: '#/definitions/Response'
          examples:
            application/json:
              - action: INSERTED
                id:
                  - 12356
                  - 345
              - action: UPDATED
                id:
                  - 1234
                  - 945
        '201':
          description: OK
          schema:
            type: array
            description: 201 response
            items:
              $ref: '#/definitions/Response'
          examples:
            application/json:
              - action: INSERTED
                id:
                  - 12356
                  - 345
              - action: UPDATED
                id:
                  - 1234
                  - 945
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ApiErrorResponse'
          examples:
            application/json:
              status: '400'
              userMessage: Bad Request
              errors:
                - Bad Request
        '401':
          description: Unauthorized
          schema:
            $ref: '#/definitions/ApiErrorResponse'
          examples:
            application/json:
              status: '401'
              userMessage: Unauthorized
              errors:
                - Unauthorized
        '403':
          description: Client access denied
          schema:
            $ref: '#/definitions/ApiErrorResponse'
          examples:
            application/json:
              status: '403'
              userMessage: Client access denied
              errors:
                - Client access denied
        '404':
          description: Resource not found
          schema:
            $ref: '#/definitions/ApiErrorResponse'
          examples:
            application/json:
              status: '404'
              userMessage: Resource not found
              errors:
                - Resource not found
        '405':
          description: Method Not Allowed
          schema:
            $ref: '#/definitions/ApiErrorResponse'
          examples:
            application/json:
              status: '405'
              userMessage: Method Not Allowed
              errors:
                - Method Not Allowed
        '406':
          description: Mismatching data format
          schema:
            $ref: '#/definitions/ApiErrorResponse'
          examples:
            application/json:
              status: '406'
              userMessage: Mismatching data format
              errors:
                - Mismatching data format
        '409':
          description: Invalid data
          schema:
            $ref: '#/definitions/ApiErrorResponse'
          examples:
            application/json:
              status: '409'
              userMessage: Invalid data
              errors:
                - Invalid data
        '415':
          description: Unsupported Media Type
          schema:
            $ref: '#/definitions/ApiErrorResponse'
          examples:
            application/json:
              status: '415'
              userMessage: Unsupported Media Type
              errors:
                - Unsupported Media Type
        '500':
          description: System Error
          schema:
            $ref: '#/definitions/ApiErrorResponse'
          examples:
            application/json:
              status: '500'
              userMessage: System Error
              errors:
                - System Error
        '503':
          description: Service unavailable
          schema:
            $ref: '#/definitions/ApiErrorResponse'
          examples:
            application/json:
              status: '503'
              userMessage: Service unavailable
              errors:
                - Service unavailable
      security:
        - oauth2:
            - ''
      deprecated: false
definitions:
  Benefit:
    type: object
    properties:
      amount:
        type: number
        example: 21
        description: Benefit amount.
      description:
        type: string
        example: BAN tenure
        description: Benefit description.
        pattern: '^[\S]*$'
      discountCode:
        type: string
        example: AMC01
        description: Discount code.
        pattern: '^[\S]*$'
      frequency:
        type: string
        example: Billing Cycle
        description: Frequency of applied benefit.
        enum:
          - Billing Cycle
      numberOfOccurrences:
        type: integer
        format: int32
        example: 1
        description: Number of occurrences of benefit.
        pattern: '^[\d]*$'
    title: Benefit
    description: Benefit details for a device.
  Reason:
    type: object
    properties:
      care:
        type: string
        example: Added as valid.
        description: Care Reason.
      system:
        type: string
        example: promo code PC001 was eligible.
        description: System Reason.
    title: Reason
    description: Evaluation Reason details for a device.
  ControlFields:
    type: object
    required:
      - eventCreatedBy
      - eventProcessingDate
    properties:
      eventCreatedBy:
        type: string
        example: XYZ
        description: Creator of the event.
        pattern: '^[\S]*$'
      eventProcessingDate:
        type: string
        example: 2018-12-28T00:40:00.000-0800
        description: Creation date of the event.
        format: date-time
    title: ControlFields
    description: Technical properties regarding this record.
  CustomerProductIdentifier:
    type: object
    required:
      - ban
      - deviceId
      - enrollmentType
      - msisdn
    properties:
      ban:
        type: integer
        example: 111670685
        description: Customer ban
        pattern: '^[\d]*$'
      previousBan:
        type: integer
        example: 111670685
        description: Previous customer ban
        pattern: '^[\d]*$'
      deviceId:
        type: string
        minimum: 0
        example: 7000167
        description: Customer device ID or equipment ID
        pattern: '^[\S]*$'
      enrollmentType:
        type: string
        example: N
        description: Enrollment Type. N for new enrollment, R for re-enrollment
        enum:
          - N
          - R
      msisdn:
        type: string
        minimum: 0
        example: 6125321552
        description: Customer msisdn
        pattern: '^[\S]*$'
      previousMsisdn:
        type: string
        example: 6125321552
        description: Previous customer msisdn
        pattern: '^[\S]*$'
    title: CustomerProductIdentifier
    description: Customer Product Identifier
  DpsPayload:
    type: object
    required:
      - devicePromoEvaluations
    properties:
      devicePromoEvaluations:
        type: array
        description: List of evaluated device promotions for a BAN
        items:
          $ref: '#/definitions/DevicePromoEvaluation'
      lastPromoBanEvalStamp:
        type: string
        description: Stamp for BAN locking
        example: uuid_value
        pattern: '^[\S]*$'
    title: DpsPayload
    description: Payload
  Decision:
    type: object
    required:
      - state
    properties:
      reminderNotificationDate:
        type: string
        example: 2018-12-27T14:40:00.000-0800
        description: Reminder notification date
        format: date-time
      financeAgreementId:
        type: integer
        example: 1315
        description: Finance agreement id
      promoEnrollmentDeadlineDate:
        type: string
        example: 2018-12-27T14:40:00.000-0800
        description: Promo Enrollment deadline date
        format: date-time
      benefit:
        $ref: '#/definitions/Benefit'
      reason:
        $ref: '#/definitions/Reason'
      fulfillConditionalParams:
        type: array
        description: Parameters applicable for GOODWILL transactions.
        items:
          $ref: '#/definitions/Matrix'
      manualOverride:
        $ref: '#/definitions/ManualOverride'
      matrix:
        type: array
        description: Evaluated base parameters for device and promo.
        items:
          $ref: '#/definitions/Matrix'
      pendingAction:
        $ref: '#/definitions/PendingAction'
      relations:
        type: array
        description: Related product list for a BOGO promo.
        items:
          $ref: '#/definitions/Relation'
      showMatrix:
        type: boolean
        example: false
        description: Whether to show matrix.
      state:
        type: string
        example: ELIGIBLE
        description: Evaluated decision state.
        enum:
          - NONE
          - IGNORED
          - CANDIDATE
          - EXPIRED
          - EXPECTED
          - ELIGIBLE
          - ENROLLED
          - INELIGIBLE
          - PENDING_REMOVAL
          - UNENROLLED
          - COMPLETED
          - GOODWILL_ELIGIBLE
          - GOODWILL_ENROLLED
          - EXCLUDED
    title: Decision
    description: decision
  DevicePromoEvaluation:
    type: object
    required:
      - customerProductIdentifier
      - decisions
    properties:
      customerProductIdentifier:
        $ref: '#/definitions/CustomerProductIdentifier'
      decisions:
        type: array
        description: Evaluated properties of all promos for a device.
        items:
          $ref: '#/definitions/PromoDecision'
    title: DevicePromoEvaluation
    description: List of evaluated device promotions for a BAN
  ManualOverride:
    type: object
    properties:
      ind:
        type: string
        example: Y
        description: Manual override indicator. Y or N.
        pattern: '^[\S]*$'
      userId:
        type: string
        example: customer-111
        description: Manual override userid.
        pattern: '^[\S]*$'
      manualActionChannelId:
        type: string
        example: care
        description: Manual request channel
        pattern: '^[\S]*$'
      maintenanceCheckInd:
        type: string
        example: Y
        description: Maintenance Check Indicator. Y or N
        pattern: '^[\S]*$'    
    title: ManualOverride
    description: Property for indicating to override a state transition manually.
  Matrix:
    type: object
    properties:
      criteriaName:
        type: string
        example: SOC
        description: Evaluated criteria name.
        pattern: '^[\S]*$'
      eventValues:
        type: array
        example:
          - attribute1=value2
          - attribute2=value2
        description: Evaluated criteria values.
        items:
          type: string
          pattern: '^[\S]*$'
      match:
        type: string
        example: SUCCESS
        description: Evaluated status of the criteria.
        pattern: '^[\S]*$'
      ruleType:
        type: string
        example: CANDIDATE
        description: Criteria type.
        enum:
          - CANDIDATE
          - ENROLLMENT
          - INFLIGHT
          - GDWTRNS
    title: Matrix
    description: Evaluated base parameters for device and promo.
  PendingAction:
    type: object
    properties:
      system:
        type: array
        description: List of evaluated INFLIGHT criterias.
        items:
          $ref: '#/definitions/Matrix'
    title: PendingAction
    description: Parameters applicable for inflight transactions.
  PromoDecision:
    type: object
    required:
      - decision
      - promoDefinition
      - controlFields
    properties:
      controlFields:
        $ref: '#/definitions/ControlFields'
      decision:
        $ref: '#/definitions/Decision'
      promoDefinition:
        $ref: '#/definitions/PromoDefinition'
      productAction:
        type: string
        enum:
          - SYSTEM_RULE_DROP
          - MOVE_MSISDN
          - CHANGE_MSISDN
        example: MOVE_MSISDN
        description: Product action
    title: PromoDecision
    description: Promotion decision.
  PromoDefinition:
    type: object
    required:
      - code
    properties:
      code:
        type: string
        example: RGST01
        description: Promo Code.
        pattern: '^[\S]*$'
      description:
        type: string
        example: Business 2018 Samsung Note9 and Tab E
        description: Promo Description.
      configId:
        type: integer
        format: int32
        example: 23423
        description: Configuration ID.
        pattern: '^[\S]*$'
      configVersion:
        type: integer
        format: int32
        example: 1
        description: Configuration version.
        pattern: '^[\d]*$'
      subType:
        type: string
        example: STANDARD
        description: Promotions sub type.
        enum:
          - STANDARD
          - BOGO
          - TRADEIN
          - BO
      type:
        type: string
        example: DEVICE_FINANCE
        description: Promotion type.
        enum:
          - DEVICE_FINANCE
    title: PromoDefinition
    description: Promotion details.
  Relation:
    type: object
    properties:
      products:
        type: array
        example:
          - 23423
        description: Equipments IDs.
        items:
          type: string
          pattern: '^[\S]*$'
      type:
        type: string
        example: BO
        description: Relation type. In case  TYPE = 'AAL_SUBNO'  need to send maintenance Msisdn as products values. e.g. PRODUCTS = ["123456789"]. If need to delete existing value(s), send TYPE = 'AAL_SUBNO'  and product as empty list. e.g.  PRODUCTS = [] (empty product list) In case of update exising value(s) send TYPE = 'AAL_SUBNO'  and product value list containing updated values. e.g. PRODUCTS = ["234567890"].
        enum:
          - BO
          - AAL_SUBNO
    title: Relation
    description: Related product list for a BOGO promo.
  ApiErrorResponse:
    type: object
    properties:
      status:
        type: string
        example: 404
        description: HTTP status code
        pattern: '^[\S]*$'
      userMessage:
        type: string
        example: url not found
        description: User friendly error message
        pattern: '^[\S]*$'
      errors:
        type: array
        example:
          - Resource not found
        description: Error messages
        items:
          type: string
          pattern: '^[\S]*$'
    title: ApiErrorResponse
    description: Error response for API
  Response:
    type: object
    properties:
      action:
        type: string
        example: INSERTED
        description: Whether record is INSERTED/UPDATED or IGNORED.
        enum:
          - INSERTED
          - UPDATED
          - IGNORED
      id:
        type: array
        example:
          - 1234
          - 12356
        description: Record id.
        items:
          type: integer
          pattern: '^[\d]*$'
    title: Response
    description: Response
securityDefinitions:
  oauth2:
    type: oauth2
    description: oauth2 Authentication
    tokenUrl: 'https://host:env/v1/oauth2/accesstoken?grant_type=client_credentials'
    flow: application
    scopes:
      '': '' 
      

