swagger: '2.0'
info:
  description: 'Release: 1.0.0'
  version: 1.0.0
  title: Decision Persistence Service
  contact:
    name: Promo Platform Dev Ops
    url: https://tmobileusa.sharepoint.com/:w:/r/teams/eitpromo/Shared%20Documents/Amdocs%20Dev%20Ops/02%20DevOps%20Digital%20for%20EFPE%20and%20SPE/DIGITAL%20Team-Branch/technical%20docs/Decision%20Persistance_V4.docx?d=w61328ebdaae348b0a9c7736aa2b1fd02&csf=1
    email: PromoPlatformDevOps@T-Mobile.com
  license:
    name: Proprietary software
    url: https://en.wikipedia.org/wiki/Proprietary_software
  termsOfService: https://www.t-mobile.com/templates/popup.aspx?PAsset=Ftr_Ftr_TermsAndConditions
  x-createdBy:
    dateCreated: Mon Mar 12 14:11:13 2019
    createdBy: shivankp
    application: None
    appVersion: 0.0.0.0
    documentId: c709abc4-af25-4703-856d-820dd7b11035
    status: Production - Inactive
    classification: 1.4 Customer Promotion Presentment
    profile: Core Business Capability Service
    serviceLayer: Resource - Other
x-servers:
  - url: https://decision-persistence-service-dlab.apps.px-npe01b.cf.t-mobile.com/
    description: Test Server 1
tags:
  - name: Decision persistence service APIs
    description: Decision persistence service APIs for persisting and retreiving promotion decisions
host: decision-persistence-service-dlab.apps.px-npe01b.cf.t-mobile.com
basePath: /decision-persistence/v1
schemes:
  - https
paths:
  /{ban}:
    get:
      tags:
        - Decision persistence service APIs
      x-api-pattern: QueryCollection
      summary: Retreive promotion decisions for a BAN
      description: Retreive promotion decisions for a BAN
      operationId: retreiveUsingGET
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: header
          name: Authorization
          description: Authorization header for the request.
          type: string
          required: true
          pattern: Bearer .*$
          x-example: Bearer WoOhe88khLZ3rMVqujG9GGa4PRH2
        - name: ban
          in: path
          description: Customer product BAN
          required: true
          type: integer
          x-example: 123456789
          pattern: '^[\d]*$'
          minimum: 0
          maximum: 999999999
        - name: deviceId
          in: query
          description: Cutomer product device ID
          required: false
          type: string
          x-example: 123456
          pattern: '^[\S]*$'
        - name: fields
          in: query
          description: Fields to return in response
          required: false
          type: string
          pattern: '^[\S]*$'
          x-example: payload.devicePromoEvaluations.decisions.decision.state
        - name: msisdn
          in: query
          description: msisdn
          required: false
          type: string
          x-example: 11111
          pattern: '^[\S]*$'
        - name: promo
          in: query
          description: Promo code
          required: false
          type: string
          x-example: NM01
          pattern: '^[\S]*$'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/DpsPayload'
          examples:
            application/json:
              devicePromoEvaluations:
              - customerProductIdentifier:
                  ban: 111670685
                  deviceId: 7000167
                  enrollmentType: N
                  msisdn: 6125321552
                decisions:
                - decision:
                    reminderNotificationDate: '2018-12-27T14:40:00.000-0800'
                    financeAgreementId: 1315
                    promoEnrollmentDeadlineDate: '2018-12-27T14:40:00.000-0800'
                    benefit:
                      amount: 21
                      description: BAN tenure
                      discountCode: AMC01
                      frequency: Billing Cycle
                      numberOfOccurrences: 1
                    reason:
                      care: Added as valid.
                      system: promo code PC001 was eligible.
                    fulfillConditionalParams:
                      criteriaName: APPLID
                      match: SUCCESS
                      ruleType: ENROLLMENT
                      eventValues:
                      - ATTRIBUTE=VALUE
                    manualOverride:
                      ind: Y
                      userId: customer-111
                      manualActionChannelId: care
                      lastManualEnrollDate: '2018-12-28T00:40:00.000-0800'
                      lastManualUnenrollDate: '2018-12-28T00:40:00.000-0800'
                      maintenanceCheckInd: Y
                    matrix:
                      criteriaName: APPLID
                      match: SUCCESS
                      ruleType: ENROLLMENT
                      eventValues:
                      - ATTRIBUTE=VALUE
                    pendingAction:
                      system:
                      - criteriaName: APPLID
                        match: SUCCESS
                        ruleType: INFLIGHT
                        eventValues:
                        - ATTRIBUTE=VALUE
                    relations:
                    - products:
                      - 23423
                      type: BO
                    showMatrix: false
                    state: ELIGIBLE
                  promoDefinition:
                    code: RGST01
                    description: Business 2018 Samsung Note9 and Tab E
                    configID: 23423
                    type: DEVICE_FINANCE
                    subType: STANDARD
                    configVersion: 1
              lastPromoBanEvalStamp: uuid_value
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ApiErrorResponse'
          examples:
            application/json:
              status: '400'
              userMessage: Bad Request
              errors:
                - Bad Request
        '401':
          description: Unauthorized
          schema:
            $ref: '#/definitions/ApiErrorResponse'
          examples:
            application/json:
              status: '401'
              userMessage: Unauthorized
              errors:
                - Unauthorized
        '404':
          description: Resource not found
          schema:
            $ref: '#/definitions/ApiErrorResponse'
          examples:
            application/json:
              status: '404'
              userMessage: Resource not found
              errors:
                - Resource not found
        '405':
          description: Method Not Allowed
          schema:
            $ref: '#/definitions/ApiErrorResponse'
          examples:
            application/json:
              status: '405'
              userMessage: Method Not Allowed
              errors:
                - Method Not Allowed
        '406':
          description: Mismatching data format
          schema:
            $ref: '#/definitions/ApiErrorResponse'
          examples:
            application/json:
              status: '406'
              userMessage: Mismatching data format
              errors:
                - Mismatching data format
        '500':
          description: System Error
          schema:
            $ref: '#/definitions/ApiErrorResponse'
          examples:
            application/json:
              status: '500'
              userMessage: System Error
              errors:
                - System Error
        '503':
          description: Service unavailable
          schema:
            $ref: '#/definitions/ApiErrorResponse'
          examples:
            application/json:
              status: '503'
              userMessage: Service unavailable
              errors:
                - Service unavailable
      security:
        - oauth2:
            - ''
      deprecated: false
definitions:
  Benefit:
    type: object
    properties:
      amount:
        type: number
        example: 21
        description: Benefit amount.
      description:
        type: string
        example: BAN tenure
        description: Benefit description.
        pattern: '^[\S]*$'
      discountCode:
        type: string
        example: AMC01
        description: Discount code.
        pattern: '^[\S]*$'
      frequency:
        type: string
        example: Billing Cycle
        description: Frequency of applied benefit.
        enum:
          - Billing Cycle
      numberOfOccurrences:
        type: integer
        format: int32
        example: 1
        description: Number of occurrences of benefit.
        pattern: '^[\d]*$'
    title: Benefit
    description: Benefit
  Reason:
    type: object
    properties:
      care:
        type: string
        example: Added as valid.
        description: Care Reason.
      system:
        type: string
        example: promo code PC001 was eligible.
        description: System Reason.
    title: Reason
    description: Evaluation Reason details for a device.
  CustomerProductIdentifier:
    type: object
    required:
      - ban
      - deviceId
      - enrollmentType
      - msisdn
    properties:
      ban:
        type: integer
        example: 111670685
        description: Customer ban
        pattern: '^[\d]*$'
      deviceId:
        type: string
        example: 7000167
        description: Customer device ID or equipment ID
        pattern: '^[\S]*$'
      enrollmentType:
        type: string
        example: N
        description: Enrollment Type. N for new enrollment, R for re-enrollment
        enum:
          - N
          - R
      msisdn:
        type: string
        example: 6125321552
        description: Customer msisdn
        pattern: '^[\S]*$'
    title: CustomerProductIdentifier
    description: Customer Product Identifier.
  DpsPayload:
    type: object
    required:
      - devicePromoEvaluations
    properties:
      devicePromoEvaluations:
        type: array
        description: List of evaluated device promotions for a BAN
        items:
          $ref: '#/definitions/DevicePromoEvaluation'
      lastPromoBanEvalStamp:
        type: string
        example: uuid_value
        pattern: '^[\S]*$'
        description: Last Promo Ban Eval Stamp
    title: DpsPayload
    description: DPS Payload
  Decision:
    type: object
    properties:
      reminderNotificationDate:
        type: string
        example: 2018-12-27T14:40:00.000-0800
        format: date-time
        description: Reminder Notification Date
      financeAgreementId:
        type: integer
        example: 1315
        description: Finance Agreement Id
      promoEnrollmentDeadlineDate:
        type: string
        example: 2018-12-27T14:40:00.000-0800
        format: date-time
        description: Promo Enrollment Deadline Date
      benefit:
        $ref: '#/definitions/Benefit'
      reason:
        $ref: '#/definitions/Reason'
      fulfillConditionalParams:
        type: array
        example:
          criteriaName: APPLID
          match: SUCCESS
          ruleType: ENROLLMENT
          eventValues: 
            - ATTRIBUTE=VALUE
        description: Parameters applicable for GOODWILL transactions.
        items:
          $ref: '#/definitions/Matrix'
      manualOverride:
        $ref: '#/definitions/ManualOverride'
      matrix:
        type: array
        example:
          criteriaName: APPLID
          match: SUCCESS
          ruleType: ENROLLMENT
          eventValues: 
            - ATTRIBUTE=VALUE
        description: Evaluated base parameters for device and promo.
        items:
          $ref: '#/definitions/Matrix'
      pendingAction:
        $ref: '#/definitions/PendingAction'
      relations:
        type: array
        description: Related product list for a BOGO promo.
        items:
          $ref: '#/definitions/Relation'
      showMatrix:
        type: boolean
        example: false
        description: Whether to show matrix.
      state:
        type: string
        example: ELIGIBLE
        description: Evaluated decision state.
        enum:
          - NONE
          - IGNORED
          - CANDIDATE
          - EXPIRED
          - EXPECTED
          - ELIGIBLE
          - ENROLLED
          - INELIGIBLE
          - PENDING_REMOVAL
          - UNENROLLED
          - COMPLETED
          - GOODWILL_ELIGIBLE
          - GOODWILL_ENROLLED
          - EXCLUDED
    title: Decision
    description: The evaluated promotion decision.
  DevicePromoEvaluation:
    type: object
    required:
      - customerProductIdentifier
      - decisions
    properties:
      customerProductIdentifier:
        $ref: '#/definitions/CustomerProductIdentifier'
      decisions:
        type: array
        description: Evaluated properties of all promos for a device.
        items:
          $ref: '#/definitions/PromoDecision'
    title: DevicePromoEvaluation
    description: List of evaluated device promotions for a BAN
  ManualOverride:
    type: object
    properties:
      ind:
        type: string
        example: Y
        description: Manual override indicator. Y or N.
        pattern: '^[\S]*$'
      userId:
        type: string
        example: customer-111
        description: Manual override userid.
        pattern: '^[\S]*$'
      manualActionChannelId:
        type: string
        example: care
        description: Manual request channel
        pattern: '^[\S]*$'
      lastManualEnrollDate:
        type: string
        example: 2018-12-28T00:40:00.000-0800
        description: Last manual enrollment date
        format: date-time
      lastManualUnenrollDate:
        type: string
        example: 2018-12-28T00:40:00.000-0800
        description: Last manual un-enrollment date
        format: date-time
      maintenanceCheckInd:
        type: string
        example: Y
        description: Maintenance Check Indicator. Y or N
        pattern: '^[\S]*$'  
    title: ManualOverride
    description: Property for indicating to override a state transition manually.
  Matrix:
    type: object
    properties:
      criteriaName:
        type: string
        example: SOC
        description: Evaluated criteria name.
        pattern: '^[\S]*$'
      eventValues:
        type: array
        example:
          - CUSTVA
        description: Evaluated criteria values.
        items:
          type: string
          pattern: '^[\S]*$'
      match:
        type: string
        example: SUCCESS
        description: Evaluated status of the criteria.
        pattern: '^[\S]*$'
      ruleType:
        type: string
        example: CANDIDATE
        description: Criteria type.
        enum:
          - CANDIDATE
          - ENROLLMENT
          - INFLIGHT
          - GDWTRNS
    title: Matrix
    description: Matrix
  PendingAction:
    type: object
    properties:
      system:
        type: array
        example:
          - criteriaName: APPLID
            match: SUCCESS
            ruleType: INFLIGHT
            eventValues: 
              - ATTRIBUTE=VALUE
        description: List of evaluated INFLIGHT criterias.
        items:
          $ref: '#/definitions/Matrix'
    title: PendingAction
    description: Parameters applicable for inflight transactions.
  PromoDecision:
    type: object
    required:
      - promoDefinition
    properties:
      decision:
        $ref: '#/definitions/Decision'
      promoDefinition:
        $ref: '#/definitions/PromoDefinition'
    title: PromoDecision
    description: Promo Decision
  PromoDefinition:
    type: object
    required:
      - code
    properties:
      code:
        type: string
        example: RGST01
        description: Promo Code.
        pattern: '^[\S]*$'
      description:
        type: string
        example: Business 2018 Samsung Note9 and Tab E
        description: Promo Description.
      configId:
        type: integer
        format: int32
        example: 23423
        description: Configuration ID.
        pattern: '^[\d]*$'
      configVersion:
        type: integer
        format: int32
        example: 1
        description: Configuration version.
        pattern: '^[\d]*$'
      subType:
        type: string
        example: STANDARD
        description: Promotions sub type.
        enum:
          - STANDARD
          - BOGO
          - TRADEIN
          - BO
      type:
        type: string
        example: DEVICE_FINANCE
        description: Promotion type.
        enum:
          - DEVICE_FINANCE
    title: PromoDefinition
    description: Promotion details.
    example:
      code: RGST01
      description: Business 2018 Samsung Note9 and Tab E
      configID: 23423
      type: DEVICE_FINANCE
      subType: STANDARD
      configVersion: 1
  Relation:
    type: object
    properties:
      products:
        type: array
        example:
          - 23423
        description: Equipments IDs.
        items:
          type: string
      type:
        type: string
        example: BO
        description: Relation type. In case of KEY as AALSUBNO, products values will hold maintenance msisdn value e.g PRODUCTS = ["234567890"]. In case no value product will be empty list. e.g.PRODUCTS = []
        enum:
          - BO
          - AAL_SUBNO
    title: Relation
    description: Relation
  ApiErrorResponse:
    type: object
    properties:
      status:
        type: string
        example: 404
        description: HTTP status code
        pattern: '^[\S]*$'
      userMessage:
        type: string
        example: url not found
        description: User friendly error message
        pattern: '^[\S]*$'
      errors:
        type: array
        example:
          - Resource not found
        description: Error messages
        items:
          type: string
          pattern: '^[\S]*$'
    title: ApiErrorResponse
securityDefinitions:
  oauth2:
    type: oauth2
    description: oauth2 Authentication
    tokenUrl: 'https://host:env/v1/oauth2/accesstoken?grant_type=client_credentials'
    flow: application
    scopes:
      '': '' 
      
