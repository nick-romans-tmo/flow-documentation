{
  "swagger": "2.0",
  "info": {
    "version": "1.0.0",
    "title": "Tfb-Subsidy Catalog",
    "description": "Populates subsidy Devices and RatePlans",
    "termsOfService": "http://swagger.io/terms/",
    "contact": {
      "name": "Apigee/Documentation Support",
      "email": "TFB_DTD_API_DEVOPS@T-Mobile.com"
    },
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    }
  },
  "host": "qlab03.core.op.api.t-mobile.com",
  "basePath": "/b2b-experience/v1/catalog-service-v1",
  "x-servers": [
    {
      "url": "qlab03.core.op.api.t-mobile.com",
      "description": "server path"
    }
  ],
  "securityDefinitions": {
    "OAuth2": {
      "type": "oauth2",
      "description": "oauth2",
      "flow": "accessCode",
      "authorizationUrl": "https://qlab03.core.op.api.t-mobile.com/v1/oauth2/accesstoken?grant_type=client_credentials",
      "tokenUrl": "https://qlab03.core.op.api.t-mobile.com/v1/oauth2/accesstoken?grant_type=client_credentials",
      "scopes": {
        "read": "Grants read access"
      }
    }
  },
  "schemes": [
    "https"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/subsidy/devices": {
      "get": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "x-api-pattern": "ExecuteFunction",
        "description": "lists subsidy Devices",
        "summary": "subsidyDevices",
        "tags": [
          "Misc"
        ],
        "operationId": "devices",
        "deprecated": false,
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "Content-Type",
            "in": "header",
            "required": true,
            "type": "string",
            "description": "application/json;charset=UTF-8",
            "minLength": 0,
            "maxLength": 20,
            "x-example": "application/json;charset=UTF-8"
          },
          {
            "$ref": "#/parameters/Authorization"
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/devicessdef"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/errorResponse"
            }
          }
        }
      }
    },
    "/subsidy/rateplans": {
      "get": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "x-api-pattern": "ExecuteFunction",
        "description": "lists subsidy rateplans",
        "summary": "subsidyplans",
        "tags": [
          "Misc"
        ],
        "operationId": "rateplans",
        "deprecated": false,
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "Content-Type",
            "in": "header",
            "required": true,
            "type": "string",
            "description": "application/json;charset=UTF-8",
            "minLength": 0,
            "maxLength": 20,
            "x-example": "application/json;charset=UTF-8"
          },
          {
            "$ref": "#/parameters/Authorization"
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/rateplanssdef"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/errorResponse"
            }
          }
        }
      }
    }
  },
  "parameters": {
    "Authorization": {
      "name": "Authorization",
      "in": "header",
      "description": "+   The request header must contain a valid access token \n\n+   The access token is environment specific and is generated dynamically.\n\n\n+   The sample Value is *'EGQ94pYxAzk2JRmSoW1GTZifPAJI'*.",
      "minLength": 0,
      "maxLength": 20,
      "required": true,
      "type": "string"
    }
  },
  "definitions": {
    "devicessdef": {
      "type": "object",
      "properties": {
        "devices": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/devicedef"
          }
        }
      },
      "example": {
        "devices": [
          {
            "sku": "176179",
            "description": "iPhone XR",
            "price": "10",
            "effectiveDate": "2019-10-24",
            "eol": "true"
          },
          {
            "sku": "176180",
            "description": "Samsung Galaxy S9",
            "price": "10",
            "effectiveDate": "2019-10-24",
            "eol": "true"
          }
        ]
      }
    },
    "rateplanssdef": {
      "type": "object",
      "properties": {
        "rateplans": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/rateplandef"
          }
        }
      },
      "example": {
        "rateplans": [
          {
            "sku": "176179",
            "description": "Invidual Subsidy monthly-1",
            "price": "10",
            "effectiveDate": "2019-10-24",
            "grandfathered": "true"
          },
          {
            "sku": "176180",
            "description": "Invidual Subsidy monthly-2",
            "price": "10",
            "effectiveDate": "2019-10-24",
            "grandfathered": "true"
          }
        ]
      }
    },
    "errorResponse": {
      "type": "object",
      "description": "Returns error response",
      "properties": {
        "error": {
          "description": "Used to pass error information in a response.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/error"
          }
        }
      }
    },
    "devicedef": {
      "type": "object",
      "description": "Properties of SKU",
      "properties": {
        "sku": {
          "type": "string",
          "description": "SKU or Part Number"
        },
        "description": {
          "type": "string",
          "description": "SKU Name or Description"
        },
        "price": {
          "type": "string",
          "description": "MSRP"
        },
        "effectiveDate": {
          "type": "string",
          "format": "date",
          "description": "Effective from Date to use this SKU - yyyy-mm-dd"
        },
        "eol": {
          "type": "string",
          "description": "End Of Life. The item that can not be sold any more. But, sold item can be processed at a later point of time Possible values: true or false",
          "pattern": "true"
        }
      }
    },
    "rateplandef": {
      "type": "object",
      "description": "Properties of SKU - Rate Plan",
      "properties": {
        "sku": {
          "type": "string",
          "description": "SKU or Part Number of Rate Plan"
        },
        "description": {
          "type": "string",
          "description": "SKU Name or Description"
        },
        "price": {
          "type": "string",
          "description": "Rate Plan Charge"
        },
        "effectiveDate": {
          "type": "string",
          "format": "date",
          "description": "Effective from Date to use this SKU - Date format would be yyyy-mm-dd"
        },
        "grandFathered": {
          "type": "string",
          "description": "Grand Fathered - Rate Plans that are no longer sold",
          "pattern": "true"
        }
      }
    },
    "error": {
      "type": "object",
      "description": "Returns an error",
      "properties": {
        "code": {
          "type": "string",
          "description": "Provides the error code",
          "minLength": 0,
          "maxLength": 20,
          "pattern": "ERR_DATA_NOT_FOUND or SYS_ERR"
        },
        "userMessage": {
          "type": "string",
          "description": "Provides the error message",
          "minLength": 0,
          "maxLength": 20,
          "enum": [
            "Data Not found for the request"
          ]
        }
      }
    }
  },
  "tags": [
    {
      "name": "Misc",
      "description": ""
    }
  ]
}
