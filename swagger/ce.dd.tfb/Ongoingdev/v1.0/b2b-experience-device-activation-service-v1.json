{
  "swagger": "2.0",
  "info": {
    "description": "These APIs are used for Device Activation services.",
    "version": "1.0.0",
    "title": "Device Activation Service v1",
    "contact": {
      "name": "Apigee/Documentation Support",
      "email": "TFB_DTD_API_DEVOPS@T-Mobile.com"
    }
  },
  "host": "tmobileqat-qat01.apigee.net",
  "basePath": "/b2b-experience/v1/device-activation-service",
  "x-servers": [
    {
      "url": "https://tmobileqat-qat01.apigee.net",
      "description": "server path"
    }
  ],
  "tags": [
    {
      "name": "Device Activation Service v1",
      "description": "Device Activation Service v1 APIs"
    }
  ],
  "securityDefinitions": {
    "TAAP": {
      "type": "oauth2",
      "description": "With TAAP, the access token becomes a digitally signed JWT, and includes a PoP token which digitally signs the entire request. The result is the resource server (REST API) that receives a TAAP request which determines whether it has originated from the client who possesses the signing key and whether the client has been authenticated by the Authorization server. This approach is intended to be backwards compatible. That is, a resource server that does not understand the full TAAP JWT PoP method can still treat the TAAP access token as an OAuth bearer token, even though it is a TAAP JWT PoP. The resource server will not benefit from the security and performance improvements of TAAP but should still be able to function",
      "flow": "accessCode",
      "authorizationUrl": "https://tmobileqat-qat01.apigee.net/oauth2/v4/tokens",
      "tokenUrl": "https://tmobileqat-qat01.apigee.net/oauth2/v4/tokens",
      "scopes": {
        "read": "Grants read access"
      }
    }
  },
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/billing-accounts/order-line-activation": {
      "post": {
        "security": [
          {
            "TAAP": [
              "read"
            ]
          }
        ],
        "description": "# Description\nThe purpose of this API is to submit the order for line activation.",
        "operationId": "postOrderLineActivation",
        "x-api-pattern": "ExecuteFunction",
        "summary": "Order Line Activation",
        "parameters": [
          {
            "$ref": "#/parameters/Accept"
          },
          {
            "$ref": "#/parameters/Content-Type"
          },
          {
            "$ref": "#/parameters/authorization"
          },
          {
            "$ref": "#/parameters/B2B-Client"
          },
          {
            "$ref": "#/parameters/B2B-Org"
          },
          {
            "$ref": "#/parameters/x-authorization"
          },
          {
            "$ref": "#/parameters/x-auth-originator"
          },
          {
            "name": "body",
            "required": true,
            "description": "Provide request payload to submit order for activation",
            "in": "body",
            "schema": {
              "$ref": "#/definitions/OrderLineActivationRequest"
            }
          }
        ],
        "tags": [
          "Device Activation Service v1"
        ],
        "responses": {
          "200": {
            "description": "Successful response",
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "whether cache control is defined",
                "x-example": "no-cache"
              }
            },
            "schema": {
              "$ref": "#/definitions/OrderLineActivationResponse"
            },
            "examples": {
              "application/json": {
                "status": "SUCCESS",
                "message": "Submit order activation submitted successfully",
                "accountTransactionReferenceNumber": "800282068"
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponseStatus"
            },
            "examples": {
              "application/json": {
                "code": "TFB_ERR_40XXX",
                "userMessage": "{error text}",
                "systemMessage": "error_message:{technical error text};time_stamp:2019-10-29T12:23:89.212Z;b2b_request_Id:TEST-1485b0d6-9ab3-418f-8199-f2a51cbc5e7;target_app:{target name}"
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponseStatus"
            },
            "examples": {
              "application/json": {
                "code": "TFB_ERR_40XXX",
                "userMessage": "{error text}",
                "systemMessage": "error_message:{technical error text};time_stamp:2019-10-29T12:23:89.212Z;b2b_request_Id:TEST-1485b0d6-9ab3-418f-8199-f2a51cbc5e7;target_app:{target name}"
              }
            }
          },
          "403": {
            "description": "Forbidden",
            "schema": {
              "$ref": "#/definitions/ErrorResponseStatus"
            },
            "examples": {
              "application/json": {
                "code": "TFB_ERR_40XXX",
                "userMessage": "{error text}",
                "systemMessage": "error_message:{technical error text};time_stamp:2019-10-29T12:23:89.212Z;b2b_request_Id:TEST-1485b0d6-9ab3-418f-8199-f2a51cbc5e7;target_app:{target name}"
              }
            }
          },
          "404": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponseStatus"
            },
            "examples": {
              "application/json": {
                "code": "TFB_ERR_40XXX",
                "userMessage": "{error text}",
                "systemMessage": "error_message:{technical error text};time_stamp:2019-10-29T12:23:89.212Z;b2b_request_Id:TEST-1485b0d6-9ab3-418f-8199-f2a51cbc5e7;target_app:{target name}"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponseStatus"
            },
            "examples": {
              "application/json": {
                "code": "TFB_ERR_50XXX",
                "userMessage": "{error text}",
                "systemMessage": "error_message:{technical error text};time_stamp:2019-10-29T12:23:89.212Z;b2b_request_Id:TEST-1485b0d6-9ab3-418f-8199-f2a51cbc5e7;target_app:{target name}"
              }
            }
          },
          "501": {
            "description": "Method Not Allowed",
            "schema": {
              "$ref": "#/definitions/ErrorResponseStatus"
            },
            "examples": {
              "application/json": {
                "code": "TFB_ERR_50XXX",
                "userMessage": "{error text}",
                "systemMessage": "error_message:{technical error text};time_stamp:2019-10-29T12:23:89.212Z;b2b_request_Id:TEST-1485b0d6-9ab3-418f-8199-f2a51cbc5e7;target_app:{target name}"
              }
            }
          },
          "502": {
            "description": "Gateway timeout",
            "schema": {
              "$ref": "#/definitions/ErrorResponseStatus"
            },
            "examples": {
              "application/json": {
                "code": "TFB_ERR_50XXX",
                "userMessage": "{error text}",
                "systemMessage": "error_message:{technical error text};time_stamp:2019-10-29T12:23:89.212Z;b2b_request_Id:TEST-1485b0d6-9ab3-418f-8199-f2a51cbc5e7;target_app:{target name}"
              }
            }
          },
          "503": {
            "description": "Service unavailable",
            "schema": {
              "$ref": "#/definitions/ErrorResponseStatus"
            },
            "examples": {
              "application/json": {
                "code": "TFB_ERR_50XXX",
                "userMessage": "{error text}",
                "systemMessage": "error_message:{technical error text};time_stamp:2019-10-29T12:23:89.212Z;b2b_request_Id:TEST-1485b0d6-9ab3-418f-8199-f2a51cbc5e7;target_app:{target name}"
              }
            }
          }
        }
      }
    }
  },
  "parameters": {
    "Accept": {
      "name": "Accept",
      "in": "header",
      "description": "+   The request header must contain the Accept element.\n\n+   Accept header is used by HTTP clients to tell the server what content types they'll accept\n\n+   The default value is *'application/json'*.",
      "required": false,
      "type": "string",
      "x-example": "application/json",
      "pattern": "^[\\S]*$"
    },
    "Content-Type": {
      "name": "Content-Type",
      "in": "header",
      "description": "+   The request header must contain the Content-Type element.\n\n+   In HTTP requests, API clients must specify the format in which the data is submitted.\n\n+   The default value is *'application/json'*.",
      "required": true,
      "type": "string",
      "x-example": "application/json",
      "pattern": "^[\\S]*$"
    },
    "B2b-Client": {
      "name": "B2b-client",
      "in": "header",
      "description": "+   The request header must contain B2b Client Id.\n\n\n+   All  B2B customers will have a distinct Client Id. ",
      "required": true,
      "type": "string",
      "pattern": "TEST",
      "x-example": "^[\\S]*$"
    },
    "B2B-Org": {
      "name": "B2b-org",
      "in": "header",
      "required": true,
      "description": "+ All B2B customers will have an organization Id, which needs to be passed here. ",
      "type": "string",
      "pattern": "^[a-zA-Z0-9-]*$|^[0-9]$",
      "x-example": "7000000999999911111"
    },
    "B2B-Client": {
      "name": "B2b-client",
      "in": "header",
      "description": "+   The request header must contain B2b Client Id.\n\n\n+   All  B2B customers will have a distinct Client Id. ",
      "required": true,
      "type": "string",
      "pattern": "^[\\S]*$",
      "x-example": "TEST"
    },
    "authorization": {
      "name": "Authorization",
      "in": "header",
      "description": "+   Access Token valid within one App/domain",
      "required": true,
      "type": "string",
      "pattern": "^[\\S]+$",
      "x-example": "Bearer EGQ94pYxAzk2JRmSoW1GTZifPAJI"
    },
    "x-authorization": {
      "name": "x-authorization",
      "in": "header",
      "description": "+ Validity of PoP Token is one Hop and one Message, a Proof of Possession Token (PoP Token) to bind the access token to the rest of the request using a digital signature.",
      "required": true,
      "type": "string",
      "format": "string",
      "x-tmo-sensitive": "private",
      "x-example": "Bearer aodsidj9038j203jd203dj230djaSFAfa3"
    },
    "x-auth-originator": {
      "name": "x-auth-originator",
      "in": "header",
      "description": "+ ID Token is valid across domains/hops and the received token needs to be propagated across.",
      "required": false,
      "type": "string",
      "format": "string",
      "x-tmo-sensitive": "private",
      "x-example": "Bearer aasFAFq3fwsdgsw4GTgw442.sdjbfsdjksg2o34t. dsfbhifh9h2wf92f"
    }
  },
  "definitions": {
    "OrderLineActivationRequest": {
      "type": "object",
      "description": "Provide request payload for submit order activation.",
      "required": [
        "billingAccount",
        "shippingMethod",
        "userId",
        "userFirstName",
        "userLastName",
        "logonId"
      ],
      "properties": {
        "logonId": {
          "type": "string",
          "description": "The submitter logon Id. This parameter is an alphanumeric value.",
          "example": "TEST_PARTNER",
          "pattern": "^[a-zA-Z0-9_]$"
        },
        "billingAccount": {
          "type": "string",
          "description": "The value must be T-Mobile billing account number (BAN) for the line (phone number) you wish to activate. This is a 9 digit value",
          "example": "985612345",
          "pattern": "^[0-9]{9}$"
        },
        "shippingMethod": {
          "type": "string",
          "description": "Valid values for shipping method are overnight, 2-days and standard",
          "example": "overnight",
          "pattern": "^[\\S]+$"
        },
        "userId": {
          "type": "string",
          "description": "Provide the user identifier",
          "example": "RileySmith",
          "pattern": "^[\\S]+$"
        },
        "userFirstName": {
          "type": "string",
          "description": "First name of the user specified in the userId parameter.",
          "example": "Riley",
          "pattern": "^[\\S]+$"
        },
        "userLastName": {
          "type": "string",
          "description": "Last name of the user specified in the userId parameter.",
          "example": "Smith",
          "pattern": "^[\\S]+$"
        },
        "package": {
          "type": "array",
          "description": "The package list with device/accessory/rate plan/service combined",
          "items": {
            "description": "The package list with device/accessory/rate plan/service combined",
            "type": "object",
            "required": [
              "deviceId",
              "ratePlanId",
              "eipFlag",
              "packageQuantity",
              "npa"
            ],
            "properties": {
              "npa": {
                "type": "string",
                "description": "This is the 3 digit area code for the line to be activated.",
                "example": "248",
                "pattern": "^[0-9]{3}$"
              },
              "packageQuantity": {
                "type": "string",
                "description": "The total quantity of the device/accessory/rateplan/service combined in the package",
                "example": "2",
                "pattern": "^[\\S]+$"
              },
              "eipFlag": {
                "type": "boolean",
                "description": "If EIP plan is required or not, please provide false",
                "example": false,
                "pattern": "^(True|False)$"
              },
              "device": {
                "type": "object",
                "description": "The object node with device sku identifier",
                "required": [
                  "sku"
                ],
                "properties": {
                  "sku": {
                    "type": "string",
                    "description": "Unique device Id.",
                    "example": "610214661401",
                    "pattern": "^[\\S]+$"
                  }
                }
              },
              "accessories": {
                "type": "array",
                "description": "The list of accessory identifier. Accessory Id corresponding to the device Id",
                "items": {
                  "description": "The list of accessory identifier. Accessory Id corresponding to the device Id",
                  "type": "object",
                  "properties": {
                    "sku": {
                      "type": "string",
                      "description": "Unique Store Keeping Unit that is assigned to the accessory.",
                      "example": "1234567890",
                      "pattern": "^[\\S]+$"
                    }
                  }
                }
              },
              "ratePlan": {
                "type": "object",
                "description": "The object node with rateplan sku identifier.",
                "required": [
                  "sku"
                ],
                "properties": {
                  "sku": {
                    "type": "string",
                    "description": "Rate plan Id to be chosen.",
                    "example": "SCVUTTBIZ",
                    "pattern": "^[\\S]+$"
                  }
                }
              },
              "services": {
                "type": "array",
                "description": "The list of service identifier. Service Id corresponding to the chosen rate plan",
                "items": {
                  "description": "The list of service identifier. Service Id corresponding to the chosen rate plan",
                  "type": "object",
                  "properties": {
                    "sku": {
                      "type": "string",
                      "description": "Service Id corresponding to the chosen rate plan",
                      "example": "2718238045",
                      "pattern": "^[\\S]+$"
                    }
                  }
                }
              }
            }
          }
        },
        "legalDocuments": {
          "type": "array",
          "description": "Provide legalDocuments",
          "items": {
            "type": "object",
            "description": "Provide items",
            "required": [
              "docId",
              "docType",
              "acceptedTimestamp"
            ],
            "properties": {
              "docId": {
                "type": "string",
                "description": "The identifier for the document. This is a unique number that identifies the agreement document generated by this request. Document IDs may only be used once.",
                "example": "7806",
                "pattern": "^[\\S]+$"
              },
              "docType": {
                "type": "string",
                "description": "The paremeter contains the type of the document generated(For example:BUSINESS_ADVISEMENT, SERVICE_AGREEMENT,TERM_CONDITIONS, AUTOPAY_AGREEMENT etc.)",
                "example": "SERVICE_AGREEMENT",
                "pattern": "^[\\S]+$"
              },
              "acceptedTimestamp": {
                "type": "string",
                "description": "The accepted time stamp for the document generated.This is time and date stamp that identifies when the  agreement document was generated by this request. This parameter has a standard time-date-stamp format.An example time-date-stamp:2019-06-05T18:07:57.472+0000",
                "example": "2019-06-05T18:07:57.472+0000",
                "pattern": "2019-06-05T18:07:57.472+0000"
              }
            }
          }
        },
        "shipToAddress": {
          "type": "object",
          "description": "The object node with shipping address",
          "required": [
            "shipToCompanyName",
            "shipToContactName",
            "address1",
            "city",
            "state",
            "zip"
          ],
          "properties": {
            "shipToCompanyName": {
              "type": "string",
              "description": "The company name to which the package is shipped.",
              "example": "My-Org",
              "pattern": "^[\\S]+$"
            },
            "shipToContactName": {
              "type": "string",
              "description": "The contact name for the package shipped",
              "example": "My-Org-Admin",
              "pattern": "^[\\S]+$"
            },
            "address1": {
              "type": "string",
              "description": "The address line 1 of the shipment address.",
              "example": "15301 NE Turing St",
              "pattern": "^[\\S]+$"
            },
            "city": {
              "type": "string",
              "description": "The city name to which the package is shipped.",
              "example": "Redmond",
              "pattern": "^[\\S]+$"
            },
            "state": {
              "type": "string",
              "description": "The state name to which the package is shipped.",
              "example": "WA",
              "pattern": "^[\\S]+$"
            },
            "zip": {
              "type": "string",
              "description": "The zip code for the shipped package.",
              "example": "98052",
              "pattern": "^[\\S]+$"
            }
          }
        },
        "contactAddresses": {
          "type": "object",
          "description": "The contact address node",
          "required": [
            "firstName",
            "lastName",
            "email",
            "address1",
            "city",
            "state",
            "zip"
          ],
          "properties": {
            "firstName": {
              "type": "string",
              "description": "The first name for the contact address",
              "example": "John",
              "pattern": "^[\\S]+$"
            },
            "lastName": {
              "type": "string",
              "description": "The last name for the contact address",
              "example": "Doe",
              "pattern": "^[\\S]+$"
            },
            "phone": {
              "type": "string",
              "description": "The contact phone number",
              "example": "9876543210",
              "pattern": "^[0-9]{10]$"
            },
            "email": {
              "type": "string",
              "description": "The email address of the customer contact",
              "example": "jhon.doe@company.com",
              "pattern": "^\\S+@\\S+\\.\\S+$"
            },
            "address1": {
              "type": "string",
              "description": "The address line 1 of contact address",
              "example": "15301 NE Turing St",
              "pattern": "^[\\S]+$"
            },
            "city": {
              "type": "string",
              "description": "The city name of the contact address",
              "example": "Redmond",
              "pattern": "^[\\S]+$"
            },
            "state": {
              "type": "string",
              "description": "The state name for the contact address",
              "example": "WA",
              "pattern": "^[\\S]+$"
            },
            "zip": {
              "type": "string",
              "description": "The zip code for the contact address",
              "example": "98052",
              "pattern": "^[\\S]+$"
            }
          }
        },
        "e911Address": {
          "type": "object",
          "description": "The Emergency(E911) address",
          "required": [
            "address1",
            "city",
            "state",
            "zip"
          ],
          "properties": {
            "address1": {
              "type": "string",
              "description": "The address line 1 of Emergency(E911) address",
              "example": "15301 NE Turing St",
              "pattern": "^[\\S]+$'"
            },
            "address2": {
              "type": "string",
              "description": "The address line 2 of Emergency(E911) address",
              "example": "APT 435",
              "pattern": "^[\\S]+$"
            },
            "city": {
              "type": "string",
              "description": "The city name of Emergency(E911) address",
              "example": "Redmond",
              "pattern": "^[\\S]+$"
            },
            "state": {
              "type": "string",
              "description": "The state name for the Emergency(E911) address",
              "example": "WA",
              "pattern": "^[\\S]+$"
            },
            "zip": {
              "type": "string",
              "description": "The zip code for the Emergency(E911) address",
              "example": "98052",
              "pattern": "^[\\S]+$"
            }
          }
        },
        "ppuAddress": {
          "type": "object",
          "description": "The primary place of use address",
          "required": [
            "address1",
            "city",
            "state",
            "zip"
          ],
          "properties": {
            "address1": {
              "type": "string",
              "description": "The address line 1 of PPU address",
              "example": "15301 NE Turing St",
              "pattern": "^[\\S]+$'"
            },
            "city": {
              "type": "string",
              "description": "The city name of PPU address",
              "example": "Redmond",
              "pattern": "^[\\S]+$"
            },
            "state": {
              "type": "string",
              "description": "The state name for the PPU address",
              "example": "WA",
              "pattern": "^[\\S]+$"
            },
            "zip": {
              "type": "string",
              "description": "The zip code for the PPU address",
              "example": "98052",
              "pattern": "^[\\S]+$"
            }
          }
        }
      }
    },
    "OrderLineActivationResponse": {
      "type": "object",
      "description": "Return submit order activation response.",
      "properties": {
        "status": {
          "type": "string",
          "description": "Returns the status of the submit order activation",
          "example": "SUCCESS"
        },
        "message": {
          "type": "string",
          "description": "Returns the message of the submit order activation",
          "example": "Submit order activation submitted successfully"
        },
        "accountTransactionReferenceNumber": {
          "type": "string",
          "description": "Reference number to check back the order status later",
          "example": "800282068"
        }
      }
    },
    "ErrorResponseStatus": {
      "type": "object",
      "description": "Returns ErrorResponse",
      "properties": {
        "code": {
          "type": "string",
          "description": "Returns the code to identify the error. A succinct, domain-specific, human-readable text string to identify the type of error for the given status code",
          "pattern": "^[\\S ]+$"
        },
        "userMessage": {
          "type": "string",
          "description": "Returns the message to identify the error. A human-readable message describing the error.",
          "pattern": "^[\\S ]+$"
        },
        "systemMessage": {
          "type": "string",
          "description": "Text that provides a more detailed technical explanation of the error",
          "pattern": "^[\\S ]+$"
        }
      }
    }
  }
}
