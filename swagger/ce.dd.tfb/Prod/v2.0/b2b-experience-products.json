{
  "swagger": "2.0",
  "info": {
    "version": "2.0.0",
    "title": "B2B Experience Products",
    "description": "These APIs are used for B2B Experience Products.",
    "contact": {
      "name": "Apigee/Documentation Support",
      "email": "TFB_DTD_API_DEVOPS@T-Mobile.com"
    }
  },
  "host": "core.saas.api.t-mobile.com",
  "basePath": "/b2b-experience/v2",
  "x-servers": {
    "url": "https://core.saas.api.t-mobile.com",
    "description": "server path"
  },
  "tags": [
    {
      "name": "Ordering service",
      "description": "Ordering service related APIs"
    },
    {
      "name": "Catalog service",
      "description": "Catalog APIs"
    }
  ],
  "securityDefinitions": {
    "OAuth2": {
      "type": "oauth2",
      "flow": "accessCode",
      "description": "Oauth2 Authorization",
      "authorizationUrl": "https://core.saas.api.t-mobile.com/v1/oauth2/accesstoken?grant_type=client_credentials",
      "tokenUrl": "https://core.saas.api.t-mobile.com/v1/oauth2/accesstoken?grant_type=client_credentials",
      "scopes": {
        "read": "Grants read access"
      }
    }
  },
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/line-upgrade-eligibility-profiles": {
      "get": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "description": "\n# Description \n\n The purpose of this API is to validate the upgrade eligibility for a msisdn or mobile number\n   ***\n\n     * The API is designed to check the eligibility for line upgradation. Customer can check eligibility including financing for Upgrade at discounted price by providing MSISDN or list of delimited MSISDNs\n \n     * Once the field constraints are satisfied, the API will validate the mobile number and if found valid will then return success, otherwise will return appropriate error.\n         \n\n  # Key Highlights \n\n + <b>Allow line to be upgraded after eligibility check is satisfied</b>.\n\n\n  + <b>Validates MSISDN/mobile number before line upgradation..\n\n\n  + <b>Validate upgrade eligibility including financing (EIP) eligibility",
        "operationId": "getEligibilityUpgrade",
        "summary": "B2B Upgrade Eligibility including EIP",
        "x-api-pattern": "QueryCollection",
        "tags": [
          "Ordering service"
        ],
        "parameters": [
          {
            "$ref": "#/parameters/Accept"
          },
          {
            "$ref": "#/parameters/Authorization"
          },
          {
            "$ref": "#/parameters/Cache-Control"
          },
          {
            "$ref": "#/parameters/mobile-numbers"
          },
          {
            "$ref": "#/parameters/jump-check"
          },
          {
            "$ref": "#/parameters/blackberry-check"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful response",
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "whether cache control is defined",
                "x-example": "no-cache"
              }
            },
            "schema": {
              "$ref": "#/definitions/UpgradeEligibilityResponse"
            },
            "examples": {
              "application/json": {
                "subscriberUpgradeEligibilityResponse": {
                  "upgradeEligibility": "CLASSIC",
                  "discountEligible": "False",
                  "eipEligible": "False",
                  "msisdn": "",
                  "ban": "",
                  "socJumpList": "list of socs eligible for Jump upgrade",
                  "socBlackberryList": "list of blackberry data socs eligible for upgrade"
                },
                "status": "SUCCESS",
                "message": "Upgrade Eligiblity Details"
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40010",
                "userMessage": "Bad Request"
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 401,
                "userMessage": "Unauthorized"
              }
            }
          },
          "404": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40401",
                "userMessage": "No data found"
              }
            }
          },
          "405": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40501",
                "userMessage": "Method Not Allowed"
              }
            },
            "headers": {
              "Allow": {
                "type": "string",
                "description": "List of supported methods for URI",
                "x-example": "GET"
              }
            }
          },
          "406": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40601",
                "userMessage": "Not Acceptable"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_50001",
                "userMessage": "Generic Service Error"
              }
            }
          },
          "503": {
            "description": "Service unavailable",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_50301",
                "userMessage": "Service Unavailable"
              }
            }
          }
        }
      }
    },
    "/rate-plans": {
      "get": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "description": "\n# Description \n\n  The purpose of this API is to retrieve the applicable rate plans and its features for a given OrgId.\n***\n\n    API is designed to return the list of rate plans applicable for a given OrgId. ",
        "operationId": "getRateplans",
        "summary": "B2B Fetching Rate Plans",
        "x-api-pattern": "QueryCollection",
        "tags": [
          "Catalog service"
        ],
        "parameters": [
          {
            "$ref": "#/parameters/Accept"
          },
          {
            "$ref": "#/parameters/Content-Type"
          },
          {
            "$ref": "#/parameters/B2b-client"
          },
          {
            "$ref": "#/parameters/B2b-org"
          },
          {
            "$ref": "#/parameters/Authorization"
          },
          {
            "$ref": "#/parameters/Cache-Control"
          },
          {
            "name": "org",
            "in": "query",
            "required": true,
            "description": "+   OrgId is usually a 19 digit number. \n\n+   All B2B customers will have an organization Id, which needs to be passed here. \n+   It is mandatory to have OrgId either in header or in path param/query param.",
            "type": "string",
            "pattern": "7000000000000013000",
            "x-example": "7000000000000013000"
          },
          {
            "name": "classic",
            "in": "query",
            "required": false,
            "description": "+ Provide classic pricing.",
            "type": "string",
            "x-example": "string"
          },
          {
            "name": "billing-account",
            "in": "query",
            "required": false,
            "description": "+ The value must be the base64 encoded billing account number (BAN) for the line (phone number) you wish to activate.  This is a 9 digit value. BAN is mandatory only for partners, for others BAN is optional.",
            "type": "string",
            "pattern": "OTg1NjEyMzQ1",
            "x-example": "OTg1NjEyMzQ1"
          },
          {
            "name": "order",
            "in": "query",
            "required": false,
            "description": "+ Provide Order Id.\n\n\n+ The field value is numeric.",
            "type": "string",
            "pattern": "1234",
            "x-example": "1234"
          },
          {
            "name": "device",
            "in": "query",
            "required": false,
            "description": "+ Provide Device Id. (Optional)\n\n+ The field value is numeric.",
            "type": "string",
            "pattern": "3234",
            "x-example": "3234"
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "whether cache control is defined",
                "x-example": "no-cache"
              }
            },
            "schema": {
              "$ref": "#/definitions/RatePlanResponse"
            },
            "examples": {
              "application/json": {
                "plans": {
                  "isAutoPayEligible": false,
                  "isMBB": "Y",
                  "isPoolingPlan": "N",
                  "itemPartNumber": "BZWCOF2G",
                  "itemUniqueId": 45229,
                  "name": "2GB Enterprise Mobile Broadband (No Overage)",
                  "planCategory": "C",
                  "price": "15.00",
                  "selected": false,
                  "uniqueId": "45226",
                  "isStackable": false,
                  "planAccountType": "BC,IS,GR,GF,BP,BO,BN,BM,BL",
                  "planTaxIndicator": "TE",
                  "planType": "C",
                  "isDuplicateRatePlan": "N",
                  "isVirtualRateplan": "N",
                  "features": [
                    "�Unlimited 2G & 3G data with no overage charges (speeds slow at 2GB)<br><br>�Instant Messages billed as data and comes out of 2GB monthly data bucket<br><br>�SMS available as Pay-Per-Use<br><br>Domestic data on T-Mobile network only (not available in Puerto Rico). All domestic cellular data roaming is blocked (this includes all T-Mobile 2G and 3G Roaming Partner's Networks in the United States)."
                  ],
                  "isAVD": true,
                  "rate_category": "Mobile Broadband"
                }
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40010",
                "userMessage": "Bad Request"
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 401,
                "userMessage": "Unauthorized"
              }
            }
          },
          "404": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40401",
                "userMessage": "No data found"
              }
            }
          },
          "405": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40501",
                "userMessage": "Method Not Allowed"
              }
            },
            "headers": {
              "Allow": {
                "type": "string",
                "description": "List of supported methods for URI",
                "x-example": "GET"
              }
            }
          },
          "406": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40601",
                "userMessage": "Not Acceptable"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_50001",
                "userMessage": "Generic Service Error"
              }
            }
          },
          "503": {
            "description": "Service unavailable",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_50301",
                "userMessage": "Service Unavailable"
              }
            }
          }
        }
      }
    },
    "/services": {
      "get": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "description": "\n# Description \n\n  The purpose of this API is to retrieve the applicable services and its details (SOC, SOCName, MRC, SOCDescription) for a given OrgId, device and rate-plan.\n  ***\n\n      API is designed to return the list of services applicable for a given OrgId, deviceid and rate-plan.\n  ",
        "operationId": "getServices",
        "summary": "B2B Fetching Service Details",
        "x-api-pattern": "QueryCollection",
        "tags": [
          "Catalog service"
        ],
        "parameters": [
          {
            "$ref": "#/parameters/Accept"
          },
          {
            "$ref": "#/parameters/Content-Type"
          },
          {
            "$ref": "#/parameters/B2b-client"
          },
          {
            "$ref": "#/parameters/B2b-org"
          },
          {
            "$ref": "#/parameters/Authorization"
          },
          {
            "$ref": "#/parameters/Cache-Control"
          },
          {
            "name": "org",
            "in": "query",
            "required": true,
            "description": "+   OrgId is usually a 19 digit number. \n\n+   All B2B customers will have an organization Id, which needs to be passed here. \n+   It is mandatory to have OrgId either in header or in path param/query param.",
            "type": "string",
            "pattern": "7000000000000013000",
            "x-example": "7000000000000013000"
          },
          {
            "name": "device",
            "in": "query",
            "required": false,
            "description": "+ Provide Device Id. \n\n+ The field value is numeric.",
            "type": "string",
            "pattern": "3234",
            "x-example": "3234"
          },
          {
            "name": "rateplan",
            "in": "query",
            "required": true,
            "description": "+ Provide Rate Plan Id.",
            "type": "string",
            "pattern": "string",
            "x-example": "string"
          },
          {
            "name": "devicesku",
            "in": "query",
            "required": false,
            "description": "+ Provide device sku Id.",
            "type": "string",
            "x-example": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "whether cache control is defined",
                "x-example": "no-cache"
              }
            },
            "schema": {
              "$ref": "#/definitions/ServicesResponse"
            },
            "examples": {
              "application/json": {
                "endOfResult": true,
                "services": {
                  "displayPrice": "0.00",
                  "features": "Prevents dialing to most international long distance numbers. You will still be able to place calls to destinations in Canada, Mexico, Puerto Rico, and the US Virgin Islands as well as the United States including Alaska and Hawaii.",
                  "items": {
                    "partNumber": "ILDBARSB",
                    "uniqueID": "148654"
                  },
                  "name": "International Long Distance Barring ALL",
                  "partNumber": "ILDBARSBP",
                  "productType": "Service",
                  "rate_category": "International",
                  "uniqueID": "148653",
                  "price": "0.00",
                  "selected": "false",
                  "category": "International"
                }
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40010",
                "userMessage": "Bad Request"
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 401,
                "userMessage": "Unauthorized"
              }
            }
          },
          "404": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40401",
                "userMessage": "No data found"
              }
            }
          },
          "405": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40501",
                "userMessage": "Method Not Allowed"
              }
            },
            "headers": {
              "Allow": {
                "type": "string",
                "description": "List of supported methods for URI",
                "x-example": "GET"
              }
            }
          },
          "406": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40601",
                "userMessage": "Not Acceptable"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_50001",
                "userMessage": "Generic Service Error"
              }
            }
          },
          "503": {
            "description": "Service unavailable",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_50301",
                "userMessage": "Service Unavailable"
              }
            }
          }
        }
      }
    }
  },
  "parameters": {
    "Accept": {
      "name": "Accept",
      "in": "header",
      "description": "+   The request header must contain the Accept element.\n+   Accept header is used by HTTP clients to tell the server what content types they'll accept\n+   The default value is *'application/json'*.",
      "required": true,
      "type": "string",
      "pattern": "application/json",
      "x-example": "application/json"
    },
    "Content-Type": {
      "name": "Content-Type",
      "in": "header",
      "required": true,
      "description": "\n+   The request header must contain the Content-Type element. \n\n\n\n+ In HTTP requests, API clients must specify the format in which the\n       data is submitted.\n\n\n\n+   The default value is *'application/json'*.",
      "type": "string",
      "pattern": "application/json",
      "x-example": "application/json"
    },
    "Authorization": {
      "name": "Authorization",
      "in": "header",
      "description": "+   The request header must contain a valid access token generated by APIGEE.\n+   The access token is environment specific and is generated dynamically.\n+   It is mandatory to be passed in the request parameter for authentication.\n+   The sample Value is *'EGQ94pYxAzk2JRmSoW1GTZifPAJI'*.",
      "required": true,
      "type": "string",
      "pattern": "EGQ94pYxAzk2JRmSoW1GTZifPAJI",
      "x-example": "EGQ94pYxAzk2JRmSoW1GTZifPAJI"
    },
    "B2b-client": {
      "name": "B2b-client",
      "in": "header",
      "required": true,
      "description": "+   The request header must contain B2b Client Id.\n\n+   The sample value is *'TEST'*.",
      "type": "string",
      "pattern": "TEST",
      "x-example": "TEST"
    },
    "B2b-org": {
      "name": "B2b-org",
      "in": "header",
      "required": true,
      "description": "+   The request header contains OrgId.\n\n\n\n+   It is usually a 19 digit number. \n\n\n+   All B2B customers will have an organization Id, which needs to\n       be passed here. \n\n+   It is mandatory to have OrgId either in header or in path\n       param/query param.",
      "type": "string",
      "pattern": "7000000000000013000",
      "x-example": "7000000000000013000"
    },
    "Cache-Control": {
      "name": "Cache-Control",
      "in": "header",
      "description": "+   no-cache",
      "required": false,
      "type": "string",
      "x-example": "no-cache",
      "pattern": "no-cache"
    },
    "mobile-numbers": {
      "name": "mobile-numbers",
      "in": "query",
      "required": true,
      "description": "\n+   The request query parameters must contain a base64 encoded mobile numbers. \n\n+   Mobile numbers should be a base64 encoded valid number and separated by comma",
      "type": "string",
      "pattern": "984512345",
      "x-example": "984512345"
    },
    "jump-check": {
      "name": "jump-check",
      "in": "query",
      "required": false,
      "description": "\n+ The request query parameter determines whether soc is a Jump soc",
      "type": "boolean",
      "pattern": "false",
      "x-example": false
    },
    "blackberry-check": {
      "name": "blackberry-check",
      "in": "query",
      "required": false,
      "description": "\n+ The request query paramter determines whether soc is blackberry simple choice data",
      "type": "boolean",
      "pattern": "false",
      "x-example": false
    }
  },
  "definitions": {
    "ServicesResponse": {
      "type": "object",
      "description": "ServicesResponse",
      "properties": {
        "endOfResult": {
          "type": "boolean",
          "description": "Returns service end results"
        },
        "services": {
          "type": "array",
          "description": "Return list of services",
          "items": {
            "type": "object",
            "description": "items",
            "properties": {
              "displayPrice": {
                "type": "string",
                "description": "Returns displayPrice of service"
              },
              "features": {
                "type": "array",
                "description": "Return features",
                "items": {
                  "type": "object",
                  "description": "Prevents dialing to most international long distance numbers. You will still be able to place calls to destinations in Canada, Mexico, Puerto Rico, and the US Virgin Islands as well as the United States including Alaska and Hawaii."
                }
              },
              "items": {
                "type": "array",
                "description": "items",
                "items": {
                  "type": "object",
                  "description": "items",
                  "properties": {
                    "partNumber": {
                      "type": "string",
                      "description": "Returns partNumber of service"
                    },
                    "uniqueId": {
                      "type": "string",
                      "description": "Returns uniqueID of service"
                    }
                  }
                }
              },
              "name": {
                "type": "string",
                "description": "Returns name of service (sample - Enhanced VoiceMail)"
              },
              "partNumber": {
                "type": "string",
                "description": "Returns partNumber of service"
              },
              "productType": {
                "type": "string",
                "description": "Returns service product type (sample - Basic Service)"
              },
              "rate_category": {
                "type": "string",
                "description": "Returns service rate category"
              },
              "uniqueId": {
                "type": "string",
                "description": "Returns service uniqueID"
              },
              "price": {
                "type": "string",
                "description": "Returns service price"
              },
              "selected": {
                "type": "string",
                "description": "Returns service selected"
              },
              "category": {
                "type": "string",
                "description": "Return the category. (For example:Blocking, Value-Add etc.\n      )"
              }
            }
          }
        }
      }
    },
    "RatePlanResponse": {
      "type": "object",
      "description": "Returns rate plan response",
      "properties": {
        "plans": {
          "type": "array",
          "description": "Returns list of plans",
          "items": {
            "type": "object",
            "description": "Returns plan details",
            "properties": {
              "isAutoPayEligible": {
                "type": "boolean",
                "description": "Returns true or false Auto Pay Eligible"
              },
              "isMbb": {
                "type": "string",
                "description": "Returns true or false for Mobile Brod Brand"
              },
              "isPoolingPlan": {
                "type": "string",
                "description": "Returns true or false for Pooling Plan"
              },
              "itemPartNumber": {
                "type": "string",
                "description": "Returns item part Number"
              },
              "itemUniqueId": {
                "type": "string",
                "description": "Returns rate plan item UniqueId"
              },
              "name": {
                "type": "string",
                "description": "Returns rate plan list "
              },
              "planCategory": {
                "type": "string",
                "description": "Returns plan Category for rate plan"
              },
              "price": {
                "type": "string",
                "description": "Returns price for rate plan"
              },
              "selected": {
                "type": "string",
                "description": "Returns selected  rate plan"
              },
              "uniqueId": {
                "type": "string",
                "description": "Returns uniqueId for rate plan"
              },
              "isStackable": {
                "type": "string",
                "description": "Returns whether the to stack"
              },
              "planAccountType": {
                "type": "string",
                "description": "Returns plan account type code"
              },
              "planTaxIndicator": {
                "type": "string",
                "description": "Returns the plan tax indicator"
              },
              "planType": {
                "type": "string",
                "description": "Returns rate plan Type"
              },
              "isDuplicateRatePlan": {
                "type": "boolean",
                "description": "Returns true or false for DuplicateRatePlan"
              },
              "isVirtualRateplan": {
                "type": "boolean",
                "description": "Returns true or false for Virtual Rate plan"
              },
              "features": {
                "type": "array",
                "description": "Returns list of features",
                "items": {
                  "type": "string",
                  "description": "Returns device Category name"
                }
              },
              "isAvd": {
                "type": "boolean",
                "description": "Returns true or false for Anuval volume disscount"
              },
              "rate_category": {
                "type": "string",
                "description": "Returns rate_category"
              }
            }
          }
        }
      }
    },
    "ErrorResponse": {
      "type": "object",
      "description": "error response",
      "properties": {
        "code": {
          "type": "string",
          "description": "Provides the error code",
          "x-example": "ATWRK_ORDSVC_ERR_50205"
        },
        "userMessage": {
          "type": "string",
          "description": "Provides the error message",
          "x-example": "Cannot process request . Re-try required"
        }
      }
    },
    "UpgradeEligibilityResponse": {
      "type": "object",
      "description": "Upgrade eligibility response",
      "properties": {
        "subscriberUpgradeEligibilityResponse": {
          "type": "array",
          "description": "Returns list of upgrade eligibility details",
          "items": {
            "type": "object",
            "description": "Returns upgrade eligibility details",
            "properties": {
              "upgradeEligibility": {
                "type": "string",
                "description": "+ Returns upgradeEligibility.\n\n\n+ The sample value is *'CLASSIC'*."
              },
              "discountEligible": {
                "type": "string",
                "description": "+ Returns discountEligible.\n\n\n+ The value can be either *'True'* or *'False'*."
              },
              "eipEligible": {
                "type": "string",
                "description": "+ Returns eipEligible.\n\n\n+ The value can be either *'True'* or *'False'*."
              },
              "msisdn": {
                "type": "string",
                "description": "+ Returns subscriber's 10 digits phone Number\n+   Mobile number should be a base64 encoded valid number. "
              },
              "ban": {
                "type": "string",
                "description": "+ Returns 9 Digits Billing Account Number is used to locate customer data.\n\n\n\n + Billing-account is a base64 enoded ban Id.\n\n\n\n  + BAN is unique for each customer."
              },
              "socJumpList": {
                "type": "array",
                "description": "+ Returns list of socs that are eligible for Jump upgrade",
                "items": {
                  "type": "string",
                  "description": "+ Returns list of socs that are eligible for Jump upgrade"
                }
              },
              "socBlackberryList": {
                "type": "array",
                "description": "+ Returns list of blackberry data socs that are eligible for upgrade",
                "items": {
                  "type": "string",
                  "description": "+ Returns list of blackberry data socs that are eligible for upgrade"
                }
              }
            }
          }
        },
        "status": {
          "type": "string",
          "pattern": "SUCCESS",
          "description": "+ Returns status. (sample value - *'SUCCESS\"*)"
        },
        "message": {
          "type": "string",
          "pattern": "Upgrade Eligiblity Details",
          "description": "+ Returns message.\n\n\n+ The field value is *'Upgrade Eligiblity Details'*."
        }
      }
    }
  }
}
