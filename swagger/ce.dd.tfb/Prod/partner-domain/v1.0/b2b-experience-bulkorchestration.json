{
  "swagger": "2.0",
  "info": {
    "version": "0.0.1",
    "title": "B2B Bulk Orchestration service",
    "description": "These APIs are used for B2B Experience bulk orchestration services",
    "contact": {
      "name": "Apigee/Documentation Support",
      "email": "TFB_DTD_API_DEVOPS@T-Mobile.com"
    }
  },
  "host": "core.saas.api.t-mobile.com",
  "basePath": "/b2b-experience/v1/bulk",
  "x-servers": [
    {
      "url": "https://core.saas.api.t-mobile.com",
      "description": "server path"
    }
  ],
  "securityDefinitions": {
    "OAuth2": {
      "type": "oauth2",
      "flow": "accessCode",
      "description": "Oauth2 Authorization",
      "authorizationUrl": "https://core.saas.api.t-mobile.com/v1/oauth2/accesstoken?grant_type=client_credentials",
      "tokenUrl": "https://core.saas.api.t-mobile.com/v1/oauth2/accesstoken?grant_type=client_credentials",
      "scopes": {
        "read": "Grants read access"
      }
    }
  },
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/bulkprocessor": {
      "post": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "description": "# Description \n\n  This API is designed for bulk processing of various services including Restore,Change MSISDN, Suspension,SIM Swap etc. It provides the capability to process the requests for the required services in bulk ",
        "operationId": "postBulkProcess",
        "summary": "B2B Bulk Processor",
        "x-api-pattern": "CreateInCollection",
        "tags": [
          "Bulk Orchestration service"
        ],
        "parameters": [
          {
            "name": "Accept",
            "in": "header",
            "required": false,
            "description": "+   The request header must contain the Accept element. \n\n\n+ Accept header is used by HTTP clients to tell the server what content types they'll accept. \n\n\n+   The default value is *'application/json'*.",
            "type": "string",
            "pattern": "application/json",
            "x-example": "application/json"
          },
          {
            "name": "Content-Type",
            "in": "header",
            "required": true,
            "description": "\n+   The request header must contain the Content-Type element. \n\n\n\n + In HTTP requests, API clients must specify the format in which the\n data is submitted.\n\n\n\n +   The default value is *'application/json'*.",
            "type": "string",
            "pattern": "application/json",
            "x-example": "application/json"
          },
          {
            "name": "Authorization",
            "in": "header",
            "required": true,
            "description": "\n+   The request header must contain a valid access token. \n\n\n+   The access token is environment specific and is generated dynamically. \n\n\n+   It is mandatory to be passed in the request parameter for authentication.",
            "type": "string",
            "pattern": "EGQ94pYxAzk2JRmSoW1GTZifPAJI",
            "x-example": "EGQ94pYxAzk2JRmSoW1GTZifPAJI"
          },
          {
            "name": "B2b-client",
            "in": "header",
            "required": true,
            "description": "+   The request header must contain B2b Client Id.\n\n+   The sample value is *'TEST'*.",
            "type": "string",
            "pattern": "TEST",
            "x-example": "TEST"
          },
          {
            "name": "B2b-org",
            "in": "header",
            "required": true,
            "description": "+   The request header contains OrgId.\n\n\n+   It is usually a 19 digit number. \n\n+   All B2B customers will have an organization Id, which needs to be passed here. \n+   It is mandatory to have OrgId either in header or in path param/query param.",
            "type": "string",
            "pattern": "7000000000000013000",
            "x-example": "7000000000000013000"
          },
          {
            "name": "order-id",
            "description": "+ Provides order Id instead of bulk token to process the change request",
            "in": "query",
            "type": "string",
            "pattern": "1234",
            "x-example": 1234
          },
          {
            "name": "body",
            "description": "Process bulk orchestration",
            "in": "body",
            "schema": {
              "$ref": "#/definitions/BulkProcess"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/BulkProcessResponse"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 400,
                "userMessage": "Input validation failed"
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 401,
                "userMessage": "Invalid client or org"
              }
            }
          },
          "404": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 404,
                "userMessage": "Not found"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 500,
                "userMessage": "Internal Server Error"
              }
            }
          },
          "503": {
            "description": "Service unavailable",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 503,
                "userMessage": "Service Unavailable"
              }
            }
          },
          "504": {
            "description": "Gateway timeout",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 504,
                "userMessage": "Backend Service Timeout:System Name"
              }
            }
          }
        }
      }
    }
  },
  "definitions": {
    "BulkProcess": {
      "type": "object",
      "description": "Bulk process",
      "properties": {
        "targetServiceName": {
          "type": "string",
          "description": "+ Provides target service name that need to be execute  like MSISDN suspension, MSISDN, restore etc.",
          "example": "sim-swap",
          "pattern": "sim-swap"
        },
        "requests": {
          "type": "array",
          "description": "requests",
          "items": {
            "type": "object",
            "description": "requests",
            "properties": {
              "orgId": {
                "type": "integer",
                "minimum": 1,
                "maximum": 2048,
                "pattern": "1234567891234567891",
                "example": "1234567891234567891",
                "description": "Provide the organization Id"
              },
              "logonId": {
                "type": "string",
                "pattern": "test01",
                "example": "test01",
                "description": "Provide the logon Id"
              },
              "email": {
                "type": "string",
                "pattern": "test@t-mobile.com",
                "example": "test@t-mobile.com",
                "description": "Provide the email"
              },
              "newSim": {
                "type": "integer",
                "minimum": 1,
                "maximum": 2048,
                "pattern": "8901240109020050000",
                "example": "8901240109020050779",
                "description": "Provide the new sim number"
              },
              "reasonCode": {
                "type": "string",
                "pattern": "CR",
                "example": "CR",
                "description": "Provide the reason code. The sample Value i 'CR'"
              },
              "msisdn": {
                "type": "integer",
                "minimum": 1,
                "maximum": 2048,
                "pattern": "4252198948",
                "example": "4252198948",
                "description": "Provide the msisdn"
              }
            },
            "required": [
              "orgId",
              "logonId",
              "email",
              "newSim",
              "reasonCode",
              "msisdn"
            ]
          }
        }
      }
    },
    "BulkProcessResponse": {
      "type": "object",
      "description": "Bulk process response",
      "properties": {
        "message": {
          "type": "string",
          "description": "+ Return the bulk process generic response message.\n\n\n+ The sample value is *'New Request accepted'*.",
          "pattern": "New Request accepted:1234",
          "example": "New Request accepted:1234"
        },
        "status": {
          "type": "string",
          "description": "+ Returns success or failure status or BulkprocessResponse.",
          "pattern": "SUCCESS",
          "example": "SUCCESS"
        },
        "token": {
          "type": "string",
          "description": "+ Returns a token for bulk process of target service.",
          "pattern": "9999",
          "example": "9999"
        },
        "orderId": {
          "type": "string",
          "description": "+ Returns order Id",
          "pattern": "1234",
          "example": 1234
        }
      }
    },
    "ErrorResponse": {
      "type": "object",
      "description": "error response",
      "properties": {
        "error": {
          "description": "Returns error message for failure of invoked service ",
          "type": "array",
          "items": {
            "$ref": "#/definitions/Error"
          }
        }
      }
    },
    "Error": {
      "type": "object",
      "description": "error",
      "properties": {
        "code": {
          "type": "string",
          "description": "Provides the Error code"
        },
        "userMessage": {
          "type": "string",
          "description": "Provides the user error message"
        }
      }
    }
  }
}
