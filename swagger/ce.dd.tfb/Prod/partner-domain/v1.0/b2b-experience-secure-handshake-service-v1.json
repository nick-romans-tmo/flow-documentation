{
  "swagger": "2.0",
  "info": {
    "version": "1.0.0",
    "title": "B2B Experience Secure Handshake v1",
    "description": "These APIs will be used for secure handshake for subsequent calls",
    "contact": {
      "name": "Apigee/Documentation Support",
      "email": "TFB_DTD_API_DEVOPS@T-Mobile.com"
    }
  },
  "host": "core.saas.api.t-mobile.com",
  "basePath": "/b2b-experience/v1/secure-handshake-service-v1",
  "x-servers": [
    {
      "url": "https://core.saas.api.t-mobile.com",
      "description": "server path"
    }
  ],
  "tags": [
    {
      "name": "Secure Handshake service v1",
      "description": "Secure Handshake service v1 APIs"
    }
  ],
  "securityDefinitions": {
    "OAuth2": {
      "type": "oauth2",
      "flow": "accessCode",
      "description": "Oauth2 Authorization",
      "authorizationUrl": "https://core.saas.api.t-mobile.com/v1/oauth2/accesstoken?grant_type=client_credentials",
      "tokenUrl": "https://core.saas.api.t-mobile.com/v1/oauth2/accesstoken?grant_type=client_credentials",
      "scopes": {
        "read": "Grants read access"
      }
    }
  },
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/jwks/key": {
      "get": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "description": "\n# Description \n\n  The purpose of this API is to get public key for payload encryption.",
        "operationId": "getEncryptionPublicKey",
        "summary": "B2B Fetching Encryption public key",
        "x-api-pattern": "QueryCollection",
        "tags": [
          "Secure Handshake service v1"
        ],
        "parameters": [
          {
            "name": "Accept",
            "in": "header",
            "required": false,
            "description": "+   The request header must contain the Accept element. \n\n\n+ Accept header is used by HTTP clients to tell the server what content types they'll accept. \n\n\n+   The default value is *'application/json'*.",
            "type": "string",
            "pattern": "application/json",
            "x-example": "application/json"
          },
          {
            "name": "Content-Type",
            "in": "header",
            "required": true,
            "description": "\n+   The request header must contain the Content-Type element. \n\n\n\n + In HTTP requests, API clients must specify the format in which the\n data is submitted.\n\n\n\n +   The default value is *'application/json'*.",
            "type": "string",
            "pattern": "application/json",
            "x-example": "application/json"
          },
          {
            "name": "Authorization",
            "in": "header",
            "required": true,
            "description": "\n+   The request header must contain a valid access token. \n\n\n+   The access token is environment specific and is generated dynamically. \n\n\n+   It is mandatory to be passed in the request parameter for authentication.",
            "type": "string",
            "pattern": "EGQ94pYxAzk2JRmSoW1GTZifPAJI",
            "x-example": "EGQ94pYxAzk2JRmSoW1GTZifPAJI"
          },
          {
            "name": "B2b-client",
            "in": "header",
            "required": true,
            "description": "+   The request header must contain B2b Client Id.\n\n+   The sample value is *'TEST'*.",
            "type": "string",
            "pattern": "TEST",
            "x-example": "TEST"
          },
          {
            "name": "B2b-org",
            "in": "header",
            "required": false,
            "description": "+   The request header B2b Org Id.\n\n+   The sample value is 19 chars '0000000000000000TSB'",
            "type": "string",
            "pattern": "0000000000000000TSB",
            "x-example": "0000000000000000TSB"
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              },
              "B2b-requestId": {
                "type": "string",
                "description": "B2b-requestId-84b2fab6-7656-47f9-beea-f0dc02c8a956",
                "x-example": "B2b-requestId-84b2fab6-7656-47f9-beea-f0dc02c8a956"
              },
              "b2b-client": {
                "type": "string",
                "description": "B2b-requestId",
                "x-example": "B2b-requestId"
              }
            },
            "schema": {
              "$ref": "#/definitions/KeyResponse"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            }
          },
          "403": {
            "description": "Forbidden",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            }
          },
          "404": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            }
          },
          "501": {
            "description": "Method Not Allowed",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            }
          },
          "502": {
            "description": "Gateway timeout",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            }
          },
          "503": {
            "description": "Service unavailable",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            }
          }
        }
      }
    }
  },
  "parameters": {
    "Accept": {
      "name": "Accept",
      "in": "header",
      "description": "+   The request header must contain the Accept element.\n+   Accept header is used by HTTP clients to tell the server what content types they'll accept\n+   The default value is *'application/json'*.",
      "required": false,
      "type": "string",
      "pattern": "application/json",
      "x-example": "application/json"
    },
    "Content-Type": {
      "name": "Content-Type",
      "in": "header",
      "description": "+   The request header must contain the Content-Type element.\n+   In HTTP requests, API clients must specify the format in which the data is submitted.\n+   The default value is *'application/json'*.",
      "required": true,
      "type": "string",
      "pattern": "application/json",
      "x-example": "application/json"
    },
    "Authorization": {
      "name": "Authorization",
      "in": "header",
      "description": "+   The request header must contain a valid access token generated by APIGEE.\n+   The access token is environment specific and is generated dynamically.\n+   It is mandatory to be passed in the request parameter for authentication.\n+   The sample Value is *'EGQ94pYxAzk2JRmSoW1GTZifPAJI'*.",
      "required": true,
      "type": "string",
      "pattern": "EGQ94pYxAzk2JRmSoW1GTZifPAJI",
      "x-example": "EGQ94pYxAzk2JRmSoW1GTZifPAJI"
    },
    "B2b-Client": {
      "name": "B2b-Client",
      "in": "header",
      "description": "+   The request header must contain B2b Client Id.\n\n+   All  B2B customers will have a distinct Client Id. For testing purposes use 'TEST'",
      "required": true,
      "type": "string",
      "pattern": "TEST",
      "x-example": "TEST"
    }
  },
  "definitions": {
    "Key": {
      "type": "object",
      "description": "Key object",
      "properties": {
        "kty": {
          "type": "string",
          "description": "Algorithm RSA"
        },
        "kid": {
          "type": "string",
          "description": "Unique id for the key"
        },
        "use": {
          "type": "string",
          "description": "Use for encryption"
        },
        "n": {
          "type": "string",
          "description": "JWE key N"
        },
        "e": {
          "type": "string",
          "description": "JWE key E"
        }
      }
    },
    "KeyResponse": {
      "type": "object",
      "description": "Key response body",
      "properties": {
        "keys": {
          "description": "Public key information in a response.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/Key"
          }
        }
      }
    },
    "ErrorResponse": {
      "type": "object",
      "description": "Returns error response",
      "properties": {
        "error": {
          "description": "Used to pass error information in a response.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/Error"
          }
        }
      }
    },
    "Error": {
      "type": "object",
      "description": "Returns an error",
      "properties": {
        "code": {
          "type": "string",
          "description": "Provides the error code"
        },
        "userMessage": {
          "type": "string",
          "description": "Provides the error message"
        }
      }
    }
  }
}
