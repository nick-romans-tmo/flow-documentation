{
  "swagger": "2.0",
  "info": {
    "description": "These APIs are used for Billing Account services",
    "version": "2.0.0",
    "title": "B2B Billing Accounts service",
    "contact": {
      "name": "Apigee/Documentation Support",
      "email": "TFB_DTD_API_DEVOPS@T-Mobile.com"
    }
  },
  "host": "core.saas.api.t-mobile.com",
  "basePath": "/b2b-experience/v2/billing-accounts",
  "x-servers": [
    {
      "url": "https://core.saas.api.t-mobile.com",
      "description": "server path"
    }
  ],
  "tags": [
    {
      "name": "Account Billing Service",
      "description": "Account billing APIs"
    }
  ],
  "securityDefinitions": {
    "OAuth2": {
      "type": "oauth2",
      "flow": "accessCode",
      "description": "Oauth2 Authorization",
      "authorizationUrl": "https://core.saas.api.t-mobile.com/v1/oauth2/accesstoken?grant_type=client_credentials",
      "tokenUrl": "https://core.saas.api.t-mobile.com/v1/oauth2/accesstoken?grant_type=client_credentials",
      "scopes": {
        "read": "Grants read access"
      }
    }
  },
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/{billing-account}/bill-summary": {
      "get": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "description": "# Description \n\n  The purpose of this API is to retrieve the bill summary details at BAN level or for the lines associated with BAN at Subscriber level.",
        "operationId": "getbillSummary",
        "x-api-pattern": "QueryCollection",
        "summary": "Bill Summary",
        "parameters": [
          {
            "$ref": "#/parameters/Accept"
          },
          {
            "$ref": "#/parameters/Content-Type"
          },
          {
            "$ref": "#/parameters/Authorization"
          },
          {
            "$ref": "#/parameters/billing-account"
          },
          {
            "$ref": "#/parameters/Cache-Control"
          },
          {
            "$ref": "#/parameters/B2b-Client"
          },
          {
            "$ref": "#/parameters/B2B-Org"
          },
          {
            "name": "bill-sequence-number",
            "in": "query",
            "required": false,
            "type": "string",
            "description": "+ To see billed usage, provide a bill sequence number which can be obtained using the Bill Sequence API.\n\n +  To see unbilled usage, do not pass the bill sequence number.",
            "x-example": 12341234,
            "pattern": "12341234"
          },
          {
            "name": "filter-category-level",
            "in": "query",
            "required": true,
            "type": "string",
            "description": "Filter category levels can have multiple filter values separated by ','. DEPOSITS,OVERAGE_AND_ROAMING,ONE_TIME_CHARGES,ALL_CHARGE_CATEGORIES",
            "x-example": "DEPOSITS",
            "pattern": "DEPOSITS"
          },
          {
            "name": "summary-level",
            "in": "query",
            "required": false,
            "type": "string",
            "description": "Expected values are BAN and SUBSCRIBER. BAN will be set as default value internally if summary level is not passed.",
            "x-example": "BAN",
            "pattern": "BAN"
          },
          {
            "name": "page-size",
            "in": "query",
            "required": false,
            "type": "string",
            "description": "+  Provide the number of subscribers you wish to obtain in the request.\n\n + This parameter is used in conjunction with `page-number`, to determine which subscribers are returned for each call.\n\n + If your BAN has 40 subscribers and you wish to see the subscribers 10 at a time, the `page-size`, will be 10.  So to see subscribers 11-20, use `page-size`, 10 and `page-number`, 2.\n\n + If no value is passed, the default value is 100. \n\n + After your call, there is a value returned in the response body named `moreRowsIndicator`, that will indicate if there are more subscribers that can be returned.\n\n + If your BAN has 40 subscribers and specify subscribers 10 at a time with the page-number value 1, the moreRowsIndicator will return the value `true`.",
            "x-example": 100,
            "pattern": "100"
          },
          {
            "name": "page-number",
            "in": "query",
            "required": false,
            "type": "string",
            "description": "+  Provide a pagination number for which subscribers you wish to obtain data.\n\n +  This parameter is used in conjunction with `page-size` to determine which subscribers are returned for each call.\n\n + If your BAN has 40 subscribers and you wish to see the subscribers 10 at a time, the `page-number` value 1 will obtain subscribers 1-10, the `page-number` 2 will obtain subscribers 11-20, and so on. \n\n + If no value is passed, the default value is 1. \n\n + After your call, there is a value returned in the response body named moreRowsIndicator that will indicate if there are more subscribers that can be returned.\n\n + If your BAN has 40 subscribers and specify subscribers 10 at a time with the `page-number` value 1, the `moreRowsIndicator` will return the value `true`.",
            "x-example": 1,
            "pattern": "1"
          }
        ],
        "tags": [
          "Account Billing Service"
        ],
        "responses": {
          "200": {
            "description": "Successful response",
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "wether cache control is defined",
                "x-example": "no-cache"
              }
            },
            "schema": {
              "$ref": "#/definitions/BillSummaryResponse"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 400,
                "userMessage": "Input validation failed"
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 401,
                "userMessage": "Invalid client or org"
              }
            }
          },
          "404": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 404,
                "userMessage": "Not found"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 500,
                "userMessage": "Internal Server Error"
              }
            }
          },
          "503": {
            "description": "Service unavailable",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 503,
                "userMessage": "Service Unavailable"
              }
            }
          },
          "504": {
            "description": "Gateway timeout",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 504,
                "userMessage": "Backend Service Timeout:System Name"
              }
            }
          }
        }
      }
    }
  },
  "parameters": {
    "Accept": {
      "name": "Accept",
      "in": "header",
      "description": "+   The request header must contain the Accept element.\n+   Accept header is used by HTTP clients to tell the server what content types they'll accept\n+   The default value is *'application/json'*.",
      "required": false,
      "type": "string",
      "x-example": "application/json",
      "pattern": "application/json"
    },
    "Cache-Control": {
      "name": "Cache-Control",
      "in": "header",
      "description": "+   no-cache",
      "required": false,
      "type": "string",
      "x-example": "no-cache",
      "pattern": "no-cache"
    },
    "Content-Type": {
      "name": "Content-Type",
      "in": "header",
      "required": true,
      "description": "\n+   The request header must contain the Content-Type element. \n\n\n+ In HTTP requests, API clients must specify the format in which the\n      data is submitted.\n\n\n\n+   The default value is *'application/json'*.",
      "type": "string",
      "x-example": "application/json",
      "pattern": "application/json"
    },
    "Authorization": {
      "name": "Authorization",
      "in": "header",
      "description": "+   The request header must contain a valid access token generated by APIGEE.\n+   The access token is environment specific and is generated dynamically.\n+   It is mandatory to be passed in the request parameter for authentication.\n+   The sample Value is *'EGQ94pYxAzk2JRmSoW1GTZifPAJI'*.",
      "required": true,
      "type": "string",
      "x-example": "EGQ94pYxAzk2JRmSoW1GTZifPAJI",
      "pattern": "EGQ94pYxAzk2JRmSoW1GTZifPAJI"
    },
    "billing-account": {
      "name": "billing-account",
      "in": "path",
      "required": true,
      "description": "+ Billing-account is a base64 encoded ban Id.\n\n+ BAN is a 9 digit number.",
      "type": "string",
      "x-example": "OTU2NzEyMzQ1",
      "pattern": "OTU2NzEyMzQ1"
    },
    "B2b-Client": {
      "name": "B2b-Client",
      "in": "header",
      "description": "+   The request header must contain B2b Client Id.\n\n\n+   All  B2B customers will have a distinct Client Id. ",
      "required": true,
      "type": "string",
      "pattern": "TEST",
      "x-example": "TEST"
    },
    "B2B-Org": {
      "name": "B2b-Org",
      "in": "header",
      "required": true,
      "description": "+   The request header contains OrgId.\n\n+   It is usually a 19 digit number. \n\n+   All B2B customers will have an organization Id, which needs to be passed here. \n\n+   It is mandatory to have OrgId either in header or in path param/query param. \n\n+   The sample OrgId value is *'7000000000000013000'*.",
      "type": "string",
      "pattern": "7000000000000013000",
      "x-example": 7000000000000013000
    }
  },
  "definitions": {
    "BillSummaryResponse": {
      "type": "object",
      "properties": {
        "billCycleCloseDate": {
          "type": "string",
          "description": "Return the bill cycle close date. End date of the bill cycle",
          "example": "2018-10-13"
        },
        "confirmedBillIndicator": {
          "type": "string",
          "description": "Indicates what type of data is returned in the response body.\n\n +  N = unbilled usage, Y = Billed usage",
          "example": "Y or N"
        },
        "summaryLevel": {
          "type": "string",
          "description": "Return the summary level. It can be BAN or SUBSCRIBER",
          "example": "BAN or SUBSCRIBER"
        },
        "summary": {
          "type": "array",
          "description": "+   Return the summary details",
          "items": {
            "type": "object",
            "description": "Return items",
            "properties": {
              "mobileNumber": {
                "type": "string",
                "description": "When the call species BAN level, there will be no value returned.\n\n +  When the call specifies Subscriber level, the subscriber mobile number will be returned.\n\n +  It is a 10 digit number",
                "example": 9432123456
              },
              "financialSummary": {
                "type": "object",
                "description": "Return the financial Summary details",
                "properties": {
                  "depositAmount": {
                    "type": "number",
                    "description": "Return the deposit Amount in transactional currency ($)",
                    "example": "250.00"
                  },
                  "adjustment": {
                    "type": "object",
                    "description": "Return the adjustment amount in transactional currency ($)",
                    "properties": {
                      "amount": {
                        "type": "number",
                        "description": "The Amount of the Pending Adjustment, without taxes in transactional currency ($). This will be positive amount on Response",
                        "example": "20.00"
                      },
                      "taxAmount": {
                        "type": "number",
                        "description": "The Tax Amount of the Pending Adjustment in transactional currency ($)",
                        "example": "2.00"
                      }
                    }
                  },
                  "charges": {
                    "type": "array",
                    "description": "Return  array list of various charges",
                    "items": {
                      "type": "object",
                      "description": "Return items",
                      "properties": {
                        "amount": {
                          "type": "number",
                          "description": "Captures the amount associated with the subscription type in transactional currency ($)",
                          "example": "20.00"
                        },
                        "taxAmount": {
                          "type": "number",
                          "description": "Captures the tax amount associated with the subscription type in transactional currency ($)",
                          "example": "2.00"
                        },
                        "chargeCategory": {
                          "type": "string",
                          "description": "An indicator stating the category of charges applicable",
                          "example": "General"
                        }
                      }
                    }
                  },
                  "usages": {
                    "type": "object",
                    "description": "Return usages information",
                    "properties": {
                      "usageCharges": {
                        "type": "array",
                        "description": "Return the associated charges on usage",
                        "items": {
                          "type": "object",
                          "description": "Return items",
                          "properties": {
                            "international": {
                              "type": "number",
                              "description": "Return international usage charge in transactional currency ($)",
                              "example": "34.00"
                            },
                            "domestic": {
                              "type": "number",
                              "description": "Return domestic usage charges in transctional currency ($)",
                              "example": "12.00"
                            },
                            "others": {
                              "type": "number",
                              "description": "Return other associated charges  if applicable in transactional currency ($)",
                              "example": "0.0"
                            },
                            "longDistance": {
                              "type": "number",
                              "description": "Return long Distance charges in transactional currency ($)",
                              "example": "0.0"
                            },
                            "usageType": {
                              "type": "string",
                              "description": "The type of usage being measured:Message, Voice, Data. The possible values are VOICE, SMS, DATA",
                              "example": "VOICE"
                            }
                          }
                        }
                      },
                      "total": {
                        "type": "object",
                        "description": "Return total",
                        "properties": {
                          "voice": {
                            "type": "number",
                            "description": "Return voice charges in transactional currency ($)",
                            "example": "0.0"
                          },
                          "sms": {
                            "type": "number",
                            "description": "Return sms charges in transactional currency ($)",
                            "example": "0.0"
                          },
                          "data": {
                            "type": "number",
                            "description": "Return data charges in transactional currency ($)",
                            "example": "0.0"
                          },
                          "thirdPartyPurchases": {
                            "type": "number",
                            "description": "Return third Party Purchase charges in transactional currency ($)",
                            "example": "0.0"
                          },
                          "tmobilePurchases": {
                            "type": "number",
                            "description": "Return tmobile Purchase charges in transactional currency ($)",
                            "example": "0.0"
                          },
                          "overages": {
                            "type": "number",
                            "description": "Return overage charges in transactional currency ($)",
                            "example": "0.0"
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        },
        "status": {
          "type": "string",
          "description": "Indicating status of response. (SUCCESS or FAILURE)",
          "example": "SUCCESS"
        },
        "message": {
          "type": "string",
          "description": "Message indicating status of response",
          "example": "Successfully fetched the Bill Summary"
        },
        "moreRowsIndicator": {
          "type": "boolean",
          "description": "+ After your call, the a value returned in the response body will indicate if there are more subscribers that can be returned. \n\n + If your BAN has 40 subscribers and specify subscribers 10 at a time with the page-number value 1, the moreRowsIndicator will return the value `true`.",
          "example": true
        }
      }
    },
    "ErrorResponse": {
      "type": "object",
      "description": "error response",
      "properties": {
        "code": {
          "type": "string",
          "description": "Provides the error code",
          "x-example": "ATWRK_ORDSVC_ERR_50205"
        },
        "userMessage": {
          "type": "string",
          "description": "Provides the error message",
          "x-example": "Cannot process request . Re-try required"
        }
      }
    }
  }
}
