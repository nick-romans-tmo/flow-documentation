{
  "swagger": "2.0",
  "info": {
    "description": "These APIs are used for B2B Experience Products.",
    "version": "2.0.0",
    "title": "B2B Experience Products",
    "contact": {
      "name": "Apigee/Documentation Support",
      "email": "TFB_DTD_API_DEVOPS@T-Mobile.com"
    }
  },
  "host": "core.saas.api.t-mobile.com",
  "basePath": "/b2b-experience/v2",
  "x-servers": [
    {
      "url": "https://core.saas.api.t-mobile.com",
      "description": "server path"
    }
  ],
  "tags": [
    {
      "name": "Catalog service",
      "description": "Catalog APIs"
    }
  ],
  "securityDefinitions": {
    "OAuth2": {
      "type": "oauth2",
      "flow": "accessCode",
      "description": "Oauth2 Authorization",
      "authorizationUrl": "https://core.saas.api.t-mobile.com/v1/oauth2/accesstoken?grant_type=client_credentials",
      "tokenUrl": "https://core.saas.api.t-mobile.com/v1/oauth2/accesstoken?grant_type=client_credentials",
      "scopes": {
        "read": "Grants read access"
      }
    }
  },
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/rate-plans": {
      "get": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "description": "\n# Description \n  The Select Rate Plans API is intended to give you a list of rate plans that are possible for a specific account (BAN) of your business (Org ID).\n\n  The purpose of this API is to retrieve the applicable rate plans and its features for a given OrgId.\n\n\n  API is designed to return the list of rate plans applicable for a given OrgId. ",
        "operationId": "getRateplans",
        "summary": "B2B Fetching Rate Plans",
        "x-api-pattern": "QueryCollection",
        "tags": [
          "Catalog service"
        ],
        "parameters": [
          {
            "$ref": "#/parameters/Accept"
          },
          {
            "$ref": "#/parameters/Content-Type"
          },
          {
            "$ref": "#/parameters/B2b-client"
          },
          {
            "$ref": "#/parameters/B2b-org"
          },
          {
            "$ref": "#/parameters/Authorization"
          },
          {
            "$ref": "#/parameters/Cache-Control"
          },
          {
            "name": "org",
            "in": "query",
            "required": true,
            "description": "+   OrgId is usually a 19 digit number. \n\n+   All B2B customers will have an organization Id, which needs to be passed here. \n+   It is mandatory to have OrgId either in header or in path param/query param.",
            "type": "string",
            "pattern": "7000000000000013000",
            "x-example": "7000000000000013000"
          },
          {
            "name": "classic",
            "in": "query",
            "required": false,
            "description": "+ Not Used for Partner\n\n+ This field contains classic pricing flag.",
            "type": "string",
            "x-example": true,
            "pattern": "true"
          },
          {
            "name": "billing-account",
            "in": "query",
            "required": true,
            "description": "+ The value must be the base64 encoded billing account number (BAN) for the line (phone number) you wish to activate.  This is a 9 digit value. BAN is mandatory only for partners, for others BAN is optional.",
            "type": "string",
            "pattern": "OTg1NjEyMzQ1",
            "x-example": "OTg1NjEyMzQ1"
          },
          {
            "name": "simonhandactivation",
            "in": "query",
            "required": false,
            "description": "+ Not Used for Partner\n+ This field contains the simonhandactivation flag. The default value is TRUE.\n+ If simOnHandActivation is TRUE (default value) then packageType as SIM will be passed to backend system.\n+ If simOnHandActivation is FALSE then classic parameter will be passed. If classic is TRUE then packageType as NEW, else classic is FALSE then packageType as NEWV will be enforced.",
            "type": "string",
            "pattern": "true",
            "x-example": true
          },
          {
            "name": "order",
            "in": "query",
            "required": false,
            "description": "+ Not Used for Partner\n\n+ This field contains the Order Id.\n\n\n+ The field value is numeric.",
            "type": "string",
            "pattern": "123456",
            "x-example": 123456
          },
          {
            "name": "device",
            "in": "query",
            "required": false,
            "description": "+ Not Used for Partner\n\n+ This field contains Device Id. (Optional)\n\n+ If this parameter is specified, only the rate plans available for that device will be returned.  For example, if the device type is an iPhone Z (device type 123456), then only plans for an iPhone X will be returned.  If no device ID is specified, all possible rate plans for all possible devices will be returned.  This is a 6 digit value.",
            "type": "string",
            "pattern": "123456",
            "x-example": 123456
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "whether cache control is defined",
                "x-example": "no-cache"
              }
            },
            "schema": {
              "$ref": "#/definitions/RatePlanResponse"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40010",
                "userMessage": "Bad Request"
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 401,
                "userMessage": "Unauthorized"
              }
            }
          },
          "404": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40401",
                "userMessage": "No data found"
              }
            }
          },
          "405": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40501",
                "userMessage": "Method Not Allowed"
              }
            },
            "headers": {
              "Allow": {
                "type": "string",
                "description": "List of supported methods for URI",
                "x-example": "GET"
              }
            }
          },
          "406": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40601",
                "userMessage": "Not Acceptable"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_50001",
                "userMessage": "Generic Service Error"
              }
            }
          },
          "503": {
            "description": "Service unavailable",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_50301",
                "userMessage": "Service Unavailable"
              }
            }
          }
        }
      }
    },
    "/services": {
      "get": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "description": "\n# Description \n  The Select Service Plans API is intended to give you a list of services that may be added for a specific rate plan.  \n\n  The purpose of this API is to retrieve the applicable services and its details (SOC, SOCName, MRC, SOCDescription) for a given OrgId, device and rate-plan.\n  \n\n  API is designed to return the list of services applicable for a given OrgId, deviceid and rate-plan.\n  ",
        "operationId": "getServices",
        "summary": "B2B Fetching Service Details",
        "x-api-pattern": "QueryCollection",
        "tags": [
          "Catalog service"
        ],
        "parameters": [
          {
            "$ref": "#/parameters/Accept"
          },
          {
            "$ref": "#/parameters/Content-Type"
          },
          {
            "$ref": "#/parameters/B2b-client"
          },
          {
            "$ref": "#/parameters/B2b-org"
          },
          {
            "$ref": "#/parameters/Authorization"
          },
          {
            "$ref": "#/parameters/Cache-Control"
          },
          {
            "name": "org",
            "in": "query",
            "required": true,
            "description": "+   OrgId is usually a 19 digit number. \n\n+   All B2B customers will have an organization Id, which needs to be passed here. \n+   It is mandatory to have OrgId either in header or in path param/query param.",
            "type": "string",
            "pattern": "7000000000000013000",
            "x-example": "7000000000000013000"
          },
          {
            "name": "device",
            "in": "query",
            "required": false,
            "description": "+ Not Used for Partner\n\n+ This field contains the Device Id. \n\n+ The field value is numeric.",
            "type": "string",
            "pattern": "123456",
            "x-example": 123456
          },
          {
            "name": "rateplan",
            "in": "query",
            "required": true,
            "description": "\n+    Provide Rate Plan Id. Use `uniqueID` (Rate Plan ID) you selected from the Select Rate Plan API.*Note:this is not the  parameter used to activate the line in the SIM on Hand Activation API. This is a 6 digit value.'",
            "type": "string",
            "pattern": "266510",
            "x-example": 266510
          },
          {
            "name": "devicesku",
            "in": "query",
            "required": false,
            "description": "+ Not Used for Partner\n+ This field contains the device sku Id. \n\n+ The field value is numeric",
            "type": "string",
            "x-example": 1234,
            "pattern": "1234"
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "whether cache control is defined",
                "x-example": "no-cache"
              }
            },
            "schema": {
              "$ref": "#/definitions/ServicesResponse"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40010",
                "userMessage": "Bad Request"
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 401,
                "userMessage": "Unauthorized"
              }
            }
          },
          "404": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40401",
                "userMessage": "No data found"
              }
            }
          },
          "405": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40501",
                "userMessage": "Method Not Allowed"
              }
            },
            "headers": {
              "Allow": {
                "type": "string",
                "description": "List of supported methods for URI",
                "x-example": "GET"
              }
            }
          },
          "406": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_40601",
                "userMessage": "Not Acceptable"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_50001",
                "userMessage": "Generic Service Error"
              }
            }
          },
          "503": {
            "description": "Service unavailable",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "ATWRK_TXNSVC_ERR_50301",
                "userMessage": "Service Unavailable"
              }
            }
          }
        }
      }
    }
  },
  "parameters": {
    "Accept": {
      "name": "Accept",
      "in": "header",
      "description": "+   The request header must contain the Accept element.\n+   Accept header is used by HTTP clients to tell the server what content types they'll accept\n+   The default value is *'application/json'*.",
      "required": false,
      "type": "string",
      "pattern": "application",
      "x-example": "application/json"
    },
    "Content-Type": {
      "name": "Content-Type",
      "in": "header",
      "required": true,
      "description": "\n+   The request header must contain the Content-Type element. \n\n\n\n+ In HTTP requests, API clients must specify the format in which the\n       data is submitted.\n\n\n\n+   The default value is *'application/json'*.",
      "type": "string",
      "pattern": "application",
      "x-example": "application/json"
    },
    "Authorization": {
      "name": "Authorization",
      "in": "header",
      "description": "+   The request header must contain a valid access token generated by APIGEE.\n+   The access token is environment specific and is generated dynamically.\n+   It is mandatory to be passed in the request parameter for authentication.\n+   The sample Value is *'EGQ94pYxAzk2JRmSoW1GTZifPAJI'*.",
      "required": true,
      "type": "string",
      "pattern": "EGQ94pYxAzk2JRmSoW1GTZifPAJI",
      "x-example": "EGQ94pYxAzk2JRmSoW1GTZifPAJI"
    },
    "B2b-client": {
      "name": "B2b-client",
      "in": "header",
      "required": true,
      "description": "+   The request header must contain B2b Client Id.\n\n+   The sample value is *'TEST'*.",
      "type": "string",
      "pattern": "TEST",
      "x-example": "TEST"
    },
    "B2b-org": {
      "name": "B2b-org",
      "in": "header",
      "required": true,
      "description": "+   The request header contains OrgId.\n\n\n\n+   It is usually a 19 digit number. \n\n\n+   All B2B customers will have an organization Id, which needs to\n       be passed here. \n\n+   It is mandatory to have OrgId either in header or in path\n       param/query param.",
      "type": "string",
      "pattern": "7000000000000013000",
      "x-example": "7000000000000013000"
    },
    "Cache-Control": {
      "name": "Cache-Control",
      "in": "header",
      "description": "+   no-cache",
      "required": false,
      "type": "string",
      "x-example": "no-cache",
      "pattern": "no-cache"
    },
    "mobile-numbers": {
      "name": "mobile-numbers",
      "in": "query",
      "required": true,
      "description": "\n+   The request query parameters must contain a base64 encoded mobile numbers. \n\n+   Mobile numbers should be a base64 encoded valid number and separated by comma",
      "type": "string",
      "pattern": "9865123456",
      "x-example": "9865123456"
    },
    "jump-check": {
      "name": "jump-check",
      "in": "query",
      "required": false,
      "description": "\n+ The request query parameter determines whether soc is a Jump soc",
      "type": "boolean",
      "pattern": "true",
      "x-example": true
    },
    "blackberry-check": {
      "name": "blackberry-check",
      "in": "query",
      "required": false,
      "description": "\n+ The request query paramter determines whether soc is blackberry simple choice data",
      "type": "boolean",
      "pattern": "true",
      "x-example": true
    }
  },
  "definitions": {
    "ServicesResponse": {
      "type": "object",
      "description": "An array of values for various services including details to display to aid in service(s) selection.",
      "properties": {
        "endOfResult": {
          "type": "boolean",
          "description": "Not Used for Partner. Returns service end results. The sample value is 'true'",
          "example": "true"
        },
        "services": {
          "type": "array",
          "description": "Return list of services. An array of values for various services including details to display to aid in service(s) selection.",
          "items": {
            "type": "object",
            "description": "Return list of service features",
            "properties": {
              "displayPrice": {
                "type": "string",
                "description": "Not Used for Partner. Returns displayPrice of service. The sample value is '0.0'",
                "example": "0.0"
              },
              "features": {
                "type": "array",
                "description": "Not Used for Partner. Return features",
                "items": {
                  "type": "object",
                  "description": "Not Used for Partner. Prevents dialing to most international long distance numbers. You will still be able to place calls to destinations in Canada, Mexico, Puerto Rico, and the US Virgin Islands as well as the United States including Alaska and Hawaii.",
                  "example": "Prevents dialing to most international long distance numbers. You will still be able to place calls to destinations in Canada, Mexico, Puerto Rico, and the US Virgin Islands as well as the United States including Alaska and Hawaii."
                }
              },
              "items": {
                "type": "array",
                "description": "items",
                "items": {
                  "type": "object",
                  "description": "items",
                  "properties": {
                    "partNumber": {
                      "type": "string",
                      "description": "+ Returns partNumber of service. When you are ready to do the new activation, use this parameter in conjunction with the rate plan code as input parameters to the Retrieve Service Agreement API and the SIM on Hand Activation API.  *Note: It is not required that services be added during activation using the SIM on HAND Activation API.",
                      "example": "ILDBARSB"
                    },
                    "uniqueId": {
                      "type": "string",
                      "description": "Not Used for Partner. Returns uniqueID of service. The sample value is '148654'",
                      "example": "148654"
                    }
                  }
                }
              },
              "name": {
                "type": "string",
                "description": "Returns name of service (sample - International Long Distance Barring ALL)",
                "example": "International Long Distance Barring ALL"
              },
              "partNumber": {
                "type": "string",
                "description": "+ Not Used for Partner. Returns partNumber of service. When you are ready to do the new activation, use this parameter in conjunction with the rate plan code as input parameters to the Retrieve Service Agreement API and the SIM on Hand Activation API.  *Note: It is not required that services be added during activation using the SIM on HAND Activation API.",
                "example": "ILDBARSBP"
              },
              "productType": {
                "type": "string",
                "description": "Not Used for Partner. Returns service product type (sample - Basic Service)",
                "example": "Service"
              },
              "rate_category": {
                "type": "string",
                "description": "Not Used for Partner. Returns service rate category. The sample value is 'International'",
                "example": "International"
              },
              "uniqueId": {
                "type": "string",
                "description": "Returns service uniqueID. The sample value is '148653'",
                "example": "148653"
              },
              "price": {
                "type": "string",
                "description": "Not Used for Partner. Returns service price. The sample value is '0.0'",
                "example": "0.0"
              },
              "selected": {
                "type": "string",
                "description": "Not Used for Partner. Returns service selected. The sample value is 'false'",
                "example": "false"
              },
              "category": {
                "type": "string",
                "description": "Not Used for Partner. Return the category. (For example:Blocking, Value-Add etc.\n      )",
                "example": "International"
              }
            }
          }
        }
      }
    },
    "RatePlanResponse": {
      "type": "object",
      "description": "An array of values for various rate plans including details to display to aid in plan selection.",
      "properties": {
        "plans": {
          "type": "array",
          "description": "An array of values for various rate plans including details to display to aid in plan selection.",
          "items": {
            "type": "object",
            "description": "Returns plan details",
            "properties": {
              "isAutoPayEligible": {
                "type": "boolean",
                "description": "Not Used for Partner. Returns true or false Auto Pay Eligible",
                "example": "false"
              },
              "isMBB": {
                "type": "string",
                "description": "Not Used for Partner. Returns true or false for Mobile Brod Brand",
                "example": "Y"
              },
              "isPoolingPlan": {
                "type": "string",
                "description": "Not Used for Partner. Returns true or false for Pooling Plan",
                "example": "N"
              },
              "itemPartNumber": {
                "type": "string",
                "description": "+ Returns item part Number. Once a plan has been chosen, this parameters will be used in other API calls in SIM On hand Activation flow\n+ When you are ready to do the new activation, use this parameter from the rate plan selected as a required input parameter to the SIM on Hand Activation API to look for services.  *Note: this is not the parameter used to get a list of available services for a specific rate plan in Select Rate Plan API.",
                "example": "BZWCOF2G"
              },
              "itemUniqueId": {
                "type": "string",
                "description": "Not Used for Partner. Returns rate plan item UniqueId. The sample value is '45229'",
                "example": "45229"
              },
              "name": {
                "type": "string",
                "description": "Returns rate plan name. The sample value is '2GB Enterprise Mobile Broadband (No Overage)'",
                "example": "2GB Enterprise Mobile Broadband (No Overage)"
              },
              "planCategory": {
                "type": "string",
                "description": "Not Used for Partner. Returns plan Category for rate plan. the sample value is 'C'",
                "example": "C"
              },
              "price": {
                "type": "string",
                "description": "Not Used for Partner. Returns price for rate plan. The sample value is '15.00'",
                "example": "15.00"
              },
              "selected": {
                "type": "string",
                "description": "Not Used for Partner. Returns selected  rate plan. The sample value is 'false'",
                "example": "false"
              },
              "uniqueId": {
                "type": "string",
                "description": "+ Returns uniqueId for rate plan. Once a plan has been chosen, this parameters will be used in other API calls in SIM On hand Activation flow. \n+ If you also want to add services for the new activation, use this parameter from the rate plan selected as a required input parameter to the Select Services API to look for services.  *Note: this is not the parameter used to activate the line in the SIM on Hand Activation API.",
                "example": "45226"
              },
              "isStackable": {
                "type": "string",
                "description": "Not Used for Partner. Returns whether the to stack. The sample value is 'false'",
                "example": "false"
              },
              "planAccountType": {
                "type": "string",
                "description": "Not Used for Partner. 'Returns plan account type code. The sample value is ''BC,IS,GR,GF,BP,BO,BN,BM,BL'''",
                "example": "BC,IS,GR,GF,BP,BO,BN,BM,BL"
              },
              "planTaxIndicator": {
                "type": "string",
                "description": "Not Used for Partner. Returns the plan tax indicator. The sample value is 'TE'",
                "example": "TE"
              },
              "planType": {
                "type": "string",
                "description": "Not Used for Partner. Returns rate plan Type. The sample value is 'C'",
                "example": "C"
              },
              "isDuplicateRatePlan": {
                "type": "boolean",
                "description": "Not Used for Partner. Returns true or false for DuplicateRatePlan. the sample value is 'N'",
                "example": "N"
              },
              "isVirtualRateplan": {
                "type": "boolean",
                "description": "Not Used for Partner. Returns true or false for Virtual Rate plan. The sample value is 'N'",
                "example": "N"
              },
              "features": {
                "type": "array",
                "description": "Not Used for Partner. Returns list of features. The sample value is 'Unlimited 2G & 3G data with no overage charges (speeds slow at 2GB).Instant Messages billed as data and comes out of 2GB monthly data bucket. SMS available as Pay-Per-Use. Domestic data on T-Mobile network only (not available in Puerto Rico). All domestic cellular data roaming is blocked (this includes all T-Mobile 2G and 3G Roaming Partners Networks in the United States).'",
                "example": "Unlimited 2G & 3G data with no overage charges (speeds slow at 2GB).Instant Messages billed as data and comes out of 2GB monthly data bucket. SMS available as Pay-Per-Use. Domestic data on T-Mobile network only (not available in Puerto Rico). All domestic cellular data roaming is blocked (this includes all T-Mobile 2G and 3G Roaming Partners Networks in the United States)",
                "items": {
                  "type": "string",
                  "description": "Returns device Category name"
                }
              },
              "isAvd": {
                "type": "boolean",
                "description": "Not Used for Partner. Returns true or false for Anuval volume disscount",
                "example": true
              },
              "rate_category": {
                "type": "string",
                "description": "Not Used for Partner. Returns rate_category. The sample value is 'Mobile Broadband'",
                "example": "Mobile Broadband"
              }
            }
          }
        }
      }
    },
    "ErrorResponse": {
      "type": "object",
      "description": "error response",
      "properties": {
        "code": {
          "type": "string",
          "description": "Provides the error code",
          "x-example": "ATWRK_ORDSVC_ERR_50205"
        },
        "userMessage": {
          "type": "string",
          "description": "Provides the error message",
          "x-example": "Cannot process request . Re-try required"
        }
      }
    }
  }
}
