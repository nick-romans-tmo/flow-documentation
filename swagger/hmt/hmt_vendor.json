{
  "swagger": "2.0",
  "info": {
    "title": "Vendor Details API",
    "description": "This service returns vendor details based on applicationUserId",
    "version": "1.0.0",
    "contact": {
      "name": "Namitha Jose",
      "email": "Namitha.Jose9@T-Mobile.com"
    },
    "x-createdBy": {
      "dateCreated": "Thur May 09",
      "createdBy": "Thulasi Ram Nandarpavaru",
      "application": "getVendorDetails"
    }
  },
  "host": "tmobileb-sb06.apigee.net",
  "basePath": "/hmt-vendor",
  "schemes": [
    "https"
  ],
  "paths": {
    "/accesstoken": {
      "post": {
        "tags": [
          "getVendorDetails"
        ],
        "summary": "",
        "description": "Resource path with POST method to get Vendor details",
        "operationId": "getVendor",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "tokenAttributes",
            "in": "body",
            "description": "Request object to with apigee client credentials",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Token"
            }
          },
          {
            "name": "Authorization",
            "in": "header",
            "description": "Token from OKTA",
            "x-example": "Bearer eyslvnalndfajdbfkjahsd",
            "required": true,
            "type": "string",
            "minLength": 1
          },
          {
            "name": "Application User Id",
            "in": "header",
            "description": "NTID of the user logged in",
            "x-example": "GCHTestCoach01",
            "required": true,
            "type": "string"
          },
          {
            "name": "userRole",
            "in": "header",
            "description": "role of the user logged in",
            "x-example": "GCH_Coach",
            "required": true,
            "type": "string"
          },
          {
            "name": "Accept",
            "in": "header",
            "description": "Content-Types that are acceptable including application/json",
            "x-example": "application/json",
            "required": false,
            "type": "string"
          },
          {
            "name": "Accept-Charset",
            "in": "header",
            "description": "Character sets that are acceptable.",
            "x-example": "utf-8",
            "required": false,
            "type": "string"
          },
          {
            "name": "Accept-Encoding",
            "in": "header",
            "description": "List of acceptable encodings. See HTTP compression.",
            "x-example": "gzip,deflate",
            "required": false,
            "type": "string"
          },
          {
            "name": "Accept-Language",
            "in": "header",
            "description": "List of acceptable human languages for response",
            "x-example": "en-us",
            "required": false,
            "type": "string"
          },
          {
            "name": "Content-Type",
            "in": "header",
            "description": "The Content-Length header specifies the actual length of the returned payload.",
            "enum": [
              "application/json"
            ],
            "x-example": "text",
            "required": true,
            "type": "string"
          },
          {
            "name": "session-id",
            "in": "header",
            "description": "A value populated by the sender used to track the transactions that occur during a session, a long-lasting interaction that ismaintained by the sender.",
            "x-example": "350b91ec-4a64-4b10-a3f3-a78c8db3924a",
            "required": false,
            "type": "string",
            "minLength": 1,
            "maxLength": 256,
            "pattern": "^[\\S]*$"
          },
          {
            "name": "timestamp",
            "in": "header",
            "description": "ms since 1/1/70 adjusted to GMT",
            "x-example": "1524599536",
            "required": false,
            "type": "integer",
            "format": "int64",
            "minimum": 0,
            "maximum": 999999999999,
            "pattern": "^[\\d]*$"
          },
          {
            "name": "workflow-id",
            "in": "header",
            "description": "Business Use Case",
            "x-example": "ACTIVATION",
            "required": false,
            "type": "string",
            "minLength": 1,
            "maxLength": 256,
            "pattern": "^[\\S]*$"
          }
        ],
        "responses": {
          "200": {
            "description": "Ok",
            "schema": {
              "$ref": "#/definitions/VendorResponse"
            },
            "examples": {
              "application/json": {
                "code": "200",
                "userMessage": "Successfully got Vendor Details.",
                "systemMessage": "SUCCESS"
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "400",
                "userMessage": "Bad Request",
                "systemMessage": "Bad Request"
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "401",
                "userMessage": "Unauthorized",
                "systemMessage": "Unauthorized"
              }
            }
          },
          "404": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "404",
                "userMessage": "Resource not found",
                "systemMessage": "Resource not found"
              }
            }
          },
          "415": {
            "description": "Unsupported Media Type",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "415",
                "userMessage": "Unsupported Media Type",
                "systemMessage": "Unsupported Media Type"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "500",
                "userMessage": "System Error",
                "systemMessage": "System Error"
              }
            }
          },
          "503": {
            "description": "Service unavailable",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "code": "503",
                "userMessage": "Service unavailable",
                "systemMessage": "Service unavailable"
              }
            }
          }
        }
      }
    }
  },
  "definitions": {
    "Token": {
      "type": "object",
      "description": "Request Client credentials",
      "required": [
        "client_id",
        "grant_type"
      ],
      "properties": {
        "client_id": {
          "description": "client_id of the HMT-dev app apigee",
          "type": "string",
          "example": "XJOXSdlHsB0LPviVimw7gk4uVAALkOOC"
        },
        "grant_type": {
          "description": "grant_type of the HMT-dev app",
          "type": "string",
          "example": "client_credentials"
        }
      }
    },
    "VendorResponse": {
      "type": "object",
      "description": "response of the vendor details",
      "required": [
        "u_id",
        "interactionId",
        "access_token",
        "backdateDays"
      ],
      "properties": {
        "u_id": {
          "description": "VendorId of the user logged in",
          "type": "string",
          "example": "102"
        },
        "interactionId": {
          "description": "interactionId generated for the logging purpose to be unique for transactions",
          "type": "string",
          "example": "HMT-Global.102.1558377494001685242414"
        },
        "access_token": {
          "description": "OKTA JWT Token registered in apigee",
          "type": "string",
          "example": "eyJraWQiOiJEVFhVZ0JaNnRWV2N1YVlUZzVrSWR2WE1yQjhvbHB4dVdtWm04VmJsdjhBIiwiYWxnIjoiUlMyNTYifQ"
        },
        "application": {
          "description": "application name",
          "type": "string",
          "example": "GHMT"
        },
        "applicationUserId": {
          "description": "nT ID of the logged in user",
          "type": "string",
          "example": "GCHtestCoach01"
        },
        "userRole": {
          "description": "Role of the logged in user",
          "type": "string",
          "example": "GCH_Coach"
        },
        "backdateDays": {
          "description": "Number of days user is capable of changing the user details in calendar",
          "type": "string",
          "example": "21"
        }
      }
    },
    "Error": {
      "type": "object",
      "description": "Describes the error message, status and system message",
      "required": [
        "code",
        "userMessage"
      ],
      "properties": {
        "code": {
          "description": "A succinct, domain-specific, human-readable text string to identify the type of error for the given status code",
          "example": "400",
          "type": "string",
          "pattern": "^[\\S ]+$"
        },
        "userMessage": {
          "description": "A human-readable message describing the error.",
          "example": "Unable to update the employee details",
          "type": "string",
          "pattern": "^[\\S ]+$"
        },
        "systemMessage": {
          "description": "Text that provides a more detailed technical explanation of the error",
          "example": "Bad Request",
          "type": "string"
        }
      }
    }
  }
}