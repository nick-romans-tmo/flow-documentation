{
  "swagger": "2.0",
  "info": {
    "title": "Response Code Reference API",
    "description": "API for response code reference in T-Mobile Technology. Includes HTTP status codes in addition to JSON-based error codes and messages.",
    "version": "1.0.0"
  },
  "basePath": "/common/v1",
  "schemes": [
    "https"
  ],
  "paths": {
    "/response-codes": {
      "post": {
        "tags": [
          "addResponseCodes"
        ],
        "operationId": "addResponseCodes",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "response-codes",
            "in": "body",
            "description": "Array of Reponse code objects",
            "required": true,
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/responseCode"
              }
            }
          },
          {
            "$ref": "#/parameters/AuthorizationParam"
          },
          {
            "$ref": "#/parameters/XAuthorizationParam"
          }
        ],
        "responses": {
          "200": {
            "description": "The ResponseCode resource has been created."
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        },
        "summary": "Provides a method to create an instance of the response code entity."
      },
      "get": {
        "tags": [
          "getResponseCodes"
        ],
        "operationId": "getResponseCodes",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "response-code",
            "in": "query",
            "description": "response code",
            "required": false,
            "type": "string"
          },
          {
            "name": "application",
            "in": "query",
            "description": "application or system name (can use APM)",
            "required": false,
            "type": "string"
          },
          {
            "name": "domain",
            "in": "query",
            "description": "Functional capability domain",
            "required": false,
            "type": "string"
          },
          {
            "$ref": "#/parameters/AuthorizationParam"
          },
          {
            "$ref": "#/parameters/XAuthorizationParam"
          }
        ],
        "responses": {
          "200": {
            "description": "Ok",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/responseCode"
              }
            },
            "headers": {
              "servicetransactionid": {
                "type": "string"
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        },
        "summary": "GET Errors returns errors for given query parameters based on the search /lookup result."
      },
      "put": {
        "tags": [
          "replaceResponseCode"
        ],
        "operationId": "replaceResponseCode",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "errors",
            "in": "body",
            "description": "Response Code",
            "required": true,
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/responseCode"
              }
            }
          },
          {
            "$ref": "#/parameters/AuthorizationParam"
          },
          {
            "$ref": "#/parameters/XAuthorizationParam"
          }
        ],
        "responses": {
          "200": {
            "description": "Ok"
          },
          "304": {
            "description": "Not Modified",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        },
        "summary": "Response code instance(s) replaced for the provided instance."
      },
      "patch": {
        "tags": [
          "updateResponseCode"
        ],
        "operationId": "updateResponseCode",
        "consumes": [
          "application/merge-patch+json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "errors",
            "in": "body",
            "description": "Response Code",
            "required": true,
            "schema": {
              "$ref": "#/definitions/responseCode"
            }
          },
          {
            "$ref": "#/parameters/AuthorizationParam"
          },
          {
            "$ref": "#/parameters/XAuthorizationParam"
          }
        ],
        "responses": {
          "200": {
            "description": "Ok",
            "schema": {
              "$ref": "#/definitions/responseCode"
            }
          },
          "304": {
            "description": "Not Modified",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/error"
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        },
        "summary": "Update/patch responseCode instance  ."
      }
    }
  },
  "definitions": {
    "contacts": {
      "type": "object",
      "description": "Team contact details",
      "properties": {
        "contactId": {
          "description": "Contact Id or Team id",
          "type": "string"
        },
        "teamName": {
          "description": "Contact or Team Name",
          "type": "string"
        },
        "emailAddress": {
          "description": "Email id of the team",
          "type": "string"
        }
      }
    },
    "responseCode": {
      "type": "object",
      "description": "Response Code definition",
      "properties": {
        "code": {
          "description": "Response Code / id / short code",
          "type": "string"
        },
        "application": {
          "description": "System/application which generates/owns/responsible for this the response code - incl AKM System id",
          "type": "string"
        },
        "domain": {
          "description": "Domain which owns/responsible for this the response code",
          "type": "string"
        },
        "category": {
          "description": "response code category [ERROR, WARN, MESSAGE]",
          "type": "string",
          "enum": [
            "ERROR",
            "WARN",
            "SUCCESS"
          ]
        },
        "severity": {
          "description": "Severity of the response code (mostly applicable for ERROR category)",
          "type": "string"
        },
        "userMessage": {
          "description": "user / developer friendly message for the response code",
          "type": "string"
        },
        "systemMessage": {
          "description": "Description/Message to explain from system/application perspective",
          "type": "string"
        },
        "resolution": {
          "description": "satisfactory answer to resolve this issue, if category is ERROR/WARN.",
          "type": "string"
        },
        "contacts": {
          "description": "Team contact details (Team Contacts API reference. Refer Id /Href here)",
          "type": "array",
          "items": {
            "$ref": "#/definitions/contacts"
          }
        },
        "referenceLinks": {
          "description": "Reference link to the ERROR/WARN (Knowledge Source, discussion/Forum links)",
          "type": "array",
          "items": {
            "type": "string"
          }
        }
      }
    },
    "error": {
      "description": "error message",
      "type": "object",
      "properties": {
        "code": {
          "description": "Used to pass error codes",
          "type": "string"
        },
        "userMessage": {
          "description": "Use to pass human friendly information to the user.",
          "type": "string"
        },
        "systemMessage": {
          "description": "Used to pass system information.",
          "type": "string"
        }
      }
    }
  },
  "parameters": {
    "AuthorizationParam": {
      "name": "Authorization",
      "in": "header",
      "required": true,
      "type": "string"
    },
    "XAuthorizationParam": {
      "name": "X-Authorization",
      "in": "header",
      "required": true,
      "type": "string"
    }
  }
}