{
  "swagger": "2.0",
  "info": {
    "description": "The purpose of this document is to define product requirements of “T-Mobile API Store Locator” for locating T-Mobile retail stores based on geo locations.",
    "version": "1.0.0",
    "title": "StoreLocator",
    "contact": {
      "name": "API",
      "email": "api@t-mobile.com"
    }
  },
  "host": "tmobileb-sb01.apigee.net",
  "basePath":"/selfservice/v1",
  "x-tryOut":"no",
  "x-servers": [],
  "tags": [
    {
      "name": "Retrieve Store List",
      "description": "Retrieve Store List"
    },
    {
      "name": "Find Store",
      "description": "Search Store with Address or Geo Location"
    }
  ],
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/stores.list": {
      "get": {
        "tags": [
          "Retrieve Store List"
        ],
        "summary": "Get list of T-Mobile store ID's",
        "description": "The purpose of this document is to define product requirements of “T-Mobile API Store Locator” for locating T-Mobile retail stores.",
        "operationId": "getStoreList",
        "produces": [
          "application/json"
        ],
        "security": [
          {
            "Oauth": [
              "read:StoreLocation"
            ]
          }
        ],
        "parameters": [
          {
            "name": "Authorization",
            "in": "header",
            "required": true,
            "type": "string",
            "description": "Oauth 2.0 Bearer Token",
            "x-example": "Bearer YWMtHNa3Okb6EeemCisoFv6"
          },
          {
            "name": "Accept",
            "in": "header",
            "required": true,
            "type": "string",
            "description": "The Accept request-header field can be used to specify certain media types which are acceptable for the response.",
            "x-example": "application/json"
          }
        ],
        "responses": {
          "200": {
            "description": "Array of Store ID's",
            "schema": {
              "required": [
                "storeID"
              ],
              "type": "object",
              "properties": {
                "storeID": {
                  "type": "array",
                  "description": "Returns array of store id's",
                  "items": {
                    "type": "string"
                  }
                }
              }
            },
            "examples": {
              "application/json": {
                "storeID": [
                  "FL43Test",
                  "PA126",
                  "TPR - 2323",
                  "TPR - 2165",
                  "TPR - 2251",
                  "LTDC - 1146",
                  "LTDC - 1212"
                ]
              }
            },
            "headers": {
              "Access-Control-Allow-Headers": {
                "type": "string",
                "description": "CORS Allow Headers",
                "x-example": "origin, x-requested-with, accept"
              },
              "Access-Control-Allow-Methods": {
                "type": "string",
                "description": "CORS Allow Methods",
                "x-example": "GET, PUT, POST, DELETE"
              },
              "Access-Control-Allow-Origin": {
                "type": "string",
                "description": "CORS Allow Origin",
                "x-example": "*"
              },
              "Access-Control-Max-Age": {
                "type": "string",
                "description": "Max Age",
                "x-example": "3628800"
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "responseError": [
                  {
                    "code": "StoreLocator-1000",
                    "systemMessage": "Either the geo location, auth headers or address is missing as part of the request.",
                    "userMessage": "Bad request. Please check the request and try again later."
                  }
                ]
              }
            }
          },
          "404": {
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "responseError": [
                  {
                    "code": "StoreLocator-1000",
                    "systemMessage": "No Stores found with the given geo location or address info. Please try again with a valid address or geo location details.",
                    "userMessage": "No Records for qiven query search."
                  }
                ]
              }
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "responseError": [
                  {
                    "code": "General-1000",
                    "systemMessage": "Internal server error. Please try again later. If problem persists please contact support.",
                    "userMessage": "Internal server error."
                  }
                ]
              }
            }
          },
          "503": {
            "description": "Service Unavailable",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "responseError": [
                  {
                    "code": "General-1001",
                    "systemMessage": "Service Unavailable. Please try again later. If problem persists please contact support.",
                    "userMessage": "Service Unavailable."
                  }
                ]
              }
            }
          }
        }
      }
    },
    "/stores.search": {
      "get": {
        "tags": [
          "Find Store"
        ],
        "summary": "Search store based on address",
        "description": "The purpose of this document is to define product requirements of “T-Mobile API Store Locator” for locating T-Mobile retail stores based on Address",
        "operationId": "SearchStoresOnAddress",
        "produces": [
          "application/json"
        ],
        "security": [
          {
            "Oauth": [
              "read:StoreLocation"
            ]
          }
        ],
        "parameters": [
          {
            "name": "address",
            "in": "query",
            "required": true,
            "type": "string",
            "description": "Address Info",
            "x-example": "13328 Northeast 70th Street, Redmond, WA 98052&radius=10"
          },
          {
            "name": "Authorization",
            "in": "header",
            "required": true,
            "type": "string",
            "description": "Oauth 2.0 Bearer Token",
            "x-example": "Bearer VkxFTld3d0lyVmhxMjNXRENNUm"
          },
          {
            "name": "Accept",
            "in": "header",
            "required": true,
            "type": "string",
            "description": "The Accept request-header field can be used to specify certain media types which are acceptable for the response.",
            "x-example": "application/json"
          }
        ],
        "responses": {
          "200": {
            "description": "Store locator info based on address query",
            "schema": {
              "type": "array",
              "items": {
                "required": [
                  "Address_1",
                  "Address_2",
                  "City",
                  "HomePage",
                  "Hours",
                  "Location",
                  "MainPhone",
                  "Name",
                  "PostalCode",
                  "State",
                  "StoreCode"
                ],
                "type": "object",
                "properties": {
                  "StoreCode": {
                    "type": "string",
                    "description": "Store ID"
                  },
                  "Name": {
                    "type": "string",
                    "description": "Store name"
                  },
                  "Address_1": {
                    "type": "string",
                    "description": "Store address"
                  },
                  "Address_2": {
                    "type": "string",
                    "description": "Store address"
                  },
                  "City": {
                    "type": "string",
                    "description": "City where the store is located"
                  },
                  "State": {
                    "type": "string",
                    "description": "State where the store is located"
                  },
                  "PostalCode": {
                    "type": "string",
                    "description": "Zip code where the store is located"
                  },
                  "MainPhone": {
                    "type": "string",
                    "description": "Contact Number of the store"
                  },
                  "HomePage": {
                    "type": "string",
                    "description": "Store web address"
                  },
                  "Hours": {
                    "type": "array",
                    "items": {
                      "required": [
                        "CloseTime",
                        "DayOfWeek",
                        "OpenTime"
                      ],
                      "type": "object",
                      "properties": {
                        "DayOfWeek": {
                          "type": "string"
                        },
                        "OpenTime": {
                          "type": "string"
                        },
                        "CloseTime": {
                          "type": "string"
                        }
                      }
                    }
                  },
                  "Location": {
                    "required": [
                      "lat",
                      "lon"
                    ],
                    "type": "object",
                    "properties": {
                      "lat": {
                        "type": "string",
                        "description": "Latitude"
                      },
                      "lon": {
                        "type": "string",
                        "description": "Longitude"
                      }
                    }
                  }
                }
              }
            },
            "examples": {
              "application/json": [
                {
                  "StoreCode": "TPRiT - 4104",
                  "Name": "T-Mobile Bothell",
                  "Address_1": "18821 Bothell Way NE",
                  "Address_2": "Suite 101",
                  "City": "Bothell",
                  "State": "WA",
                  "PostalCode": "98011-1934",
                  "MainPhone": "(425) 481-1111",
                  "HomePage": "http://www.t-mobile.com/store/cell-phone-bothell-wa-3201.html",
                  "Hours": [
                    {
                      "DayOfWeek": "SUN",
                      "OpenTime": "11:00AM",
                      "CloseTime": "07:00PM"
                    },
                    {
                      "DayOfWeek": "MON",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "TUE",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "WED",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "THU",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "FRI",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "SAT",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    }
                  ],
                  "Location": {
                    "lat": 47.7642045,
                    "lon": -122.2082482
                  }
                },
                {
                  "StoreCode": "WA54",
                  "Name": "T-Mobile Bothell",
                  "Address_1": "20804 Bothell Everett Hwy",
                  "Address_2": "",
                  "City": "Bothell",
                  "State": "WA",
                  "PostalCode": "98021-8404",
                  "MainPhone": "(425) 424-1208",
                  "HomePage": "http://www.t-mobile.com/store/cell-phone-bothell-wa-2696.html",
                  "Hours": [
                    {
                      "DayOfWeek": "SUN",
                      "OpenTime": "11:00AM",
                      "CloseTime": "06:00PM"
                    },
                    {
                      "DayOfWeek": "MON",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "TUE",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "WED",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "THU",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "FRI",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "SAT",
                      "OpenTime": "10:00AM",
                      "CloseTime": "07:00PM"
                    }
                  ],
                  "Location": {
                    "lat": 47.8091378,
                    "lon": -122.2082854
                  }
                }
              ]
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "responseError": [
                  {
                    "code": "StoreLocator-1000",
                    "systemMessage": "Either the geo location, auth headers or address is missing as part of the request.",
                    "userMessage": "Bad request. Please check the request and try again later."
                  }
                ]
              }
            }
          },
          "404": {
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "responseError": [
                  {
                    "code": "StoreLocator-1000",
                    "systemMessage": "No Stores found with the given geo location or address info. Please try again with a valid address or geo location details.",
                    "userMessage": "No Records for qiven query search."
                  }
                ]
              }
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "responseError": [
                  {
                    "code": "General-1000",
                    "systemMessage": "Internal server error. Please try again later. If problem persists please contact support.",
                    "userMessage": "Internal server error."
                  }
                ]
              }
            }
          },
          "503": {
            "description": "Service Unavailable",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "responseError": [
                  {
                    "code": "General-1001",
                    "systemMessage": "Service Unavailable. Please try again later. If problem persists please contact support.",
                    "userMessage": "Service Unavailable."
                  }
                ]
              }
            }
          }
        }
      }
    },
    "/stores": {
      "get": {
        "tags": [
          "Find Store"
        ],
        "summary": "Search Store with Geo Location",
        "description": "The purpose of this document is to define product requirements of “T-Mobile API Store Locator” for locating T-Mobile retail stores based on Address",
        "operationId": "SearchStoresOnGeoLocation",
        "produces": [
          "application/json"
        ],
        "security": [
          {
            "Oauth": [
              "read:StoreLocation"
            ]
          }
        ],
        "parameters": [
          {
            "name": "latitude",
            "in": "header",
            "required": true,
            "type": "string",
            "description": "latitude",
            "x-example": "47.7649887"
          },
          {
            "name": "longitude",
            "in": "header",
            "required": true,
            "type": "string",
            "description": "longitude",
            "x-example": "-122.2100902"
          },
          {
            "name": "radius",
            "in": "header",
            "required": true,
            "type": "string",
            "description": "radius",
            "x-example": "10"
          },
          {
            "name": "limit",
            "in": "header",
            "required": true,
            "type": "string",
            "description": "limit",
            "x-example": "2"
          },
          {
            "name": "Authorization",
            "in": "header",
            "required": true,
            "type": "string",
            "description": "Oauth 2.0 Bearer Token",
            "x-example": "Bearer VkxFTld3d0lyVmhxMjNXRENNUm"
          },
          {
            "name": "Accept",
            "in": "header",
            "required": true,
            "type": "string",
            "description": "The Accept request-header field can be used to specify certain media types which are acceptable for the response.",
            "x-example": "application/json"
          }
        ],
        "responses": {
          "200": {
            "description": "Store locator info based on Geo Location",
            "schema": {
              "type": "array",
              "items": {
                "required": [
                  "Address_1",
                  "Address_2",
                  "City",
                  "HomePage",
                  "Hours",
                  "Location",
                  "MainPhone",
                  "Name",
                  "PostalCode",
                  "State",
                  "StoreCode"
                ],
                "type": "object",
                "properties": {
                  "StoreCode": {
                    "type": "string",
                    "description": "Store ID"
                  },
                  "Name": {
                    "type": "string",
                    "description": "Store name"
                  },
                  "Address_1": {
                    "type": "string",
                    "description": "Store address"
                  },
                  "Address_2": {
                    "type": "string",
                    "description": "Store address"
                  },
                  "City": {
                    "type": "string",
                    "description": "City where the store is located"
                  },
                  "State": {
                    "type": "string",
                    "description": "State where the store is located"
                  },
                  "PostalCode": {
                    "type": "string",
                    "description": "Zip code where the store is located"
                  },
                  "MainPhone": {
                    "type": "string",
                    "description": "Contact Number of the store"
                  },
                  "HomePage": {
                    "type": "string",
                    "description": "Store web address"
                  },
                  "Hours": {
                    "type": "array",
                    "items": {
                      "required": [
                        "CloseTime",
                        "DayOfWeek",
                        "OpenTime"
                      ],
                      "type": "object",
                      "properties": {
                        "DayOfWeek": {
                          "type": "string"
                        },
                        "OpenTime": {
                          "type": "string"
                        },
                        "CloseTime": {
                          "type": "string"
                        }
                      }
                    }
                  },
                  "Location": {
                    "required": [
                      "lat",
                      "lon"
                    ],
                    "type": "object",
                    "properties": {
                      "lat": {
                        "type": "string",
                        "description": "Latitude"
                      },
                      "lon": {
                        "type": "string",
                        "description": "Longitude"
                      }
                    }
                  }
                }
              }
            },
            "examples": {
              "application/json": [
                {
                  "StoreCode": "TPRiT - 4104",
                  "Name": "T-Mobile Bothell",
                  "Address_1": "18821 Bothell Way NE",
                  "Address_2": "Suite 101",
                  "City": "Bothell",
                  "State": "WA",
                  "PostalCode": "98011-1934",
                  "MainPhone": "(425) 481-1111",
                  "HomePage": "http://www.t-mobile.com/store/cell-phone-bothell-wa-3201.html",
                  "Hours": [
                    {
                      "DayOfWeek": "SUN",
                      "OpenTime": "11:00AM",
                      "CloseTime": "07:00PM"
                    },
                    {
                      "DayOfWeek": "MON",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "TUE",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "WED",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "THU",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "FRI",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "SAT",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    }
                  ],
                  "Location": {
                    "lat": 47.7642045,
                    "lon": -122.2082482
                  }
                },
                {
                  "StoreCode": "WA54",
                  "Name": "T-Mobile Bothell",
                  "Address_1": "20804 Bothell Everett Hwy",
                  "Address_2": "",
                  "City": "Bothell",
                  "State": "WA",
                  "PostalCode": "98021-8404",
                  "MainPhone": "(425) 424-1208",
                  "HomePage": "http://www.t-mobile.com/store/cell-phone-bothell-wa-2696.html",
                  "Hours": [
                    {
                      "DayOfWeek": "SUN",
                      "OpenTime": "11:00AM",
                      "CloseTime": "06:00PM"
                    },
                    {
                      "DayOfWeek": "MON",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "TUE",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "WED",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "THU",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "FRI",
                      "OpenTime": "10:00AM",
                      "CloseTime": "09:00PM"
                    },
                    {
                      "DayOfWeek": "SAT",
                      "OpenTime": "10:00AM",
                      "CloseTime": "07:00PM"
                    }
                  ],
                  "Location": {
                    "lat": 47.8091378,
                    "lon": -122.2082854
                  }
                }
              ]
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "responseError": [
                  {
                    "code": "StoreLocator-1000",
                    "systemMessage": "Either the geo location, auth headers or address is missing as part of the request.",
                    "userMessage": "Bad request. Please check the request and try again later."
                  }
                ]
              }
            }
          },
          "404": {
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "responseError": [
                  {
                    "code": "StoreLocator-1000",
                    "systemMessage": "No Stores found with the given geo location or address info. Please try again with a valid address or geo location details.",
                    "userMessage": "No Records for qiven query search."
                  }
                ]
              }
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "responseError": [
                  {
                    "code": "General-1000",
                    "systemMessage": "Internal server error. Please try again later. If problem persists please contact support.",
                    "userMessage": "Internal server error."
                  }
                ]
              }
            }
          },
          "503": {
            "description": "Service Unavailable",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "responseError": [
                  {
                    "code": "General-1001",
                    "systemMessage": "Service Unavailable. Please try again later. If problem persists please contact support.",
                    "userMessage": "Service Unavailable."
                  }
                ]
              }
            }
          }
        }
      }
    }
  },
  "definitions": {
    "errors": {
      "type": "object",
      "properties": {
        "responseError": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/responseError"
          }
        }
      }
    },
    "responseError": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string"
        },
        "systemMessage": {
          "type": "string"
        },
        "userMessage": {
          "type": "string"
        }
      }
    }
  },
  "securityDefinitions": {
    "Oauth": {
      "type": "oauth2",
      "description": "When you sign up for an account, you are given your first API key.\nTo do so please follow this link: https://www.t-mobile.com/site/signup/\nAlso you can generate additional API keys, and delete API keys (as you may\nneed to rotate your keys in the future).\n",
      "tokenUrl": "https://tmobileb-sb01.apigee.net/v1/oauth2/accesstoken?grant_type=client_credentials",
      "flow": "application",
      "scopes": {
        "read:StoreLocation": "Get store based on geo location or address"
      }
    }
  }
}
