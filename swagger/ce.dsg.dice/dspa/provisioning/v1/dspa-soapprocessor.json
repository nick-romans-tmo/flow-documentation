{
    "swagger": "2.0",
    "info": {
        "description": "\"Spring Boot REST API for web service calls from DSPA\"",
        "version": "1.0.0",
        "title": "order-dspa-soapprocessor",
        "contact": {
            "name": "P&T DSPA Dev",
            "email": "P&TDSPADev@T-Mobile.com"
        },
        "license": {
            "name": "T-Mobile Version 1.4"
        }
    },
    "host": "dspa-soapprocessor-dlab02.dev.px-npe01b.cf.t-mobile.com",
    "basePath": "/common/v1/order-dspa-soap",
    "schemes": [
        "https"
      ],
    "tags": [
        {
            "name": "soap-processor-rest-controller",
            "description": "retreives and updates subscriber information from Tibco and RPX backends"
        }
    ],
    "paths": {
        "/reconcile": {
            "post": {
                "tags": [
                    "soap-processor-rest-controller"
                ],
                "summary": "reconcile the subscriber with the details sent in the Reconcile request",
                "operationId": "reconcileUsingPOST",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "in": "body",
                        "name": "reconcileRequest",
                        "description": "reconcileRequest",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/ReconcileRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successfully reconciled the customer"
                    },
                    "201": {
                        "description": "Created"
                    },
                    "400": {
                        "description": "The request/URL sent is invalid"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Accessing the resource you were trying to reach is forbidden"
                    },
                    "404": {
                        "description": "The resource you were trying to reach is not found"
                    },
                    "500": {
                        "description": "Reconcillation failed due to some internal error"
                    }
                }
            }
        },
        "/search/profile": {
            "post": {
                "tags": [
                    "soap-processor-rest-controller"
                ],
                "summary": "retrieve subscriber profile for the msisdn, username or email address sent ",
                "operationId": "getSubscriberProfileUsingPOST",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "in": "body",
                        "name": "subscriberProfileRequest",
                        "description": "subscriberProfileRequest",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/GetSubscriberProfileRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successfully retrieved subscriber profile",
                        "schema": {
                            "$ref": "#/definitions/GetSubscriberProfileResponse"
                        }
                    },
                    "201": {
                        "description": "Created"
                    },
                    "400": {
                        "description": "The request/URL sent is invalid"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Accessing the resource you were trying to reach is forbidden"
                    },
                    "404": {
                        "description": "The resource you were trying to reach is not found or the subscriber profile is not found for the search criteria sent"
                    },
                    "500": {
                        "description": "The service failed due to some internal error"
                    }
                }
            }
        },
        "/search/subscriber/msisdn/{msisdn}/customertype/{customertype}": {
            "get": {
                "tags": [
                    "soap-processor-rest-controller"
                ],
                "summary": "retrieve subscriber details for the msisdn sent",
                "operationId": "getSubscriberDetailsByMsisdn",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "msisdn",
                        "in": "path",
                        "description": "msisdn",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "customertype",
                        "in": "path",
                        "description": "customertype",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successfully retrieved subscriber details",
                        "schema": {
                            "$ref": "#/definitions/GetSubscriberDetailsResponse"
                        }
                    },
                    "400": {
                        "description": "The request sent in the URL is invalid"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Accessing the resource you were trying to reach is forbidden"
                    },
                    "404": {
                        "description": "The resource you were trying to reach is not found or the subscriber details are not found for the msisdn"
                    },
                    "500": {
                        "description": "The service failed due to some internal error"
                    }
                }
            }
        }
    },
    "definitions": {
        "DeleteRequest": {
            "type": "object",
            "properties": {
                "reconcileOrDeleteCriteria": {
                    "$ref": "#/definitions/ReconcileOrDeleteCriteria"
                }
            },
            "title": "DeleteRequest"
        },
        "GetSubscriberDetailsResponse": {
            "type": "object",
            "properties": {
                "subscriberDetails": {
                    "$ref": "#/definitions/SubscriberDetails"
                }
            },
            "title": "GetSubscriberDetailsResponse"
        },
        "GetSubscriberProfileRequest": {
            "type": "object",
            "properties": {
                "profileSearchCriteria": {
                    "$ref": "#/definitions/ProfileSearchCriteria"
                }
            },
            "title": "GetSubscriberProfileRequest"
        },
        "GetSubscriberProfileResponse": {
            "type": "object",
            "properties": {
                "subscriberProfile": {
                    "$ref": "#/definitions/SubscriberProfile"
                }
            },
            "title": "GetSubscriberProfileResponse"
        },
        "ProfileSearchCriteria": {
            "type": "object",
            "properties": {
                "emailAddress": {
                    "type": "string"
                },
                "msisdn": {
                    "type": "string"
                },
                "userName": {
                    "type": "string"
                }
            },
            "title": "ProfileSearchCriteria"
        },
        "ReconcileOrDeleteCriteria": {
            "type": "object",
            "properties": {
                "ban": {
                    "type": "string"
                },
                "customerId": {
                    "type": "string"
                },
                "customerType": {
                    "type": "string"
                },
                "espRecon": {
                    "type": "boolean"
                },
                "msisdn": {
                    "type": "string"
                },
                "sourceType": {
                    "type": "string"
                }
            },
            "title": "ReconcileOrDeleteCriteria"
        },
        "ReconcileRequest": {
            "type": "object",
            "properties": {
                "reconcileOrDeleteCriteria": {
                    "$ref": "#/definitions/ReconcileOrDeleteCriteria"
                }
            },
            "title": "ReconcileRequest"
        },
        "SubscriberDetails": {
            "type": "object",
            "properties": {
                "accountStatus": {
                    "type": "string"
                },
                "accountTypeSubtype": {
                    "type": "string"
                },
                "alertsFlag": {
                    "type": "string"
                },
                "aolFlag": {
                    "type": "string"
                },
                "ban": {
                    "type": "string"
                },
                "billCycleCloseDay": {
                    "type": "string"
                },
                "billingBirthDate": {
                    "type": "string"
                },
                "billingFirstName": {
                    "type": "string"
                },
                "billingLastName": {
                    "type": "string"
                },
                "billingZipCode": {
                    "type": "string"
                },
                "birthDate": {
                    "type": "string"
                },
                "customerId": {
                    "type": "string"
                },
                "dangerFlag": {
                    "type": "string"
                },
                "deleteRequest": {
                    "$ref": "#/definitions/DeleteRequest"
                },
                "detailInfo": {
                    "type": "string"
                },
                "displayName": {
                    "type": "string"
                },
                "email": {
                    "type": "string"
                },
                "errorCode": {
                    "type": "string"
                },
                "errorMessage": {
                    "type": "string"
                },
                "firstName": {
                    "type": "string"
                },
                "gprsFlag": {
                    "type": "string"
                },
                "imsi": {
                    "type": "string"
                },
                "languageIndicator": {
                    "type": "string"
                },
                "lastName": {
                    "type": "string"
                },
                "masterPrivileges": {
                    "type": "string"
                },
                "middleName": {
                    "type": "string"
                },
                "msisdn": {
                    "type": "string"
                },
                "multilineCustomerType": {
                    "type": "string"
                },
                "operatorId": {
                    "type": "string"
                },
                "otherCustomerIds": {
                    "type": "string"
                },
                "otherMsisdns": {
                    "type": "string"
                },
                "password": {
                    "type": "string"
                },
                "passwordChangeRequired": {
                    "type": "string"
                },
                "passwordDecrypted": {
                    "type": "boolean"
                },
                "passwordLegacyFlag": {
                    "type": "string"
                },
                "primaryMsisdn": {
                    "type": "string"
                },
                "reconcileRequest": {
                    "$ref": "#/definitions/ReconcileRequest"
                },
                "registrationStatus": {
                    "type": "string"
                },
                "samsonFeatures": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "samsonSocs": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "secretQuestion": {
                    "type": "string"
                },
                "secretQuestionAnswer": {
                    "type": "string"
                },
                "serviceLevel": {
                    "type": "string"
                },
                "simSerial": {
                    "type": "string"
                },
                "socIndicators": {
                    "type": "string"
                },
                "statusChangeDate": {
                    "type": "string"
                },
                "subscriberFirstName": {
                    "type": "string"
                },
                "subscriberLastName": {
                    "type": "string"
                },
                "subscriberType": {
                    "type": "string"
                },
                "type": {
                    "type": "string"
                },
                "userId": {
                    "type": "string"
                },
                "zipCode": {
                    "type": "string"
                }
            },
            "title": "SubscriberDetails"
        },
        "SubscriberProfile": {
            "type": "object",
            "properties": {
                "accountStatus": {
                    "type": "string"
                },
                "ban": {
                    "type": "string"
                },
                "birthDate": {
                    "type": "string"
                },
                "detailInfo": {
                    "type": "string"
                },
                "effectiveDate": {
                    "type": "string"
                },
                "emailAddress": {
                    "type": "string"
                },
                "errorCode": {
                    "type": "string"
                },
                "errorMessage": {
                    "type": "string"
                },
                "firstName": {
                    "type": "string"
                },
                "imsi": {
                    "type": "string"
                },
                "indicators": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "initialPasswordResetDate": {
                    "type": "string"
                },
                "lastEmailUpdate": {
                    "type": "string"
                },
                "lastLoginDate": {
                    "type": "string"
                },
                "lastName": {
                    "type": "string"
                },
                "linkedAccounts": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "msisdn": {
                    "type": "string"
                },
                "password": {
                    "type": "string"
                },
                "passwordDecrypted": {
                    "type": "boolean"
                },
                "registrationStatus": {
                    "type": "string"
                },
                "role": {
                    "type": "string"
                },
                "sim": {
                    "type": "string"
                },
                "subscriberType": {
                    "type": "string"
                },
                "userName": {
                    "type": "string"
                },
                "zipCode": {
                    "type": "string"
                }
            },
            "title": "SubscriberProfile"
        }
    }
}
