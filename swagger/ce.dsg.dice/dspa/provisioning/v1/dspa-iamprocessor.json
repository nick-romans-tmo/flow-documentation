{
    "swagger": "2.0",
    "info": {
        "description": "\"Spring Boot REST API for IAM calls from DSPA\"",
        "version": "1.0.0",
        "title": "order-dspa-iamprocessor",
        "contact": {
            "name": "P&T DSPA Dev",
            "email": "P&TDSPADev@T-Mobile.com"
        },
        "license": {
            "name": "T-Mobile Version 1.4"
        }
    },
    "host": "dspa-iamprocessor-dlab02.dev.px-npe01b.cf.t-mobile.com",
    "basePath": "/common/v1/order-dspa-iam",
    "schemes": [
        "https"
      ],
    "tags": [
        {
            "name": "iam-processor-rest-controller",
            "description": "retrieves and updates data in IAM backend"
        }
    ],
    "paths": {
        "/search/profile/msisdn/{msisdn}": {
            "get": {
                "tags": [
                    "iam-processor-rest-controller"
                ],
                "summary": "retreive Customer profile details of the msisdn sent in the request",
                "operationId": "getProfile",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "msisdn",
                        "in": "path",
                        "description": "msisdn",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "retreived CustomerProfile details of the msisdn succesfully",
                        "schema": {
                            "$ref": "#/definitions/CustomerProfileResponse"
                        }
                    },
                    "400": {
                        "description": "The request sent in the URL is invalid"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Accessing the resource you were trying to reach is forbidden"
                    },
                    "404": {
                        "description": "The resource you were trying to reach is not found or the subscriber details are not found for the msisdn"
                    },
                    "500": {
                        "description": "The service failed due to some internal error"
                    }
                }
            }
        },
        "/search/subscriber/customerid/{customerid}": {
            "get": {
                "tags": [
                    "iam-processor-rest-controller"
                ],
                "summary": "retrieve subscriber details for the customerId sent",
                "operationId": "getSubscriberDetailsByCustId",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "customerid",
                        "in": "path",
                        "description": "customerid",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successfully retrieved subscriber details",
                        "schema": {
                            "$ref": "#/definitions/GetSubscriberDetailsResponse"
                        }
                    },
                    "400": {
                        "description": "The request sent in the URL is invalid"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Accessing the resource you were trying to reach is forbidden"
                    },
                    "404": {
                        "description": "The resource you were trying to reach is not found or the subscriber details are not found for the msisdn"
                    },
                    "500": {
                        "description": "The service failed due to some internal error"
                    }
                }
            }
        },
        "/search/subscriber/msisdn/{msisdn}": {
            "get": {
                "tags": [
                    "iam-processor-rest-controller"
                ],
                "summary": "retrieve subscriber details for the msisdn sent",
                "operationId": "getSubscriberDetailsByMsisdn",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "msisdn",
                        "in": "path",
                        "description": "msisdn",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successfully retrieved subscriber details",
                        "schema": {
                            "$ref": "#/definitions/GetSubscriberDetailsResponse"
                        }
                    },
                    "400": {
                        "description": "The request sent in the URL is invalid"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Accessing the resource you were trying to reach is forbidden"
                    },
                    "404": {
                        "description": "The resource you were trying to reach is not found or the subscriber details are not found for the msisdn"
                    },
                    "500": {
                        "description": "The service failed due to some internal error"
                    }
                }
            }
        }
    },
    "definitions": {
        "CustomerProfile": {
            "type": "object",
            "properties": {
                "accountStatus": {
                    "type": "string"
                },
                "accountSubType": {
                    "type": "string"
                },
                "accountType": {
                    "type": "string"
                },
                "addressLine1": {
                    "type": "string"
                },
                "addressLine2": {
                    "type": "string"
                },
                "authenticationFailureCount": {
                    "type": "integer",
                    "format": "int32"
                },
                "ban": {
                    "type": "string"
                },
                "cityName": {
                    "type": "string"
                },
                "communicationPreferenceOptIn": {
                    "type": "boolean"
                },
                "contactPhNumber": {
                    "type": "string"
                },
                "controlledPlanType": {
                    "type": "string"
                },
                "customerId": {
                    "type": "number"
                },
                "customerPermissions": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/Permission"
                    }
                },
                "customerType": {
                    "type": "string"
                },
                "dangerCustomer": {
                    "type": "boolean"
                },
                "dateOfBirth": {
                    "$ref": "#/definitions/DateOfBirth"
                },
                "defaultWebPref": {
                    "type": "boolean"
                },
                "emailAddress": {
                    "type": "string"
                },
                "friendlyFirstName": {
                    "type": "string"
                },
                "friendlyLastName": {
                    "type": "string"
                },
                "friendlyMiddleName": {
                    "type": "string"
                },
                "grantMasterPrivileges": {
                    "type": "boolean"
                },
                "imsi": {
                    "type": "string"
                },
                "lastEmailUpdate": {
                    "type": "string",
                    "format": "date-time"
                },
                "msisdn": {
                    "type": "integer",
                    "format": "int64"
                },
                "multilineCustomerType": {
                    "type": "string"
                },
                "mustChangePassword": {
                    "type": "boolean"
                },
                "pah": {
                    "type": "boolean"
                },
                "password": {
                    "type": "string"
                },
                "registrationStatus": {
                    "type": "string"
                },
                "secretQuestion": {
                    "type": "string"
                },
                "secretQuestionAnswer": {
                    "type": "string"
                },
                "securityAnswer": {
                    "$ref": "#/definitions/SecurityAnswer"
                },
                "securityQuestionId": {
                    "$ref": "#/definitions/SecurityQuestionId"
                },
                "simSerial": {
                    "type": "string"
                },
                "stateName": {
                    "type": "string"
                },
                "zipCode": {
                    "type": "string"
                }
            },
            "title": "CustomerProfile"
        },
        "CustomerProfileResponse": {
            "type": "object",
            "properties": {
                "customerProfile": {
                    "$ref": "#/definitions/CustomerProfile"
                }
            },
            "title": "CustomerProfileResponse"
        },
        "DateOfBirth": {
            "type": "object",
            "properties": {
                "dayOfMonth": {
                    "type": "integer",
                    "format": "int32"
                },
                "month": {
                    "type": "integer",
                    "format": "int32"
                },
                "year": {
                    "type": "integer",
                    "format": "int32"
                }
            },
            "title": "DateOfBirth"
        },
        "DeleteRequest": {
            "type": "object",
            "properties": {
                "reconcileOrDeleteCriteria": {
                    "$ref": "#/definitions/ReconcileOrDeleteCriteria"
                }
            },
            "title": "DeleteRequest"
        },
        "GetSubscriberDetailsResponse": {
            "type": "object",
            "properties": {
                "subscriberDetails": {
                    "$ref": "#/definitions/SubscriberDetails"
                }
            },
            "title": "GetSubscriberDetailsResponse"
        },
        "Permission": {
            "type": "object",
            "properties": {
                "canBeModified": {
                    "type": "boolean"
                },
                "permissionCode": {
                    "type": "string"
                },
                "permissionDesc": {
                    "type": "string"
                }
            },
            "title": "Permission"
        },
        "ReconcileOrDeleteCriteria": {
            "type": "object",
            "properties": {
                "ban": {
                    "type": "string"
                },
                "customerId": {
                    "type": "string"
                },
                "customerType": {
                    "type": "string"
                },
                "espRecon": {
                    "type": "boolean"
                },
                "msisdn": {
                    "type": "string"
                },
                "sourceType": {
                    "type": "string"
                }
            },
            "title": "ReconcileOrDeleteCriteria"
        },
        "ReconcileRequest": {
            "type": "object",
            "properties": {
                "reconcileOrDeleteCriteria": {
                    "$ref": "#/definitions/ReconcileOrDeleteCriteria"
                }
            },
            "title": "ReconcileRequest"
        },
        "SecurityAnswer": {
            "type": "object",
            "properties": {
                "answer1": {
                    "type": "string"
                },
                "answer2": {
                    "type": "string"
                },
                "answer3": {
                    "type": "string"
                },
                "answer4": {
                    "type": "string"
                },
                "answer5": {
                    "type": "string"
                },
                "answer6": {
                    "type": "string"
                },
                "answer7": {
                    "type": "string"
                }
            },
            "title": "SecurityAnswer"
        },
        "SecurityQuestionId": {
            "type": "object",
            "properties": {
                "questionId1": {
                    "type": "string"
                },
                "questionId2": {
                    "type": "string"
                },
                "questionId3": {
                    "type": "string"
                },
                "questionId4": {
                    "type": "string"
                },
                "questionId5": {
                    "type": "string"
                },
                "questionId6": {
                    "type": "string"
                },
                "questionId7": {
                    "type": "string"
                }
            },
            "title": "SecurityQuestionId"
        },
        "SubscriberDetails": {
            "type": "object",
            "properties": {
                "accountStatus": {
                    "type": "string"
                },
                "accountTypeSubtype": {
                    "type": "string"
                },
                "alertsFlag": {
                    "type": "string"
                },
                "aolFlag": {
                    "type": "string"
                },
                "ban": {
                    "type": "string"
                },
                "billCycleCloseDay": {
                    "type": "string"
                },
                "billingBirthDate": {
                    "type": "string"
                },
                "billingFirstName": {
                    "type": "string"
                },
                "billingLastName": {
                    "type": "string"
                },
                "billingZipCode": {
                    "type": "string"
                },
                "birthDate": {
                    "type": "string"
                },
                "customerId": {
                    "type": "string"
                },
                "dangerFlag": {
                    "type": "string"
                },
                "deleteRequest": {
                    "$ref": "#/definitions/DeleteRequest"
                },
                "detailInfo": {
                    "type": "string"
                },
                "displayName": {
                    "type": "string"
                },
                "email": {
                    "type": "string"
                },
                "errorCode": {
                    "type": "string"
                },
                "errorMessage": {
                    "type": "string"
                },
                "firstName": {
                    "type": "string"
                },
                "gprsFlag": {
                    "type": "string"
                },
                "imsi": {
                    "type": "string"
                },
                "languageIndicator": {
                    "type": "string"
                },
                "lastName": {
                    "type": "string"
                },
                "masterPrivileges": {
                    "type": "string"
                },
                "middleName": {
                    "type": "string"
                },
                "msisdn": {
                    "type": "string"
                },
                "multilineCustomerType": {
                    "type": "string"
                },
                "operatorId": {
                    "type": "string"
                },
                "otherCustomerIds": {
                    "type": "string"
                },
                "otherMsisdns": {
                    "type": "string"
                },
                "password": {
                    "type": "string"
                },
                "passwordChangeRequired": {
                    "type": "string"
                },
                "passwordDecrypted": {
                    "type": "boolean"
                },
                "passwordLegacyFlag": {
                    "type": "string"
                },
                "primaryMsisdn": {
                    "type": "string"
                },
                "reconcileRequest": {
                    "$ref": "#/definitions/ReconcileRequest"
                },
                "registrationStatus": {
                    "type": "string"
                },
                "samsonFeatures": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "samsonSocs": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "secretQuestion": {
                    "type": "string"
                },
                "secretQuestionAnswer": {
                    "type": "string"
                },
                "serviceLevel": {
                    "type": "string"
                },
                "simSerial": {
                    "type": "string"
                },
                "socIndicators": {
                    "type": "string"
                },
                "statusChangeDate": {
                    "type": "string"
                },
                "subscriberFirstName": {
                    "type": "string"
                },
                "subscriberLastName": {
                    "type": "string"
                },
                "subscriberType": {
                    "type": "string"
                },
                "type": {
                    "type": "string"
                },
                "userId": {
                    "type": "string"
                },
                "zipCode": {
                    "type": "string"
                }
            },
            "title": "SubscriberDetails"
        }
    }
}
