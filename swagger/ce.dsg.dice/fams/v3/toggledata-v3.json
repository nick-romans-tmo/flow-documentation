{
  "swagger": "2.0",
  "host": "dev01.api.t-mobile.com",
  "basePath": "/eai/v3",
  "schemes": [
    "https"
  ],
  "info": {
    "title": "ToggleData",
    "description": "This service manages autorization tokens.",
    "contact": {
      "name": "DSGDICE-UMS",
      "url": "http://www.t-mobile.com",
      "email": "DSGDICE-UMS@T-Mobile.com"
    },
    "version": "1.0.0"
  },
  "x-servers": [
    {
      "url": "https://tmobileb-sb01.apigee.net/selfservice/v1",
      "description": "Sandbox Server"
    }
  ],
  "tags": [
    {
      "name": "postToggleData",
      "description": "Provides interface to store toggle data."
    },
    {
      "name": "getToggleData",
      "description": "Retrieves toggle data information from repository."
    },
    {
      "name": "deleteToggleData",
      "description": "Deletes toggle data in the repository."
    }
  ],
  "paths": {
    "/toggle-data": {
      "post": {
        "tags": [
          "postToggleData"
        ],
        "x-api-pattern": "CreateInCollection",
        "summary": "Provides interface to store toggle data.",
        "description": "Generates a token that can later be verified by another application. Through the optional parameters to this operation the token can be made self-renewing, given an arbitrary expiration period, and can be limited in the number of times it can be verified.",
        "operationId": "postToggleData",
        "consumes": [
          "application/json",
          "application/xml"
        ],
        "produces": [
          "application/json",
          "application/xml"
        ],
        "responses": {
          "201": {
            "description": "Created",
            "examples": {
              "application/json": {
                "userdata": "string",
                "togglekey": "string",
                "expirationTime": "string"
              }
            },
            "schema": {
              "$ref": "#/definitions/ToggleData"
            },
            "headers": {
              "Date": {
                "description": "The Date general HTTP header contains the date and time at which the message was originated. Required",
                "x-example": "Mon, 05 Mar 2018 16:38:08 GMT",
                "type": "string",
                "format": "date-time"
              },
              "Location": {
                "description": "The Location header specifies the URI of a created resource, or redirects the API consumer to an alternate resource location. Required",
                "x-example": "http://api.t-mo.com/order-mgmt/v1/orders/123xyz-0/status",
                "type": "string",
                "minLength": 1,
                "maxLength": 65536
              },
              "Content-Type": {
                "description": "The Content-Length header specifies the actual length of the returned payload. Required if content returned.",
                "x-example": "application/json",
                "type": "string",
                "pattern": "^[\\S]*$"
              },
              "ETag": {
                "description": "The ETag header specifies the unique entity tag value for the returned resource.",
                "x-example": "*",
                "type": "string",
                "pattern": "^[\\S]*$"
              },
              "Content-Length": {
                "description": "The Content-Length header specifies the actual length of the returned payload.",
                "x-example": 9112300000003,
                "type": "integer",
                "format": "int64",
                "minimum": 0,
                "pattern": "^[\\d]*$"
              },
              "Transfer-Encoding": {
                "description": "Specifies the form of encoding used to safely transfer the entity to the user.",
                "x-example": "chunked",
                "type": "string",
                "pattern": "^[a-zA-Z0-9]*$"
              },
              "service-transaction-id": {
                "description": "Request ID echoed back by server",
                "x-example": "rrt104eadn-21295-143945655-1-1444066988453",
                "type": "string",
                "pattern": "^[a-zA-Z0-9-]*$"
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Bad Request",
                "userMessage": "System not able to process message.",
                "systemMessage": "400 Bad Request",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Unauthorized",
                "userMessage": "Invalid credentials for target resource.",
                "systemMessage": "401 Unauthorized",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "403": {
            "description": "Forbidden",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Forbidden",
                "userMessage": "Operation is forbidden on this server.",
                "systemMessage": "403 Forbidden",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "404": {
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Not Found",
                "userMessage": "Resource not found on this server.",
                "systemMessage": "404 Not Found",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "405": {
            "description": "Method Not Allowed",
            "headers": {
              "Allow": {
                "description": "List of supported methods for API.",
                "x-example": "GET",
                "type": "string",
                "pattern": "^[a-zA-Z]+$"
              },
              "service-transaction-id": {
                "description": "Request ID echoed back by server",
                "x-example": "rrt104eadn-21295-143945655-1-1444066988453",
                "type": "string",
                "pattern": "^[\\S]*$"
              }
            },
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Method Not Allowed",
                "userMessage": "Method not supported by this server.",
                "systemMessage": "405 Method Not Allowed",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "406": {
            "description": "Not Acceptable",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Not Acceptable",
                "userMessage": "Resource problem deteceted.",
                "systemMessage": "406 Not Acceptable",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "409": {
            "description": "Conflict",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Conflict",
                "userMessage": "Target resource in wrong state.",
                "systemMessage": "409 Conflict",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "415": {
            "description": "Unsupported Media Type",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Unsupported Media Type",
                "userMessage": "Payload not supported by target resource.",
                "systemMessage": "415 Unsupported Media Type",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Internal Server Error",
                "userMessage": "Unexpected error condition encountered.",
                "systemMessage": "500 Internal Server Error",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "503": {
            "description": "Service Unavailable",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Service Unavailable",
                "userMessage": "System is under maintenance.",
                "systemMessage": "503 Service Unavailable",
                "detailLink": "http://www.tmus.com"
              }
            }
          }
        },
        "parameters": [
          {
            "name": "toggleData",
            "in": "body",
            "description": "Provides interface to store toggle data.",
            "schema": {
              "$ref": "#/definitions/ToggleData"
            }
          },
          {
            "name": "expiration-period",
            "in": "query",
            "required": false,
            "x-example": 1200,
            "description": "An integer representing the lifetime of the token, in seconds. The default value represents a lifetime of 20 minutes, the maximum represents a value of two hours. This value both sets the initial expiration time and establishes how far to extend the expiration time with each successful validation of self-renewing tokens.",
            "type": "string"
          },
          {
            "name": "max-validations",
            "in": "query",
            "required": false,
            "x-example": 2,
            "description": "An integer representing the maximum number of times that the generated token may be validated. This allows true \"single-use\" tokens, if required. The default value represents a theoretically unlimited number of validations.",
            "type": "integer"
          },
          {
            "name": "self-renewal",
            "in": "query",
            "required": false,
            "x-example": true,
            "description": "A boolean value representing whether the generated token should be self-renewing, default is false.",
            "type": "boolean"
          },
          {
            "$ref": "#/parameters/AuthorizationParam"
          },
          {
            "$ref": "#/parameters/ContentTypeParam"
          },
          {
            "$ref": "#/parameters/activityidParam"
          },
          {
            "$ref": "#/parameters/dateParam"
          },
          {
            "$ref": "#/parameters/AcceptParam"
          },
          {
            "$ref": "#/parameters/xAuthParam"
          },
          {
            "$ref": "#/parameters/xAuthOrigParam"
          },
          {
            "$ref": "#/parameters/sessionidParam"
          },
          {
            "$ref": "#/parameters/interactionidParam"
          },
          {
            "$ref": "#/parameters/workflowidParam"
          },
          {
            "$ref": "#/parameters/timestampParam"
          },
          {
            "$ref": "#/parameters/servTransIdParam"
          }
        ],
        "security": [
          {
            "JWT": []
          }
        ]
      }
    },
    "/toggle-data/{toggle-key}": {
      "get": {
        "tags": [
          "getToggleData"
        ],
        "x-api-pattern": "QueryResource",
        "summary": "Retrieves toggle data information from repository",
        "description": "Validate a token, as generated by the POST operation. A valid token must a) exist in the database, b) be unexpired (by comparing the token's expiration time to the current time), and c) must not have been validated more times than allowed. If any of these conditions are not met then the token will be invalid, and calls to this operation will reflect that in their returned status values.",
        "operationId": "getToggleData",
        "consumes": [
          "application/json",
          "application/xml"
        ],
        "produces": [
          "application/json",
          "application/xml"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/ToggleData"
            },
            "examples": {
              "application/json": {
                "userdata": "string",
                "togglekey": "string",
                "expirationTime": "string"
              }
            },
            "headers": {
              "Date": {
                "description": "The Date general HTTP header contains the date and time at which the message was originated. (Required)",
                "x-example": "Mon, 05 Mar 2018 16:38:08 GMT",
                "type": "string",
                "format": "date-time"
              },
              "Content-Type": {
                "description": "The Content-Length header specifies the actual length of the returned payload.",
                "x-example": "application/json",
                "type": "string",
                "pattern": "^[\\S]*$"
              },
              "Content-Length": {
                "description": "The Content-Length header specifies the actual length of the returned payload.",
                "x-example": 9112300000003,
                "type": "integer",
                "format": "int64",
                "minimum": 0,
                "pattern": "^[\\d]*$"
              },
              "Transfer-Encoding": {
                "description": "Specifies the form of encoding used to safely transfer the entity to the user.",
                "x-example": "chunked",
                "type": "string",
                "pattern": "^[a-zA-Z0-9]*$"
              },
              "ETag": {
                "description": "The ETag header specifies the unique entity tag value for the returned resource.",
                "x-example": "*",
                "type": "string",
                "pattern": "^[\\S]*$"
              },
              "Cache-Control": {
                "description": "Cache policy supported for the resource by the server.",
                "x-example": "no-cache",
                "type": "string",
                "pattern": "^[a-zA-Z0-9-]*$"
              },
              "Content-Encoding": {
                "description": "Encoding of the content (e.g. gzip)",
                "x-example": "gzip",
                "type": "string",
                "pattern": "^[\\S]*$"
              },
              "Content-Language": {
                "description": "Spoken language of the response.",
                "x-example": "EN",
                "type": "string",
                "pattern": "^[\\S]*$"
              },
              "Last-Modified": {
                "description": "The Last-Modified entity-header field indicates the date and time at which the origin server believes the variant was last modified.",
                "x-example": "Mon, 05 Mar 2018 16:38:08 GMT",
                "type": "string",
                "format": "date-time",
                "pattern": "^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), [0-3]{1}[0-9]{1} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [1-3][0-9]{3} [0-2][0-9]:[0-6][0-9]:[0-6][0-9] GMT$"
              },
              "service-transaction-id": {
                "description": "Request ID echoed back by server",
                "x-example": "rrt104eadn-21295-143945655-1-1444066988453",
                "type": "string",
                "pattern": "^[\\S]*$"
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Bad Request",
                "userMessage": "System not able to process message.",
                "systemMessage": "400 Bad Request",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Unauthorized",
                "userMessage": "Invalid credentials for target resource.",
                "systemMessage": "401 Unauthorized",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "404": {
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Not Found",
                "userMessage": "Resource not found on this server.",
                "systemMessage": "404 Not Found",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "405": {
            "description": "Method Not Allowed",
            "headers": {
              "Allow": {
                "description": "List of supported methods for API.",
                "x-example": "GET",
                "type": "string",
                "pattern": "^[a-zA-Z]+$"
              },
              "service-transaction-id": {
                "description": "Request ID echoed back by server",
                "x-example": "rrt104eadn-21295-143945655-1-1444066988453",
                "type": "string",
                "pattern": "^[\\S]*$"
              }
            },
            "examples": {
              "application/json": {
                "code": "Method Not Allowed",
                "userMessage": "Method not supported by this server.",
                "systemMessage": "405 Method Not Allowed",
                "detailLink": "http://www.tmus.com"
              }
            },
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "406": {
            "description": "Not Acceptable",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Not Acceptable",
                "userMessage": "Resource problem deteceted.",
                "systemMessage": "406 Not Acceptable",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "System Error",
                "userMessage": "Unexpected error condition encountered.",
                "systemMessage": "500 Internal Server Error",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "503": {
            "description": "Service Unavailable",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Service Unavailable",
                "userMessage": "System is under maintenance.",
                "systemMessage": "503 Service Unavailable",
                "detailLink": "http://www.tmus.com"
              }
            }
          }
        },
        "parameters": [
          {
            "name": "toggle-key",
            "in": "path",
            "required": true,
            "description": "A string representing the token itself. This string will be twenty characters in length, composed of upper- and lower-case letters and digits.",
            "x-example": "Faskdjfhi1319827HJDa",
            "type": "string",
            "pattern": "^[a-zA-Z0-9]{20}$"
          },
          {
            "$ref": "#/parameters/AuthorizationParam"
          },
          {
            "$ref": "#/parameters/If-None-MatchParam"
          },
          {
            "$ref": "#/parameters/activityidParam"
          },
          {
            "$ref": "#/parameters/dateParam"
          },
          {
            "$ref": "#/parameters/If-Modified-SinceParam"
          },
          {
            "$ref": "#/parameters/AcceptParam"
          },
          {
            "$ref": "#/parameters/acceptEncodeParam"
          },
          {
            "$ref": "#/parameters/acceptLangParam"
          },
          {
            "$ref": "#/parameters/xAuthParam"
          },
          {
            "$ref": "#/parameters/xAuthOrigParam"
          },
          {
            "$ref": "#/parameters/sessionidParam"
          },
          {
            "$ref": "#/parameters/interactionidParam"
          },
          {
            "$ref": "#/parameters/workflowidParam"
          },
          {
            "$ref": "#/parameters/timestampParam"
          },
          {
            "$ref": "#/parameters/servTransIdParam"
          }
        ],
        "security": [
          {
            "JWT": []
          }
        ]
      },
      "delete": {
        "tags": [
          "deleteToggleData"
        ],
        "x-api-pattern": "RemoveResource",
        "summary": "Deletes toggle data in the repository.",
        "description": "A request to destroy an existing token (with the implied purpose that the token is of no further use, even if it hasn't expired). Please note that the only error condition will be if the database is inaccessible, as it is impossible to distinguish between an invalid (not present) token and one that has already been deleted by the periodic data management routines that remove expired tokens.",
        "operationId": "deleteToggleData",
        "consumes": [
          "application/json",
          "application/xml"
        ],
        "produces": [
          "application/json",
          "application/xml"
        ],
        "responses": {
          "204": {
            "description": "No Content",
            "headers": {
              "Date": {
                "description": "The Date general HTTP header contains the date and time at which the message was originated.",
                "x-example": "Mon, 05 Mar 2018 16:38:08 GMT",
                "type": "string",
                "format": "date-time"
              },
              "service-transaction-id": {
                "description": "Request ID echoed back by server",
                "x-example": "rrt104eadn-21295-143945655-1-1444066988453",
                "type": "string",
                "pattern": "^[\\S]*$"
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Bad Request",
                "userMessage": "System not able to process message.",
                "systemMessage": "400 Bad Request",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Unauthorized",
                "userMessage": "Invalid credentials for target resource.",
                "systemMessage": "401 Unauthorized",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "404": {
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Not Found",
                "userMessage": "Resource not found on this server.",
                "systemMessage": "404 Not Found",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "405": {
            "description": "Method Not Allowed",
            "headers": {
              "Allow": {
                "description": "List of supported methods for API.",
                "x-example": "GET",
                "type": "string",
                "pattern": "^[a-zA-Z]+$"
              },
              "service-transaction-id": {
                "description": "Request ID echoed back by server",
                "x-example": "rrt104eadn-21295-143945655-1-1444066988453",
                "type": "string",
                "pattern": "^[\\S]*$"
              }
            },
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Method Not Allowed",
                "userMessage": "Method not supported by this server.",
                "systemMessage": "405 Method Not Allowed",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "409": {
            "description": "Conflict",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Conflict",
                "userMessage": "Target resource in wrong state.",
                "systemMessage": "409 Conflict",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "412": {
            "description": "Precondition Failed",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Precondition Failed",
                "userMessage": "Target ressource is in unexpected state.",
                "systemMessage": "412 Precondition Failed",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Internal Server Error",
                "userMessage": "Unexpected error condition encountered.",
                "systemMessage": "500 Internal Server Error",
                "detailLink": "http://www.tmus.com"
              }
            }
          },
          "503": {
            "description": "Service Unavailable",
            "schema": {
              "$ref": "#/definitions/errors"
            },
            "examples": {
              "application/json": {
                "code": "Service Unavailable",
                "userMessage": "System is under maintenance.",
                "systemMessage": "503 Service Unavailable",
                "detailLink": "http://www.tmus.com"
              }
            }
          }
        },
        "parameters": [
          {
            "name": "toggle-key",
            "in": "path",
            "required": true,
            "description": "A string representing the token itself. This string will be twenty characters in length, composed of upper- and lower-case letters and digits.",
            "x-example": "Faskdjfhi1319827HJDa",
            "type": "string",
            "pattern": "^[a-zA-Z0-9]{20}$"
          },
          {
            "$ref": "#/parameters/AuthorizationParam"
          },
          {
            "$ref": "#/parameters/If-MatchParam"
          },
          {
            "$ref": "#/parameters/activityidParam"
          },
          {
            "$ref": "#/parameters/dateParam"
          },
          {
            "$ref": "#/parameters/If-Unmodified-SinceParam"
          },
          {
            "$ref": "#/parameters/xAuthParam"
          },
          {
            "$ref": "#/parameters/xAuthOrigParam"
          },
          {
            "$ref": "#/parameters/sessionidParam"
          },
          {
            "$ref": "#/parameters/interactionidParam"
          },
          {
            "$ref": "#/parameters/workflowidParam"
          },
          {
            "$ref": "#/parameters/timestampParam"
          },
          {
            "$ref": "#/parameters/servTransIdParam"
          }
        ],
        "security": [
          {
            "JWT": []
          }
        ]
      }
    }
  },
  "definitions": {
    "errors": {
      "type": "object",
      "required": [
        "code",
        "userMessage"
      ],
      "description": "As defined in http://api-standards.apps.px-npe01.cf.t-mobile.com/http/status-codes/error-response-format/",
      "properties": {
        "code": {
          "description": "Succinct, domain-specific, human-readable text string to identify the type of error for the given status code.",
          "example": "Product Not Found",
          "type": "string",
          "pattern": "^[\\S]+$"
        },
        "userMessage": {
          "description": "Human readable text that describes the nature of the error.",
          "example": "Product with the SKU 80275D could not be found",
          "type": "string",
          "pattern": "^[\\S]+$"
        },
        "systemMessage": {
          "description": "Text that provides a more detailed technical explanation of the error.",
          "example": "404 Not Found",
          "type": "string",
          "pattern": "^[\\S]*$"
        },
        "detailLink": {
          "description": "Link to custom information providing greater detail on error or errors.",
          "example": "http://www.tmus.com",
          "type": "string",
          "pattern": "^[\\S]*$"
        }
      }
    },
    "ToggleData": {
      "description": "Structure containing the toggledata",
      "type": "object",
      "properties": {
        "userData": {
          "description": "An arbitrary string that will be hidden behind the generated token. This string should have meaning to both the application generating the token and to the application validating the token. This string will be returned to the application(s) that successfully validate the token.",
          "type": "string"
        },
        "toggleKey": {
          "description": "A string representing the token itself. This string will be twenty characters in length, composed of upper- and lower-case letters and digits. This field will not be present in the event of a processing failure.",
          "type": "string",
          "pattern": "^[a-zA-Z0-9]{20}$"
        },
        "expirationTime": {
          "description": "The date and time that the token is set to expire. Note that this expiration is not necessarily the final expiration value, as it will be extended by each successful validation of self-renewing tokens. This field will not be present in the event of a processing failure.",
          "type": "string",
          "format": "date-time"
        }
      }
    }
  },
  "securityDefinitions": {
    "JWT": {
      "type": "apiKey",
      "description": "JWT is a means of representing claims to be transferred between two parties. The claims in a JWT are encoded as a JSON object that is digitally signed using JSON Web Signature (JWS) and/or encrypted using JSON Web Encryption (JWE).",
      "name": "Authorization",
      "in": "header"
    }
  },
  "parameters": {
    "AuthorizationParam": {
      "name": "Authorization",
      "description": "JWT access token with the authentication type set as Bearer.",
      "in": "header",
      "required": true,
      "x-example": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9",
      "type": "string",
      "pattern": "^[\\S]*$"
    },
    "ContentTypeParam": {
      "name": "Content-Type",
      "description": "Request message format.",
      "x-example": "application/json",
      "in": "header",
      "required": true,
      "type": "string",
      "pattern": "^[\\S]*$"
    },
    "activityidParam": {
      "name": "activity-id",
      "description": "Alphanumeric value generated and sent by the partner to identify every single service request uniquely.",
      "x-example": "XyzA:D1s5s1i-X6AFRvfiPBfs-1234",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "dateParam": {
      "name": "Date",
      "description": "The Date general HTTP header contains the date and time at which the message was originated.",
      "x-example": "Mon, 05 Mar 2018 16:38:08 GMT",
      "in": "header",
      "required": false,
      "type": "string",
      "format": "date-time",
      "pattern": "^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), [0-3]{1}[0-9]{1} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [1-3][0-9]{3} [0-2][0-9]:[0-6][0-9]:[0-6][0-9] GMT$"
    },
    "AcceptParam": {
      "name": "Accept",
      "description": "Content-Types that are acceptable for the response.",
      "x-example": "application/json",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "xAuthParam": {
      "name": "x-authorization",
      "description": "Contains Proof of Possession token generated by API caller",
      "x-example": "c34e7acd-384b-4c22-8b02-ba3963682508",
      "in": "header",
      "required": false,
      "type": "string",
      "pattern": "^[\\S]*$"
    },
    "xAuthOrigParam": {
      "name": "x-auth-originator",
      "description": "API chain initiating caller's IDP token",
      "x-example": "c34e7acd-384b-4c22-8b02-ba3963682508",
      "required": false,
      "in": "header",
      "type": "string",
      "pattern": "^[\\S]*$"
    },
    "sessionidParam": {
      "name": "session-id",
      "description": "Unique Id for an application user session.",
      "x-example": "XXX1:D1s5s1i-X6AFRvfiPBfs-2345",
      "in": "header",
      "required": false,
      "type": "string",
      "pattern": "^[\\S]*$"
    },
    "interactionidParam": {
      "name": "interaction-id",
      "description": "Alphanumeric value which represents a common transaction id across all calls made from the Partner application, to serve an individual customer.",
      "x-example": "YYY1:D1s5s1i-X6AFRvfiPBfs-83923",
      "in": "header",
      "required": false,
      "type": "string",
      "pattern": "^[\\S]*$"
    },
    "workflowidParam": {
      "name": "workflow-id",
      "description": "Name of business purpose/flow, for which the API is being invoked.",
      "x-example": "ACTIVATION",
      "in": "header",
      "required": false,
      "type": "string",
      "pattern": "^[\\S]*$"
    },
    "timestampParam": {
      "name": "timestamp",
      "description": "A timestamp provided by sender to track their workflow.",
      "x-example": 1557863635828,
      "in": "header",
      "required": false,
      "type": "integer",
      "format": "int64",
      "minimum": 0,
      "maximum": 9557863635828
    },
    "servTransIdParam": {
      "name": "service-transaction-id",
      "description": "Internal identifier for transaction tracking an individual transaction within API platform.",
      "in": "header",
      "required": false,
      "x-example": "rrt104eadn-21295-143945655-1-1444066988453",
      "type": "string",
      "pattern": "^[\\S]*$"
    },
    "If-MatchParam": {
      "name": "If-Match",
      "description": "An HTTP request header that makes the request conditional.",
      "x-example": "*",
      "in": "header",
      "required": false,
      "type": "string",
      "pattern": "^[\\S]*$"
    },
    "If-None-MatchParam": {
      "name": "If-None-Match",
      "description": "An HTTP request header that makes the request conditional.",
      "x-example": "*",
      "in": "header",
      "required": false,
      "type": "string",
      "pattern": "^[\\S]*$"
    },
    "If-Unmodified-SinceParam": {
      "name": "If-Unmodified-Since",
      "description": "An HTTP request header that makes the request conditional.",
      "x-example": "Mon, 05 Mar 2018 16:38:08 GMT",
      "in": "header",
      "required": false,
      "type": "string",
      "format": "date-time",
      "pattern": "^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), [0-3]{1}[0-9]{1} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [1-3][0-9]{3} [0-2][0-9]:[0-6][0-9]:[0-6][0-9] GMT$"
    },
    "If-Modified-SinceParam": {
      "name": "If-Modified-Since",
      "description": "An HTTP request header that makes the request conditional.",
      "x-example": "Mon, 05 Mar 2018 16:38:08 GMT",
      "in": "header",
      "required": false,
      "type": "string",
      "format": "date-time",
      "pattern": "^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), [0-3]{1}[0-9]{1} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [1-3][0-9]{3} [0-2][0-9]:[0-6][0-9]:[0-6][0-9] GMT$"
    },
    "acceptEncodeParam": {
      "name": "Accept-Encoding",
      "description": "HTTP request header which advertises which content encoding is supported by the client.",
      "x-example": "gzip",
      "in": "header",
      "required": false,
      "type": "string",
      "pattern": "^[\\S]&$"
    },
    "acceptLangParam": {
      "name": "Accept-Language",
      "description": "HTTP request header which advertises which language the client is able to understand.",
      "x-example": "EN",
      "in": "header",
      "required": false,
      "type": "string",
      "pattern": "^[\\S]*$"
    }
  }
}