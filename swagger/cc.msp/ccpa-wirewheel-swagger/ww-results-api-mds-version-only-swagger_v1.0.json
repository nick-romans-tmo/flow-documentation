{
	"swagger": "2.0",
	"info": {
		"description": "WireWheel task results API for T-Mobile",
		"version": "1.0.0",
		"title": "DSR Result Upload API",
		"contact": {
			"name": "Ed Peters",
			"email": "ed@wirewheel.com"
		},
		"license": {
			"name": "UNLICENSED"
		}
	},
	"host": "ww-tmobile-dev-982895215.us-east-1.elb.amazonaws.com",
	"basePath": "/wwapi/v1",
	"x-servers": [{
		"url": "https://ww-tmobile-dev-982895215.us-east-1.elb.amazonaws.com/results",
		"description": "Dev Server"
	}],
	"tags": [{
		"name": "DSR Result Upload API",
		"description": "Upload File and responseMessage publish API"
	}],
	"schemes": ["https"],
	"paths": {
		"/results": {
			"get": {
				"security": [{
					"Oauth": []
				}],
				"tags": ["WireWheel get API"],
				"operationId": "getMetaData",
				"x-api-pattern": "QueryCollection",
				"summary": "Get metadata about previously submitted requests",
				"description": "This is to retrieve requestId and appId for wirewheel",
				"parameters": [{
					"in": "query",
					"name": "requestId",
					"type": "string",
					"description": "the ID of the request of interest",
					"required": true,
					"x-example": "46372abcjU",
					"pattern": "^[A-Za-z0-9-_]+$",
					"minLength": 1,
					"maxLength": 255
				},
				{
					"in": "query",
					"name": "appId",
					"type": "string",
					"description": "the ID of the app of interest",
					"required": true,
					"x-example": "46372",
					"pattern": "^[A-Za-z0-9-_]+$",
					"minLength": 1,
					"maxLength": 255
				}],
				"responses": {
					"200": {
						"description": "Successful operation.",
						"schema": {
							"$ref": "#/definitions/Metadata"
						},
						"examples": {
							"lastModified": "2019-11-11T20:23:36.000Z",
							"size": 1894
						}
					},
					"400": {
						"description": "Bad Request",
						"schema": {
							"$ref": "#/definitions/Error"
						},
						"examples": {
							"application/json": {
								"statusCode": "400",
								"errors": ["missing required field 'appId'"]
							}
						}
					},
					"401": {
						"description": "Unauthorized",
						"schema": {
							"$ref": "#/definitions/Error"
						},
						"examples": {
							"application/json": {
								"statusCode": "401",
								"errors": ["Unauthorized"]
							}
						}
					},
					"403": {
						"description": "Forbidden",
						"schema": {
							"$ref": "#/definitions/Error"
						},
						"examples": {
							"application/json": {
								"statusCode": "403",
								"errors": ["Forbidden"]
							}
						}
					},
					"404": {
						"description": "Not Found",
						"schema": {
							"$ref": "#/definitions/Error"
						},
						"examples": {
							"application/json": {
								"statusCode": "404",
								"errors": ["Not Found"]
							}
						}
					},
					"500": {
						"description": "Internal Server Error",
						"schema": {
							"$ref": "#/definitions/Error"
						},
						"examples": {
							"application/json": {
								"statusCode": "500",
								"errors": ["failure reading metadata"]
							}
						}
					}
				}
			},
			"post": {
				"security": [{
					"Oauth": []
				}],
				"tags": ["DSR Result Upload API"],
				"operationId": "publishResults",
				"x-api-pattern": "ExecuteFunction",
				"summary": "Submit take status and (optional) results",
				"description": "This is to submit response message and/or upload file to wirewheel",
				"consumes": ["multipart/form-data"],
				"parameters": [{
					"in": "formData",
					"name": "responseMessage",
					"description": "JSON status",
					"required": true,
					"type": "string"
				},
				{
					"in": "formData",
					"name": "uploadFile",
					"type": "file",
					"description": "Results, if any are available"
				}],
				"responses": {
					"204": {
						"description": "Successful operation."
					},
					"400": {
						"description": "Bad Request",
						"schema": {
							"$ref": "#/definitions/Error"
						},
						"examples": {
							"application/json": {
								"statusCode": "400",
								"errors": ["missing required field 'appId'"]
							}
						}
					},
					"401": {
						"description": "Unauthorized",
						"schema": {
							"$ref": "#/definitions/Error"
						},
						"examples": {
							"application/json": {
								"statusCode": "401",
								"errors": ["Unauthorized"]
							}
						}
					},
					"403": {
						"description": "Forbidden",
						"schema": {
							"$ref": "#/definitions/Error"
						},
						"examples": {
							"application/json": {
								"statusCode": "403",
								"errors": ["Forbidden"]
							}
						}
					},
					"404": {
						"description": "Not Found",
						"schema": {
							"$ref": "#/definitions/Error"
						},
						"examples": {
							"application/json": {
								"statusCode": "404",
								"errors": ["Not Found"]
							}
						}
					},
					"500": {
						"description": "Internal Server Error",
						"schema": {
							"$ref": "#/definitions/Error"
						},
						"examples": {
							"application/json": {
								"statusCode": "500",
								"errors": ["failure streaming content to storage"]
							}
						}
					}
				}
			}
		}
	},
	"definitions": {
		"responseMessage": {
			"type": "object",
			"required": ["requestId",
			"appId",
			"processingStatus"],
			"description": " DSR subtask status",
			"properties": {
				"requestId": {
					"type": "string",
					"description": "Unique Request Id that is sent in DSR request by Wire wheeel",
					"example": "9877jhhh39",
					"format": "string",
					"minLength": 6,
					"maxLength": 128
				},
				"appId": {
					"type": "string",
					"description": "App Id is a unique identifier for the application.",
					"example": "28484",
					"format": "string",
					"minLength": 6,
					"maxLength": 11
				},
				"processingStatus": {
					"type": "string",
					"enum": ["Success",
					"Failed"],
					"description": "Status of the DSR request processing",
					"example": "Success"
				},
				"processStartedDateTime": {
					"type": "string",
					"description": "UTC Date and Time of the DSR Request process started",
					"format": "date-time",
					"example": "2019-08-19T16:20:11.108Z"
				},
				"processEndedDateTime": {
					"type": "string",
					"description": "UTC Date and Time of the DSR Request process ended",
					"format": "date-time",
					"example": "2019-08-30T17:00:11.108Z"
				},
				"statusInfo": {
					"type": "string",
					"description": "If the status is Failed - Reason for failure of the DSR request processing, if the records not found then processingStatus is Success and the statusInfo is Not found",
					"example": "System error",
					"format": "string"
				}
			}
		},
		"Error": {
			"type": "object",
			"required": ["name",
			"code",
			"message"],
			"properties": {
				"name": {
					"description": "Generic name for the error type",
					"example": "BadRequest",
					"type": "string",
					"pattern": "^[\\S ]+$"
				},
				"code": {
					"description": "HTTP status code",
					"example": 400,
					"type": "number",
					"pattern": "^[\\d ]+$"
				},
				"message": {
					"description": "More descriptive message",
					"example": "missing required part 'status'",
					"type": "string",
					"pattern": "^[\\S ]+$"
				},
				"errors": {
					"description": "Messages giving more detail",
					"example": "Bad Request",
					"type": "array",
					"items": {
						"type": "string"
					}
				}
			}
		},
		"Metadata": {
			"type": "object",
			"required": ["lastModified",
			"size"],
			"properties": {
				"lastModified": {
					"description": "Timestamp when the data was last uploaded",
					"type": "string",
					"format": "date-time",
					"example": "Last Modified timestamp"
				},
				"size": {
					"description": "Size (in bytes)",
					"type": "number",
					"pattern": "^[\\d ]+$",
					"example": 320
				}
			}
		}
	},
	"securityDefinitions": {
		"Oauth": {
			"type": "apiKey",
			"description": "Must be an ID or Access token issued by Wire Wheeel token url. JWT is a means of representing claims to be transferred between two parties. The claims in a JWT are encoded as a JSON object that is digitally signed using JSON Web Signature (JWS) and/or encrypted using JSON Web Encryption (JWE).",
			"name": "Authorization",
			"in": "header"
		}
	},
	"security": [{
		"Oauth": []
	}]
}