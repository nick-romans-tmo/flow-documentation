swagger: '2.0'
info:
  description: This is a swagger for DSR Response Producer API
  version: 1.0.0
  title: DSR Response Producer API
  termsOfService: 'https://tmobileusa.sharepoint.com/teams/da/DM/CCPA/SitePages/CCPA%20Program.aspx'
  contact:
    name: Devendar Kotha
    email: P&TChannelsTeam@T-Mobile.com
  license:
    name: Apache 2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
x-servers:
  - url: 'https://tmobilea-sb02.apigee.net/mdsapi/v1/publishResponse'
    description: Dev Server
  - url: TBD
    description: Test Server
tags:
  - name: DSR Response Producer
    description: DSR Response Producer API
host: tmobilea-sb02.apigee.net
basePath: /mdsapi/v1
schemes:
  - https
paths:
  /publishResponse:
    post:
      tags:
        - DSR Response Producer
      summary: Submit response message and/or upload file
      description: This is to submit response message and/or upload file to wirewheel
      operationId: publishResponse
      x-api-pattern: CreateInCollection
      consumes:
        - multipart/form-data
      parameters:
        - in: formData
          name: uploadFile
          type: file
          description: 'The file to upload, file should be .json format and prefixed with appId_requestId_'
        - in: formData
          name: responseMessage
          type: string
          description: Json String to upload
          required: true
      responses:
        '200':
          description: Successful operation.
          examples:
            application/json:
              code: '200'
              userMessage: OK
              systemMessage: status ok
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad Request
              systemMessage: Invalid_Field_Name
          headers: {}
        '401':
          description: Unauthorized
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '401'
              userMessage: Unauthorized
              systemMessage: Unauthorized
          headers: {}
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '403'
              userMessage: Forbidden
              systemMessage: Forbidden
          headers: {}
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '404'
              userMessage: Not Found
              systemMessage: Not Found
          headers: {}
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '500'
              userMessage: Internal Server Error
              systemMessage: Internal Server Error
          headers: {}
      security:
        - Oauth: []
definitions:
  responseMessage:
    type: object
    required:
      - requestId
      - requestType
      - appId
      - processingStatus
    description: ' DSR Response message Object'
    properties:
      requestId:
        type: string
        description: Unique Request Id that is sent in DSR request by Wire wheeel
        example: 9877jhhh39
        format: string
      requestType:
        type: string
        enum:
          - Access
          - Delete
        description: Type of DSR Request
        example: Delete
      processStartedDateTime:
        type: string
        description: UTC Date and Time of the DSR Request process started
        format: date-time
        example: '2019-08-19T16:20:11.108Z'
      processEndedDateTime:
        type: string
        description: UTC Date and Time of the DSR Request process ended
        format: date-time
        example: '2019-08-30T17:00:11.108Z'
      appId:
        type: string
        description: App Id is a unique identifier for the application.
        example: '28484'
        format: string
      processingStatus:
        type: string
        enum:
          - Success
          - Failed
        description: Status of the DSR request processing
        example: Success
      statusInfo:
        type: string
        description: 'If the status is Failed - Reason for failure of the DSR request processing, if the records not found then processingStatus is Success and the statusInfo is Not found'
        example: System error
      deletedRecordCount:
        type: string
        description: Number of records deleted in the application
        example: '23'
  Error:
    type: object
    description: 'As defined in http://api-standards.apps.px-npe01.cf.t-mobile.com/http/status-codes/error-response-format/'
    required:
      - code
      - userMessage
    properties:
      code:
        description: HTTP status code
        example: '400'
        type: string
        pattern: '^[\S ]+$'
      userMessage:
        description: A human-readable message describing the error.
        example: Bad Request
        type: string
        pattern: '^[\S ]+$'
      systemMessage:
        description: Text that provides a more detailed technical explanation of the error
        example: Invalid eventDateTime
        type: string
securityDefinitions:
  Oauth:
    type: oauth2
    description: |
      When you sign up for an account, you are given your first API key.
      To do so please follow this link: https://www.t-mobile.com/site/signup/
      Also you can generate additional API keys, and delete API keys (as you may
      need to rotate your keys in the future).
      Token can change to a JwT and ID and PoP tokens.
    tokenUrl: 'https://host:env/v1/oauth2/accesstoken?grant_type=client_credentials'
    flow: application
    scopes: {}
security:
  - Oauth: []
externalDocs:
  description: Find out more about DSR Response Producer API service
  url: 'https://tmobileusa.sharepoint.com/teams/da/DM/CCPA/SitePages/CCPA%20Program.aspx'
