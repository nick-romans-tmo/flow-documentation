swagger: "2.0" # see: https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md

info:
  description: "A microservice providing a view of all Plans on an Account, including RatePlans, Equipment Installation \
                Plans and Lease Plans. This details what the subscriber can expect for reoccurring Charges, Discounts \
                and Credits for Plans, Equipment and AddOns."

  version: "0.0.4" # Fourth revision of Draft
            # Changes from 0.0.4
            # - Removed Line.autopaystatus
            # - Added Line.Type
            # - Restored Account.accountEIPS - (If a line is cancelled, but the customer still owes for the equipment, it will be assigned at the Account level!)
            # - Added Account.number - (for change plan)

            # Changes from 0.0.3
            # - Removed Account.accountEIPs (didn't match backend reality)
            # - Correct account.ratePlans to be an array
            # - Line.equipmentOnNetwork changed to IMEI string from Equipment object
            # - Added more Inline comments to add clarity
            # - Added to Line: statusCode, statusReasonCode
            # - Added Account: type, subtype, statusCode, statusReasonCode, autoPayEnabled
            #
            # Changes from 0.0.2:
            # - Cleaned up documentation on Account level sums descriptions
            # - renamed ratePlanSums to allRatePlansSums for consistency/clarity.
            # - Added missing Line.sums and Line.sumsWithAAL

  title: "AccountPlans"

host: "localhost"
basePath: "/v1/"
schemes:
 - https # Sensitive data in transit must be encrypted.

paths:
  /accountPlans:
    get:
      description: "Returns recurring Charges, Discounts and Credits for Plans, Services, Devices, AddOns for each line \
        \ of an account."
      operationId: "subscriptions"
      produces:
      - "application/json"
      parameters:

# The following are the Digital Axiom requirements for responses from this tier:
#   GUID Metadata – All data in transit or at creation will have Time & Date (ISO 8601), Source System ID, Transaction ID, API, and Service or Event ID​

# The following must be logged to Splunk
# - trace: Zipkin Trace ID
# - span: Zipkin Span ID
# - origination_source_app_id: Origin Source App ID
# - akm_id: AKM ID # Application ID as defined in EDS Master Data Model
# - application_id: Application ID for calling application
# - channel_id: Channel ID
# - client_id: Client ID : How does this differ from AKM & Application ID?
# - op_code: OpCode : indicates sub operation for the ervice operation being called
# - transaction_type: Transaction Type: The flow or activity this service operation is supporting
# - trans_timestamp: field in the microservice log to be populated in Unix Epoch Milliseconds format the moment the
#                    service executes a response back to the requesting client so that data analysis can be done at the
#                    millisecond level with minimum processing required on BI systems

# - user ids: indicate the user that is navigating the front end UI and invoking the services so that we may be able to analyze customer behavior.
# - status codes: status code and values to indicates the flow or activity that the service operation is supporting
# - latency: a positive integer that indicates the time between receipt of the request and submission of the response in milliseconds
# - (must log requests & responses in a specific format)
#  ...all logs to be generated immediately following the submission of the response back to the client and sent to a designated S3 bucket for processing
#  ...specific fields that relate to PII/CPNI data in the request or response payload to be filtered from the log for privacy and security reasons
#  ...utilize Zipkin trace and span ids for telemetry and flow analysis
#
# THESE ARE STANDARD EOS HEADERS
#      - name: "X-B3-TraceId"
#        in: "header"
#        description: "This is interactionId or correlationId equivalent that is required to
#        passed to MW/Backend. If request is from app, they also pass this to a
#        web-view request as X-B3-TraceId header."
#        required: true
#        type: "string"
#      - name: "Authorization"
#        in: "header"
#        description: "Authorization Header"
#        required: true
#        type: "string"
#      - name: "X-B3-SpanId"
#        in: "header"
#        description: "App need to pass X-B3-SpanId in header"
#        required: true
#        type: "string"
      - name: "application_id"
        in: "header"
        description: "TMOId associated with IAM account, must be Base64 encoded."
        required: true
        type: "string"
      - name: "channel_id"
        in: "header"
        description: "Should be enum and fail if any other value is passed. API should respond with error and meaningful message"
        required: true
        type: "string"
      - name: "usn"
        in: "header"
        description: "Unique session id for user. It will be returned by IAM. (CIS requirement)"
        required: false
        type: "string"
      - name: "user-token"
        in: "header"
        description: "JWT token, this service will use this token to ensure only information relevant to this users account is returned."
        required: false
        type: "string"
#
# These fields are sensitive information.
#
#      - name: "tmo-id"
#        in: "header"
#        description: "TMOID associated with IAM account, must be Base64 encoded."
#        required: false
#        type: "string"
#      - name: "BAN"
#        in: "path"
#        description: "Billing/financial account number, Base64 Encoded."
#        required: true
#        type: "string"
#      - name: "msisdn"
#        in: "header"
#        description: "MSISDN, must be Base64-encoded."
#        required: false
#        type: "string"
      - name: "application_client"
        in: "header"
        description: "This will be the unique identifier that is used with the OS Store [user-agent]"
        required: false
        type: "string"
      - name: "application_version_code"
        in: "header"
        description: "This is the App Client version number [user-agent]"
        required: false
        type: "string"
      - name: "device_os"
        in: "header"
        description: "The operating system of the mobile device [user-agent]"
        required: false
        type: "string"
      - name: "os_version"
        in: "header"
        description: "The operating system version of the mobile device [user-agent]"
        required: false
        type: "string"
      - name: "os_language"
        in: "header"
        description: "The language of the user's device operating system [user-agent]"
        required: false
        type: "string"
      - name: "device_manufacturer"
        in: "header"
        description: "The manufacturer of the user's device [user-agent]"
        required: false
        type: "string"
      - name: "device_model"
        in: "header"
        description: "The model of the user's device [user-agent]"
        required: false
        type: "string"
      - name: "marketing_cloud_id"
        in: "header"
        description: "The adobe id for the user"
        required: false
        type: "string"
      - name: "sd_id"
        in: "header"
        description: "another adobe id"
        required: false
        type: "string"
      - name: "adobe_uuid"
        in: "header"
        description: "A unique adobe uuid that is used with Adobe partners"
        required: false
        type: "string"
      - name: "role"
        in: "header"
        description: "Access level of currently logged in user (Primary Account Holder, Full Access, Restricted, Standard)"
        required: false
        type: "string"
      responses:
        200:
          description: "The request was successful"
          schema:
            $ref: "#/definitions/Account"
        304:
          description: "Not modified" # Client supplied 'if-none-match' version for object it holds, object hasn't changed
        400:
          description: "Bad request error"
        404:
          description: "Record not found"
        500:
          description: "The server encountered an unexpected condition which prevented it from fulfilling the request."

definitions:

  Sums:
    description: "Sums of listPrices, discounts and netPrices."
    type: object
    properties:
      listPriceSums:
        description: "The sum of list prices of all values in the map (a derived value)."
        type: integer
        format: float
      discountSums:
        description: "The sum of discounts of all values in the map (a derived value)."
        type: integer
        format: float
      netPriceSums:
        description: "The sum of net prices of all values in the map (a derived value)."
        type: integer
        format: float

  MarketingFeature:
    type: object
    description: "A Rate plan."
    properties:
      description:
        type: "string"
        description: "Hover Text for capability."
      richDescription:
        type: "string"
        description: "Richly formatted HTML text from AEM."
      image:
        type: "string"
        description: "URL to image for capability."
      icon:
        type: "string"
        description: "URL to icon for capability."
      tags:
        description: "Tags/categories for capabilities for product comparisons."
        type: array
        items:
          $ref: '#/definitions/Tag'

  Tag:
    type: object
    description: "An tag attribute of a MarketingFeature."
    properties:
      name:
        type: "string"
      value:
        type: "string"

  Product:
    description: "A Product serves as the base class for all things sold by T-Mobile."
    type: "object"
    properties:
      code:
        description: "Product code, SOC code, Discount code, UPC, SKU, etc."
        type: "string"
      name:
        description: "The name to show for the product."
        type: "string"
      fullImage:
        description: "A URL pointing to a full image of the product."
        type: "string"
      croppedImage:
        description: "A URL pointing to a cropped image of the product."
        type: "string"
      listPrice:
        description: "The list price of the product."
        type: number
        format: float
      discount:
        description: "Any discount associated with this product."
        type: number
        format: float
      netPrice:
        description: "Net Price is the List price less any Discounts associated with this product."
        type: number
        format: float
      isAvailable: # Not required this phase, needed in next phase
        description: "Is the product currently available for Sale (i.e. not grandfathered)."
        type: boolean
      toolTip:
        description: "Any tooltip to be associated with the display of the product."
        type: "string"
      features:
        description: "Specific product features allowing a feature for feature comparison of products."
        type: array
        items:
          $ref: "#/definitions/MarketingFeature"
    required:
      - code
      - name
      - listPrice
      - discount

  Equipment:
    type: object
    description: "Any equipment or device a Subscriber may use with, purchase, lease, or loan from, T-Mobile."
    allOf:
      - $ref: '#/definitions/Product'
    properties:
      imei:
        description: "The IMEI corresponding to the Equipment, when applicable (financed accessories may not have IMEIs)."
        type: string

  AddOn:
    description: "An AddOn service product, including 'Data Plan' services."
    # source: ServiceAgreement
    # Look to ServiceAgreement.serviceType to determine if this is an AddOn, Service, or Rateplan
    type: object
    allOf:
      - $ref: '#/definitions/Product'

  Discount:
    description: "Discounts."
    # source: ServiceAgreement
    # Look to ServiceAgreement.serviceType to determine if this is an AddOn, Service, or Rateplan
    type: object
    properties:
      name:
        description: "The name of the discount"
        type: string
      amount:
        description: "The amount of the discount."
        type: number
        format: float

  EquipmentFinanceStatus:
    description: "The status of financed equipment"
    properties:
      originalEIPAmount:
        description: "The List Price of this Equipment as returned from EIP."
        type: number
        format: float
      monthlyInstallment:
        description: "The agreed upon monthly installment for this Equipment."
        type: number
        format: float
      remainingBalance:
        description: "The remaining balance on the financed Equipment."
        type: number
        format: float
      remainingInstallments:
        description: "The number of installments remaining."
        type: integer
        format: int32
      equipmentId:
        description: "The IMEI of the Equipment this FinanceStatus is associated with (or UPC if no IMEI)."
        type: string

  EquipmentInstallmentPlan:
    description: "An EquipmentInstallationPlan defines the terms under which Equipment will be loaned (Jump) or leased (Jump-On-Demand) to a Subscriber."
    properties:
      eipId:
        description: "The ID of this EquipmentInstallmentPlan."
        type: integer
        format: int32
      msisdn:
        description: "The MSISDN of the Line this EquipmentInstallmentPlan is associated with."
        type: string
      planStatus:
        enum:
          - ACTIVE
          - CLOSED
      financeType:
        enum:
          - LOANED
          - LEASED
          - POIP # No current requirement identified
      financeStatuseSums:
        description: "The total listPrice, discounts and net prices for all equipment financed under this Plan"
        $ref: '#/definitions/Sums'
      financeStatuses:
        description: "The finance status of each piece of equipment financed under this Plan"
        type: array
        $ref: '#/definitions/EquipmentFinanceStatus'

  RatePlan:
    description: "A RatePlan defines the terms under which Service will be provided to a Subscriber."
    # Look to ServiceAgreement.serviceType to determine if this is an AddOn, Service, or Rateplan
    type: object
    allOf:
      - $ref: '#/definitions/Product'
    properties:
      title: # Isn't this a product.name?
        # Look to MPM first, if missing use name coming back from Samson
        description: "Customer-friendly plan name (voice plan only)."
      type:
        description: "The type of rate plan, GSM (voice) or MBB (internet)."
        # SOC table w/in ServiceAgreement
        enum:
          - GSM
          - MBB
      subtype:
        # source: GetSubscriber
          enum:
          # TBD Complete this next iteration
            - NONE # None is applicable to GSM RatePlans
            - IoT
      startDate:
        # source: GetServiceAgreement
        description: "Effective start date for the RatePlan"
        type: integer
        format: 'date-time'
      endDate:
        # source: GetServiceAgreement
        description: "Effective end date for the RatePlan."
        type: integer
        format: 'date-time'
      tagLine:
        # source: New request to MPM
        description: "A Tag line for the Plan." # e.g. "One price. Not a Penny more."
        type: "string"
      lineMISDNs:
        # source: GetServiceAgreement
        description: "The MSISDNs for Lines associated with this RatePlan."
        type: array
        items:
          type: string
      planSums:
        # source: Derived
        description: "The RatePlan base rate plus all Add-a-Line - List Price, Discount and Net price Sums."
        type: object
        $ref: '#/definitions/Sums'
      taxExclusive:
        description: "Is this a tax exclusive plan?"
        type: boolean
    required:
      - planSums
      - taxExclusive

  Line:
    description: "A Line of service (Active, Suspended & Blocked, but not Canceled)"
    # source: ServiceAgreement
    type: object
    properties:
      statusCode:
        description: "The StatusCode for the Line."
        type: "string"
      statusReasonCode:
        description: "The Reason for the StatusCode on the Line."
        type: "string"
      type:
        # phase: PlansOverview - UI's need it for ordering lines
        description: "The Type of the Line."
        type: string
      msisdn:
        # source: ServiceAgreement - called MobileNumber
        description: "The MSISDN for the line."
        type: "string"
      firstNickname:
        # source: Not in ServiceAgreement, in getSubscriber?, Possibly CHUB in Phase 2
        description: "The subscriber assigned first name for the line."
        type: "string"
      lastNickname:
        # source: Not in ServiceAgreement, in getSubscriber?, Possibly CHUB in Phase 2
        description: "The subscriber assigned last name for the line."
        type: "string"
      ratePlanSOC:
      # source: ServiceAgreement
        description: "The SOC code of the RatePlan associated with this line."
        type: string
      aalPrice:
        # source: ServiceAgreement
        description: "The cost associated with this line, set to 0 if included with plan."
        type: number
        format: float
      equipmentOnNetwork:
        description: "The IMEI of Equipment assigned on this Line of the network."
        # source: EIR, mapped to IMEI open for august release
        type: string
      eips:
        # source: EIP
        description: "An array of EquipmentInstallmentPlans associated with this line."
        type: array
        items:
          $ref: '#/definitions/EquipmentInstallmentPlan'
      eipSums:
        # source: Derived, multiple EIP on one line
        description: "Plan/Account Level EquipmentInstallmentPlans Sums (list price, discounts, net prices)."
        type: object
        $ref: '#/definitions/Sums'
      addOns:
        # source: GEtServiceAgreement
        description: "All AddOns applied to this Line."
        type: array
        items:
          $ref: '#/definitions/AddOn'
      addOnsSums:
        # source: Derived
        description: "The Sums of all AddOns on this Line."
        type: object
        $ref: '#/definitions/Sums'
      discounts:
        description: "All discounts applied to this Line  (e.g. Autopay, free add-a-line, Employee, Kickback)."
        type: array
        items:
          $ref: '#/definitions/Discount'
      discountSums:
        # source: Derived
        description: "Sums (list prices, discounts, net prices) from all Discounts applied to this Line."
        type: object
        $ref: '#/definitions/Sums'
      sums:
        description: "Sums (list prices, discounts, net prices) for all EIPs and AddOns for this Line."
        type: object
        $ref: '#/definitions/Sums'
      sumsWithAAL:
        description: "Sums (list prices, discounts, net prices) for all EIPs, AddOns _and_ Add-a-Line charges for this Line."
        type: object
        $ref: '#/definitions/Sums'
    required:
      - msisdn
      - eipSums
      - addOnsSums
      - discountSums
      - sums
      - sumsWithAAL

  Account:
    description: "The Account of the Primary Account Holder."
    properties:
      number:
        # phase: ChangePlan
        description: "The account identifier, a.k.a BAN."
        type: string
      type:
        # phase: ChangePlan
        description: "The Type of Account."
        type: string
      subtype:
        # phase: ChangePlan
        description: "The SubType of the Account."
        type: string
      statusCode:
        # phase: ChangePlan
        description: "The Reason for the Account StatusCode."
        type: string
      statusReasonCode:
        # phase: ChangePlan
        description: "The Reason for the Account StatusCode."
        type: string
      autoPayEnabled:
        # phase: ChangePlan
        description: "True if Autopay has been enabled on this Account."
        type: boolean
      lines:
        # source: ServiceAgreement
        description: "All active & suspended lines on an Account (but not cancelled)."
        type: array
        $ref: '#/definitions/Line'
      equipment:
        description: "All Equipment on the Account"
        # source:
        # Equipment instances here will be populated from:
        # - For equipment on network: EIR
        # - For accessories equipment device details sku, imei etc: Source EIP
        type: array
        $ref: '#/definitions/Equipment'
      ratePlans:
        # source: ServiceAgreement
        description: "All RatePlans on the Account."
        type: array
        $ref: '#/definitions/RatePlan'
      allRatePlanSums:
        # source: ServiceAgreement
        description: "The rollup of list prices, discounts and net prices of all RatePlans on the Account."
        type: object
        $ref: '#/definitions/Sums'
      allEIPSums:
        # source: Derived
        description: "Sums for all EquipmentInstallmentPlans on the account."
        type: object
        $ref: '#/definitions/Sums'
      accountDiscounts:
        description: "Account-level only discounts (e.g. AutoPay, Employee, Kickback)."
        type: array
        $ref: '#/definitions/Discount'
      accountDiscountSums:
        # source: Derived
        description: "Sum of Account-level only discounts, the list price and net price will show the impact of applying the discount."
        type: object
        $ref: '#/definitions/Sums'
      accountAddOns:
        description: "Account-level only AddOns (e.g. Netflix, Detail Billing, Family Allowance)."
        type: array
        $ref: '#/definitions/AddOn'
      accountAddOnsSums:
        # source: Derived
        description: "Sums for all Account-level only AddOns."
        type: object
        $ref: '#/definitions/Sums'
      accountEIPS:
        # source: EIP
        # NOTE: If a line is cancelled, but the customer still owes for the equipment, it will be assigned at the Account level!
        description: "An array of EquipmentInstallmentPlans associated with this line."
        type: array
        items:
          $ref: '#/definitions/EquipmentInstallmentPlan'
      accountEIPSums:
        # source: Derived
        description: "Account-level only EquipmentInstallmentPlans Sums (list price, discounts, net prices)."
        type: object
        $ref: '#/definitions/Sums'
      sums:
        # source: Derived
        description: "The total Sums for the entire Account."
        type: object
        $ref: '#/definitions/Sums'