{
  "swagger": "2.0",
  "basePath": "/deep/v1",
  "schemes": [
    "http"
  ],
  "info": {
    "title": "Event",
    "description": "This API is for DEEP - Subscriber Finance Change Events",
    "contact": {},
    "version": "1"
  },
  "paths": {
    "/events/{eventtype}": {
      "post": {
        "tags": [
          "Event"
        ],
        "consumes": [
          "application/json",
          "application/xml"
        ],
        "produces": [
          "application/json",
          "application/xml"
        ], 
        "responses": {
          "200": {
            "description": "no description",
            "schema": {
              "maxItems": 1,
              "type": "string"
            }
          },
          "400": {
            "description": "Bad Request"
          },
          "500": {
            "description": "Service Unavailable"
          }
        },
        "operationId": "Event",
        "parameters": [
          {
            "name": "eventtype",
            "in": "path",
            "required": true,
            "description": "Unique name of the event",
            "type": "string"
          },
          {
            "name": "event",
            "in": "body",
            "description": "Event Complex Element",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Event"
            }
          }
        ],
        "summary": "Event"
      }
    }
  },
  "tags": [
    {
      "name": "Event"
    }
  ],
  "definitions": {
    "auditInfo": {
      "type": "object",
      "properties": {
        "customerId": {
          "description": "Uniquely identifies the Customer.",
          "type": "string"
        },
        "accountNumber": {
          "description": "The financial account number.",
          "type": "string"
        },
        "universalLineId": {
          "description": "ULID - Universal identifier of the LineofService, as defined in Customer Hub. ",
          "type": "string"
        },
        "lineId": {
          "description": "Uniquely identifies a  line of service.",
          "type": "string"
        },
        "phoneNumber": {
          "description": "The phone number (MSISDN) associated with the line of service.",
          "type": "string"
        },
        "iamUniqueId": {
          "description": "Unique identifier for Identity and Access Management.",
          "type": "string"
        },
        "batchId": {
          "description": "Identifer of batch job.",
          "type": "string"
        },
        "orderId": {
          "description": "Identifier of order.",
          "type": "string"
        }
      }
    },
    "headerReference": {
      "required": [
        "timestamp",
        "channelId",
        "applicationId",
        "senderId"
      ],
      "type": "object",
      "properties": {
        "activityId": {
          "type": "string"
        },
        "applicationId": {
          "type": "string"
        },
        "applicationUserId": {
          "type": "string"
        },
        "channelId": {
          "type": "string"
        },
        "dealerCode": {
          "type": "string"
        },
        "interactionId": {
          "type": "string"
        },
        "segmentationId": {
          "type": "string"
        },
        "senderId": {
          "type": "string"
        },
        "sessionId": {
          "type": "string"
        },
        "storeId": {
          "type": "string"
        },
        "terminalId": {
          "type": "string"
        },
        "tillId": {
          "type": "string"
        },
        "workflowId": {
          "type": "string"
        },
        "authCustomerId": {
          "type": "string"
        },
        "authFinancialAccountId": {
          "type": "string"
        },
        "authLineOfServiceId": {
          "type": "string"
        },
        "timestamp": {
          "type": "string"
        },
        "masterDealerCode": {
          "type": "string"
        },
        "sourceSystemCode": {
          "type": "string"
        }
      }
    },
    "Specifications": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string",
          "example": "null"
        },
        "value": {
          "type": "string",
          "example": "null"
        }
      },
      "description": "Name value pair."
    },
    "Payload": {
      "required": [
        "subscriberNumber",
        "sourceTransactionTime"
      ],
      "type": "object",
      "properties": {
        "subscriberNumber": {
          "description": "Subscriber Phone Number",
          "type": "string"
        },
        "sourceTransactionTime": {
			"type": "string",
			"format": "date-time",
			"example": "2018-08-28T16:20:11.108Z or 2018-02-08T12:30:52-05:00",
			"description": "EIP Event Date Time - Original EIP event datetime for which this new event was trigerred. This date time will be different from event date time. For evaulating different scenarios this eventDateTime needs to be considered."
        },
        "financeInfo": {
	        "minItems": 0,
	        "description": "Financing Information.",
	        "type": "array",
	        "items": {
	            "$ref": "#/definitions/FinanceInfo"
	        }
        }
      }
    },
    "FinanceInfo": {
    	"required": [
        	"accountNumber"
      	],
		"description": "Active Finance Details for the subscriber.",
		"type": "object",
		"properties": {
		    "accountNumber": {
				"description": "Billing Account Number",
				"type": "string"
			},
		    "activeLoanInfo": {
				"minItems": 0,
				"description": "Loan Financing Details.",
				"type": "array",
				"items": {
					"$ref": "#/definitions/ActiveLoanInfo"
				}
			}
		}
    },
    "ActiveLoanInfo": {
    	"required": [
        	"equipmentType",
			"activeCount"
      	],
		"description": "Active Finance Details for the subscriber.",
		"type": "object",
		"properties": {
		    "equipmentType": {
				"description": "Billing Account Number",
				"type": "string"
			},
		    "activeCount": {
				"description": "Number of Active Loan Information.",
				"type": "integer",
                "format": "int32"
			}
		}
    },
    "Event": {
      "required": [
        "eventProducerId",
        "eventTime",
        "eventType",
        "payload"
      ],
      "type": "object",
      "properties": {
        "eventId": {
          "description": "Identifier of the event",
          "type": "string"
        },
        "eventType": {
          "description": "Type of event",
          "type": "string"
        },
        "eventTime": {
          "description": "The date and time that the event occurred.",
          "type": "string",
          "format": "date-time"
        },
        "eventProducerId": {
          "description": "Identifier of the event producer",
          "type": "string"
        },
        "eventVersion": {
          "description": "Version of the event",
          "type": "string"
        },
        "auditInfo": {
          "$ref": "#/definitions/auditInfo"
        },
        "payload": {
          "$ref": "#/definitions/Payload"
        },
        "headerReference": {
          "$ref": "#/definitions/headerReference"
        },
        "specifications": {
          "type": "array",
          "example": "null",
          "description": "added for future reference so can ignore.",
          "items": {
            "$ref": "#/definitions/Specifications"
          }
        }
      }
    }
  }
}