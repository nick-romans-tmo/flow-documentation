{
  "swagger": "2.0",
  "info": {
    "description": "These APIs are used for auto attendant service.",
    "version": "1.0.0",
    "title": "UCC Auto Attendant Service",
    "contact": {
      "name": "Apigee/Documentation Support",
      "email": "TFB_DTD_API_DEVOPS@T-Mobile.com"
    }
  },
  "host": "dev.services.ucc.kube.t-mobile.com",
  "basePath": "/b2b-experience/v1/auto-attendant",
  "x-servers": [
    {
      "url": "dev.services.ucc.kube.t-mobile.com",
      "description": "server path"
    }
  ],
  "tags": [
    {
      "name": "ucc-auto-attendant service",
      "description": "ucc-auto-attendant APIs"
    }
  ],
  "securityDefinitions": {
    "OAuth2": {
      "type": "oauth2",
      "flow": "accessCode",
      "description": "Oauth2 Authorization",
      "authorizationUrl": "https://example.com/oauth/authorize",
      "tokenUrl": "https://tmobileqat-qat01.apigee.net/v1/oauth2/accesstoken?grant_type=client_credentials",
      "scopes": {
        "read": "Grants read access"
      }
    }
  },
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/enterprise/{tenant-id}/auto-attendant/{entry-policy-id}": {
      "post": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "description": "The purpose of this API is to scedule the auto attendant service for the tenant",
        "operationId": "postAutoAttendant",
        "x-api-pattern": "ExecuteFunction",
        "summary": "schedule auto-attendant",
        "parameters": [
          {
            "$ref": "#/parameters/Accept"
          },
          {
            "$ref": "#/parameters/Authorization"
          },
          {
            "$ref": "#/parameters/Content-Type"
          },
          {
            "$ref": "#/parameters/tenant-id"
          },
          {
            "$ref": "#/parameters/entry-policy-id"
          },
          {
            "name": "body",
            "required": true,
            "description": "Provide request payload",
            "in": "body",
            "schema": {
              "$ref": "#/definitions/AutoAttendantRequest"
            }
          }
        ],
        "tags": [
          "ucc-auto-attendant service"
        ],
        "responses": {
          "200": {
            "description": "Successful response",
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "whether cache control is defined",
                "x-example": "no-cache"
              }
            },
            "schema": {
              "$ref": "#/definitions/AutoAttendantResponse"
            },
            "examples": {
              "application/json": {
                "entryPolicyId": "COLA_99988e_383822",
                "autoAttendantName": "Marketing Department",
                "msisdn": "4257774444",
                "timeZone": "PDT",
                "scheduleRows": [
                  {
                    "startTime": "08:00",
                    "endTime": "10:00",
                    "days": [
                      "Monday",
                      "Tuesday",
                      "Wednesday",
                      "Thursday"
                    ]
                  }
                ]
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          },
          "404": {
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          }
        }
      }
    }
  },
  "parameters": {
    "Accept": {
      "name": "Accept",
      "in": "header",
      "description": "+   The request header must contain the Accept element.\n+   Accept header is used by HTTP clients to tell the server what content types they'll accept\n+   The default value is *'application/json'*.",
      "required": false,
      "type": "string",
      "x-example": "application/json",
      "pattern": "application/json"
    },
    "Content-Type": {
      "name": "Content-Type",
      "in": "header",
      "required": true,
      "description": "\n+   The request header must contain the Content-Type element. \n\n\n\n\n\n\n+ In HTTP requests, API clients must specify the format in which the\n      data is submitted.\n\n\n\n+   The default value is *'application/json'*.",
      "type": "string",
      "x-example": "application/json",
      "pattern": "application/json"
    },
    "Authorization": {
      "name": "Authorization",
      "in": "header",
      "description": "+   The request header must contain a valid access token generated by APIGEE.\n\n\n+   The access token is environment specific and is generated dynamically.\n\n\n+   It is mandatory to be passed in the request parameter for authentication.\n\n\n+   The sample Value is *'4354e71c-27c4-4730-b356-59bfe409d456,8a3921cc-a8e2-4323-87e3-6274e71b570f'*.",
      "required": true,
      "type": "string",
      "x-example": "4354e71c-27c4-4730-b356-59bfe409d456,8a3921cc-a8e2-4323-87e3-6274e71b570f",
      "pattern": "4354e71c-27c4-4730-b356-59bfe409d456,8a3921cc-a8e2-4323-87e3-6274e71b570f"
    },
    "tenant-id": {
      "name": "tenant-id",
      "in": "path",
      "description": "+   The enterprise Id/tenant Id",
      "required": true,
      "type": "string",
      "x-example": "1234",
      "pattern": "^[a-zA-Z0-9-]*$"
    },
    "entry-policy-id": {
      "name": "entry-policy-id",
      "in": "path",
      "description": "+   The entry policy id to schedule the auto attendant",
      "required": true,
      "type": "string",
      "x-example": "332",
      "pattern": "332"
    }
  },
  "definitions": {
    "AutoAttendantResponse": {
      "type": "object",
      "description": "The auto attendant schedule response",
      "properties": {
        "entryPolicyId": {
          "type": "string",
          "description": "Return entryPolicyId",
          "example": "COLA_99988e_383822"
        },
        "autoAttendantName": {
          "type": "string",
          "description": "Return autoAttendentName",
          "example": "Marketing Department"
        },
        "msisdn": {
          "type": "string",
          "description": "The 10 digit valid T-Mobile mobile number",
          "example": "4257774444"
        },
        "timeZone": {
          "type": "string",
          "description": "The time zone to open/close the auto attendant scheduler",
          "example": "PDT"
        },
        "scheduleRows": {
          "type": "array",
          "description": "The array list to set start time, end time and days",
          "items": {
            "type": "object",
            "description": "Schedule auto attendant",
            "properties": {
              "startTime": {
                "type": "string",
                "description": "The auto attendant start time",
                "example": "08:00"
              },
              "endTime": {
                "type": "string",
                "description": "The auto attendant end time",
                "example": "10:00"
              },
              "days": {
                "type": "array",
                "description": "The auto attendant scheduled days",
                "items": {
                  "type": "string",
                  "example": [
                    "Monday",
                    "Tuesday",
                    "Wednesday",
                    "Thursday"
                  ]
                }
              }
            }
          }
        }
      }
    },
    "AutoAttendantRequest": {
      "type": "object",
      "description": "The auto attendant schedule request",
      "properties": {
        "entryPolicyId": {
          "type": "string",
          "description": "Provide entryPolicyId",
          "example": "COLA_99988e_383822"
        },
        "schedules": {
          "type": "array",
          "description": "The array list to create auto attendant scheduler",
          "items": {
            "type": "object",
            "description": "Schedule auto attendant",
            "properties": {
              "scheduleId": {
                "type": "string",
                "description": "The auto attendant schedule identifier",
                "example": "123456"
              },
              "scheduleName": {
                "type": "string",
                "description": "The auto attendant schedule name",
                "example": "Bob'\\'s Open Schedule'"
              },
              "type": {
                "type": "string",
                "description": "The type can be  OPEN or CLOSE",
                "example": "OPEN"
              },
              "scheduleRows": {
                "type": "array",
                "description": "The array list to set start time, end time and days",
                "items": {
                  "type": "object",
                  "description": "Schedule auto attendant",
                  "properties": {
                    "startTime": {
                      "type": "string",
                      "description": "The auto attendant start time",
                      "example": "08:00"
                    },
                    "endTime": {
                      "type": "string",
                      "description": "The auto attendant end time",
                      "example": "10:00"
                    },
                    "days": {
                      "type": "array",
                      "description": "The auto attendant scheduled days",
                      "items": {
                        "type": "string",
                        "example": [
                          "Monday",
                          "Tuesday",
                          "Wednesday",
                          "Thursday"
                        ]
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "ErrorResponse": {
      "type": "object",
      "description": "error response",
      "properties": {
        "code": {
          "type": "string",
          "description": "Provides the error code",
          "x-example": "System error"
        },
        "userMessage": {
          "type": "string",
          "description": "Provides the error message",
          "x-example": "System error"
        }
      }
    }
  }
}