{
  "swagger": "2.0",
  "info": {
    "title": "File Download Service",
    "description": "This is the API specification for File download service",
    "version": "1.0.0",
    "contact": {
      "name": "Apigee/Documentation support",
      "email": "TFB_DTD_API_DEVOPS@T-Mobile.com"
    }
  },
  "host": "qlab02.core.op.api.t-mobile.com",
  "schemes": [
    "https"
  ],
  "basePath": "/b2b-experience/v1/file-download",
  "x-servers": {
    "url": "qlab02.core.op.api.t-mobile.com",
    "description": "server path"
  },
  "tags": [
    {
      "name": "File Download Service",
      "description": "File Download Service"
    }
  ],
  "securityDefinitions": {
    "OauthSecurity": {
      "description": "security definition",
      "type": "oauth2",
      "flow": "accessCode",
      "authorizationUrl": "https://auth14.iam.msg.lab.t-mobile.com/oauth2/v1/token?grant_type=client_credentials&redirect_uri=https%3A%2F%2Flocalhost&scope=TMO_ID_profile+extended_lines",
      "tokenUrl": "https://tmobileh-sb05.apigee.net/oauth/client_credential/accesstoken?grant_type=client_credentials",
      "scopes": {
        "user": "User scope"
      }
    }
  },
  "produces": [
    "application/json"
  ],
  "paths": {
    "/file-download-details": {
      "post": {
        "security": [
          {
            "OauthSecurity": [
              "user"
            ]
          }
        ],
        "x-api-pattern": "CreateInCollection",
        "description": "This is the API specification to create file download",
        "operationId": "postFileDownload",
        "summary": "Create usage download details",
        "parameters": [
          {
            "in": "header",
            "name": "Accept",
            "description": "Accept",
            "required": false,
            "type": "string",
            "x-example": "application/json",
            "enum": [
              "application/json"
            ]
          },
          {
            "in": "header",
            "name": "Authorization",
            "description": "Authorization",
            "required": true,
            "type": "string",
            "x-example": "EGQ94pYxAzk2JRmSoW1GTZifPAJI",
            "enum": [
              "EGQ94pYxAzk2JRmSoW1GTZifPAJI"
            ]
          },
          {
            "in": "header",
            "name": "Content-Type",
            "description": "Content-Type",
            "required": true,
            "type": "string",
            "x-example": "application/json",
            "enum": [
              "application/json"
            ]
          },
          {
            "in": "body",
            "name": "request-payload",
            "description": "CreateFileDownload",
            "required": true,
            "schema": {
              "$ref": "#/definitions/CreateFileDownload"
            }
          }
        ],
        "tags": [
          "File Download Service"
        ],
        "responses": {
          "201": {
            "description": "Success. (OK)",
            "schema": {
              "$ref": "#/definitions/FileDownloadResponse"
            },
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              },
              "Location": {
                "type": "string",
                "description": "Location",
                "x-example": "Location"
              }
            },
            "examples": {
              "application/json": {
                "status": "Success",
                "message": "Successfully created the usage download details",
                "usageFileId": 23
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 400,
                "userMessage": "Bad Request"
              }
            }
          },
          "401": {
            "description": "Authorization Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 401,
                "userMessage": "Authorization Error"
              }
            }
          },
          "403": {
            "description": "Forbidden",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 403,
                "userMessage": "Forbidden"
              }
            }
          },
          "404": {
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 404,
                "userMessage": "Not Found"
              }
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 500,
                "userMessage": "Internal Server Error"
              }
            }
          }
        }
      },
      "get": {
        "security": [
          {
            "OauthSecurity": [
              "user"
            ]
          }
        ],
        "x-api-pattern": "QueryCollection",
        "description": "This is the API specification to retrieve the usage details for given tenant",
        "operationId": "getFileDownload",
        "summary": "Retrieve Usage Details",
        "parameters": [
          {
            "in": "header",
            "name": "Accept",
            "description": "Accept",
            "required": false,
            "type": "string",
            "x-example": "application/json",
            "enum": [
              "application/json"
            ]
          },
          {
            "in": "header",
            "name": "Authorization",
            "description": "Authorization",
            "required": true,
            "type": "string",
            "x-example": "EGQ94pYxAzk2JRmSoW1GTZifPAJI",
            "enum": [
              "EGQ94pYxAzk2JRmSoW1GTZifPAJI"
            ]
          },
          {
            "in": "header",
            "name": "Content-Type",
            "description": "Content-Type",
            "required": true,
            "type": "string",
            "x-example": "application/json",
            "enum": [
              "application/json"
            ]
          }
        ],
        "tags": [
          "File Download Service"
        ],
        "responses": {
          "200": {
            "description": "Success. (OK)",
            "schema": {
              "$ref": "#/definitions/GetFileDownload"
            },
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              }
            },
            "examples": {
              "application/json": {
                "message": "Successfully fetched the usage details for the given tenant",
                "details": [
                  {
                    "fileId": 7,
                    "tenantId": "prodtest",
                    "fileType": "USAGE_DETAILS",
                    "status": "PENDING",
                    "adminEmail": "ucctest007@gmail.com",
                    "errorDetail": "Error in status update to COMPLETED",
                    "createdBy": "surya",
                    "createdDate": "1572522581000",
                    "updatedBy": "SUCCESSTest1",
                    "updatedDate": "1572522710000",
                    "searchParams": {
                      "fromDate": "2019-10-08T23:00:00",
                      "toDate": "2019-10-08T23:59:59",
                      "sort": "ASC"
                    }
                  }
                ]
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 400,
                "userMessage": "Bad Request"
              }
            }
          },
          "401": {
            "description": "Authorization Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 401,
                "userMessage": "Authorization Error"
              }
            }
          },
          "403": {
            "description": "Forbidden",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 403,
                "userMessage": "Forbidden"
              }
            }
          },
          "404": {
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 404,
                "userMessage": "Not Found"
              }
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 500,
                "userMessage": "Internal Server Error"
              }
            }
          }
        }
      }
    },
    "/pre-sign-url": {
      "get": {
        "security": [
          {
            "OauthSecurity": [
              "user"
            ]
          }
        ],
        "x-api-pattern": "QueryCollection",
        "description": "This is the API specification to get the pre-sign url",
        "operationId": "getSign",
        "summary": "Retrieve Pre-Sign Url",
        "parameters": [
          {
            "in": "header",
            "name": "Accept",
            "description": "Accept",
            "required": false,
            "type": "string",
            "x-example": "application/json",
            "enum": [
              "application/json"
            ]
          },
          {
            "in": "header",
            "name": "Authorization",
            "description": "Authorization",
            "required": true,
            "type": "string",
            "x-example": "EGQ94pYxAzk2JRmSoW1GTZifPAJI",
            "enum": [
              "EGQ94pYxAzk2JRmSoW1GTZifPAJI"
            ]
          },
          {
            "in": "header",
            "name": "Content-Type",
            "description": "Content-Type",
            "required": true,
            "type": "string",
            "x-example": "application/json",
            "enum": [
              "application/json"
            ]
          },
          {
            "name": "file-name",
            "in": "query",
            "required": true,
            "description": "+ Name of the file",
            "type": "string",
            "pattern": "Usage_Details_20191029_22.03.57.csv",
            "x-example": "Usage_Details_20191029_22.03.57.csv"
          },
          {
            "name": "file-type",
            "in": "query",
            "required": true,
            "description": "+ The type of the filr",
            "type": "string",
            "pattern": "USAGE_DETAILS",
            "x-example": "USAGE_DETAILS"
          }
        ],
        "tags": [
          "File Download Service"
        ],
        "responses": {
          "200": {
            "description": "Success. (OK)",
            "schema": {
              "$ref": "#/definitions/PreSignUrl"
            },
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              }
            },
            "examples": {
              "application/json": "\"https://uco-npe-userimport.s3.us-west-2.amazonaws.com/dev/usageDetails/Usage_Details_20191029_22.03.57.csv?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20191029T163533Z&X-Amz-SignedHeaders=host&X-Amz-Expires=899&X-Amz-Credential=AKIAXBWQUNDKHLMPPL6C%2F20191029%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Signature=4934ca3106618c4e3705e6499aeba715a52906726e8db5d1a8e1c1f41afcfcd2\""
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 400,
                "userMessage": "Bad Request"
              }
            }
          },
          "401": {
            "description": "Authorization Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 401,
                "userMessage": "Authorization Error"
              }
            }
          },
          "403": {
            "description": "Forbidden",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 403,
                "userMessage": "Forbidden"
              }
            }
          },
          "404": {
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 404,
                "userMessage": "Not Found"
              }
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": 500,
                "userMessage": "Internal Server Error"
              }
            }
          }
        }
      }
    }
  },
  "definitions": {
    "PreSignUrl": {
      "type": "string",
      "description": "Pre-sign Url",
      "example": "\"https://uco-npe-userimport.s3.us-west-2.amazonaws.com/dev/usageDetails/Usage_Details_20191029_22.03.57.csv?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20191029T163533Z&X-Amz-SignedHeaders=host&X-Amz-Expires=899&X-Amz-Credential=AKIAXBWQUNDKHLMPPL6C%2F20191029%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Signature=4934ca3106618c4e3705e6499aeba715a52906726e8db5d1a8e1c1f41afcfcd2\""
    },
    "GetFileDownload": {
      "type": "object",
      "description": "This field contains the payload response to retrieve the usage details",
      "properties": {
        "message": {
          "type": "string",
          "description": "Provide the status message",
          "example": "Successfully fetched the usage details for the given tenant"
        },
        "details": {
          "type": "array",
          "description": "Details array list",
          "items": {
            "type": "object",
            "description": "details array list object nodes",
            "properties": {
              "fileId": {
                "type": "number",
                "description": "Returns the fileId",
                "example": 7
              },
              "tenantId": {
                "type": "string",
                "description": "Returns the tenantId",
                "example": "prodtest"
              },
              "fileType": {
                "type": "string",
                "description": "Returns the fileType",
                "example": "USAGE_DETAILS"
              },
              "status": {
                "type": "string",
                "description": "Returns the status",
                "example": "PENDING"
              },
              "adminEmail": {
                "type": "string",
                "description": "Returns the adminEmail",
                "example": "ucctest007@gmail.com"
              },
              "errorDetail": {
                "type": "string",
                "description": "Returns the errorDetail",
                "example": "Error in status update to COMPLETED"
              },
              "createdBy": {
                "type": "string",
                "description": "Returns the file createdBy user name",
                "example": "surya"
              },
              "createdDate": {
                "type": "string",
                "description": "Returns the date stamp of file creation",
                "example": "1572522581000"
              },
              "updatedBy": {
                "type": "string",
                "description": "Returns the file updatedBy user name",
                "example": "SUCCESSTest1"
              },
              "updatedDate": {
                "type": "string",
                "description": "Returns the date stamp of file updation",
                "example": "1572522710000"
              },
              "searchParams": {
                "type": "object",
                "description": "Search parameters object node",
                "properties": {
                  "fromDate": {
                    "type": "string",
                    "description": "The from date time stamp to limit the usage details search",
                    "example": "2019-10-08T23:00:00"
                  },
                  "toDate": {
                    "type": "string",
                    "description": "The until date time stamp to limit the usage details search",
                    "example": "2019-10-08T23:59:59"
                  },
                  "sort": {
                    "type": "string",
                    "description": "Sort the details returned either ascending (ASC) or descending (DESC)",
                    "example": "ASC"
                  }
                }
              }
            }
          }
        },
        "totalNoOfRecords": {
          "type": "string",
          "description": "Indicate the total number of records returned in the resultset",
          "example": "1"
        },
        "endOfResult": {
          "type": "string",
          "description": "Indicate the end of result either true or false",
          "example": "true"
        }
      }
    },
    "CreateFileDownload": {
      "type": "object",
      "description": "This field contains the payload request to retrieve the usage details",
      "properties": {
        "tenantId": {
          "type": "string",
          "description": "Returns the tenantId",
          "example": "prodtest"
        },
        "fileType": {
          "type": "string",
          "description": "Returns the fileType",
          "example": "USAGE_DETAILS"
        },
        "status": {
          "type": "string",
          "description": "Returns the status",
          "example": "PENDING"
        },
        "createdBy": {
          "type": "string",
          "description": "Returns the file createdBy user name",
          "example": "surya"
        },
        "updatedBy": {
          "type": "string",
          "description": "Returns the file updatedBy user name",
          "example": "SUCCESSTest1"
        },
        "adminEmail": {
          "type": "string",
          "description": "Returns the adminEmail",
          "example": "ucctest007@gmail.com"
        },
        "searchParams": {
          "type": "object",
          "description": "Search parameters object node",
          "properties": {
            "fromDate": {
              "type": "string",
              "description": "The from date time stamp to limit the usage details search",
              "example": "2019-10-08T23:00:00"
            },
            "toDate": {
              "type": "string",
              "description": "The until date time stamp to limit the usage details search",
              "example": "2019-10-08T23:59:59"
            },
            "sort": {
              "type": "string",
              "description": "Sort the details returned either ascending (ASC) or descending (DESC)",
              "example": "ASC"
            }
          }
        }
      }
    },
    "FileDownloadResponse": {
      "type": "object",
      "description": "Return the reponse for created hunt group",
      "properties": {
        "status": {
          "type": "string",
          "description": "Return the status. (Success or Failure)",
          "example": "Success"
        },
        "message": {
          "type": "string",
          "description": "Return the message .",
          "example": "Successfully created the usage download details"
        },
        "usageFileId": {
          "type": "number",
          "description": "Return the usageFileId .",
          "example": 23
        }
      }
    },
    "ErrorResponse": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string",
          "description": "Missing mandatory attribute. Conflicting data .Invalid attribute"
        },
        "userMessage": {
          "type": "string",
          "description": "The attribute name for the error"
        }
      }
    }
  }
}