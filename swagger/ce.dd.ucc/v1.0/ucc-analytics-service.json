{
  "swagger": "2.0",
  "info": {
    "description": "These APIs are used for analytice service.",
    "version": "1.0.0",
    "title": "UCC Analytics Service",
    "contact": {
      "name": "Apigee/Documentation Support",
      "email": "TFB_DTD_API_DEVOPS@T-Mobile.com"
    }
  },
  "host": "dev.services.ucc.kube.t-mobile.com",
  "basePath": "/b2b-experience/v1/analytics-service",
  "x-servers": [
    {
      "url": "dev.services.ucc.kube.t-mobile.com",
      "description": "server path"
    }
  ],
  "tags": [
    {
      "name": "Analytics Service",
      "description": "Analytics service APIs"
    }
  ],
  "securityDefinitions": {
    "OAuth2": {
      "type": "oauth2",
      "flow": "accessCode",
      "description": "Oauth2 Authorization",
      "authorizationUrl": "https://example.com/oauth/authorize",
      "tokenUrl": "https://tmobileqat-qat01.apigee.net/v1/oauth2/accesstoken?grant_type=client_credentials",
      "scopes": {
        "read": "Grants read access"
      }
    }
  },
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/usage-details": {
      "post": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "description": "The purpose of this API is to retrieve the usage details.",
        "operationId": "postUsageDetails",
        "x-api-pattern": "ExecuteFunction",
        "summary": "Usage Details",
        "parameters": [
          {
            "$ref": "#/parameters/Accept"
          },
          {
            "$ref": "#/parameters/Authorization"
          },
          {
            "$ref": "#/parameters/Content-Type"
          },
          {
            "name": "body",
            "required": true,
            "description": "Provide request payload",
            "in": "body",
            "schema": {
              "$ref": "#/definitions/UsageDetailsRequest"
            }
          }
        ],
        "tags": [
          "Analytics Service"
        ],
        "responses": {
          "200": {
            "description": "Successful response",
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "whether cache control is defined",
                "x-example": "no-cache"
              }
            },
            "schema": {
              "$ref": "#/definitions/UsageDetailsResponse"
            },
            "examples": {
              "application/json": {
                "callMessageList": [
                  {
                    "date": "2019-11-19T05:35:13.000Z",
                    "type": "Outgoing call",
                    "status": "Success",
                    "mos": "4.4",
                    "duration": "3 min. 1 sec",
                    "number": "NA",
                    "software": "NA",
                    "otherParticipants": null,
                    "latency": "0",
                    "audioPacketLoss": "0",
                    "audioJitter": "13",
                    "callerEmail": "futautotest15@outlook.com",
                    "calleeEmail": "futautotest14@outlook.com",
                    "sip": "futautotest12@outlook.com",
                    "callerNumber": "+12532853687",
                    "calleeNumber": "+12532853687",
                    "startDateTime": "2019-11-19 05:35:13",
                    "endDateTime": "2019-11-19 05:38:14",
                    "calledPartyMos": "0",
                    "callingPartyMos": "4.4",
                    "user": "futautotest15@outlook.com"
                  }
                ],
                "callNext": "j4/zAgFz2AREbkYx",
                "messageNext": "j4/zAgFz2AREb"
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          },
          "404": {
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          }
        }
      }
    },
    "/usage": {
      "get": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "description": "The purpose of this API is to retrieve the usage statistics for the given time stamp.",
        "operationId": "getUsage",
        "x-api-pattern": "QueryCollection",
        "summary": "Retrieve Usage data",
        "parameters": [
          {
            "$ref": "#/parameters/Accept"
          },
          {
            "$ref": "#/parameters/Authorization"
          },
          {
            "$ref": "#/parameters/Content-Type"
          },
          {
            "$ref": "#/parameters/Cache-Control"
          },
          {
            "name": "from-date",
            "in": "query",
            "type": "string",
            "required": true,
            "description": "This field contains the date from which the usage data needs to be retrieved. The sample format is '2019-05-30T00:00:00'",
            "x-example": "2019-05-30T00:00:00",
            "pattern": "2019-05-30T00:00:00"
          },
          {
            "name": "to-date",
            "in": "query",
            "type": "string",
            "required": true,
            "description": "This field contains the date until which the usage data needs to be retrieved. The sample format is '2019-06-30T23:59:59'",
            "x-example": "2019-06-30T23:59:59",
            "pattern": "2019-06-19T23:59:59"
          },
          {
            "name": "sort",
            "in": "query",
            "type": "string",
            "required": true,
            "description": "Indicator for sorting. It can be ascending or descending",
            "x-example": "DESC",
            "enum": [
              "DESC",
              "ASC"
            ]
          },
          {
            "name": "tenant",
            "in": "query",
            "type": "string",
            "required": true,
            "description": "Corporate Tenant Id",
            "x-example": "1-3R5P8YI",
            "pattern": "1-3R5P8YI"
          },
          {
            "name": "aggregation-by",
            "in": "query",
            "type": "string",
            "required": false,
            "description": "Provide the filter for aggregation",
            "x-example": "day"
          }
        ],
        "tags": [
          "Analytics Service"
        ],
        "responses": {
          "200": {
            "description": "Successful response",
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "whether cache control is defined",
                "x-example": "no-cache"
              }
            },
            "schema": {
              "$ref": "#/definitions/GetUsageResponse"
            },
            "examples": {
              "application/json": [
                {
                  "usageTimePeriod": "06/30/2019",
                  "usagePhoneCalls": {
                    "originating": "0",
                    "terminating": "0"
                  },
                  "usageMessages": {
                    "originating": "0",
                    "terminating": "0"
                  }
                }
              ]
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          },
          "404": {
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          }
        }
      }
    },
    "/details": {
      "get": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "description": "The purpose of this API is to retrieve mos rating score. Create Call/Message-level drilldown page for call quality usage measurements.",
        "operationId": "getUsageDetails",
        "x-api-pattern": "QueryCollection",
        "summary": "MOS score",
        "parameters": [
          {
            "$ref": "#/parameters/Accept"
          },
          {
            "$ref": "#/parameters/Authorization"
          },
          {
            "$ref": "#/parameters/Content-Type"
          },
          {
            "$ref": "#/parameters/Cache-Control"
          },
          {
            "name": "from-date",
            "in": "query",
            "type": "string",
            "required": true,
            "description": "This field contains the date from which the usage data measurement needs to be retrieved. The sample format is '2019-06-19T00:00:00'",
            "x-example": "2019-06-19T00:00:00",
            "pattern": "2019-06-19T00:00:00"
          },
          {
            "name": "to-date",
            "in": "query",
            "type": "string",
            "required": true,
            "description": "This field contains the date until which the usage data measurement needs to be retrieved. The sample format is '2019-06-19T23:59:59'",
            "x-example": "2019-06-19T23:59:59",
            "pattern": "2019-06-19T23:59:59"
          },
          {
            "name": "sort",
            "in": "query",
            "type": "string",
            "required": true,
            "description": "Indicator for sorting. It can be ascending or descending",
            "x-example": "ASC",
            "enum": [
              "DESC",
              "ASC"
            ]
          }
        ],
        "tags": [
          "Analytics Service"
        ],
        "responses": {
          "200": {
            "description": "Successful response",
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "whether cache control is defined",
                "x-example": "no-cache"
              }
            },
            "schema": {
              "$ref": "#/definitions/UsageDetailResponse"
            },
            "examples": {
              "application/json": [
                {
                  "date": "05/31/2019, 06:39 PM",
                  "type": "outgoing call",
                  "status": "Success",
                  "mos": "1.4"
                }
              ]
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          },
          "404": {
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          }
        }
      }
    },
    "/usage-details-file": {
      "get": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "description": "The purpose of this API is to get the link to download the usage details in csv format",
        "operationId": "getUsageDetailsFile",
        "x-api-pattern": "QueryCollection",
        "summary": "Usage Details CSV",
        "parameters": [
          {
            "$ref": "#/parameters/Accept"
          },
          {
            "$ref": "#/parameters/Authorization"
          },
          {
            "$ref": "#/parameters/Content-Type"
          },
          {
            "$ref": "#/parameters/Cache-Control"
          },
          {
            "name": "from-date",
            "in": "query",
            "type": "string",
            "required": true,
            "description": "This field contains the date from which the usage data measurement needs to be retrieved. The sample format is 2019-09-03T00:00:00",
            "x-example": "2019-09-03T00:00:00",
            "pattern": "2019-09-03T00:00:00"
          },
          {
            "name": "to-date",
            "in": "query",
            "type": "string",
            "required": true,
            "description": "This field contains the date until which the usage data measurement needs to be retrieved. The sample format is 2019-06-19T23:59:59",
            "x-example": "2019-09-04T03:59:59",
            "pattern": "2019-09-04T03:59:59"
          },
          {
            "name": "sort",
            "in": "query",
            "type": "string",
            "required": true,
            "description": "Indicator for sorting. It can be ascending or descending",
            "x-example": "ASC",
            "enum": [
              "DESC",
              "ASC"
            ]
          }
        ],
        "tags": [
          "Analytics Service"
        ],
        "responses": {
          "200": {
            "description": "Successful response",
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "whether cache control is defined",
                "x-example": "no-cache"
              }
            },
            "schema": {
              "$ref": "#/definitions/UsageDetailFileResponse"
            },
            "examples": {
              "application/json": "https://uco-npe-userimport.s3.us-west-2.amazonaws.com/dev/usageDetails/Usage_Details_20190919_16.51.28.csv?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20190919T112135Z&X-Amz-SignedHeaders=host&X-Amz-Expires=899&X-Amz-Credential=AKIAXBWQUNDKHLMPPL6C%2F20190919%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Signature=0f7ed31f7437abd093644de42cce7c26fed9f2b42c22365ef4456780f261ae9f"
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          },
          "404": {
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          }
        }
      }
    },
    "/mos-details": {
      "get": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "description": "The purpose of this API is to get the average mean opinion score (MOS score) details",
        "operationId": "getMosScoreDetails",
        "x-api-pattern": "QueryCollection",
        "summary": "Average MOS Details",
        "parameters": [
          {
            "$ref": "#/parameters/Accept"
          },
          {
            "$ref": "#/parameters/Authorization"
          },
          {
            "$ref": "#/parameters/Content-Type"
          },
          {
            "$ref": "#/parameters/Cache-Control"
          },
          {
            "name": "from-date",
            "in": "query",
            "type": "string",
            "required": false,
            "description": "This field contains the date from which the usage data measurement needs to be retrieved. The sample format is 2019-09-03T00:00:00",
            "x-example": "2019-09-03T00:00:00",
            "pattern": "2019-09-03T00:00:00"
          },
          {
            "name": "to-date",
            "in": "query",
            "type": "string",
            "required": false,
            "description": "This field contains the date until which the usage data measurement needs to be retrieved. The sample format is 2019-06-19T23:59:59",
            "x-example": "2019-09-04T03:59:59",
            "pattern": "2019-09-04T03:59:59"
          },
          {
            "name": "sort",
            "in": "query",
            "type": "string",
            "required": false,
            "description": "Indicator for sorting. It can be ascending or descending",
            "x-example": "ASC",
            "enum": [
              "DESC",
              "ASC"
            ]
          },
          {
            "name": "aggregation-by",
            "in": "query",
            "type": "string",
            "required": false,
            "description": "Indicator for how the data needs to be agggregated.For example aggregation-by:month",
            "x-example": "month",
            "pattern": "month"
          }
        ],
        "tags": [
          "Analytics Service"
        ],
        "responses": {
          "200": {
            "description": "Successful response",
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "whether cache control is defined",
                "x-example": "no-cache"
              }
            },
            "schema": {
              "$ref": "#/definitions/ScoreResponse"
            },
            "examples": {
              "application/json": [
                {
                  "monthYear": "09/2019",
                  "avgMos": 6.9,
                  "callCount": 11358
                }
              ]
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          },
          "404": {
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "string",
                "userMessage": "string"
              }
            }
          }
        }
      }
    }
  },
  "parameters": {
    "Accept": {
      "name": "Accept",
      "in": "header",
      "description": "+   The request header must contain the Accept element.\n+   Accept header is used by HTTP clients to tell the server what content types they'll accept\n+   The default value is *'application/json'*.",
      "required": false,
      "type": "string",
      "x-example": "application/json",
      "pattern": "application/json"
    },
    "Cache-Control": {
      "name": "Cache-Control",
      "in": "header",
      "description": "+   no-cache",
      "required": false,
      "type": "string",
      "x-example": "no-cache",
      "pattern": "no-cache"
    },
    "Content-Type": {
      "name": "Content-Type",
      "in": "header",
      "required": true,
      "description": "\n+   The request header must contain the Content-Type element. \n\n\n\n\n\n\n+ In HTTP requests, API clients must specify the format in which the\n      data is submitted.\n\n\n\n+   The default value is *'application/json'*.",
      "type": "string",
      "x-example": "application/json",
      "pattern": "application/json"
    },
    "Authorization": {
      "name": "Authorization",
      "in": "header",
      "description": "+   The request header must contain a valid access token generated by APIGEE.\n\n\n+   The access token is environment specific and is generated dynamically.\n\n\n+   It is mandatory to be passed in the request parameter for authentication.\n\n\n+   The sample Value is *'4354e71c-27c4-4730-b356-59bfe409d456,8a3921cc-a8e2-4323-87e3-6274e71b570f'*.",
      "required": true,
      "type": "string",
      "x-example": "4354e71c-27c4-4730-b356-59bfe409d456,8a3921cc-a8e2-4323-87e3-6274e71b570f",
      "pattern": "4354e71c-27c4-4730-b356-59bfe409d456,8a3921cc-a8e2-4323-87e3-6274e71b570f"
    }
  },
  "definitions": {
    "ScoreResponse": {
      "type": "array",
      "description": "The MOS score details array node",
      "items": {
        "type": "object",
        "description": "Return the object elements",
        "properties": {
          "monthYear": {
            "type": "string",
            "description": "Return the month and year for MOS score returned",
            "example": "09/2019"
          },
          "avgMos": {
            "type": "integer",
            "description": "Return the average MOS score",
            "example": 6.9
          },
          "callCount": {
            "type": "integer",
            "description": "Returns the call count",
            "example": 11358
          }
        }
      }
    },
    "UsageDetailFileResponse": {
      "type": "string",
      "description": "Usage detailsresponse link to download file in csv format",
      "example": "\"https://uco-npe-userimport.s3.us-west-2.amazonaws.com/dev/usageDetails/Usage_Details_20190919_16.51.28.csv?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20190919T112135Z&X-Amz-SignedHeaders=host&X-Amz-Expires=899&X-Amz-Credential=AKIAXBWQUNDKHLMPPL6C%2F20190919%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Signature=0f7ed31f7437abd093644de42cce7c26fed9f2b42c22365ef4456780f261ae9f\""
    },
    "GetUsageResponse": {
      "type": "array",
      "description": "Response usage array response",
      "items": {
        "type": "object",
        "description": "Return the array items",
        "properties": {
          "usageTimePeriod": {
            "type": "string",
            "description": "Return the the date stamp for the usage data",
            "example": "06/30/2019",
            "pattern": "06/30/2019,06/29/2019,06/28/2019"
          },
          "usagePhoneCalls": {
            "type": "object",
            "description": "Orginating and terminating call usage details",
            "properties": {
              "originating": {
                "type": "string",
                "description": "Orginating call usage details",
                "example": "0"
              },
              "terminating": {
                "type": "string",
                "description": "Terminating call usage details",
                "example": "0"
              }
            }
          },
          "usageMessages": {
            "type": "object",
            "description": "Orginating and terminating message/text usage details",
            "properties": {
              "originating": {
                "type": "string",
                "description": "Orginating message/text usage details",
                "example": "0"
              },
              "terminating": {
                "type": "string",
                "description": "Terminating message/text usage details",
                "example": "0"
              }
            }
          }
        }
      }
    },
    "UsageDetailResponse": {
      "type": "array",
      "description": "Response usage details array response",
      "items": {
        "type": "object",
        "description": "Return the array items",
        "properties": {
          "date": {
            "type": "string",
            "description": "Date time stamp response during  which the call usage quality score is measured",
            "example": "05/31/2019, 06:39 PM",
            "pattern": "05/31/2019, 06:39 PM"
          },
          "type": {
            "type": "string",
            "description": "The call type for which mos rating is measured like outgoing call /incoming call etc.",
            "example": "outgoing call",
            "enum": [
              "outgoing call",
              "incoming call"
            ]
          },
          "status": {
            "type": "string",
            "description": "Return the status. For example:'Success'",
            "example": "Success"
          },
          "mos": {
            "type": "string",
            "description": "MOS measures subjective call quality for a call. MOS scores range from 1 for unacceptable to 5 for excellent.",
            "example": "1.4"
          }
        }
      }
    },
    "UsageResponse": {
      "type": "object",
      "description": "Return Usage data response",
      "properties": {
        "usageData": {
          "type": "array",
          "description": "Return usage data array list",
          "items": {
            "type": "object",
            "description": "Return the usage data array list items",
            "properties": {
              "usageTimePeriod": {
                "type": "string",
                "description": "Return usageTimePeriod",
                "example": "02/2019"
              },
              "usagePhoneCalls": {
                "type": "string",
                "description": "Retuen usagePhoneCalls",
                "example": "0"
              },
              "usageMessages": {
                "type": "string",
                "description": "Return usageMessages",
                "example": "0"
              }
            }
          }
        }
      }
    },
    "UsageRequest": {
      "type": "object",
      "description": "Provide UsageRequest",
      "properties": {
        "fromTimestamp": {
          "type": "string",
          "description": "Provide fromtimestamp",
          "example": "2019-02-25T00:00:00"
        },
        "toTimestamp": {
          "type": "string",
          "description": "Provide totimestamp",
          "example": "2019-05-31T00:00:59"
        },
        "filterTenant": {
          "type": "array",
          "description": "Provide filterTenant",
          "items": {
            "type": "string",
            "description": "Providethe filterTenant",
            "example": "prod"
          }
        }
      }
    },
    "UsageDetailsResponse": {
      "type": "object",
      "description": "Return the usage details response",
      "properties": {
        "callMessageList": {
          "type": "array",
          "description": "Array list of call message details",
          "items": {
            "type": "object",
            "description": "Array list of call message details",
            "properties": {
              "date": {
                "type": "string",
                "description": "The date time stamp in UTC timezone format",
                "example": "2019-11-19T05:35:13.000Z"
              },
              "type": {
                "type": "string",
                "description": "The type of call like Outgoing call, Incoming call, Outgoing message, Incoming message",
                "example": "Outgoing call"
              },
              "status": {
                "type": "string",
                "description": "Return the status",
                "example": "Success"
              },
              "mos": {
                "type": "string",
                "description": "MOS measures subjective call quality for a call. MOS scores range from 1 for unacceptable to 5 for excellent.",
                "example": "4.4"
              },
              "duration": {
                "type": "string",
                "description": "The call duaration in measured in seconds",
                "example": "3 min. 1 sec"
              },
              "number": {
                "type": "string",
                "description": "Returns the number if applicable",
                "example": "NA"
              },
              "software": {
                "type": "string",
                "description": "software details if applicable",
                "example": "NA"
              },
              "otherParticipants": {
                "type": "array",
                "description": "List of other participants in call",
                "items": {
                  "type": "string",
                  "description": "List of other participants in call",
                  "example": ""
                }
              },
              "latency": {
                "type": "string",
                "description": "Measure of delay in transfer of call/data",
                "example": "0"
              },
              "audioPacketLoss": {
                "type": "string",
                "description": "Measure of audio packets lost",
                "example": "0"
              },
              "audioJitter": {
                "type": "string",
                "description": "Measure in variation in the delay of received packets",
                "example": "13"
              },
              "callerEmail": {
                "type": "string",
                "description": "The caller email address",
                "example": "futautotest15@outlook.com"
              },
              "calleeEmail": {
                "type": "string",
                "description": "The callee email address",
                "example": "futautotest14@outlook.com"
              },
              "sip": {
                "type": "string",
                "description": "The sip",
                "example": "futautotest12@outlook.com"
              },
              "callerNumber": {
                "type": "string",
                "description": "The caller Number",
                "example": "+12064348508"
              },
              "calleeNumber": {
                "type": "string",
                "description": "The callee Number",
                "example": "+12532853687"
              },
              "startDateTime": {
                "type": "string",
                "description": "The usage start date time stamp",
                "example": "2019-11-19 05:35:13"
              },
              "endDateTime": {
                "type": "string",
                "description": "The usage end date time stamp",
                "example": "2019-11-19 05:38:14"
              },
              "calledPartyMos": {
                "type": "string",
                "description": "The called party mean opinion score",
                "example": "0"
              },
              "callingPartyMos": {
                "type": "string",
                "description": "The calling party mean opinion score",
                "example": "4.4"
              },
              "user": {
                "type": "string",
                "description": "The user email address",
                "example": "futautotest15@outlook.com"
              }
            }
          }
        },
        "callNext": {
          "type": "string",
          "description": "The call next",
          "example": "j4/zAgFz2AREbkYx"
        },
        "messageNext": {
          "type": "string",
          "description": "The message next",
          "example": "j4/zAgFz2AREb"
        }
      }
    },
    "UsageDetailsRequest": {
      "type": "object",
      "description": "Provide UsageRequest",
      "properties": {
        "usageType": {
          "type": "string",
          "description": "The type of usage",
          "example": ""
        },
        "fromTime": {
          "type": "string",
          "description": "Provide fromtimestamp",
          "example": "2019-11-19T00:00:00"
        },
        "toTime": {
          "type": "string",
          "description": "Provide totimestamp",
          "example": "2019-11-20T23:59:59"
        },
        "sort": {
          "type": "string",
          "description": "Indicator for sorting. It can be ascending (ASC) or descending (DESC)",
          "example": "ASC",
          "pattern": "ASC"
        },
        "fullSort": {
          "type": "string",
          "description": "Indicator for full sorting.",
          "example": "false",
          "pattern": "false"
        },
        "email": {
          "type": "array",
          "description": "List of email adrdress for filtering.",
          "items": {
            "type": "string",
            "description": "List of email address",
            "example": ""
          }
        },
        "phone": {
          "type": "array",
          "description": "List of phone adrdress for filtering.",
          "items": {
            "type": "string",
            "description": "List of email address",
            "example": ""
          }
        },
        "messageNext": {
          "type": "string",
          "description": "The message next",
          "example": ""
        },
        "callNext": {
          "type": "string",
          "description": "The call next",
          "example": ""
        },
        "timeZone": {
          "type": "string",
          "description": "The time zone",
          "example": "Asia/Calcutta"
        },
        "pageSize": {
          "type": "string",
          "description": "Indicator for page size",
          "example": "50"
        }
      }
    },
    "ErrorResponse": {
      "type": "object",
      "description": "error response",
      "properties": {
        "code": {
          "type": "string",
          "description": "Provides the error code",
          "x-example": "System error"
        },
        "userMessage": {
          "type": "string",
          "description": "Provides the error message",
          "x-example": "System error"
        }
      }
    }
  }
}
