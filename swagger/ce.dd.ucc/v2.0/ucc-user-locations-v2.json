{
  "swagger": "2.0",
  "info": {
    "version": "2.0.0",
    "title": "UCC User Location APIs",
    "description": "These are APIs used to set and manage locations for their Org",
    "contact": {
      "name": "Apigee/Documentation Support",
      "email": "TFB_DTD_API_DEVOPS@T-Mobile.com"
    }
  },
  "host": "tmobileqat-qat01.apigee.net",
  "basePath": "/b2b-experience/v2/ucc/user-location",
  "x-servers": [
    {
      "url": "https://tmobileqat-qat01.apigee.net",
      "description": "server path"
    }
  ],
  "tags": [
    {
      "name": "User Location API",
      "description": "User Location API"
    }
  ],
  "securityDefinitions": {
    "OAuth2": {
      "type": "oauth2",
      "description": "oauth2",
      "flow": "accessCode",
      "authorizationUrl": "https://tmobileqat-qat01.apigee.net/v1/oauth2/accesstoken?grant_type=client_credentials",
      "tokenUrl": "https://tmobileqat-qat01.apigee.net/v1/oauth2/accesstoken?grant_type=client_credentials",
      "scopes": {
        "read": "Grants read access"
      }
    }
  },
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/users/{user}": {
      "put": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "description": "\n# Description \n\n  + The purpose of this API is update the user location",
        "operationId": "putUser",
        "summary": "Update User",
        "x-api-pattern": "CreateByName",
        "tags": [
          "User Location API"
        ],
        "parameters": [
          {
            "$ref": "#/parameters/Accept"
          },
          {
            "$ref": "#/parameters/Content-Type"
          },
          {
            "$ref": "#/parameters/Authorization"
          },
          {
            "$ref": "#/parameters/user"
          },
          {
            "name": "body",
            "required": true,
            "description": "Provide request payload",
            "in": "body",
            "schema": {
              "$ref": "#/definitions/UpdateUserRequest"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Success (Ok)",
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "whether cache control is defined",
                "x-example": "no-cache"
              },
              "Location": {
                "type": "string",
                "description": "Location",
                "x-example": "Location"
              }
            },
            "schema": {
              "$ref": "#/definitions/UpdateUserResponse"
            },
            "examples": {
              "application/json": {
                "status": "Success",
                "message": "Location updated successfully"
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Bad Request",
                "userMessage": "string"
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Unauthorized",
                "userMessage": "string"
              }
            }
          },
          "403": {
            "description": "Forbidden",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Forbidden",
                "userMessage": "string"
              }
            }
          },
          "404": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Resource not found",
                "userMessage": "string"
              }
            }
          },
          "405": {
            "description": "Method Not Allowed",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "headers": {
              "Allow": {
                "type": "string",
                "description": "List of supported methods for URI",
                "x-example": "POST"
              }
            },
            "examples": {
              "application/json": {
                "code": "Method Not Allowed",
                "userMessage": "string"
              }
            }
          },
          "406": {
            "description": "Not Acceptable",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Not Acceptable",
                "userMessage": "string"
              }
            }
          },
          "409": {
            "description": "Conflict",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Conflict",
                "userMessage": "string"
              }
            }
          },
          "412": {
            "description": "Precondition Failed",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Precondition Failed",
                "userMessage": "string"
              }
            }
          },
          "415": {
            "description": "Unsupported Media Type",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Unsupported Media Type",
                "userMessage": "string"
              }
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Internal Server Error",
                "userMessage": "string"
              }
            }
          },
          "503": {
            "description": "Service Unavailable",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Service Unavailable",
                "userMessage": "string"
              }
            }
          }
        }
      },
      "delete": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "description": "\n# Description \n\n  + The purpose of this API is to remove the user",
        "operationId": "deleteUser",
        "summary": "Remove User",
        "x-api-pattern": "RemoveResource",
        "tags": [
          "User Location API"
        ],
        "parameters": [
          {
            "$ref": "#/parameters/Accept"
          },
          {
            "$ref": "#/parameters/Authorization"
          },
          {
            "$ref": "#/parameters/user"
          },
          {
            "$ref": "#/parameters/admin"
          }
        ],
        "responses": {
          "204": {
            "description": "Success (Ok)",
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Bad Request",
                "userMessage": "string"
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Unauthorized",
                "userMessage": "string"
              }
            }
          },
          "404": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Resource not found",
                "userMessage": "string"
              }
            }
          },
          "405": {
            "description": "Method Not Allowed",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "headers": {
              "Allow": {
                "type": "string",
                "description": "List of supported methods for URI",
                "x-example": "POST"
              }
            },
            "examples": {
              "application/json": {
                "code": "Method Not Allowed",
                "userMessage": "string"
              }
            }
          },
          "409": {
            "description": "Conflict",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Conflict",
                "userMessage": "string"
              }
            }
          },
          "412": {
            "description": "Precondition Failed",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Precondition Failed",
                "userMessage": "string"
              }
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Internal Server Error",
                "userMessage": "string"
              }
            }
          },
          "503": {
            "description": "Service Unavailable",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Service Unavailable",
                "userMessage": "string"
              }
            }
          }
        }
      }
    },
    "/users/list": {
      "post": {
        "security": [
          {
            "OAuth2": [
              "read"
            ]
          }
        ],
        "description": "\n# Description \n\n  + The purpose of this API is to fetch the user list",
        "operationId": "postUserList",
        "summary": "Fetch user list",
        "x-api-pattern": "ExecuteFunction",
        "tags": [
          "User Location API"
        ],
        "parameters": [
          {
            "$ref": "#/parameters/Accept"
          },
          {
            "$ref": "#/parameters/Content-Type"
          },
          {
            "$ref": "#/parameters/Authorization"
          },
          {
            "name": "body",
            "required": true,
            "description": "Provide request payload",
            "in": "body",
            "schema": {
              "$ref": "#/definitions/CreateUserListRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success (Ok)",
            "headers": {
              "Date": {
                "type": "string",
                "description": "Tue, 11 Sep 2018 20:42:21 GMT",
                "x-example": "Tue, 11 Sep 2018 20:42:21 GMT"
              },
              "Content-Type": {
                "type": "string",
                "description": "application/json;charset=UTF-8",
                "x-example": "application/json;charset=UTF-8"
              },
              "Cache-Control": {
                "type": "string",
                "description": "whether cache control is defined",
                "x-example": "no-cache"
              },
              "Location": {
                "type": "string",
                "description": "Location",
                "x-example": "Location"
              }
            },
            "schema": {
              "$ref": "#/definitions/CreateUserListResponse"
            },
            "examples": {
              "application/json": {
                "status": "Success",
                "message": "Successfully fetched the data",
                "users": [
                  {
                    "tenantId": "1-37OCZNJ",
                    "banId": 966384873,
                    "tuid": "U-18553cbe-8112-4733-ab43-4f0550c1ddbf@null",
                    "userId": "1-40CPK25",
                    "orgId": "1-37OCZNJ",
                    "roleId": 202,
                    "uuid": "U-18553cbe-8112-4733-ab43-4f0550c1ddbf",
                    "locationName": "Andra",
                    "firstName": "Adame",
                    "lastName": "Gill12",
                    "email": "gillad678@gmail.com",
                    "role": "Standard_User",
                    "locationId": 602,
                    "sharedNumber": "",
                    "desklines": 14252401523
                  }
                ],
                "pageSize": 10,
                "totalNoOfRecords": 12,
                "endOfResult": true,
                "page": 1
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Bad Request",
                "userMessage": "string"
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Unauthorized",
                "userMessage": "string"
              }
            }
          },
          "404": {
            "description": "Resource not found",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Resource not found",
                "userMessage": "string"
              }
            }
          },
          "405": {
            "description": "Method Not Allowed",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "headers": {
              "Allow": {
                "type": "string",
                "description": "List of supported methods for URI",
                "x-example": "POST"
              }
            },
            "examples": {
              "application/json": {
                "code": "Method Not Allowed",
                "userMessage": "string"
              }
            }
          },
          "409": {
            "description": "Conflict",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Conflict",
                "userMessage": "string"
              }
            }
          },
          "415": {
            "description": "Unsupported Media Type",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Unsupported Media Type",
                "userMessage": "string"
              }
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Internal Server Error",
                "userMessage": "string"
              }
            }
          },
          "503": {
            "description": "Service Unavailable",
            "schema": {
              "$ref": "#/definitions/ErrorResponse"
            },
            "examples": {
              "application/json": {
                "code": "Service Unavailable",
                "userMessage": "string"
              }
            }
          }
        }
      }
    }
  },
  "parameters": {
    "Accept": {
      "name": "Accept",
      "in": "header",
      "description": "+   The request header must contain the Accept element.\n\n\n+   Accept header is used by HTTP clients to tell the server what content types they'll accept\n\n\n+   The default value is *'application/json'*.",
      "required": false,
      "type": "string",
      "enum": [
        "application/json"
      ],
      "x-example": "application/json"
    },
    "Content-Type": {
      "name": "Content-Type",
      "in": "header",
      "description": "+   The request header must contain the Content-Type element.\n\n\n+   In HTTP requests, API clients must specify the format in which the data is submitted.\n\n\n+   The default value is *'application/json'*.",
      "required": true,
      "type": "string",
      "enum": [
        "application/json;charset=UTF-8"
      ],
      "x-example": "application/json;charset=UTF-8"
    },
    "Authorization": {
      "name": "Authorization",
      "in": "header",
      "description": "+   The request header must contain a valid access token generated by APIGEE.\n\n\n+   The access token is environment specific and is generated dynamically.\n\n\n+   It is mandatory to be passed in the request parameter for authentication.\n\n\n+   The sample Value is *'EGQ94pYxAzk2JRmSoW1GTZifPAJI'*.",
      "required": true,
      "type": "string",
      "enum": [
        "EGQ94pYxAzk2JRmSoW1GTZifPAJI"
      ],
      "x-example": "EGQ94pYxAzk2JRmSoW1GTZifPAJI"
    },
    "user": {
      "name": "user",
      "in": "path",
      "description": "+ Provide the user",
      "required": true,
      "type": "string",
      "pattern": "1-48V7F3M",
      "x-example": "1-48V7F3M"
    },
    "admin": {
      "name": "admin",
      "in": "query",
      "description": "+ Provide the administrator details",
      "required": false,
      "type": "string",
      "pattern": "Admin",
      "x-example": "Admin"
    }
  },
  "definitions": {
    "CreateUserListResponse": {
      "type": "object",
      "description": "Returns the user list response based on search criteria",
      "properties": {
        "status": {
          "type": "string",
          "description": "Return status. (Success/Failure)",
          "example": "Success"
        },
        "message": {
          "type": "string",
          "description": "Return the message for status. (sample value-Successfully fetched the data)",
          "example": "Successfully fetched the data"
        },
        "users": {
          "type": "array",
          "description": "The user array list",
          "items": {
            "type": "object",
            "description": "Return the user array list items",
            "properties": {
              "tenantId": {
                "type": "string",
                "description": "Return tenantId",
                "example": "1-37OCZNJ"
              },
              "banId": {
                "type": "integer",
                "description": "Return banId",
                "example": 966384873
              },
              "tuid": {
                "type": "string",
                "description": "Return tuid",
                "example": "U-18553cbe-8112-4733-ab43-4f0550c1ddbf@null"
              },
              "userId": {
                "type": "string",
                "description": "Return userId",
                "example": "1-40CPK25"
              },
              "orgId": {
                "type": "string",
                "description": "Return organization identifier",
                "example": "1-37OCZNJ"
              },
              "roleId": {
                "type": "integer",
                "description": "Return role identifier",
                "example": 202
              },
              "uuid": {
                "type": "string",
                "description": "Return uuid",
                "example": "U-18553cbe-8112-4733-ab43-4f0550c1ddbf"
              },
              "locationName": {
                "type": "string",
                "description": "Return the name of location",
                "example": "Andra"
              },
              "firstName": {
                "type": "string",
                "description": "Return the firstName",
                "example": "Adame"
              },
              "lastName": {
                "type": "string",
                "description": "Return the lastName",
                "example": "Gill12"
              },
              "email": {
                "type": "string",
                "description": "Return the email address",
                "example": "gillad678@gmail.com"
              },
              "role": {
                "type": "string",
                "description": "Return the role name",
                "example": "Standard_User"
              },
              "locationId": {
                "type": "integer",
                "description": "Return the location identifier",
                "example": 602
              },
              "sharedNumber": {
                "type": "string",
                "description": "Return the shared Number",
                "example": "string"
              },
              "desklines": {
                "type": "integer",
                "description": "Return the desklines",
                "example": 14252401523
              }
            }
          }
        },
        "pageSize": {
          "type": "integer",
          "description": "Sizing the number of records to be displayed on the given page. Sample value is 10",
          "example": 10
        },
        "totalNoOfRecords": {
          "type": "integer",
          "description": "Return total number of records fetched",
          "example": 12
        },
        "endOfResult": {
          "type": "boolean",
          "description": "Indicator (true) to show the end of result",
          "example": true
        },
        "page": {
          "type": "integer",
          "description": "The page number to display the records in the given page. Sample value is 1",
          "example": 1
        }
      }
    },
    "CreateUserListRequest": {
      "type": "object",
      "description": "Create User list Request",
      "properties": {
        "search": {
          "type": "string",
          "description": "Field value will be used in textual search",
          "example": "andra"
        },
        "sort": {
          "type": "string",
          "description": "Sort the data",
          "example": "string"
        },
        "sortOrder": {
          "type": "string",
          "description": "Order for sorting, either ascending or descending",
          "example": "desc"
        },
        "locationName": {
          "type": "string",
          "description": "The location name",
          "example": "Rome"
        },
        "role": {
          "type": "string",
          "description": "The role name",
          "example": "UCC_Admin"
        },
        "productType": {
          "type": "string",
          "description": "The product type",
          "example": "string"
        },
        "lineType": {
          "type": "string",
          "description": "The line/phone service type. The sample value is 'VIRTUAL'",
          "example": "VIRTUAL"
        },
        "status": {
          "type": "string",
          "description": "The status of the line like active, suspended etc..",
          "example": "active"
        },
        "page": {
          "type": "string",
          "description": "The page number to display the records in the given page. Sample value is 1",
          "example": "1"
        },
        "pageSize": {
          "type": "string",
          "description": "Sizing the number of records to be displayed on the given page. Sample value is 10",
          "example": "10"
        }
      }
    },
    "ErrorResponse": {
      "type": "object",
      "description": "error response",
      "properties": {
        "code": {
          "type": "string",
          "description": "Provides the error code"
        },
        "userMessage": {
          "type": "string",
          "description": "Provides the error message"
        }
      }
    },
    "UpdateUserRequest": {
      "type": "object",
      "description": "Create User Request",
      "properties": {
        "locationId": {
          "type": "string",
          "description": "Provide locationId",
          "example": 44
        },
        "firstName": {
          "type": "string",
          "description": "Provide firstName",
          "example": "Jack"
        },
        "lastName": {
          "type": "string",
          "description": "Provide lastName",
          "example": "Sparrow"
        },
        "emailAddress": {
          "type": "string",
          "description": "Provide emailAddress",
          "example": "ksparrow@t-mobile.com"
        },
        "role": {
          "type": "string",
          "description": "Provide role",
          "example": "StandardUser"
        },
        "isActiveFlag": {
          "type": "string",
          "description": "Provide isActiveFlag",
          "example": true
        },
        "tenantId": {
          "type": "string",
          "description": "Provide tenantId",
          "example": "4-3SQ2IS2"
        },
        "roleId": {
          "type": "string",
          "description": "Provide roleId",
          "example": 78
        },
        "createdBy": {
          "type": "string",
          "description": "Provide createdBy",
          "example": "Henry Rolling"
        },
        "updatedBy": {
          "type": "string",
          "description": "Provide updatedBy",
          "example": "Henry Rolling"
        }
      }
    },
    "UpdateUserResponse": {
      "type": "object",
      "description": "Update Location Response",
      "properties": {
        "status": {
          "type": "string",
          "description": "Return status",
          "example": "Success"
        },
        "message": {
          "type": "string",
          "description": "Return message",
          "example": "Location updated successfully"
        }
      }
    }
  }
}
