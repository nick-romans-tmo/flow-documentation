{
  "swagger": "2.0",
  "info": {
    "description": "The Generic version of DEEP.io Publish Event API",
    "version": "1.0",
    "title": "DEEP.io Publish Event POST API - Generic Version"
  },
  "host": "localhost:8080",
  "basePath": "/deep/v1/events",
  "paths": {
    "/{eventType}": {
      "post": {
        "tags": [
          "messaging-controller"
        ],
        "summary": "This Generic API shall be used by producers to publish events into DEEP.io.",
        "operationId": "sendMessageUsingPOST",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "*/*"
        ],
        "parameters": [
          {
            "name": "eventType",
            "in": "path",
            "description": "Unique name of the event",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "event",
            "description": "Event Complex Element",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Event"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Message Published Successfully.",
            "schema": {
              "type": "object",
              "properties": {
                
              }
            }
          },
          "201": {
            "description": "Message Broker has not accepted the message but has been Saved and need resubmission from UI. Please Contact Admin to resubmit"
          },
          "400": {
            "description": "Not a valid Request"
          },
          "429": {
            "description": "Too Many Requests"
          },
          "500": {
            "description": "Error while sending message to Queue OR Error happened inside producer"
          },
          "501": {
            "description": "Rule is Not Configured For EventType"
          },
          "502": {
            "description": "Contract is Not Defined"
          },
          "503": {
            "description": "Contract is defined but payload posted is not valid as per uploaded contract"
          }
        }
      }
    }
  },
  "definitions": {
    "AuditInfo": {
      "type": "object",
      "properties": {
        "accountNumber": {
          "type": "string",
          "example": "F123111",
          "description": "The financial account number."
        },
        "batchId": {
          "type": "string",
          "example": "batch0",
          "description": "Identifer of batch job."
        },
        "customerId": {
          "type": "string",
          "example": "Customer1",
          "description": "Uniquely identifies the Customer."
        },
        "iamUniqueId": {
          "type": "string",
          "example": "deepuser",
          "description": "Unique identifier for Identity and Access Management."
        },
        "lineId": {
          "type": "string",
          "example": "1234567890",
          "description": "Uniquely identifies a  line of service."
        },
        "orderId": {
          "type": "string",
          "example": "order1",
          "description": "Identifier of order."
        },
        "phoneNumber": {
          "type": "string",
          "example": "1234567890",
          "description": "The phone number (MSISDN) associated with the line of service."
        },
        "universalLineId": {
          "type": "string",
          "example": "1234567890",
          "description": "ULID - Universal identifier of the LineofService, as defined in Customer Hub. "
        }
      },
      "description": "Audit information used to search specific event. All elements included in this complex element will be searchable in DEEP.io.",
      "example": "null"
    },
    "HeaderReference": {
      "type": "object",
      "properties": {
        "activityId": {
          "type": "string",
          "example": "null",
          "description": "Unique id alphanumeric value generated and sent by the partner to identify every service request uniquely."
        },
        "applicationId": {
          "type": "string",
          "example": "null",
          "description": "Identifies the application, system or platform name that is being used to initiate the transaction  related to this request."
        },
        "applicationUserId": {
          "type": "string",
          "example": "null",
          "description": "NT id of the rep who is managing the transaction from CARE/RETAIL channels. Required for assisted channels, not required for unassisted channels."
        },
        "authCustomerId": {
          "type": "string",
          "example": "null",
          "description": "Customer Id of Tmo customer that is being served/using the application. (Required for non anonymous flows)"
        },
        "authFinancialAccountId": {
          "type": "string",
          "example": "null",
          "description": "Financial Account Id that the customer that is being served/using the application is working on.  (Required for non anonymous flows)"
        },
        "authLineOfServiceId": {
          "type": "string",
          "example": "null",
          "description": "Line of service Id , within the Financial Account of the customer that is being served/using the application is working on. (Required for non anonymous flows)"
        },
        "channelId": {
          "type": "string",
          "example": "null",
          "description": "Identifies the business unit or sales channel."
        },
        "dealerCode": {
          "type": "string",
          "example": "null",
          "description": "Uniquely identifies the dealer/rep user."
        },
        "interactionId": {
          "type": "string",
          "example": "null",
          "description": "Alphanumeric value represent a common transaction id across all calls made from UI, while completing all business activity needs of a particular customer."
        },
        "masterDealerCode": {
          "type": "string",
          "example": "null",
          "description": "Code that uniquely identifies the master dealer for a large retail partner, such as Apple or Costco. Will not be applicable for TMo channels."
        },
        "segmentationId": {
          "type": "string",
          "example": "null",
          "description": "Identifier of customer���s primary data center."
        },
        "senderId": {
          "type": "string",
          "example": "null",
          "description": "Uniquely identifies an Operation consumer/Partner."
        },
        "sessionId": {
          "type": "string",
          "example": "null",
          "description": "A value populated by the sender used to track the transactions that occur during a session, a long-lasting interaction, managed by the sender. GUID generated by partner. Within one session (sessionid) for Rep/system can serve  multiple customers(interactionid) and in a given customer interation ��� multiple API calls (activityid) will be made to serve the customer. 1 sessionid -> many interactionid 1 interactionid -> many activityid"
        },
        "storeId": {
          "type": "string",
          "example": "null",
          "description": "Unique identifier for the retail store location."
        },
        "terminalId": {
          "type": "string",
          "example": "null",
          "description": "Unique identifier for the retail store terminal."
        },
        "tillId": {
          "type": "string",
          "example": "null",
          "description": "Unique identifier for the retail store terminal till number."
        },
        "timestamp": {
          "type": "string",
          "format": "date-time",
          "example": "null",
          "description": "A timestamp provided by sender to track their workflow."
        },
        "workflowId": {
          "type": "string",
          "example": "null",
          "description": "Name of business purpose/flow, for which the API is being invoked."
        }
      },
      "description": "standard HTTP header elements to be able to deliver them to consumer without interruption",
      "example": "null"
    },
    "Specifications": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string",
          "example": "null"
        },
        "value": {
          "type": "string",
          "example": "null"
        }
      },
      "description": "Name value pair."
    },
    "Event": {
      "type": "object",
      "required": [
        "eventProducerId",
        "eventTime",
        "eventType",
        "payload"
      ],
      "properties": {
        "auditInfo": {
          "$ref": "#/definitions/AuditInfo"
        },
        "eventId": {
          "type": "string",
          "example": "Event-123",
          "description": "Unique identifier for the event generated by the event producer. This element will be searchable in DEEP.io UI"
        },
        "eventProducerId": {
          "type": "string",
          "example": "Producer1",
          "description": "Unique identifier for the producer of the event."
        },
        "eventTime": {
          "type": "string",
          "format": "date-time",
          "example": "2017-03-27T16:20:11.108Z",
          "description": "The date and time that the event occurred."
        },
        "eventType": {
          "type": "string",
          "example": "Activation",
          "description": "Event Name that you want to publish"
        },
        "eventVersion": {
          "type": "string",
          "example": "1.0",
          "description": "version of the event , will be used to distinguish different versions by a consumer"
        },
        "headerReference": {
          "$ref": "#/definitions/HeaderReference"
        },
        "payload": {
          "$ref": "#/definitions/Payload"
        },
        "specifications": {
          "type": "array",
          "example": "null",
          "description": "added for future reference so can ignore.",
          "items": {
            "$ref": "#/definitions/Specifications"
          }
        }
      }
    },
    "Payload": {
      "type": "object",
      "description": "Event specific information that can be modeled using APIv2 or inline elements.",
      "properties": {
        "businessCustomer": {
          "$ref": "#/definitions/BusinessCustomer"
        }
      }
    },
    "BusinessCustomer": {
      "type": "object",
      "properties": {
        "customerId": {
          "description": "Customer Id ",
          "type": "number",
          "format": "integer"
        },
        "customerTypeCode": {
          "type": "string",
          "example": "P",
          "description": "Identifer for Customer type"
        },
        "firstName": {
          "type": "string",
          "example": "Alex",
          "description": "First Name of Customer"
        },
        "middleName": {
          "type": "string",
          "example": "A",
          "description": "Middle Name of Customer"
        },
        "lastName": {
          "type": "string",
          "example": "Wells",
          "description": "Last Name of Customer"
        },
        "companyName": {
          "type": "string",
          "description": "Company Name"
        },
        "department": {
          "type": "string",
          "description": "Department"
        },
        "taxId": {
          "type": "string",
          "example": "012456784",
          "description": "Tax Id"
        },
        "mobilePhoneNumber": {
          "type": "string",
          "example": "1234567890",
          "description": "Mobile Number"
        },
        "homePhoneNumber": {
          "type": "string",
          "example": "1234567890",
          "description": "Home Number"
        },
        "workPhoneNumber": {
          "type": "string",
          "example": "1234567890",
          "description": "Work Number"
        },
        "faxNumber": {
          "type": "string",
          "example": "1234567890",
          "description": "Fax Number"
        },
        "pin": {
          "type": "string",
          "example": "1234",
          "description": "PIN"
        },
        "isAcceptedCreditCheck": {
          "description": "An User Defined Object",
          "type": "boolean"
        },
        "servicePartnerId": {
          "type": "string",
          "example": "MKB",
          "description": "Partner Id"
        },
        "address": {
          "$ref": "#/definitions/Address"
        },
        "creditCheck": {
          "$ref": "#/definitions/CreditCheck"
        }
      },
      "description": "Customer Details",
      "example": "null"
    },
    "Address": {
      "type": "object",
      "properties": {
        "addressId": {
          "description": "Address Id ",
          "type": "number",
          "format": "integer"
        },
        "attention": {
          "type": "string",
          "description": "Attention"
        },
        "address1": {
          "type": "string",
          "example": "123 Main Street",
          "description": "Address1 of customer"
        },
        "address2": {
          "type": "string",
          "description": "Address2 of Customer"
        },
        "suite": {
          "type": "string",
          "description": "Suite"
        },
        "city": {
          "type": "string",
          "description": "City"
        },
        "county": {
          "type": "string",
          "description": "County"
        },
        "state": {
          "type": "string",
          "description": "State"
        },
        "zip": {
          "type": "string",
          "example": "98022",
          "description": "Zip code"
        },
        "countryCode": {
          "type": "string",
          "example": "USA",
          "description": "Country Code"
        },
        "countryType": {
          "type": "string",
          "description": "Country Type"
        },
        "geoCode": {
          "type": "string",
          "description": "Geo Code"
        },
        "statusCode": {
          "type": "string",
          "description": "Status Code"
        }
      },
      "description": "Address information",
      "example": "null"
    },
    "CreditCheck": {
      "type": "object",
      "properties": {
        "creditDate": {
          "description": "Credit check run date ",
          "type": "string",
          "format": "date-time"
        },
        "expirationDate": {
          "type": "string",
          "format": "date-time"
        },
        "statusCode": {
          "type": "string",
          "description": "status code"
        },
        "creditValue": {
          "type": "number",
          "format": "integer",
          "description": "credit value"
        },
        "originalCreditScore": {
          "type": "string",
          "description": "original credit score"
        },
        "creditClass": {
          "type": "string",
          "description": "credit score"
        },
        "creditBureau": {
          "type": "string",
          "description": "credit Bureau"
        },
        "creditReferenceNum": {
          "type": "string",
          "description": "Credit Tracking Number"
        },
        "creditFailReasonCode": {
          "type": "string",
          "description": "Credit Fail Reason Code"
        },
        "otherCreditFailreason": {
          "type": "string",
          "description": "Other Credit Fail reason Code"
        },
        "numberOfLinesRequested": {
          "type": "number",
          "format": "integer",
          "description": "Number Services requested"
        },
        "creditUser": {
          "type": "string",
          "description": "Credit user"
        },
        "creditComments": {
          "type": "string",
          "description": "Credit Comments"
        },
        "notifyDate": {
          "type": "string",
          "format": "date-time"
        },
        "storeId": {
          "type": "string",
          "description": "Store Id"
        },
        "dealerCode": {
          "type": "string",
          "description": "Sale Code"
        },
        "decisionCriteria": {
          "type": "string",
          "description": "decision Criteria"
        },
        "reKeyCount": {
          "type": "string",
          "description": "Re key Count"
        },
        "deposit": {
          "type": "number",
          "format": "integer",
          "description": "deposit"
        },
        "numberOfLines": {
          "type": "number",
          "format": "integer",
          "description": "Number Of lines"
        },
        "newCreditScore": {
          "type": "number",
          "format": "integer",
          "description": "new Credit Score"
        },
        "scoreRange": {
          "type": "string",
          "description": "Score range"
        },
        "modelName": {
          "type": "string",
          "description": "Model Name"
        },
        "commonCustomerId": {
          "type": "string",
          "description": "Common Customer Id"
        },
        "vendor": {
          "type": "string",
          "description": "vendor"
        }
      },
      "description": "Credit Check informations",
      "example": "null"
    }
  }
}