{
	"swagger": "2.0",
	"info": {
		"title": "Credit Profile",
		"description": "credit profile like MPEC, LCA, ECB, ECA and CECL",
		"version": "2.0",
		"contact": {
			"name": "CreditOriginationQC",
			"email": "CreditOriginationQC@T-Mobile.com"
		}
	},
	"host": "services.t-mobile.com",
	"basePath": "/customer-credit/v2/credit-profiles",
	"schemes": ["https"],
	"paths": {
		"/personal": {
			"post": {
				"tags": ["POST-ConsumerCreditProfile"],
				"operationId": "consumercreditprofile",
				"consumes": ["application/json"],
				"produces": ["application/json"],
				"parameters": [{
					"name": "creditProfileRequest",
					"in": "body",
					"description": "An User Defined Object",
					"required": true,
					"schema": {
						"$ref": "#/definitions/creditProfileRequest"
					}
				},
				{
					"$ref": "#/parameters/AuthorizationParam"
				},
				{
					"$ref": "#/parameters/Content-typeParam"
				},
				{
					"$ref": "#/parameters/AcceptParam"
				},
				{
					"$ref": "#/parameters/applicationuseridParam"
				},
				{
					"$ref": "#/parameters/sessionidParam"
				},
				{
					"$ref": "#/parameters/interactionidParam"
				},
				{
					"$ref": "#/parameters/activityidParam"
				},
				{
					"$ref": "#/parameters/senderidParam"
				},
				{
					"$ref": "#/parameters/channelidParam"
				},
				{
					"$ref": "#/parameters/applicationidParam"
				},
				{
					"$ref": "#/parameters/servicetransactionidParam"
				},
				{
					"$ref": "#/parameters/workflowidParam"
				},
				{
					"$ref": "#/parameters/timestampParam"
				},
				{
					"$ref": "#/parameters/storeidParam"
				},
				{
					"$ref": "#/parameters/terminalidParam"
				},
				{
					"$ref": "#/parameters/tillidParam"
				},
				{
					"$ref": "#/parameters/dealercodeParam"
				},
				{
					"$ref": "#/parameters/segmentationidParam"
				},
				{
					"$ref": "#/parameters/authcustomeridParam"
				},
				{
					"$ref": "#/parameters/authfinancialaccountidParam"
				},
				{
					"$ref": "#/parameters/authlineofserviceidParam"
				},
				{
					"$ref": "#/parameters/Access-Control-Request-HeadersParam"
				},
				{
					"$ref": "#/parameters/Access-Control-Request-MethodParam"
				},
				{
					"$ref": "#/parameters/If-MatchParam"
				},
				{
					"$ref": "#/parameters/If-Modified-SinceParam"
				},
				{
					"$ref": "#/parameters/If-None-MatchParam"
				},
				{
					"$ref": "#/parameters/OriginParam"
				}],
				"responses": {
					"200": {
						"description": "Ok",
						"schema": {
							"$ref": "#/definitions/creditProfileResponse"
						},
						"headers": {
							"Access-Control-Allow-Headers": {
								"type": "string"
							},
							"Access-Control-Allow-Methods": {
								"type": "string"
							},
							"Access-Control-Allow-Origin": {
								"type": "string"
							},
							"Cache-Control": {
								"type": "string"
							},
							"Content-Length": {
								"type": "string"
							},
							"Content-Type": {
								"type": "string"
							},
							"ETag": {
								"type": "string"
							},
							"Expires": {
								"type": "string"
							},
							"Location": {
								"type": "string"
							},
							"servicetransactionid": {
								"type": "string"
							}
						}
					},
					"401": {
						"description": "Client not authorized",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"403": {
						"description": "Client access denied",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"404": {
						"description": "Resource not found",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"409": {
						"description": "Invalid data",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"500": {
						"description": "System Error",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"502": {
						"description": "Backend system problem",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"503": {
						"description": "Service unavailable",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					}
				},
				"description": "This API will provide customer credit profile like MPEC, LCA, ECB, ECA and CECL based on FAN, CICD, Credit Class and account tenure."
			}
		}
	},
    "definitions": {
			"error": {
            "description": "Error object.",
            "type": "object",
            "properties": {
                "code": {
                    "description": "A specific T-Mobile error code.",
                    "type": "string"
                },
                "userMessage": {
                    "description": "A human-readable message describing the error.",
                    "type": "string"
                },
                "systemMessage": {
                    "description": "Backend system error message.",
                    "type": "string"
                }
            }
        },
        "errors": {
            "description": "The array of error objects. As defined in https://tmobileusa.sharepoint.com/teams/EnterpriseServices/ESG%20Standards%20Documents/APIs/T-Mobile%20API%20Design%20Guidelines%20v1.0.docx",
            "type": "object",
            "properties": {
                "errors": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/error"
                    }
                }
            }
        },
		"creditProfileRequest": {
			"type": "object",
			"description": "An User Defined Object",
			"required": [
                "transactionType"
            ],
			"properties": {
				"transactionType": {
					"description": "transaction type",
					"type": "string",
					"enum": [
						"ACTIVATION",
						"AAL",
						"UPGRADE",
						"JUMP",
						"EXCHANGE"
					]
				},
				"commonCustomerId": {
					"description": "Common customer identifier (CCID)",
					"type": "string"
				},
				"financialAccountNumber": {
					"description": "Customer billing account number",
					"type": "string"
				},
				"accountType": {
					"description": "Account Type.",
					"type": "string"
				},
				"accountSubType": {
					"description": "Account Sub Type.",
					"type": "string"
				},
				"creditClass": {
					"description": "Credit class of the customer when the account is active.",
					"type": "string"
				},
				"behaviorScore": {
					"description": "A numeric summary of the banks experience with the customer",
					"type": "integer",
					"format": "int32"
				},
				"accountTenure": {
					"description": "Number of days account is opened.",
					"type": "integer",
					"format": "int32"
				},
				"financeType": {
					"$ref": "#/definitions/FinanceType"
				},
				"orderLocationStateCode": {
					"description": "State Code where store is located and where purchase of this loan/lease is carried out - in case of Retail Channel",
					"type": "string"
				},	
				"billingAddressStateCode": {
					"description": "Billing Address State code of the customer",
					"type": "string"
				},
				"exchangeReservationIds": {
                    "description": "List of device exchange reservation ids ",
                    "type": "array",
                    "minItems": 1,
                    "items": {
                        "$ref": "#/definitions/exchangeReservationIds"
                    }
                },
				"subscriberInfo": {
					"description": "An array of subscriberInfo objects.",
					"type": "array",
					"minItems": 1,
					"items": {
					"$ref": "#/definitions/SubscriberInfo"
					}
				},
			}
		},
		"exchangeReservationIds": {
            "type": "object",
            "description": "An User Defined Object",
            "properties": {
                "creditReservationId": {
                    "description": "Identifier of credit reservation which may be loan, lease or poip.",
                    "type": "string"
                }
            }
        },
		"SubscriberInfo": {
			"type": "object",
			"description": "An array of subscriberInfo objects.",
			"required": [
				"transactionType"
			],
			"properties": {
				"phoneNumber": {
					"description": "Phone number",
					"type": "string"
				},
				"ratePlanCode": {
					"description": "Rate plan code of the subscriber(SOC)",
					"type": "string"
				}
			}
		},
		"FinanceType": {
			"description": "Type of financing.",
			"type": "string",
			"enum": ["LOAN",
			"LEASE"]
		},
		"creditProfileResponse": {
			"type": "object",
			"description": "An User Defined Object",
			"properties": {
				"creditClass": {
					"description": "Customer's credit classification code",
					"type": "string"
				},
				"subscriberEligibility": {
					"$ref": "#/definitions/subscriberEligibilityProfile"
				},
				"accountStatus": {
					"description": "Customer FAN status",
					"type": "string"
				},
				"creditLimitsProfile": {
					"$ref": "#/definitions/creditLimitsProfile"
				},
				"creditRiskProfile": {
					"$ref": "#/definitions/creditRiskProfile"
				}
			}
		},
		"creditLimitsProfile": {
			"type": "object",
			"properties": {
				"tenure": {
					"$ref": "#/definitions/TenureRange"
				},
				"maxPotentialEquipmentCredit": {
					"description": "Maximum potential equipment credit amount.",
					"type": "number",
					"format": "double"
				},
				"lineContributionAmount": {
					"description": "Line Contribution amount for each line.",
					"type": "number",
					"format": "double"
				},
				"equipmentCreditAvailableAmount": {
					"description": "The available equipment credit amount. It is the credit limit minus the credit used.",
					"type": "number",
					"format": "double"
				},
				"equipmentCreditUsed": {
					"description": "The amount of equipment financing for the account with all active loan/lease current balance amounts, plus any reserved loan/lease amounts. Also known as the ECLB",
					"type": "number",
					"format": "double"
				},
				"currentEquipmentCreditLimit": {
					"description": "An User Defined Object",
					"type": "number",
					"format": "double"
				}
			}
		},
		"creditRiskProfile": {
			"type": "object",
			"properties": {
				"creditRiskProfileId": {
					"description": "customer Credit Risk Profile Id",
					"type": "integer",
					"format": "int32"
				},
				"description": {
					"description": "Description",
					"type": "string"
				},
				"accountTenure": {
					"description": "account Tenure",
					"type": "integer",
					"format": "int32"
				},
				"behaviorScore": {
					"description": "A numeric summary of the banks experience with the customer",
					"type": "integer",
					"format": "int32"
				},
				"creditClass": {
					"description": "Customer's credit classification code",
					"type": "string"
				},
				"financeType": {
					"$ref": "#/definitions/FinanceType"
				},
				"tenure": {
					"$ref": "#/definitions/TenureRange"
				}
			}
		},
		"TenureRange": {
			"type": "object",
			"properties": {
				"daysMin": {
					"description": "Minumum Tenure Days Range",
					"type": "integer",
					"format": "int32"
				},
				"daysMax": {
					"description": "Maximum Tenure Days Range",
					"type": "integer",
					"format": "int32"
				}
			}
		},
		"subscriberEligibilityProfile": {
			"type": "object",
			"properties": {
				"phoneNumber" : {
					"description": "Phone number",
					"type": "string"
				},
				"eligibility": {
					"description" : "Eligibility indicator",
					"type": "boolean"
				}
			}
		}
	},
    "securityDefinitions": {
		"Oauth": {
			"type": "oauth2",
			"tokenUrl": "https://ilab02.core.op.api.t-mobile.com/customer-credit/v1/credit-profiles/personal",
			"flow": "application",
			"scopes": {
				
			}
		}
	},
    "parameters": {
		"AuthorizationParam": {
			"name": "Authorization",
			"in": "header",
			"required": true,
			"type": "string"
		},
		"Content-typeParam": {
			"name": "Content-type",
			"in": "header",
			"required": true,
			"type": "string"
		},
		"AcceptParam": {
			"name": "Accept",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"applicationuseridParam": {
			"name": "applicationuserid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"sessionidParam": {
			"name": "sessionid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"interactionidParam": {
			"name": "interactionid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"activityidParam": {
			"name": "activityid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"workflowidParam": {
			"name": "workflowid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"timestampParam": {
			"name": "timestamp",
			"in": "header",
			"required": false,
			"type": "string",
			"format": "date-time"
		},
		"storeidParam": {
			"name": "storeid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"terminalidParam": {
			"name": "terminalid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"tillidParam": {
			"name": "tillid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"dealercodeParam": {
			"name": "dealercode",
			"in": "header",
			"required": true,
			"type": "integer",
			"format": "int32"
		},
		"segmentationidParam": {
			"name": "segmentationid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"authcustomeridParam": {
			"name": "authcustomerid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"authfinancialaccountidParam": {
			"name": "authfinancialaccountid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"authlineofserviceidParam": {
			"name": "authlineofserviceid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"Access-Control-Request-HeadersParam": {
			"name": "Access-Control-Request-Headers",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"Access-Control-Request-MethodParam": {
			"name": "Access-Control-Request-Method",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"If-MatchParam": {
			"name": "If-Match",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"If-Modified-SinceParam": {
			"name": "If-Modified-Since",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"If-None-MatchParam": {
			"name": "If-None-Match",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"OriginParam": {
			"name": "Origin",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"senderidParam": {
			"name": "senderid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"channelidParam": {
			"name": "channelid",
			"in": "header",
			"required": true,
			"type": "string"
		},
		"applicationidParam": {
			"name": "applicationid",
			"in": "header",
			"required": true,
			"type": "string"
		},
		"servicetransactionidParam": {
			"name": "servicetransactionid",
			"in": "header",
			"required": false,
			"type": "string"
		}
	}

}

