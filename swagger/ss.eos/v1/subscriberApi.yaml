swagger: '2.0'
info:
  description: Account Api
  version: '0.2'
  title: customer account Api
  contact:
    email: Global_Invaders@T-Mobile.com
host: eos.corporate.t-mobile.com
schemes:
  - https
basePath: /v1/subscriber
tags:
  - name: customer account api
    description: 'fetches customer account, bill, upgrade eligibility and usage details.'
produces:
  - application/json
consumes:
  - application/json
parameters:
  X-B3-TraceId:
    name: X-B3-TraceId
    in: header
    description: >-
      This is interactionId or correlationId equivalent that is required to
      passed to MW/Backend. If request is from app, they also pass this to a
      web-view request as X-B3-TraceId header.
    required: true
    type: string
  X-B3-SpanId:
    name: X-B3-SpanId
    in: header
    description: App need to pass X-B3-SpanId in header.
    required: true
    type: string
  Channel-Id:
    name: channel_id
    in: header
    description: 'DESKTOP, MOBILE, TMO-APP.'
    required: true
    type: string
  Authorization:
    name: Authorization
    in: header
    description: Access token that is received from IAM after authentication.
    required: true
    type: string
  AccessToken:
    name: access_token
    in: header
    description: Access token that is received from IAM after authentication.
    required: true
    type: string
  AplicationID:
    name: application_id
    in: header
    description: Front end application. ‘MYTMO’ for eServices.
    required: true
    type: string
  User-Token:
    name: user-token
    in: header
    description: JWT token need to be sent.
    required: false
    type: string
  Tmo-Id:
    name: tmo-id
    in: header
    description: TMOId assoiated with IAM account.
    required: false
    type: string
  Msisdn:
    name: msisdn
    in: header
    description: Logged in MSISDN
    required: true
    type: string
  BAN:
    name: ban
    in: header
    description: BAN associated with logged in MSISDN.
    required: true
    type: string
  USN:
    name: usn
    in: header
    description: Unique session id for user. It will be returned by IAM.
    required: false
    type: string
  Application-Client:
    name: application_client
    in: header
    description: >-
      This will be the unique identifier that is used with the OS Store
      [user-agent]
    required: false
    type: string
  Application-Version-Code:
    name: application_version_code
    in: header
    description: 'This is the App Client version number [user-agent].'
    required: false
    type: string
  Device-OS:
    name: device_os
    in: header
    description: 'The operating system of the mobile device [user-agent].'
    required: false
    type: string
  OS-Version:
    name: os_version
    in: header
    description: 'The operating system version of the mobile device [user-agent].'
    required: false
    type: string
  OS-Language:
    name: os_language
    in: header
    description: 'The language of the user''s device operating system [user-agent]'
    required: false
    type: string
  Device-Manufacturer:
    name: device_manufacturer
    in: header
    description: 'The manufacturer of the user''s device [user-agent]'
    required: false
    type: string
  Device-Model:
    name: device_model
    in: header
    description: 'The model of the user''s device [user-agent]'
    required: false
    type: string
  Marketing-Cloud-Id:
    name: marketing_cloud_id
    in: header
    description: The adobe id for the user
    required: false
    type: string
  Sd-Id:
    name: sd_id
    in: header
    description: another adobe id
    required: false
    type: string
  Adobe-UUID:
    name: adobe_uuid
    in: header
    description: A unique adobe uuid that is used with Adobe partners
    required: false
    type: string
  Source:
    name: Source
    in: query
    description: The source from where the request comes from
    required: false
    type: string
  Service:
    name: Service
    in: query
    description: The Service to which the request is forwarded to
    required: false
    type: string
  MsisdnList:
    name: msisdnList
    in: header
    description: All msisdns associated for the Parent msisdn
    required: true
    type: array
    items:
      type: string
  LoggedInUserPerm:
    name: loggedInUserPerm
    in: header
    description: Permission type of loggedinMsisdn
    required: false
    type: string
paths:
  /account:
    get:
      tags:
        - getAccount
      summary: Get Account and Bill details for home page
      description: Get account detail.
      operationId: getAccount
      parameters:
        - $ref: '#/parameters/X-B3-TraceId'
        - $ref: '#/parameters/X-B3-SpanId'
        - $ref: '#/parameters/Channel-Id'
        - $ref: '#/parameters/Authorization'
        - $ref: '#/parameters/AccessToken'
        - $ref: '#/parameters/AplicationID'
        - $ref: '#/parameters/User-Token'
        - $ref: '#/parameters/Tmo-Id'
        - $ref: '#/parameters/Msisdn'
        - $ref: '#/parameters/BAN'
        - $ref: '#/parameters/USN'
        - $ref: '#/parameters/Application-Client'
        - $ref: '#/parameters/Application-Version-Code'
        - $ref: '#/parameters/Device-OS'
        - $ref: '#/parameters/OS-Version'
        - $ref: '#/parameters/OS-Language'
        - $ref: '#/parameters/Device-Manufacturer'
        - $ref: '#/parameters/Device-Model'
        - $ref: '#/parameters/Marketing-Cloud-Id'
        - $ref: '#/parameters/Sd-Id'
        - $ref: '#/parameters/Adobe-UUID'
        - $ref: '#/parameters/MsisdnList'
        - $ref: '#/parameters/LoggedInUserPerm'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Account'
        '400':
          description: Bad request
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized
          schema:
            $ref: '#/definitions/Error'
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/Error'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
  /prepaidaccount:
    get:
      tags:
        - getPrepaidAccount
      summary: Get Account and Bill details for home page
      description: Get account detail.
      operationId: getPrepaidAccount
      parameters:
        - $ref: '#/parameters/X-B3-TraceId'
        - $ref: '#/parameters/X-B3-SpanId'
        - $ref: '#/parameters/Channel-Id'
        - $ref: '#/parameters/Authorization'
        - $ref: '#/parameters/AccessToken'
        - $ref: '#/parameters/AplicationID'
        - $ref: '#/parameters/User-Token'
        - $ref: '#/parameters/Tmo-Id'
        - $ref: '#/parameters/Msisdn'
        - $ref: '#/parameters/BAN'
        - $ref: '#/parameters/USN'
        - $ref: '#/parameters/Application-Client'
        - $ref: '#/parameters/Application-Version-Code'
        - $ref: '#/parameters/Device-OS'
        - $ref: '#/parameters/OS-Version'
        - $ref: '#/parameters/OS-Language'
        - $ref: '#/parameters/Device-Manufacturer'
        - $ref: '#/parameters/Device-Model'
        - $ref: '#/parameters/Marketing-Cloud-Id'
        - $ref: '#/parameters/Sd-Id'
        - $ref: '#/parameters/Adobe-UUID'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/PrepaidAccount'
        '400':
          description: Bad request
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized
          schema:
            $ref: '#/definitions/Error'
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/Error'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
            
  /prepaidfeatures:
    put:
      tags:
        - updatePrepaidFeatures
      summary: add/remove bingeon and datamaximizer
      description: update Prepaid Features
      operationId: updatePrepaidFeatures
      parameters:
        - $ref: '#/parameters/X-B3-TraceId'
        - $ref: '#/parameters/X-B3-SpanId'
        - $ref: '#/parameters/Channel-Id'
        - $ref: '#/parameters/Authorization'
        - $ref: '#/parameters/AccessToken'
        - $ref: '#/parameters/AplicationID'
        - $ref: '#/parameters/User-Token'
        - $ref: '#/parameters/Tmo-Id'
        - $ref: '#/parameters/Msisdn'
        - $ref: '#/parameters/USN'
        - $ref: '#/parameters/Application-Client'
        - $ref: '#/parameters/Application-Version-Code'
        - $ref: '#/parameters/Device-OS'
        - $ref: '#/parameters/OS-Version'
        - $ref: '#/parameters/OS-Language'
        - $ref: '#/parameters/Device-Manufacturer'
        - $ref: '#/parameters/Device-Model'
        - $ref: '#/parameters/Marketing-Cloud-Id'
        - $ref: '#/parameters/Sd-Id'
        - $ref: '#/parameters/Adobe-UUID'
        - name: request
          in: body
          required: true
          schema:
            $ref: '#/definitions/FeatureRequest'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/FeatureResponse'
        '400':
          description: Bad request
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized
          schema:
            $ref: '#/definitions/Error'
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/Error'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
  /lines:
    get:
      tags:
        - getSubscriberLines
      summary: Get basic info of subscriber and all lines associated to BAN
      description: Get basic info of subscriber and all lines associated to BAN
      operationId: getSubscriberLines
      parameters:
        - $ref: '#/parameters/X-B3-TraceId'
        - $ref: '#/parameters/X-B3-SpanId'
        - $ref: '#/parameters/Channel-Id'
        - $ref: '#/parameters/Authorization'
        - $ref: '#/parameters/AccessToken'
        - $ref: '#/parameters/AplicationID'
        - $ref: '#/parameters/User-Token'
        - $ref: '#/parameters/Tmo-Id'
        - $ref: '#/parameters/Msisdn'
        - $ref: '#/parameters/BAN'
        - $ref: '#/parameters/USN'
        - $ref: '#/parameters/Application-Client'
        - $ref: '#/parameters/Application-Version-Code'
        - $ref: '#/parameters/Device-OS'
        - $ref: '#/parameters/OS-Version'
        - $ref: '#/parameters/OS-Language'
        - $ref: '#/parameters/Device-Manufacturer'
        - $ref: '#/parameters/Device-Model'
        - $ref: '#/parameters/Marketing-Cloud-Id'
        - $ref: '#/parameters/Sd-Id'
        - $ref: '#/parameters/Adobe-UUID'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/SubscriberInfo'
        '400':
          description: Bad request
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized
          schema:
            $ref: '#/definitions/Error'
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/Error'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
            
  /partnertoken:            
   get:
      tags:
        - getPartnerToken
      summary: Get encrypted user data for partner
      description: Get encrypted user data for partner
      operationId: fetchPartnerToken
      parameters:
        - $ref: '#/parameters/X-B3-TraceId'
        - $ref: '#/parameters/X-B3-SpanId'
        - $ref: '#/parameters/Channel-Id'
        - $ref: '#/parameters/Authorization'
        - $ref: '#/parameters/AccessToken'
        - $ref: '#/parameters/AplicationID'
        - $ref: '#/parameters/User-Token'
        - $ref: '#/parameters/Tmo-Id'
        - $ref: '#/parameters/Msisdn'
        - $ref: '#/parameters/BAN'
        - $ref: '#/parameters/USN'
        - $ref: '#/parameters/Application-Client'
        - $ref: '#/parameters/Application-Version-Code'
        - $ref: '#/parameters/Device-OS'
        - $ref: '#/parameters/OS-Version'
        - $ref: '#/parameters/OS-Language'
        - $ref: '#/parameters/Device-Manufacturer'
        - $ref: '#/parameters/Device-Model'
        - $ref: '#/parameters/Marketing-Cloud-Id'
        - $ref: '#/parameters/Sd-Id'
        - $ref: '#/parameters/Adobe-UUID'
        - $ref: '#/parameters/Source'
        - $ref: '#/parameters/Service'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/PartnerToken'
        '400':
          description: Bad request
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized
          schema:
            $ref: '#/definitions/Error'
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/Error'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'         
definitions:
  SubscriberInfo:
    type: object
    properties:
      accountNumber:
        type: string
        description: account number
      billingStartDate:
        type: string
        description: billing start date for account
      billingEndDate:
        type: string
        description: billing end date for account
      lines:
        type: array
        items:
          $ref: '#/definitions/Line'
  PartnerToken:
       type: object
       properties:
        tokenType:
          type: string
          description: enum string which describes the way token needs to be passed to third party
          enum:
          - cookie
          - query
        partnerUrl:
          type: string
          description: partner service redirection url
        partnerToken:
           type: string
           description: partner Token   
  Account:
    type: object
    properties:
      myLine:
        $ref: '#/definitions/Line'
      otherLines:
        type: array
        items:
          $ref: '#/definitions/Line'
      bill:
        $ref: '#/definitions/Bill'
      autoPay:
        $ref: '#/definitions/AutoPay'
      accountNumber:
        type: string
        description: customer account number
      employee:
        type: boolean
        description: employee account
      accountType:
        type: string
        description: customer account type
      accountSubType:
        type: string
        description: customer account sub type
      lastLoginTimeStamp:
        type: string
        description: customer last login time stamp
      isPAEligible:
        type: boolean
        description: payment arrangement link on home page.
  PrepaidAccount:
    type: object
    properties:
      myLine:
        $ref: '#/definitions/PrepaidLine'
      bill:
        $ref: '#/definitions/PrepaidBill'
  Line:
    type: object
    properties:
      msisdn:
        type: string
        description: phone number of the customer
      imei:
        type: string
        description: imei number of the phone
      firstName:
        type: string
        description: first name of the customer
      lastName:
        type: string
        description: last name of the customer
      permission:
        type: string
        description: customer permission level
        enum:
          - PAH
          - FULL
          - STANDARD
          - RESTRICTED
          - NOACCESS
          - MASTER
          - NONMASTER_PERMISSIONED
          - NONMASTER_RESTRICTED
      productType:
        type: string
        description: gsm or mbb
      device:
        $ref: '#/definitions/Device'
      ratePlan:
        $ref: '#/definitions/RatePlan'
      addOns:
        type: integer
        description: customer additional features
      deviceUpgrade:
        $ref: '#/definitions/DeviceUpgrade'
      usage:
        $ref: '#/definitions/Usage'
      suspended:
        type: boolean
        description: is customer involuntary suspended.
      lineType:
        type: string
        description: virtual line
  PrepaidLine:
    type: object
    properties:
      msisdn:
        type: string
        description: phone number of the customer
      firstName:
        type: string
        description: first name of the customer
      lastName:
        type: string
        description: last name of the customer
      device:
        $ref: '#/definitions/Device'
      ratePlan:
        $ref: '#/definitions/PrepaidRatePlan'
      pendingRatePlan:
        $ref: '#/definitions/PrepaidPendingRatePlan'
      usage:
        $ref: '#/definitions/PrepaidUsage'
      goldRewards:
        $ref: '#/definitions/GoldRewards'
      socsAvailable:
        $ref: '#/definitions/Socs'
      enableFamilySoc:
        type: boolean
        description: enables family allowances quicklinks on home page.
      imei:
        type: string
        description: imei number of msisdn.
  Bill:
    type: object
    properties:
      dueDate:
        type: string
        description: bill due date
      lastPaymentDate:
        type: string
        description: customer last payment date
      lastPaymentAmount:
        type: number
        format: double
        description: customer last payment amount
      balanceDueAmount:
        type: number
        format: double
        description: balance due amount
      pastDueAmount:
        type: number
        format: double
        description: past due amount
      paperlessBilling:
        type: boolean
        description: is customer on papaerless billing
      pastDueDate:
        type: string
        description: customer pastDueDate if has past due amount
  PrepaidBill:
    type: object
    properties:
      accountBalance:
        type: number
        format: double
        description: Balance Amount
      accountStatus:
        type: string
        description: Status of the account
      nextPaymentAmount:
        type: number
        format: double
        description: Balance Amount
      nextChargeDate:
        type: string
        description: Upcoming charge date
      useByDate:
        type: string
        description: Date and time it can be used for Legacy paygo customers
      autoPay:
        type: boolean
        description: is customer on autopay
      canEnrollAutoPay:
        type: boolean
        description: is customer on autopay
      lowBalanceWarning:
        type: boolean
        description: is customer on autopay
  AutoPay:
    type: object
    properties:
      enabled:
        type: boolean
        description: is customer on autopay
      autoPayDate:
        type: string
        description: autopay scheduled date
      paymentType:
        type: string
        description: 'customer payment type, card, checking'
      cardType:
        type: string
        enum:
          - VISA
          - AMEX
          - DISC
          - MASTERCARD
      cardNumber:
        type: string
        description: last 4 digit of card or checking number
  RatePlan:
    type: object
    properties:
      name:
        type: string
      planType:
        type: string
        enum:
          - OTHER
          - SIMPLECHOICE
          - TMOONE
      socCode:
        type: string
        description: rate plan soc code
  PrepaidRatePlan:
    type: object
    properties:
      name:
        type: string
      dataOnly:
        type: boolean
        description: is data only plan
      payAsYouGo:
        type: boolean
        description: is pay as you go plan
      refillRestricted:
        type: boolean
        description: is refill Restricted
      planType:
        type: string
        enum:
          - DAILY
          - MONTHLY
          - OTHER     
  PrepaidPendingRatePlan:
    type: object
    properties:
      name:
        type: string
  Device:
    type: object
    properties:
      name:
        type: string
        description: name of customer device on network
      color:
        type: string
        description: color of customer device on network
      memory:
        type: string
        description: memory variant of customer device on network
      deviceImage:
        type: string
        description: customer device image url
  DeviceUpgrade:
    properties:
      eligible:
        type: boolean
        description: is eligible for upgrade
      programType:
        type: string
        enum:
          - NONE
          - JUMP1
          - JUMP1IPHONEUPGRADE
          - JUMP1JUMP3IPHONEUPGRADE
          - JUMP2JUMP3IPHONEUPGRADE
          - JUMP2
          - JUMP3
          - EIP
          - JOD
        description: upgrade program type
      totalDeviceAmount:
        type: number
        format: double
      daysRemainingUntilUpgrade:
        type: integer
        description: number of days remaining until eligible for upgrade
      amountRemainingUntilUpgrade:
        type: number
        format: double
      daysRemainingUntilLeaseEnds:
        type: integer
        description: JOD Lease - days remaining until lease ends
  Usage:
    properties:
      data:
        $ref: '#/definitions/bucket'
  bucket:
    properties:
      total:
        type: string
      used:
        type: string
      unit:
        type: string
  PrepaidUsage:
    type: object
    properties:
      minutes:
        type: string
        description: Number of minutes avaiable to the customer
      messages:
        type: string
        description: Messages avaiable to the customer
      minMessagesRemaining:
        type: string
        description: Minutes & Messages remaining
      usedData:
        type: number
        format: double
        description: Data used
      totalPlanData:
        type: number
        format: double
        description: Total plan data
      planDataHighSpeed:
        type: number
        format: double
        description: Plan data throttled at
      tetheringData:
        type: number
        format: double
        description: Tethering Data used
      tetheringDataHighSpeed:
        type: number
        format: double
        description: Tethering Data throttled at
      networkData:
        type: number
        format: double
        description: Network Data used
      dataStashRemaining:
        type: number
        format: double
      dataText:
        type: string
        description: Replaces Minutes and Messages for two and three dollar plans
  GoldRewards:
    type: object
    properties:
      isEligible:
        type: boolean
      balance:
        type: number
        format: double
        description: balance avaiable
  Socs:
    type: object
    properties:
      bingeOn:
        type: boolean
      bingeOnAvailable:
        type: boolean
      dataMaximizer:
        type: boolean
      dataMaximizerAvailable:
        type: boolean
  Error:
    type: object
    properties:
      code:
        type: string
        description: Error code
      message:
        type: string
        description: Error message
  FeatureRequest:
    type: object
    properties:
      featureName:
        type: string
        description: bingeon or data maximizer
        enum:
          - BINGEON
          - DATAMAXIMIZER
      updateAction:
        type: string
        description: status description
        enum:
          - ADD
          - REMOVE
  FeatureResponse:
    type: object
    properties:
      statusCode:
        type: string
        description: status code
      statusDescription:
        type: string
        description: status description