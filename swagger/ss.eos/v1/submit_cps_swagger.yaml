swagger: '2.0'
info:
  description: Change plan and services API
  version: 1.0.0
  title: Change plan and services
  termsOfService: 'http://swagger.io/terms/'
  contact:
    email: api@t-mobile.io
host: eos.corporate.t-mobile.com
basePath: /v1/cps
tags:
  - name: plans
    description: Change plan and services API
schemes:
  - https
  
parameters:
  X-B3-TraceId: 
    name: X-B3-TraceId
    type: string
    in: header
    description: This is interactionId or correlationId equivalent that is required to passed to MW/Backend.
    required: true
  X-B3-SpanId:
    name: X-B3-SpanId
    type: string
    in: header 
    description: App passes this X-B3-SpanId header
    required: false
  Channel:
    name: Channel
    type: string
    in: header
    description: represents the client channel - desktop or mobile or app
    required: true
  Channel-Version:
    name: Channel-Version
    in: header
    description: represents the channel version if the channel is app
    required: false
    type: string
  Authorization:
    name: Authorization 
    type: string
    in: header
    description: Access token that is received from IAM after authentication
    required: true
  User-Token:
    name: User-Token
    in: header
    type: string
    description: JWT token
    required: false   
  Tmo-Id:
    name: Tmo-Id
    in: header
    type: string
    description: TMOID associated with IAM account. This is for future work.
    required: false
  Phone-Number:
    name: Phone-Number
    in: header
    type: string
    description: MSISDN - phone number in action
    required: false
  Billing-Account-Number:
    name: Billing-Account-Number
    in: header
    type: string
    description: BAN - account number in action
    required: false             
  session-num:
    name: session-num
    type: string
    in: header
    description: session identifier, a unique key. 
  usn:
    name: usn
    in: header
    type: string
    description: Unique session id for user. It will be returned by IAM. (CIS requirement)
    required: false
paths:
  /submit:
    post:
      operationId: submitPlanChange
      summary: Change plan and services     
      parameters:
        - name: changePlanRequest
          in: body
          schema:
            $ref: '#/definitions/ChangePlanRequest' 
        - $ref: '#/parameters/X-B3-TraceId'
        - $ref: '#/parameters/X-B3-SpanId'
        - $ref: '#/parameters/Channel'
        - $ref: '#/parameters/Channel-Version'
        - $ref: '#/parameters/Authorization'
        - $ref: '#/parameters/User-Token'
        - $ref: '#/parameters/Tmo-Id'
        - $ref: '#/parameters/Phone-Number'        
        - $ref: '#/parameters/Billing-Account-Number'
        - $ref: '#/parameters/session-num'
        - $ref: '#/parameters/usn'
        
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/ChangePlanResponse'
        '400':
          description: Bad request
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
definitions:
  ChangePlanRequest:
    type: object
    properties:
      accountNumber:
        type: string
        description: BAN in action
      msisdn:
        type: string
        description: MSISDN in action
      serviceChanges:
        type: array
        items:
          $ref: '#/definitions/ServiceChange'
    required:
      - accountNumber
      - msisdn
      - serviceChanges
  ServiceChange:
    type: object
    properties:
      accountLevel:
        type: string
        description: Is change an account level or line level
      changeDetails:
        $ref: '#/definitions/ChangeDetails'
        description: details of service being changed
    required:
      - changeDetails
  ChangeDetails:
    type: object
    properties:
      changeType:
        type: string
        description: type of soc in action ex service or data pass or data plan, allowed values are changeService/changeDataPass/changePlan for data service, data pass and data plan change respectively.
      serviceSubtype:
        type: string
        description: Sub type for the change type, populated for data plan flow  
      action:
        type: string
        description: action being taken on this soc ex ADD or REMOVE.
      soc:
        type: string
        description: soc of the item-in-action
      sequenceNumber:
        type: integer
        format: int64
        description: sequenceNumber of the item-in-action
      effectiveDate:
        type: string
        description: Date the given action has to be effective on the SOC
      expirationDate:
        type: string
        description: Date the given action has to be expired on the SOC
    required:
      - changeType
      - action
      - soc
  ChangePlanResponse:
    type: object
    properties:
      statusDetails:
        $ref: '#/definitions/StatusDetails'
  StatusDetails:
    type: object
    properties:
      status:
        type: string
        description: This field represent status of the service(SUCCESS or ERROR).
      statusCode: 
        type: string
        description: This field represent status code(200 , 404 , 500)
      statusMessage:
        type: string
        description: This field represent status message coming from MW.
  Error:
    type: object
    properties:
      category:
        type: string
        description: Error category
      code:
        type: integer
        format: int32
        description: error code
      message:
        type: string
        description: error message
      details:
        type: string
        description: Additional information for the error