swagger: '2.0'

info:
  version: "0.0.1"
  title: "EOS Account Management Service"

host: eos.eitss.tmobile.com
produces:
  - application/json
consumes:
  - application/json
basePath: "/v1"
schemes:
  - https
  - http
parameters:
  accountNumber:
    name: accountNumber
    in: path
    description: customer's billing account number
    required: true
    type: number
    format: double
  code:
    name: code
    in: path
    description: identifier for stored payment method
    type: string
    required: true
  msisdn:
    name: msisdn
    in: query
    description: Customer 
    type: string
    required: true
  paymentMethod:
    name: paymentMethod
    in: body
    schema: 
      $ref: "#/definitions/PaymentMethod"
    required: true
          
# Describe your paths here
paths:
  /accounts/{accountNumber}/paymentmethods:
    # get all payment methods
    get:
      # all stored payment methods for a specific account
      description: |
        Gets stored payment methods on an account. Returns collection of `PaymentMethod` objects.
      operationId: getPaymentMethods
      parameters:
        - $ref: "#/parameters/accountNumber"
        - $ref: "#/parameters/msisdn"
        
      # Expected responses for this operation:
      responses:
        # Response code
        200:
          description: Successful response
          schema:
            type: array
            items: 
              $ref: "#/definitions/PaymentMethod"
        400:
          description: Bad request
        500:
          description: Internal server error
          schema:
            $ref: "#/definitions/Error"
    post:
      description: Create new payment method
      operationId: addNewPaymentMethod
      parameters:
        - $ref: "#/parameters/accountNumber"
        - $ref: "#/parameters/paymentMethod"
        - name: validate
          type: boolean
          in: query
      responses:
        200:
          description: OK
          schema:
            $ref: "#/definitions/PaymentMethod"
        400:
          description: Bad Request
          schema:
            $ref: "#/definitions/Error"
        500:
          description: Internal Server Error
          schema:
            $ref: "#/definitions/Error"
        
  /accounts/{accountNumber}/paymentmethods/{code}:
    get:
      description: Retrieve payment method by code
      operationId: getPaymentMethodByCode
      parameters:
         - $ref: "#/parameters/accountNumber"
         - $ref: "#/parameters/code"
      responses:
        200:
          description: OK
          schema:
            $ref: "#/definitions/PaymentMethod"
        400:
          description: Bad request
        500:
          description: Internal server error
          schema:
            $ref: "#/definitions/Error" 
    put:
      description: Update stored payment methods to add new store a new payment instrument
      operationId: updatePaymentMethod
      parameters:
        - $ref: "#/parameters/accountNumber"
        - $ref: "#/parameters/code"
        - $ref: "#/parameters/paymentMethod"
      responses:
        200:
          description: Ok
          schema:
            $ref: "#/definitions/PaymentMethod"
        400:
          description: Bad request
        500:
          description: Internal server error
          schema:
            $ref: "#/definitions/Error" 
    delete:
      description: Delete payment method
      parameters:
         - $ref: "#/parameters/accountNumber"
         - $ref: "#/parameters/code"
      responses:
        200:
          description: OK
        400:
          description: Bad request
        500:
          description: Internal server error
          schema:
            $ref: "#/definitions/Error" 
  
definitions:
  Card:
    description: Card payment
    type: object
    properties:
      cardAlias:
        type: string
        description: Card alias
      cardNumber:
        type: string
        description: Card number
      cardHolderName:
        type: string
        description: Card holder name (as it appears on card)
      cardHolderFirstName:
        type: string
        description: Card holder first name
      cardHolderLastName:
        type: string
        description: Card holder last name
      expirationMonthYear:
        type: string
        description: Expiration month/year
      defaultPaymentMethod: 
        type: boolean
        description: Default payment method
      type: 
        type: string
        enum:
          - Visa
          - Mastercard
          - Amex
      billingAddress:
        $ref: "#/definitions/Address"
      cvv:
        type: string
        description: verification code
    required:
      - billingAddress
      - cardHolderName
      - type
      - expirationMonthYear
      - cvv
      
  Address:
    type: object
    description: Address
    properties:
      line1:
        type: string
        description: address line 1
      line2:
        type: string
        description: address line 2
      city:
        type: string
        description: city
      state:
        type: string
        description: state
      zip:
        type: string
        description: zip code
    required:
      - line1
      - city
      - state
      - zip
      
  CreditCard:
    type: object
    description: credit card payment instrument
    allOf:
      - $ref: "#/definitions/Card"
      
  DebitCard:
    type: object
    description: bank card payment instrument
    allOf:
      - $ref: "#/definitions/Card"
      - properties:
          pin:
            type: integer
            description: Pin code for bank card
    required:
      - pin
  
  BankAccount:
    type: object
    description: Bank account payment instrument
    properties:
      accountType:
        type: string
        enum:
          - Checking
          - Savings
      accountNumber:
        type: string
        description: account number
      routingNumber:
        type: string
        description: bank routing number
    required:
      - accountType
      - accountNumber
      - routingNumber

  PaymentMethod:
    type: object
    description: Stored payment method
    properties:
      blocked:
        type: boolean
        description: Customer blocked from using this payment method
      code:
        type: string
        description: default payment method code identifying current payment instrument
      storedPaymentMethodId:
        type: string
        description: stored payment method id
      status:
        type: string
        description: current payment method status
      defaultPaymentMethod:
        type: boolean
        description: default payment method
      store:
        type: boolean
        description: save this payment instrument
      creditCard:
        $ref: "#/definitions/CreditCard"
      debitCard:
        $ref: "#/definitions/DebitCard"
      bankAccount:
        $ref: "#/definitions/BankAccount"
          
  Error:
    type: object
    properties:
      code:
        type: integer
        format: int32
        description: error code
      message:
        type: string
        description: error message
      details:
        type: string
        description: Additional information for the error
        