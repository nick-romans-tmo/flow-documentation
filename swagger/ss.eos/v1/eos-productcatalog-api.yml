swagger: "2.0"
info:
  description: "This is the API specification for the Product microservice"
  version: "1.0.0"
  title: "Experience Product APIs"
host: "localhost:8007"
basePath: "/v1"

parameters:
     oAuth:
          name: Authorization
          type: string
          in: header
          description: Bearer Token for Oauth Validation
          required: true
     JWTToken:
          name: JWTToken
          type: string
          in: header
          description: JWT token for authorizatoin
          required: true       
     transactionId:
          name: transactionId
          type: string
          in: header
          description: unique GUID for the transaction
          required: true
     correlationId:
          in: header
          name: correlationId
          type: string
          description: This is used to tie multiple transactions across multiple calls (eg msisdn could be a correlation id , another example is a unique key generated which will be used across multiple api calls in the flow)
          required: false
     transactionBusinessKey:
          in: header
          name: transactionBusinessKey
          type: string
          description: This is place holder for a functional key , could be BAN or MSISDN .
          required: false    
     transactionBusinessKeyType:
          in: header
          name: transactionBusinessKeyType
          type: string
          description: Indicates the functional key example "BAN","MSISDN","SKU" .
          required: false      
     transactionType:
          in: header
          name: transactionType
          type: string
          description: Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc 
          required: true
     applicationId:
          in: header
          name: applicationId
          type: string
          description: application id like MyTMO or TMO
          required: true
     channelId:
          in: header
          name: channelId
          type: string
          description: This indicates whether the original request came from Mobile / web / retail / mw  etc
          required: true
     clientId:
         in: header
         name: clientId
         type: string
         description: unique identifier for the client (could be e-servicesUI , MWSAPConsumer etc )

     dealerCode:
         in: header
         name: dealerCode
         type: string
         description: Middleware specific field wil be defaulted to '000000' for mw requests
paths:
  /catalog/availability:
    get:
      tags:
      - "Product availability details."
      summary: "GET the Product availability details."
      description: "This API will return the Product availability details."
      operationId: "getProductAvailabilityDetailsUsingGET"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:

        - $ref: "#/parameters/oAuth"
        - $ref: "#/parameters/JWTToken"        
        - $ref: "#/parameters/transactionId"
        - $ref: "#/parameters/correlationId"
        - $ref: "#/parameters/applicationId"
        - $ref: "#/parameters/channelId"
        - $ref: "#/parameters/clientId"
        - $ref: "#/parameters/transactionBusinessKey"
        - $ref: "#/parameters/transactionBusinessKeyType"
        - $ref: "#/parameters/transactionType"
        - $ref: "#/parameters/dealerCode"        
        - name: "channelCode"
          in: "query"
          description: "Channel Code (MYT, AAL, HSU)."
          required: true
          type: "string"
          enum:
              - "MYT"
              - "HSU"
              - "AAL"
             
        - name: "skus"
          in: "query"
          description: "Array of SKUs."
          required: true
          type: "array"
          items:
            type: "string"
          collectionFormat: "multi"
      responses:
        200:
          description: "Returns the Product availability details."
          schema:
            $ref: "#/definitions/ProductAvailability"
        400:
          description: "Invalid Request Message Processing - Technical Error Bad Request"
          schema:
            $ref: "#/definitions/Error"
        401:
          description: "Unauthorized"
        403:
          description: "Forbidden"
        404:
          description: "Authorization Error"
          schema:
            $ref: "#/definitions/Error"
        500:
          description: "Internal Server Error"
          schema:
            $ref: "#/definitions/Error"
  /catalog/deviceFilters:
    get:
      tags:
      - "List of Filter Options for filtering the Products."
      summary: "GET the List of Filter Options for filtering the Products."
      description: "This API will return the List of Filter Options for filtering\
        \ the Products."
      operationId: "getFilterOptionsUsingGET"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - $ref: "#/parameters/oAuth"   
      - $ref: "#/parameters/JWTToken"
      
      - $ref: "#/parameters/transactionId"
      - $ref: "#/parameters/correlationId"
      - $ref: "#/parameters/applicationId"
      - $ref: "#/parameters/channelId"
      - $ref: "#/parameters/clientId"
      - $ref: "#/parameters/transactionBusinessKey"
      - $ref: "#/parameters/transactionBusinessKeyType"
      - $ref: "#/parameters/transactionType"
      - $ref: "#/parameters/dealerCode" 
      - name: "channelCode"
        in: "query"
        description: "Channel Code (MYT, AAL, HSU). MYT has both HSU and AAL data"
        required: false
        type: "string"
        enum:
        - "MYT"
        - "HSU"
        - "AAL"
      - name: "marketId"
        in: "query"
        description: "Market ID"
        required: false
        type: "string"
      responses:
        200:
          description: "Returns the List of Filter Options for filtering the Products."
          schema:
            $ref: "#/definitions/ProductFilterData"
        400:
          description: "Invalid Request Message Processing - Technical Error Bad Request"
          schema:
            $ref: "#/definitions/Error"
        401:
          description: "Unauthorized"
        403:
          description: "Forbidden"
        404:
          description: "Authorization Error"
          schema:
            $ref: "#/definitions/Error"
        500:
          description: "Internal Server Error"
          schema:
            $ref: "#/definitions/Error"
  /catalog/financeOptions:
    post:
      tags:
      - "Pricing/Financing Options for Products."
      summary: "GET the Pricing/Financing Options for Products."
      description: "This API will return the Pricing/Financing Options (Loan/Lease/Club)\
        \ for Products, for all the given CRP(s)."
      operationId: "getFinanceOptionsUsingGET"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - $ref: "#/parameters/oAuth"  
      - $ref: "#/parameters/JWTToken"      
      - $ref: "#/parameters/transactionId"
      - $ref: "#/parameters/correlationId"
      - $ref: "#/parameters/applicationId"
      - $ref: "#/parameters/channelId"
      - $ref: "#/parameters/clientId"
      - $ref: "#/parameters/transactionBusinessKey"
      - $ref: "#/parameters/transactionBusinessKeyType"
      - $ref: "#/parameters/transactionType"
      - $ref: "#/parameters/dealerCode" 
      - name: "FinanceOptionsRequest"
        in: "body"
        description: "Array of SKUs."
        required: true
        schema:
         $ref: "#/definitions/financeOptionsRequest"

      responses:
        200:
          description: "Returns the Pricing/Financing Options for Products."
          schema:
            $ref: "#/definitions/FinanceOptions"
        400:
          description: "Invalid Request Message Processing - Technical Error Bad Request"
          schema:
            $ref: "#/definitions/Error"
        401:
          description: "Unauthorized"
        403:
          description: "Forbidden"
        404:
          description: "Authorization Error"
          schema:
            $ref: "#/definitions/Error"
        500:
          description: "Internal Server Error"
          schema:
            $ref: "#/definitions/Error"
  /catalog/products:
    post:
      tags:
      - "List of Products"
      summary: "List of Products"
      description: "This API will return the List of Products, for the given filter\
        \ criteria wrapped in SearchRequest."
      operationId: "retrieveProductsUsingPOST_1"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - $ref: "#/parameters/oAuth"  
      - $ref: "#/parameters/JWTToken"      
      - $ref: "#/parameters/transactionId"
      - $ref: "#/parameters/correlationId"
      - $ref: "#/parameters/applicationId"
      - $ref: "#/parameters/channelId"
      - $ref: "#/parameters/clientId"
      - $ref: "#/parameters/transactionBusinessKey"
      - $ref: "#/parameters/transactionBusinessKeyType"
      - $ref: "#/parameters/transactionType"
      - $ref: "#/parameters/dealerCode" 
      - in: "body"
        name: "searchRequest"
        description: "Search Request"
        required: true
        schema:
          $ref: "#/definitions/SearchRequest"
      responses:
        200:
          description: "Returns the List of Products"
          schema:
            $ref: "#/definitions/ProductInfo"
        201:
          description: "Created"
        400:
          description: "Invalid Request Message Processing - Technical Error Bad Request"
          schema:
            $ref: "#/definitions/Error"
        401:
          description: "Unauthorized"
        403:
          description: "Forbidden"
        404:
          description: "Authorization Error"
          schema:
            $ref: "#/definitions/Error"
        500:
          description: "Internal Server Error"
          schema:
            $ref: "#/definitions/Error"
  /catalog/products/family:
    post:
      tags:
      - "List of Products"
      summary: "List of Products"
      description: "This API will return the List of Products, for the given filter\
        \ criteria wrapped in SearchRequest."
      operationId: "retrieveGroupedProductsUsingPOST"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - $ref: "#/parameters/oAuth"
      - $ref: "#/parameters/JWTToken"
      - $ref: "#/parameters/transactionId"
      - $ref: "#/parameters/correlationId"
      - $ref: "#/parameters/applicationId"
      - $ref: "#/parameters/channelId"
      - $ref: "#/parameters/clientId"
      - $ref: "#/parameters/transactionBusinessKey"
      - $ref: "#/parameters/transactionBusinessKeyType"
      - $ref: "#/parameters/transactionType"
      - $ref: "#/parameters/dealerCode"       
      - in: "body"
        name: "searchRequest"
        description: "Search Request"
        required: true
        schema:
          $ref: "#/definitions/GroupBySearchRequest"
      responses:
        200:
          description: "Returns the List of Products"
          schema:
            $ref: "#/definitions/GroupedProductInfo"
        201:
          description: "Created"
        400:
          description: "Invalid Request Message Processing - Technical Error Bad Request"
          schema:
            $ref: "#/definitions/Error"
        401:
          description: "Unauthorized"
        403:
          description: "Forbidden"
        404:
          description: "Authorization Error"
          schema:
            $ref: "#/definitions/Error"
        500:
          description: "Internal Server Error"
          schema:
            $ref: "#/definitions/Error"
definitions:
  GroupingInfo:
    type: "object"
    properties:
      data:
        type: "array"
        items:
          $ref: "#/definitions/GroupedData"
  SearchRequest:
    type: "object"
    properties:
      filterByFields:
        type: "array"
        description: "List of Fields to be used for filtering the records."
        items:
          $ref: "#/definitions/FilterByField"
      page:
        type: "integer"
        format: "int32"
        example: 1
        description: "Page Number for which the records to be filtered."
      size:
        type: "integer"
        format: "int32"
        example: 30
        description: "Total size of the records (for a particular Page) to be returned."
      sortByFields:
        type: "array"
        description: "List of Fields to be used for sorting the records."
        items:
          $ref: "#/definitions/SortByField"
  ProductFilterValue:
    type: "object"
    properties:
      value:
        type: "string"
  Rating:
    type: "object"
    properties:
      reviewCount:
        type: "number"
        format: "double"
        description: "The total review count"
      reviewRating:
        type: "number"
        format: "double"
        description: "Average review rating"
  Image:
    type: "object"
    properties:
      name:
        type: "string"
      type:
        type: "string"
      url:
        type: "string"
  CapabilityGroup:
    type: "object"
    properties:
      description:
        type: "string"
      name:
        type: "string"
  Feature:
    type: "object"
    properties:
      description:
        type: "string"
      name:
        type: "string"
  BoxContents:
    type: "object"
    properties:
      name:
        type: "string"
      values:
        type: "array"
        items:
          type: "string"
  ProductFilterData:
    type: "object"
    properties:
      data:
        type: "array"
        items:
          $ref: "#/definitions/ProductFilterOption"
  ProductFilterOption:
    type: "object"
    properties:
      name:
        type: "string"
      values:
        type: "array"
        items:
          $ref: "#/definitions/ProductFilterValue"
  Pagination:
    type: "object"
    properties:
      currentPageItems:
        type: "integer"
        format: "int32"
      pageNumber:
        type: "integer"
        format: "int32"
      pageSize:
        type: "integer"
        format: "int32"
      totalPages:
        type: "integer"
        format: "int32"
      totalRecordCount:
        type: "integer"
        format: "int32"
  EIPInfo:
    type: "object"
    properties:
      termModifier:
        type: "string"
      downPaymentModifier:
        $ref: "#/definitions/Amount"
      financeAmount:
        $ref: "#/definitions/Amount"
      dueTodayAmount:
        $ref: "#/definitions/Amount"
  Amount:
    type: "object"
    properties:
      unit:
        type: "string"
      value:
        type: "number"
        format: "double"
  GroupBySearchRequest:
    type: "object"
    properties:
      filterByFields:
        type: "array"
        description: "List of Fields to be used for filtering the records."
        items:
          $ref: "#/definitions/FilterByField"
      groupSortOptions:
        type: "array"
        description: "Sorting option to be used for the groups."
        items:
          type: "string"
          enum:
          - "OFFER_PRICE_ASC"
          - "OFFER_PRICE_DESC"
          - "FEATURED_ASC"
          - "FEATURED_DESC"
          - "RATING_ASC"
          - "RATING_DESC"
      page:
        type: "integer"
        format: "int32"
        example: 1
        description: "Page Number for which the records to be filtered."
      size:
        type: "integer"
        format: "int32"
        example: 30
        description: "Total size of the records (for a particular Page) to be returned."
      sortByFields:
        type: "array"
        description: "List of Fields to be used for sorting the records."
        items:
          $ref: "#/definitions/SortByField"
  ProductAvailability:
    type: "object"
    properties:
      data:
        type: "array"
        items:
          $ref: "#/definitions/ProductAvailabilityData"
  ProductData:
    type: "object"
    properties:
      boxContents:
        $ref: "#/definitions/BoxContents"
      capabilityGroup:
        type: "array"
        items:
          $ref: "#/definitions/CapabilityGroup"
      category:
        type: "string"
        enum:
        - "DEVICE"
        - "TABLET"
        - "ACCESSORIES"
        - "WEARABLE"
        - "SIMS"
        - "PHONE"
      channelCode:
        type: "string"
      color:
        type: "string"
      condition:
        type: "string"
      description:
        type: "string"
      deviceId:
        type: "string"
      deviceTier:
        type: "string"
      family:
        type: "string"
      featured:
        type: "integer"
        format: "int32"
      features:
        type: "array"
        items:
          $ref: "#/definitions/Feature"
      images:
        type: "array"
        items:
          $ref: "#/definitions/Image"
      make:
        type: "string"
      manufacturerType:
        type: "string"
      marketId:
        type: "string"
      memory:
        type: "integer"
        format: "int32"
      memoryUOM:
        type: "string"
      modelName:
        type: "string"
      operatingSystem:
        type: "string"
      pricing:
        $ref: "#/definitions/Price"
      productAvailableTime:
        type: "string"
      productStatus:
        type: "string"
      productType:
        type: "string"
      promotions:
        type: "array"
        items:
          $ref: "#/definitions/PromotionType"
      ratings:
        $ref: "#/definitions/Rating"
      skuNumber:
        type: "string"
      specifications:
        type: "array"
        items:
          $ref: "#/definitions/Specification"
      swatch:
        type: "string"
      taccode:
        type: "string"
  ProductInfo:
    type: "object"
    properties:
      data:
        type: "array"
        items:
          $ref: "#/definitions/ProductData"
  FinanceType:
    type: "object"
    properties:
      lease:
          $ref: "#/definitions/Lease"
      loans:
          $ref: "#/definitions/Loan"
  PromotionType:
    type: "object"
    properties:
      additionalOfferDescription:
        type: "string"
        description: "Additional details about the offer."
      additionalOfferLink:
        type: "string"
      additionalOfferText:
        type: "string"
      applyToCart:
        type: "string"
      description:
        type: "string"
        description: "Promotion description"
      type:
        type: "string"
        description: "Type of the promotion"
      unit:
        type: "string"
      value:
        type: "string"
        description: "Value of the promotion"
  Lease:
    type: "object"
    properties:
      configuredDownPayment:
        $ref: "#/definitions/Amount"
      configuredTerm:
        type: "string"
      financeDetails:
        type: array
        items:
          $ref: "#/definitions/financeDetails"
  Loan:
    type: "object"
    properties:
      configuredDownPayment:
        $ref: "#/definitions/Amount"
      configuredTerm:
        type: "string"
      financeDetails:
        type: array
        items:
          $ref: "#/definitions/financeDetails"
  GroupedProductInfo:
    type: "object"
    properties:
      deviceFamilyInfo:
        $ref: "#/definitions/GroupingInfo"
      paginationMetadata:
        $ref: "#/definitions/Pagination"
  GroupedData:
    type: "object"
    properties:
      devicesInfo:
        type: "array"
        items:
          $ref: "#/definitions/ProductData"
      family:
        type: "string"
  SortByField:
    type: "object"
    properties:
      fieldName:
        type: "string"
        example: "pricing.offerPrice"
        description: "Name of the Field to be used for sorting the records."
      sortOrder:
        type: "string"
        example: "ASC"
        description: "Sort Order/Direction (ASC/DESC) to be used while sorting the\
          \ records."
  Error:
    type: "object"
    properties:
      code:
        type: "integer"
        format: "int32"
      details:
        type: "string"
      message:
        type: "string"
  ProductAvailabilityData:
    type: "object"
    properties:
      availabilityStatus:
        type: "string"
        description: " SKU Availability status"
      availableQuantity:
        type: "integer"
        format: "int32"
        description: "Available quantity of the SKU"
      estimatedShipDateFrom:
        type: "string"
        description: "Estimated shipping start date"
      estimatedShipDateTo:
        type: "string"
        description: "Estimated shipping end date"
      preorderAvailableEndTime:
        type: "string"
        description: "pre order end time"
      preorderAvailableStartTime:
        type: "string"
        description: "pre order start time"
      skuNumber:
        type: "string"
        description: "the product SKU"
  Specification:
    type: "object"
    properties:
      name:
        type: "string"
        description: "The specification key, e.g. Memory"
      values:
        type: "array"
        description: "The actual specification, e.g. 4GB RAM, 32GB ROM"
        items:
          type: "string"
  FinanceOptionsData:
    type: "object"
    properties:
      CreditLineInfo:
        type: object
        properties:
            availableCredit:
                $ref: "#/definitions/Amount"
        
      skuItems:
        type: "array"
        items:
          $ref: "#/definitions/SkuItem"
  FilterByField:
    type: "object"
    properties:
      fieldName:
        type: "string"
        example: "category"
        description: "Name of the Field to be used for filtering the records."
      fieldValues:
        type: "array"
        description: "List of Values against the Value of the fieldName to be matched."
        items:
          type: "string"

  Price:
    type: "object"
    properties:
      baseCurrency:
        type: "string"
      basePrice:
        type: "number"
        format: "double"
      offerCurrency:
        type: "string"
      offerPrice:
        type: "number"
        format: "double"
  FinanceOptions:
    type: "object"
    properties:
      data:
        $ref: "#/definitions/FinanceOptionsData"
  SkuItem:
    type: "object"
    properties:    
      financeTypes:
        $ref: "#/definitions/FinanceType"
      skuNumber:
        type: "string"
      promotionAmount:
        $ref: "#/definitions/Amount"
      suggestedRetailPrice:
        $ref: "#/definitions/Amount"
  financeOptionsRequest:
    type: object
    properties: 
      creditLine:
        $ref: "#/definitions/creditLineSummaryRequest"
      skuList:
        type: array
        items:
          type: string
      crpList:
        type: array
        items:
          type: string     
       
  creditLineSummaryRequest:
    type: "object"
    properties:
      billingAccountNumber:
        type: string
        description: Billing Account Number
      accountType: 
        type: string
        description: Billing Account Type
      accountStatus:
        type: string
        description: Billing Account status     
  creditLineSummaryRequestDetail:
    type: "object"
    properties:
      billingAccountNumber:
        type: string
        description: Billing Account Number
      accountType: 
        type: string
        description: Billing Account Type
      accountStatus:
        type: string
        description: Billing Account status       
      creditClass:
        type: string 
        description: Credit Class of customer
      state:
        type: string
        description: Billing State of customer
      ratePlanType:
        type: string
        description: The customer Price Plan category in Samson. It can be either New plan types (N), Classic Plan (Flavor of Old plan types) within New Plan types (C) OR (A) All plan type will be returned if valid. If this element is empty then the same will be passed along to EIP and existing ECL, ECLB, ECLA will be returned.
      ratePlanContractTerm:
        type: number
        description: BAN level Plan contract term
      numberOfLines:
        type: number
        description: Total Number of Activelines
      returnCreditLineBalanceOnly:
        type: boolean
        description: If this flag is set to true, the service returns only CreditLineBalance (ECLB) otherwise all ECL values
      subscriberEIPParameters:
        type: object
        properties:
           overrideEIPParameters:
              type: boolean
              description: If overrideEIPParameters='true', then service will Override input Subscriber EIP Info with samson infomation and also will return EIP parameters in response for these subscribers
      creditRiskProfile:
        $ref: "#/definitions/creditRiskProfile"
      transactionType: 
        type: string
        description: Type of transaction being made in this interaction with EIP. Possible value being JUMP and JUMP2.

      includeLeaseEligibility:
        type: string
        description: Used to find whether it is eligible for lease or not.
  creditRiskProfile:        
    type: "object"
    properties:
      identifier:
        type: number
        description: A unique value that represents the credit risk profile of a Customer based on various credentials of his account. This may be a number such as '1', '2'..
      accountTenure:
        type: number
        description: Number of days since the customer's BAN is associated with T-Mobile. For new customers, this may be set to '0'.
      creditClass:
        type: string
        description: A unique identifier dynamically assigned to a Customer based on his current credit history. Usually this is a single alphabet such 'A', 'B' or a number such as '1', '2'.
      behaviorScore:
        type: string
        description: A value that is dynamically assigned to a Customer based on his various account activities. For new Customers this may not be returned.
      decisionCriteria:
        type: string
        description: The customer’s credit risk profile is determined based on several factors. This element specifies what has been used to assign the credit risk profile ID. A valid value is 'CREDIT_CLASS_BASED'
      creditApplicationReferenceNumber:
        type: string
        description: A unique reference number that can locate the Customer’s credit application. On rare conditions when the System does not have this information, it will be defaulted to '0'.
      financeType:
        type: string
        description: Defines the valid financeType.
        enum:
         - 'LEASE'
         - 'LOAN'
         - 'ALL'
  financeDetails:
    type: object
    properties:
      crp:
        type: "string"
      eipInfo:
        $ref: "#/definitions/EIPInfo"

        