swagger: '2.0'
info:
  title: EOS Promotion APIs
  description: Micro services for Tradein & standalone Promotions
  version: 1.0.0
schemes:
  - https
  - http
basePath: /v1/promotions
produces:
  - application/json
consumes:
  - application/json
parameters:
  oAuth:
    name: Authorization
    type: string
    in: header
    description: Bearer Token for Oauth Validation.
    required: true
  transactionId:
    name: transactionId
    type: string
    in: header
    description: unique GUID for the transaction.
    required: true
  correlationId:
    in: header
    name: correlationId
    type: string
    description: >-
      This is used to tie multiple transactions across multiple calls (a unique
      key generated will be used across multiple api calls in the flow).
    required: true
  transactionBusinessKey:
    in: header
    name: transactionBusinessKey
    type: string
    description: 'This is place holder for a functional key, the value is MSISDN.'
    required: true
  transactionBusinessKeyType:
    in: header
    name: transactionBusinessKeyType
    type: string
    description: 'Indicates the functional key, "MSISDN" for trade-in APIs.'
    required: true
  transactionType:
    in: header
    name: transactionType
    type: string
    description: 'Indicates the kind of transaction eg AAL, CIHU, inventoryfeed etc .'
    required: true
  applicationId:
    in: header
    name: applicationId
    type: string
    description: Application id like MyTMO or TMO.
    required: true
  channelId:
    in: header
    name: channelId
    type: string
    description: >-
      This indicates whether the original request came from Mobile / web /
      retail / mw  etc.
    required: true
  clientId:
    in: header
    name: clientId
    type: string
    description: >-
      Unique identifier for the client (could be e-servicesUI , MWSAPConsumer
      etc ).
    required: true
paths:
  /browse:
    post:
      summary: Search for Promotions based on specified criteria.
      description: >-
        Lookup Promotions based on multiple search criteria. This is used to
        drive marketing offers. The actual manifestation of promotional
        discounts is part of the products or carts operations.
      parameters:
        - $ref: '#/parameters/oAuth'
        - $ref: '#/parameters/transactionId'
        - $ref: '#/parameters/correlationId'
        - $ref: '#/parameters/applicationId'
        - $ref: '#/parameters/channelId'
        - $ref: '#/parameters/clientId'
        - $ref: '#/parameters/transactionBusinessKey'
        - $ref: '#/parameters/transactionBusinessKeyType'
        - $ref: '#/parameters/transactionType'
        - name: body
          in: body
          description: request containing the search criteria
          schema:
            $ref: '#/definitions/promoSearchCriteria'
      tags:
        - promotions
      responses:
        '200':
          description: List of Promotions details
          schema:
            $ref: '#/definitions/PromotionDetails'
        '400':
          description: Bad request
          schema:
            $ref: '#/definitions/Errors'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/Errors'
  /prices:
    post:
      summary: Lookup for products pricing.
      description: >-
        Lookup for product pricing which can also deliver promotional price if
        promotion details are provided.
      parameters:
        - $ref: '#/parameters/oAuth'
        - $ref: '#/parameters/transactionId'
        - $ref: '#/parameters/correlationId'
        - $ref: '#/parameters/applicationId'
        - $ref: '#/parameters/channelId'
        - $ref: '#/parameters/clientId'
        - $ref: '#/parameters/transactionBusinessKey'
        - $ref: '#/parameters/transactionBusinessKeyType'
        - $ref: '#/parameters/transactionType'
        - name: body
          in: body
          description: Body of the request
          schema:
            $ref: '#/definitions/ProductsPrice'
          required: true
      tags:
        - Pricing
      responses:
        '200':
          description: product price details
          schema:
            $ref: '#/definitions/ProductPriceDetails'
        '400':
          description: Bad request
          schema:
            $ref: '#/definitions/Errors'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/Errors'
definitions:
  ProductsPrice:
    type: object
    properties:
      crpId:
        type: string
        description: credit risk profile id of the customer in context.
      priceType:
        type: array
        description: Price type
        enum:
          - FRP
          - EIP
        items:
          type: string
      skuCode:
        type: array
        description: sku codes
        items:
          type: string
      promotionDetails:
        type: object
        description: Details of Promotion for which price is needed.
        properties:
          promoName:
            type: string
            description: >-
              promo Name - mandatory if promotional price is required in
              response
          salesChannel:
            type: string
            description: sales Channel
            enum:
              - WEB
              - APP
          transactionType:
            type: string
            description: transaction Types
            enum:
              - ACTIVATION
              - UPGRADE
              - ADDLINE
              - JUMPUPGRADE
  ProductPriceDetails:
    type: object
    properties:
      status:
        $ref: '#/definitions/Status'
      errors:
        $ref: '#/definitions/Errors'
      skuItems:
        type: array
        items:
          $ref: '#/definitions/ProductPrice'
  ProductPrice:
    type: object
    properties:
      id:
        type: string
        description: Unique ID of the sku
      price:
        type: array
        items:
          properties:
            crp:
              type: string
              description: crp id
            option:
              type: array
              items:
                type: object
                properties:
                  priceType:
                    type: string
                    description: price Type
                  payNowPrice:
                    type: object
                    properties:
                      listPrice:
                        $ref: '#/definitions/Price'
                      salePrice:
                        $ref: '#/definitions/Price'
                  monthlyPrice:
                    type: object
                    properties:
                      listPrice:
                        $ref: '#/definitions/Price'
                      salePrice:
                        $ref: '#/definitions/Price'
      promotions:
        type: array
        items:
          properties:
            promoName:
              type: string
              description: promo name
            promoType:
              type: string
              description: promo Type
            shortDescription:
              type: string
              description: short Description
            discountValue:
              $ref: '#/definitions/Price'
  Price:
    type: object
    description: This is the smallest building block for pricing structures
    properties:
      amount:
        type: number
        format: float
      currency:
        type: string
      display:
        type: string
  Errors:
    type: array
    items:
      properties:
        code:
          type: string
        Message:
          type: string
  promoSearchCriteria:
    type: object
    properties:
      shopper:
        type: object
        description: Details about the customer to return relavent promotions for.
        properties:
          salesChannel:
            $ref: '#/definitions/salesChannel'
          market:
            type: string
            description: Market of the Customer
            enum:
              - USA
              - MAINLAND
              - PUERTORICO
          zipCode:
            type: string
            description: postal code to qualify for promotion offers
          accountType:
            type: string
            description: Account type is individual or business
          accountSubType:
            type: string
            description: >-
              Sub type of the account that further narrows the criteria ex.
              Regular, VIP etc. 
          crpid:
            type: string
            description: crpid (Credit Risk Profile) of subscriber.
      products:
        type: array
        description: List of products to search for match promotions for.
        items:
          $ref: '#/definitions/Product'
      promotions:
        type: object
        properties:
          promoName:
            type: string
            description: Name of the promotion
          pricingOption:
            type: string
            description: price option for the device. This could be Full price or financed
            enum:
              - EIP
              - FRP
          promotionTypes:
            type: array
            items:
              $ref: '#/definitions/PromotionType'
          transactionType:
            type: string
            description: transaction Types
            enum:
              - ACTIVATION
              - UPGRADE
              - ADDLINE
              - JUMPUPGRADE
          transactionDate:
            type: string
            format: dateTime
            description: Date that the transaction is being made.
          groupPromoId:
            type: array
            items:
              type: string
  salesChannel:
    type: string
    description: Sales channel of the promotion
    minItems: 1
    enum:
      - MYT
      - TMO
      - WEB
      - METRO
      - TMOAPP
  PromotionType:
    type: string
    description: Type of promotion
    enum:
      - TRADEIN
      - CART
      - CATALOG
  Promotion:
    type: object
    properties:
      promoName:
        type: string
        description: Name of the promotion that is being returned.
      promoCustomerFacingName:
        type: string
        description: Customer facing name for promotion
      longDescriptionHtml:
        type: string
        description: Promotion html description
      longDescription:
        type: string
        description: Promotion description
      promotionType:
        $ref: '#/definitions/PromotionType'
      tradeInIntent:
        type: string
        description: intent of trade-in the customer is making
        enum:
          - CARRIERFREEDOM
          - STANDARTRADEIN
          - TRADEINPROMOTION
      transactionType:
        type: string
        description: transaction Types
        enum:
          - ACTIVATION
          - UPGRADE
          - ADDLINE
          - JUMPUPGRADE
      groupPromoId:
        type: string
        description: This is the hop id.
      defaultOfferId:
        type: string
        description: This base Offer that this promo is targeting.
      defaultSku:
        type: string
        description: This base Sku that this promo is targeting.
      defaultTradeInDeviceModel:
        type: string
        description: This base trade in model that this promo is targeting.
      defaultPrice:
        type: string
        description: This price (typically best available) that this promo is targeting.
      itemsInCondition:
        type: array
        description: >-
          List of itemTypes that are required to meet the criteria for the
          promotion.
        items:
          type: object
          properties:
            itemType:
              type: string
              description: >-
                Type of product that is required to meet the criteria for this
                promotion.
              enum:
                - OFFERID
                - SKU
                - SOC
                - TRADEINMODEL
            items:
              type: array
              description: >-
                Collection of Socs, Skus, ProductFamilies, or Trade-In model
                skus.
              items:
                type: string
      Discounts:
        type: array
        description: FILL IN
        items:
          type: object
          properties:
            discountActionOn:
              type: string
              description: This describes how the discount is applied.
              enum:
                - SKU
                - CART
                - SHIPPING
                - ACCOUNT
                - REBATE
            discountSchedule:
              type: string
              description: This describes when the discount is applied.
              enum:
                - IMMEDIATE
                - DEFERRED
            pricingOption:
              type: array
              description: >-
                List of recurring discounts with a value and a supressFMV
                boolean
              items:
                type: object
                properties:
                  pricingType:
                    type: string
                    description: Type of pricing available for this promo.
                    enum:
                      - FRP
                      - EIP
                  pricingCreditLevels:
                    type: array
                    items:
                      type: string
                      description: Type of pricing available for this promo.
            discountTotal:
              type: number
              description: Total amount of the discount to be applied.
            immediateDiscountValue:
              type: number
              description: Value of the discounts that will be applied immediately.
            recurringItem:
              type: object
              properties:
                recurringItemDiscount:
                  type: number
                  description: Value of the recurring discount.
                recurringCreditDiscountType:
                  type: string
                  description: This describes when the discount is applied.
                  enum:
                    - RECURRING
                    - LUMPSUM
                    - N/A
                recurringCreditDiscountValue:
                  type: array
                  description: >-
                    List of recurring discounts with a value and a supressFMV
                    boolean
                  items:
                    type: object
                    properties:
                      itemType:
                        type: string
                        description: Type of credit for this promo.
                        enum:
                          - INSTANTTRADEIN
                          - DEFERREDTRADEIN
                          - INSTANT
                          - DEFERRED
                      recurringCredit:
                        type: number
                        description: Amount of the recurring credit.
                      suppressFMV:
                        type: boolean
                        description: >-
                          For the TradeIn scenarios, indicates if the FMV should
                          be not be shown.
                recurringDuration:
                  type: string
                  description: Time period that the recurring discount will continue for.
                  enum:
                    - '24'
                    - '18'
                    - DEFAULT
                    - N/A
  PromotionDetails:
    type: object
    properties:
      status:
        $ref: '#/definitions/Status'
      errors:
        $ref: '#/definitions/Errors'
      promotions:
        type: array
        items:
          $ref: '#/definitions/Promotion'
  Product:
    type: object
    properties:
      offerId:
        type: array
        description: >-
          Collection of product family ids. Use this to get list of promotions
          for devices in the given Product Family.
        items:
          type: string
      productType:
        type: string
        description: >-
          Type of product. Device, Plan, Services, or Accessories to search for
          promotions for.
        enum:
          - DEVICE
          - PLAN
          - SERVICES
          - ACCESSORIES
      sku:
        type: array
        description: Collection of skus to find promotions for.
        items:
          type: string
      planandServices:
        type: array
        description: Collection of Plan & SOCs to find promotions for.
        items:
          type: string
      tradeInDeviceModel:
        type: string
        description: Model of the device
  Status:
    type: object
    properties:
      code:
        type: string
      message:
        type: string
