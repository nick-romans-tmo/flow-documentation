# this is the EOS Monthly recurring charge API
# as a demonstration of an API spec in YAML
swagger: '2.0'
info:
  title: EOS Monthly recurring charges API
  description: This is the API specification for the Monthly Recurring charges. 

  version: "1.0.0"
# the domain of the service
host: jcp.com
# array of all schemes that your API supports
schemes:
  - https
  - http
# will be prefixed to all paths
basePath: /v1/finance/recurringChargeSummary
produces:
  - application/json




parameters:
     requestBody:
          name: Recurring charge  Request
          in: body
          description: request  payload
          required: true
          schema:
           $ref: "#/definitions/reccuringchargereqP"
     oAuth:
          name: Authorization
          type: string
          in: header
          description: Bearer Token for Oauth Validation
          required: true
     
     transactionId:
          name: transactionId
          type: string
          in: header
          description: unique GUID for the transaction
          required: true
     correlationId:
          in: header
          name: correlationId
          type: string
          description: This is used to tie multiple transactions across multiple calls (eg msisdn could be a correlation id , another example is a unique key generated which will be used across multiple api calls in the flow)
          required: false
     transactionBusinessKey:
          in: header
          name: transactionBusinessKey
          type: string
          description: This is place holder for a functional key , could be BAN or MSISDN .
          required: false    
     transactionBusinessKeyType:
          in: header
          name: transactionBusinessKeyType
          type: string
          description: Indicates the functional key example "BAN","MSISDN","SKU" .
          required: false      
     transactionType:
          in: header
          name: transactionType
          type: string
          description: Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc 
          required: true
     applicationId:
          in: header
          name: applicationId
          type: string
          description: application id like MyTMO or TMO
          required: true
     channelId:
          in: header
          name: channelId
          type: string
          description: This indicates whether the original request came from Mobile / web / retail / mw  etc
          required: true
     clientId:
         in: header
         name: clientId
         type: string
         description: unique identifier for the client (could be e-servicesUI , MWSAPConsumer etc )

     dealerCode:
         in: header
         name: dealerCode
         type: string
         description: Middleware specific field wil be defaulted to '000000' for mw requests
paths:
  /:
    post:
      consumes:
        - application/json
      produces:
        - application/json
        - text/xml
        - text/html
      summary: post request for MRC
      operationId: monthlyRecurringCharge
      tags: [monthlyRecurringCharge]

      description: |
       This method is used to retreive monthly recurring charge
      parameters:
# Added Authorization Bearer header for Oauth2
        - $ref: "#/parameters/oAuth"
        - $ref: "#/parameters/transactionId"
        - $ref: "#/parameters/correlationId"
        - $ref: "#/parameters/applicationId"
        - $ref: "#/parameters/channelId"
        - $ref: "#/parameters/clientId"
        - $ref: "#/parameters/transactionBusinessKey"
        - $ref: "#/parameters/transactionBusinessKeyType"
        - $ref: "#/parameters/transactionType"
        - $ref: "#/parameters/dealerCode"        
        - $ref: "#/parameters/requestBody"

      responses:
        200:
          description: Returns the monthly recurring charge
          schema:
            $ref: '#/definitions/reccuringchargeresP'
        400:
          description: Invalid Request Message Processing - Technical Error Bad Request
          schema:
            $ref: '#/definitions/Error'
        500:
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
        404:
          description: Authorization Error
          schema:
            $ref: '#/definitions/Error'  


definitions:
  reccuringchargereqP:
    type: object
    properties:
        ban:
          type: string
          description: nilling account number
        msisdn:
          type: string
          description: List of subscriber numbers for whom the MRC is being requested. If left empty, all MRCs will be provided in response.
        billCycleCode:
          type: string
          description: Indicate the Bill Cycle Code of the account for which MRC summary is being requested. This code may be obtained by getting the account info prior to this call.

        
  reccuringchargeresP: 
    type: object
    properties:
       serviceStatus:
          $ref: "#/definitions/serviceStatusP"
       pooledAccountInd:
          type: boolean
          description: Indicator to denote if the account is pooled

       accountLevelMRC:
          $ref: "#/definitions/monthlyReccuringChargeP"
       subscriberLevelMRC:
          $ref: "#/definitions/monthlyReccuringChargeP"  
  monthlyReccuringChargeP:
    type: object
    properties:
        ban:
          type: string
        msisdn:
          type: string 
        pricePlanCode:
          type: string  
        pricePlanDescription:
          type: string 
        pricePlanMRC:
          type: number 
        messageMRC:
          type: number 
        dataMRC:
          type: number 
        mobileToMobileMRC:
          type: number 
        mobileToAnyMobileMRC:
          type: number 
        equipmentProtectionMRC:
          type: number 
        talkForeverMRC:
          type: number 
        otherMRC:
          type: number 
        estimatedTaxesFees:
          type: number 
        pricePlanTieredDesc:
          type: string 
        pricePlanTieredMRC:
          type: number 
        classicToValueRateplan:
          type: boolean 
        subscriberType:
          type: string 
          enum:
            - 'PRIMARYSUBSCRIBER'
            - 'AALSUBSCRIBER'
            - 'NONPOOLEDSUBSCRIBER'
        classicToValue:
          type: string 
          enum:
            - 'NONE'
            - 'TENTATIVE'
            - 'INVALID_ACCOUNT_TYPE_SUB_TYPE'
            - 'RATEPLAN'
            - 'ADD_A_LINE'
            - 'DATA'
            - 'RATEPLAN_AND_DATA'
            - 'ADD_A_LINE_AND_DATA'
            - 'NON_POOLED_VOICE'
            - 'NON_POOLED_DATA' 
        addALineSOC:
          type: string
       
   


  Error:
    type: object
    properties:
      code:
        type: integer
        format: int32
        description: This is the  error code which is custom for the microservice
      message:
        type: string
        description: Error message for the microservice
      details:
        type: string
        description: Additional information for the error

  serviceStatusP:
    type: object
    properties: 
        statusCode:
            type: string
            description: A number code used to depict the status of a service response. This code allows developers to programmatically handle the status.

        statusDescription:
            type: string
            description: A definition of statusCode.
        explanation:
            type: string
            description: Optional information related to a statusCode to further define the result of a request. .
        referenceId:
            type: string
            description: A unique identifier used to locate a service log message; useful for debugging the issues. 


