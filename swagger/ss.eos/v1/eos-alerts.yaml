swagger: '2.0'
info:
  description: Alerts Backend For Frontend API
  version: 1.0.0
  title: Alerts BFF API
  termsOfService: 'http://swagger.io/terms/'
  contact:
    email: api@t-mobile.io
host: eos.corporate.t-mobile.com
basePath: /v1/alerts
tags:
  - name: alert
    description: All customer alerts
schemes:
  - https
parameters:
  X-B3-TraceId:
    in: header
    name: X-B3-TraceId
    type: string
    description: >-
      This is interactionId or correlationId equivalent that is required to
      passed to MW/Backend. If request is from app, they also pass this to a
      web-view request as X-B3-TraceId header.
    required: true
  X-B3-SpanId:
    in: header
    name: X-B3-SpanId
    type: string
    description: App need to pass X-B3-SpanId in header.
    required: true
  Channel:
    in: header
    name: Channel
    type: string
    description: >-
      Possible value are DESKTOP, MOBILE, TMO-APP if there is any other value ,
      API will give an error
    required: false
  Channel-Version:
    in: header
    name: Channel-Version
    type: string
    description: >-
      ApplicationVersionCode/XXX for example:ApplicationVersionCode/300 based on
      if app is android or ios.Required if channel is TMO-APP. Empty for DESKTOP
      and MOBILE.
    required: false
  Authorization:
    in: header
    name: Authorization
    type: string
    description: Access token that is received from IAM after authentication.
    required: true
  User-Token:
    in: header
    name: User-Token
    type: string
    description: JWT token need to be sent.
    required: false
  Tmo-Id:
    in: header
    name: Tmo-Id
    type: string
    description: TMOId assoiated with IAM account.
    required: false
  Phone-Number:
    in: header
    name: Phone-Number
    type: string
    description: Logged in MSISDN
    required: false
  Billing-Account-Number:
    in: header
    name: Billing-Account-Number
    type: string
    description: BAN associated with logged in MSISDN.
    required: false
  session-num:
    in: header
    name: session-num
    type: string
    description: >-
      A value populated by the Sender used to track the transactions that occur
      during a session, a long-lasting interaction that is maintained by the
      sender. Senders are encouraged to develop a useful scheme. Do not use
      customer-sensitive information
    required: false
paths:
  /:
    post:
      operationId: getAlerts
      summary: Get Alerts
      parameters:
        - $ref: '#/parameters/X-B3-TraceId'
        - $ref: '#/parameters/X-B3-SpanId'
        - $ref: '#/parameters/Channel'
        - $ref: '#/parameters/Channel-Version'
        - $ref: '#/parameters/Authorization'
        - $ref: '#/parameters/User-Token'
        - $ref: '#/parameters/Tmo-Id'
        - $ref: '#/parameters/Phone-Number'
        - $ref: '#/parameters/Billing-Account-Number'
        - $ref: '#/parameters/session-num'
        - name: request
          in: body
          schema:
            $ref: '#/definitions/AlertRequest'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/AlertResponse'
        '400':
          description: Bad request
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
  /postback:
    put:
      operationId: postbackAlerts
      summary: Postback Alerts
      tags:
        - Postbacks
      description: >-
        Postback Alerts Backend For Frontend API. Using this API, the client (or
        an aggregator system) can send back information about action taken by
        the user after receiving an alert.
      parameters:
        - $ref: '#/parameters/X-B3-TraceId'
        - $ref: '#/parameters/X-B3-SpanId'
        - $ref: '#/parameters/Channel'
        - $ref: '#/parameters/Channel-Version'
        - $ref: '#/parameters/Authorization'
        - $ref: '#/parameters/User-Token'
        - $ref: '#/parameters/Tmo-Id'
        - $ref: '#/parameters/Phone-Number'
        - $ref: '#/parameters/Billing-Account-Number'
        - $ref: '#/parameters/session-num'
        - name: postback-request
          in: body
          schema:
            $ref: '#/definitions/PostbackRequest'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/PostbackResponse'
        '400':
          description: Bad request
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
definitions:
  Alert:
    type: object
    properties:
      alertId:
        type: string
      eventId:
        type: string
      title:
        type: string
      message:
        type: string
      level:
        type: string
      tags:
        type: array
        items:
          type: string
      ctas:
        type: array
        items:
          $ref: '#/definitions/CallToAction'
      publishedVersion:
        type: string
    required:
      - alertId
      - title
      - message
  AlertRequest:
    type: object
    properties:
      line:
        type: array
        items:
          $ref: '#/definitions/Line'
      tags:
        type: array
        items:
          type: string
      alertLevel:
        type: string
        description: alert level
      weight:
        type: integer
      maximumAlerts:
        type: integer
  Line:
    type: object
    properties:
      msisdn:
        type: string
        description: Customers msisdn
      imei:
        type: string
        description: device IMEI
    required:
      - msisdn
  Error:
    type: object
    properties:
      category:
        type: string
        description: Error category
      code:
        type: integer
        format: int32
        description: error code
      message:
        type: string
        description: error message
      details:
        type: string
        description: Additional information for the error
      ctas:
        type: array
        items:
          $ref: '#/definitions/CallToAction'
  CallToAction:
    type: object
    properties:
      ctaId:
        type: string
        description: call to action item ctaId
      action:
        type: string
        description: call to action URL
  AlertResponse:
    type: object
    properties:
      alerts:
        type: array
        items:
          $ref: '#/definitions/Alert'
  PostbackResponse:
    type: object
    properties:
      statusCode:
        type: string
        description: Status Code
      statusMessage:
        type: string
        description: Status Message
  PostbackAction:
    type: object
    properties:
      actionType:
        type: string
        enum:
          - cta
          - displayed
          - delivery_status
      timeStamp:
        type: string
        description: 8601 date.
      ctaId:
        type: string
        description: CTA ID from Alerts Engine.
      ctaClicked:
        type: boolean
        description: Was the CTA URL followed? Usually true.
      status:
        type: string
        enum:
          - unable_to_deliver
          - queued
          - delivered
      displayType:
        type: string
        enum:
          - temporary
          - permanent
          - notification_tray
          - page
  PostbackEvent:
    type: object
    properties:
      imei:
        type: string
        description: IMEI of phone in quesiton.
      msisdn:
        type: string
        description: MSISDN of subscriber in question.
      eventId:
        type: string
        description: the uuid / event_id received from an /evaluate request.
      alertId:
        type: string
        description: The ID of the alert from the firing request.
      alertLevel:
        type: string
        description: Message level received
      actions:
        type: array
        items:
          $ref: '#/definitions/PostbackAction'
  PostbackRequest:
    type: object
    properties:
      channel:
        type: string
        description: my_tmo for My T-Mobile.com
      postbackEvents:
        type: array
        items:
          $ref: '#/definitions/PostbackEvent'