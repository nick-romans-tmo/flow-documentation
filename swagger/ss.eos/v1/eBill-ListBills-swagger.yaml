swagger: '2.0'
info:
  title: ListBills
  description: Retrieves list of Bills for requested Account Number
  version: 1.0.0
  contact:
    name: P&T eBill development team
    url: 'http://www.t-mobile.com'
    email: EIT_eBill_Development@TMobileUSA.onmicrosoft.com
  license:
    name: Apache 2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0'
  termsOfService: 'urn:tos'
  x-createdBy:
    dateCreated: 'Thu May 30 08:32:16 2019'
    createdBy: kharris31
    application: Recite
    appVersion: 0.4.3.2801
    documentId: 2cc9d3a2-bcc0-4d64-9db9-2048dde434f6
    status: Production
    classification: 7.3 Billing Mgmt.
    profile: Core Business Capability Service
    serviceLayer: Enterprise - Billing
x-servers:
  - url: 'https://ebillservice.internal.t-mobile.com'
    description: Production service
host: ebillservice.internal.t-mobile.com
basePath: /services/rest/listbills
schemes:
  - https
paths:
  /services/rest/listbills:
    get:
      tags:
        - List Bills
      x-api-pattern: QueryResource
      summary: Retrieves list of bills
      description: Retrieves list of bills for requested Account Number
      operationId: listbills
      consumes:
        - application/json
        - text/xml
      produces:
        - application/json
        - text/xml
      parameters:
        - name: billingaccountnumber
          in: query
          description: 9-digit string
          required: true
          type: string
          x-example: string
        - name: Accept
          in: header
          description: Preferred content type _ default to text/xml
          x-example: application/json
          required: false
          type: string
          minLength: 1
          maxLength: 256
          pattern: '^[ \S]+$'
        - name: Authorization
          in: header
          description: >-
            The HTTP Authorization request header contains the credentials to
            authenticate a user agent with a server.
          x-example: Basic ZWJpbGw6cGE4OGIxbGx6
          required: true
          type: string
          minLength: 1
          maxLength: 256
          pattern: '^[\S ]+$'
        - name: Cache-Control
          in: header
          description: Cache policy supported for the resource by the server
          x-example: no-cache
          required: false
          type: string
          minLength: 1
          maxLength: 256
          pattern: '^[\S]*$'
      responses:
        '200':
          description: Success
          schema:
            items:
              $ref: "#/definitions/previousBills"
          headers:
            Date:
              description: Cache policy supported for the resource by the server
              x-example: no-cache
              type: string
              minLength: 1
              maxLength: 256
              pattern: '^[\S]*$'
            Content-Length:
              description: >-
                Length of the response body in octets; not supported if
                Transfer-Endoding = chunked
              x-example: '9112300000003'
              type: integer
              format: int64
              minimum: 0
              pattern: '^[\d]*$'
            Content-Type:
              description: The MIME type of this content
              x-example: text/xml
              type: string
              minLength: 1
              maxLength: 256
              pattern: '^[ \S]+$'
        '400':
          description: Bad Request
          examples:
            application/json:
              code: '400'
              userMessage: Bad Request
              systemMessage: Bad Request
              detailLink: 'http://www.tmus.com'
          headers: {}
        '401':
          description: Unauthorized
          examples:
            application/json:
              code: '401'
              userMessage: Unauthorized
              systemMessage: Unauthorized
              detailLink: 'http://www.tmus.com'
          headers: {}
        '404':
          description: Not Found
          examples:
            application/json:
              code: '404'
              userMessage: Not Found
              systemMessage: Not Found
              detailLink: 'http://www.tmus.com'
          headers: {}
        '500':
          description: Internal Server Error
          examples:
            application/json:
              code: '500'
              userMessage: Internal Server Error
              systemMessage: Internal Server Error
              detailLink: 'http://www.tmus.com'
          headers: {}
        '503':
          description: Service Unavailable
          examples:
            application/json:
              code: '503'
              userMessage: Service Unavailable
              systemMessage: Service Unavailable
              detailLink: 'http://www.tmus.com'
          headers: {}
      security:
        - JWT: []
definitions:
  previousBills:
    type: object
    required:
      - bill
    properties:
      bill:
        type: array
        description: An array of bill objects.
        items:
          $ref: '#/definitions/bill'
    example:
      - amountDue: 133.54
        billDate: "2019-05-27T00:00:00-07:00"
        billFromDate: "2019-04-27T00:00:00-07:00"
        billStatementId: 10059984788
        billToDate: "2019-05-26T00:00:00-07:00"
        currentCharges: 134.55
        dateRange: "4/27/19 - 5/26/19"
        format: "SB_Paper"
      - amountDue: 168.16
        billDate: "2019-04-27T00:00:00-07:00"
        billFromDate: "2019-03-27T00:00:00-07:00"
        billSequenceNumber: 9964945583
        billToDate: "2019-04-26T00:00:00-07:00"
        currentCharges: 170.17
        dateRange: "3/27/19 - 4/26/19"
        format: "SB_Paper"
      - amountDue: 161.16
        billDate: "2019-03-27T00:00:00-07:00"
        billFromDate: "2019-02-27T00:00:00-08:00"
        billSequenceNumber: 9869689183
        billToDate: "2019-03-26T00:00:00-07:00"
        currentCharges: 161.67
        dateRange: "1/27/19 - 2/26/19"
        format: "SB_Paper"
  bill:
    type: object
    properties:
      amountDue:
        description: Amount due at statement generation
        type: number
        format: double
      billDate:
        description: Date the statement was generated
        type: string
        format: date
      billFromDate:
        description: Start date of the bill cycle
        type: string
        format: date
      billSequenceNumber:
        description: Unique id for the statement
        type: number
        format: integer
      billToDate:
        description: End date of bill cycle
        type: string
        format: date
      currentCharges:
        description: Charges incurred this bill period
        type: number
        format: double
      dateRange:
        description: Date range of this bill period (M/DD/YY – M/DD/YY)
        type: string
      format:
        description: Bill type (SB_Paper = M3 / VS_Paper2 = M1)
        type: string
securityDefinitions:
  JWT:
    type: apiKey
    description: >-
      JWT is a means of representing claims to be transferred between two
      parties. The claims in a JWT are encoded as a JSON object that is
      digitally signed using JSON Web Signature (JWS) and/or encrypted using
      JSON Web Encryption (JWE).
    name: Authorization
    in: header
