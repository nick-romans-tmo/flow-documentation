---
swagger: '2.0'
info:
  version: '2018-02-09T00:00:00Z'
  title: EOS Order Flow Events API
  description: Swagger specification for order flow events API
host: eos.corporate.t-mobile.com
basePath: "/v1/orderflow"
schemes:
- https
parameters:
  Content-typeParam:
    name: Content-type
    in: header
    required: true
    type: string
  transactionId:
    name: transactionId
    type: string
    in: header
    description: unique GUID for the transaction.
    required: true
  correlationId:
    in: header
    name: correlationId
    type: string
    description: >-
      This is used to tie multiple transactions across multiple calls (a unique
      key generated which will be used across multiple api calls in the flow).
    required: true
  transactionBusinessKey:
    in: header
    name: transactionBusinessKey
    type: string
    description: 'This is place holder for a functional key, the value is order Number.'
    required: true
  transactionBusinessKeyType:
    in: header
    name: transactionBusinessKeyType
    type: string
    description: 'Indicates the functional key, "Order Number" for Orderflow APIs.'
    required: true
  transactionType:
    in: header
    name: transactionType
    type: string
    description: 'orderflow'
    required: true
  applicationId:
    in: header
    name: applicationId
    type: string
    description: Application id like MyTMO or TMO.
    required: true
  channelId:
    in: header
    name: channelId
    type: string
    description: >-
      This indicates whether the original request came from Mobile / web /
      retail / mw  etc.
    required: true
  clientId:
    in: header
    name: clientId
    type: string
    description: >-
      Unique identifier for the client (could be e-servicesUI).
    required: true
paths:
  "/events":
    post:
      description: POST method to record an order flow event
      consumes:
      - application/json
      - application/x-www-form-urlencoded
      produces:
      - application/json
      - application/x-www-form-urlencoded
      parameters:
      - $ref: '#/parameters/Content-typeParam'
      - $ref: '#/parameters/transactionId'
      - $ref: '#/parameters/correlationId'
      - $ref: '#/parameters/applicationId'
      - $ref: '#/parameters/channelId'
      - $ref: '#/parameters/clientId'
      - $ref: '#/parameters/transactionBusinessKey'
      - $ref: '#/parameters/transactionBusinessKeyType'
      - $ref: '#/parameters/transactionType'
      - name: param
        in: body
        description: post body parameter
        schema:
          "$ref": "#/definitions/OrderFlowEvent"
      responses:
        '200':
          description: Successful Execution and order event recorded
          schema:
            "$ref": "#/definitions/CreateOrderFlowEventResponse"
          headers:
            Access-Control-Allow-Headers:
              type: string
            Access-Control-Allow-Methods:
              type: string
            Access-Control-Allow-Origin:
              type: string
        '400':
          description: Bad Request
          schema:
            "$ref": "#/definitions/400ErrorResponse"
          headers:
            Access-Control-Allow-Headers:
              type: string
            Access-Control-Allow-Methods:
              type: string
            Access-Control-Allow-Origin:
              type: string
        '404':
          description: Not Found
          schema:
            "$ref": "#/definitions/404ErrorResponse"
          headers:
            Access-Control-Allow-Headers:
              type: string
            Access-Control-Allow-Methods:
              type: string
            Access-Control-Allow-Origin:
              type: string
        '500':
          description: Internal Server Error
          schema:
            "$ref": "#/definitions/500ErrorResponse"
          headers:
            Access-Control-Allow-Headers:
              type: string
            Access-Control-Allow-Methods:
              type: string
            Access-Control-Allow-Origin:
              type: string
definitions:
  CreateOrderFlowEventResponse:
    type: object
    properties:
      eventId:
        type: string
        description: The unique ID returned for the event
    required:
    - eventId
  400ErrorResponse:
    type: object
    properties:
      error_type:
        type: string
        default: Bad Request format
      message:
        type: string
  404ErrorResponse:
    type: object
    properties:
      error_type:
        type: string
        default: Resource Not Found
      message:
        type: string
  500ErrorResponse:
    type: object
    properties:
      error_type:
        type: string
        default: Internal Server Error
      message:
        type: string
  OrderFlowEvent:
    type: object
    properties:
      orderId:
        type: string
        description: Order ID
      application:
        type: string
        description: Application sending the event
        enum:
        - MYTMO
        - DCD
        - OMD
      eventType:
        type: string
        description: Type of the event
        enum:
        - DISCLOSURE_CREATED
        - DISCLOSURE_SIGNED
        - ORDER_CONFIRMED
        - ORDER_RECEIVED
        - ORDER_PUSHED
        - ORDER_INITIATED
        - ORDER_CREATED
        - ORDER_PICKEDUP
      msisdn:
        type: integer
        format: int32
        description: MSISDN of the user placing the order
      transaction:
        type: string
        description: Type of the transaction
        enum:
        - standard-upgrade
        - jump-upgrade
        - add-line
        - activation
      channel:
        type: string
        description: originating channel for the event
        enum:
        - WEB
        - RETAIL
        - CARE
    required:
    - orderId
    - application
    - eventType