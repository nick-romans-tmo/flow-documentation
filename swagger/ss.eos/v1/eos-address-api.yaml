swagger: '2.0'
info:
  title: EOS Address API
  description: 'Micro services to add,update and pull shipping/Billing/E911/PPU addresses'
  version: 1.0.0
schemes:
  - https
  - http
basePath: /v1/address
produces:
  - application/json
consumes:
  - application/json
parameters:
  oAuth:
    name: Authorization
    type: string
    in: header
    description: Bearer Token for Oauth Validation.
    required: true
  transactionId:
    name: transactionId
    type: string
    in: header
    description: unique GUID for the transaction.
    required: true
  correlationId:
    in: header
    name: correlationId
    type: string
    description: >-
      This is used to tie multiple transactions across multiple calls (a unique
      key generated will be used across multiple api calls in the flow).
    required: true
  transactionBusinessKey:
    in: header
    name: transactionBusinessKey
    type: string
    description: 'This is place holder for a functional key, the value is MSISDN.'
    required: true
  transactionBusinessKeyType:
    in: header
    name: transactionBusinessKeyType
    type: string
    description: 'Indicates the functional key, "MSISDN" for address APIs.'
    required: true
  transactionType:
    in: header
    name: transactionType
    type: string
    description: 'Indicates the kind of transaction eg AAL, CIHU, inventoryfeed etc .'
    required: true
  applicationId:
    in: header
    name: applicationId
    type: string
    description: Application id like MyTMO or TMO.
    required: true
  channelId:
    in: header
    name: channelId
    type: string
    description: >-
      This indicates whether the original request came from Mobile / web /
      retail / mw  etc.
    required: true
  clientId:
    in: header
    name: clientId
    type: string
    description: >-
      Unique identifier for the client (could be e-servicesUI , MWSAPConsumer
      etc ).
    required: true
paths:
  '/':
    put:
      summary: Update address.
      description: Update address.
      operationId: updateAddress
      parameters:
        - $ref: '#/parameters/oAuth'
        - $ref: '#/parameters/transactionId'
        - $ref: '#/parameters/correlationId'
        - $ref: '#/parameters/applicationId'
        - $ref: '#/parameters/channelId'
        - $ref: '#/parameters/clientId'
        - $ref: '#/parameters/transactionBusinessKey'
        - $ref: '#/parameters/transactionBusinessKeyType'
        - $ref: '#/parameters/transactionType'
        - name: addressRequest
          in: body
          required: true
          description: Account details for the account which needs to be updated
          schema:
            $ref: '#/definitions/Address'
      tags:
        - Address
      responses:
        '200':
          description: Updated Account
          schema:
            $ref: '#/definitions/UpdateAddressResponse'
        '400':
          description: Invalid request
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal error
          schema:
            $ref: '#/definitions/Error'
definitions:
  Error:
    type: object
    properties:
      code:
        type: integer
        format: int32
        description: Error code.
      message:
        type: string
        description: Error message.
  UpdateAddressResponse:
    type: object
    properties:
      address:
        type: object
        items:
          $ref: '#/definitions/Address'
      statusCode:
        type: string
        description: error code from backend
      statusMessage:
        type: string
        description: A description of the status code or error message from backend.
  Address:
    type: object
    required:
      - addressId
      - firstName
      - lastName
      - address1
      - country
      - state
      - city
      - zipCode
      - addressType
    properties:
      firstName:
        type: string
        description: First Name on Address
      lastName:
        type: string
        description: Last Name  on Address
      address1:
        type: string
        description: street address or Address Line 1
      address2:
        type: string
        description: extended address or Address Line 2. ex. Apt/Unit etc.
      country:
        type: string
        description: Country name
      state:
        type: string
        description: State
      city:
        type: string
        description: city
      zipCode:
        type: string
        description: postal code
      zipCodeExtension:
        type: string
        description: 4 digit postal code extension
      addressType:
        type: string
        description: Type of address.
        enum:
          - BILLING
          - SHIPPING
          - PPU
          - E911
      addressId:
        type: string
        description: >-
          Encoded address ID specific to DCP which is basically used in case of
          UPDATE/DELETE address operations.
