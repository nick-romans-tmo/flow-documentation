swagger: '2.0'

info:
  version: "1.0.0"
  title: "EOS Payment Microservice"

host: eos.eitss.tmobile.com
produces:
  - application/json
consumes:
  - application/json
basePath: "/v1"
schemes:
  - https

parameters:
  transactionId:
    name: transactionId
    type: string
    in: path
    required: true
    description: payment transaction identifier
  interactionId:
    name: interactionId
    in: header
    required: true
    type: string
    description: Unique identifier for transaction, passed from UI and common accross service calls
  workflowId:
    name: workflowId
    in: header
    required: true
    type: string
    description: workflow identifier required by MW
    
paths:
  /payments:
    post:
      description: Process a new payment 
      parameters:
        - $ref: '#/parameters/interactionId'
        - $ref: '#/parameters/workflowId'
        - name: payment
          in: body
          schema:
            $ref: '#/definitions/Payment'
          required: true
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/Payment'
        400:
          description: Bad Request
          schema:
            type: array
            items:
              $ref: "#/definitions/Error"
        500:
          description: Internal Server Error
          schema:
            $ref: "#/definitions/Error" 
  /payments/{transactionId}/refund:
    post:
      description: Refund customer the amount processed via payment processing 
      parameters:
        - $ref: '#/parameters/transactionId'
        - $ref: '#/parameters/interactionId'
        - $ref: '#/parameters/workflowId'
        - name: refundRequest
          in: body
          required: true
          schema:
            $ref: "#/definitions/Refund"
      responses:
        200:
          description: OK
          schema:
            $ref: "#/definitions/Refund"
        400:
          description: Bad Request
          schema:
            $ref: "#/definitions/Error" 
        500:
          description: Internal Server Error 
          schema:
            $ref: "#/definitions/Error"
  /payments/{transactionId}/errors/{code}:
    get:
      description: get custom error info with CTAs for a specific code
      parameters:
        - $ref: '#/parameters/transactionId'
        - name: code
          in: path
          required: true
          type: string
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/Error'
          
definitions:
  Refund:
    type: object
    description: refund request
    properties:
      chargeAmount:
        description: >-
          The total amount of the transaction inclusive of all additional
          amounts.
        type: number
        format: double
      authorizedAmount:
        description: Amount authorized by the financial institution.
        type: number
        format: double
      statusCode:
        description: Status code
        type: string
      reasonCode:
        description: Status reason code
        type: string
      reasonDescription:
        description: Status reason description
        type: string
      sourceId:
        type: string
        description: source id
      paymentMethod:
        $ref: "#/definitions/PaymentMethod"
      transactionId:
        type: string
        description: transaction identifier
      transactionType:
        type: string
        description: transaction type
      creationTime:
        type: string
        description: creation time
      customerId:
        type: string
        description: customer identifier
  Payment:
    type: object
    description: Payment entity
    properties:
      accountNumber:
        type: string
        description: Customer's account number
      msisdn:
        type: string
        description: line
      loanId:
        type: string
        description: Loan identifier, Required if Payment type is EIP
      sessionId:
        type: string
        description: session identifier
      equipmentId:
        type: string
        description: equipment identifier
      creditClass:
        type: string
        description: credit class
      channelId:
        type: string
        enum:
          - Desktop
          - Mobile
        description: channel id
      orderNumber:
        description: The order number this payment is associated with.
        type: string
      transactionId:
        type: string
        description: Unique identifier for payment transaction
      chargeAmount:
        type: number
        format: double
        description:  The total amount of the transaction inclusive of all additional
          amounts.
      authorizedAmount:
        description: Amount authorized by the financial institution.
        type: number
        format: double
      authorizationId:
        description: Identifier or code of the transaction authorization.
        type: string
      creationTime:
        description: Time when the Payment request was created.
        type: string
        format: date-time
      transactionDate:
        description: Transaction Date
        type: string
        format: date
      transactionType:
        type: string
        enum:
          - EIP
          - OTP
          - DATAPASS
          - ODF
          - PA
      termsAgreementIndicator:
        description: To indicate if the customer accept the agreement or not.
        type: boolean
      termsAgreementTime:
        description: Time when the customer accepts the terms agreement.
        type: string
        format: date-time
      transactionCode:
        description: Transaction code
        type: string
      taxAmount:
        description: Amount of tax included in the payment transaction.
        type: number
        format: double
      paymentMethod:
        $ref: '#/definitions/PaymentMethod'
      statusCode:
        description: Status code
        type: string
      reasonCode:
        description: Status reason code
        type: string
      reasonDescription:
        description: Status reason description
        type: string
      cvvResponseCode:
        description: Card Verification Value (CVV) response code
        type: string
      avsResponseCode:
        description: Address Verification Service (AVS) Response Code
        type: string
      paymentApprovalCode:
        type: string
        description: payment approval code
      paymentReferenceId:
        type: string
        description: payment approval reference id
    required:
      - transactionType
      - accountNumber
      - transactionDate
      
  Address:
    type: object
    description: Address
    properties:
      line1:
        type: string
        description: address line 1
      line2:
        type: string
        description: address line 2
      city:
        type: string
        description: city
      state:
        type: string
        description: state
      zip:
        type: string
        description: zip code
    required:
      - line1
      - city
      - state
      - zip

  PaymentMethod:
    type: object
    description: Payment method
    properties:
      code:
        type: string
        description: default payment method code identifying current payment instrument
      storedPaymentMethodId:
        type: string
        description: stored payment method id
      status:
        type: string
        description: current payment method status
      defaultPaymentMethod:
        type: boolean
        description: default payment method
      store:
        type: boolean
        description: save this payment instrument
      creditCard:
        $ref: "#/definitions/CreditCard"
      debitCard:
        $ref: "#/definitions/DebitCard"
      bankAccount:
        $ref: "#/definitions/BankAccount"
      
  Card:
    type: object
    description: Card Payment instrument
    properties:
      cardNumber:
        type: string
        description: card number
      cardAlias:
        type: string
        description: card alias
      cardHolderName:
        type: string
        description: Card holder name (as it appears on card)
      cardHolderFirstName:
        type: string
        description: Card holder first name
      cardHolderLastName:
        type: string
        description: Card holder last name
      expirationMonthYear:
        type: string
        description: Expiration month/year
      type: 
        type: string
        enum:
          - Visa
          - Mastercard
          - Amex
      billingAddress:
        $ref: "#/definitions/Address"
      cvv:
        type: string
        description: verification code
        
    required:
      - billingAddress
      - cardHolderName
      - type
      - expirationMonthYear
      - cvv

  CreditCard:
    type: object
    description: credit card payment instrument
    allOf:
      - $ref: "#/definitions/Card"
      
  DebitCard:
    type: object
    description: bank card payment instrument
    allOf:
      - $ref: "#/definitions/Card"
      - properties:
          pin:
            type: integer
            description: Pin code for bank card
          pinlessDebit:
            type: boolean
            description: pinless debit card processing
  
  BankAccount:
    type: object
    description: Bank account payment instrument
    properties:
      accountType:
        type: string
        enum:
          - Checking
          - Savings
      bankAccountAlias:
        type: string
        description: Bank account number alias
      accountNumber:
        type: string
        description: account number
      routingNumber:
        type: string
        description: bank ruoting number
    required:
      - accountType
      - accountNumber
      - routingNumber

  Error:
    type: object
    properties:
      category:
        type: string
        description: Error category
      code:
        type: integer
        format: int32
        description: error code
      message:
        type: string
        description: error message
      details:
        type: string
        description: Additional information for the error
      ctas:
        type: array
        items:
          $ref: "#/definitions/CallToAction"
          
  CallToAction:
    type: object
    properties:
      text:
        type: string
        description: call to action item text
      action:
        type: string
        description: call to action URL
        
        