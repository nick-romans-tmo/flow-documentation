swagger: '2.0'
info:
  description: This is the API specification for the Product and Accessories microservice
  version: 2.0.0
  title: Experience Product APIs
  contact: {}
  license: {}
host: stage.eos.corporate.t-mobile.com
basePath: /v3/catalog
tags:
  - name: product-api-controller
    description: Product Api Controller
paths:
  /availability:
    get:
      tags:
        - Product availability details.
      summary: GET the Product availability details.
      description: This API will return the Product availability details.
      operationId: getProductAvailabilityDetailsUsingGET
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: Authorization
          in: header
          description: Bearer Token for Oauth Validation
          required: false
          type: string
        - name: transactionId
          in: header
          description: Unique GUID for the transaction
          required: false
          type: string
        - name: transactionType
          in: header
          description: 'Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc'
          required: false
          type: string
        - name: applicationId
          in: header
          description: Application id like MyTMO or TMO
          required: false
          type: string
        - name: channelId
          in: header
          description: >-
            This indicates whether the original request came from Mobile / web /
            retail / mw  etc
          required: false
          type: string
        - name: correlationId
          in: header
          description: >-
            This is used to tie multiple transactions across multiple calls (eg
            msisdn could be a correlation id , another example is a unique key
            generated which will be used across multiple api calls in the flow)
          required: false
          type: string
        - name: clientId
          in: header
          description: >-
            Unique identifier for the client (could be e-servicesUI ,
            MWSAPConsumer etc )
          required: false
          type: string
        - name: transactionBusinessKey
          in: header
          description: >-
            This is the place holder for a functional key , could be BAN or
            MSISDN .
          required: false
          type: string
        - name: transactionBusinessKeyType
          in: header
          description: 'Indicates the functional key example "BAN","MSISDN","SKU" .'
          required: false
          type: string
        - name: dealerCode
          in: header
          description: >-
            Middleware specific field wil be defaulted to '000000' for mw
            requests
          required: false
          type: string
        - name: channelCode
          in: query
          description: 'Channel Code (MYT, AAL, HSU).'
          required: true
          type: string
          enum:
            - MYT
            - HSU
            - AAL
        - name: skus
          in: query
          description: Array of SKUs.
          required: true
          type: array
          items:
            type: string
          collectionFormat: multi
      responses:
        '200':
          description: Returns the Product availability details.
          schema:
            $ref: '#/definitions/ProductAvailability'
        '400':
          description: Invalid Request Message Processing - Technical Error Bad Request
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Authorization Error
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
  /deviceFilters:
    get:
      tags:
        - List of Filter Options for filtering the Products.
      summary: GET the List of Filter Options for filtering the Products.
      description: >-
        This API will return the List of Filter Options for filtering the
        Products.
      operationId: getFilterOptionsUsingGET
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: Authorization
          in: header
          description: Bearer Token for Oauth Validation
          required: false
          type: string
        - name: transactionId
          in: header
          description: Unique GUID for the transaction
          required: false
          type: string
        - name: transactionType
          in: header
          description: 'Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc'
          required: false
          type: string
        - name: applicationId
          in: header
          description: Application id like MyTMO or TMO
          required: false
          type: string
        - name: channelId
          in: header
          description: >-
            This indicates whether the original request came from Mobile / web /
            retail / mw  etc
          required: false
          type: string
        - name: correlationId
          in: header
          description: >-
            This is used to tie multiple transactions across multiple calls (eg
            msisdn could be a correlation id , another example is a unique key
            generated which will be used across multiple api calls in the flow)
          required: false
          type: string
        - name: clientId
          in: header
          description: >-
            Unique identifier for the client (could be e-servicesUI ,
            MWSAPConsumer etc )
          required: false
          type: string
        - name: transactionBusinessKey
          in: header
          description: >-
            This is the place holder for a functional key , could be BAN or
            MSISDN .
          required: false
          type: string
        - name: transactionBusinessKeyType
          in: header
          description: 'Indicates the functional key example "BAN","MSISDN","SKU" .'
          required: false
          type: string
        - name: dealerCode
          in: header
          description: >-
            Middleware specific field wil be defaulted to '000000' for mw
            requests
          required: false
          type: string
        - name: channelCode
          in: query
          description: 'Channel Code (MYT, AAL, HSU). MYT has both HSU and AAL data'
          required: false
          type: string
          enum:
            - MYT
            - HSU
            - AAL
        - name: marketId
          in: query
          description: Market ID
          required: false
          type: string
      responses:
        '200':
          description: Returns the List of Filter Options for filtering the Products.
          schema:
            $ref: '#/definitions/ProductFilterData'
        '400':
          description: Invalid Request Message Processing - Technical Error Bad Request
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Authorization Error
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
  /financeOptions:
    post:
      tags:
        - Pricing/Financing Options for Products.
      summary: GET Pricing/Financing Options for Products.
      description: >-
        This API will return the Pricing/Financing Options (Loan/Lease/Club) for
        Products, for all the given CRP(s).
      operationId: getFinanceOptionsUsingPostUsingPOST
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: Authorization
          in: header
          description: Bearer Token for Oauth Validation
          required: false
          type: string
        - name: transactionId
          in: header
          description: Unique GUID for the transaction
          required: false
          type: string
        - name: transactionType
          in: header
          description: 'Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc'
          required: false
          type: string
        - name: applicationId
          in: header
          description: Application id like MyTMO or TMO
          required: false
          type: string
        - name: channelId
          in: header
          description: >-
            This indicates whether the original request came from Mobile / web /
            retail / mw  etc
          required: false
          type: string
        - name: correlationId
          in: header
          description: >-
            This is used to tie multiple transactions across multiple calls (eg
            msisdn could be a correlation id , another example is a unique key
            generated which will be used across multiple api calls in the flow)
          required: false
          type: string
        - name: clientId
          in: header
          description: >-
            Unique identifier for the client (could be e-servicesUI ,
            MWSAPConsumer etc )
          required: false
          type: string
        - name: transactionBusinessKey
          in: header
          description: >-
            This is the place holder for a functional key , could be BAN or
            MSISDN .
          required: false
          type: string
        - name: transactionBusinessKeyType
          in: header
          description: 'Indicates the functional key example "BAN","MSISDN","SKU" .'
          required: false
          type: string
        - name: dealerCode
          in: header
          description: >-
            Middleware specific field wil be defaulted to '000000' for mw
            requests
          required: false
          type: string
        - in: body
          name: financeOptionsRequest
          description: Finance Options Request
          required: true
          schema:
            $ref: '#/definitions/FinanceOptionsRequest'
      responses:
        '200':
          description: Returns the Pricing/Financing Options for Products.
          schema:
            $ref: '#/definitions/FinanceOptions'
        '201':
          description: Created
        '400':
          description: Invalid Request Message Processing - Technical Error Bad Request
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized
          schema:
            $ref: '#/definitions/Error'
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/Error'
        '404':
          description: Authorization Error
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
  /products:
    post:
      tags:
        - List of Products
      summary: List of Products
      description: >-
        This API will return the List of Products, for the given filter criteria
        wrapped in SearchRequest.
      operationId: retrieveProductsUsingPOST
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: Authorization
          in: header
          description: Bearer Token for Oauth Validation
          required: false
          type: string
        - name: transactionId
          in: header
          description: Unique GUID for the transaction
          required: false
          type: string
        - name: transactionType
          in: header
          description: 'Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc'
          required: false
          type: string
        - name: applicationId
          in: header
          description: Application id like MyTMO or TMO
          required: false
          type: string
        - name: channelId
          in: header
          description: >-
            This indicates whether the original request came from Mobile / web /
            retail / mw  etc
          required: false
          type: string
        - name: correlationId
          in: header
          description: >-
            This is used to tie multiple transactions across multiple calls (eg
            msisdn could be a correlation id , another example is a unique key
            generated which will be used across multiple api calls in the flow)
          required: false
          type: string
        - name: clientId
          in: header
          description: >-
            Unique identifier for the client (could be e-servicesUI ,
            MWSAPConsumer etc )
          required: false
          type: string
        - name: transactionBusinessKey
          in: header
          description: >-
            This is the place holder for a functional key , could be BAN or
            MSISDN .
          required: false
          type: string
        - name: transactionBusinessKeyType
          in: header
          description: 'Indicates the functional key example "BAN","MSISDN","SKU" .'
          required: false
          type: string
        - name: dealerCode
          in: header
          description: >-
            Middleware specific field wil be defaulted to '000000' for mw
            requests
          required: false
          type: string
        - in: body
          name: searchRequest
          description: Search Request
          required: true
          schema:
            $ref: '#/definitions/SearchRequest'
      responses:
        '200':
          description: Returns the List of Products
          schema:
            $ref: '#/definitions/ProductInfo'
        '201':
          description: Created
        '400':
          description: Invalid Request Message Processing - Technical Error Bad Request
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Authorization Error
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
  /products/family:
    post:
      tags:
        - List of Products
      summary: List of Products
      description: >-
        This API will return the List of Products, for the given filter criteria
        wrapped in SearchRequest.
      operationId: retrieveGroupedProductsUsingPOST
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: Authorization
          in: header
          description: Bearer Token for Oauth Validation
          required: false
          type: string
        - name: transactionId
          in: header
          description: Unique GUID for the transaction
          required: false
          type: string
        - name: transactionType
          in: header
          description: 'Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc'
          required: false
          type: string
        - name: applicationId
          in: header
          description: Application id like MyTMO or TMO
          required: false
          type: string
        - name: channelId
          in: header
          description: >-
            This indicates whether the original request came from Mobile / web /
            retail / mw  etc
          required: false
          type: string
        - name: correlationId
          in: header
          description: >-
            This is used to tie multiple transactions across multiple calls (eg
            msisdn could be a correlation id , another example is a unique key
            generated which will be used across multiple api calls in the flow)
          required: false
          type: string
        - name: clientId
          in: header
          description: >-
            Unique identifier for the client (could be e-servicesUI ,
            MWSAPConsumer etc )
          required: false
          type: string
        - name: transactionBusinessKey
          in: header
          description: >-
            This is the place holder for a functional key , could be BAN or
            MSISDN .
          required: false
          type: string
        - name: transactionBusinessKeyType
          in: header
          description: 'Indicates the functional key example "BAN","MSISDN","SKU" .'
          required: false
          type: string
        - name: dealerCode
          in: header
          description: >-
            Middleware specific field wil be defaulted to '000000' for mw
            requests
          required: false
          type: string
        - in: body
          name: searchRequest
          description: Search Request
          required: true
          schema:
            $ref: '#/definitions/GroupBySearchRequest'
      responses:
        '200':
          description: Returns the List of Products
          schema:
            $ref: '#/definitions/GroupedProductInfo'
        '201':
          description: Created
        '400':
          description: Invalid Request Message Processing - Technical Error Bad Request
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Authorization Error
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
definitions:
  GroupingInfo:
    type: object
    properties:
      data:
        type: array
        items:
          $ref: '#/definitions/GroupedData'
  SearchRequest:
    type: object
    properties:
      filterByFields:
        type: array
        description: List of Fields to be used for filtering the records.
        items:
          $ref: '#/definitions/FilterByField'
      sortByFields:
        type: array
        description: List of Fields to be used for sorting the records.
        items:
          $ref: '#/definitions/SortByField'
      deviceId:
        type: string
        example: deviceId
        description: The deviceId for which the compactible accessories are required
      familyId:
        type: string
        example: familyId
        description: The sku for which the accessories are required
      crpList:
        type: array
        items:
          type: string
        description: list of CRP values for which prices are required
      page:
        type: integer
        format: int32
        example: 1
        description: Page Number for which the records to be filtered.
      size:
        type: integer
        format: int32
        example: 30
        description: Total size of the records (for a particular Page) to be returned.
  ProductFilterValue:
    type: object
    properties:
      value:
        type: string
  Rating:
    type: object
    properties:
      reviewCount:
        type: number
        format: double
        description: The total review count
      reviewRating:
        type: number
        format: double
        description: Average review rating
  Image:
    type: object
    properties:
      name:
        type: string
      type:
        type: string
      url:
        type: string
  FinanceOptionsRequest:
    type: object
    properties:
      creditLine:
        $ref: '#/definitions/CreditLineSummaryRequest'
      crpList:
        type: array
        items:
          type: string
      skuList:
        type: array
        items:
          type: string
  CapabilityGroup:
    type: object
    properties:
      description:
        type: string
      name:
        type: string
  Feature:
    type: object
    properties:
      description:
        type: string
      name:
        type: string
  BoxContents:
    type: object
    properties:
      name:
        type: string
      values:
        type: array
        items:
          type: string
  ProductFilterData:
    type: object
    properties:
      data:
        type: array
        items:
          $ref: '#/definitions/ProductFilterOption'
  ProductFilterOption:
    type: object
    properties:
      name:
        type: string
      values:
        type: array
        items:
          $ref: '#/definitions/ProductFilterValue'
  Pagination:
    type: object
    properties:
      currentPageItems:
        type: integer
        format: int32
      pageNumber:
        type: integer
        format: int32
      pageSize:
        type: integer
        format: int32
      totalPages:
        type: integer
        format: int32
      totalRecordCount:
        type: integer
        format: int32
  EIPInfo:
    type: object
    properties:
      downPaymentModifier:
        $ref: '#/definitions/Amount'
      dueTodayAmount:
        $ref: '#/definitions/Amount'
      financeAmount:
        $ref: '#/definitions/Amount'
      termModifier:
        type: string
  GroupBySearchRequest:
    type: object
    properties:
      filterByFields:
        type: array
        description: List of Fields to be used for filtering the records.
        items:
          $ref: '#/definitions/FilterByField'
      groupSortOptions:
        type: array
        description: Sorting option to be used for the groups.
        items:
          type: string
          enum:
            - OFFER_PRICE_ASC
            - OFFER_PRICE_DESC
            - FEATURED_ASC
            - FEATURED_DESC
            - RATING_ASC
            - RATING_DESC
      page:
        type: integer
        format: int32
        example: 1
        description: Page Number for which the records to be filtered.
      size:
        type: integer
        format: int32
        example: 30
        description: Total size of the records (for a particular Page) to be returned.
      sortByFields:
        type: array
        description: List of Fields to be used for sorting the records.
        items:
          $ref: '#/definitions/SortByField'
  ProductAvailability:
    type: object
    properties:
      data:
        type: array
        items:
          $ref: '#/definitions/ProductAvailabilityData'
  FinanceOptionsDataCreditLineInfo:
    type: object
    properties:
      availableCredit:
        $ref: '#/definitions/Amount'
  ProductData:
    type: object
    properties:
      boxContents:
        $ref: '#/definitions/BoxContents'
      capabilityGroup:
        type: array
        items:
          $ref: '#/definitions/CapabilityGroup'
      category:
        type: string
        enum:
          - ACCESSORIES
          - DEVICE
          - TABLET
          - WEARABLE
          - SIMS
          - PHONE
      channelCode:
        type: string
      color:
        type: string
      condition:
        type: string
      description:
        type: string
      deviceId:
        type: string
      deviceTier:
        type: string
      family:
        type: string
      featured:
        type: integer
        format: int32
      features:
        type: array
        items:
          $ref: '#/definitions/Feature'
      images:
        type: array
        items:
          $ref: '#/definitions/Image'
      make:
        type: string
      manufacturerType:
        type: string
      marketId:
        type: string
      memory:
        type: integer
        format: int32
      memoryUOM:
        type: string
      modelName:
        type: string
      operatingSystem:
        type: string
      preOrderEndTime:
        type: string
        format: date-time
      pricing:
        $ref: '#/definitions/Price'
      productAvailableTime:
        type: string
        format: date-time
      productStatus:
        type: string
      productType:
        type: string
      promotions:
        type: array
        items:
          $ref: '#/definitions/PromotionType'
      ratings:
        $ref: '#/definitions/Rating'
      skuNumber:
        type: string
      specifications:
        type: array
        items:
          $ref: '#/definitions/Specification'
      swatch:
        type: string
      taccode:
        type: string
      productAvailability:
        $ref: '#/definitions/ProductAvailabilityData'
  ProductInfo:
    type: object
    properties:
      data:
        type: array
        items:
          $ref: '#/definitions/ProductData'
  FinanceType:
    type: object
    properties:
      lease:
        $ref: '#/definitions/Lease'
      loans:
        $ref: '#/definitions/Loan'
  PromotionType:
    type: object
    properties:
      additionalOfferDescription:
        type: string
        description: Additional details about the offer.
      additionalOfferLink:
        type: string
      additionalOfferText:
        type: string
      applyToCart:
        type: string
      description:
        type: string
        description: Promotion description
      type:
        type: string
        description: Type of the promotion
      unit:
        type: string
      value:
        type: string
        description: Value of the promotion
  Lease:
    type: object
    properties:
      crp:
        type: string
      eipInfo:
        $ref: '#/definitions/EIPInfo'
  SortByField:
    type: object
    properties:
      fieldName:
        type: string
        example: pricing.offerPrice
        description: Name of the Field to be used for sorting the records.
      sortOrder:
        type: string
        example: ASC
        description: Sort Order/Direction (ASC/DESC) to be used while sorting the records.
  GroupedProductInfo:
    type: object
    properties:
      deviceFamilyInfo:
        $ref: '#/definitions/GroupingInfo'
      paginationMetadata:
        $ref: '#/definitions/Pagination'
  GroupedData:
    type: object
    properties:
      devicesInfo:
        type: array
        items:
          $ref: '#/definitions/ProductData'
      family:
        type: string
  FinanceDetails:
    type: object
    properties:
      crp:
        type: string
      eipInfo:
        $ref: '#/definitions/EIPInfo'
  Amount:
    type: object
    properties:
      unit:
        type: string
      value:
        type: number
        format: double
  Error:
    type: object
    properties:
      code:
        type: string
      details:
        type: string
      message:
        type: string
  ProductAvailabilityData:
    type: object
    properties:
      availabilityStatus:
        type: string
        description: ' SKU Availability status'
      availableQuantity:
        type: integer
        format: int32
        description: Available quantity of the SKU
      estimatedShipDateFrom:
        type: string
        description: Estimated shipping start date
      estimatedShipDateTo:
        type: string
        description: Estimated shipping end date
      preorderAvailableEndTime:
        type: string
        description: pre order end time
      preorderAvailableStartTime:
        type: string
        description: pre order start time
      skuNumber:
        type: string
        description: the product SKU
  Specification:
    type: object
    properties:
      name:
        type: string
        description: 'The specification key, e.g. Memory'
      values:
        type: array
        description: 'The actual specification, e.g. 4GB RAM, 32GB ROM'
        items:
          type: string
  FinanceOptionsData:
    type: object
    properties:
      CreditLineInfo:
        $ref: '#/definitions/FinanceOptionsDataCreditLineInfo'
      skuItems:
        type: array
        items:
          $ref: '#/definitions/SkuItem'
  FilterByField:
    type: object
    properties:
      fieldName:
        type: string
        example: category
        description: Name of the Field to be used for filtering the records.
      fieldValues:
        type: array
        description: List of Values against the Value of the fieldName to be matched.
        items:
          type: string
  Loan:
    type: object
    properties:
      configuredDownPayment:
        $ref: '#/definitions/Amount'
      configuredTerm:
        type: string
      financeDetails:
        type: array
        items:
          $ref: '#/definitions/FinanceDetails'
  Price:
    type: object
    properties:
      baseCurrency:
        type: string
      basePrice:
        type: number
        format: double
      offerCurrency:
        type: string
      offerPrice:
        type: number
        format: double
      suggestedRetailPrice:
        $ref: '#/definitions/Amount'
      loans:
        $ref: '#/definitions/Loan'
  CreditLineSummaryRequest:
    type: object
    properties:
      accountStatus:
        type: string
        description: Billing Account status
      accountType:
        type: string
        description: Billing Account Type
      billingAccountNumber:
        type: string
        description: Billing Account Number
  FinanceOptions:
    type: object
    properties:
      data:
        $ref: '#/definitions/FinanceOptionsData'
  SkuItem:
    type: object
    properties:
      financeTypes:
        $ref: '#/definitions/FinanceType'
      promotionAmount:
        $ref: '#/definitions/Amount'
      skuNumber:
        type: string
      suggestedRetailPrice:
        $ref: '#/definitions/Amount'