# this is an example of the Cart API
# as a demonstration of an API spec in YAML
swagger: '2.0'
info:
  title: EOS Cart API
  description: This is the API specification for the EOS Cart API

  version: "2.0.0"
# the domain of the service
host: t-mobile.com
# array of all schemes that your API supports
schemes:
  - https

# will be prefixed to all paths
basePath: cart-management/v2/
produces:
  - application/json

securityDefinitions:
  OauthSecurity:
    type: oauth2
    flow: implicit
    authorizationUrl: https://account.t-mobile.com/oauth2/v1/auth?client_id=A-3amGd14-iz0&&redirect_uri=REDIRECT-URI&access_type=ONLINE&response_type=code
    scopes:
      standard: standard user
      restricted: restricted user
      permissioned: permissioned user



parameters:
     oAuth:
          name: Authorization
          type: string
          in: header
          description: Bearer Token for Oauth Validation
          required: true

     transactionId:
          name: transactionId
          type: string
          in: header
          description: unique GUID for the transaction
          required: true
     correlationId:
          in: header
          name: correlationId
          type: string
          description: This is used to tie multiple transactions across multiple calls (eg msisdn could be a correlation id , another example is a unique key generated which will be used across multiple api calls in the flow)
          required: false
     transactionBusinessKey:
          in: header
          name: transactionBusinessKey
          type: string
          description: This is place holder for a functional key , could be BAN or MSISDN .
          required: false
     transactionBusinessKeyType:
          in: header
          name: transactionBusinessKeyType
          type: string
          description: Indicates the functional key example "BAN","MSISDN","SKU" .
          required: false
     transactionType:
          in: header
          name: transactionType
          type: string
          description: Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc
          required: true
     applicationId:
          in: header
          name: applicationId
          type: string
          description: application id like MyTMO or TMO
          required: true
     channelId:
          in: header
          name: channelId
          type: string
          description: This indicates whether the original request came from Mobile / web / retail / mw  etc
          required: true
     clientId:
         in: header
         name: clientId
         type: string
         description: unique identifier for the client (could be e-servicesUI , MWSAPConsumer etc )

     dealerCode:
         in: header
         name: dealerCode
         type: string
         description: Middleware specific field wil be defaulted to '000000' for mw requests

     cartId:
          name: cartId
          type: string
          in: path
          description: This is the cart id for the customer
          required: true




paths:
  /cart/lines:
    get:
      summary: Retreive the number of lines within the cart .
      operationId: getCartLinesbySessionId
      tags: [getCartLines]
      security:
       - OauthSecurity:
         - standard
         - permissioned
      description: |
        This will get the cart details based on the cart id. This is used to identify if the cart is empty or already has valid lines. This will be used by the clients to determine which operation to call next (update cart or create cart)
      parameters:
# Added Authorization Bearer header for Oauth2
        - $ref: "#/parameters/oAuth"
        - $ref: "#/parameters/transactionId"
        - $ref: "#/parameters/correlationId"
        - $ref: "#/parameters/applicationId"
        - $ref: "#/parameters/channelId"
        - $ref: "#/parameters/clientId"
        - $ref: "#/parameters/transactionBusinessKey"
        - $ref: "#/parameters/transactionBusinessKeyType"
        - $ref: "#/parameters/transactionType"
        - $ref: "#/parameters/dealerCode"


      responses:
        200:
          description: Returns the cart infomation
          schema:
            $ref: '#/definitions/cartlines'
        400:
          description: Invalid Request Message Processing - Technical Error Bad Request
          schema:
            $ref: '#/definitions/Error'
        500:
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
        403:
          description: Authorization Error
          schema:
            $ref: '#/definitions/Error'
  /cart/{cartId}:
    get:
      summary: Retreive the cart details
      operationId: getCartBySessionId
      tags: [getCart]
      security:
       - OauthSecurity:
         - standard
         - permissioned
      description: |
        This will get the cart details based on the session id.
      parameters:
# Added Authorization Bearer header for Oauth2
        - $ref: "#/parameters/oAuth"
        - $ref: "#/parameters/transactionId"
        - $ref: "#/parameters/correlationId"
        - $ref: "#/parameters/applicationId"
        - $ref: "#/parameters/channelId"
        - $ref: "#/parameters/clientId"
        - $ref: "#/parameters/transactionBusinessKey"
        - $ref: "#/parameters/transactionBusinessKeyType"
        - $ref: "#/parameters/transactionType"
        - $ref: "#/parameters/dealerCode"
        - $ref: '#/parameters/cartId'

      responses:
        200:
          description: Returns the cart infomation
          schema:
            $ref: '#/definitions/cart'
        400:
          description: Invalid Request Message Processing - Technical Error Bad Request
          schema:
            $ref: '#/definitions/Error'
        500:
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
        404:
          description: Authorization Error
          schema:
            $ref: '#/definitions/Error'

    delete:
      produces:
        - application/json
      summary: delete the cart information
      operationId: deleteCart
      tags: [deleteCart]
      security:
       - OauthSecurity:
         - permissioned
      description: |
       This method is used to delete the  cart information for the session
      parameters:
# Added Authorization Bearer header for Oauth2
        - $ref: "#/parameters/oAuth"
        - $ref: "#/parameters/transactionId"
        - $ref: "#/parameters/correlationId"
        - $ref: "#/parameters/applicationId"
        - $ref: "#/parameters/channelId"
        - $ref: "#/parameters/clientId"
        - $ref: "#/parameters/transactionBusinessKey"
        - $ref: "#/parameters/transactionBusinessKeyType"
        - $ref: "#/parameters/transactionType"
        - $ref: "#/parameters/dealerCode"
        - $ref: '#/parameters/cartId'

      responses:
        200:
          description: Returns the success status message
          schema:
            $ref: '#/definitions/Success'
        400:
          description: Invalid Request Message Processing - Technical Error Bad Request
          schema:
            $ref: '#/definitions/Error'
        500:
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
        404:
          description: Authorization Error
          schema:
            $ref: '#/definitions/Error'

    put:
      consumes:
        - application/json
      produces:
        - application/json
        - text/xml
        - text/html
      summary: Add items to cart
      operationId: addItemtoCart
      security:
       - OauthSecurity:
         - permissioned
      tags: [addToCart,removefromCart,updateCart]
      description: |
       This method is used to  add item(s) to cart . Only specific items which need to be added will be passed in the input. The implementation will take care of appropriately adding the items.
       The specific identifiers :
        - Line level - msisdnnumber
        - plan level - plan identifier
        - device - device sku
        - accessory - accessory sku
        - plan - planidentifier
        - service - soc code
      parameters:
# Added Authorization Bearer header for Oauth2
        - $ref: "#/parameters/oAuth"
        - $ref: "#/parameters/transactionId"
        - $ref: "#/parameters/correlationId"
        - $ref: "#/parameters/applicationId"
        - $ref: "#/parameters/channelId"
        - $ref: "#/parameters/clientId"
        - $ref: "#/parameters/transactionBusinessKey"
        - $ref: "#/parameters/transactionBusinessKeyType"
        - $ref: "#/parameters/transactionType"
        - $ref: "#/parameters/dealerCode"
        - $ref: '#/parameters/cartId'
        - name: cartpayload
          in: body
          required: true
          description: posting the cart data
          schema:
           $ref: '#/definitions/cartchange'
      responses:
        200:
          description: Returns the success status message
          schema:
            $ref: '#/definitions/cart'
        400:
          description: Invalid Request Message Processing - Technical Error Bad Request
          schema:
            $ref: '#/definitions/Error'
        500:
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
        404:
          description: Authorization Error
          schema:
            $ref: '#/definitions/Error'

  /cart:
    post:
      consumes:
        - application/json
      produces:
        - application/json
        - text/xml
        - text/html
      summary: create the cart information
      operationId: createCart
      tags: [createCart]
      security:
       - OauthSecurity:
         - permissioned
      description: |
       This method is used to create cart information
      parameters:
# Added Authorization Bearer header for Oauth2
        - $ref: "#/parameters/oAuth"
        - $ref: "#/parameters/transactionId"
        - $ref: "#/parameters/correlationId"
        - $ref: "#/parameters/applicationId"
        - $ref: "#/parameters/channelId"
        - $ref: "#/parameters/clientId"
        - $ref: "#/parameters/transactionBusinessKey"
        - $ref: "#/parameters/transactionBusinessKeyType"
        - $ref: "#/parameters/transactionType"
        - $ref: "#/parameters/dealerCode"
        - name: createCartRequest
          in: body
          required: true
          description: posting the cart data
          schema:
           $ref: '#/definitions/createCartP'
      responses:
        200:
          description: Returns the success status message
          schema:
            $ref: '#/definitions/Success'
        400:
          description: Invalid Request Message Processing - Technical Error Bad Request
          schema:
            $ref: '#/definitions/Error'
        500:
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
        404:
          description: Authorization Error
          schema:
            $ref: '#/definitions/Error'


definitions:
  cartlines:
    type: object
    properties:
        cartEmpty:
            type: boolean
            description: flag which indicates whether there is atleast one line in the cart . True indicates that the cart needs to be created . False indicates that create cart operation needs to be called.
        msisdnList:
            type: array
            items:
                type: string
  cartchange:
    type: object
    properties:
       transactionMetadata:
         $ref: '#/definitions/transactionMetadataP'
       opCode:
         type: string
         description: operation code for adding/removing/updating cart element
          - pass "ADD" for adding items
          - pass "REMOVE" for removing items
          - pass "UPDATE" to update existing items
       cartPriceSummary:
         $ref: '#/definitions/pricingOption'
       lines:
         type: array
         items:
           $ref: '#/definitions/line'
  createCartP:
    type: object
    properties:
       transactionMetadata:
         $ref: '#/definitions/transactionMetadataP'
       cartPriceSummary:
         $ref: '#/definitions/pricingOption'
       lines:
         type: array
         items:
           $ref: '#/definitions/line'

  cart:
    type: object
    properties:
       cartMetadata:
        type: object
        properties:
            cartId:
             type: string
             description: unique cart id
            cartsessionduration:
             type: number
             description: duration in minutes for keeping the cart data.
            cartsessioncreationtime:
             type: number
             description: cart creation time in minutes.
            remainingcartsessiontime:
             type: number
             description: duration in minutes remaining before cart expires.
       transactionMetadata:
         $ref: '#/definitions/transactionMetadataP'
       cartPriceSummary:
         $ref: '#/definitions/pricingOption'
       promotions:
        type: array
        items:
         $ref: '#/definitions/promotion'
       lines:
         type: array
         items:
           $ref: '#/definitions/line'
       errors:
         type: array
         items:
           $ref: '#/definitions/Error'

  transactionMetadataP:
       type: object
       properties:
        transactionId:
         type: string
         description: unique transaction id
        sourceSystem:
         type: string
         description: system from where the origin is (exammple web , app etc)
        requestDecription:
         type: string
        userId:
         type: string
         description: login user id

  line:
    type: object
    properties:

      ban:
       type: string
       description: Billing account number

      msisdn:
       type: string
       description: This is the subscribers phone number
      subscriberName:
       type: string
       description: Name of the subscriber
      creditLevel:
       type: string
       description: Name of the subscriber
      linePriceSummary:
       $ref: '#/definitions/pricingOption'
      promotions:
       type: array
       items:
        $ref: '#/definitions/promotion'
      items:
       type: object
       properties:
        device:
         $ref: '#/definitions/device'
        accessories:
         type: array
         items:
           $ref: '#/definitions/accessory'
        plans:
         type: array
         items:
          $ref: '#/definitions/plan'
        services:
           type: array
           items:
             $ref: '#/definitions/service'
        tradeIn:
         type: object
         $ref: '#/definitions/tradeIn'
        financePayoff:
          type: object
          $ref: '#/definitions/financePayoff'

  device:
    type: object
    properties:
     sku:
      type: string
      description: This gives the sku code for the device
     modelName:
      type: string
      description: This gives the modelName for the device
     familyId:
      type: string
      description: This gives the familyId for the device
     fullRetailPrice:
        $ref: '#/definitions/Amount'
        description: The device monthly price.
     color:
      type: string
      description: Color of the Product
     colorSwatch:
      type: string
      description: Image of the color
     description:
      type: string
      description: Description of the Product

     legalText:
      type: string
     memory:
      type: string
      description: Memory Capacity
     memoryUom:
      type: string
      description: Unit of memory
     imageUrl:
        type: string
        description: URL for the Product Image
     productSubType:
        type: string
        description: The product subtype such as Handset/Tablet/Simcard
     deviceAvailability:
      $ref: '#/definitions/availability'

     pricingOptions:
      type: array
      items:
        $ref: '#/definitions/pricingOption'
     promotions:
      type: array
      items:
        $ref: '#/definitions/promotion'

  accessory:
    type: object
    properties:
     sku:
      type: string
      description: This gives the sku code for the device
     color:
      type: string
      description: Color of the Product
     colorSwatch:
      type: string
      description: Image of the color
     description:
      type: string
      description: Description of the Product

     legalText:
      type: string
     memory:
      type: string
      description: Memory Capacity
     memoryUOM:
      type: string
      description: Unit of memory
     ImageURL:
        type: string
        description: URL for the Product Image

     quantity:
      type: number
      format: int32
      description: This will contain the quantity of accessories ordered
     pricingOptions:
      type: array
      items:
        $ref: '#/definitions/pricingOption'
     promotions:
      type: array
      items:
        $ref: '#/definitions/promotion'

  plan:
   type: object
   properties:
     planId:
      type: string
     description:
      type: string
     type:
      type: string
      description: Type of the plan
     name:
      type: string
      description: Name of the plan

     pricing:
       $ref: '#/definitions/pricingOption'
     action:
       $ref: '#/definitions/Action'

  service:
    type: object
    properties:
     socCode:
      type: string
     socIndicator:
      type: string
      description: This is a mandatory field . This indicate the soc indicator code - "PBX" , "MBX" etc
     description:
      type: string
     serviceName:
      type: string

     pricing:
       $ref: '#/definitions/pricingOption'
     action:
       $ref: '#/definitions/Action'
  pricingOption:
    type: object
    properties:
      type:
        type: string
        description: Identifies the pricing option type(FULL/EIP/CLUB)
        enum:
          - FULL
          - EIP
          - CLUB
          - FRP

      payNowAmount:
        $ref: '#/definitions/price'
        description: The amount due today.
      monthlyAmount:
        $ref: '#/definitions/price'
        description: The amount due monthly.
  price:
    type: object
    properties:
      amount:
        type: number
        format: float
        description: Amount value
      currency:
        type: string
        description: Name of currency
      noOfMonths:
        type: string
        description: No Of Months for EIP
      display:
        type: string
        description: Amount value

  promotion:
    type: object
    properties:
      promoType:
        type: string
        description: Type of Promo Applied
      promoCode:
        type: string
        description: Promo code to be applied
      promoId:
        type: string
        description: Promo ID
      promoTitle:
        type: string
        description: Name of the promo applied
      discountValue:
        type: number
        format: float
      description:
        type: string

  availability:
    type: object
    properties:
      estimatedShipDateFrom:
        type: string
        description: Amount value
      estimatedShipDateTo:
        type: string
        description: Name of currency
      availabilityStatus:
        type: string
        description: No Of Months for EIP
  tradeIn:
    type: object
    properties:
      tradeInId:
        type: string
        description: Commerce platform specific Trade-id which is used in update/delete trade-ins API.
      tradeInPrice:
        type: string
        description: trade in value of the device
      promoName:
        type: string
        description: Name of the promotion applied
      disposition:
        type: string
        description: dispostion from the user. ACCEPT or ACCEPT DEFERRED
      deviceMake:
        type: string
        description: Manufacturer of the trade in device
      deviceModel:
        type: string
        description: Model of the trade in device
      deviceCarrier:
        type: string
        description: Current carrier of the device being traded in
      deviceIMEI:
        type: string
        description: IMEI of the device being traded in
      quoteId:
        type: string
        description: Tradein Quote id from the financial system
      tradeInType:
        type: string
        enum:
          - STANDARD
          - CARRIERFREEDOM
          - DOLLARZEROPROMO
        description: It is for type of the trade-in.
      deviceCondition:
        type: string
        enum:
          - BAD
          - GOOD
          - VERY GOOD
          - EXCELLENT
        description: It is used to define the Tradein device condition.
      deviceSku:
        type: string
        description: Device sku for the TradeIn.
      deviceName:
        type: string
        description: Device Name for the TradeIn.
      excludeFairMarketValue:
        type: boolean
        description: flag to indicate whether to exclude fair market value
  financePayoff:
    type: object
    properties:
      agreementId:
        type: string
        description: Agreement ID against which the balance is being charged.
      financeType:
        type: string
        description: Finance type.
        enum:
          - LOAN
          - LEASE
      loanSystem:
        type: string
        description: Loan System.
        enum:
          - EIP
          - OFS
      equipmentId:
        type: string
        description: Installment equipment id.
      balancePayNowAmount:
        $ref: '#/definitions/price'
  Error:
    type: object
    properties:
      code:
        type: integer
        format: int32
        description: This is the  error code which is custom for the microservice
      message:
        type: string
        description: Error message for the microservice
      details:
        type: string
        description: Additional information for the error
  Success:
    type: object
    properties:
      code:
        type: integer
        format: int32
        description: This is the  error code which is custom for the microservice
      cartId:
        type: string
        description: This is the cart  id
      cart:
        $ref: '#/definitions/cart'
      message:
        type: string
        description: Error message for the microservice
      details:
        type: string
        description: Additional information for the error
  Action:
    type: object
    properties:
       actionCode:
        type: string
        description: This will denote the acton to be taken on the specific item
         - device - action code is "ADD"
         - Accessory - action code is "ADD"
         - plan - action code is "ADD/REMOVE"
         - service - action code is "ADD/REMOVE"
       itemLevel:
        type: string
        description: This will indicate whether the item is account level or plan level
        enum:
          - ACCOUNT
          - LINE
  Amount:
    type: "object"
    properties:
      unit:
        type: "string"
      value:
        type: "number"
        format: "double"
