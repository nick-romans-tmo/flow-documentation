swagger: '2.0'
info:
  title: EOS Billing Service
  description: Microservices to fetch customer's bills and bill details
  version: 0.0.2
  contact:
    name: 'P&T eServices PUB (Payments, Usage, Billing) team'
    email: web_pub_core@t-mobile.com
host: eos.eitss.tmobile.com
schemes:
  - https
  - http
basePath: /v2/billing
produces:
  - application/json
consumes:
  - application/json
parameters:
  oAuth:
    name: Authorization
    type: string
    in: header
    description: Bearer Token for oAuth validation
    required: true
  transactionId:
    name: transactionId
    type: string
    in: header
    description: Unique GUID for the transaction
    required: true
  correlationId:
    in: header
    name: correlationId
    type: string
    description: >-
      This is used to tie multiple transactions across multiple calls (a unique
      key generated will be used across multiple api calls in the flow)
    required: true
  transactionBusinessKey:
    in: header
    name: transactionBusinessKey
    type: string
    description: 'This is placeholder for a functional key, the value is MSISDN'
    required: true
  transactionBusinessKeyType:
    in: header
    name: transactionBusinessKeyType
    type: string
    description: 'Indicates the functional key, MSISDN for billing APIs'
    required: true
  transactionType:
    in: header
    name: transactionType
    type: string
    description: >-
      Indicates the kind of transaction e.g. AAL, CIHU, inventoryfeed, billing,
      payments etc.
    required: true
  applicationId:
    in: header
    name: applicationId
    type: string
    description: Application id like MyTMO or TMO
    required: true
  channelId:
    in: header
    name: channelId
    type: string
    description: >-
      This indicates whether the original request came from mobile/web/retail/mw
      etc.
    required: true
  channelVersion:
    in: header
    name: channelVersion
    type: string
    description: This indicates the browser type from where the original request came from.
    required: true
  clientId:
    in: header
    name: clientId
    type: string
    description: >-
      Unique identifier for the client (could be e-servicesUI, MWSAPConsumer
      etc.)
    required: true
  userRole:
    name: userRole
    type: string
    in: header
    description: Logged in user's role
    required: true
  ban:
    name: ban
    type: string
    in: header
    description: Billing account number
    required: true
  msisdn:
    name: msisdn
    type: string
    in: header
    description: Billing MSISDN
    required: true
  billingCycleEndDate:
    name: billingCycleEndDate
    type: string
    in: header
    description: Billing end cycle date
    required: true
  easyPayIndicator:
    name: easyPayIndicator
    type: string
    in: header
    description: Easy pay indicator
    required: true
  sprintaccountnumber:
    name: sprintaccountnumber
    type: string
    in: header
    description: Customer Account number associated with SPRINT
    x-example: string
    required: false
  sprintmigrationindicator:
    name: sprintmigrationindicator
    type: string
    in: header
    description: 'A indicator to decide whether response should be from tmobile db only or sprint Api only or if there is no indicatior at all then we do asynchronous calls to both the tmobile and sprint and merge the responses. '
    x-example: string
    required: false
paths:
  /cycles:
    get:
      summary: 'Gets the bill list from eBill, BriteBill & Sprint via Apigee'
      description: Gets an array of bills for the customer
      parameters:
        - $ref: '#/parameters/oAuth'
        - $ref: '#/parameters/transactionId'
        - $ref: '#/parameters/correlationId'
        - $ref: '#/parameters/applicationId'
        - $ref: '#/parameters/channelId'
        - $ref: '#/parameters/channelVersion'
        - $ref: '#/parameters/clientId'
        - $ref: '#/parameters/transactionBusinessKey'
        - $ref: '#/parameters/transactionBusinessKeyType'
        - $ref: '#/parameters/transactionType'
        - $ref: '#/parameters/sprintaccountnumber'
        - $ref: '#/parameters/sprintmigrationindicator'
        - name: ban
          in: query
          description: Billing account number
          required: true
          type: string
      operationId: getBillList
      tags:
        - BillList
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Bills'
        '400':
          description: Bad request
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized
          schema:
            $ref: '#/definitions/Error'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/Error'
        '503':
          description: Service Unavailable
          schema:
            $ref: '#/definitions/Error'
  /details:
    get:
      summary: 'Gets the bill details from Brite:Bill'
      description: Gets the bill details in the form of encoded string
      parameters:
        - $ref: '#/parameters/oAuth'
        - $ref: '#/parameters/transactionId'
        - $ref: '#/parameters/correlationId'
        - $ref: '#/parameters/applicationId'
        - $ref: '#/parameters/channelId'
        - $ref: '#/parameters/channelVersion'
        - $ref: '#/parameters/clientId'
        - $ref: '#/parameters/transactionBusinessKey'
        - $ref: '#/parameters/transactionBusinessKeyType'
        - $ref: '#/parameters/transactionType'
        - name: ban
          in: query
          description: Billing account number for samson.
          required: true
          type: string
        - name: documentId
          in: query
          description: Document Id
          required: true
          type: string
        - name: format
          in: query
          description: Format of bill Summary or Detail
          required: true
          type: string
        - name: type
          in: query
          description: Type of bill (PDF/JSON)
          required: true
          type: string
      operationId: getBillDetails
      tags:
        - BillDetails
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/BillDetails'
        '400':
          description: Bad request
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized
          schema:
            $ref: '#/definitions/Error'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/Error'
        '503':
          description: Service Unavailable
          schema:
            $ref: '#/definitions/Error'
  /usagedetails:
    get:
      summary: 'Gets the bill details from Brite:Bill'
      description: Gets the bill details in the form of JSON
      parameters:
        - $ref: '#/parameters/oAuth'
        - $ref: '#/parameters/transactionId'
        - $ref: '#/parameters/correlationId'
        - $ref: '#/parameters/applicationId'
        - $ref: '#/parameters/channelId'
        - $ref: '#/parameters/channelVersion'
        - $ref: '#/parameters/clientId'
        - $ref: '#/parameters/transactionBusinessKey'
        - $ref: '#/parameters/transactionBusinessKeyType'
        - $ref: '#/parameters/transactionType'
        - name: ban
          in: query
          description: Billing account number for samson.
          required: true
          type: string
        - name: documentId
          in: query
          description: Document Id
          required: true
          type: string
        - name: format
          in: query
          description: Format of bill Summary or Detail
          required: true
          type: string
        - name: type
          in: query
          description: Type of bill (PDF/JSON)
          required: true
          type: string
      operationId: getBillDetailsJSON
      tags:
        - BillDetails
      responses:
        '200':
          description: OK
          schema:
            type: string
        '400':
          description: Bad request
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized
          schema:
            $ref: '#/definitions/Error'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/Error'
        '503':
          description: Service Unavailable
          schema:
            $ref: '#/definitions/Error'
  /dataset:
    get:
      summary: 'Gets the data set for Brite:Bill'
      description: Gets the data set in the form of JSON
      parameters:
        - $ref: '#/parameters/oAuth'
        - $ref: '#/parameters/transactionId'
        - $ref: '#/parameters/correlationId'
        - $ref: '#/parameters/applicationId'
        - $ref: '#/parameters/channelId'
        - $ref: '#/parameters/channelVersion'
        - $ref: '#/parameters/clientId'
        - $ref: '#/parameters/transactionBusinessKey'
        - $ref: '#/parameters/transactionBusinessKeyType'
        - $ref: '#/parameters/transactionType'
        - $ref: '#/parameters/userRole'
        - $ref: '#/parameters/ban'
        - $ref: '#/parameters/msisdn'
        - $ref: '#/parameters/billingCycleEndDate'
        - $ref: '#/parameters/easyPayIndicator'
      operationId: getDataSet
      tags:
        - BriteBillDataSet
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/BriteBillDataSet'
        '400':
          description: Bad request
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized
          schema:
            $ref: '#/definitions/Error'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/Error'
        '503':
          description: Service Unavailable
          schema:
            $ref: '#/definitions/Error'
definitions:
  BillDetails:
    type: object
    properties:
      statusCode:
        type: string
        description: ''
      statusMessage:
        type: string
        description: ''
      jsonPayload:
        type: string
        description: ''
      payload:
        type: string
        description: Base64 payload
  Error:
    type: object
    properties:
      code:
        type: integer
        description: Error code
      message:
        type: string
        description: Error message
  Bills:
    type: object
    properties:
      statusCode:
        type: string
        description: '0 - Success, 999 - Backend System Error'
      statusMessage:
        type: string
        description: A description of the status code or error message from backend
      BillList:
        type: array
        items:
          $ref: '#/definitions/BillList'
  BillList:
    type: object
    properties:
      billStatus:
        $ref: '#/definitions/BillStatus'
      invoiceCharges:
        description: An array of invoiceCharges objects.
        type: array
        items:
          $ref: '#/definitions/InvoiceCharges'
      currentBalanceAmount:
        description: currentBalanceAmount
        type: number
        format: double
      billCycleStartDate:
        description: Start date of the bill cycle
        type: string
        format: date
      billCycleCloseDate:
        description: End date of bill cycle
        type: string
        format: date
      billSequenceNumber:
        description: Identifier of the statement from eBill system
        type: string
      documentId:
        description: Identifier of the statement from BriteBill system
        type: string
      format:
        description: 'Bill format, PDF, xml, JSON etc'
        type: string
      accountBillId:
        description: accountbillId
        type: string
      invoiceDate:
        type: string
        format: date
        description: Date of invoice in YYYY-MM-DD format
      invoiceType:
        type: string
        minLength: 1
        maxLength: 1
        description: 'Invoice Type (D - Detailed Invoice, S - Summary Invoice)'
      billingSystemCode:
        type: string
        maxLength: 3
        description: Indicates billing system that bill was generated from (UBP - Ensemble)
  BillStatus:
    type: object
    properties:
      statusCode:
        description: Code that specifies the status of the bill.
        type: string
  InvoiceCharges:
    type: object
    description: Total Charge amount for various charge types . This is the sum of all the line Charge Amounts.
    properties:
      amount:
        description: Charge Amount in the transactional currency
        type: number
        format: double
  BriteBillDataSet:
    type: object
    properties:
      statusCode:
        type: string
        description: ''
      statusMessage:
        type: string
        description: ''
      paymentReceived:
        type: array
        items:
          $ref: '#/definitions/PaymentReceived'
      unbilledChargesAndCreditsDetails:
        type: array
        items:
          $ref: '#/definitions/UnbilledChargesAndCreditsDetails'
      arBalance:
        $ref: '#/definitions/ArBalance'
      currentBillCharges:
        $ref: '#/definitions/CurrentBillCharges'
      autoPay:
        $ref: '#/definitions/AutoPay'
      eipDetailsSection:
        $ref: '#/definitions/EipDetailsSection'
      jodDetailsSection:
        $ref: '#/definitions/JodDetailsSection'
      paymentArrangementDetails:
        $ref: '#/definitions/PaymentArrangementDetails'
      loggedInUserInfo:
        $ref: '#/definitions/LoggedInUserInfo'
      pastdueAmountDetails:
        $ref: '#/definitions/PastdueAmountDetails'
      paperlessBilling:
        $ref: '#/definitions/PaperlessBilling'
      accountNumber:
        type: string
        description: ''
      msisdn:
        type: string
        description: ''
  PaymentReceived:
    type: object
    properties:
      amount:
        type: string
        description: ''
      date:
        type: string
        description: ''
      cardNumber:
        type: string
        description: ''
      cardType:
        type: string
        description: ''
      authorized:
        type: boolean
        description: ''
      activityCode:
        type: string
        description: ''
      activityReasonCode:
        type: string
        description: ''
      activityReasonDesc:
        type: string
        description: ''
  UnbilledChargesAndCreditsDetails:
    type: object
    properties:
      type:
        type: string
        description: ''
      date:
        type: string
        description: ''
      amount:
        type: string
        description: ''
      balanceImpactCode:
        type: string
        description: ''
      activityCode:
        type: string
        description: ''
      activityReasonCode:
        type: string
        description: ''
      activityReasonDesc:
        type: string
        description: ''
  ArBalance:
    type: object
    properties:
      balanceDue:
        type: string
        description: ''
  CurrentBillCharges:
    type: object
    properties:
      currentBillDueAmount:
        type: string
        description: ''
  AutoPay:
    type: object
    properties:
      easyPayStatus:
        type: boolean
        description: ''
      scheduledDate:
        type: string
        description: ''
      dueDate:
        type: string
        description: ''
      amount:
        type: string
        description: ''
      autoPaySavings:
        type: string
        description: ''
      paymentMethod:
        type: string
        description: ''
      CreditCardDetails:
        $ref: '#/definitions/CreditCardDetails'
      CheckDetails:
        $ref: '#/definitions/CheckDetails'
  CreditCardDetails:
    type: object
    properties:
      cardNumber:
        type: string
        description: ''
      cardType:
        type: string
        description: ''
      cardExpirationDate:
        type: string
        description: ''
  CheckDetails:
    type: object
    properties:
      checkNumber:
        type: string
        description: ''
  EipDetailsSection:
    type: object
    properties:
      totalEIPBalance:
        type: string
        description: ''
      isEIPDetailsExists:
        type: boolean
        description: ''
  JodDetailsSection:
    type: object
    properties:
      totalJODBalance:
        type: string
        description: ''
      isJumpLeaseDetailsExists:
        type: boolean
        description: ''
  PaymentArrangementDetails:
    type: object
    properties:
      paymentArrangementIndicator:
        type: boolean
        description: ''
      PaymentArrangementData:
        type: array
        items:
          $ref: '#/definitions/PaymentArrangementData'
  PaymentArrangementData:
    type: object
    properties:
      paymentDate:
        type: string
        description: ''
      amount:
        type: string
        description: ''
      paymentArrangementType:
        type: string
        description: ''
      paymentMethod:
        type: string
        description: ''
      paymentStatus:
        type: string
        description: ''
      cardDetails:
        $ref: '#/definitions/CreditCardDetails'
      BankDetails:
        $ref: '#/definitions/BankDetails'
  BankDetails:
    type: object
    properties:
      bankAccNumber:
        type: string
        description: ''
      bankAccType:
        type: string
        description: ''
  LoggedInUserInfo:
    type: object
    properties:
      firstName:
        type: string
        description: ''
      lastName:
        type: string
        description: ''
      nickName:
        type: string
        description: ''
      userRole:
        type: string
        description: ''
  PastdueAmountDetails:
    type: object
    properties:
      pastdueAmountIndicator:
        type: boolean
        description: ''
      pastdueAmount:
        type: string
        description: ''
  PaperlessBilling:
    type: object
    properties:
      paperlessBillingIndicator:
        type: string
        description: ''
      paperlessBillAccEligibility:
        type: boolean
        description: ''