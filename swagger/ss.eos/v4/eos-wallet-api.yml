swagger: "2.0"
info:
  title: EOS Wallet Microservice
  description: EOS Wallet
  version: 0.0.4
  contact:
    name: Web PUB Core Team
    email: Web_PUB_Core@T-Mobile.com
host: eos.corporate.t-mobile.com
produces:
  - application/json
consumes:
  - application/json
basePath: /wallet/v4
schemes:
  - https
parameters:
  msisdn:
    name: msisdn
    in: header
    description: Customer msisdn
    type: string
    required: false
  interactionId:
    name: interactionId
    in: header
    required: false
    type: string
    description: Unique identifier for transaction, passed from UI and common accross service calls
  workflowId:
    name: workflowId
    in: header
    required: false
    type: string
    description: workflow identifier required by MW
  Authorization:
    name: Authorization
    in: header
    required: true
    type: string
    description: Authorization token required by MW
  channelId:
    name: channel_id
    in: header
    required: true
    type: string
    description: "application client id, passed from UI"
  channel:
    name: channel
    in: header
    required: false
    type: string
    description: channel for BI logging
  channelVersion:
    name: channelVersion
    in: header
    required: false
    type: string
    description: channel version for BI logging
  tmoId:
    name: tmoId
    in: header
    required: false
    type: string
    description: TMOID associated with IAM account. This is for future work.
  ban:
    name: ban
    in: header
    required: false
    type: string
    description: BAN
  usn:
    name: usn
    in: header
    required: false
    type: string
    description: Unique session id for user. It will be returned by IAM. (CIS requirement)
  applicationId:
    name: application_id
    in: header
    required: false
    type: string
    description: Application id, passed from UI
  clientId:
    name: client_id
    in: header
    required: false
    type: string
    description: Client id, passed from UI
tags:
  - name: Encryption
    description: get public key
  - name: Validation
    description: Card, Bank account and routing number validation
  - name: StoredPaymentMethods
    description: Get Stored Payment Methods and  Save/Delete  Payment Methods by alias
  - name: ManagePaymentInstruments
    description: Add/update Payment Instrument to wallet with $0 authorization
paths:
  /publicKey:
    get:
      tags:
        - Encryption
      summary: Get public key for card/account number encryption.
      operationId: getPublickey
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - $ref: "#/parameters/Authorization"
        - $ref: "#/parameters/interactionId"
        - $ref: "#/parameters/msisdn"
        - $ref: "#/parameters/workflowId"
        - $ref: "#/parameters/tmoId"
        - $ref: "#/parameters/ban"
        - $ref: "#/parameters/usn"
        - $ref: "#/parameters/applicationId"
        - $ref: "#/parameters/clientId"
        - $ref: "#/parameters/channelId"
        - $ref: "#/parameters/channel"
        - $ref: "#/parameters/channelVersion"
      responses:
        "200":
          description: OK
          schema:
            $ref: "#/definitions/PublicKeyResponse"
        "400":
          description: Bad Request
          schema:
            $ref: "#/definitions/Error"
        "401":
          description: Unauthorized
          schema:
            $ref: "#/definitions/Error"
        "500":
          description: Internal Server Error
          schema:
            $ref: "#/definitions/Error"
  /validatePaymentMethod:
    post:
      tags:
        - Validation
      summary: validate Payment Method
      description: This API validates card/account number.
      operationId: validatePaymentMethod
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - $ref: "#/parameters/Authorization"
        - $ref: "#/parameters/interactionId"
        - $ref: "#/parameters/msisdn"
        - $ref: "#/parameters/workflowId"
        - $ref: "#/parameters/tmoId"
        - $ref: "#/parameters/ban"
        - $ref: "#/parameters/usn"
        - $ref: "#/parameters/applicationId"
        - $ref: "#/parameters/clientId"
        - $ref: "#/parameters/channelId"
        - $ref: "#/parameters/channel"
        - $ref: "#/parameters/channelVersion"
        - in: body
          name: paymentMethodValidationRequest
          description: Payment Method Validation Details
          required: true
          schema:
            $ref: "#/definitions/PaymentMethodValidationRequest"
      responses:
        "200":
          description: OK
          schema:
            $ref: "#/definitions/PaymentMethodValidationResponse"
        "201":
          description: Created
        "400":
          description: Bad Request
          schema:
            $ref: "#/definitions/Error"
        "401":
          description: Unauthorized
        "500":
          description: Internal Server Error
          schema:
            $ref: "#/definitions/Error"
  /validateRoutingNumber/{routingNumber}:
    post:
      tags:
        - Validation
      summary: Validate Routing Number
      description: Validate Routing Number
      operationId: getAbaRoutingDetails
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - $ref: "#/parameters/Authorization"
        - $ref: "#/parameters/interactionId"
        - $ref: "#/parameters/msisdn"
        - $ref: "#/parameters/workflowId"
        - $ref: "#/parameters/tmoId"
        - $ref: "#/parameters/ban"
        - $ref: "#/parameters/usn"
        - $ref: "#/parameters/applicationId"
        - $ref: "#/parameters/clientId"
        - $ref: "#/parameters/channelId"
        - $ref: "#/parameters/channel"
        - $ref: "#/parameters/channelVersion"
        - name: routingNumber
          in: path
          description: bank routing number
          required: true
          type: string
      responses:
        "200":
          description: OK
          schema:
            $ref: "#/definitions/BankDetails"
        "400":
          description: Bad Request
          schema:
            $ref: "#/definitions/Error"
        "401":
          description: Unauthorized
          schema:
            $ref: "#/definitions/Error"
        "500":
          description: Internal Server Error
          schema:
            $ref: "#/definitions/Error"
  /spm:
    get:
      tags:
        - StoredPaymentMethods
      summary: get Stored Payment Methods
      description: Gets stored payment methods and details on an account number.
      operationId: getPaymentMethods
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: accountNumber
          in: path
          description: customer's billing account number
          required: true
          type: number
          format: double
        - $ref: "#/parameters/Authorization"
        - $ref: "#/parameters/interactionId"
        - $ref: "#/parameters/msisdn"
        - $ref: "#/parameters/workflowId"
        - $ref: "#/parameters/tmoId"
        - $ref: "#/parameters/ban"
        - $ref: "#/parameters/usn"
        - $ref: "#/parameters/applicationId"
        - $ref: "#/parameters/clientId"
        - $ref: "#/parameters/channelId"
        - $ref: "#/parameters/channel"
        - $ref: "#/parameters/channelVersion"
        - name: walletCapacity
          in: header
          description: Wallet Capacity
          required: false
          type: integer
          format: int32
      responses:
        "200":
          description: Successful response
          schema:
            $ref: "#/definitions/WalletDetails"
        "400":
          description: Bad request
          schema:
            $ref: "#/definitions/Error"
        "401":
          description: Unauthorized
          schema:
            $ref: "#/definitions/Error"
        "500":
          description: Internal server error
          schema:
            $ref: "#/definitions/Error"
  /spm/verifyPaymentMethod:
    post:
      tags:
        - StoredPaymentMethods
      summary: validate migrated Payment Method
      description: This API validates migrated card/account number.
      operationId: verifyPaymentMethod
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - $ref: "#/parameters/Authorization"
        - $ref: "#/parameters/interactionId"
        - $ref: "#/parameters/msisdn"
        - $ref: "#/parameters/workflowId"
        - $ref: "#/parameters/tmoId"
        - $ref: "#/parameters/ban"
        - $ref: "#/parameters/usn"
        - $ref: "#/parameters/applicationId"
        - $ref: "#/parameters/clientId"
        - $ref: "#/parameters/channelId"
        - $ref: "#/parameters/channel"
        - $ref: "#/parameters/channelVersion"
        - in: body
          name: paymentMethodValidationRequest
          description: Payment Method Validation Details
          required: true
          schema:
            $ref: "#/definitions/VerifyPaymentMethodRequest"
      responses:
        "200":
          description: OK
          schema:
            $ref: "#/definitions/PaymentMethodResponse"
        "201":
          description: Created
        "400":
          description: Bad Request
          schema:
            $ref: "#/definitions/Error"
        "401":
          description: Unauthorized
        "500":
          description: Internal Server Error
          schema:
            $ref: "#/definitions/Error"

  /mpi:
    post:
      tags:
        - ManagePaymentInstruments
      summary: Add payment Instrument
      description: Add payment Instrument to wallet with $0 authorizaton.
      operationId: addPaymentInstrument
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: accountNumber
          in: path
          description: customer's billing account number
          required: true
          type: number
          format: double
        - $ref: "#/parameters/Authorization"
        - $ref: "#/parameters/interactionId"
        - $ref: "#/parameters/msisdn"
        - $ref: "#/parameters/workflowId"
        - $ref: "#/parameters/tmoId"
        - $ref: "#/parameters/ban"
        - $ref: "#/parameters/usn"
        - $ref: "#/parameters/applicationId"
        - $ref: "#/parameters/clientId"
        - $ref: "#/parameters/channelId"
        - $ref: "#/parameters/channel"
        - $ref: "#/parameters/channelVersion"
        - in: body
          name: paymentMethodRequest
          description: Payment Method Details
          required: true
          schema:
            $ref: "#/definitions/PaymentMethodRequest"
      responses:
        "200":
          description: Successful response
          schema:
            $ref: "#/definitions/PaymentMethodResponse"
        "400":
          description: Bad request
          schema:
            $ref: "#/definitions/Error"
        "401":
          description: Unauthorized
          schema:
            $ref: "#/definitions/Error"
        "500":
          description: Internal server error
          schema:
            $ref: "#/definitions/Error"
    put:
      tags:
        - ManagePaymentInstruments
      summary: update Payment Instrument
      description: update Payment Instrument to wallet with $0 authorizaton.
      operationId: updatePaymentInstrument
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - $ref: "#/parameters/Authorization"
        - $ref: "#/parameters/interactionId"
        - $ref: "#/parameters/msisdn"
        - $ref: "#/parameters/workflowId"
        - $ref: "#/parameters/tmoId"
        - $ref: "#/parameters/ban"
        - $ref: "#/parameters/usn"
        - $ref: "#/parameters/applicationId"
        - $ref: "#/parameters/clientId"
        - $ref: "#/parameters/channelId"
        - $ref: "#/parameters/channel"
        - $ref: "#/parameters/channelVersion"
        - in: body
          name: paymentMethodRequest
          description: Payment Method Details
          required: true
          schema:
            $ref: "#/definitions/PaymentMethodRequest"
      responses:
        "200":
          description: OK
          schema:
            $ref: "#/definitions/PaymentMethodResponse"
        "400":
          description: Bad Request
          schema:
            $ref: "#/definitions/Error"
        "401":
          description: Unauthorized
          schema:
            $ref: "#/definitions/Error"
        "500":
          description: Internal Server Error
          schema:
            $ref: "#/definitions/Error"
    delete:
      tags:
        - ManagePaymentInstruments
      summary: delete Payment Method
      description: This API deletes stored payment methods for given ban
      operationId: deletePaymentMethod
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - $ref: "#/parameters/Authorization"
        - $ref: "#/parameters/interactionId"
        - $ref: "#/parameters/msisdn"
        - $ref: "#/parameters/workflowId"
        - $ref: "#/parameters/tmoId"
        - $ref: "#/parameters/ban"
        - $ref: "#/parameters/usn"
        - $ref: "#/parameters/applicationId"
        - $ref: "#/parameters/clientId"
        - $ref: "#/parameters/channelId"
        - $ref: "#/parameters/channel"
        - $ref: "#/parameters/channelVersion"
        - in: body
          name: paymentMethodRequest
          description: Payment Method details
          required: true
          schema:
            $ref: "#/definitions/PaymentMethodRequest"
      responses:
        "200":
          description: OK
          schema:
            $ref: "#/definitions/PaymentMethodResponse"
        "400":
          description: Bad request
        "401":
          description: Unauthorized
          schema:
            $ref: "#/definitions/Error"
        "500":
          description: Internal server error
          schema:
            $ref: "#/definitions/Error"
definitions:
  Error:
    type: object
    properties:
      code:
        type: string
      systemMessage:
        type: string
      userMessage:
        type: string
  PublicKeyResponse:
    type: object
    properties:
      publicKey:
        type: string
  Address:
    type: object
    properties:
      addressId:
        type: string
      city:
        type: string
      countryCode:
        type: string
      line1:
        type: string
      line2:
        type: string
      linkSequenceNo:
        type: string
      state:
        type: string
      stateCode:
        type: string
      zip:
        type: string
  PaymentInstrument:
    type: object
    properties:
      bankAccount:
        $ref: "#/definitions/BankAccount"
      creditCard:
        $ref: "#/definitions/CreditCard"
      walletId:
        type: string
        description: To indicate the WalletID.
      paymentMethodCode:
        type: string
        description: Describes the BIN payment type such as CC - Credit Card,HD - Hybrid Debit,HC - Hybrid Credit,DC - Debit Card,PD - Prepaid Debit or PC - Prepaid Credit.".
      paymentMethodStatus:
        type: string
        description: Status of payment method, such as VALID, INVALID.
      verificationIndicator:
        type: boolean
        description: To indicate if the Instrument is verified
      migrationIndicator:
        type: boolean
        description: To indicate if the Instrument is migrated
      defaultPaymentMethodIndicator:
        type: boolean
        description: To indicate if this payment method is default.
      autopayEnabledIndicator:
        type: boolean
        description: Indicates whether the payment method is enabled for automatic payment. true/false
  BankAccount:
    type: object
    properties:
      accountHolderFirstName:
        type: string
      accountHolderLastName:
        type: string
      accountHolderName:
        type: string
      accountHolderNames:
        type: array
        items:
          type: string
      accountNumber:
        type: string
      accountType:
        type: string
        enum:
          - Checking
          - Savings
          - Check
      bankAccountAlias:
        type: string
      billingAddress:
        $ref: "#/definitions/Address"
      nickName:
        type: string
      routingNumber:
        type: string
      status:
        type: string
        description: Provide bank account current status
        enum:
          - VALID
          - INVALID
      updateTime:
        type: string
        format: date-time
  CreditCard:
    type: object
    properties:
      billingAddress:
        $ref: "#/definitions/Address"
      cardAlias:
        type: string
      cardHolderFirstName:
        type: string
      cardHolderLastName:
        type: string
      cardHolderName:
        type: string
      cardNumber:
        type: string
      cvv:
        type: string
      expirationMonthYear:
        type: string
      expirationStatus:
        type: string
      last4CardNumber:
        type: string
      nickName:
        type: string
      status:
        type: string
      type:
        type: string
        enum:
          - VISA
          - MSCD
          - AMEX
          - DISC
      updateTime:
        type: string
        format: date-time
  BankDetails:
    type: object
    properties:
      bankName:
        type: string
  PaymentMethodValidationRequest:
    type: object
    properties:
      authorizationRequired:
        type: boolean
      bin:
        type: string
      paymentInstrumentList:
        type: array
        items:
          $ref: "#/definitions/PaymentInstrument"
  PaymentMethodValidationResponse:
    type: object
    properties:
      status:
        type: string
        enum:
          - SUCCESS
          - FAILURE
          - VALID
          - INVALID
          - ERROR
      errorDescription:
        type: string
      category:
        type: string
        enum:
          - NOTELIGIBLE
          - CREDIT
          - HYBRID
          - DEBIT
      cardType:
        type: string
        enum:
          - VISA
          - AMERICANEXPRESS
          - DISCOVER
          - MASTERCARD
          - PINONLY
      paymentMethodCode:
        type: string
        enum:
          - CC
          - HC
          - DC
          - HD
          - PC
          - PD
  PaymentMethodRequest:
    type: object
    properties:
      code:
        type: string
      termsAgreementIndicator:
        type: boolean
      termsAgreementTime:
        type: string
        format: date-time
      storePaymentMethodIndicator:
        type: boolean
        description: To identify if payment instrument is present in stored payment (Wallet) of customer.
      paymentInstrumentList:
        type: array
        items:
          $ref: "#/definitions/PaymentInstrument"
  PaymentMethodResponse:
    type: object
    properties:
      status:
        type: string
      reasonCode:
        type: string
      reasonDescription:
        type: string
  WalletDetails:
    type: object
    properties:
      eligibleForBankPayment:
        type: boolean
      eligibleForCardPayment:
        type: boolean
      eligibleForStoredBankPayment:
        type: boolean
      eligibleForStoredCardPayment:
        type: boolean
      isWalletCapacityReached:
        type: boolean
      paymentInstrumentList:
        type: array
        items:
          $ref: "#/definitions/PaymentInstrument"
  VerifyPaymentMethodRequest:
    type: object
    properties:
      paymentMethodCode:
        description: "Describes the BIN payment type such as CC - Credit Card,HD - Hybrid Debit,HC - Hybrid Credit,DC - Debit Card,PD - Prepaid Debit or PC - Prepaid Credit."
        type: string
      paymentInstrument:
        $ref: "#/definitions/VerifyPaymentInstrument"
  VerifyPaymentInstrument:
    type: object
    properties:
      bankPayment:
        $ref: "#/definitions/VerifyBankPayment"
      paymentCard:
        $ref: "#/definitions/VerifyPaymentCard"
  VerifyBankPayment:
    type: object
    description: details of payment method
    properties:
      routingNumber:
        description: Routing Number of the bank on whom the payment instrument is drawn.
        type: string
      bankAccountAlias:
        description: Bank account alias used by the customer.
        type: string
      accountNumber:
        description: Account number in the bank.
        type: string
  VerifyPaymentCard:
    type: object
    description: details of payment method
    properties:
      paymentCardAlias:
        description: A nickname assigned by the customer as an alternative way to identify a stored payment card.
        type: string
      cardNumber:
        description: Payment card number.
        type: string
