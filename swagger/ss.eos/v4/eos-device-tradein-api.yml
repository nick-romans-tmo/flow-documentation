swagger: '2.0'
info:
  title: EOS Trade-In Service
  contact:
    name: Web Shop Core Team
    email: Web_Shop_Core@T-Mobile.com  
  description: Micro services for device trade-in with upgrade and stand-alone trade-in
  version: 0.0.1
host: eos.eitss.tmobile.com
schemes:
  - https
securityDefinitions:
  OAuth2:
    type: oauth2
    description: 'When you invoke POST on token URL, you get a API OAuth2 token which needs to be set as Authorization header in subsequent calls.'
    flow: password
    tokenUrl: 'https://eos.corporate.t-mobile.com/shop/token/v2/create'
    scopes:
      read: read access
      write: write access
security:
  - OAuth2:
      - read
      - write
x-servers:
  - url: 'https://eos.corporate.t-mobile.com'
    description: Live Server
basePath: /shop/tradein/v4
produces:
  - application/json
consumes:
  - application/json
parameters:
  oAuth:
    name: Authorization
    type: string
    in: header
    description: Bearer Token for Oauth Validation.
    required: true
    x-example: eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlVGSlBSRVJCVkMweU1ERTUifQ.eyJpYXQiOjE1NjY3MTc2MzUsImV4cCI6MTU2Njc1MzYzNSwiaXNzIjoiaHR0cHM6Ly9icmFzcy5hY2NvdW50LnQtbW9iaWxlLmNvbSIsImF1ZCI6IkEtM2FtR2QxNC1pejAiLCJBVCI6IjAyLlVTUi5JRWhXZHN3MU9YZ1M3M3lRSCIsInN1YiI6IlUtM2E3OGY2NDgtYTQwYy00ZGM4LWI0NmUtYmRjOGViNTEzNmQxIiwiYWNyIjoibG9hMiIsImFtciI6WyJwYXNzd29yZCJdLCJ1c24iOiJkMmFiNGU0MjM5ZDFjMDY1IiwiZW50X3R5cGUiOiJkaXJlY3QiLCJlbnQiOnsiYWNjdCI6W3siciI6IkFPIiwiaWQiOiI5NTU3MzkxODIiLCJ0c3QiOiJJTSIsImxpbmVfY291bnQiOjIsImxpbmVzIjpbeyJwaG51bSI6IjI1MzI4NTA0MzEiLCJyIjoiRCJ9XX1dfX0.l0tXzGybAYgMK7z6bZGXXEmWfByss2gQmc4jQN3kgs_LsMmvckq_bEP1Y-hRrFt_cPDn4YSD0PEhVrnGDXvKsUnay_wjA4wWrXOFIJpJ-D2G1HtacPhuaRwRetoWY9J4PEJ4Scr5RaCK_5bOP3qloELXIu_MpDb__GpdNDj4hGwCl9yhS19gqG-SV5rTkTflsiFtDaVaVsnY5mR5k_vmOwJH4qrDQUPpnit6xMxeSPiQRFErd5LkmZrX9SL1Ci_a0a7K0nffm42vfG_yEbqwAc5lIik6-2TG81s86qR-vzoLp6uNp2C_kC4j9l17AymO7OsvGoSVgz6Ykbzk6Ukaaw
    pattern: ^.*$
  transactionId:
    name: transactionId
    type: string
    in: header
    description: unique GUID for the transaction.
    required: true
    x-example: f877b8de-3994-46ab-b254-f065f6311076
    pattern: ^.*$
  correlationId:
    in: header
    name: correlationId
    type: string
    description: >-
      This is used to tie multiple transactions across multiple calls (a unique
      key generated will be used across multiple api calls in the flow).
    required: true
    x-example: 082520191222
    pattern: ^.*$
  transactionBusinessKey:
    in: header
    name: transactionBusinessKey
    type: string
    description: 'This is place holder for a functional key, the value is MSISDN.'
    required: true
    x-example: BAN
    pattern: ^.*$
  transactionBusinessKeyType:
    in: header
    name: transactionBusinessKeyType
    type: string
    description: 'Indicates the functional key, "MSISDN" for trade-in APIs.'
    required: true
    x-example: 955739182
    pattern: ^.*$
  transactionType:
    in: header
    name: transactionType
    type: string
    description: 'Indicates the kind of transaction eg AAL, CIHU, inventoryfeed etc .'
    required: true
    x-example: AAL
    pattern: ^.*$
  applicationId:
    in: header
    name: applicationId
    type: string
    description: Application id like MyTMO or TMO.
    required: true
    x-example: MyTMO
    pattern: ^.*$
  channelId:
    in: header
    name: channelId
    type: string
    description: >-
      This indicates whether the original request came from Mobile / web /
      retail / mw  etc.
    required: true
    x-example: WEB
    pattern: ^.*$    
  clientId:
    in: header
    name: clientId
    type: string
    description: >-
      Unique identifier for the client (could be e-servicesUI , MWSAPConsumer
      etc ).
    required: true
    x-example: ESERVICES
    pattern: ^.*$     
  usn:
    name: usn
    in: header
    type: string
    description: Unique session id for user. It will be returned by IAM. (CIS requirement)
    required: false
    x-example: "2006248d4221b778"
    pattern: ^[a-zA-Z0-9]*$    
paths:
  /devices:
    get:
      x-api-pattern: QueryCollection     
      summary: Gets a list of devices that can be traded in.
      security:
        - OAuth2: []      
      description: >
        Gets an array of carriers, array of makes for each carrier and an array
        of model

        for each make.
      operationId: getTradeinEligibleDevices
      tags:
        - Devices
      parameters:
        - $ref: '#/parameters/oAuth'
        - $ref: '#/parameters/transactionId'
        - $ref: '#/parameters/correlationId'
        - $ref: '#/parameters/applicationId'
        - $ref: '#/parameters/channelId'
        - $ref: '#/parameters/clientId'
        - $ref: '#/parameters/transactionBusinessKey'
        - $ref: '#/parameters/transactionBusinessKeyType'
        - $ref: '#/parameters/transactionType'
        - $ref: '#/parameters/usn'
        - name: carrier
          in: query
          description: Name of the wireless carrier.
          required: false
          type: string
          x-example: ATT
        - name: make
          in: query
          description: Name of the make of the device.
          required: false
          type: string
          x-example: iPhone 4s 16GB White
      responses:
        '200':
          description: OK
          examples:
            type: object          
          schema:
            $ref: '#/definitions/Devices'
          headers:
            Date:
              description: time stamp of response generation
              x-example: Thu, 29 Aug 2019 17:53:11 GMT
              pattern: .*$
              type: string
            Content-Type:
              description: The MIME type of this content
              x-example: application/json;charset=UTF-8
              type: string
              pattern: .*$
            Cache-Control:
              description: specify directives for caching mechanisms
              x-example: 'no-cache'
              type: string
        '400':
          description: Bad request.
        '500':
          description: Internal server error.
          schema:
            $ref: '#/definitions/Error'
        default:
          description: A valid response from micro service to UI layer.
  /eligibility:
    post:
      x-api-pattern: ExecuteFunction       
      summary: Checks if a device is eligible for tradein
      security:
        - OAuth2: []
      description: This method is used to check if a device is eligible for tradein        
      tags:
        - eligibility
      operationId: checkTradeInEligibility
      parameters:
        - $ref: '#/parameters/oAuth'
        - $ref: '#/parameters/transactionId'
        - $ref: '#/parameters/correlationId'
        - $ref: '#/parameters/applicationId'
        - $ref: '#/parameters/channelId'
        - $ref: '#/parameters/clientId'
        - $ref: '#/parameters/transactionBusinessKey'
        - $ref: '#/parameters/transactionBusinessKeyType'
        - $ref: '#/parameters/transactionType'
        - $ref: '#/parameters/usn'
        - name: request
          in: body
          description: TradeIn request
          schema:
            $ref: '#/definitions/TradeIn'
      responses:
        '200':
          description: OK
          examples:
            type: object
          schema:
            $ref: '#/definitions/TradeIn'
          headers:
            Date:
              description: time stamp of response generation
              x-example: Thu, 29 Aug 2019 17:53:11 GMT
              pattern: .*$
              type: string
            Content-Type:
              description: The MIME type of this content
              x-example: application/json;charset=UTF-8
              type: string
              pattern: .*$
            Cache-Control:
              description: specify directives for caching mechanisms
              x-example: 'no-cache'
              type: string
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
  /questions:
    get:
      x-api-pattern: QueryCollection    
      summary: >-
        Gets questions regarding the condition of the device based on
        carrier/make/model.
      security:
        - OAuth2: []     
      operationId: getQuestions    
      description: >
        This service gets the questions and answer options that we need to check
        regarding the physical

        condition of the device.
      parameters:
        - $ref: '#/parameters/oAuth'
        - $ref: '#/parameters/transactionId'
        - $ref: '#/parameters/correlationId'
        - $ref: '#/parameters/applicationId'
        - $ref: '#/parameters/channelId'
        - $ref: '#/parameters/clientId'
        - $ref: '#/parameters/transactionBusinessKey'
        - $ref: '#/parameters/transactionBusinessKeyType'
        - $ref: '#/parameters/transactionType'
        - name: carrier-name
          in: query
          description: Name of the wireless carrier from where the device is bought.
          pattern: ^.*$
          x-example: ATT
          required: true
          type: string
        - name: make-name
          in: query
          description: Make of the device being traded-in.
          pattern: ^.*$
          x-example: Apple
          required: true
          type: string
        - name: model-name
          in: query
          description: Model of the device being traded-in.
          pattern: ^.*$
          x-example: iPhone 6s 16GB Gold
          required: true
          type: string
      tags:
        - Questions
      responses:
        '200':
          description: OK
          examples:
            type: object
          schema:
            $ref: '#/definitions/QuoteQuestions'
          headers:
            Date:
              description: time stamp of response generation
              x-example: Thu, 29 Aug 2019 17:53:11 GMT
              pattern: .*$
              type: string
            Content-Type:
              description: The MIME type of this content
              x-example: application/json;charset=UTF-8
              type: string
              pattern: .*$
            Cache-Control:
              description: specify directives for caching mechanisms
              x-example: 'no-cache'
              type: string
        '400':
          description: Bad request.
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/Error'
  /:
    post:
      x-api-pattern: CreateInCollection    
      summary: Creates a quote based on device information
      security:
        - OAuth2: []   
      description: This method returns a quote based on device information     
      operationId: createQuote
      tags:
        - Create Quote
      parameters:
        - $ref: '#/parameters/oAuth'
        - $ref: '#/parameters/transactionId'
        - $ref: '#/parameters/correlationId'
        - $ref: '#/parameters/applicationId'
        - $ref: '#/parameters/channelId'
        - $ref: '#/parameters/clientId'
        - $ref: '#/parameters/transactionBusinessKey'
        - $ref: '#/parameters/transactionBusinessKeyType'
        - $ref: '#/parameters/transactionType'
        - $ref: '#/parameters/usn'
        - name: request
          in: body
          description: TradeIn request
          required: true
          schema:
            $ref: '#/definitions/TradeIn'
      responses:
        '200':
          description: OK
          examples:
            type: object
          schema:
            $ref: '#/definitions/TradeIn'
          headers:
            Date:
              description: time stamp of response generation
              x-example: Thu, 29 Aug 2019 17:53:11 GMT
              pattern: .*$
              type: string
            Content-Type:
              description: The MIME type of this content
              x-example: application/json;charset=UTF-8
              type: string
              pattern: .*$
            Cache-Control:
              description: specify directives for caching mechanisms
              x-example: 'no-cache'
              type: string
        '400':
          description: Bad request.
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/Error'
    put:
      x-api-pattern: ReplaceResource    
      summary: Submit Quote
      security:
        - OAuth2: []
      description: Submit Quote      
      operationId: submitQuote
      tags:
        - Submit Quote
      parameters:
        - $ref: '#/parameters/oAuth'
        - $ref: '#/parameters/transactionId'
        - $ref: '#/parameters/correlationId'
        - $ref: '#/parameters/applicationId'
        - $ref: '#/parameters/channelId'
        - $ref: '#/parameters/clientId'
        - $ref: '#/parameters/transactionBusinessKey'
        - $ref: '#/parameters/transactionBusinessKeyType'
        - $ref: '#/parameters/transactionType'
        - $ref: '#/parameters/usn'
        - name: request
          in: body
          description: TradeIn request
          required: true
          schema:
            $ref: '#/definitions/TradeIn'
      responses:
        '200':
          description: OK
          examples:
            type: object
          schema:
            $ref: '#/definitions/TradeIn'
          headers:
            Date:
              description: time stamp of response generation
              x-example: Thu, 29 Aug 2019 17:53:11 GMT
              pattern: .*$
              type: string
            Content-Type:
              description: The MIME type of this content
              x-example: application/json;charset=UTF-8
              type: string
              pattern: .*$
            Cache-Control:
              description: specify directives for caching mechanisms
              x-example: 'no-cache'
              type: string
        '400':
          description: Bad request.
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/Error'
  '/{quote-id}':
    get:
      x-api-pattern: QueryResource
      summary: Get a trade in request
      security:
        - OAuth2: [] 
      description: Get a trade in request     
      operationId: getTradeIn
      tags:
        - Get TradeIn
      parameters:
        - $ref: '#/parameters/oAuth'
        - $ref: '#/parameters/transactionId'
        - $ref: '#/parameters/correlationId'
        - $ref: '#/parameters/applicationId'
        - $ref: '#/parameters/channelId'
        - $ref: '#/parameters/clientId'
        - $ref: '#/parameters/transactionBusinessKey'
        - $ref: '#/parameters/transactionBusinessKeyType'
        - $ref: '#/parameters/transactionType'
        - $ref: '#/parameters/usn'   
        - name: quote-id
          in: path
          type: string
          description: Quote unique identifier
          pattern: ^.*$
          required: true
          x-example: 2345e557 
      responses:
        '200':
          description: OK
          examples:
            type: object
          schema:
            $ref: '#/definitions/TradeIn'
          headers:
            Date:
              description: time stamp of response generation
              x-example: Thu, 29 Aug 2019 17:53:11 GMT
              pattern: .*$
              type: string
            Content-Type:
              description: The MIME type of this content
              x-example: application/json;charset=UTF-8
              type: string
              pattern: .*$
            Cache-Control:
              description: specify directives for caching mechanisms
              x-example: 'no-cache'
              type: string
        '400':
          description: Bad request.
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/Error'
  '/{rma-number}/shipping-label':
    get:
      x-api-pattern: QueryResource    
      summary: Gets shipping label based on RMA number
      security:
        - OAuth2: []
      description: get shipping label      
      tags:
        - Shipping Label
      operationId: getShippingLabel
      parameters:
        - $ref: '#/parameters/oAuth'
        - $ref: '#/parameters/transactionId'
        - $ref: '#/parameters/correlationId'
        - $ref: '#/parameters/applicationId'
        - $ref: '#/parameters/channelId'
        - $ref: '#/parameters/clientId'
        - $ref: '#/parameters/transactionBusinessKey'
        - $ref: '#/parameters/transactionBusinessKeyType'
        - $ref: '#/parameters/transactionType'
        - name: rma-number
          in: path
          description: Return Merchandise Authorization number
          pattern: ^.*$
          x-example: 1230001326
          required: true
          type: string
        - name: ban-number
          in: query
          description: account number
          pattern: ^.*$
          x-example: 960001326
          required: true
          type: string
        - name: language
          type: string
          description: language used
          x-example: en
          enum:
            - en
            - es
          in: query
          required: true
      responses:
        '200':
          description: OK
          examples:
            type: object
          schema:
            $ref: '#/definitions/ShippingLabel'
          headers:
            Date:
              description: time stamp of response generation
              x-example: Thu, 29 Aug 2019 17:53:11 GMT
              pattern: .*$
              type: string
            Content-Type:
              description: The MIME type of this content
              x-example: application/json;charset=UTF-8
              type: string
              pattern: .*$
            Cache-Control:
              description: specify directives for caching mechanisms
              x-example: 'no-cache'
              type: string
        '400':
          description: Bad request.
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
  /codes:
    get:
      x-api-pattern: QueryCollection    
      summary: Gets lookup data for tradein flow
      security:
        - OAuth2: []
      description: Get codes      
      operationId: getCodes
      tags:
        - Lookups
      parameters:
        - $ref: '#/parameters/oAuth'
        - $ref: '#/parameters/transactionId'
        - $ref: '#/parameters/correlationId'
        - $ref: '#/parameters/applicationId'
        - $ref: '#/parameters/channelId'
        - $ref: '#/parameters/clientId'
        - $ref: '#/parameters/transactionBusinessKey'
        - $ref: '#/parameters/transactionBusinessKeyType'
        - $ref: '#/parameters/transactionType'
        - $ref: '#/parameters/usn'
        - name: code-type
          type: string
          description: Type of the code
          x-example: codeType
          in: query
      responses:
        '200':
          description: OK
          examples:
            type: object
          schema:
            $ref: '#/definitions/Codes'
          headers:
            Date:
              description: time stamp of response generation
              x-example: Thu, 29 Aug 2019 17:53:11 GMT
              pattern: .*$
              type: string
            Content-Type:
              description: The MIME type of this content
              x-example: application/json;charset=UTF-8
              type: string
              pattern: .*$
            Cache-Control:
              description: specify directives for caching mechanisms
              x-example: 'no-cache'
              type: string
  /account-devices:
    post:
      x-api-pattern: ExecuteFunction    
      summary: list of devices on the account that are eligible for trade-in
      security:
        - OAuth2: []  
      operationId: accountDevicesPost      
      description: >
        This service gets the list of devices associated with each line on the
        account that are eligible for a trade-in during a qualifying
        transaction.
      parameters:
        - $ref: '#/parameters/oAuth'
        - $ref: '#/parameters/transactionId'
        - $ref: '#/parameters/correlationId'
        - $ref: '#/parameters/applicationId'
        - $ref: '#/parameters/channelId'
        - $ref: '#/parameters/clientId'
        - $ref: '#/parameters/transactionBusinessKey'
        - $ref: '#/parameters/transactionBusinessKeyType'
        - $ref: '#/parameters/transactionType'
        - $ref: '#/parameters/usn'
        - name: request
          in: body
          description: Account Devices Request
          required: true
          schema:
            $ref: '#/definitions/AccountDevicesRequest'
      tags:
        - AccountDevices
      responses:
        '200':
          description: OK
          examples:
            type: object
          schema:
            $ref: '#/definitions/AccountDevices'
          headers:
            Date:
              description: time stamp of response generation
              x-example: Thu, 29 Aug 2019 17:53:11 GMT
              pattern: .*$
              type: string
            Content-Type:
              description: The MIME type of this content
              x-example: application/json;charset=UTF-8
              type: string
              pattern: .*$
            Cache-Control:
              description: specify directives for caching mechanisms
              x-example: 'no-cache'
              type: string
        '400':
          description: Bad request.
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
definitions:
  Devices:
    type: object
    description: Definition of device
    properties:
      devices:
        type: array
        description: Definition of device
        items:
          $ref: '#/definitions/Device'
  AccountDevices:
    type: object
    description: Definition of trade in account devices
    properties:
      accountDevices:
        type: array
        description: Definition of trade in account devices
        items:
          $ref: '#/definitions/TradeIn'
  QuoteQuestions:
    type: object
    description: Description of Questions
    properties:
      questions:
        type: array
        description: Description of Questions
        items:
          $ref: '#/definitions/Question'
  Codes:
    type: object
    description: Definition of Code
    properties:
      codes:
        type: array
        description: Definition of Code
        items:
          $ref: '#/definitions/Code'
  Code:
    type: object
    description: Definition of Code
    properties:
      code:
        type: string
        description: reason code
      description:
        type: string
        description: reason description
      languageCode:
        type: string
        description: language code
        enum:
          - en
          - es
      type:
        type: string
        description: type
        enum:
          - Carriers
          - RejectReasons
      carriers:
        type: array
        description: carriers
        items:
          $ref: '#/definitions/Carrier'
  TradeIn:
    type: object
    description: Definition of TradeIn
    properties:
      ban:
        type: string
        description: Ban of the subscriber submitting the trade-in.
      lineMsisdn:
        type: string
        description: MSISDN of the line for which the trade-in is being processed.
      device:
        $ref: '#/definitions/Device'
      finance:
        $ref: '#/definitions/Finance'
      emailAddress:
        type: string
        description: Email address of the customer.
      orderId:
        type: string
        description: Order Id against which trade-in is submitted.
      quoteId:
        type: string
        description: quote identifier
      quotePrice:
        type: string
        description: Non promo quote price from Assurant.
      questions:
        type: array
        description: questions
        items:
          $ref: '#/definitions/Question'
      eventType:
        type: string
        enum:
          - HANDSET_UPGRADE
          - ADD_A_LINE
          - ACTIVATION
        description: >-
          Event type for the order. One of the following value -
          HANDSET_UPGRADE, ADD_A_LINE, ACTIVATION.
      languageCode:
        description: Language for shipping label.
        type: string
        enum:
          - en
          - es
      offerId:
        type: string
        description: Tradein promotion offerID from DCP.
      offerPrice:
        type: number
        format: double
        description: Dollar value of the quote.
      offerStatus:
        type: string
        description: Status of the offer.
      acceptanceStatus:
        description: >-
          Accept or Decline based on user decision. Values can be 'REJECTED',
          'ACCEPTED DEFERRED'.
        type: string
      declineReason:
        description: >-
          Reason for declining offer - applicable only when customer is
          declining an offer.
        type: string
      statusCode:
        type: string
        description: error code from backend
      statusMessage:
        type: string
        description: A description of the status code or error message from backend.
      isEligible:
        type: boolean
        description: True if the IMEI is eligible for trade-in.
      rmaNumber:
        type: string
        description: RMA number for the submitted trade-in.
      expiryDate:
        type: string
        format: date-time
        description: The date until the user can mail in the traded-in device.
      shippingLabelUrl:
        type: string
        description: Url of the shipping label.
      imei:
        type: string
        description: device imei
      displayName:
        description: Device name with make and model.
        type: string
      sku:
        description: SKU number.
        type: string
      loan:
        $ref: '#/definitions/Loan'
  Loan:
    type: object
    description: Definition of Loan
    properties:
      eipBalance:
        description: The remaining EIP balance on the line.
        type: number
        format: double
      loanId:
        description: Installment plan/loan id.
        type: string
      equipmentId:
        description: Installment equipment id.
        type: string
  Question:
    type: object
    description: Definition of Question
    properties:
      name:
        type: string
        description: question text that user will see as an option to select
      description:
        type: string
        description: >-
          The question description that needs to be sent back to the backend for
          estimation.
      languageCode:
        type: string
        description: language code for question
        enum:
          - en
          - es
      choices:
        type: array
        description: definition of Choice
        items:
          $ref: '#/definitions/Choice'
  Choice:
    type: object
    description: Definition of Choice
    properties:
      code:
        type: string
        description: Choice code/name
      response:
        type: string
        description: Indicates user selection - language based
  Finance:
    type: object
    description: Finance details
    properties:
      financeType:
        type: string
        description: Type of financing 
      totalInstallment:
        type: number
        description: Total installment to be paid over whole term
      installmentBalance:
        type: number
        description: Remaining installment balance to be paid over remaining term.
  Device:
    type: object
    description: Definition of Device
    properties:
      msisdn:
        type: string
        description: MSISDN initiating the tradein
      imei:
        description: Name of the wireless carrier from where the device is bought.
        type: string
      eligibility:
        $ref: '#/definitions/Eligibility'
      carrier:
        type: array
        description: Name and make of carrier
        items:
          $ref: '#/definitions/Carrier'
  Carrier:
    type: object
    description: Definition of Carrier
    properties:
      carrierName:
        type: string
        description: Name of the Mobile Carrier.
      make:
        type: array
        description: Name and model of the make
        items:
          $ref: '#/definitions/Make'
  Make:
    type: object
    description: Definition of Make
    properties:
      makeName:
        type: string
        description: Name of the Device Make.
      model:
        type: array
        description: Model of the Device Make.
        items:
          $ref: '#/definitions/Model'
  Model:
    type: object
    description: Definition of Model
    properties:
      modelName:
        type: string
        description: Name of the Device Model.
      quotePrice:
        type: number
        format: double
        description: Quote price.
  ShippingLabel:
    type: object
    description: Definition of ShippingLabel
    properties:
      statusCode:
        type: string
        description: '0 - Success, 999 - Backend System Error.'
      statusMessage:
        type: string
        description: A description of the status code or error message from backend.
      shippingLabelUrl:
        type: string
        description: URL for the shipping label.
  AccountDevicesRequest:
    type: object
    description: Definition of AccountDevicesRequest
    properties:
      accountNumber:
        type: string
        description: Customer Account Number
      msisdns:
        type: array
        description: array of msisdns for which devices information needs to be fetched
        items:
          type: string
          description: msisdn
  Eligibility:
    type: object
    description: Definition of Eligibility
    properties:
      isEligible:
        type: boolean
        description: True if the IMEI is eligible for trade-in.
      ineligibilityReasonCode:
        type: string
        description: error code from code Trade-in domain in case of ineligibility
      ineligibilityMessage:
        type: string
        description: >-
          A description of the  ineligibility error message from Trade-in domain
          in case of ineligibility.
  Error:
    type: object
    description: Definition of Error
    properties:
      code:
        type: integer
        format: int32
        description: Error code.
      message:
        type: string
        description: Error message.