{
  "swagger": "2.0",
  "info": {
    "description": "The first version of DEEP.io Publish Event API for writing a memo - Payload Author: Santosh Pillai",
    "version": "1.0",
    "title": "DEEP.io Publish Event POST API - memo creation event - CHUB call"
  },
  "host": "localhost:8080",
  "basePath": "/deep/v1/events",
  "paths": {
    "/{eventType}": {
      "post": {
        "tags": [
          "messaging-controller"
        ],
        "summary": "This Generic API shall be used by producers to publish events into DEEP.io.",
        "operationId": "sendMessageUsingPOST",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "*/*"
        ],
        "parameters": [
          {
            "name": "eventType",
            "in": "path",
        "description": "Unique name of the event. Expected Value - <eventname>",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "event",
            "description": "Event Complex Element",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Event"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Message Published Successfully.",
            "schema": {
              "type": "object",
              "properties": {}
            }
          },
          "201": {
            "description": "Message Broker has not accepted the message but has been Saved and need resubmission from UI. Please Contact Admin to resubmit"
          },
          "400": {
            "description": "Not a valid Request"
          },
          "429": {
            "description": "Too Many Requests"
          },
          "500": {
            "description": "Error while sending message to Queue OR Error happened inside producer"
          },
          "501": {
            "description": "Rule is Not Configured For EventType"
          },
          "502": {
            "description": "Contract is Not Defined"
          },
          "503": {
            "description": "Contract is defined but payload posted is not valid as per uploaded contract"
          }
        }
      }
    }
  },
  "definitions": {
    "AuditInfo": {
      "type": "object",
      "properties": {
        "accountNumber": {
          "type": "string",
          "example": "F123111",
          "description": "The financial account number."
        },
        "batchId": {
          "type": "string",
          "example": "batch0",
          "description": "Identifer of batch job."
        },
        "customerId": {
          "type": "string",
          "example": "Customer1",
          "description": "Uniquely identifies the Customer."
        },
        "iamUniqueId": {
          "type": "string",
          "example": "deepuser",
          "description": "Unique identifier for Identity and Access Management."
        },
        "lineId": {
          "type": "string",
          "example": "1234567890",
          "description": "Uniquely identifies a  line of service."
        },
        "orderId": {
          "type": "string",
          "example": "order1",
          "description": "Identifier of order."
        },
        "phoneNumber": {
          "type": "string",
          "example": "1234567890",
          "description": "The phone number (MSISDN) associated with the line of service."
        },
        "universalLineId": {
          "type": "string",
          "example": "1234567890",
          "description": "ULID - Universal identifier of the LineofService, as defined in Customer Hub. "
        }
      },
      "description": "Audit information used to search specific event. All elements included in this complex element will be searchable in DEEP.io.",
      "example": "null"
    },
    "HeaderReference": {
      "type": "object",
      "properties": {
        "activityId": {
          "type": "string",
          "example": "null",
          "description": "Unique id alphanumeric value generated and sent by the partner to identify every service request uniquely."
        },
        "applicationId": {
          "type": "string",
          "example": "null",
          "description": "Identifies the application, system or platform name that is being used to initiate the transaction  related to this request."
        },
        "applicationUserId": {
          "type": "string",
          "example": "null",
          "description": "NT id of the rep who is managing the transaction from CARE/RETAIL channels. Required for assisted channels, not required for unassisted channels."
        },
        "authCustomerId": {
          "type": "string",
          "example": "null",
          "description": "Customer Id of Tmo customer that is being served/using the application. (Required for non anonymous flows)"
        },
        "authFinancialAccountId": {
          "type": "string",
          "example": "null",
          "description": "Financial Account Id that the customer that is being served/using the application is working on.  (Required for non anonymous flows)"
        },
        "authLineOfServiceId": {
          "type": "string",
          "example": "null",
          "description": "Line of service Id , within the Financial Account of the customer that is being served/using the application is working on. (Required for non anonymous flows)"
        },
        "channelId": {
          "type": "string",
          "example": "null",
          "description": "Identifies the business unit or sales channel."
        },
        "dealerCode": {
          "type": "string",
          "example": "null",
          "description": "Uniquely identifies the dealer/rep user."
        },
        "interactionId": {
          "type": "string",
          "example": "null",
          "description": "Alphanumeric value represent a common transaction id across all calls made from UI, while completing all business activity needs of a particular customer."
        },
        "masterDealerCode": {
          "type": "string",
          "example": "null",
          "description": "Code that uniquely identifies the master dealer for a large retail partner, such as Apple or Costco. Will not be applicable for TMo channels."
        },
        "segmentationId": {
          "type": "string",
          "example": "null",
          "description": "Identifier of customer\ufffd\ufffd\ufffds primary data center."
        },
        "senderId": {
          "type": "string",
          "example": "null",
          "description": "Uniquely identifies an Operation consumer/Partner."
        },
        "sessionId": {
          "type": "string",
          "example": "null",
          "description": "A value populated by the sender used to track the transactions that occur during a session, a long-lasting interaction, managed by the sender. GUID generated by partner. Within one session (sessionid) for Rep/system can serve  multiple customers(interactionid) and in a given customer interation \ufffd\ufffd\ufffd multiple API calls (activityid) will be made to serve the customer. 1 sessionid -> many interactionid 1 interactionid -> many activityid"
        },
        "storeId": {
          "type": "string",
          "example": "null",
          "description": "Unique identifier for the retail store location."
        },
        "terminalId": {
          "type": "string",
          "example": "null",
          "description": "Unique identifier for the retail store terminal."
        },
        "tillId": {
          "type": "string",
          "example": "null",
          "description": "Unique identifier for the retail store terminal till number."
        },
        "timestamp": {
          "type": "string",
          "format": "date-time",
          "example": "null",
          "description": "A timestamp provided by sender to track their workflow."
        },
        "workflowId": {
          "type": "string",
          "example": "null",
          "description": "Name of business purpose/flow, for which the API is being invoked."
        }
      },
      "description": "standard HTTP header elements to be able to deliver them to consumer without interruption",
      "example": "null"
    },
    "Specifications": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string",
          "example": "null"
        },
        "value": {
          "type": "string",
          "example": "null"
        }
      },
      "description": "Name value pair."
    },
    "Event": {
      "type": "object",
      "required": [
        "eventProducerId",
        "eventTime",
        "eventType",
        "payload"
      ],
      "properties": {
        "auditInfo": {
          "$ref": "#/definitions/AuditInfo"
        },
        "eventId": {
          "type": "string",
          "example": "Event-123",
          "description": "Unique identifier for the event generated by the event producer. This element will be searchable in DEEP.io UI"
        },
        "eventProducerId": {
          "type": "string",
          "example": "Producer1",
          "description": "Unique identifier for the producer of the event."
        },
        "eventTime": {
          "type": "string",
          "format": "date-time",
          "example": "2017-03-27T16:20:11.108Z",
          "description": "The date and time that the event occurred."
        },
        "eventType": {
          "type": "string",
          "example": "<eventname>",
          "description": "Event Name that you want to publish"
        },
        "eventVersion": {
          "type": "string",
          "example": "1.0",
          "description": "version of the event , will be used to distinguish different versions by a consumer"
        },
        "headerReference": {
          "$ref": "#/definitions/HeaderReference"
        },
        "payload": {
          "$ref": "#/definitions/Payload"
        },
        "specifications": {
          "type": "array",
          "example": "null",
          "description": "added for future reference so can ignore.",
          "items": {
            "$ref": "#/definitions/Specifications"
          }
        }
      }
    },
    "Payload": {
      "type": "object",
      "description": "Event specific information that can be modeled using APIv2 or inline elements",
      "properties": {
        "memoEventToChub": {
          "$ref": "#/definitions/InteractionAgreement"
        }
      }
    },
    "CustomerRef": {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int32",
          "description": "CustomerAccountId or BAN of Customer",
          "example": "123456789"
        }
      }
    }, 
    "SubscriberRef": {
      "type": "object",
      "properties": {
        "id": {
          "type": "string",
          "example": "4041231234",
          "description": "Phone/Subscriber Number "
        },
        "productType": {
          "$ref": "#/definitions/ProductType"
        }
      }
    }, 
    "ProductType": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string",
          "example": "A",
          "description": "A or G or B A=all (customerlevel), G=GSM Line, B=Mobile Internet Line"
        }
      }
    }, 
    "InteractionType": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string",
          "description": "MEMO_TYPE to be generated, e.g 'WARA' - for 'RVC store warranty exchange'",
          "example": "WARA"
        }     }
    },    
    "InteractionAgreement": {
      "type": "object",
      "properties": {
        "amount": {
          "type": "number",
          "format": "double"
        },
        "applicationId": {
          "type": "string"
        },
        "channelId": {
          "type": "string",
          "description": "maps to the numeric Channel Id: e.g 3 - MYTMOBILE, 4 - CSM, 5 - SIVR, 7 - STREAMLINE, 22 - IVR, 51 - POS, 52 - MYTMOAPP, 61 - QV RETAIL, 62 - QV CARE, 63 - QVXP RETAIL, 64 - QVXP CARE, 60 - WIDGET",
          "example": "60"          
        },
        "createdBy": {
          "type": "integer",
          "format": "int32",
          "description": "maps to operator Id field/creator/userId who created memo record",
          "example": "481991"
        },
        "customer": {
          "$ref": "#/definitions/CustomerRef"
        },
        "date": {
            "type": "string",
            "example": "2017-02-23T10:00:27.000+00:00",
            "description": "yyyy-mm-ddThh:mi:ss.000+offsetfromutc:00, offset of 00.00 means UTC time. UTC time is desirable"
        },
        "manualText": {
          "type": "string",
          "description": "Additional Text/information that Rep wants to be populated in the memo. Required if ManualIndicator = 'Y' for the the Samson Memo type"          
        },
        "subscriber": {
          "$ref": "#/definitions/SubscriberRef"
        },
        "systemTextParameters": {
          "type": "array",
          "items": {
            "type": "string"
          },
          "example": ["Aug 1, 2018", "20", "Aug 15, 2018"],
          "description": "comma separated array of values that need to be substituted/filled in the memo string when generating System Memos e.g  'As of %s you have %s Whenever minutes remaining in your monthly plan. Your new plan minutes start %s. (Free T-Mobile Msg.)' will be stored as 'As of Aug 1, 2018 you have 20 Whenever minutes remaining in your monthly plan. Your new plan minutes start Aug 15, 2018. (Free T-Mobile Msg.)', using the above parameters"
        },
        "type": {
          "$ref": "#/definitions/InteractionType"
        }
      }
    }
  }
}