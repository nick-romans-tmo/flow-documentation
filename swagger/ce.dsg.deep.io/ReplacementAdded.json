{
  "swagger" : "2.0",
  "info" : {
    "description" : "The Generic version of DEEP.io Publish Event API",
    "version" : "1.0",
    "title" : "DEEP.io Publish Event POST API - Generic Version"
  },
  "host" : "localhost:8080",
  "basePath" : "/deep/v1/events",
  "paths" : {
    "/{eventType}" : {
      "post" : {
        "summary" : "This Generic API shall be used by producers to publish events into DEEP.io.",
        "operationId" : "publishEvent",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "eventType",
          "in" : "path",
          "description" : "Unique name of the event",
          "required" : true,
          "type" : "string"
        }, {
          "in" : "body",
          "name" : "event",
          "description" : "Event Complex Element",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/Event"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "Message Published Successfully."
          },
          "400" : {
            "description" : "Not a valid Request."
          },
          "500" : {
            "description" : "Error while sending message to Queue OR Error happened inside producer"
          },
          "501" : {
            "description" : "Rule is Not Configured For EventType"
          },
          "502" : {
            "description" : "Contract is Not Defined"
          },
          "503" : {
            "description" : "Contract is defined but payload posted is not valid as per uploaded contract"
          }
        }
      }
    }
  },
  "definitions" : {
    "Event" : {
      "type" : "object",
      "required" : [ "eventId", "eventProducerId", "eventTime", "eventType", "payload" ],
      "properties" : {
        "eventId" : {
          "type" : "string",
          "description" : "Unique identifier for the event generated by the event producer."
        },
        "eventType" : {
          "type" : "string",
          "description" : "Name of the event."
        },
        "eventTime" : {
          "type" : "string",
          "format" : "date-time",
          "description" : "The date and time that the event occurred."
        },
        "eventProducerId" : {
          "type" : "string",
          "description" : "Unique identifier for the producer of the event."
        },
        "eventVersion" : {
          "type" : "string",
          "description" : "Version of the event."
        },
        "specifications" : {
          "type" : "array",
          "description" : "Name value pair.",
          "items" : {
            "$ref" : "#/definitions/Specifications"
          }
        },
        "auditInfo" : {
          "$ref" : "#/definitions/AuditInfo"
        },
        "headerReference" : {
          "$ref" : "#/definitions/HeaderReference"
        },
        "payload" : {
          "$ref" : "#/definitions/Payload"
        }
      }
    },
    "AuditInfo" : {
      "type" : "object",
      "properties" : {
        "customerId" : {
          "type" : "string",
          "description" : "Uniquely identifies the Customer."
        },
        "accountNumber" : {
          "type" : "string",
          "description" : "The financial account number."
        },
        "universalLineId" : {
          "type" : "string",
          "description" : "ULID - Universal identifier of the LineofService, as defined in Customer Hub. "
        },
        "lineId" : {
          "type" : "string",
          "description" : "Uniquely identifies a  line of service."
        },
        "phoneNumber" : {
          "type" : "string",
          "description" : "The phone number (MSISDN) associated with the line of service."
        },
        "iamUniqueId" : {
          "type" : "string",
          "description" : "Unique identifier for Identity and Access Management."
        },
        "batchId" : {
          "type" : "string",
          "description" : "Identifer of batch job."
        },
        "orderId" : {
          "type" : "string",
          "description" : "Identifier of order."
        }
      },
      "description" : "Audit information."
    },
    "Specifications" : {
      "type" : "object",
      "properties" : {
        "name" : {
          "type" : "string"
        },
        "value" : {
          "type" : "string"
        }
      },
      "description" : "Name value pair."
    },
    "HeaderReference" : {
      "type" : "object",
      "properties" : {
        "activityId" : {
          "type" : "string",
          "description" : "Unique id alphanumeric value generated and sent by the partner to identify every service request uniquely."
        },
        "applicationId" : {
          "type" : "string",
          "description" : "Identifies the application, system or platform name that is being used to initiate the transaction  related to this request."
        },
        "applicationUserId" : {
          "type" : "string",
          "description" : "NT id of the rep who is managing the transaction from CARE/RETAIL channels. Required for assisted channels, not required for unassisted channels."
        },
        "authCustomerId" : {
          "type" : "string",
          "description" : "Customer Id of Tmo customer that is being served/using the application. (Required for non anonymous flows)"
        },
        "authFinancialAccountId" : {
          "type" : "string",
          "description" : "Financial Account Id that the customer that is being served/using the application is working on.  (Required for non anonymous flows)"
        },
        "authLineOfServiceId" : {
          "type" : "string",
          "description" : "Line of service Id , within the Financial Account of the customer that is being served/using the application is working on. (Required for non anonymous flows)"
        },
        "channelId" : {
          "type" : "string",
          "description" : "Identifies the business unit or sales channel."
        },
        "dealerCode" : {
          "type" : "string",
          "description" : "Uniquely identifies the dealer/rep user."
        },
        "interactionId" : {
          "type" : "string",
          "description" : "Alphanumeric value represent a common transaction id across all calls made from UI, while completing all business activity needs of a particular customer."
        },
        "masterDealerCode" : {
          "type" : "string",
          "description" : "Code that uniquely identifies the master dealer for a large retail partner, such as Apple or Costco. Will not be applicable for TMo channels."
        },
        "segmentationId" : {
          "type" : "string",
          "description" : "Identifier of customer’s primary data center."
        },
        "senderId" : {
          "type" : "string",
          "description" : "Uniquely identifies an Operation consumer/Partner."
        },
        "sessionId" : {
          "type" : "string",
          "description" : "A value populated by the sender used to track the transactions that occur during a session, a long-lasting interaction, managed by the sender. GUID generated by partner. Within one session (sessionid) for Rep/system can serve  multiple customers(interactionid) and in a given customer interation – multiple API calls (activityid) will be made to serve the customer. 1 sessionid -> many interactionid 1 interactionid -> many activityid"
        },
        "storeId" : {
          "type" : "string",
          "description" : "Unique identifier for the retail store location."
        },
        "terminalId" : {
          "type" : "string",
          "description" : "Unique identifier for the retail store terminal."
        },
        "tillId" : {
          "type" : "string",
          "description" : "Unique identifier for the retail store terminal till number."
        },
        "workflowId" : {
          "type" : "string",
          "description" : "Name of business purpose/flow, for which the API is being invoked."
        },
        "timestamp" : {
          "type" : "string",
          "format" : "date-time",
          "description" : "A timestamp provided by sender to track their workflow."
        }
      },
      "description" : "Header Parameters."
    },
    "Payload" : {
      "type" : "object",
      "properties" : {
        "replacementKey" : {
          "type" : "string",
          "description" : "Unique Identifier for Product Type"
        },
        "defectiveProductId" : {
          "type" : "string",
          "description" : "name of the Product Type"
        },
        "replacementId" : {
          "type" : "string",
          "description" : "category of the Product Type"
        },
        "otvId" : {
          "type" : "string",
          "description" : "category of the Product Type"
        },
        "warrantyId" : {
          "type" : "string",
          "description" : "category of the Product Type"
        },
        "replacementPriorityNumber" : {
          "type" : "string",
          "description" : "category of the Product Type"
        },
        "createdOn" : {
          "type" : "string",
          "description" : "created on date"
        },
        "createdBy" : {
          "type" : "string",
          "description" : "created by detail"
        },
        "lastUpdatedDate" : {
          "type" : "string",
          "description" : "last updated date by detail"
        },
        "lastUpdatedBy" : {
          "type" : "string",
          "description" : "last updated by detail"
        }
      },
      "description" : "Unique Identifier for Product Type"
    }
  }
}