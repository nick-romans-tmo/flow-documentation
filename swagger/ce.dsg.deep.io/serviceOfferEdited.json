{
  "swagger": "2.0",
  "info": {
    "description": "DEEP.io Publish Event API for ServiceOffer resource.",
    "version": "1.0",
    "title": "Service Offer"
  },
  "host": "localhost:8080",
  "basePath": "/deep/v1/events",
  "paths": {
    "/{eventType}": {
      "post": {
        "tags": [
          "messaging-controller"
        ],
        "summary": "This API shall be used by producers to publish ServiceOffer events (create, delete, edit) into DEEP.io.",
        "operationId": "publishEvent",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "*/*"
        ],
        "parameters": [
          {
            "name": "eventType",
            "in": "path",
            "description": "Unique name of the event",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "event",
            "description": "Event Complex Element",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Event"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Message Published Successfully.",
            "schema": {
              "type": "object",
              "properties": {
              }
            }
          },
          "201": {
            "description": "Message Broker has not accepted the message but has been Saved and need resubmission from UI. Please Contact Admin to resubmit"
          },
          "400": {
            "description": "Not a valid Request."
          },
          "429": {
            "description": "Too Many Requests."
          },
          "500": {
            "description": "Error while sending message to Queue OR Error happened inside producer"
          },
          "501": {
            "description": "Rule is Not Configured For EventType"
          },
          "502": {
            "description": "Contract is Not Defined"
          },
          "503": {
            "description": "Contract is defined but payload posted is not valid as per uploaded contract"
          }
        }
      }
    }
  },
  "definitions": {
    "Specifications": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string",
          "example": "null"
        },
        "value": {
          "type": "string",
          "example": "null"
        }
      },
      "description": "Name value pair."
    },
    "AuditInfo": {
      "type": "object",
      "properties": {
        "accountNumber": {
          "type": "string",
          "example": "F123111",
          "description": "The financial account number."
        },
        "batchId": {
          "type": "string",
          "example": "batch0",
          "description": "Identifer of batch job."
        },
        "customerId": {
          "type": "string",
          "example": "Customer1",
          "description": "Uniquely identifies the Customer."
        },
        "iamUniqueId": {
          "type": "string",
          "example": "deepuser",
          "description": "Unique identifier for Identity and Access Management."
        },
        "lineId": {
          "type": "string",
          "example": "1234567890",
          "description": "Uniquely identifies a  line of service."
        },
        "orderId": {
          "type": "string",
          "example": "order1",
          "description": "Identifier of order."
        },
        "phoneNumber": {
          "type": "string",
          "example": "1234567890",
          "description": "The phone number (MSISDN) associated with the line of service."
        },
        "universalLineId": {
          "type": "string",
          "example": "1234567890",
          "description": "ULID - Universal identifier of the LineofService, as defined in Customer Hub. "
        }
      },
      "description": "Audit information used to search specific event. All elements included in this complex element will be searchable in DEEP.io.",
      "example": "null"
    },
    "HeaderReference": {
      "type": "object",
      "properties": {
        "activityId": {
          "type": "string",
          "example": "null",
          "description": "Unique id alphanumeric value generated and sent by the partner to identify every service request uniquely."
        },
        "applicationId": {
          "type": "string",
          "example": "null",
          "description": "Identifies the application, system or platform name that is being used to initiate the transaction  related to this request."
        },
        "applicationUserId": {
          "type": "string",
          "example": "null",
          "description": "NT id of the rep who is managing the transaction from CARE/RETAIL channels. Required for assisted channels, not required for unassisted channels."
        },
        "authCustomerId": {
          "type": "string",
          "example": "null",
          "description": "Customer Id of Tmo customer that is being served/using the application. (Required for non anonymous flows)"
        },
        "authFinancialAccountId": {
          "type": "string",
          "example": "null",
          "description": "Financial Account Id that the customer that is being served/using the application is working on.  (Required for non anonymous flows)"
        },
        "authLineOfServiceId": {
          "type": "string",
          "example": "null",
          "description": "Line of service Id , within the Financial Account of the customer that is being served/using the application is working on. (Required for non anonymous flows)"
        },
        "channelId": {
          "type": "string",
          "example": "null",
          "description": "Identifies the business unit or sales channel."
        },
        "dealerCode": {
          "type": "string",
          "example": "null",
          "description": "Uniquely identifies the dealer/rep user."
        },
        "interactionId": {
          "type": "string",
          "example": "null",
          "description": "Alphanumeric value represent a common transaction id across all calls made from UI, while completing all business activity needs of a particular customer."
        },
        "masterDealerCode": {
          "type": "string",
          "example": "null",
          "description": "Code that uniquely identifies the master dealer for a large retail partner, such as Apple or Costco. Will not be applicable for TMo channels."
        },
        "segmentationId": {
          "type": "string",
          "example": "null",
          "description": "Identifier of customer's primary data center."
        },
        "senderId": {
          "type": "string",
          "example": "null",
          "description": "Uniquely identifies an Operation consumer/Partner."
        },
        "sessionId": {
          "type": "string",
          "example": "null",
          "description": "A value populated by the sender used to track the transactions that occur during a session, a long-lasting interaction, managed by the sender. GUID generated by partner. Within one session (sessionid) for Rep/system can serve  multiple customers(interactionid) and in a given customer interation ��� multiple API calls (activityid) will be made to serve the customer. 1 sessionid -> many interactionid 1 interactionid -> many activityid"
        },
        "storeId": {
          "type": "string",
          "example": "null",
          "description": "Unique identifier for the retail store location."
        },
        "terminalId": {
          "type": "string",
          "example": "null",
          "description": "Unique identifier for the retail store terminal."
        },
        "tillId": {
          "type": "string",
          "example": "null",
          "description": "Unique identifier for the retail store terminal till number."
        },
        "timestamp": {
          "type": "string",
          "format": "date-time",
          "example": "null",
          "description": "A timestamp provided by sender to track their workflow."
        },
        "workflowId": {
          "type": "string",
          "example": "null",
          "description": "Name of business purpose/flow, for which the API is being invoked."
        }
      },
      "description": "standard HTTP header elements to be able to deliver them to consumer without interruption",
      "example": "null"
    },
    "Event": {
      "type": "object",
      "required": [
        "eventType",
        "eventTime",
        "eventProducerId",
        "payload"
      ],
      "properties": {
        "eventId": {
          "description": "Unique identifier for the event generated by the event producer.",
          "type": "string"
        },
        "eventType": {
          "description": "Name of the event: serviceCreated|serviceDeleted|serviceEdited.",
          "type": "string"
        },
        "eventTime": {
          "description": "The date and time that the event occurred.",
          "type": "string",
          "format": "date-time"
        },
        "eventProducerId": {
          "description": "Unique identifier for the producer of the event.",
          "type": "string"
        },
        "eventVersion": {
          "description": "Version of the event.",
          "type": "string"
        },
        "headerReference": {
          "$ref": "#/definitions/HeaderReference"
        },
        "auditInfo": {
          "$ref": "#/definitions/AuditInfo"
        },
        "specifications": {
          "type": "array",
          "example": "null",
          "description": "added for future reference so can ignore.",
          "items": {
            "$ref": "#/definitions/Specifications"
          }
        },
        "payload": {
          "$ref": "#/definitions/Payload"
        }
      }
    },
    "Payload": {
      "type": "object",
      "description": "Event specific information that can be modeled using APIv2 or inline elements.",
      "properties": {
        "serviceOffer": {
          "$ref": "#/definitions/ServiceOffer"
        }
      }
    },
    "Account": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string"
        },
        "typeCode": {
          "type": "string"
        },
        "subType": {
          "type": "string"
        },
        "subTypeCode": {
          "type": "string"
        }
      },
      "required": [
        "type",
        "typeCode",
        "subType",
        "subTypeCode"
      ]
    },
    "Availability": {
      "type": "object",
      "properties": {
        "channels": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Channel"
          }
        },
        "markets": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Market"
          }
        }
      },
      "required": [
        "channels",
        "markets"
      ]
    },
    "ByProductAttribute": {
      "type": "object",
      "properties": {
        "field": {
          "type": "string"
        },
        "operator": {
          "type": "string"
        },
        "values": {
          "type": "array",
          "items": {
            "type": "string"
          }
        }
      },
      "required": [
        "field",
        "operator",
        "values"
      ]
    },
    "ByProductCategory": {
      "type": "object",
      "properties": {
        "categoryPath": {
          "type": "string"
        }
      },
      "required": [
        "categoryPath"
      ]
    },
    "ChangeSet": {
      "type": "object",
      "properties": {
        "activationDateTime": {
          "type": "string"
        },
        "stagingDateTime": {
          "type": "string"
        },
        "changeSetName": {
          "type": "string"
        }
      },
      "required": [
        "activationDateTime",
        "stagingDateTime",
        "changeSetName"
      ]
    },
    "Channel": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "currentStatus": {
          "type": "string"
        },
        "lifeCycle": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/LifeCycle"
          }
        }
      },
      "required": [
        "name",
        "lifeCycle"
      ]
    },
    "ChannelVariance": {
      "type": "object",
      "properties": {
        "salesChannelCode": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "shortDescription": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "longDescription": {
          "type": "string"
        },
        "marketingMessages": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/MarketingMessage"
          }
        }
      },
      "required": [
        "salesChannelCode"
      ]
    },
    "CurrentCompatibleDevice": {
      "type": "object",
      "properties": {
        "deviceOfferId": {
          "type": "integer"
        },
        "sku": {
          "type": "string"
        }
      }
    },
    "CurrentCompatibleRatePlan": {
      "type": "object",
      "properties": {
        "ratePlanOfferId": {
          "type": "integer"
        },
        "soc": {
          "type": "string"
        },
        "rpxBillingCode": {
          "type": "string"
        }
      },
      "required": [
        "soc"
      ]
    },
    "CurrentCompatibleService": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string"
        },
        "socs": {
          "type": "array",
          "items": {
            "type": "string"
          }
        }
      }
    },
    "DeviceCompatibility": {
      "type": "object",
      "properties": {
        "inclusionRules": {
          "$ref": "#/definitions/InclusionRule"
        },
        "exclusionRules": {
          "$ref": "#/definitions/ExclusionRule"
        },
        "currentCompatibleDevices": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/CurrentCompatibleDevice"
          }
        },
        "compatibleCapabilityCodes": {
          "type": "array",
          "items": {
            "type": "string"
          }
        }
      }
    },
    "Eligibility": {
      "type": "object",
      "properties": {
        "transactionTypes": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "billingTypes": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "customerGroups": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "accounts": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Account"
          }
        },
        "prerequisite": {
          "$ref": "#/definitions/Prerequisite"
        }
      },
      "required": [
        "billingTypes"
      ]
    },
    "ExclusionRule": {
      "type": "object",
      "properties": {
        "byProductCategory": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ByProductCategory"
          }
        },
        "byProductAttribute": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ByProductAttribute"
          }
        }
      }
    },
    "Identifier": {
      "type": "object",
      "properties": {
        "offerId": {
          "type": "integer"
        },
        "mpmVersionId": {
          "type": "integer"
        }
      },
      "required": [
        "offerId",
        "mpmVersionId"
      ]
    },
    "Identifier2": {
      "type": "object",
      "properties": {
        "serviceSpecId": {
          "type": "integer"
        }
      },
      "required": [
        "serviceSpecId"
      ]
    },
    "InclusionRule": {
      "type": "object",
      "properties": {
        "byProductCategory": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ByProductCategory"
          }
        },
        "byProductAttribute": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ByProductAttribute"
          }
        }
      }
    },
    "LifeCycle": {
      "type": "object",
      "properties": {
        "status": {
          "type": "string"
        },
        "customStatusText": {
          "type": "string"
        },
        "effectiveDateTime": {
          "type": "string"
        }
      },
      "required": [
        "status",
        "effectiveDateTime"
      ]
    },
    "Market": {
      "type": "object",
      "properties": {
        "specificationType": {
          "type": "string"
        },
        "code": {
          "type": "string"
        },
        "name": {
          "type": "string"
        }
      },
      "required": [
        "specificationType",
        "name"
      ]
    },
    "MarketingMessage": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string"
        },
        "message": {
          "type": "string"
        }
      },
      "required": [
        "code",
        "message"
      ]
    },
    "MeteredCharge": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "type": {
          "type": "string"
        },
        "usageType": {
          "type": "string"
        },
        "usageUnit": {
          "type": "string"
        },
        "incurredAt": {
          "type": "string"
        },
        "amount": {
          "type": "number"
        }
      },
      "required": [
        "code",
        "amount"
      ]
    },
    "Offer": {
      "type": "object",
      "properties": {
        "serviceOfferId": {
          "type": "integer"
        },
        "soc": {
          "type": "string"
        },
        "rpxBillingCode": {
          "type": "string"
        }
      },
      "required": [
        "serviceOfferId"
      ]
    },
    "OneTimeCharge": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "type": {
          "type": "string"
        },
        "level": {
          "type": "string"
        },
        "incurredAt": {
          "type": "string"
        },
        "amount": {
          "type": "number"
        },
        "rules": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Rule"
          }
        }
      },
      "required": [
        "code",
        "amount"
      ]
    },
    "OneTimeDiscount": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "type": {
          "type": "string"
        },
        "level": {
          "type": "string"
        },
        "incurredAt": {
          "type": "string"
        },
        "amount": {
          "type": "number"
        },
        "rules": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Rule"
          }
        }
      },
      "required": [
        "code",
        "amount"
      ]
    },
    "Prerequisite": {
      "type": "object",
      "properties": {
        "offers": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Offer"
          }
        },
        "serviceSpecIds": {
          "type": "array",
          "items": {
            "type": "integer"
          }
        }
      }
    },
    "PricePoint": {
      "type": "object",
      "properties": {
        "serviceTaxIncluded": {
          "type": "boolean"
        },
        "recurringCharges": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/RecurringCharge"
          }
        },
        "oneTimeCharges": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/OneTimeCharge"
          }
        },
        "meteredCharges": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/MeteredCharge"
          }
        },
        "recurringDiscounts": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/RecurringDiscount"
          }
        },
        "oneTimeDiscounts": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/OneTimeDiscount"
          }
        }
      }
    },
    "ProductSpec": {
      "type": "object",
      "properties": {
        "identifier": {
          "$ref": "#/definitions/Identifier2"
        }
      }
    },
    "RecurringCharge": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "type": {
          "type": "string"
        },
        "level": {
          "type": "string"
        },
        "frequency": {
          "type": "string"
        },
        "numberOfOccurrences": {
          "type": "integer"
        },
        "initiationOccurrence": {
          "type": "integer"
        },
        "amount": {
          "type": "number"
        },
        "rules": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Rule"
          }
        }
      },
      "required": [
        "code",
        "amount"
      ]
    },
    "RecurringDiscount": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "type": {
          "type": "string"
        },
        "level": {
          "type": "string"
        },
        "frequency": {
          "type": "string"
        },
        "numberOfOccurrences": {
          "type": "integer"
        },
        "initiationOccurrence": {
          "type": "integer"
        },
        "amount": {
          "type": "number"
        },
        "rules": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Rule"
          }
        }
      },
      "required": [
        "code",
        "amount"
      ]
    },
    "Rule": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "value": {
          "type": "string"
        }
      },
      "required": [
        "name",
        "value"
      ]
    },
    "SamsonBilling": {
      "type": "object",
      "properties": {
        "soc": {
          "type": "string"
        },
        "socIndicator": {
          "type": "string"
        },
        "socClassifications": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "samsonProductType": {
          "type": "string"
        }
      },
      "required": [
        "soc",
        "samsonProductType"
      ]
    },
    "ServiceOffer": {
      "type": "object",
      "properties": {
        "changeSet": {
          "$ref": "#/definitions/ChangeSet"
        },
        "identifier": {
          "$ref": "#/definitions/Identifier"
        },
        "offerType": {
          "type": "string"
        },
        "productSpec": {
          "$ref": "#/definitions/ProductSpec"
        },
        "samsonBilling": {
          "$ref": "#/definitions/SamsonBilling"
        },
        "rpxBillingCode": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "channelVariances": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ChannelVariance"
          }
        },
        "creditCheckRequired": {
          "type": "boolean"
        },
        "isPromotional": {
          "type": "boolean"
        },
        "duration": {
          "type": "integer"
        },
        "durationUom": {
          "type": "string"
        },
        "pricePoint": {
          "$ref": "#/definitions/PricePoint"
        },
        "removableBy": {
          "type": "string"
        },
        "visibility": {
          "type": "string"
        },
        "currentCompatibleRatePlans": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/CurrentCompatibleRatePlan"
          }
        },
        "currentCompatibleServices": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/CurrentCompatibleService"
          }
        },
        "isDeviceIndependent": {
          "type": "boolean"
        },
        "deviceCompatibility": {
          "$ref": "#/definitions/DeviceCompatibility"
        },
        "availability": {
          "$ref": "#/definitions/Availability"
        },
        "eligibility": {
          "$ref": "#/definitions/Eligibility"
        }
      },
      "required": [
        "changeSet",
        "identifier",
        "productSpec",
        "samsonBilling",
        "name",
        "creditCheckRequired",
        "isPromotional",
        "visibility",
        "isDeviceIndependent",
        "availability",
        "eligibility"
      ]
    }
  }
}