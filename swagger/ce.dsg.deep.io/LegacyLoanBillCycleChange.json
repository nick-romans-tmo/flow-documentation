

{
   "swagger": "2.0",
   "basePath": "/deep/v1",
   "schemes": [
      "http"
   ],
   "info": {
      "title": "EIP Legacy Loan Update Events",
      "description": "This API is for DEEP - EIP Legacy Loan Update Events",
      "contact": {},
      "version": "1"
   },
   "paths": {
      "/events/{eventtype}": {
         "post": {
            "tags": [
               "UpdateLoanEventsRequest"
            ],
            "consumes": [
               "application/json",
               "application/xml"
            ],
            "produces": [
               "application/json",
               "application/xml"
            ],
            "responses": {
               "200": {
                  "description": "Message Published Successfully.",
                  "schema": {
                     "type": "object",
                     "properties": {}
                  }
               },
               "201": {
                  "description": "Message Broker has not accepted the message but has been Saved and need resubmission from UI. Please Contact Admin to resubmit"
               },
               "400": {
                  "description": "Not a valid Request"
               },
               "429": {
                  "description": "Too Many Requests"
               },
               "500": {
                  "description": "Error while sending message to Queue OR Error happened inside producer"
               },
               "501": {
                  "description": "Rule is Not Configured For EventType"
               },
               "502": {
                  "description": "Contract is Not Defined"
               },
               "503": {
                  "description": "Contract is defined but payload posted is not valid as per uploaded contract"
               }
            },
            "operationId": "UpdateLoanEventsRequest",
            "parameters": [
               {
                  "name": "eventtype",
                  "in": "path",
                  "required": true,
                  "description": "",
                  "type": "string"
               },
               {
                  "name": "updateLoanEventsRequest",
                  "in": "body",
                  "schema": {
                     "maxItems": 1,
                     "$ref": "#/definitions/Event"
                  },
                  "description": ""
               }
            ],
            "summary": "updateLoan Deep Event"
         }
      }
   },
   "tags": [
      {
         "name": "Event"
      }
   ],
   "definitions": {
      "auditInfo": {
         "type": "object",
         "properties": {
            "customerId": {
               "description": "Uniquely identifies the Customer.",
               "type": "string"
            },
            "accountNumber": {
               "description": "The financial account number.",
               "type": "string"
            },
            "universalLineId": {
               "description": "ULID - Universal identifier of the LineofService, as defined in Customer Hub. ",
               "type": "string"
            },
            "lineId": {
               "description": "Uniquely identifies a  line of service.",
               "type": "string"
            },
            "phoneNumber": {
               "description": "The phone number (MSISDN) associated with the line of service.",
               "type": "string"
            },
            "iamUniqueId": {
               "description": "Unique identifier for Identity and Access Management.",
               "type": "string"
            },
            "batchId": {
               "description": "Identifer of batch job.",
               "type": "string"
            },
            "orderId": {
               "description": "Identifier of order.",
               "type": "string"
            }
         }
      },
      "headerReference": {
         "required": [
            "timestamp",
            "channelId",
            "applicationId",
            "senderId"
         ],
         "type": "object",
         "properties": {
            "activityId": {
               "type": "string"
            },
            "applicationId": {
               "type": "string"
            },
            "applicationUserId": {
               "type": "string"
            },
            "channelId": {
               "type": "string"
            },
            "dealerCode": {
               "type": "string"
            },
            "interactionId": {
               "type": "string"
            },
            "segmentationId": {
               "type": "string"
            },
            "senderId": {
               "type": "string"
            },
            "sessionId": {
               "type": "string"
            },
            "storeId": {
               "type": "string"
            },
            "terminalId": {
               "type": "string"
            },
            "tillId": {
               "type": "string"
            },
            "workflowId": {
               "type": "string"
            },
            "authCustomerId": {
               "type": "string"
            },
            "authFinancialAccountId": {
               "type": "string"
            },
            "authLineOfServiceId": {
               "type": "string"
            },
            "timestamp": {
               "type": "string",
               "format": "date-time"
            },
            "masterDealerCode": {
               "type": "string"
            },
            "sourceSystemCode": {
               "type": "string"
            }
         }
      },
      "purchasedEquipment": {
         "description": "Purchased equipment",
         "type": "object",
         "properties": {
            "imei": {
               "description": "International Mobile Equipment Identifier.  Unique identifier for a mobile device on a network.",
               "type": "string"
            },
            "serialNumber": {
               "description": "Serial number of the equipment (device or accessory).",
               "type": "string"
            },
            "sku": {
               "description": "The sku of the device offer variant.  Only relevant if the customer purchased the device from T-Mobile.",
               "type": "string"
            },
            "description": {
               "description": "Description of the offer variant",
               "type": "string"
            }
         }
      },
      "specifications": {
         "description": "Name value pair",
         "type": "object",
         "properties": {
            "name": {
               "type": "string"
            },
            "value": {
               "type": "string"
            }
         }
      },
      "Payload": {
         "required": [
            "accountNumber"
         ],
         "type": "object",
         "properties": {
            "loanId": {
               "description": "Loan identifier",
               "type": "string"
            },
            "balance": {
               "description": "Loan amount that is owed",
               "type": "number",
               "format": "double"
            },
            "phoneNumber": {
               "description": "Phone number or MSISDN",
               "type": "string"
            },
            "chargeId": {
               "description": "Charge ID",
               "type": "string"
            },
            "chargeCloseDate": {
               "description": "Charge Close Date",
               "type": "string",
               "format": "date-time"
            },
            "chargeAmount": {
               "description": "Amount of down payment",
               "type": "number",
               "format": "double"
            },
            "downPaymentAmount": {
               "description": "Amount of down payment",
               "type": "number",
               "format": "double"
            },
			"billCycleDay": {
			  "description": "Day of the month when bill cycle normally runs.",
			  "type": "integer",
			  "format": "int32"
			},
            "paymentSchedule": {
               "description": "Schedule of payments for the loan/lease.",
               "type": "array",
               "items": {
                  "$ref": "#/definitions/paymentSchedule"
               }
            },
            "installmentPlanId": {
               "type": "string"
            },
            "devicePayment": {
               "description": "Device Payments Details",
               "$ref": "#/definitions/devicePayment"
            },
            "purchasedEquipment": {
               "description": "Purchased equipment",
               "$ref": "#/definitions/purchasedEquipment"
            },
            "statusCode": {
               "description": "Status code",
               "type": "string"
            },
            "accountNumber": {
               "type": "string"
            },
            "totalTransactionAmount": {
               "description": "Total amount charged/ credited due to this event on the customer account",
               "type": "number",
               "format": "double"
            }
         }
      },
      "Event": {
         "required": [
            "eventProducerId",
            "eventTime",
            "eventType",
            "payload"
         ],
         "type": "object",
         "properties": {
            "eventId": {
               "description": "Identifier of the event",
               "type": "string"
            },
            "eventType": {
               "description": "Type of event",
               "type": "string"
            },
            "eventTime": {
               "description": "The date and time that the event occurred.",
               "type": "string",
               "format": "date-time"
            },
            "eventProducerId": {
               "description": "Identifier of the event producer",
               "type": "string"
            },
            "eventVersion": {
               "description": "Version of the event",
               "type": "string"
            },
            "specifications": {
               "description": "Name value pair",
               "type": "array",
               "items": {
                  "$ref": "#/definitions/specifications"
               }
            },
            "auditInfo": {
               "$ref": "#/definitions/auditInfo"
            },
            "payload": {
               "$ref": "#/definitions/Payload"
            },
            "headerReference": {
               "$ref": "#/definitions/headerReference"
            }
         }
      },
      "paymentSchedule": {
         "description": "Schedule of payments for the loan.",
         "type": "object",
         "properties": {
            "paymentDueDate": {
               "description": "Due date of payment in a month",
               "type": "string",
               "format": "date-time"
            },
            "installmentNumber": {
               "description": "Installment number of the scheduled payment",
               "type": "integer",
               "format": "int32"
            },
            "paymentAmount": {
               "description": "Payment amount",
               "type": "number",
               "format": "double"
            }
         }
      },
      "devicePayment": {
         "description": "Schedule of payments for the loan/lease.",
         "type": "object",
         "properties": {
            "paymentReferenceId": {
               "description": "Payment Refrence Id",
               "type": "string"
            },
            "paymentId": {
               "description": "Payment Transaction Id",
               "type": "string"
            },
            "jPayid": {
               "description": "JPayid for payment",
               "type": "string"
            },
            "paymentAmount": {
               "description": "Amount of the payment",
               "type": "number",
               "format": "double"
            },
            "paymentType": {
               "description": "Loan Payment Type",
               "type": "string"
            },
            "cardType": {
               "description": "Crad Type for Payment",
               "type": "string"
            },
            "last4CardNumber": {
               "description": "Crad last 4 Digit",
               "type": "string"
            }
         }
      }
   }
}

