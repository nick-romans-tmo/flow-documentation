{
	"swagger": "2.0",
	"info": {
		"description": "DEEP.io Publish Event API",
		"version": "1.0",
		"title": "DEEP.io Publish Event API"
	},
	"host": "localhost:8080",
	"basePath": "/deep/v1/events",
	"paths": {
		"/{eventType}": {
			"post": {
				"tags": ["LoanUpdateException",
				"LoanMonthlyChargeExceptionUpdate"],
				"summary": "This Generic API shall be used by producers to publish events into DEEP.io.",
				"operationId": "chargeInjectionUsingPOST",
				"consumes": ["application/json"],
				"produces": ["*/*"],
				"parameters": [{
					"name": "eventType",
					"in": "path",
					"description": "Unique name of the event",
					"required": true,
					"type": "string"
				},
				{
					"in": "body",
					"name": "event",
					"description": "Event Complex Element",
					"required": true,
					"schema": {
						"$ref": "#/definitions/Event"
					}
				}],
				"responses": {
					"200": {
						"description": "Message Published Successfully.",
						"schema": {
							"type": "object",
							"properties": {
								
							}
						}
					},
					"201": {
						"description": "Message Broker has not accepted the message but has been Saved and need resubmission from UI. Please Contact Admin to resubmit"
					},
					"400": {
						"description": "Not a valid Request",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"429": {
						"description": "Too Many Requests"
					},
					"500": {
						"description": "Error while sending message to Queue OR Error happened inside producer",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"501": {
						"description": "Rule is Not Configured For EventType"
					},
					"502": {
						"description": "Contract is Not Defined",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"503": {
						"description": "Contract is defined but payload posted is not valid as per uploaded contract",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					}
				}
			}
		}
	},
	"definitions": {
		"error": {
			"description": "Errorobject.",
			"type": "object",
			"properties": {
				"code": {
					"description": "AspecificT-Mobileerrorcode.",
					"type": "string"
				},
				"userMessage": {
					"description": "Ahuman-readablemessagedescribingtheerror.",
					"type": "string"
				},
				"systemMessage": {
					"description": "Backendsystemerrormessage.",
					"type": "string"
				}
			}
		},
		"errors": {
			"description": "Thearrayoferrorobjects.Asdefinedinhttps: //tmobileusa.sharepoint.com/teams/EnterpriseServices/ESG%20Standards%20Documents/APIs/T-Mobile%20API%20Design%20Guidelines%20v1.0.docx",
			"type": "object",
			"properties": {
				"errors": {
					"type": "array",
					"items": {
						"$ref": "#/definitions/error"
					}
				}
			}
		},
		"Event": {
			"type": "object",
			"required": ["eventProducerId",
			"eventTime",
			"eventType",
			"payload"],
			"properties": {
				"auditInfo": {
					"$ref": "#/definitions/AuditInfo"
				},
				"eventId": {
					"description": "Uniqueidentifierfortheeventgeneratedbytheeventproducer.ThiselementwillbesearchableinDEEP.ioUI",
					"type": "string"
				},
				"eventProducerId": {
					"description": "Uniqueidentifierfortheproduceroftheevent.",
					"type": "string"
				},
				"eventTime": {
					"description": "Thedateandtimethattheeventoccurred.",
					"type": "string",
					"format": "date-time"
				},
				"eventType": {
					"description": "EventNamethatyouwanttopublish",
					"type": "string"
				},
				"eventVersion": {
					"description": "versionoftheevent, willbeusedtodistinguishdifferentversionsbyaconsumer",
					"type": "string"
				},
				"headerReference": {
					"$ref": "#/definitions/HeaderReference"
				},
				"payload": {
					"$ref": "#/definitions/Payload"
				},
				"specifications": {
					"description": "addedforfuturereferencesocanignore.",
					"type": "array",
					"items": {
						"$ref": "#/definitions/Specifications"
					}
				}
			}
		},
		"AuditInfo": {
			"type": "object",
			"description": "Auditinformationusedtosearchspecificevent.AllelementsincludedinthiscomplexelementwillbesearchableinDEEP.io.",
			"properties": {
				"accountNumber": {
					"description": "Thefinancialaccountnumber.",
					"type": "string"
				},
				"batchId": {
					"description": "Identiferofbatchjob.",
					"type": "string"
				},
				"customerId": {
					"description": "UniquelyidentifiestheCustomer.",
					"type": "string"
				},
				"iamUniqueId": {
					"description": "UniqueidentifierforIdentityandAccessManagement.",
					"type": "string"
				},
				"lineId": {
					"description": "Uniquelyidentifiesalineofservice.",
					"type": "string"
				},
				"orderId": {
					"description": "Identifieroforder.",
					"type": "string"
				},
				"phoneNumber": {
					"description": "Thephonenumber(MSISDN)associatedwiththelineofservice.",
					"type": "string"
				},
				"universalLineId": {
					"description": "ULID-Universal identifier of the Line of Service, as defined in CustomerHub.",
					"type": "string"
				}
			}
		},
		"HeaderReference": {
			"type": "object",
			"description": "standardHTTPheaderelementstobeabletodeliverthemtoconsumerwithoutinterruption",
			"properties": {
				"activityId": {
					"description": "Uniqueidalphanumericvaluegeneratedandsentbythepartnertoidentifyeveryservicerequestuniquely.",
					"type": "string"
				},
				"applicationId": {
					"description": "Identifiestheapplication, systemorplatformnamethatisbeingusedtoinitiatethetransactionrelatedtothisrequest.",
					"type": "string"
				},
				"applicationUserId": {
					"description": "NTidoftherepwhoismanagingthetransactionfromCARE/RETAILchannels. Requiredforassistedchannels, notrequiredforunassistedchannels.",
					"type": "string"
				},
				"authCustomerId": {
					"description": "CustomerIdofTmocustomerthatisbeingserved/usingtheapplication.(Requiredfornonanonymousflows)",
					"type": "string"
				},
				"authFinancialAccountId": {
					"description": "FinancialAccountIdthatthecustomerthatisbeingserved/usingtheapplicationisworkingon.(Requiredfornonanonymousflows)",
					"type": "string"
				},
				"authLineOfServiceId": {
					"description": "LineofserviceId, withintheFinancialAccountofthecustomerthatisbeingserved/usingtheapplicationisworkingon.(Requiredfornonanonymousflows)",
					"type": "string"
				},
				"channelId": {
					"description": "Identifiesthebusinessunitorsaleschannel.",
					"type": "string"
				},
				"dealerCode": {
					"description": "Uniquelyidentifiesthedealer/repuser.",
					"type": "string"
				},
				"interactionId": {
					"description": "AlphanumericvaluerepresentacommontransactionidacrossallcallsmadefromUI, whilecompletingallbusinessactivityneedsofaparticularcustomer.",
					"type": "string"
				},
				"masterDealerCode": {
					"description": "Codethatuniquelyidentifiesthemasterdealerforalargeretailpartner, suchasAppleorCostco.WillnotbeapplicableforTMochannels.",
					"type": "string"
				},
				"segmentationId": {
					"description": "Identifierofcustomer���sprimarydatacenter.",
					"type": "string"
				},
				"senderId": {
					"description": "UniquelyidentifiesanOperationconsumer/Partner.",
					"type": "string"
				},
				"sessionId": {
					"description": "Avaluepopulatedbythesenderusedtotrackthetransactionsthatoccurduringasession, along-lastinginteraction, managedbythesender.GUIDgeneratedbypartner.Withinonesession(sessionid)forRep/systemcanservemultiplecustomers(interactionid)andinagivencustomerinteration���multipleAPIcalls(activityid)willbemadetoservethecustomer.1sessionid->manyinteractionid1interactionid->manyactivityid",
					"type": "string"
				},
				"storeId": {
					"description": "Uniqueidentifierfortheretailstorelocation.",
					"type": "string"
				},
				"terminalId": {
					"description": "Uniqueidentifierfortheretailstoreterminal.",
					"type": "string"
				},
				"tillId": {
					"description": "Uniqueidentifierfortheretailstoreterminaltillnumber.",
					"type": "string"
				},
				"timestamp": {
					"description": "Atimestampprovidedbysendertotracktheirworkflow.",
					"type": "string",
					"format": "date-time"
				},
				"workflowId": {
					"description": "Nameofbusinesspurpose/flow, forwhichtheAPIisbeinginvoked.",
					"type": "string"
				}
			}
		},
		"Payload": {
			"type": "object",
			"description": "EventspecificinformationthatcanbemodeledusingAPIv2orinlineelements.",
			"properties": {
				"exceptionData": {
					"$ref": "#/definitions/ExceptionData"
				}
			}
		},
		"ExceptionData": {
			"type": "object",
			"properties": {
				"loanId": {
					"description": "Loanidentifier",
					"type": "string"
				},
				"accountNumber": {
					"description": "Thefinancialaccountnumberthatidentifiestheaccount.",
					"type": "string"
				},
				"agreementNumber": {
					"description": "UniqueIdentifierforthetransactionwherecustomeragreestotheLoan/Lease.",
					"type": "string"
				},
				"universalLineId": {
					"description": "ULID-UniversalidentifieroftheLineofService, asdefinedinCustomerHub.",
					"type": "string"
				},
				"billingChargeCreationTime": {
					"description": "Transactioncreationdateandtime",
					"type": "string",
					"format": "date-time"
				},
				"chargeType": {
					"description": "Tansactioncode, sentbyMercuy.",
					"type": "string"
				},
				"chargeReason": {
					"description": "Transactiondescription.",
					"type": "string"
				},
				"chargeAmount": {
					"description": "Transactionamount",
					"type": "number",
					"format": "double"
				},
				"paymentNumber": {
					"description": "Installmentnumber",
					"type": "integer",
					"format": "int32"
				},
				"lastPaymentNumber": {
					"description": "Lastinstallmentnumber",
					"type": "integer",
					"format": "int32"
				},
				"remainingBalance": {
					"description": "unpaidloanbalance",
					"type": "number",
					"format": "double"
				},
				"billingChargeCode": {
					"description": "Activitycodeassociatedwiththecharge",
					"type": "string"
				},
				"deviceDescription": {
					"description": "Descriptionofthedeviceassociatedwiththeloan",
					"type": "string"
				},
				"chargeId": {
					"description": "Chargeidentifierusedforbilling/postingscheduledpayments",
					"type": "string"
				},
				"statementId": {
					"description": "Statementidentifierusedforbilling/postingscheduledpayments",
					"type": "string"
				},
				"unbilledChargeMonth": {
					"description": "Themonthfortheunbilledcharge.",
					"type": "integer",
					"format": "int32"
				},
				"unbilledChargeYear": {
					"description": "Theyearfortheunbilledcharge",
					"type": "integer",
					"format": "int32"
				},
				"unbilledAmount": {
					"description": "Unbilledamountoftheloan, itisbalanceminusbilledamount",
					"type": "number",
					"format": "double"
				},
				"chargeActivityId": {
					"description": "Uniqueidentifierforthechargeactivity",
					"type": "string"
				},
				"exceptionDetails": {
					"$ref": "#/definitions/ExceptionDetails"
				},
				"sourceTransaction": {
					"$ref": "#/definitions/SourceTransaction"
				}
			}
		},
		"ExceptionDetails": {
			"type": "object",
			"properties": {
				"errorCode": {
					"type": "string"
				},
				"errorDescription": {
					"type": "string"
				}
			}
		},
		"SourceTransaction": {
			"type": "object",
			"properties": {
				"sourceTransactionId": {
					"description": "GUIDoftheoriginalsourcetransaction",
					"type": "string"
				},
				"sourceTransactionTime": {
					"description": "Timestampofthesourcetransaction",
					"type": "string",
					"format": "date-time"
				},
				"sourceEventId": {
					"description": "IDofthesourcetransaction",
					"type": "string"
				},
				"sourceEventType": {
					"description": "Nameofthesourcetransactionevent",
					"type": "string"
				}
			}
		},
		"Specifications": {
			"type": "object",
			"description": "addedforfuturereferencesocanignore.",
			"properties": {
				"name": {
					"type": "string"
				},
				"value": {
					"type": "string"
				}
			}
		}
	}
}