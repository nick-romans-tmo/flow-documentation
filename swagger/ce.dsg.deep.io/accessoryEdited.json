{
  "swagger": "2.0",
  "info": {
    "description": "DEEP.io Publish Event API for Accessory resource.",
    "version": "1.0",
    "title": "Accessory"
  },
  "host": "localhost:8080",
  "basePath": "/deep/v1/events",
  "paths": {
    "/{eventType}": {
      "post": {
        "tags": [
          "messaging-controller"
        ],
        "summary": "This API shall be used by producers to publish Accessory events (create, delete, edit) into DEEP.io.",
        "operationId": "publishEvent",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "*/*"
        ],
        "parameters": [
          {
            "name": "eventType",
            "in": "path",
            "description": "Unique name of the event",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "event",
            "description": "Event Complex Element",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Event"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Message Published Successfully.",
            "schema": {
              "type": "object",
              "properties": {
              }
            }
          },
          "201": {
            "description": "Message Broker has not accepted the message but has been Saved and need resubmission from UI. Please Contact Admin to resubmit"
          },
          "400": {
            "description": "Not a valid Request."
          },
          "429": {
            "description": "Too Many Requests."
          },
          "500": {
            "description": "Error while sending message to Queue OR Error happened inside producer"
          },
          "501": {
            "description": "Rule is Not Configured For EventType"
          },
          "502": {
            "description": "Contract is Not Defined"
          },
          "503": {
            "description": "Contract is defined but payload posted is not valid as per uploaded contract"
          }
        }
      }
    }
  },
  "definitions": {
    "Specifications": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string",
          "example": "null"
        },
        "value": {
          "type": "string",
          "example": "null"
        }
      },
      "description": "Name value pair."
    },
    "AuditInfo": {
      "type": "object",
      "properties": {
        "accountNumber": {
          "type": "string",
          "example": "F123111",
          "description": "The financial account number."
        },
        "batchId": {
          "type": "string",
          "example": "batch0",
          "description": "Identifer of batch job."
        },
        "customerId": {
          "type": "string",
          "example": "Customer1",
          "description": "Uniquely identifies the Customer."
        },
        "iamUniqueId": {
          "type": "string",
          "example": "deepuser",
          "description": "Unique identifier for Identity and Access Management."
        },
        "lineId": {
          "type": "string",
          "example": "1234567890",
          "description": "Uniquely identifies a  line of service."
        },
        "orderId": {
          "type": "string",
          "example": "order1",
          "description": "Identifier of order."
        },
        "phoneNumber": {
          "type": "string",
          "example": "1234567890",
          "description": "The phone number (MSISDN) associated with the line of service."
        },
        "universalLineId": {
          "type": "string",
          "example": "1234567890",
          "description": "ULID - Universal identifier of the LineofService, as defined in Customer Hub. "
        }
      },
      "description": "Audit information used to search specific event. All elements included in this complex element will be searchable in DEEP.io.",
      "example": "null"
    },
    "HeaderReference": {
      "type": "object",
      "properties": {
        "activityId": {
          "type": "string",
          "example": "null",
          "description": "Unique id alphanumeric value generated and sent by the partner to identify every service request uniquely."
        },
        "applicationId": {
          "type": "string",
          "example": "null",
          "description": "Identifies the application, system or platform name that is being used to initiate the transaction  related to this request."
        },
        "applicationUserId": {
          "type": "string",
          "example": "null",
          "description": "NT id of the rep who is managing the transaction from CARE/RETAIL channels. Required for assisted channels, not required for unassisted channels."
        },
        "authCustomerId": {
          "type": "string",
          "example": "null",
          "description": "Customer Id of Tmo customer that is being served/using the application. (Required for non anonymous flows)"
        },
        "authFinancialAccountId": {
          "type": "string",
          "example": "null",
          "description": "Financial Account Id that the customer that is being served/using the application is working on.  (Required for non anonymous flows)"
        },
        "authLineOfServiceId": {
          "type": "string",
          "example": "null",
          "description": "Line of service Id , within the Financial Account of the customer that is being served/using the application is working on. (Required for non anonymous flows)"
        },
        "channelId": {
          "type": "string",
          "example": "null",
          "description": "Identifies the business unit or sales channel."
        },
        "dealerCode": {
          "type": "string",
          "example": "null",
          "description": "Uniquely identifies the dealer/rep user."
        },
        "interactionId": {
          "type": "string",
          "example": "null",
          "description": "Alphanumeric value represent a common transaction id across all calls made from UI, while completing all business activity needs of a particular customer."
        },
        "masterDealerCode": {
          "type": "string",
          "example": "null",
          "description": "Code that uniquely identifies the master dealer for a large retail partner, such as Apple or Costco. Will not be applicable for TMo channels."
        },
        "segmentationId": {
          "type": "string",
          "example": "null",
          "description": "Identifier of customer's primary data center."
        },
        "senderId": {
          "type": "string",
          "example": "null",
          "description": "Uniquely identifies an Operation consumer/Partner."
        },
        "sessionId": {
          "type": "string",
          "example": "null",
          "description": "A value populated by the sender used to track the transactions that occur during a session, a long-lasting interaction, managed by the sender. GUID generated by partner. Within one session (sessionid) for Rep/system can serve  multiple customers(interactionid) and in a given customer interation ��� multiple API calls (activityid) will be made to serve the customer. 1 sessionid -> many interactionid 1 interactionid -> many activityid"
        },
        "storeId": {
          "type": "string",
          "example": "null",
          "description": "Unique identifier for the retail store location."
        },
        "terminalId": {
          "type": "string",
          "example": "null",
          "description": "Unique identifier for the retail store terminal."
        },
        "tillId": {
          "type": "string",
          "example": "null",
          "description": "Unique identifier for the retail store terminal till number."
        },
        "timestamp": {
          "type": "string",
          "format": "date-time",
          "example": "null",
          "description": "A timestamp provided by sender to track their workflow."
        },
        "workflowId": {
          "type": "string",
          "example": "null",
          "description": "Name of business purpose/flow, for which the API is being invoked."
        }
      },
      "description": "standard HTTP header elements to be able to deliver them to consumer without interruption",
      "example": "null"
    },
    "Event": {
      "type": "object",
      "required": [
        "eventType",
        "eventTime",
        "eventProducerId",
        "payload"
      ],
      "properties": {
        "eventId": {
          "description": "Unique identifier for the event generated by the event producer.",
          "type": "string"
        },
        "eventType": {
          "description": "Name of the event: serviceCreated|serviceDeleted|serviceEdited.",
          "type": "string"
        },
        "eventTime": {
          "description": "The date and time that the event occurred.",
          "type": "string",
          "format": "date-time"
        },
        "eventProducerId": {
          "description": "Unique identifier for the producer of the event.",
          "type": "string"
        },
        "eventVersion": {
          "description": "Version of the event.",
          "type": "string"
        },
        "headerReference": {
          "$ref": "#/definitions/HeaderReference"
        },
        "auditInfo": {
          "$ref": "#/definitions/AuditInfo"
        },
        "specifications": {
          "type": "array",
          "example": "null",
          "description": "added for future reference so can ignore.",
          "items": {
            "$ref": "#/definitions/Specifications"
          }
        },
        "payload": {
          "$ref": "#/definitions/Payload"
        }
      }
    },
    "Payload": {
      "type": "object",
      "description": "Event specific information that can be modeled using APIv2 or inline elements.",
      "properties": {
        "accessorySpec": {
          "$ref": "#/definitions/AccessorySpec"
        }
      }
    },
    "AccessorySpec": {
      "type": "object",
      "properties": {
        "changeSet": {
          "$ref": "#/definitions/ChangeSet"
        },
        "identifier": {
          "$ref": "#/definitions/Identifier"
        },
        "name": {
          "type": "string"
        },
        "displayBadges": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/DisplayBadge"
          }
        },
        "titleTag": {
          "type": "string"
        },
        "altTag": {
          "type": "string"
        },
        "metaTag": {
          "type": "string"
        },
        "h1Tag": {
          "type": "string"
        },
        "sortOrder": {
          "type": "integer"
        },
        "images": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Image"
          }
        },
        "physicalDimension": {
          "$ref": "#/definitions/PhysicalDimension"
        },
        "colorSpecification": {
          "$ref": "#/definitions/ColorSpecification"
        },
        "searchableColors": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "condition": {
          "type": "string"
        },
        "boxContents": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/BoxContent"
          }
        },
        "fullRetailPrice": {
          "type": "number"
        },
        "itemCost": {
          "type": "number"
        },
        "damageCharge": {
          "type": "number"
        },
        "depositCharge": {
          "type": "number"
        },
        "nonReturnCharge": {
          "type": "number"
        },
        "warrantyDescription": {
          "type": "string"
        },
        "warrantyDuration": {
          "type": "number"
        },
        "warrantyDurationUom": {
          "type": "string"
        },
        "outOfStockThreshold": {
          "type": "integer"
        },
        "limitedQuantityThreshold": {
          "type": "integer"
        },
        "characteristics": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Characteristic"
          }
        },
        "model": {
          "$ref": "#/definitions/Model"
        }
      },
      "required": [
        "changeSet",
        "identifier",
        "name",
        "condition",
        "fullRetailPrice",
        "itemCost",
        "outOfStockThreshold",
        "limitedQuantityThreshold",
        "model"
      ]
    },
    "Battery": {
      "type": "object",
      "properties": {
        "description": {
          "type": "string"
        },
        "standbyTime": {
          "type": "number"
        },
        "standbyTimeUom": {
          "type": "string"
        },
        "talkTime": {
          "type": "number"
        },
        "talkTimeUom": {
          "type": "string"
        }
      }
    },
    "BoxContent": {
      "type": "object",
      "properties": {
        "content": {
          "type": "string"
        },
        "sortOrder": {
          "type": "integer"
        }
      },
      "required": [
        "content",
        "sortOrder"
      ]
    },
    "ChangeSet": {
      "type": "object",
      "properties": {
        "activationDateTime": {
          "type": "string"
        },
        "stagingDateTime": {
          "type": "string"
        },
        "changeSetName": {
          "type": "string"
        }
      },
      "required": [
        "activationDateTime",
        "stagingDateTime",
        "changeSetName"
      ]
    },
    "ChannelVariance": {
      "type": "object",
      "properties": {
        "salesChannelCode": {
          "type": "string"
        },
        "shortDescription": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "longDescription": {
          "type": "string"
        },
        "marketingMessages": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/MarketingMessage"
          }
        }
      },
      "required": [
        "salesChannelCode"
      ]
    },
    "Characteristic": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "value": {
          "type": "string"
        }
      },
      "required": [
        "name",
        "value"
      ]
    },
    "ChargingSolution": {
      "type": "object",
      "properties": {
        "wiredChargingCapable": {
          "type": "boolean"
        },
        "wirelessChargingCapable": {
          "type": "boolean"
        },
        "wiredSolutions": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "wirelessSolutions": {
          "type": "array",
          "items": {
            "type": "string"
          }
        }
      }
    },
    "Classification": {
      "type": "object",
      "properties": {
        "productType": {
          "type": "string"
        },
        "productCategory": {
          "type": "string"
        },
        "productClass": {
          "type": "string"
        },
        "productSubClass": {
          "type": "string"
        }
      },
      "required": [
        "productType",
        "productCategory",
        "productClass",
        "productSubClass"
      ]
    },
    "ColorSpecification": {
      "type": "object",
      "properties": {
        "color": {
          "type": "string"
        },
        "colorSwatchImageUrl": {
          "type": "string"
        },
        "colorSwatchOutOfStockImageUrl": {
          "type": "string"
        }
      },
      "required": [
        "color",
        "colorSwatchImageUrl",
        "colorSwatchOutOfStockImageUrl"
      ]
    },
    "DataConnectivityOption": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string"
        },
        "name": {
          "type": "array",
          "items": {
            "type": "string"
          }
        }
      },
      "required": [
        "type",
        "name"
      ]
    },
    "DisplayBadge": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "sortOrder": {
          "type": "integer"
        }
      },
      "required": [
        "code",
        "name",
        "sortOrder"
      ]
    },
    "Feature": {
      "type": "object",
      "properties": {
        "specificationType": {
          "type": "string"
        },
        "featureType": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "sortOrder": {
          "type": "integer"
        }
      },
      "required": [
        "specificationType",
        "name",
        "description",
        "sortOrder"
      ]
    },
    "Identifier": {
      "type": "object",
      "properties": {
        "accessorySpecId": {
          "type": "integer"
        },
        "mpmVersionId": {
          "type": "integer"
        },
        "sku": {
          "type": "string"
        },
        "upcs": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "seoName": {
          "type": "string"
        }
      },
      "required": [
        "accessorySpecId",
        "mpmVersionId",
        "sku",
        "upcs",
        "seoName"
      ]
    },
    "Identifier2": {
      "type": "object",
      "properties": {
        "accessoryModelSpecId": {
          "type": "integer"
        },
        "mpmVersionId": {
          "type": "integer"
        },
        "seoName": {
          "type": "string"
        }
      },
      "required": [
        "accessoryModelSpecId",
        "mpmVersionId",
        "seoName"
      ]
    },
    "Image": {
      "type": "object",
      "properties": {
        "imageType": {
          "type": "string"
        },
        "imageUrl": {
          "type": "string"
        },
        "altText": {
          "type": "string"
        },
        "thumbnails": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Thumbnail"
          }
        }
      },
      "required": [
        "imageType",
        "imageUrl",
        "altText"
      ]
    },
    "Manufacturer": {
      "type": "object",
      "properties": {
        "manufacturerId": {
          "type": "integer"
        },
        "name": {
          "type": "string"
        },
        "displayName": {
          "type": "string"
        },
        "isPrimary": {
          "type": "boolean"
        },
        "phoneNumber": {
          "type": "string"
        },
        "website": {
          "type": "string"
        },
        "sortOrder": {
          "type": "integer"
        }
      },
      "required": [
        "manufacturerId",
        "name",
        "displayName",
        "isPrimary",
        "sortOrder"
      ]
    },
    "MarketingMessage": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string"
        },
        "message": {
          "type": "string"
        }
      },
      "required": [
        "code",
        "message"
      ]
    },
    "Model": {
      "type": "object",
      "properties": {
        "identifier": {
          "$ref": "#/definitions/Identifier2"
        },
        "variantDifferentiators": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/VariantDifferentiator"
          }
        },
        "name": {
          "type": "string"
        },
        "displayName": {
          "type": "string"
        },
        "shortDescription": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "longDescription": {
          "type": "string"
        },
        "marketingMessages": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/MarketingMessage"
          }
        },
        "channelVariances": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ChannelVariance"
          }
        },
        "sortOrder": {
          "type": "integer"
        },
        "displayBadges": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/DisplayBadge"
          }
        },
        "manufacturers": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Manufacturer"
          }
        },
        "classification": {
          "$ref": "#/definitions/Classification"
        },
        "battery": {
          "$ref": "#/definitions/Battery"
        },
        "ports": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "chargingSolution": {
          "$ref": "#/definitions/ChargingSolution"
        },
        "dataConnectivityOptions": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/DataConnectivityOption"
          }
        },
        "productPlacements": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ProductPlacement"
          }
        },
        "productTechnologies": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ProductTechnology"
          }
        },
        "features": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Feature"
          }
        },
        "tags": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "currentCompatibleDeviceSkus": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "wpcClassification": {
          "$ref": "#/definitions/WpcClassification"
        }
      },
      "required": [
        "identifier",
        "name",
        "displayName",
        "description",
        "manufacturers",
        "classification",
        "wpcClassification"
      ]
    },
    "PhysicalDimension": {
      "type": "object",
      "properties": {
        "width": {
          "type": "number"
        },
        "widthUom": {
          "type": "string"
        },
        "height": {
          "type": "number"
        },
        "heightUom": {
          "type": "string"
        },
        "depth": {
          "type": "number"
        },
        "depthUom": {
          "type": "string"
        },
        "length": {
          "type": "number"
        },
        "lengthUom": {
          "type": "string"
        },
        "weight": {
          "type": "number"
        },
        "weightUom": {
          "type": "string"
        },
        "boxWeight": {
          "type": "number"
        },
        "boxWeightUom": {
          "type": "string"
        }
      }
    },
    "ProductPlacement": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "description": {
          "type": "string"
        }
      },
      "required": [
        "code",
        "name"
      ]
    },
    "ProductTechnology": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "description": {
          "type": "string"
        }
      },
      "required": [
        "code",
        "name"
      ]
    },
    "Thumbnail": {
      "type": "object",
      "properties": {
        "imageType": {
          "type": "string"
        },
        "imageUrl": {
          "type": "string"
        },
        "altText": {
          "type": "string"
        }
      },
      "required": [
        "imageType",
        "imageUrl",
        "altText"
      ]
    },
    "VariantDifferentiator": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "attribute": {
          "type": "string"
        },
        "attributeSuffix": {
          "type": "string"
        },
        "sortOrder": {
          "type": "integer"
        }
      },
      "required": [
        "name",
        "attribute",
        "sortOrder"
      ]
    },
    "WpcClassification": {
      "type": "object",
      "properties": {
        "category": {
          "type": "string"
        },
        "class": {
          "type": "string"
        },
        "subClass": {
          "type": "string"
        }
      },
      "required": [
        "category"
      ]
    }
  }
}