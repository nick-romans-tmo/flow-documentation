{
    "swagger": "2.0",
    "info": {
        "description": "ProvisionNonTmoWorker API",
        "version": "1.0",
        "title": "DEEP.io ProvisionNonTmoWorker POST API"
    },
    "host": "localhost:8080",
    "basePath": "/deep/v1/events",
    "paths": {
        "/{eventType}": {
            "post": {
                "tags": [
                    "messaging-controller"
                ],
                "summary": "This ProvisionNonTmoWorker shall be used by producers to publish events into DEEP.io.",
                "operationId": "sendMessageUsingPOST",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "*/*"
                ],
                "parameters": [
                    {
                        "name": "eventType",
                        "in": "path",
                        "description": "Unique name of the event",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "in": "body",
                        "name": "event",
                        "description": "Event Complex Element",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/Event"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Message Published Successfully.",
                        "schema": {
                            "type": "object",
                            "properties": {}
                        }
                    },
                    "201": {
                        "description": "Message Broker has not accepted the message but has been Saved and need resubmission from UI. Please Contact Admin to resubmit"
                    },
                    "400": {
                        "description": "Not a valid Request"
                    },
                    "429": {
                        "description": "Too Many Requests"
                    },
                    "500": {
                        "description": "Error while sending message to Queue OR Error happened inside producer"
                    },
                    "501": {
                        "description": "Rule is Not Configured For EventType"
                    },
                    "502": {
                        "description": "Contract is Not Defined"
                    },
                    "503": {
                        "description": "Contract is defined but payload posted is not valid as per uploaded contract"
                    }
                }
            }
        }
    },
    "definitions": {
        "AuditInfo": {
            "type": "object",
            "properties": {
                "accountNumber": {
                    "type": "string",
                    "example": "F123111",
                    "description": "The financial account number."
                },
                "batchId": {
                    "type": "string",
                    "example": "batch0",
                    "description": "Identifer of batch job."
                },
                "customerId": {
                    "type": "string",
                    "example": "Customer1",
                    "description": "Uniquely identifies the Customer."
                },
                "iamUniqueId": {
                    "type": "string",
                    "example": "deepuser",
                    "description": "Unique identifier for Identity and Access Management."
                },
                "lineId": {
                    "type": "string",
                    "example": "1234567890",
                    "description": "Uniquely identifies a  line of service."
                },
                "orderId": {
                    "type": "string",
                    "example": "order1",
                    "description": "Identifier of order."
                },
                "phoneNumber": {
                    "type": "string",
                    "example": "1234567890",
                    "description": "The phone number (MSISDN) associated with the line of service."
                },
                "universalLineId": {
                    "type": "string",
                    "example": "1234567890",
                    "description": "ULID - Universal identifier of the LineofService, as defined in Customer Hub. "
                }
            },
            "description": "Audit information used to search specific event. All elements included in this complex element will be searchable in DEEP.io.",
            "example": "null"
        },
        "HeaderReference": {
            "type": "object",
            "properties": {
                "activityId": {
                    "type": "string",
                    "example": "null",
                    "description": "Unique id alphanumeric value generated and sent by the partner to identify every service request uniquely."
                },
                "applicationId": {
                    "type": "string",
                    "example": "null",
                    "description": "Identifies the application, system or platform name that is being used to initiate the transaction  related to this request."
                },
                "applicationUserId": {
                    "type": "string",
                    "example": "null",
                    "description": "NT id of the rep who is managing the transaction from CARE/RETAIL channels. Required for assisted channels, not required for unassisted channels."
                },
                "authCustomerId": {
                    "type": "string",
                    "example": "null",
                    "description": "Customer Id of Tmo customer that is being served/using the application. (Required for non anonymous flows)"
                },
                "authFinancialAccountId": {
                    "type": "string",
                    "example": "null",
                    "description": "Financial Account Id that the customer that is being served/using the application is working on.  (Required for non anonymous flows)"
                },
                "authLineOfServiceId": {
                    "type": "string",
                    "example": "null",
                    "description": "Line of service Id , within the Financial Account of the customer that is being served/using the application is working on. (Required for non anonymous flows)"
                },
                "channelId": {
                    "type": "string",
                    "example": "null",
                    "description": "Identifies the business unit or sales channel."
                },
                "dealerCode": {
                    "type": "string",
                    "example": "null",
                    "description": "Uniquely identifies the dealer/rep user."
                },
                "interactionId": {
                    "type": "string",
                    "example": "null",
                    "description": "Alphanumeric value represent a common transaction id across all calls made from UI, while completing all business activity needs of a particular customer."
                },
                "masterDealerCode": {
                    "type": "string",
                    "example": "null",
                    "description": "Code that uniquely identifies the master dealer for a large retail partner, such as Apple or Costco. Will not be applicable for TMo channels."
                },
                "segmentationId": {
                    "type": "string",
                    "example": "null",
                    "description": "Identifier of customer???s primary data center."
                },
                "senderId": {
                    "type": "string",
                    "example": "null",
                    "description": "Uniquely identifies an Operation consumer/Partner."
                },
                "sessionId": {
                    "type": "string",
                    "example": "null",
                    "description": "A value populated by the sender used to track the transactions that occur during a session, a long-lasting interaction, managed by the sender. GUID generated by partner. Within one session (sessionid) for Rep/system can serve  multiple customers(interactionid) and in a given customer interation ??? multiple API calls (activityid) will be made to serve the customer. 1 sessionid -> many interactionid 1 interactionid -> many activityid"
                },
                "storeId": {
                    "type": "string",
                    "example": "null",
                    "description": "Unique identifier for the retail store location."
                },
                "terminalId": {
                    "type": "string",
                    "example": "null",
                    "description": "Unique identifier for the retail store terminal."
                },
                "tillId": {
                    "type": "string",
                    "example": "null",
                    "description": "Unique identifier for the retail store terminal till number."
                },
                "timestamp": {
                    "type": "string",
                    "format": "date-time",
                    "example": "null",
                    "description": "A timestamp provided by sender to track their workflow."
                },
                "workflowId": {
                    "type": "string",
                    "example": "null",
                    "description": "Name of business purpose/flow, for which the API is being invoked."
                }
            },
            "description": "standard HTTP header elements to be able to deliver them to consumer without interruption",
            "example": "null"
        },
        "Specifications": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string",
                    "example": "null"
                },
                "value": {
                    "type": "string",
                    "example": "null"
                }
            },
            "description": "Name value pair."
        },
        "Event": {
            "type": "object",
            "required": [
                "eventId",
                "eventProducerId",
                "eventTime",
                "eventType",
                "payload"
            ],
            "properties": {
                "auditInfo": {
                    "$ref": "#/definitions/AuditInfo"
                },
                "eventId": {
                    "type": "string",
                    "example": "Event-123",
                    "description": "Unique identifier for the event generated by the event producer. This element will be searchable in DEEP.io UI"
                },
                "eventProducerId": {
                    "type": "string",
                    "example": "Producer1",
                    "description": "Unique identifier for the producer of the event."
                },
                "eventTime": {
                    "type": "string",
                    "format": "date-time",
                    "example": "2017-03-27T16:20:11.108Z",
                    "description": "The date and time that the event occurred."
                },
                "eventType": {
                    "type": "string",
                    "example": "Activation",
                    "description": "Event Name that you want to publish"
                },
                "eventVersion": {
                    "type": "string",
                    "example": "1.0",
                    "description": "version of the event , will be used to distinguish different versions by a consumer"
                },
                "headerReference": {
                    "$ref": "#/definitions/HeaderReference"
                },
                "payload": {
                    "$ref": "#/definitions/Payload"
                },
                "specifications": {
                    "type": "array",
                    "example": "null",
                    "description": "added for future reference so can ignore.",
                    "items": {
                        "$ref": "#/definitions/Specifications"
                    }
                }
            }
        },
        "Payload": {
            "type": "object",
            "description": "Event specific information that can be modeled using APIv2 or inline elements.",
            "properties": {
                "ProvisionNonTmoWorker": {
                    "$ref": "#/definitions/ProvisionNonTmoWorker"
                }
            }
        },
        "ProvisionNonTmoWorker": {
            "type": "object",
            "description": "Provisioning status update for User",
            "properties": {
                "sourceID": {
                    "type": "string",
                    "description": "Reference ID at the originating source (ie WorkDay, FG, etc)"
                },
        "UserDetails": {
            "type": "object",
            "description": "Details of User",
            "properties": {
      
                "workerId": {
                    "type": "string",
                    "description": "Uniquely identifies the Fieldglass Worker ID"
                },
                "securityId": {
                    "type": "string",
                    "description": "Fieldglass Worker Security ID"
                },
                "workerStatus": {
                    "type": "string",
                    "description": "Worker status ACTIVE OR CLOSED"
                },
                "firstName": {
                    "type": "string",
                    "description": "Worker First name"
                },
                "lastName": {
                    "type": "string",
                    "description": "Worker Last Name"
                },
                "tmobileEmail": {
                    "type": "string",
                    "description": "Worker TMobile EMail ID"
                },
                "businessUnitCode": {
                    "type": "string",
                    "description": "Business Unit Code"
                },
                "businessUnitName": {
                    "type": "string",
                    "description": "Business Unit Name"
                },
                "vendorName": {
                    "type": "string",
                    "description": "Fieldglass Supplier name"
                },
                "vendorEmail": {
                    "type": "string",
                    "description": "Supplier Primary Contact Person Email"
                },
                "buyerCode": {
                    "type": "string",
                    "description": "Tmobile Buyer code default to TMUS"
                },
                "costCenterName": {
                    "type": "string",
                    "description": "Primary Cost Center Name or WBS Element for Worker"
                },
                "costCenterCode": {
                    "type": "string",
                    "description": "Primary Cost Center Code or WBS Element for Worker"
                },
                "startDate": {
                    "type": "string",
                    "description": "Worker StartDate MM/DD/YYYY"
                },
				 "endDate": {
                    "type": "string",
                    "description": "Worker EndDate MM/DD/YYYY"
                },
                "locationCode": {
                    "type": "string",
                    "description": "Fieldglass Site Code"
                },
                "addressLine1": {
                    "type": "string",
                    "description": "Fieldglass Site Address Line1"
                },
                "addressLine2": {
                    "type": "string",
                    "description": "Fieldglass Site Address Line2"
                },
                "city": {
                    "type": "string",
                    "description": "Fieldglass Site City"
                },
                "region": {
                    "type": "string",
                    "description": "Fieldglass Site State; Two Characters"
                },
                "zipCode": {
                    "type": "string",
                    "description": "Fieldglass Site Zip Code"
                },
                "country": {
                    "type": "string",
                    "description": "Fieldglass Site Country"
                },
				  "locationName": {
                    "type": "string",
                    "description": "Fieldglass Site Name"
                },
				  "managerPNumber": {
                    "type": "string",
                    "description": " Worker Supervisor Employee ID "
                },
                "managerNTID": {
                    "type": "string",
                    "description": "Worker Supervisor NTID"
                },
                "managerName": {
                    "type": "string",
                    "description": " Worker Supervisor User Name"
                },
                "workerType": {
                    "type": "string",
                    "description": "Worker Type Contingent Profile SOW"
                },
                "standardJobId": {
                    "type": "string",
                    "description": "Worker standard Job ID"
                },
				 "standardJobTitle": {
                    "type": "string",
                    "description": "Worker standard Job Title"
                },
                "tmobileId": {
                    "type": "string",
                    "description": "worker employee Id"
                },
				 "tmobileNTID": {
                    "type": "string",
                    "description": "worker employee NTID"
                },
                "clientSystemAccess": {
                    "type": "string",
                    "description": "Client System access value yes or no"
                },
                "securityBadgeAccess": {
                    "type": "string",
                    "description": "securityBadge request value yes or no"
                },
                "rehire": {
                    "type": "string",
                    "description": "rehire determination flag value yes or no"
                }
              
                }
      
            }
        }
    }
}
}