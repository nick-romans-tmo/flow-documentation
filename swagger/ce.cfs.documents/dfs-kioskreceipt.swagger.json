{
    "swagger": "2.0",
    "info": {
        "title": "dfs-kioskreceipt",
        "description": "This service support the use of the legalDocumentsMessage resource. Object for Document generation, presentment, saving and retrieval ",
        "version": "1.0.0",
        "contact": {
            "name": "Aravinda Grandhi",
            "email": "Aravinda.Grandhi1@t-mobile.com"
        },
        "x-createdBy": {
            "dateCreated": "Wed Nov 27 21:01:59 2019",
            "createdBy": "AGrandh1",
            "application": "Recite",
            "appVersion": "1.2.1.3071",
            "documentId": "fc97c3bc-06da-49bb-b04a-7525de57b2f2",
            "status": "Conceptual - Initial",
            "businessCapabilityModel": "5.0",
            "classification": "17.10.1 Records Mgmt. Policy and Governance",
            "profile": "Core Business Capability Service",
            "serviceLayer": "Resource - Other"
        }
    },
    "x-servers": [
        {
            "url": "http://127.0.0.1/",
            "description": "Test Server 1"
        }
    ],
    "tags": [
        {
            "name": "kioskReceipt",
            "description": "API for KIOSK Receipt Generation and Storage"
        }
    ],
    "host": "dev01.api.t-mobile.com",
    "basePath": "/legal/v1/documents/receipt-kiosk",
    "schemes": [
        "https"
    ],
    "definitions": {
        "Address": {
            "description": "The physical address of the entity.",
            "type": "object",
            "properties": {
                "addressLine1": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 128,
                    "pattern": "^[\\S ]*$",
                    "description": "The first free form line, expressed as text, of an address"
                },
                "addressLine2": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 128,
                    "pattern": "^[\\S ]*$",
                    "description": "The second free form line, expressed as text, of an address"
                },
                "cityName": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 256,
                    "pattern": "^[a-zA-Z0-9\\-,\\.() ]*$",
                    "description": "The name, expressed as text, of the city, town or village of this address"
                },
                "stateName": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 256,
                    "pattern": "^[a-zA-Z0-9\\-,\\.() ]*$",
                    "description": "Name of the state in which  the city falls, where applicable"
                },
                "postalCode": {
                    "type": "string",
                    "maxLength": 10,
                    "pattern": "^[a-zA-Z0-9 \\-]*$",
                    "description": "A code used by the postal service for routing letters and packages."
                }
            }
        },
        "DocumentCustomer": {
            "type": "object",
            "description": "Details concerning the person or organization that is a customer, as defined by the Document domain",
            "properties": {
                "fullName": {
                    "type": "string",
                    "maxLength": 256,
                    "pattern": "^[a-zA-Z][a-zA-Z\\d\\-\\.,() ]*$",
                    "description": "Customers Full Name"
                },
                "firstName": {
                    "type": "string",
                    "maxLength": 256,
                    "pattern": "^[a-zA-Z][a-zA-Z\\d\\-\\.,() ]*$",
                    "description": "Customers First Name"
                },
                "lastName": {
                    "type": "string",
                    "maxLength": 256,
                    "pattern": "^[a-zA-Z][a-zA-Z\\d\\-\\.,() ]*$",
                    "description": "Customers Last Name"
                },
                "phoneCommunications": {
                    "$ref": "#/definitions/PhoneCommunication"
                },
                "emailCommunications": {
                    "$ref": "#/definitions/EmailCommunication"
                }
            }
        },
        "Documents": {
            "description": "Document Info",
            "type": "object",
            "required": [
                "documentType",
                "templateState",
                "templateLanguage",
                "documentsubType"
            ],
            "properties": {
                "documentType": {
                    "type": "string",
                    "enum": [
                        "ACTIVATION_AGREEMENT",
                        "SERVICE_AGREEMENT",
                        "EIP_AGREEMENT",
                        "EIP_CONFIRMATION",
                        "DRP_AGREEMENT",
                        "TRANSACTION_CONFIRMATION",
                        "ESIGN_TERMS_AND_CONDITIONS",
                        "SIGNATURE",
                        "SERVICE_TERMS",
                        "SIM_CHANGE_CONFIRMATION",
                        "SUSPENSION_CONFIRMATION",
                        "ADD_REMOVE_FEATURE",
                        "RECEIPT",
                        "CASEMANAGEMENT",
                        "LEASE_MODIFICATION_CONFIRMATION",
                        "LOAN_MODIFICATION_CONFIRMATION",
                        "LEASE_BAN_CANCEL_NOTICE"
                    ],
                    "description": "Type of Document; Eg - Service Agreement, EIP etc;"
                },
                "templateState": {
                    "type": "string",
                    "description": "Two letter state code"
                },
                "templateLanguage": {
                    "type": "string",
                    "description": "Possible Values; ENGLISH / SPANISH"
                },
                "documentsubType": {
                    "type": "string",
                    "description": "BILL_PAY_KIOSK_RECEIPT"
                }
            }
        },
        "DocumentsEmbeddedData": {
            "description": "Element holding data embedded directly in the BOD instance",
            "type": "object",
            "properties": {
                "binaryContent": {
                    "type": "string",
                    "format": "byte",
                    "description": "A set of finite-length sequences of binary octets"
                }
            }
        },
        "EmailCommunication": {
            "type": "object",
            "description": "Communication guidelines for email.",
            "properties": {
                "emailAddress": {
                    "type": "string",
                    "minLength": 6,
                    "maxLength": 256,
                    "pattern": "^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$",
                    "description": "URI in the case of email communication"
                }
            }
        },
        "Error": {
            "description": "As defined in http://api-standards.apps.px-npe01.cf.t-mobile.com/http/status-codes/error-response-format/",
            "type": "object",
            "required": [
                "code",
                "userMessage"
            ],
            "properties": {
                "code": {
                    "type": "string",
                    "pattern": "^[\\S ]+$",
                    "description": "A succinct, domain-specific, human-readable text string to identify the type of error for the given status code",
                    "example": "ProductNotFound"
                },
                "userMessage": {
                    "type": "string",
                    "pattern": "^[\\S ]+$",
                    "description": "A human-readable message describing the error.",
                    "example": "Failed to find product."
                },
                "systemMessage": {
                    "type": "string",
                    "description": "Text that provides a more detailed technical explanation of the error",
                    "example": "PRODUCT_NOT_FOUND"
                },
                "detailLink": {
                    "type": "string",
                    "description": "link to custom information providing greater detail on error or errors",
                    "example": "http://aaa.bbb.ccc/"
                }
            }
        },
        "KioskReceiptRequest": {
            "description": "Request for Receipt Generation and storage",
            "type": "object",
            "properties": {
                "stores": {
                    "$ref": "#/definitions/Store"
                },
                "customers": {
                    "$ref": "#/definitions/DocumentCustomer"
                },
                "accounts": {
                    "$ref": "#/definitions/KioskReceiptRequestAccounts"
                },
                "documents": {
                    "$ref": "#/definitions/Documents"
                },
                "contentRequired": {
                    "type": "boolean",
                    "description": "If true , document byte array will be provided else document id with succes response"
                },
                "payments": {
                    "$ref": "#/definitions/Payments"
                },
                "lineItem": {
                    "$ref": "#/definitions/LineItem"
                },
                "taxes": {
                    "$ref": "#/definitions/Tax"
                }
            }
        },
        "KioskReceiptRequestAccounts": {
            "description": "The entity that captures all aspects of a customers account",
            "type": "object",
            "required": [
                "billingType",
                "transactionDateTime",
                "transactionId"
            ],
            "properties": {
                "accountNumber": {
                    "type": "string",
                    "description": "The financial account number that identifies the account."
                },
                "billingType": {
                    "type": "string",
                    "enum": [
                        "Prepaid",
                        "Postpaid",
                        "Wholesale"
                    ],
                    "description": "A shorthand identifying name for the type of billing settlement: Prepaid, Postpaid."
                },
                "transactionDateTime": {
                    "type": "string",
                    "format": "date-time",
                    "description": "transaction Date Time"
                },
                "transactionId": {
                    "type": "string",
                    "description": "Transaction Number"
                }
            }
        },
        "KioskReceiptResponse": {
            "description": "Response of the Generated Receipt",
            "type": "object",
            "properties": {
                "documents": {
                    "$ref": "#/definitions/KioskReceiptResponseDocuments"
                }
            }
        },
        "KioskReceiptResponseDocuments": {
            "description": "Response of document generation",
            "type": "object",
            "properties": {
                "documentId": {
                    "type": "string",
                    "description": "Document ingestion id"
                },
                "documentSubtype": {
                    "type": "string",
                    "description": "Document template Sub Type"
                },
                "documentType": {
                    "type": "string",
                    "description": "Document Type of the  template"
                },
                "embeddedData": {
                    "type": "array",
                    "description": "Element holding data embedded directly in the BOD instance",
                    "items": {
                        "$ref": "#/definitions/DocumentsEmbeddedData"
                    }
                }
            }
        },
        "LineItem": {
            "description": "Line level Item details",
            "type": "object",
            "properties": {
                "sku": {
                    "type": "string",
                    "description": "SKU"
                },
                "description": {
                    "type": "string",
                    "description": "Description of SKU number"
                }
            }
        },
        "Location": {
            "type": "object",
            "description": "The location of an entity such as a store or warehouse, including address and phone numbers.",
            "properties": {
                "address": {
                    "$ref": "#/definitions/Address"
                },
                "phoneCommunications": {
                    "$ref": "#/definitions/PhoneCommunication"
                }
            }
        },
        "Payments": {
            "description": "Payment related Info.",
            "type": "object",
            "properties": {
                "paymentMethodCode": {
                    "type": "string",
                    "enum": [
                        "Cash",
                        "Credit",
                        "Debit",
                        "Check",
                        "eCheck",
                        "EBT",
                        "Prepaid",
                        "DelayPay",
                        "PLDebit",
                        "DebitSignature",
                        "PayPal",
                        "GiftCard",
                        "GPRCard",
                        "Voucher",
                        "DeductionFromAccountBalance",
                        "Hybrid"
                    ],
                    "description": "Specifies the Payment Type. Example values can be: Card, Cash, Direct Debit, Draft, Letter of Credit, GIRO, Check ( Send me a bill)"
                },
                "paymentTransactionType": {
                    "type": "string",
                    "enum": [
                        "PAYMENT",
                        "REFUND",
                        "DEPOSIT",
                        "VOUCHER_REFILL",
                        "INVOICE_TRANSFER",
                        "PAYMENT_TRANSFER",
                        "CLEARING",
                        "WRITE_OFF",
                        "OUTPAYMENT",
                        "TRANSFERFUND",
                        "DEPOSIT2FUND",
                        "OUTDEPOSIT",
                        "CLR-FPA",
                        "BACKOUT",
                        "TRANSPARTPAYMENT"
                    ],
                    "description": "Information on Transaction Type"
                },
                "cardNumber": {
                    "type": "string",
                    "description": "Payment card number"
                },
                "typeCode": {
                    "type": "string",
                    "enum": [
                        "MASTERCARD",
                        "VISA",
                        "AMERICANEXPRESS",
                        "DISCOVER"
                    ],
                    "description": "Type of Credit Card. Examples are Visa, MasterCard etc"
                },
                "checkNumber": {
                    "type": "string",
                    "description": "The number uniquely identifying the check within the the set of checks written against the customer&apos;s bank account."
                },
                "paymentReferenceNumber": {
                    "type": "string",
                    "description": "Payment Reference Number"
                },
                "cardHolderName": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 256,
                    "pattern": "^[a-zA-Z0-9\\-,\\.() ]*$",
                    "description": "Name of the Card Holder"
                },
                "amount": {
                    "type": "number",
                    "format": "double",
                    "description": "Amount that the payment method authorizes or collected in the mode of cash/Check"
                },
                "authorizationResponseCode": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 50,
                    "pattern": "^[a-zA-Z0-9_\\-]*$",
                    "description": "Code that indicates the result of the authorization request. Code values will vary depending on the payment gateway vendor."
                },
                "terminalEntryMode": {
                    "type": "string",
                    "description": "Indicates how the account number was entered on the transaction at Point of Sale (POS)."
                },
                "cashReceivedAmount": {
                    "type": "number",
                    "format": "double",
                    "description": "Cash Received Customer"
                },
                "changeDueAmount": {
                    "type": "number",
                    "format": "double",
                    "description": "Change to be given back to customer by Sales Rep"
                },
                "applicationId": {
                    "type": "string",
                    "description": "Identifier of the application"
                },
                "applicationPrefName": {
                    "type": "string",
                    "description": "Application Preferred Name"
                },
                "verificationMethod": {
                    "type": "string",
                    "description": "PIN Verified is value when card is used"
                }
            }
        },
        "PhoneCommunication": {
            "type": "object",
            "description": "Communication guidelines for telephone.",
            "required": [
                "phoneNumber"
            ],
            "properties": {
                "phoneType": {
                    "type": "string",
                    "enum": [
                        "LANDLINE",
                        "MOBILE",
                        "FAX",
                        "SATELLITE"
                    ],
                    "description": "Indicates the type of phone: LANDLINE, MOBILE, FAX, SATELLITE."
                },
                "phoneNumber": {
                    "type": "string",
                    "minLength": 10,
                    "maxLength": 15,
                    "pattern": "^[\\d\\.\\-()+]*$",
                    "description": "The complete number without separation between country code, area code etc"
                }
            }
        },
        "Store": {
            "type": "object",
            "description": "A store is a location where customers can go to perform retail transactions. It may be operated by the company directly, or by an authorized dealer or partner.",
            "properties": {
                "storeId": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 50,
                    "pattern": "^[a-zA-Z\\d\\-]*$",
                    "description": "The unique  identifier of the store, as defined by the T-Mobile system of record."
                },
                "location": {
                    "$ref": "#/definitions/Location"
                }
            }
        },
        "Tax": {
            "type": "object",
            "description": "A tax is a mandatory financial charge or some other type of levy imposed upon a taxpayer (an individual or other legal entity) by a governmental organization in order to fund various public expenditures.",
            "properties": {
                "amount": {
                    "type": "number",
                    "format": "double",
                    "description": "Tax Amount in the transactional currency"
                },
                "typeCode": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 50,
                    "pattern": "^[a-zA-Z0-9_\\-]*$",
                    "description": "Identifies the type of Tax. Could be SALES, VAT, USE, RESORT, PROPERTY, INCOME, SERVICE, E911,or Flat Rate."
                },
                "taxRate": {
                    "$ref": "#/definitions/TaxRate"
                },
                "taxable": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 50,
                    "pattern": "^[a-zA-Z0-9_\\-]*$",
                    "description": "Indicates whether the item was determined to be taxable with respect to this particular tax type."
                }
            }
        },
        "TaxRate": {
            "description": "Tax rate expressed as a decimal per base value, measured in the given units.",
            "type": "object",
            "properties": {
                "rate": {
                    "type": "number",
                    "format": "double",
                    "description": "Numeric information that is assigned or is determined by calculation, counting, or sequencing. It does not require a unit of quantity or unit of measure. Expressed as a quantity per quantity."
                }
            }
        }
    },
    "paths": {
        "/": {
            "post": {
                "tags": [
                    "kioskReceipt"
                ],
                "x-api-pattern": "CreateInCollection",
                "summary": "A Post for the KIOSK Receipt Generation and Storage",
                "description": "API for KIOSK Receipt Generation and Storage",
                "operationId": "kioskReceipt",
                "consumes": [
                    "application/json",
                    "application/xml"
                ],
                "produces": [
                    "application/json",
                    "application/xml"
                ],
                "parameters": [
                    {
                        "name": "kioskReceiptRequest",
                        "in": "body",
                        "description": "Request for Receipt Generation and storage",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/KioskReceiptRequest"
                        }
                    },
                    {
                        "name": "Accept",
                        "in": "header",
                        "description": "Preferred content type _ default to application/json",
                        "x-example": "application/json",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[ \\S]+$"
                    },
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "The HTTP Authorization request header contains the credentials to authenticate a user agent with a server.",
                        "x-example": "Bearer mF_9.B5f-4.1JqM",
                        "required": true,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S ]+$"
                    },
                    {
                        "name": "Content-Type",
                        "in": "header",
                        "description": "The MIME type of this content",
                        "x-example": "text/xml",
                        "required": true,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[ \\S]+$"
                    },
                    {
                        "name": "application-id",
                        "in": "header",
                        "description": "Identifies the application, system, or tool that is being used to perform the transaction related to this request. Sample values:ACUI, ESERVICE, REBELLION, WARP",
                        "x-example": "REBELLION",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "channel-id",
                        "in": "header",
                        "description": "Categorization of access or business stream",
                        "x-example": "IVR",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "sender-id",
                        "in": "header",
                        "description": "The related business partner or T-Mobile organization",
                        "x-example": "QVXP",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "timestamp",
                        "in": "header",
                        "description": "ms since 1/1/70 adjusted to GMT",
                        "x-example": "1524599536",
                        "required": false,
                        "type": "integer",
                        "format": "int64",
                        "minimum": 0,
                        "maximum": 999999999999,
                        "pattern": "^[\\d]*$"
                    },
                    {
                        "name": "x-auth-originator",
                        "in": "header",
                        "description": "API chain initiating callers access token",
                        "x-example": "c34e7acd-384b-4c22-8b02-ba3963682508",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "x-authorization",
                        "in": "header",
                        "description": "Contains Proof of Possession token generated by API caller",
                        "x-example": "c34e7acd-384b-4c22-8b02-ba3963682508",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S ]*$"
                    }
                ],
                "responses": {
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "400",
                                "userMessage": "Bad Request",
                                "systemMessage": "Bad Request",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "401": {
                        "description": "Unauthorized",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "401",
                                "userMessage": "Unauthorized",
                                "systemMessage": "Unauthorized",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "403": {
                        "description": "Client access denied",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "403",
                                "userMessage": "Client access denied",
                                "systemMessage": "Client access denied",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "404": {
                        "description": "Resource not found",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "404",
                                "userMessage": "Resource not found",
                                "systemMessage": "Resource not found",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "405": {
                        "description": "Method Not Allowed",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "405",
                                "userMessage": "Method Not Allowed",
                                "systemMessage": "Method Not Allowed",
                                "detailLink": "http://www.tmus.com"
                            }
                        },
                        "headers": {
                            "Allow": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 256,
                                "pattern": "^[\\S]*$",
                                "description": "list of supported methods for URI",
                                "x-example": "GET"
                            },
                            "service-transaction-id": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 256,
                                "pattern": "^[\\S]*$",
                                "description": "Request ID echoed back by server",
                                "x-example": "23409209723"
                            }
                        }
                    },
                    "406": {
                        "description": "Mismatching data format",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "406",
                                "userMessage": "Mismatching data format",
                                "systemMessage": "Mismatching data format",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "409": {
                        "description": "Invalid data",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "409",
                                "userMessage": "Invalid data",
                                "systemMessage": "Invalid data",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "415": {
                        "description": "Unsupported Media Type",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "415",
                                "userMessage": "Unsupported Media Type",
                                "systemMessage": "Unsupported Media Type",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "500": {
                        "description": "System Error",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "500",
                                "userMessage": "System Error",
                                "systemMessage": "System Error",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "503": {
                        "description": "Service unavailable",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "503",
                                "userMessage": "Service unavailable",
                                "systemMessage": "Service unavailable",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "200": {
                        "description": "Success",
                        "schema": {
                            "$ref": "#/definitions/KioskReceiptResponse"
                        },
                        "headers": {
                            "Cache-Control": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 256,
                                "pattern": "^[\\S]*$",
                                "description": "Cache policy supported for the resource by the server",
                                "x-example": "no-cache"
                            },
                            "Date": {
                                "type": "string",
                                "format": "date-time",
                                "pattern": "^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), [0-3]{1}[0-9]{1} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [1-3][0-9]{3} [0-2][0-9]:[0-6][0-9]:[0-6][0-9] GMT$",
                                "description": "The Date general HTTP header contains the date and time at which the message was originated.",
                                "x-example": "Mon, 05 Mar 2018 16:38:08 GMT"
                            },
                            "ETag": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 256,
                                "pattern": "^[\\S]*$",
                                "description": "The ETag header specifies the unique entity tag value for the returned resource.",
                                "x-example": "*"
                            },
                            "Location": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 65536,
                                "pattern": "^[\\S]*$",
                                "description": "The Location header specifies the URI of a created resource, or redirects the API consumer to an alternate resource location.",
                                "x-example": "http://api.t-mo.com/order-mgmt/v1/orders/123xyz-0/status"
                            },
                            "Transfer-Encoding": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 15,
                                "pattern": "^[\\S]*$",
                                "description": "The form of encoding used to safely transfer the entity to the user. Currently defined methods are: chunked, compress, deflate, gzip, identity.",
                                "x-example": "chunked"
                            },
                            "service-transaction-id": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 256,
                                "pattern": "^[\\S]*$",
                                "description": "Request ID echoed back by server",
                                "x-example": "23409209723"
                            }
                        }
                    }
                },
                "security": [
                    {
                        "JWT": []
                    }
                ]
            }
        }
    },
    "securityDefinitions": {
        "JWT": {
            "type": "apiKey",
            "description": "JWT is a means of representing claims to be transferred between two parties. The claims in a JWT are encoded as a JSON object that is digitally signed using JSON Web Signature (JWS) and/or encrypted using JSON Web Encryption (JWE).",
            "name": "Authorization",
            "in": "header"
        }
    }
}