{
	"swagger": "2.0",
	"basePath": "/enterprise/payments/v3",
	"schemes": [
		"https"
	],
	"info": {
		"description": "Process Refunds API",
		"version": "1.0.3",
		"title": "ProcessRefundsAPI",
		"contact": {
			"name": "T-Mobile API team",
			"url": "https://www.t-mobile.com/",
			"email": "apiteam@swagger.io"
		},
		"license": {
			"name": "Creative Commons 4.0 International",
			"url": "http://creativecommons.org/licenses/by/4.0/"
		}
	},
	"host": "api.t-mobile.com",
	"x-servers": [{
			"url": "https://dlab01.core.op.api.t-mobile.com/payments/v3",
			"description": "Live Server"
		}, {
			"url": "https://tmobileb-sb01.apigee.net/payments/v3",
			"description": "Sandbox Server"
		}
	],
	"consumes": [
		"application/json"
	],
	"produces": [
		"application/json"
	],
	"paths": {
		"/transactions/refunds": {
			"post": {
				"summary": "ECP & Card Refunds",
				"description": "This API call refunds/reverses the charged amount to customer for purchased item through ECP or CARD mode, based on the use-case.",
				"operationId": "process-refunds",
				"x-api-pattern": "ExecuteFunction",
				"tags": [
					"Process Refunds"
				],
				"security": [{
						"Oauth": []
					}
				],
				"parameters": [{
						"name": "process refunds request",
						"in": "body",
						"schema": {
							"maxItems": 1,
							"$ref": "#/definitions/ProcessRefundsRequest"
						},
						"description": "process refunds request"
					}, {
						"$ref": "#/parameters/AuthorizationParam"
					}, {
						"$ref": "#/parameters/Content-typeParam"
					}, {
						"$ref": "#/parameters/AcceptParam"
					}, {
						"$ref": "#/parameters/applicationuseridParam"
					}, {
						"$ref": "#/parameters/applicationidParam"
					}, {
						"$ref": "#/parameters/channelidParam"
					}, {
						"$ref": "#/parameters/senderidParam"
					}, {
						"$ref": "#/parameters/activityidParam"
					}, {
						"$ref": "#/parameters/workflowidParam"
					}, {
						"$ref": "#/parameters/interactionidParam"
					}, {
						"$ref": "#/parameters/timestampParam"
					}, {
						"$ref": "#/parameters/storeidParam"
					}, {
						"$ref": "#/parameters/terminalidParam"
					}, {
						"$ref": "#/parameters/tillidParam"
					}, {
						"$ref": "#/parameters/dealercodeParam"
					}, {
						"$ref": "#/parameters/segmentationidParam"
					}, {
						"$ref": "#/parameters/authcustomeridParam"
					}, {
						"$ref": "#/parameters/authfinancialaccountidParam"
					}, {
						"$ref": "#/parameters/authlineofserviceidParam"
					}, {
						"$ref": "#/parameters/servicetransactionidParam"
					}, {
						"$ref": "#/parameters/Access-Control-Request-HeadersParam"
					}, {
						"$ref": "#/parameters/Access-Control-Request-MethodParam"
					}, {
						"$ref": "#/parameters/If-MatchParam"
					}, {
						"$ref": "#/parameters/If-Modified-SinceParam"
					}, {
						"$ref": "#/parameters/If-None-MatchParam"
					}, {
						"$ref": "#/parameters/OriginParam"
					}
				],
				"responses": {
					"200": {
						"description": "OK",
						"schema": {
							"maxItems": 1,
							"$ref": "#/definitions/ProcessRefundsResponse"
						},
						"examples": {
							"ECPRefundResponse": {
								"reason": {
									"reasonCode": "8216"
								},
								"statusBody": {},
								"status": {
									"statusCode": "A"
								},
								"payment": {
									"messages": [{
											"applicationName": "DPS",
											"uiShortCode": "A",
											"uiAction": "Payment Successfully Refunded"
										}
									],
									"authorizedAmount": 20,
									"paymentId": "40778191066",
									"merchantId": "261781"
								},
								"paymentTransactionResponse": {
									"statusMessage": "Payment Successfully Refunded"
								}
							},
							"CardRefundResponse": {
								"reason": {
									"reasonCode": "8216"
								},
								"statusBody": {
									"reasonDescription": "Payment Successfully Refunded"
								},
								"status": {
									"statusCode": "A"
								},
								"payment": {
									"messages": [{
											"applicationName": "LEGACY",
											"uiMessage": "Payment Successfully Refunded",
											"uiShortCode": "A",
											"uiAction": "100-Approved"
										}
									],
									"authorizedAmount": 98,
									"paymentId": "1092345",
									"merchantId": "068466",
									"reversalAction": "2"
								},
								"paymentTransactionResponse": {
									"statusMessage": "100-Approved"
								}
							}
						},
						"headers": {
							"Access-Control-Allow-Headers": {
								"description": "The Access-Control-Allow-Headers header indicates, as part of a pre-flight request which headers can be used on the actual request.",
								"type": "string",
								"x-example": [
									"clientServiceID",
									"servicetransactionid"
								]
							},
							"Access-Control-Allow-Methods": {
								"description": "The Access-Control-Allow-Methods header indicates, as part of a pre-flight request which methods can be used on the actual request.",
								"type": "string",
								"x-example": "POST"
							},
							"Access-Control-Allow-Origin": {
								"description": "The Access-Control-Allow-Origin header indicates whether a resource can be shared by returning the value of the Origin request header or \"*\" in the response.",
								"type": "string"
							},
							"Cache-Control": {
								"description": "no-cache",
								"type": "string"
							},
							"Content-Length": {
								"description": "The Content-Length header specifies the actual length of the returned payload.",
								"type": "string",
								"x-example": "501"
							},
							"Content-Type": {
								"description": "The Content-Length header specifies the actual length of the returned payload.",
								"type": "string",
								"x-example": "application/json;charset=UTF-8"
							},
							"ETag": {
								"description": "The ETag header specifies the unique entity tag value for the returned resource.",
								"type": "string"
							},
							"Expires": {
								"description": "The Expires header gives the date-time after which the response is considered stale.",
								"type": "string"
							},
							"Location": {
								"description": "The Location header specifies the URI of a created resource, or redirects the API consumer to an alternate resource location.",
								"type": "string"
							},
							"servicetransactionid": {
								"description": "Internal identifier for transaction tracking an individual transaction/API request within API platform. Don't receive/don't send to downstream",
								"type": "string",
								"x-example": "cbf30c5a04ae11e98eb2f2801f1b9fd1"
							}
						}
					},
					"400": {
						"description": "Bad Request",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"401": {
						"description": "Unauthorized",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"403": {
						"description": "Forbidden",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"404": {
						"description": "Not Found",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"405": {
						"description": "Method Not Allowed",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"406": {
						"description": "Not Acceptable",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"409": {
						"description": "Conflict",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"415": {
						"description": "Unsupported Media Type",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"500": {
						"description": "Internal Server Error",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"501": {
						"description": "Not Implemented",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"502": {
						"description": "Bad Gateway",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					},
					"503": {
						"description": "Service Unavailable",
						"schema": {
							"$ref": "#/definitions/errors"
						}
					}
				}
			}
		}
	},
	"definitions": {
		"ProcessRefundsRequest": {
			"type": "object",
			"required": [
				"payment"
			],
			"properties": {
				"customerReference": {
					"$ref": "#/definitions/CustomerReference"
				},
				"salesContext": {
					"$ref": "#/definitions/SalesContext"
				},
				"applicationId": {
					"type": "string"
				},
				"payment": {
					"$ref": "#/definitions/Payment"
				}
			}
		},
		"Payment": {
			"type": "object",
			"required": [
				"orderNumber",
				"paymentId"
			],
			"properties": {
				"orderNumber": {
					"description": "The order number this payment is associated with.",
					"type": "string"
				},
				"messages": {
					"type": "object",
					"items": {
						"$ref": "#/definitions/Messages"
					}
				},
				"businessSegment": {
					"description": "Business Segment: Valid values - Prepaid, Postpaid, Anonymous, B2B, Dealer, NationalRetail",
					"type": "string"
				},
				"productGroup": {
					"enum": [
						"HardGoods",
						"SoftGoods"
					],
					"description": "Product grouping such as HardGoods or SoftGoods",
					"type": "string"
				},
				"businessUnit": {
					"description": "The T-Mobile business unit, such as TMOBILE, METROPCS, SPRINT",
					"type": "string"
				},
				"programCode": {
					"description": "Program identifier such as ZCP, ZCNP, U2POSTCP, U2PRECP, U2PRECNP, U2POSTCNP, U1POSTLEGACY",
					"type": "string"
				},
				"billerCode": {
					"description": "Billing system identifier such as SAMSON, ERICSSON, OFSLL, EIP",
					"type": "string"
				},
				"specifications": {
					"type": "array",
					"items": {
						"$ref": "#/definitions/Specifications"
					}
				},
				"chargeAmount": {
					"description": "The total amount of the transaction inclusive of all additional amounts.",
					"type": "number",
					"format": "double"
				},
				"accountNumber": {
					"description": "The financial account number that identifies the account. Either orderNumber or account number should be passed.",
					"type": "string"
				},
				"authorizedAmount": {
					"description": "Amount + tax amount.",
					"type": "number",
					"format": "double"
				},
				"authorizationId": {
					"description": "Identifier or code of the transaction authorization.",
					"type": "string"
				},
				"clientId": {
					"description": "Front End Application client ID name. Example POSR, POS_PLDBT etc.",
					"type": "string"
				},
				"operationType": {
					"enum": [
						"REFUND"
					],
					"description": "Valid Value is Refund.",
					"type": "string"
				},
				"parentOrderId": {
					"description": "Parent order identifier of the front end workflow. This is parent sales order id.",
					"type": "string"
				},
				"taxAmount": {
					"description": "Amount of tax included in the payment transaction.",
					"type": "number",
					"format": "double"
				},
				"orderTypes": {
					"description": "List of order type, such as EQUIPMENT UPGRADE, ACCESSORY PURCHASE, AALREFUND",
					"type": "string"
				},
				"merchantId": {
					"description": "Merchant Id.",
					"type": "string"
				},
				"managerloginid": {
					"description": "Manager's user identifier.",
					"type": "string"
				},
				"paymentId": {
					"description": "PaymentId on which refund will happen, If this is the request. Refund's PaymentId if this is the response",
					"type": "string"
				},
				"reversalReason": {
					"enum": [
						"Timeout",
						"FraudRuleVoid",
						"CustomerInitiated",
						"CVVOrAVSFailure"
					],
					"description": "Reason of Reversal.",
					"type": "string"
				},
				"reversalAction": {
					"description": "Reversal action.",
					"type": "string"
				}
			}
		},
		"Specifications": {
			"type": "object",
			"properties": {
				"name": {
					"description": "key",
					"type": "string"
				},
				"value": {
					"description": "value",
					"type": "string"
				}
			}
		},
		"SalesContext": {
			"type": "object",
			"properties": {
				"operatorId": {
					"description": "To indicate the identifier of user/operator.",
					"type": "string"
				}
			}
		},
		"CustomerReference": {
			"type": "object",
			"properties": {
				"commonCustomerId": {
					"description": "Universal identifier of the customer, as defined in Customer Hub.",
					"type": "string"
				},
				"customerId": {
					"description": "Uniquely identifies the customer billing account(BAN)                1. Required if you create/search/update/delete a record based on BAN",
					"type": "string"
				}
			}
		},
		"Reason": {
			"type": "object",
			"properties": {
				"reasonCode": {
					"description": "Status reason code",
					"type": "string"
				}
			}
		},
		"StatusBody": {
			"type": "object",
			"properties": {
				"reasonDescription": {
					"description": "Status reason description",
					"type": "string"
				}
			}
		},
		"Status": {
			"type": "object",
			"properties": {
				"statusCode": {
					"description": "Status code",
					"type": "string"
				}
			}
		},
		"errors": {
			"description": "A collection of errors",
			"type": "object",
			"items": {
				"$ref": "#/definitions/ResponseError"
			}
		},
		"ProcessRefundsResponse": {
			"type": "object",
			"properties": {
				"payment": {
					"$ref": "#/definitions/Payment"
				},
				"paymentTransactionResponse": {
					"$ref": "#/definitions/PaymentTransactionResponse"
				},
				"reason": {
					"$ref": "#/definitions/Reason"
				},
				"statusBody": {
					"$ref": "#/definitions/StatusBody"
				},
				"status": {
					"$ref": "#/definitions/Status"
				}
			}
		},
		"Messages": {
			"type": "array",
			"items": {
				"$ref": "#/definitions/UIMessage"
			}
		},
		"UIMessage": {
			"type": "object",
			"properties": {
				"applicationName": {
					"description": "Name of the application, such as Ingenico/EMV Scanner or POS/mPOS.",
					"type": "string"
				},
				"uiMessage": {
					"description": "Message to be displayed on the Device (IPC, Ingenico).",
					"type": "string"
				},
				"uiAction": {
					"description": "Action or action code returned to ui",
					"type": "string"
				},
				"uiShortCode": {
					"description": "Short Code to be returned to UI.",
					"type": "string"
				}
			}
		},
		"PaymentTransactionResponse": {
			"type": "object",
			"properties": {
				"statusMessage": {
					"description": "To indicate the StatusMessage.",
					"type": "string"
				}
			}
		},
		"ResponseError": {
			"description": "Used to pass error information in a response.",
			"type": "object",
			"properties": {
				"code": {
					"description": "Used to pass error codes",
					"type": "string"
				},
				"userMessage": {
					"description": "Use to pass human friendly information to the user.",
					"type": "string"
				},
				"systemMessage": {
					"description": "Used to pass system information.",
					"type": "string"
				}
			}
		}
	},
	"parameters": {
		"AuthorizationParam": {
			"name": "authorization",
			"in": "header",
			"required": true,
			"type": "string"
		},
		"Content-typeParam": {
			"name": "content-type",
			"in": "header",
			"required": true,
			"type": "string"
		},
		"applicationidParam": {
			"name": "applicationid",
			"in": "header",
			"required": true,
			"type": "string"
		},
		"channelidParam": {
			"name": "channelid",
			"in": "header",
			"required": true,
			"type": "string"
		},
		"AcceptParam": {
			"name": "Accept",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"applicationuseridParam": {
			"name": "applicationuserid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"sessionidParam": {
			"name": "sessionid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"senderidParam": {
			"name": "senderid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"interactionidParam": {
			"name": "interactionid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"activityidParam": {
			"name": "activityid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"workflowidParam": {
			"name": "workflowid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"timestampParam": {
			"name": "timestamp",
			"in": "header",
			"required": false,
			"type": "string",
			"format": "date-time"
		},
		"storeidParam": {
			"name": "storeid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"terminalidParam": {
			"name": "terminalid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"tillidParam": {
			"name": "tillid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"dealercodeParam": {
			"name": "dealercode",
			"in": "header",
			"required": false,
			"type": "integer",
			"format": "int32"
		},
		"segmentationidParam": {
			"name": "segmentationid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"authcustomeridParam": {
			"name": "authcustomerid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"authfinancialaccountidParam": {
			"name": "authfinancialaccountid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"authlineofserviceidParam": {
			"name": "authlineofserviceid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"servicetransactionidParam": {
			"name": "servicetransactionid",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"Access-Control-Request-HeadersParam": {
			"name": "Access-Control-Request-Headers",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"Access-Control-Request-MethodParam": {
			"name": "Access-Control-Request-Method",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"If-MatchParam": {
			"name": "If-Match",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"If-Modified-SinceParam": {
			"name": "If-Modified-Since",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"If-None-MatchParam": {
			"name": "If-None-Match",
			"in": "header",
			"required": false,
			"type": "string"
		},
		"OriginParam": {
			"name": "Origin",
			"in": "header",
			"required": false,
			"type": "string"
		}
	},
	"securityDefinitions": {
		"Oauth": {
			"type": "oauth2",
			"description": "OAuth2 Token",
			"tokenUrl": "https://dlab03.core.op.api.t-mobile.com:443/v2/oauth2/accesstoken?grant_type=client_credentials",
			"flow": "application",
			"scopes": {}
		}
	}
}
