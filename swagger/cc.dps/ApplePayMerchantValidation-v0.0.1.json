{
    "swagger": "2.0",
    "info": {
        "title": "ApplePay Merchant Validation",
        "description": "Endpoint used for validationg applepay for WEB session",
        "version": "0.0.1",
        "contact": {
            "name": "EP team",
            "email": "DPSDevelopment@T-Mobile.com"
        }
    },
    "x-servers": [
        {
            "url": "https://applepaymerchantvalidation-qlab03.test.px-npe01.cf.t-mobile.com",
            "description": "Live server."
        }
    ],
    "host": "qlab03.core.op.api.t-mobile.com",
    "schemes": [
        "https"
    ],
    "securityDefinitions": {
        "Bearer": {
            "type": "apiKey",
            "name": "Authorization",
            "in": "header",
            "description": "Use JWT token for authorization."
        }
    },
    "basePath": "/payments/v4/applepay/merchant-validation",
    "tags": [
        {
            "name": "validateMerchant"
        }
    ],
    "paths": {
        "/validate-merchant": {
            "post": {
                "x-api-pattern": "ExecuteFunction",
                "operationId": "validateMerchant",
                "security": [
                    {
                        "Bearer": []
                    }
                ],
                "tags": [
                    "validateMerchant"
                ],
                "summary": "Validates apple pay session for users of a merchant.",
                "description": "Validates apple pay session for users of a merchant.",
                "parameters": [
                    {
                        "$ref": "#/parameters/AuthorizationParam"
                    },
                    {
                        "$ref": "#/parameters/Content-typeParam"
                    },
                    {
                        "$ref": "#/parameters/AcceptParam"
                    },
                    {
                        "$ref": "#/parameters/applicationidParam"
                    },
                    {
                        "$ref": "#/parameters/channelidParam"
                    },
                    {
                        "$ref": "#/parameters/senderidParam"
                    },
                    {
                        "$ref": "#/parameters/sessionidParam"
                    },
                    {
                        "$ref": "#/parameters/interactionidParam"
                    },
                    {
                        "$ref": "#/parameters/activityidParam"
                    },
                    {
                        "in": "body",
                        "description": "Validate merchant request body.",
                        "name": "body",
                        "schema": {
                            "$ref": "#/definitions/ValidateMerchantRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK : success status response code indicates that the request has succeeded.",
                        "schema": {
                            "$ref": "#/definitions/ValidateMerchantResponse"
                        }
                    },
                    "400": {
                        "description": "Bad Request: The server cannot or will not process the request due to something that is perceived to be a client error (e.g., malformed request syntax, invalid request message framing, or deceptive request routing)",
                        "examples": {
                            "application/json": {
                                "code": "2171",
                                "userMessage": "Request Validation Failed",
                                "systemMessage": "Channel Id is required"
                            }
                        },
                        "schema": {
                            "$ref": "#/definitions/Errors"
                        }
                    },
                    "401": {
                        "description": "Unauthorized: The request has not been applied because it lacks valid authentication credentials for the target resource",
                        "examples": {
                            "application/json": {
                                "code": "Security-1004",
                                "userMessage": "Access token expired",
                                "systemMessage": "Access token expired"
                            }
                        },
                        "schema": {
                            "$ref": "#/definitions/Errors"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error: The server encountered an unexpected condition that prevented it from fulfilling the request.",
                        "examples": {
                            "application/json": {
                                "code": "500",
                                "userMessage": "Internal Server error",
                                "systemMessage": "Internal Server error"
                            }
                        },
                        "schema": {
                            "$ref": "#/definitions/Errors"
                        }
                    },
                    "503": {
                        "description": "Service Unavailable: The server is currently unable to handle the request due to a temporary overload or scheduled maintenance, which will likely be alleviated after some delay.",
                        "examples": {
                            "application/json": {
                                "code": "503",
                                "userMessage": "Service Unavailable",
                                "systemMessage": "Service Unavilable"
                            }
                        },
                        "schema": {
                            "$ref": "#/definitions/Errors"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "Errors": {
            "type": "object",
            "properties": {
                "errors": {
                    "type": "array",
                    "description": "Array of Errors returned by the API",
                    "items": {
                        "$ref": "#/definitions/ResponseError"
                    }
                }
            }
        },
        "ResponseError": {
            "description": "Error Object with error information in a response.",
            "type": "object",
            "required": [
                "code",
                "userMessage"
            ],
            "properties": {
                "code": {
                    "description": "Error code for the error occured",
                    "type": "string",
                    "pattern": "^[A-Za-z]*$",
                    "minLength": 1,
                    "maxLength": 100,
                    "example": "2160"
                },
                "userMessage": {
                    "description": "User friendly information to the user indicating what is the error about.",
                    "type": "string",
                    "pattern": "^[A-Za-z]*$",
                    "minLength": 1,
                    "maxLength": 2000,
                    "example": "Invalid token"
                },
                "systemMessage": {
                    "description": "Message from the system indicating the actual error.",
                    "type": "string",
                    "pattern": "^[A-Za-z]*$",
                    "minLength": 1,
                    "maxLength": 2000,
                    "example": "Invalid token"
                }
            }
        },
        "ValidateMerchantRequest": {
            "type": "object",
            "description": "Request to validate apple pay session for users of a merchant.",
            "required": [
                "validationUrl",
                "initiativeContext",
                "displayName",
                "merchantIdentifier"
            ],
            "properties": {
                "validationUrl": {
                    "type": "string",
                    "pattern": "^[\\S]*$",
                    "example": "https://<validation URL>/paymentSession",
                    "description": "Dynamic url for apple merchant validation, to be called by this API."
                },
                "initiativeContext": {
                    "type": "string",
                    "pattern": "^[\\S]*$",
                    "example": "e1.my.t-mobile.com",
                    "description": "Dynamic domain name to be passed to apple for domain validation."
                },
                "displayName": {
                    "type": "string",
                    "pattern": "^[\\S]*$",
                    "example": "TMOBILE",
                    "description": "Display name to be used by the calling application."
                },
                "merchantIdentifier": {
                    "type": "string",
                    "pattern": "^[\\S]*$",
                    "example": "merchant.com.t-mobile.billpay",
                    "description": "Merchant identifier name configured in apple certificate for merhcnat validation."
                }
            }
        },
        "ValidateMerchantResponse": {
            "type": "object",
            "description": "Response containing the opaque merchant session object from apple.",
            "properties": {
                "merchantSession": {
                    "$ref": "#/definitions/MerchantSession"
                }
            }
        },
        "MerchantSession" : {
          "type": "object",
            "description": "Opaque merchant session object.",
            "properties": {
              "merchantSessionIdentifier": {
                    "type": "string",
                    "pattern": "^[\\S]*$",
                    "example": "SSH6CED4EB7C24E487599002A6741CD6F77_916523AAED1343F5BC5815E12BEE9250AFFDC1A17C46B0DE5A943F0F94927C24",
                    "description": "Merchant session identifier from apple merchant session object response."
                },
                "epochTimestamp": {
                    "type": "string",
                    "format": "date-time",
                    "example": "1576804463408",
                    "description": "Epoch timestamp from apple merchant session object response."
                },
                "expiresAt": {
                    "type": "string",
                    "format": "date-time",
                    "example": "1576808063408",
                    "description": "Expire date from apple merchant session object response."
                },
                "nonce": {
                    "type": "string",
                    "pattern": "^[\\S]*$",
                    "example": "325e8529",
                    "description": "Nonce from apple merchant session object response."
                },
                "domainName": {
                    "type": "string",
                    "pattern": "^[\\S]*$",
                    "example": "http://applepaymerchantvalidation-dlab03.dev.px-npe01.cf.t-mobile.com",
                    "description": "Domain name from apple merchant session object response."
                },
                "displayName": {
                    "type": "string",
                    "pattern": "^[\\S]*$",
                    "example": "TMOBILE",
                    "description": "Display name from apple merchant session object response."
                },
                "signature": {
                    "type": "string",
                    "pattern": "^[\\S]*$",
                    "example": "308006092a864886f70d010702a0803080020101310f300d06096086480165030402010500308006092a864886f70d0107010000a080308203e63082038ba00302010202086860f699d9cca70f300a06082a8648ce3d040302307a312e302c06035504030c254170706c65204170706c69636174696f6e20496e746567726174696f6e204341202d20473331263024060355040b0c1d4170706c652043657274696669636174696f6e20417574686f7269747931133011060355040a0c0a4170706c6520496e632e310b3009060355040613025553301e170d3136303630333138313634305a170d3231303630323138313634305a30623128302606035504030c1f6563632d736d702d62726f6b65722d7369676e5f5543342d53414e44424f5831143012060355040b0c0b694f532053797374656d7331133011060355040a0c0a4170706c6520496e632e310b30090603550406130255533059301306072a8648ce3d020106082a8648ce3d030107034200048230fdabc39cf75e202c50d99b4512e637e2a901dd6cb3e0b1cd4b526798f8cf4ebde81a25a8c21e4c33ddce8e2a96c2f6afa1930345c4e87a4426ce951b1295a38202113082020d304506082b0601050507010104393037303506082b060105050730018629687474703a2f2f6f6373702e6170706c652e636f6d2f6f63737030342d6170706c6561696361333032301d0603551d0e041604140224300b9aeeed463197a4a65a299e4271821c45300c0603551d130101ff04023000301f0603551d2304183016801423f249c44f93e4ef27e6c4f6286c3fa2bbfd2e4b3082011d0603551d2004820114308201103082010c06092a864886f7636405013081fe3081c306082b060105050702023081b60c81b352656c69616e6365206f6e207468697320636572746966696361746520627920616e7920706172747920617373756d657320616363657074616e6365206f6620746865207468656e206170706c696361626c65207374616e64617264207465726d7320616e6420636f6e646974696f6e73206f66207573652c20636572746966696361746520706f6c69637920616e642063657274696669636174696f6e2070726163746963652073746174656d656e74732e303606082b06010505070201162a687474703a2f2f7777772e6170706c652e636f6d2f6365727469666963617465617574686f726974792f30340603551d1f042d302b3029a027a0258623687474703a2f2f63726c2e6170706c652e636f6d2f6170706c6561696361332e63726c300e0603551d0f0101ff040403020780300f06092a864886f76364061d04020500300a06082a8648ce3d0403020349003046022100da1c63ae8be5f64f8e11e8656937b9b69c472be93eac3233a167936e4a8d5e83022100bd5afbf869f3c0ca274b2fdde4f717159cb3bd7199b2ca0ff409de659a82b24d308202ee30820275a0030201020208496d2fbf3a98da97300a06082a8648ce3d0403023067311b301906035504030c124170706c6520526f6f74204341202d20473331263024060355040b0c1d4170706c652043657274696669636174696f6e20417574686f7269747931133011060355040a0c0a4170706c6520496e632e310b3009060355040613025553301e170d3134303530363233343633305a170d3239303530363233343633305a307a312e302c06035504030c254170706c65204170706c69636174696f6e20496e746567726174696f6e204341202d20473331263024060355040b0c1d4170706c652043657274696669636174696f6e20417574686f7269747931133011060355040a0c0a4170706c6520496e632e310b30090603550406130255533059301306072a8648ce3d020106082a8648ce3d03010703420004f017118419d76485d51a5e25810776e880a2efde7bae4de08dfc4b93e13356d5665b35ae22d097760d224e7bba08fd7617ce88cb76bb6670bec8e82984ff5445a381f73081f4304606082b06010505070101043a3038303606082b06010505073001862a687474703a2f2f6f6373702e6170706c652e636f6d2f6f63737030342d6170706c65726f6f7463616733301d0603551d0e0416041423f249c44f93e4ef27e6c4f6286c3fa2bbfd2e4b300f0603551d130101ff040530030101ff301f0603551d23041830168014bbb0dea15833889aa48a99debebdebafdacb24ab30370603551d1f0430302e302ca02aa0288626687474703a2f2f63726c2e6170706c652e636f6d2f6170706c65726f6f74636167332e63726c300e0603551d0f0101ff0404030201063010060a2a864886f7636406020e04020500300a06082a8648ce3d040302036700306402303acf7283511699b186fb35c356ca62bff417edd90f754da28ebef19c815e42b789f898f79b599f98d5410d8f9de9c2fe0230322dd54421b0a305776c5df3383b9067fd177c2c216d964fc6726982126f54f87a7d1b99cb9b0989216106990f09921d00003182018b30820187020101308186307a312e302c06035504030c254170706c65204170706c69636174696f6e20496e746567726174696f6e204341202d20473331263024060355040b0c1d4170706c652043657274696669636174696f6e20417574686f7269747931133011060355040a0c0a4170706c6520496e632e310b300906035504061302555302086860f699d9cca70f300d06096086480165030402010500a08195301806092a864886f70d010903310b06092a864886f70d010701301c06092a864886f70d010905310f170d3139313232303031313432335a302a06092a864886f70d010934311d301b300d06096086480165030402010500a10a06082a8648ce3d040302302f06092a864886f70d01090431220420d9e31682a96801d244927598c24e1997af0d8eb4fd7d7fb916fc4247f7fbb4a7300a06082a8648ce3d040302044630440220432271b2c6b3e532bfa5c05006f1af55de250ae66be6cb439857833d2f69870f02201a164094d03c391aec26ca61a1185a15f7d18ebe105dd91c3f9e65a84ea4bb08000000000000",
                    "description": "Signature from apple merchant session object response."
                },
                "merchantIdentifier": {
                    "type": "string",
                    "pattern": "^[\\S]*$",
                    "example": "0050AE12C8BC1EA04D17B488D30665911B9F75D5A060BEDC764644AAA1AF2866",
                    "description": "Merchant identifier from apple merchant session object response."
                }
            }
        }
    },
    "parameters": {
        "AuthorizationParam": {
            "name": "Authorization",
            "in": "header",
            "required": true,
            "description": "Standard HTTP header. JWT access token with the authentication type set as Bearer. e.g Bearer EOVBPSkdKUmlZR3pOckwzODQzU3U2RU46ZFRPN0hiTndBMDJyZ2VBdA==",
            "pattern": "^[a-zA-Z0-9_]=",
            "x-example": "Bearer EOVBPSkdKUmlZR3pOckwzODQzU3U2RU46ZFRPN0hiTndBMDJyZ2VBdA==",
            "type": "string"
        },
        "Content-typeParam": {
            "name": "Content-type",
            "in": "header",
            "required": true,
            "description": "Standard HTTP header. This entity header is used to indicate the media type of the resource. e.g application/json",
            "pattern": "^[a-zA-Z0-9_]/*$",
            "x-example": "application/json",
            "type": "string"
        },
        "AcceptParam": {
            "name": "Accept",
            "in": "header",
            "required": true,
            "description": "Standard HTTP header. Content types that are acceptable for the response (default assumed application/json).e.g application/json",
            "x-example": "application/json",
            "pattern": "^[a-zA-Z0-9_]/*$",
            "type": "string"
        },
        "applicationidParam": {
            "name": "applicationid",
            "in": "header",
            "required": true,
            "description": "Identifies the application, system, or tool that is being used to perform the transaction related to this request. e.g ACUI, ESERVICE, REBELLION etc",
            "x-example": "TMAG",
            "pattern": "^[a-zA-Z]*$ ",
            "type": "string"
        },
        "sessionidParam": {
            "name": "sessionid",
            "in": "header",
            "required": false,
            "description": "A value populated by the sender used to track the transactions that occur during a session, a long-lasting interaction that is maintained by the sender. e.g omuxrmt33mnetsfirxi2sdsfh4j1c2kv",
            "x-example": "omuxrmt33mnetsfirxi2sdsfh4j1c2kv",
            "pattern": "^[a-zA-Z0-9]*$",
            "type": "string"
        },
        "senderidParam": {
            "name": "senderid",
            "in": "header",
            "required": false,
            "description": "Unique Id of the sernder which has initiated the transaction. e.g 23455",
            "x-example": "23455",
            "pattern": "^[0-9]*$",
            "type": "string"
        },
        "channelidParam": {
            "name": "channelid",
            "in": "header",
            "required": true,
            "enum": [
                "Retail",
                "IVR",
                "WEB",
                "CARE"
            ],
            "description": "Identifies the business unit or sales channel for the Request. e.g CARE, WEB, RETAIL, IVR etc.",
            "x-example": "WEB or RETAIL or IVR or CARE",
            "pattern": "^[A-Z]{3,15}$ ",
            "type": "string"
        },
        "interactionidParam": {
            "name": "interactionid",
            "in": "header",
            "required": false,
            "description": "Alphanumeric value represent a common transaction id across all calls made from UI, while completing all business activity needs of a particular customer. e.g 345sr2323fw332234",
            "x-example": "345sr2323fw332234",
            "pattern": "^[a-zA-Z0-9]*$",
            "type": "string"
        },
        "activityidParam": {
            "name": "activityid",
            "in": "header",
            "required": true,
            "description": "Unique id alphanumeric value generated and sent by the partner to identify every the service request uniquely. e.g XyzA:D1s5s1i-X6AFRvfiPBfs-1234",
            "x-example": "XyzA:D1s5s1i-X6AFRvfiPBfs-1234",
            "pattern": "^[a-zA-Z0-9_]*$",
            "type": "string"
        }
    }
}