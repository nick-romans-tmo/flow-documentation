{
  "swagger": "2.0",
  "info": {
    "description": "This API allows DPS consumers to identify cardtype, category and status based on first 6 digits or alias/token of the card.",
    "version": "4.1.2",
    "title": "CardTypeValidationV4",
    "contact": {
      "name": "T-Mobile API team",
      "url": "http://swagger.io",
      "email": "apiteam@swagger.io"
    },
    "license": {
      "name": "Creative Commons 4.0 International",
      "url": "http://creativecommons.org/licenses/by/4.0/"
    }
  },
  "basePath": "/payments/v4/card-type-validation",
  "schemes": [
    "https"
  ],
  "security": [
    {
      "Oauth2": [
        "payment:cardtypevalidation"
      ]
    }
  ],
  "paths": {
    "/": {
      "post": {
        "tags": [
          "CardTypeValidation"
        ],
        "summary": "This API is used to validate and identify T-Mobile supported CardType, Category, Card and CVV length's",
        "description": "",
        "operationId": "cardtTypeCheck",
        "security": [
          {
            "Oauth2": [
              "payment:cardtypevalidation"
            ]
          }
        ],
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "description": "Request Payload for cardType Check",
            "required": true,
            "schema": {
              "maxItems": 1,
              "$ref": "#/definitions/cardTypeValidationRequest"
            }
          },
          {
            "$ref": "#/parameters/AuthorizationParam"
          },
          {
            "$ref": "#/parameters/Content-typeParam"
          },
          {
            "$ref": "#/parameters/AcceptParam"
          },
          {
            "$ref": "#/parameters/applicationuseridParam"
          },
          {
            "$ref": "#/parameters/applicationidParam"
          },
          {
            "$ref": "#/parameters/channelidParam"
          },
          {
            "$ref": "#/parameters/senderidParam"
          },
          {
            "$ref": "#/parameters/sessionidParam"
          },
          {
            "$ref": "#/parameters/interactionidParam"
          },
          {
            "$ref": "#/parameters/activityidParam"
          },
          {
            "$ref": "#/parameters/workflowidParam"
          },
          {
            "$ref": "#/parameters/timestampParam"
          },
          {
            "$ref": "#/parameters/storeidParam"
          },
          {
            "$ref": "#/parameters/terminalidParam"
          },
          {
            "$ref": "#/parameters/tillidParam"
          },
          {
            "$ref": "#/parameters/dealercodeParam"
          },
          {
            "$ref": "#/parameters/segmentationidParam"
          },
          {
            "$ref": "#/parameters/authcustomeridParam"
          },
          {
            "$ref": "#/parameters/authfinancialaccountidParam"
          },
          {
            "$ref": "#/parameters/authlineofserviceidParam"
          },
          {
            "$ref": "#/parameters/servicetransactionidParam"
          },
          {
            "$ref": "#/parameters/Access-Control-Request-HeadersParam"
          },
          {
            "$ref": "#/parameters/Access-Control-Request-MethodParam"
          },
          {
            "$ref": "#/parameters/If-MatchParam"
          },
          {
            "$ref": "#/parameters/If-Modified-SinceParam"
          },
          {
            "$ref": "#/parameters/If-None-MatchParam"
          },
          {
            "$ref": "#/parameters/OriginParam"
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/cardTypeValidationResponse"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "500": {
            "description": "System Error",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          },
          "503": {
            "description": "Service Unavailable",
            "schema": {
              "$ref": "#/definitions/errors"
            }
          }
        }
      }
    }
  },
  "tags": [
    {
      "name": "CardTypeValidation"
    }
  ],
  "securityDefinitions": {
    "Oauth2": {
      "type": "oauth2",
      "scopes": {
        "payment:cardtypevalidation": "Card Type check "
      },
      "flow": "application",
      "tokenUrl": "https://dlab02.core.op.api.t-mobile.com/v1/oauth2/accesstoken?grant_type=client_credentials"
    }
  },
  "definitions": {
    "errors": {
      "type": "object",
	  "description": "Collection of errors",
      "properties": {
        "responseError": {
          "type": "array",
		  "description": "Collection of response error",
          "items": {
            "$ref": "#/definitions/responseError"
          }
        }
      }
    },
    "responseError": {
      "description": "Error information will be sent in a response.",
      "type": "object",
      "properties": {
        "code": {
          "description": "Used to pass error codes",
          "type": "string"
        },
        "userMessage": {
          "description": "Used to pass user friendly message to the user.",
          "type": "string"
        },
        "systemMessage": {
          "description": "Used to pass system information.",
          "type": "string"
        }
      }
    },
    "cardTypeValidationResponse": {
      "type": "object",
      "required": [
        "cardType",
        "status"
      ],
	  
      "properties": {
        "status": {
          "type": "string",
          "description": "Represents whether BIN is supported or not, This field will be displayed in all scenarios. Valid values are Supported or Non-Supported",
          "x-example": "Supported or Non-Supported"
        },
        "cardType": {
          "type": "string",
          "description": "Reresents the type of card. such as VISA,MASTERCARD,AMERICANEXPRESS,DISCOVER,ATMONLY. This field will be displayed in all scenarios",
          "x-example": "VISA,MASTERCARD,AMERICANEXPRESS,DISCOVER"
        },
		"cardLength": {
          "type": "object",
          "description": "Represent lenegth of the card. This field will be displayed only for card not present clients/channels and based on application id/channel id present in request header",
		  
          "properties": {
            "minLength": {
              "description": "minimum length of the card. This field will be displayed only for card not present clients/channels and based on application id/channel id present request header",
              "type": "number",
			  "x-example": "13"
            },
            "maxLength": {
              "description": "maximum length of the card. This field will be displayed only for card not present clients/channels and based on application id/channel id present request header",
              "type": "number",
			  "x-example": "19"
            }
          }
        },
        "securityCodeLength": {
          "type": "number",
          "description": "Security Code Length. This field will be displayed only for card not present clients/channels and based on application id/channel id present request header"
        },        
        "category": {
          "type": "string",
          "description": "This field will be displayed only for card not present clients/channels and based on application id/channel id present request header",
          "x-example": "CREDIT,HYBRID,DEBIT"
        }
      }
    },
      	
    "cardTypeValidationRequest": {
      "type": "object",
      "required": [
        "bin",
        "payment"
      ],
      "properties": {
        "bin": {
          "type": "string",
          "description": "The first 6 to 11 digit of card number. This field is mandatory, if paymentCardAlias is not sent in Request"
        },
        "payment": {
          "$ref": "#/definitions/Payment"
        },
		"salesChannelReference": {
			"type": "object",
			"description": "Sales Channel Reference",
			"properties": {
				"salesChannelId": {
					"description": "This field is to pass originated sales channel. It's a optional field",
					"type": "string"
				}
			}			
		}
      }
    },
	
    "Payment": {
      "type": "object",
      "properties": {
        "cardPresentIndicator": {
          "description": "This indicator is true for CP (card Present) flow and false for CNP (Card Not Present) flow.",
          "type": "boolean"
        },
		"paymentInstrument": {
			"$ref": "#/definitions/PaymentInstrument"
		}
		
      }
    },
	"PaymentInstrument": {
		"type": "object",
		"properties": {
			"paymentCard": {
				"$ref": "#/definitions/PaymentCard"
			}
		}
	},
	"PaymentCard": {
		"type": "object",
		"description": "Payment Card Information",
		"properties": {
			"paymentCardAlias": {
				"description": "Represents payment card alias or tmo token. This field is mandatory, if bin is not sent in Request",
				"type": "string"
			}
		}
	}
  },
  "parameters": {
    "AuthorizationParam": {
      "name": "Authorization",
      "in": "header",
      "required": true,
      "type": "string"
    },
    "Content-typeParam": {
      "name": "Content-type",
      "in": "header",
      "required": true,
      "type": "string"
    },
    "AcceptParam": {
      "name": "Accept",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "applicationuseridParam": {
      "name": "applicationuserid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "applicationidParam": {
      "name": "applicationid",
      "in": "header",
      "required": true,
      "type": "string"
    },
    "sessionidParam": {
      "name": "sessionid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "senderidParam": {
      "name": "senderid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "channelidParam": {
      "name": "channelid",
      "in": "header",
      "required": true,
      "type": "string"
    },
    "interactionidParam": {
      "name": "interactionid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "activityidParam": {
      "name": "activityid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "workflowidParam": {
      "name": "workflowid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "timestampParam": {
      "name": "timestamp",
      "in": "header",
      "required": false,
      "type": "string",
      "format": "date-time"
    },
    "storeidParam": {
      "name": "storeid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "terminalidParam": {
      "name": "terminalid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "tillidParam": {
      "name": "tillid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "dealercodeParam": {
      "name": "dealercode",
      "in": "header",
      "required": false,
      "type": "integer",
      "format": "int"
    },
    "segmentationidParam": {
      "name": "segmentationid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "authcustomeridParam": {
      "name": "authcustomerid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "authfinancialaccountidParam": {
      "name": "authfinancialaccountid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "authlineofserviceidParam": {
      "name": "authlineofserviceid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "servicetransactionidParam": {
      "name": "servicetransactionid",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "Access-Control-Request-HeadersParam": {
      "name": "Access-Control-Request-Headers",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "Access-Control-Request-MethodParam": {
      "name": "Access-Control-Request-Method",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "If-MatchParam": {
      "name": "If-Match",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "If-Modified-SinceParam": {
      "name": "If-Modified-Since",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "If-None-MatchParam": {
      "name": "If-None-Match",
      "in": "header",
      "required": false,
      "type": "string"
    },
    "OriginParam": {
      "name": "Origin",
      "in": "header",
      "required": false,
      "type": "string"
    }
  }
}
