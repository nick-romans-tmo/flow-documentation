{
  "swagger": "2.0",
  "info": {
    "title": "PurchaseOrder",
    "description": "Endpoint used for Enterprise Payments Pi Internal Processes",
    "version": "0.0.1",
    "contact": {
      "name": "EP team",
      "email": "DPSDevelopment@T-Mobile.com"
    }
  },
  "x-servers": [
    {
      "url": "https://purchaseorder-qlab03.test.px-npe01.cf.t-mobile.com",
      "description": "Live server."
    }
  ],
  "host": "qlab03.core.op.api.t-mobile.com",
  "schemes": [
    "https"
  ],
  "securityDefinitions": {
    "Bearer": {
      "type": "apiKey",
      "name": "Authorization",
      "in": "header",
      "description": "Use JWT token for authorization."
    }
  },
  "basePath": "/payments/pi/v4",
  "tags": [
    {
      "name": "PurchaseOrder"
    }
  ],
  "paths": {
    "/create-order": {
      "post": {
        "x-api-pattern": "CreateInCollection",
        "operationId": "createOrder",
        "security": [
          {
            "Bearer": []
          }
        ],
        "tags": [
          "createOrder"
        ],
        "summary": "Stores the cart information",
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "schema": {
              "$ref": "#/definitions/CartDetailsRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Saved"
          },
          "400": {
            "description": "Invalid Request"
          },
          "500": {
            "description": "Failed to store"
          }
        }
      }
    },
    "/update-order": {
      "post": {
        "x-api-pattern": "CreateInCollection",
        "operationId": "updateOrder",
        "security": [
          {
            "Bearer": []
          }
        ],
        "tags": [
          "updateOrder"
        ],
        "summary": "Updates order/customer information",
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "schema": {
              "$ref": "#/definitions/UpdateOrderRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Saved"
          },
          "400": {
            "description": "Invalid Request"
          },
          "500": {
            "description": "Failed to store"
          }
        }
      }
    },
    "/retrieve-order/phone-number/{phone-number}": {
      "get": {
        "x-api-pattern": "QueryCollection",
        "operationId": "retrieveOrderByPhoneNumber",
        "security": [
          {
            "Bearer": []
          }
        ],
        "tags": [
          "retrieveOrderByPhoneNumber"
        ],
        "summary": "Retrieves a order information based on customer's phone number.",
        "parameters": [
          {
            "in": "path",
            "name": "phone-number",
            "required": true,
            "type": "string",
            "x-example": "2144567891"
          },
          {
            "in": "query",
            "name": "order-status",
            "enum": [
              "OPEN",
              "ERROR",
              "COMPLETE",
              "CANCELED"
            ],
            "required": false,
            "type": "string",
            "x-example": "OPEN"
          }
        ],
        "responses": {
          "200": {
            "description": "The cart details, including financing token information if available",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/CartDetailsResponse"
              }
            }
          },
          "400": {
            "description": "Invalid Request"
          },
          "500": {
            "description": "Failed to retrieve details"
          }
        }
      }
    },
    "/retrieve-order/email/{email}": {
      "get": {
        "x-api-pattern": "QueryCollection",
        "operationId": "retrieveOrderByEmail",
        "security": [
          {
            "Bearer": []
          }
        ],
        "tags": [
          "retrieveOrderByEmail"
        ],
        "summary": "Retrieves a order information based on customer's email.",
        "parameters": [
          {
            "in": "path",
            "name": "email",
            "required": true,
            "type": "string",
            "x-example": "a@a.com"
          },
          {
            "in": "query",
            "name": "order-status",
            "enum": [
              "OPEN",
              "ERROR",
              "COMPLETE",
              "CANCELED"
            ],
            "required": false,
            "type": "string",
            "x-example": "OPEN"
          }
        ],
        "responses": {
          "200": {
            "description": "The cart details, including financing token information if available",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/CartDetailsResponse"
              }
            }
          },
          "400": {
            "description": "Invalid Request"
          },
          "500": {
            "description": "Failed to retrieve details"
          }
        }
      }
    },
    "/retrieve-order/order-id/{order-id}": {
      "get": {
        "x-api-pattern": "QueryResource",
        "operationId": "retrieveOrderByOrderId",
        "security": [
          {
            "Bearer": []
          }
        ],
        "tags": [
          "retrieveOrderByOrderId"
        ],
        "summary": "Retrieves a order information based on customer's orderId.",
        "parameters": [
          {
            "in": "path",
            "name": "order-id",
            "required": true,
            "type": "string"
          },
          {
            "in": "query",
            "name": "order-status",
            "enum": [
              "OPEN",
              "ERROR",
              "COMPLETE",
              "CANCELED"
            ],
            "required": false,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "The cart details, including financing token information if available",
            "schema": {
              "$ref": "#/definitions/CartDetailsResponse"
            }
          },
          "400": {
            "description": "Invalid Request"
          },
          "500": {
            "description": "Failed to retrieve details"
          }
        }
      }
    }
  },
  "definitions": {
    "FinancingToken": {
      "type": "object",
      "properties": {
        "checkoutToken": {
          "description": "the token returned from Affirm",
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "5555555555554444"
        },
        "checkoutTokenStatus": {
          "type": "string",
          "description": "the status of the token.",
          "enum": [
            "IN_PROGRESS",
            "APPROVED",
            "EXPIRED"
          ],
          "example": "APPROVED"
        }
      }
    },
    "BillItem": {
      "type": "object",
      "required": [
        "customerFirstName",
        "customerLastName",
        "amount",
        "phoneNumber"
      ],
      "properties": {
        "customerFirstName": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "John",
          "description": "the first name of the customer."
        },
        "customerLastName": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "Doe",
          "description": "the last name of the customer."
        },
        "amount": {
          "type": "number",
          "format": "double",
          "minimum": 0,
          "maximum": 999999999.99,
          "example": "99.99",
          "description": "the bill amount of the customer including taxes."
        },
        "phoneNumber": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "2134567891",
          "description": "the phone number of the customer."
        },
        "accountNumber": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "123456789",
          "description": "the Billing account number (BAN) of the customer."
        }
      }
    },
    "CartItem": {
      "type": "object",
      "required": [
        "sku",
        "itemName",
        "unitCost",
        "quantity",
        "isFinanceOptionSupported"
      ],
      "properties": {
        "sku": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "description": "the sku for the item.",
          "example": "SKU1234"
        },
        "serialNumber": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "description": "the serial number or IMEI for the item.",
          "example": "IMEI1234"
        },
        "itemName": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "description": "the name of the item.",
          "example": "IPhone X"
        },
        "unitCost": {
          "type": "number",
          "pattern": "^[\\S]*$",
          "description": "the unit cost of the item with discount (Actual selling price).",
          "minimum": 0,
          "maximum": 999999999.99,
          "example": "999.99"
        },
        "quantity": {
          "type": "integer",
          "format": "int_64",
          "description": "the quantity of the item selected by the user.",
          "example": "1"
        },
        "isFinanceOptionSupported": {
          "type": "boolean",
          "description": "the affirm finance option for available for a item.",
          "example": "true"
        },
        "isFinanceOptionSelected": {
          "type": "boolean",
          "description": "the affirm finance option for a item was selected by the user.",
          "example": "true"
        },
        "isRefunded": {
          "type": "boolean",
          "description": "whether or not this item has been refunded",
          "example": "false"
        }
      }
    },
    "UpdateOrderRequest": {
      "type": "object",
      "description": "Update the order status after the payment has been approved.",
      "required": [
        "orderId",
        "orderStatus",
        "emailId",
        "phoneNumber"
      ],
      "properties": {
        "emailId": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "a@a.com",
          "description": "the email id of the customer."
        },
        "customerFirstName": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "John",
          "description": "the first name of the customer."
        },
        "customerLastName": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "Doe",
          "description": "the last name of the customer."
        },
        "phoneNumber": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "2134567891",
          "description": "the phone number of the customer."
        },
        "orderStatus": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "enum": [
            "OPEN",
            "ERROR",
            "COMPLETE",
            "CANCELED"
          ],
          "example": "COMPLETE",
          "description": "the final status of the order."
        },
        "orderId": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "px_21312321321",
          "description": "the unique identifier of the purchase order."
        },
        "subtotal": {
          "type": "number",
          "format": "double",
          "minimum": 0,
          "maximum": 999999999.99,
          "example": "80.90",
          "description": "the total value of the items in the cart."
        },
        "tax": {
          "type": "number",
          "format": "double",
          "minimum": 0,
          "maximum": 999999999.99,
          "example": "15.01",
          "description": "the total tax value of the items in the cart."
        },
        "taxPercentage": {
          "type": "number",
          "format": "double",
          "minimum": 0,
          "maximum": 999999999.99,
          "example": "10",
          "description": "the tax percentage on the order."
        },
        "grandTotal": {
          "type": "number",
          "format": "double",
          "minimum": 0,
          "maximum": 999999999.99,
          "example": "95.91",
          "description": "the total value of the items in the cart including taxes."
        },
        "billPay": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/BillItem"
          }
        },
        "items": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/CartItem"
          }
        },
        "transactionDetails": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/TransactionDetail"
          }
        },
        "financingToken": {
          "$ref": "#/definitions/FinancingToken"
        }
      }
    },
    "CartDetailsRequest": {
      "type": "object",
      "description": "The cart information to store.<br>One of emailId or name is required.<br>One of billPay or items is required.",
      "required": [
        "emailId",
        "phoneNumber",
        "orderId",
        "subtotal",
        "tax",
        "taxPercentage",
        "grandTotal"
      ],
      "properties": {
        "emailId": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "a@a.com",
          "description": "the email id of the customer."
        },
        "customerFirstName": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "John",
          "description": "the first name of the customer."
        },
        "customerLastName": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "Doe",
          "description": "the last name of the customer."
        },
        "phoneNumber": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "2134568791",
          "description": "the phone number of the customer."
        },
        "orderId": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "px_32372367",
          "description": "the unique identifier of the purchase order."
        },
        "subtotal": {
          "type": "number",
          "format": "double",
          "minimum": 0,
          "maximum": 999999999.99,
          "example": "80.90",
          "description": "the total value of the items in the cart."
        },
        "tax": {
          "type": "number",
          "format": "double",
          "minimum": 0,
          "maximum": 999999999.99,
          "example": "15.01",
          "description": "the total tax value of the items in the cart."
        },
        "taxPercentage": {
          "type": "number",
          "format": "double",
          "minimum": 0,
          "maximum": 999999999.99,
          "example": "10",
          "description": "the tax percentage on the order."
        },
        "grandTotal": {
          "type": "number",
          "format": "double",
          "minimum": 0,
          "maximum": 999999999.99,
          "example": "95.91",
          "description": "the total value of the items in the cart including taxes."
        },
        "billPay": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/BillItem"
          }
        },
        "items": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/CartItem"
          }
        }
      }
    },
    "TransactionDetail": {
      "type": "object",
      "description": "the list of payment/refund/void transactions",
      "properties": {
        "paymentId": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "description": "the enterprise payment id received when payment/refund/void is approved.",
          "example": "213888777"
        },
        "parentPaymentId": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "description": "the original enterprise payment id received when payment is approved. To be used when order is refunded or voided",
          "example": "213888999"
        },
        "last4": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "description": "the last 4 of the card used.",
          "example": "4444"
        },
        "cardBrand": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "description": "the brand of card used AffirmVCN, MasterCard, Visa, Discover or Amex.",
          "example": "AffirmVCN"
        },
        "amount": {
          "type": "number",
          "format": "double",
          "minimum": 0,
          "maximum": 999999999.99,
          "example": "95.91",
          "description": "the total value processed in this transaction"
        },
        "date": {
          "type": "string",
          "description": "the date of the transaction",
          "example": "09202019"
        },
        "type": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "enum": [
            "SALE",
            "REFUND",
            "VOID"
          ],
          "example": "SALE",
          "description": "the final status of the order."
        }
      }
    },
    "CartDetailsResponse": {
      "type": "object",
      "description": "The cart information to store.<br>One of emailId or name is required.<br>One of billPay or items is required.",
      "required": [
        "emailId",
        "phoneNumber",
        "orderId",
        "subtotal",
        "tax",
        "taxPercentage",
        "grandTotal",
        "createdDate"
      ],
      "properties": {
        "createdDate": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "2019-09-26 09:47:38.305",
          "description": "the date the order was originally created"
        },
        "emailId": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "a@a.com",
          "description": "the email id of the customer."
        },
        "customerFirstName": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "John",
          "description": "the first name of the customer."
        },
        "customerLastName": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "Doe",
          "description": "the last name of the customer."
        },
        "phoneNumber": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "2134568791",
          "description": "the phone number of the customer."
        },
        "orderId": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "example": "px_32313232313",
          "description": "the unique identifier of the purchase order."
        },
        "orderStatus": {
          "type": "string",
          "pattern": "^[\\S]*$",
          "enum": [
            "OPEN",
            "ERROR",
            "COMPLETE",
            "CANCELED"
          ],
          "example": "COMPLETE",
          "description": "the final status of the order."
        },
        "subtotal": {
          "type": "number",
          "format": "double",
          "minimum": 0,
          "maximum": 999999999.99,
          "example": "80.90",
          "description": "the total value of the items in the cart."
        },
        "tax": {
          "type": "number",
          "format": "double",
          "minimum": 0,
          "maximum": 999999999.99,
          "example": "15.01",
          "description": "the total tax value of the items in the cart."
        },
        "taxPercentage": {
          "type": "number",
          "format": "double",
          "minimum": 0,
          "maximum": 999999999.99,
          "example": "10",
          "description": "the tax percentage on the order."
        },
        "grandTotal": {
          "type": "number",
          "format": "double",
          "minimum": 0,
          "maximum": 999999999.99,
          "example": "95.91",
          "description": "the total value of the items in the cart including taxes."
        },
        "billPay": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/BillItem"
          }
        },
        "items": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/CartItem"
          }
        },
        "transactionDetails": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/TransactionDetail"
          }
        },
        "financingToken": {
          "$ref": "#/definitions/FinancingToken"
        }
      }
    }
  }
}
