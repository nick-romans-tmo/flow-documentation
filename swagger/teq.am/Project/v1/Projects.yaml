swagger: '2.0'
info:
  description: API for getting list of projects
  version: '1.0.0'
  title: Projects
  contact:
    name: Karan Singla
    email: karan.singla@t-mobile.com
host: canvas-dev.t-mobile.com
basePath: /api/project-management/v1
schemes:
  - https
paths:
  /retrieve-projects:
    get:
      tags:
        - Get Projects
      summary: get details of an existing project
      description: get details of an existing project
      operationId: getProjectDetails
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: Authorization
          in: header
          required: true
          default: 'Bearer {token}'
          type: string
          description: Authorization header with valid bearer token
        - name: interation-id
          in: header
          required: true
          default: 71febea5-2897-40ba-b302-23545e8d04f6
          type: string
          description: GUID format string unique for each interaction
        - name: Accept
          in: header
          required: true
          type: string
          description: Supported values are application/json & application/xml
          default: application/json
        - in: query
          name: limit
          type: string
          description: ntid of the user of API
          required: false
        - in: query
          name: offset
          type: string
          required: false
          description: valid id
      responses:
        '200':
          description: Successful operation
          schema:
            $ref: '#/definitions/ProjectList'
          examples:
            application/json:
              metadata:
                limit: ''
                offset: ''
                totalRecords: ''
              projects:
                - projectName: ''
                  projectStatus: ''
                  prNumber: ''
                  projectActive: ''
                  projectReserved: ''
                  projectInteg: ''
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: Invalid Request
              userMessage: Invalid request sent to server
              systemMessage: 404 Not Found
              detailLink: ''
        '401':
          description: Error message for unauthorized user
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: Unauthorized Request
              userMessage: Unauthorized request sent to server
              systemMessage: 401 Unauthorized
              detailLink: ''
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: Invalid Request
              userMessage: Requested resource not found
              systemMessage: 404 Not Found
              detailLink: ''
        '405':
          description: Method Not Supported
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: Invalid Request
              userMessage: Method not supported
              systemMessage: 405 Method Not Supported
              detailLink: ''
        '406':
          description: Not Acceptable
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: Invalid Request
              userMessage: Not Acceptable
              systemMessage: 406 Not Acceptable
              detailLink: ''
        '500':
          description: Error message for internal service error in application
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: Internal Server Error
              userMessage: Unable to process request. Please try again later
              systemMessage: 500 Internal Server Error
              detailLink: ''
        '503':
          description: Error message for service not available
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: Service Unavailable
              userMessage: Please try again later
              systemMessage: 403 Unauthorized
              detailLink: ''
      security:
        - oauth2: []
definitions:
  Projects:
    title: projects
    type: object
    description: project response object
    properties:
      projectName:
        type: string
        description: returns the project name of the queried project
        format: ''
      projectStatus:
        type: string
        description: returns the status of project i.e. approved rejected etc
      prNumber:
        type: string
        description: returns the pr code associated with the project
      projectActive:
        type: boolean
        description: >-
          returns the status of project whether it is active or inactive i.e
          true or false
      projectReserved:
        type: boolean
        description: returns the reserved status of the project i.e true or false
      projectIntegration:
        type: boolean
        description: returns the project integration status value i.e. true or false
    required:
      - projectName
      - projectStatus
      - prNumber
      - projectActive
      - projectReserved
      - projectIntegration
  Metadata:
    title: metadata
    description: metadata to be returned in api response
    type: object
    properties:
      limit:
        type: integer
        format: int32
        description: set the number of objects returned in one page
      totalRecords:
        type: integer
        format: int32
        description: total number of records present
      offset:
        type: integer
        format: int32
        description: specify the ranking number of the first item on the page
    required:
      - limit
      - totalRecords
      - offset
  ProjectList:
    type: object
    description: project list response json structure
    properties:
      metadata:
        $ref: '#/definitions/Metadata'
      projects:
        type: array
        items:
          $ref: '#/definitions/Projects'
  Error:
    title: error
    type: object
    properties:
      code:
        type: string
        description: unique error code for each error message
      userMessage:
        type: string
        description: user friendly error message for application
      systemMessage:
        type: string
        description: system error message for application
      detailLink:
        type: string
        description: link to detailed description of error message
    required:
      - code
      - userMessage
      - systemMessage
securityDefinitions:
  oauth2:
    description: ''
    type: oauth2
    flow: password
    tokenUrl: 'https://'
