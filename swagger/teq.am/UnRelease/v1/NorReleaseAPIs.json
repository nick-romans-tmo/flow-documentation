{
  "openapi": "3.0.2",
  "info": {
    "description": "Support bi-directional issue syncing for night of release issues.",
    "version": "1.0.0",
    "title": "Deployment Issues",
    "termsOfService": "https://t-mobile.com",
    "contact": {
      "email": "michael.desimone1@t-mobile.com",
      "name": "T-Mobile"
    },
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    }
  },
  "servers": [
    {
      "url": "https://nor-issue-sync.npe01.cf.t-mobile.com/it-delivery-mgmt/v1/issue-sync",
      "description": "endpoint"
    }
  ],
  "tags": [
    {
      "name": "issue",
      "description": "NOR Deployment Issues"
    }
  ],
  "paths": {
    "/issue/{issue-key}": {
      "get": {
        "summary": "find by jira issueKey",
        "description": "Obtain an issue from JiraSW",
        "x-api-pattern": "QueryResource",
        "operationId": "findIssueByIssueKey",
        "tags": [
          "Incoming to TMUS"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "issue-key",
            "schema": {
              "allOf": [
                {
                  "$ref": "#/components/schemas/issueKeyDef"
                }
              ]
            },
            "required": true,
            "description": "The issueKey of a Jira issue",
            "example": "RM-999"
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/outgoingIssue"
                }
              }
            },
            "headers": {
              "Date": {
                "description": "date",
                "schema": {
                  "$ref": "#/components/schemas/headerDate"
                }
              }
            }
          },
          "404": {
            "description": "Issue not found",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ErrorModel"
                }
              }
            },
            "headers": {
              "Date": {
                "description": "date",
                "schema": {
                  "$ref": "#/components/schemas/headerDate"
                }
              }
            }
          }
        },
        "security": [
          {
            "ApiKeyAuth": []
          }
        ]
      }
    },
    "/incident/{incident-id}": {
      "get": {
        "description": "Obtain an issue from JiraSW by ITSM ID",
        "x-api-pattern": "QueryResource",
        "operationId": "findIssueByIncidentId",
        "summary": "find by ITSM id",
        "tags": [
          "Incoming to TMUS"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "incident-id",
            "schema": {
              "$ref": "#/components/schemas/incidentId"
            },
            "required": true,
            "description": "The ITSM incident id of the issue",
            "example": "ITSM012345678"
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/outgoingIssue"
                }
              }
            }
          },
          "404": {
            "description": "Issue not found",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ErrorModel"
                }
              }
            }
          }
        },
        "security": [
          {
            "ApiKeyAuth": []
          }
        ]
      }
    },
    "/deployment-issue": {
      "post": {
        "x-api-pattern": "CreateInCollection",
        "tags": [
          "Incoming to TMUS"
        ],
        "summary": "Add a new issue to T-Mobile system",
        "operationId": "addIssue",
        "requestBody": {
          "description": "NOR Issue to be added to T-Mobile system",
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/incomingIssue"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "created",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/outgoingIssue"
                }
              }
            }
          },
          "409": {
            "description": "Issue exists",
            "content": {
              "application/json": {
                "schema": {
                  "allOf": [
                    {
                      "$ref": "#/components/schemas/ErrorModelIssueExists"
                    }
                  ]
                }
              }
            }
          }
        },
        "security": [
          {
            "ApiKeyAuth": []
          }
        ]
      },
      "put": {
        "x-api-pattern": "ReplaceResource",
        "tags": [
          "Incoming to TMUS"
        ],
        "summary": "Update an existing T-Mobile issue",
        "description": "update existing issue",
        "operationId": "updateIssue",
        "requestBody": {
          "description": "issue details",
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/incomingIssueUpdate"
              }
            }
          }
        },
        "responses": {
          "204": {
            "description": "updated"
          },
          "400": {
            "description": "could not update"
          },
          "404": {
            "description": "issue not found"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "security": [
          {
            "ApiKeyAuth": []
          }
        ]
      }
    },
    "/outbound-issue/{issue-key}": {
      "post": {
        "x-api-pattern": "CreateInCollection",
        "tags": [
          "TMUS internal calls (will invoke Sprint API)"
        ],
        "summary": "target endpoint for Jira webhook call, will repackage and then invoke Sprint API",
        "description": "call back to jira to get full issue body",
        "operationId": "addExternalIssue",
        "parameters": [
          {
            "in": "path",
            "name": "issue-key",
            "schema": {
              "$ref": "#/components/schemas/issueKeyDef"
            },
            "required": true,
            "description": "The issueKey of a Jira issue",
            "example": "RM-999"
          },
          {
            "in": "query",
            "name": "user_id",
            "schema": {
              "$ref": "#/components/schemas/string255"
            },
            "description": "user id of updating user (ignored)",
            "example": "jsmith1"
          },
          {
            "in": "query",
            "name": "user_key",
            "schema": {
              "$ref": "#/components/schemas/string255"
            },
            "description": "user key of updating user (ignored)",
            "example": "jsmith1"
          }
        ],
        "requestBody": {
          "description": "jira issue details",
          "required": false,
          "content": {
            "application/json": {
              "schema": {
                "type": "object"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "received",
            "headers": {
              "Location": {
                "description": "location",
                "schema": {
                  "$ref": "#/components/schemas/headerLocation"
                }
              }
            }
          }
        },
        "security": [
          {
            "ApiKeyAuth": []
          }
        ]
      }
    }
  },
  "components": {
    "schemas": {
      "incomingIssue": {
        "type": "object",
        "properties": {
          "incidentId": {
            "$ref": "#/components/schemas/incidentId"
          },
          "title": {
            "allOf": [
              {
                "$ref": "#/components/schemas/string255"
              },
              {
                "description": "issue summary/title"
              }
            ]
          },
          "priority": {
            "$ref": "#/components/schemas/TMUSpriority"
          },
          "createdBy": {
            "allOf": [
              {
                "$ref": "#/components/schemas/string255"
              },
              {
                "example": "John Smith"
              }
            ]
          },
          "status": {
            "type": "string",
            "enum": [
              "open",
              "closed"
            ]
          },
          "description": {
            "allOf": [
              {
                "$ref": "#/components/schemas/longString"
              },
              {
                "example": "multiline description of issue"
              }
            ]
          }
        },
        "required": [
          "incidentId"
        ]
      },
      "incomingIssueUpdate": {
        "type": "object",
        "required": [
          "issueKey"
        ],
        "properties": {
          "title": {
            "allOf": [
              {
                "$ref": "#/components/schemas/string255"
              },
              {
                "description": "issue summary/title"
              }
            ]
          },
          "priority": {
            "allOf": [
              {
                "$ref": "#/components/schemas/TMUSpriority"
              }
            ]
          },
          "status": {
            "type": "string",
            "description": "issue status",
            "enum": [
              "open",
              "closed"
            ]
          },
          "issueKey": {
            "allOf": [
              {
                "$ref": "#/components/schemas/issueKeyDef"
              }
            ]
          },
          "description": {
            "allOf": [
              {
                "$ref": "#/components/schemas/longString"
              },
              {
                "example": "description of issue"
              }
            ],
            "description": "description of issue"
          },
          "comment": {
            "allOf": [
              {
                "$ref": "#/components/schemas/longString"
              },
              {
                "description": "brief comment to add to issue as jira comment"
              }
            ]
          }
        }
      },
      "outgoingIssue": {
        "type": "object",
        "required": [
          "issueKey",
          "title",
          "priority"
        ],
        "properties": {
          "issueKey": {
            "$ref": "#/components/schemas/issueKeyDef"
          },
          "title": {
            "allOf": [
              {
                "$ref": "#/components/schemas/string255"
              },
              {
                "description": "issue summary/title"
              }
            ]
          },
          "priority": {
            "allOf": [
              {
                "$ref": "#/components/schemas/TMUSpriority"
              }
            ]
          },
          "createdBy": {
            "allOf": [
              {
                "$ref": "#/components/schemas/string255"
              },
              {
                "description": "issue creator"
              },
              {
                "example": "John Smith"
              }
            ],
            "description": "created by"
          },
          "status": {
            "type": "string",
            "description": "issue status",
            "enum": [
              "open",
              "closed"
            ]
          },
          "incidentId": {
            "allOf": [
              {
                "$ref": "#/components/schemas/incidentId"
              },
              {
                "description": "ITSM ID if known"
              }
            ]
          },
          "description": {
            "allOf": [
              {
                "$ref": "#/components/schemas/longString"
              },
              {
                "example": "multiline description of issue"
              }
            ],
            "description": "description of issue"
          },
          "comment": {
            "allOf": [
              {
                "$ref": "#/components/schemas/longString"
              },
              {
                "description": "brief comment to add to issue as jira comment"
              }
            ]
          },
          "customerImpact": {
            "allOf": [
              {
                "$ref": "#/components/schemas/longString"
              }
            ]
          },
          "workaround": {
            "allOf": [
              {
                "$ref": "#/components/schemas/longString"
              }
            ]
          },
          "assignee": {
            "allOf": [
              {
                "$ref": "#/components/schemas/string255"
              },
              {
                "description": "assigned owner"
              },
              {
                "example": "Pam Jones"
              }
            ]
          }
        }
      },
      "longString": {
        "type": "string",
        "maxLength": 65535
      },
      "string255": {
        "type": "string",
        "maxLength": 255
      },
      "issueKeyDef": {
        "type": "string",
        "example": "RM-999",
        "description": "Jira issue key",
        "minLength": 1,
        "maxLength": 18
      },
      "incidentId": {
        "type": "string",
        "description": "ITSM id of issue",
        "example": "ITSM012345678",
        "minLength": 1,
        "maxLength": 255
      },
      "TMUSpriority": {
        "type": "string",
        "description": "issue priority",
        "enum": [
          "Blocker",
          "High",
          "Medium",
          "Low"
        ],
        "example": "High"
      },
      "headerDate": {
        "type": "string",
        "maxLength": 64
      },
      "headerLocation": {
        "type": "string",
        "maxLength": 1024
      },
      "ApiResponseModel": {
        "type": "object",
        "properties": {
          "message": {
            "allOf": [
              {
                "$ref": "#/components/schemas/string255"
              },
              {
                "example": "issue created or updated"
              }
            ]
          },
          "error": {
            "type": "string",
            "description": "error occurred t/f",
            "enum": [
              "false",
              "true"
            ]
          },
          "issueKey": {
            "allOf": [
              {
                "$ref": "#/components/schemas/issueKeyDef"
              },
              {
                "description": "Jira issue key"
              }
            ]
          }
        }
      },
      "ErrorModel": {
        "type": "object",
        "required": [
          "message",
          "code"
        ],
        "properties": {
          "message": {
            "allOf": [
              {
                "$ref": "#/components/schemas/string255"
              },
              {
                "example": "issue xyz doesn't exist"
              }
            ]
          },
          "code": {
            "allOf": [
              {
                "$ref": "#/components/schemas/string255"
              },
              {
                "example": "not found"
              }
            ]
          }
        }
      },
      "ErrorModelIssueExists": {
        "type": "object",
        "required": [
          "message",
          "code"
        ],
        "properties": {
          "message": {
            "allOf": [
              {
                "$ref": "#/components/schemas/string255"
              },
              {
                "example": "issue xyz exists"
              }
            ]
          },
          "code": {
            "allOf": [
              {
                "$ref": "#/components/schemas/string255"
              },
              {
                "example": "issue found"
              }
            ]
          },
          "incidentId": {
            "allOf": [
              {
                "$ref": "#/components/schemas/incidentId"
              },
              {
                "example": "ITSM012345678"
              }
            ]
          }
        }
      }
    },
    "securitySchemes": {
      "ApiKeyAuth": {
        "type": "apiKey",
        "name": "X-API-Key",
        "in": "header",
        "description": ""
      }
    }
  }
}