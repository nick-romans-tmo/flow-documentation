swagger: '2.0'
info:
  description: "This describes Orchestrator component. Orchestrator is used as backend for various native experiences in T-Mobile App (Android and iOS)."
  version: '1.0'
  title: "Api Documentation"
  contact:
    email: "Dab_Team@t-mobile.com"
host: 'orchestrator.dab.t-mobile.com'
basePath: /
tags:
  - name: datapass-controller
    description: "Datapass Controller"
  - name: orchestrator-controller
    description: "Orchestrator Controller"
schemes:
  - "https"
paths:
  /v1/bill-data:
    get:
      tags:
        - orchestrator-controller
      summary: "It returns customers billing data along with saved payment methods."
      operationId: getBillingAndPaymentDetailsUsingGET
      produces:
        - application/json
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/CustomerBillProfileModel'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
  /v1/datapass:
    get:
      tags:
        - datapass-controller
      summary: "It returns the datapasses which a msisdn is eligible to buy."
      operationId: getEligibleDatapassUsingGET
      parameters: 
        - in: header
          name: Authorization
          type: string
          required: true 
        - in: header 
          name: X-Dat
          type: string
          required: true
        - in: header 
          name: X-Authorization
          type: string
          required: true
        - in: header 
          name: Host
          type: string
          required: true 
        - in: header 
          name: X-B3-TraceId
          type: string
          required: true
        - in: header  
          name: X-B3-ParentSpanId
          type: string
          required: true
        - in: header 
          name: X-B3-SpanId
          type: string
          required: true   

      produces:
        - application/json
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/DatapassModel'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
    post:
      tags:
        - datapass-controller
      summary: addDatapass
      operationId: addDatapassUsingPOST
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: header
          name: Authorization
          type: string
          required: true
        - in: header
          name: X-Dat
          type: string
          required: true
        - in: header
          name: X-Authorization
          type: string
          required: true
        - in: header
          name: Host
          type: string
          required: true
        - in: header
          name: X-B3-TraceId
          type: string
          required: true
        - in: header
          name: X-B3-ParentSpanId
          type: string
          required: true
        - in: header
          name: X-B3-SpanId
          type: string
          required: true
        - in: body
          name: changeDetails
          description: changeDetails
          required: true
          schema:
            $ref: '#/definitions/ChangeDetails'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/DatapassResponseModel'
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
  /v1/register:
    get:
      tags:
        - orchestrator-controller
      summary: "It is used to register a JWT with APIGEE."
      operationId: registerTokenWithApigeeUsingGET
      produces:
        - application/json
      responses:
        '200':
          description: OK
          schema:
            type: object
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
definitions:
  BankAccountModel:
    type: object
    properties:
      accountNumber:
        type: string
      billingAddress:
        $ref: '#/definitions/BillingAddressModel'
      paymentMethodStatus:
        type: string
      routingNumber:
        type: string
    title: BankAccountModel
  BillingAddressModel:
    type: object
    properties:
      zip:
        type: string
    title: BillingAddressModel
  BusinessDetailsModel:
    type: object
    properties:
      billerCode:
        type: string
      businessSegment:
        type: string
      businessUnit:
        type: string
      operationType:
        type: string
      orderTypes:
        type: string
      productGroup:
        type: string
      programCode:
        type: string
    title: BusinessDetailsModel
  CreditCardModel:
    type: object
    properties:
      billingAddress:
        $ref: '#/definitions/BillingAddressModel'
      cardAlias:
        type: string
      cardNumber:
        type: string
      expirationMonthYear:
        type: string
      paymentMethodStatus:
        type: string
      type:
        type: string
    title: CreditCardModel
  CustomerBillProfileModel:
    type: object
    properties:
      banNumber:
        type: string
      businessDetails:
        $ref: '#/definitions/BusinessDetailsModel'
      channelId:
        type: string
      nameOnAccount:
        type: string
      paymentDetails:
        $ref: '#/definitions/PaymentDetailsModel'
      paymentMethods:
        type: array
        items:
          $ref: '#/definitions/PaymentMethod'
      webPaymentCta1:
        type: string
      webPaymentCta2:
        type: string
      ctaAddBank:
        type: string
      ctaAddCard:
        type: string
    title: CustomerBillProfileModel
  ChangeDetails:
    type: object
    properties:
      action:
        type: string
        description: "Action will either be ADD or REMOVE"
      changeType:
        type: string
        description: "Type of change that is requested. For datapass it will be 'changeDataPass'"
      effectiveDate:
        type: string
        description: "Current date and time in yyyy-MM-dd'T'HH:mm:ss.SSSXXX format"
      soc:
        type: string
        description: "SOC of requested datapass"
    title: ChangeDetails
  DataPass:
    type: object
    properties:
      dataPassDescription:
        type: string
        description: "Detail description of data pass"
      dataPassLimit:
        type: string
        description: "Data limit on the datapass."
      duration:
        type: string
        description: "Duration for which datapass is valid."
      durationUnit:
        type: string
        description: "Represents hours or days for datapass validity."
      name:
        type: string
        description: "Title or short description of datapss."
      price:
        type: number
        format: float
        description: "Price of datapass."
      soc:
        type: string
        description: "SOC id of datapass."
    title: DataPass
  DatapassModel:
    type: object
    properties:
      passList:
        type: array
        items:
          $ref: '#/definitions/DataPass'
    title: DatapassModel
  DatapassResponseModel:
    type: object
    properties:
      status:
        type: string
      statusCode:
        type: string
      statusMessage:
        type: string
    title: DatapassResponseModel
  PaymentDetailsModel:
    type: object
    properties:
      pastDueBalance:
        type: number
        format: float
      paymentDueDate:
        type: string
      totalBalanceDue:
        type: number
        format: float
      transactionType:
        type: string
    title: PaymentDetailsModel
  PaymentMethod:
    type: object
    properties:
      bankAccount:
        $ref: '#/definitions/BankAccountModel'
      code:
        type: string
      creditCard:
        $ref: '#/definitions/CreditCardModel'
      isFdp:
        type: boolean
      paymentType:
        type: string
      updatedTime:
        type: string
        format: date-time
    title: PaymentMethod
