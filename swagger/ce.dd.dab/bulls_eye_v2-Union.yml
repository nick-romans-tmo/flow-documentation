swagger: '2.0'
info:
  description: This is the Union-facing API for internal use.
  version: 2.0.0
  title: Bullseye V2 Swagger for Union
  termsOfService: ''
  contact:
    name: DAB TEAM
    email: DAB_Team@t-mobile.com
  license:
    name: Some License
    url: 'http://www.SomeLicenseURL.com'
host: 'localhost:9021'
basePath: /2.0/bullseye/union
tags:
  - name: union-facing-controller
    description: 'Controller for sending messages by various identifiers. For 200 OK, the response body contains a list of call reports for each message sent. This is because for a TMOID, for instance, there might be multiple registrations on record, and some of these messages might fail whilst others succeed. Similarly, the response might be empty if no record for the identifier exists. It is up to the client to check the results. FCM error codes are passed back, including when doing a ''dryrun'', see https://firebase.google.com/docs/cloud-messaging/admin/errors.'
x-servers:
  - url:/
schemes:
  - https
paths:
  '/send':
    post:
      description: 'Bullseye V2 is a service providing messaging to user devices by storing mappings between various device identifiers. For example, messages could be sent by MSISDN, or by TMOID or by MSISDN and TMOID. The messages are pushed to the device via FCM - given that a registration exists.'
      x-api-pattern: ExecuteFunction
      tags:
        - union-facing-controller
      summary: sendByTmoidAndMsisdn
      operationId: sendByTmoidAndMsisdnUsingPOST
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: platform
          in: query
          description: 'Similar to ''app_name'' this may act as a filter of records, only returning matching ones, and so it must match the string that was provided by the device on registration with BullseyeV2. If absent all records are used.'
          required: false
          type: string
          x-example: Android
        - name: dry_run
          in: query
          description: 'If true, passes the dryrun flag to FCM, which can be useful for a validation-only/test requests. By default ''false''. See https://firebase.google.com/docs/cloud-messaging/admin/send-messages#sending_in_the_dry_run_mode'
          required: false
          type: boolean
          x-example: true
          default: false
        - name: allow_logged_out
          in: query
          description: 'If true, logged out/de-registered users will not receive push notifications'
          required: false
          type: boolean
          x-example: false
          default: false
        - in: body
          name: message
          description: message
          required: true
          schema:
            $ref: '#/definitions/SendMessageRequest'
        - name: msisdn
          in: query
          description: 'msisdn, use this property if identifier is MSISDN'
          required: true
          type: string
          x-example: 6198877172
        - name: tmoid
          in: query
          description: 'tmoid, use this property if identifier is TMOID'
          required: true
          type: string
          x-example: U-f824db2a-5803-4d1b-a4ef-7a77cf898d12
        - name: app_name
          in: query
          description: 'The provided ''app_name'' acts as a filter of records, only returning matching ones, and so it must match the string that was provided by the device on registration with BullseyeV2.'
          required: true
          type: string
          x-example: TMOApp
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/SendMessageResponse'
          examples:
            calls:
              - cnf_fingerprint: string
                exception_message: string
                fcm_token: string
                idt_msisdn: string
                message_id: string
                success: true
                tmoid: string

        '400':
          description: Invalid request
          schema:
            type: string
        '500':
          description: Internal Server Error
          schema:
            type: string
definitions:
  AndroidConfig:
    type: object
    properties:
      collapse_key:
        description: Collapse key
        type: string
      data:
        type: object
        description: Data
        additionalProperties:
          type: string
      notification:
        $ref: '#/definitions/AndroidNotification'
      priority:
        type: string
        description: Priority
      restricted_package_name:
        type: string
        description: name of the restricted package
      ttl:
        type: integer
        description: Time to live
        format: int64
    title: AndroidConfig
  AndroidNotification:
    type: object
    properties:
      body:
        type: string
        description: body
      body_loc_args:
        type: array
        description: boby loc arguments
        items:
          type: string
      body_loc_key:
        type: string
        description: body loc key
      click_action:
        type: string
        description: click action
      color:
        type: string
        description: color
      icon:
        type: string
        description: icon
      sound:
        type: string
        description: sound
      tag:
        type: string
        description: tag
      title:
        type: string
        description: title
      title_loc_args:
        description: title loc arguments
        type: array
        items:
          type: string
      title_loc_key:
        type: string
        description: title loc key
    title: AndroidNotification
  ApnsConfig:
    type: object
    properties:
      aps:
        $ref: '#/definitions/Aps'
      headers:
        type: object
        description: headers
        additionalProperties:
          type: string
      payload:
        type: object
        description: payload
    title: ApnsConfig
  Aps:
    type: object
    properties:
      alert:
        $ref: '#/definitions/ApsAlert'
      alert-as-string:
        type: string
        description: alert as String
      badge:
        type: integer
        format: int32
        description: badge
      category:
        type: string
        description: categoty
      content-available:
        type: boolean
        description: content available
      sound:
        type: string
        description: sound
      thread-id:
        type: string
        description: thread ID
    title: Aps
  ApsAlert:
    type: object
    properties:
      action-loc-key:
        type: string
        description: action loc key
      body:
        type: string
        description: body
      launch-image:
        type: string
        description: launch image
      loc-args:
        type: array
        description: loc arguments
        items:
          type: string
      loc-key:
        type: string
        description: loc key
      title:
        type: string
        description: title
      title-loc-args:
        type: array
        description: title loc arguments
        items:
          type: string
      title-loc-key:
        type: string
        description: title loc key
    title: ApsAlert
  CallReport:
    type: object
    properties:
      cnf_fingerprint:
        type: string
        description: fingerprint configuration
      exception_message:
        type: string
        description: exception message
      fcm_token:
        type: string
        description: FCM token
      idt_msisdn:
        type: string
        description: IDT MSISDN
      message_id:
        type: string
        description: message ID
      success:
        type: boolean
        description: Success
      tmoid:
        type: string
        description: TMO ID
    title: CallReport
  Notification:
    type: object
    properties:
      body:
        description: body
        type: string
      title:
        description: title
        type: string
    title: Notification
  SendMessageRequest:
    type: object
    properties:
      android_config:
        $ref: '#/definitions/AndroidConfig'
      apns_config:
        $ref: '#/definitions/ApnsConfig'
      data:
        type: object
        description: data
        additionalProperties:
          type: string
      notification:
        $ref: '#/definitions/Notification'
    title: SendMessageRequest
  SendMessageResponse:
    type: object
    properties:
      calls:
        description: calls
        type: array
        items:
          $ref: '#/definitions/CallReport'
    title: SendMessageResponse
