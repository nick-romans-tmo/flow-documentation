{
  "swagger": "2.0",
  "info": {
    "description": "This is the Union-facing API for internal use.",
    "version": "1.0.0",
    "title": "Bullseye V2 Swagger for Union",
    "termsOfService": "",
    "contact": {
      "name": "DAB Team",
      "email": "DAB_Team@t-mobile.com"
    }
  },
  "x-servers": [
    {
      "url": "bullseye-v2-union.prd-int.dab.t-mobile.com/2.0/bullseye/union/send",
      "description": "TST environment"
    }
  ],
  "host": "bullseye-v2-union.prd-int.dab.t-mobile.com",
  "basePath": "/2.0",
  "tags": [
    {
      "name": "union-facing-controller",
      "description": "Controller for sending messages by various identifiers. For 200 OK, the response body contains a list of call reports for each message sent. This is because for a TMOID, for instance, there might be multiple registrations on record, and some of these messages might fail whilst others succeed. Similarly, the response might be empty if no record for the identifier exists. It is up to the client to check the results. FCM error codes are passed back, including when doing a 'dryrun', see https://firebase.google.com/docs/cloud-messaging/admin/errors."
    }
  ],
  "schemes": [
    "https"
  ],
  "paths": {
    "/bullseye/union/send": {
      "post": {
        "tags": [
          "union-facing-controller"
        ],
        "summary": "sendByMsisdn",
        "operationId": "sendByMsisdnUsingPOST",
        "security": [
            {
              "Oauth2": [
                "send:sendByMsisdn"
              ]
            }
          ],
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "platform",
            "in": "query",
            "description": "Similar to 'app_name' this may act as a filter of records, only returning matching ones, and so it must match the string that was provided by the device on registration with BullseyeV2. If absent all records are used.",
            "required": false,
            "type": "string"
          },
          {
            "name": "dry_run",
            "in": "query",
            "description": "If true, passes the dryrun flag to FCM, which can be useful for a validation-only/test requests. By default 'false'. See https://firebase.google.com/docs/cloud-messaging/admin/send-messages#sending_in_the_dry_run_mode",
            "required": false,
            "type": "boolean",
            "default": false
          },
          {
            "in": "body",
            "name": "message",
            "description": "message",
            "required": true,
            "schema": {
              "$ref": "#/definitions/SendMessageRequest"
            }
          },
          {
            "name": "msisdn",
            "in": "query",
            "description": "msisdn",
            "required": true,
            "type": "string"
          },
          {
            "name": "app_name",
            "in": "query",
            "description": "The provided 'app_name' acts as a filter of records, only returning matching ones, and so it must match the string that was provided by the device on registration with BullseyeV2.",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/SendMessageResponse"
            }
          },
          "400": {
            "description": "Invalid request",
            "schema": {
              "type": "string"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "type": "string"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "AndroidConfig": {
      "type": "object",
      "properties": {
        "collapse_key": {
          "type": "string"
        },
        "data": {
          "type": "object",
          "additionalProperties": {
            "type": "string"
          }
        },
        "notification": {
          "$ref": "#/definitions/AndroidNotification"
        },
        "priority": {
          "type": "string"
        },
        "restricted_package_name": {
          "type": "string"
        },
        "ttl": {
          "type": "integer",
          "format": "int64"
        }
      },
      "title": "AndroidConfig"
    },
    "AndroidNotification": {
      "type": "object",
      "properties": {
        "body": {
          "type": "string"
        },
        "body_loc_args": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "body_loc_key": {
          "type": "string"
        },
        "click_action": {
          "type": "string"
        },
        "color": {
          "type": "string"
        },
        "icon": {
          "type": "string"
        },
        "sound": {
          "type": "string"
        },
        "tag": {
          "type": "string"
        },
        "title": {
          "type": "string"
        },
        "title_loc_args": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "title_loc_key": {
          "type": "string"
        }
      },
      "title": "AndroidNotification"
    },
    "ApnsConfig": {
      "type": "object",
      "properties": {
        "aps": {
          "$ref": "#/definitions/Aps"
        },
        "headers": {
          "type": "object",
          "additionalProperties": {
            "type": "string"
          }
        },
        "payload": {
          "$ref": "#/definitions/Payload"
        }
      },
      "title": "ApnsConfig"
    },
    "Aps": {
      "type": "object",
      "properties": {
        "alert": {
          "$ref": "#/definitions/ApsAlert"
        },
        "alert-as-string": {
          "type": "string"
        },
        "badge": {
          "type": "integer",
          "format": "int32"
        },
        "category": {
          "type": "string"
        },
        "content-available": {
          "type": "boolean"
        },
        "sound": {
          "type": "string"
        },
        "thread-id": {
          "type": "string"
        }
      },
      "title": "Aps"
    },
    "ApsAlert": {
      "type": "object",
      "properties": {
        "action-loc-key": {
          "type": "string"
        },
        "body": {
          "type": "string"
        },
        "launch-image": {
          "type": "string"
        },
        "loc-args": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "loc-key": {
          "type": "string"
        },
        "title": {
          "type": "string"
        },
        "title-loc-args": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "title-loc-key": {
          "type": "string"
        }
      },
      "title": "ApsAlert"
    },
    "CallReport": {
      "type": "object",
      "properties": {
        "cnf_fingerprint": {
          "type": "string"
        },
        "exception_message": {
          "type": "string"
        },
        "fcm_token": {
          "type": "string"
        },
        "idt_msisdn": {
          "type": "string"
        },
        "message_id": {
          "type": "string"
        },
        "success": {
          "type": "boolean"
        },
        "tmoid": {
          "type": "string"
        }
      },
      "title": "CallReport"
    },
    "Notification": {
      "type": "object",
      "properties": {
        "body": {
          "type": "string"
        },
        "title": {
          "type": "string"
        }
      },
      "title": "Notification"
    },
    "Payload": {
      "type": "object",
      "properties": {
        "data": {
          "$ref": "#/definitions/data"
        }
      }
    },
    "data": {
      "type": "object",
      "properties": {
        "push_type": {
          "type": "string"
        },
        "cta_id": {
          "type": "string"
        }
      }
    },
    "SendMessageRequest": {
      "type": "object",
      "properties": {
        "android_config": {
          "$ref": "#/definitions/AndroidConfig"
        },
        "apns_config": {
          "$ref": "#/definitions/ApnsConfig"
        },
        "data": {
          "type": "object",
          "additionalProperties": {
            "type": "string"
          }
        },
        "notification": {
          "$ref": "#/definitions/Notification"
        }
      },
      "title": "SendMessageRequest"
    },
    "SendMessageResponse": {
      "type": "object",
      "properties": {
        "calls": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/CallReport"
          }
        }
      },
      "title": "SendMessageResponse"
    }
  }
}