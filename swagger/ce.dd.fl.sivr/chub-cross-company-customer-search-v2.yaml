swagger: '2.0'
info:
  description: chub-cross-company-customer-search service finds the company customer is assocaited with based on msisdn
  version: 2.0.0
  title: chub-cross-company-customer-search
  contact:
    name: IVR Developers
    email: IVRDevelopers@T-Mobile.com
host: env.api.t-mobile.com
basePath: /sivr/chub-cross-company-customer-search/v2
schemes:
  - https
x-servers:
  - url: 'https://dlab01.core.op.api.t-mobile.com:443'
    description: dlab Server
consumes:
  - application/json
produces:
  - application/json
tags:
  - name: chub-cross-company-customer-search
paths:
  '/search':
    post:
      x-api-pattern: ExecuteFunction
      tags:
        - chub-cross-company-customer-search
      summary: 'Finds the company customer is assocaited with based on msisdn'
      description: Finds the company customer is assocaited with based on msisdn.
      operationId: getCrossCompanyCustomer
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - $ref: '#/parameters/AuthorizationParam'
        - $ref: '#/parameters/AcceptParam'
        - $ref: '#/parameters/ContentTypeParam'
        - $ref: '#/parameters/If-MatchParam'
        - $ref: '#/parameters/If-None-MatchParam'
        - $ref: '#/parameters/SessionIdParam'
        - $ref: '#/parameters/InteractionIdParam'
        - $ref: '#/parameters/WorkflowIdParam'
        - $ref: '#/parameters/ActivityIdParam'
        - $ref: '#/parameters/ApplicationIdParam'
        - $ref: '#/parameters/SenderIdParam'
        - $ref: '#/parameters/ChannelIdParam'
        - $ref: '#/parameters/ApplicationUserIdParam'
        - name: GetCrossCompanyCustomerRequest
          in: body
          description: Cross Company Customer Search Request
          required: true
          schema:
            $ref: '#/definitions/GetCrossCompanyCustomerRequest'
      responses:
        '200':
          description: Ok
          schema:
            $ref: '#/definitions/GetCrossCompanyCustomerResponse'
          examples:
           application/json:
                    msisdn: "4247449326"
                    companyIndicator: T-Mobile
                    customerType: Individual
                    msisdnStatus: Active
          headers:
            Content-Length:
              description: >-
                The Content-Length header specifies the actual length of the
                returned payload.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: '1024'
            Content-Type:
              description: >-
                The Content-Length header specifies the actual length of the
                returned payload.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: application/json
            Date:
              description: >-
                The date and time that the message was originated (in HTTP-date
                format as defined by RFC 7231 Date/Time Formats).
              x-example: 'Wed, 02 May 2018 02:59:10 GMT'
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              format: date-time
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 400
              userMessage: Bad Request
              systemMessage: Bad Request
        '401':
          description: Unauthorized Access
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 401
              userMessage: Unauthorized Access
              systemMessage: Unauthorized Access
        '404':
          description: Resource not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 404
              userMessage: Resource not found
              systemMessage: Resource not found
        '405':
          description: Method not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 405
              userMessage: Method not found
              systemMessage: Method not found
          headers:
            Allow:
              description: List of HTTP methods allowed
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: 'GET, HEAD'
        '409':
          description: Conflict
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 409
              userMessage: Conflict
              systemMessage: Conflict
        '415':
          description: Unsupported Media Type
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 415
              userMessage: Unsupported Media Type
              systemMessage: Unsupported Media Type
        '500':
          description: Internal service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 500
              userMessage: Internal service error
              systemMessage: Internal service error
        '503':
          description: Backend service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 503
              userMessage: Backend service error
              systemMessage: Backend service error
      security:
        - oauth2: []
securityDefinitions:
  oauth2:
    type: oauth2
    tokenUrl: 'https://host:env/v1/oauth2/accesstoken?grant_type=client_credentials'
    flow: application
    description: |
      oauth2 Authentication 
definitions:
  ErrorResponse:
    type: object
    title: ErrorResponse
    description: error response object which contains error details
    properties:
      code:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: error code
      systemMessage:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: system provided error message
      userMessage:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: user friendly error message
      detailLink:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: link to custom information
  GetCrossCompanyCustomerResponse:
    type: object
    description: Cross Company Customer Search Response
    properties:
        msisdn:
          type: string
          description: Customer mobile / phone number
          example: '4247449326'
          minLength: 10
          maxLength: 10
          pattern: ^(\d{10})$
        companyIndicator:
          type: string
          description: Company Indicator. T-Mobile, Sprint
          enum:
          - T-Mobile
          - Sprint
        customerType:
          type: string
          description: Type of the customer
          enum:
          - Individual
          - Business
          - Government
        msisdnStatus:
          type: string
          description: Status of the given MSISDN
          enum:
          - Active
          - Suspended
          - Cancelled
          - Reserved
          - Unknown
  GetCrossCompanyCustomerRequest:
    type: object
    description: Request to find the company customer is assocaited with based on MSISDN
    required:
     - msisdn
    properties:
        msisdn:
            type: string
            description: Customer mobile / phone number
            minLength: 10
            maxLength: 10
            pattern: ^(\d{10})$
            example: '4247449326'
        companyToSearch:
            type: string
            description: Company to search T-Mobile, Sprint, All
            enum:
            - Sprint
            - T-Mobile
            - All
parameters:
  AuthorizationParam:
    name: Authorization
    in: header
    description: OAuth 2.0 access token with the authentication type set as Bearer.
    required: true
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: Bearer xfhcXhZLhw51z76YP4fVGNnPtaUX
  AcceptParam:
    name: Accept
    in: header
    description: >-
      Content-Types that are acceptable is application/json (default assumed
      application/json)
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: application/json
  If-MatchParam:
    name: If-Match
    in: header
    description: >-
      The If-Match header specifies an ETag-related condition to be placed on
      the execution of a request.  The request will only be executed if the ETag
      of the requested resource matches one of those specified in the If-Match
      header.  Because the behavior is complex, the details for usage are
      provided at http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html .
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: xxyyzz
  If-None-MatchParam:
    name: If-None-Match
    in: header
    description: >-
      The If-None-Match header specifies an ETag-related condition to be placed
      on the execution of a request.  The request will NOT be executed if the
      ETag of the requested resource matches one of those specified in the
      If-None-Match header.  Because the behavior is complex, the details for
      usage are provided at
      http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: xxyyzz
  SessionIdParam:
    name: session-id
    in: header
    description: >-
      Unique ID for each session. This value will be used to track all
      transactions for a given session.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: tstvpsivr002-CUSP-1-2018039074905
  InteractionIdParam:
    name: interaction-id
    in: header
    description: >-
      This will be used to distinguish the processing of different accounts
      during the same call if a caller says, switch accounts.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: >-
      Initially the value is tstvpsivr002-CUSP-1-2018039074905_0. When caller
      says SwitchAccount, value will be changed to
      tstvpsivr002-CUSP-1-2018039074905_1
  WorkflowIdParam:
    name: workflow-id
    in: header
    description: This will be a value related to the path/experience the call receives
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: ACTIVATIONS
  ActivityIdParam:
    name: activity-id
    in: header
    description: 'Unique identifier for the service call, including the date/time'
    required: true
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: tstvpsivr002-CUSP-1-2018039074905_ ACTIVATION_1526595682000
  ApplicationUserIdParam:
    name: application-user-id
    in: header
    description: Used to identify the calling application.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: ivruser
  ApplicationIdParam:
    name: application-id
    in: header
    description: >- 
      calling application or system
    required: true
    minLength: 1
    maxLength: 100
    type: string
    x-example: REBELLION
    pattern: .*?
  SenderIdParam:
    name: sender-id
    in: header
    description: >- 
       The related business partner or T-Mobile organization
    required: true
    minLength: 1
    maxLength: 100
    type: string
    x-example: QVXP
    pattern: .*?
  ChannelIdParam:
    name: channel-id
    in: header
    description: >- 
     categorization of access or business stream
    required: true
    minLength: 1
    maxLength: 100
    type: string
    x-example: IVR
    pattern: .*?
  ContentTypeParam:
    name: Content-Type
    in: header
    description: Media type of the resource.
    required: true
    type: string
    x-example: application/json
    minLength: 1
    maxLength: 100
    pattern: .*?
