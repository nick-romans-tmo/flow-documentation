swagger: '2.0'
info:
  description: This API will add the Msisdn and Pilot indicator values
  version: 1.0.0
  contact:
    name: IVR Developers
    email: IVRDevelopers@T-Mobile.com
  title: ivr-pilot-v1
host: env.api.t-mobile.com
basePath: "/sivr/ivr-pilot/v1"
tags:
  - name: ivr-pilot-v1
    description: >-
      This API will add the Msisdn and Pilot indicator values
schemes:
  - https
x-servers:
  - url: 'https://dlab01.core.op.api.t-mobile.com:443'
    description: dlab Server
consumes:
  - application/json
produces:
  - application/json
paths:
  '/':
    post:
      x-api-pattern: ExecuteFunction
      description: Adds the the pilot indicator and MSISDN values.
      tags:
        - ivr-pilot-v1
      summary: This API  will add pilot indicator and MSISDN values.
      operationId: addPilotIndicator
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - $ref: '#/parameters/AuthorizationParam'
        - $ref: '#/parameters/AcceptParam'
        - $ref: '#/parameters/ContentTypeParam'
        - $ref: '#/parameters/If-MatchParam'
        - $ref: '#/parameters/If-None-MatchParam'
        - $ref: '#/parameters/SessionIdParam'
        - $ref: '#/parameters/InteractionIdParam'
        - $ref: '#/parameters/WorkflowIdParam'
        - $ref: '#/parameters/ActivityIdParam'
        - $ref: '#/parameters/StateIdParam'
        - $ref: '#/parameters/ApplicationUserIdParam'
        - $ref: '#/parameters/ApplicationIdParam'
        - $ref: '#/parameters/SenderIdParam'
        - $ref: '#/parameters/ChannelIdParam'
        - name: addPilotIndicatorRequest
          in: body
          description: Request parameters for addPilotIndicatorRequest.
          required: true
          schema:
            $ref: '#/definitions/AddPilotIndicatorRequest'
      responses:
        '204':
          description: No Content
          headers:
            Cache-Control:
              description: 'request successful, no response payload returned'
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: no-cache
            ETag:
              description: >-
                The ETag header specifies the unique entity tag value for the
                returned resource.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: Unique entity tag
            Content-Length:
              description: >-
                The Content-Length header specifies the actual length of the
                returned payload.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: '1024'
            Expires:
              description: >-
                The Expires header gives the date-time after which the response
                is considered stale.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: '0'
            Date:
              description: >-
                The date and time that the message was originated (in HTTP-date
                format as defined by RFC 7231 Date/Time Formats).
              x-example: 'Wed, 02 May 2018 02:59:10 GMT'
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              format: date-time
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
             code: 400
             userMessage: Bad Request
             systemMessage: Bad Request
        '401':
          description: Unauthorized Access
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 401
              userMessage: Unauthorized Access
              systemMessage: Unauthorized Access
        '404':
          description: Resource not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 404
              userMessage: Resource not found
              systemMessage: Resource not found
        '405':
          description: Method not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 405
              userMessage: Method not found
              systemMessage: Method not
          headers:
            Allow:
              description: List of HTTP methods allowed
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: 'GET, HEAD'
            service-transaction-id:
              description: >-
                unique id generated by server or echoed back by server if
                received in request.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: 23409209723
        '409':
          description: Conflict
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 409
              userMessage: Conflict
              systemMessage: Conflict
        '415':
          description: Unsupported Media Type
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 415
              userMessage: Unsupported Media Type
              systemMessage: Unsupported Media Type
        '500':
          description: Internal service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 500
              userMessage: Internal service error
              systemMessage: Internal service error
        '503':
          description: Backend service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 503
              userMessage: Backend service error
              systemMessage: Backend service error
      security:
        - oauth2: []
definitions:
  ErrorResponse:
    type: object
    title: ErrorResponse
    description: Error response object which contains error details
    properties:
      code:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: Error code
      systemMessage:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: System provided error message
      userMessage:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: User friendly error message
      detailLink:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: Link to custom information
  AddPilotIndicatorRequest:
    type: object
    description: Object containing request parameters
    required:
      - msisdn
      - indicator
    properties:
      msisdn:
        type: string
        description: 10 digit mobile number of the caller
        example: 4445556666
        minLength: 10
        maxLength: 10
        pattern: ^(\d{10})$
      indicator:
        type: string
        description: PilotIndicator is a flag value.The IVR Pilot Indicator for Postpaid system is P.Database accepts both capital P and capital R as value
        example: P
        enum : [P,R]
securityDefinitions:
  oauth2:
    type: oauth2
    tokenUrl: 'https://host:env/v1/oauth2/accesstoken?grant_type=client_credentials'
    flow: application
    description: |
      oauth2 Authentication 
parameters:
  AuthorizationParam:
    name: Authorization
    in: header
    description: OAuth 2.0 access token with the authentication type set as Bearer.
    required: true
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: Bearer xfhcXhZLhw51z76YP4fVGNnPtaUX
  AcceptParam:
    name: Accept
    in: header
    description: >-
      Content-Types that are acceptable is application/json (default assumed
      application/json)
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: application/json
  If-MatchParam:
    name: If-Match
    in: header
    description: >-
      The If-Match header specifies an ETag-related condition to be placed on
      the execution of a request.  The request will only be executed if the ETag
      of the requested resource matches one of those specified in the If-Match
      header.  Because the behavior is complex, the details for usage are
      provided at http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html .
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: xxyyzz
  If-None-MatchParam:
    name: If-None-Match
    in: header
    description: >-
      The If-None-Match header specifies an ETag-related condition to be placed
      on the execution of a request.  The request will NOT be executed if the
      ETag of the requested resource matches one of those specified in the
      If-None-Match header.  Because the behavior is complex, the details for
      usage are provided at
      http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: xxyyzz
  SessionIdParam:
    name: session-id
    in: header
    description: >-
      Unique ID for each session. This value will be used to track all
      transactions for a given session.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: tstvpsivr002-CUSP-1-2018039074905
  InteractionIdParam:
    name: interaction-id
    in: header
    description: >-
      This will be used to distinguish the processing of different accounts
      during the same call if a caller says, switch accounts.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: >-
      Initially the value is tstvpsivr002-CUSP-1-2018039074905_0. When caller
      says SwitchAccount, value will be changed to
      tstvpsivr002-CUSP-1-2018039074905_1
  WorkflowIdParam:
    name: workflow-id
    in: header
    description: This will be a value related to the path/experience the call receives
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: ACTIVATIONS
  ActivityIdParam:
    name: activity-id
    in: header
    description: 'Unique identifier for the service call, including the date/time'
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: tstvpsivr002-CUSP-1-2018039074905_ ACTIVATION_1526595682000
  StateIdParam:
    name: state-id
    in: header
    description: Provides the state id for telemetry purposes
    required: false
    minLength: 1
    maxLength: 100
    type: string
    x-example: PS1089_DB
    pattern: .*?
  ApplicationUserIdParam:
    name: application-user-id
    in: header
    description: Used to identify the calling application.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: ivruser
  ApplicationIdParam:
    name: application-id
    in: header
    description: >- 
      calling application or system
    required: false
    minLength: 1
    maxLength: 100
    type: string
    x-example: REBELLION
    pattern: .*?
  SenderIdParam:
    name: sender-id
    in: header
    description: >- 
       The related business partner or T-Mobile organization
    required: false
    minLength: 1
    maxLength: 100
    type: string
    x-example: QVXP
    pattern: .*?
  ChannelIdParam:
    name: channel-id
    in: header
    description: >- 
     categorization of access or business stream
    required: false
    minLength: 1
    maxLength: 100
    type: string
    x-example: IVR
    pattern: .*?
  ContentTypeParam:
    name: Content-Type
    in: header
    description: Media type of the resource.
    required: true
    type: string
    x-example: application/json
    minLength: 1
    maxLength: 100
    pattern: .*?