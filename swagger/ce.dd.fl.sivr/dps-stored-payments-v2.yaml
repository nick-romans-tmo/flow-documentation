swagger: '2.0'
info:
  description: This API provides the stored payment search and verify for the given customer.
  version: 2.0.0
  title: dps-stored-payments xAPI
  contact:
    name: IVR Developers
    email: IVRDevelopers@T-Mobile.com
host: env.api.t-mobile.com
basePath: /sivr/dps-stored-payments/v2
tags:
  - name: dps-stored-payments
    description: This API provides the stored payment search and verify for the given customer from DPS
schemes:
  - https
x-servers:
  - url: 'https://dlab01.core.op.api.t-mobile.com:443'
    description: dlab Server
consumes:
  - application/json
produces:
  - application/json
paths:
  '/search':
    post:
      x-api-pattern: ExecuteFunction
      tags:
        - dps-stored-payments
      summary: 'Get stored payment instrument for the given customer'
      description: Pass BAN and MemberId to retrieve stored payment instruments.
      operationId: searchStoredPaymentInstrument
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - $ref: '#/parameters/AuthorizationParam'
        - $ref: '#/parameters/AcceptParam'
        - $ref: '#/parameters/If-MatchParam'
        - $ref: '#/parameters/If-None-MatchParam'
        - $ref: '#/parameters/SessionIdParam'
        - $ref: '#/parameters/InteractionIdParam'
        - $ref: '#/parameters/WorkflowIdParam'
        - $ref: '#/parameters/ActivityIdParam'
        - $ref: '#/parameters/ApplicationIdParam'
        - $ref: '#/parameters/SenderIdParam'
        - $ref: '#/parameters/ChannelIdParam'
        - $ref: '#/parameters/StateIdParam'
        - $ref: '#/parameters/ApplicationUserIdParam'
        - name: SearchStoredPaymentInstrumentRequest
          in: body
          description: The search parameters
          required: true
          schema:
            $ref: '#/definitions/SearchStoredPaymentInstrumentRequest'
        - name: Content-Type
          in: header
          description: Media type of the resource.
          required: true
          type: string
          minLength: 1
          maxLength: 100
          pattern: .*?
          x-example: application/json
      responses:
        '200':
          description: Ok
          schema:
            $ref: '#/definitions/SearchStoredPaymentInstrumentResponse'
          examples:
           application/json:
                    specificationsName: customerBlocked
                    specificationsValue: ALL
                    payment:
                      - paymentInstrument:
                          bankPayment: null
                          cardPayment:
                            typeCode: AMERICANEXPRESS
                            expirationMonthYear: '20190201'
                            expirationStatus: 'EXPIRED'
                            paymentCardAlias: '300020228101347'
                            zip: '03112'
                        paymentMethodStatus: VALID
                        paymentMethodCode: CC
                        walletSize: 10
                        walletId: 'WTdbef59e1a1bc6e26'
                        migrationIndicator: true
                        verificationIndicator: true
                        autopayEnabledIndicator: true
                        defaultPaymentMethodIndicator: true
                      - paymentInstrument:
                          bankPayment:
                            routingNumber: '042206503'
                            bankAccountAlias: '99999900001421573477'
                          cardPayment: null
                        paymentMethodStatus: VALID
                        paymentMethodCode: CHECK
                        walletSize: 10
                        walletId: 'ZAdbef59e1a1bc6e26'
                        migrationIndicator: true
                        verificationIndicator: true
                        autopayEnabledIndicator: true
                        defaultPaymentMethodIndicator: true
                      - paymentInstrument:
                          bankPayment: null
                          cardPayment:
                            typeCode: VISA
                            expirationMonthYear: '20200701'
                            expirationStatus: 'EXPIRED'
                            paymentCardAlias: '4000020283504448'
                            zip: '30346'
                        paymentMethodStatus: VALID/INVALID
                        paymentMethodCode: CC
                        walletSize: 10
                        walletId: 'XYdbef59e1a1bc6e26'
                        migrationIndicator: true
                        verificationIndicator: true
                        autopayEnabledIndicator: true
                        defaultPaymentMethodIndicator: true
                      - paymentInstrument:
                          bankPayment:
                            routingNumber: '321370707'
                            bankAccountAlias: '99999900000126145559'
                          cardPayment: null
                        paymentMethodStatus: VALID/INVALID
                        paymentMethodCode: CHECK
                        walletSize: 10
                        walletId: 'YTdbef59e1a1bc6e26'
                        migrationIndicator: true
                        verificationIndicator: true
                        autopayEnabledIndicator: true
                        defaultPaymentMethodIndicator: true
          headers:
            Content-Length:
              description: >-
                The Content-Length header specifies the actual length of the
                returned payload.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: '1024'
            Content-Type:
              description: >-
                The Content-Length header specifies the actual length of the
                returned payload.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: application/json
            Date:
              description: >-
                The date and time that the message was originated (in HTTP-date
                format as defined by RFC 7231 Date/Time Formats).
              x-example: 'Wed, 02 May 2018 02:59:10 GMT'
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              format: date-time
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 400
              userMessage: Bad Request
              systemMessage: Bad Request
        '401':
          description: Unauthorized Access
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 401
              userMessage: Unauthorized Access
              systemMessage: Unauthorized Access
        '404':
          description: Resource not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 404
              userMessage: Resource not found
              systemMessage: Resource not found
        '405':
          description: Method not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 405
              userMessage: Method not found
              systemMessage: Method not found
          headers:
            Allow:
              description: List of HTTP methods allowed
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: 'GET, HEAD'
        '409':
          description: Conflict
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 409
              userMessage: Conflict
              systemMessage: Conflict
        '415':
          description: Unsupported Media Type
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 415
              userMessage: Unsupported Media Type
              systemMessage: Unsupported Media Type
        '500':
          description: Internal service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 500
              userMessage: Internal service error
              systemMessage: Internal service error
        '503':
          description: Backend service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 503
              userMessage: Backend service error
              systemMessage: Backend service error
      security:
        - oauth2: []
  '/verify-payment-method':
    post:
      x-api-pattern: ExecuteFunction
      tags:
        - dps-stored-payments
      summary: 'Get Payment Methods Verification status.'
      description: This API performs verification for stored payment instrument.
      operationId: verifyPaymentMethod
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - $ref: '#/parameters/AuthorizationParam'
        - $ref: '#/parameters/AcceptParam'
        - $ref: '#/parameters/If-MatchParam'
        - $ref: '#/parameters/If-None-MatchParam'
        - $ref: '#/parameters/SessionIdParam'
        - $ref: '#/parameters/InteractionIdParam'
        - $ref: '#/parameters/WorkflowIdParam'
        - $ref: '#/parameters/ActivityIdParam'
        - $ref: '#/parameters/ApplicationIdParam'
        - $ref: '#/parameters/SenderIdParam'
        - $ref: '#/parameters/ChannelIdParam'
        - $ref: '#/parameters/StateIdParam'
        - name: VerifyPaymentMethodRequest
          in: body
          description: The search parameters
          required: true
          schema:
            $ref: '#/definitions/VerifyPaymentMethodRequest'
        - name: Content-Type
          in: header
          description: Media type of the resource.
          required: true
          type: string
          minLength: 1
          maxLength: 100
          pattern: .*?
          x-example: application/json
      responses:
        '200':
          description: Ok
          schema:
            $ref: '#/definitions/VerifyPaymentMethodResponse'
          examples:
           application/json:
                    verificationIndicator: true/false
                    reasonCode: DPS CODE
                    statusCode: SUCCESS
          headers:
            Content-Length:
              description: >-
                The Content-Length header specifies the actual length of the
                returned payload.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: '1024'
            Content-Type:
              description: >-
                The Content-Length header specifies the actual length of the
                returned payload.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: application/json
            Date:
              description: >-
                The date and time that the message was originated (in HTTP-date
                format as defined by RFC 7231 Date/Time Formats).
              x-example: 'Wed, 02 May 2018 02:59:10 GMT'
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              format: date-time
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 400
              userMessage: Bad Request
              systemMessage: Bad Request
        '401':
          description: Unauthorized Access
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 401
              userMessage: Unauthorized Access
              systemMessage: Unauthorized Access
        '404':
          description: Resource not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 404
              userMessage: Resource not found
              systemMessage: Resource not found
        '405':
          description: Method not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 405
              userMessage: Method not found
              systemMessage: Method not found
          headers:
            Allow:
              description: List of HTTP methods allowed
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: 'GET, HEAD'
        '409':
          description: Conflict
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 409
              userMessage: Conflict
              systemMessage: Conflict
        '415':
          description: Unsupported Media Type
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 415
              userMessage: Unsupported Media Type
              systemMessage: Unsupported Media Type
        '500':
          description: Internal service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 500
              userMessage: Internal service error
              systemMessage: Internal service error
        '503':
          description: Backend service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 503
              userMessage: Backend service error
              systemMessage: Backend service error
      security:
        - oauth2: []
securityDefinitions:
  oauth2:
    type: oauth2
    tokenUrl: 'https://host:env/v2/oauth2/accesstoken?grant_type=client_credentials'
    flow: application
    description: |
      oauth2 Authentication 
definitions:
  ErrorResponse:
    type: object
    title: ErrorResponse
    description: error response object which contains error details
    properties:
      code:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: error code
      systemMessage:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: system provided error message
      userMessage:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: user friendly error message
      detailLink:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: link to custom information
  SearchStoredPaymentInstrumentResponse:
    description: Response details for stored payment instrument
    type: object
    properties:
      specificationsName:
        description: Describes Name for Name Value Pair.
        type: string
        minLength: 1
        maxLength: 100
        pattern: ^([a-zA-Z0-9]{1,100})$
        x-example: "customerBlocked"
      specificationsValue:
        description: Describes Value for Name Value Pair.
        type: string
        enum: [CHECK,CARD,ALL]
        minLength: 3
        maxLength: 5
        x-example: CHECK/CARD/ALL
      payment:
        description: Payment details
        type: array
        items:
          $ref: '#/definitions/Payment'
  
  Payment:
    type: object
    description: It includes paymentInstrument,paymentMethodStatus,paymentMethodCode,autoPayEnabledIndicator in the response.
    properties:
      paymentInstrument:
        $ref: '#/definitions/PaymentInstrument'
      paymentMethodStatus:
        description: 'Status of payment method, such as VALID, INVALID'
        type: string
        enum: [VALID,INVALID]
        minLength: 5
        maxLength: 7
        example: "INVALID"
      paymentMethodCode:
        description: 'Describes the Payment method type, such as CC - Credit Card,HD - Hybrid Debit,HC - Hybrid Credit,DC - Debit Card,PD - Prepaid Debit or PC - Prepaid Credit.'
        type: string
        example: "CC"
        enum: [CC, PC, HC, DC, PD, HD,CHECK]
        minLength: 2
        maxLength: 5
      walletSize:
        description: 'Current size of the wallet. If the capacity is 10 then size can be less than or equal to 10.'
        type: string
        minLength: 1
        maxLength: 2
        pattern: ^[0-9]{1,2}$
        example: 5
      walletId:
        description: 'To indicate the WalletID, which is unique for each wallet.'
        type: string
        minLength: 1
        maxLength: 50
        pattern: ^([a-zA-Z0-9]{1,50})$
        example: "WTdbef59e1a1bc6e26" 
      migrationIndicator:
        type: boolean
        description: 'To indicate if the Instrument is migrated,it returns true if the Instrument is migrated or it will return false.'
        example: true/false
        minLength: 4
        maxLength: 5
        pattern: ^(true|false)$
      verificationIndicator:
        type: boolean
        description: 'To indicate if the Instrument is verified, it returns true if the Instrument is verified or it will return false.'
        example: true/false
        minLength: 4
        maxLength: 5
        pattern: ^(true|false)$
      autopayEnabledIndicator:
        description: 'Indicates whether the payment method is enabled for automatic payment true/false.'
        type: string
        example: true/false
        minLength: 4
        maxLength: 5
        pattern: ^(true|false)$
      defaultPaymentMethodIndicator:
        description: 'To indicate if this payment method is default.'
        type: boolean
        example: true/false
        minLength: 4
        maxLength: 5
        pattern: ^(true|false)$
  PaymentInstrument:
    type: object
    description: 'It includes the payment instrument details for Bank and Card Payments.'
    properties:
      bankPayment:
        $ref: '#/definitions/BankPayment'
      paymentCard:
        $ref: '#/definitions/CardPayment'
  BankPayment:
    type: object
    description: 'It includes Routing number,Bankaccountalias and AccountNumber.'
    properties:
      routingNumber:
        description: 'Routing Number of the bank on whom the payment instrument is drawn.'
        type: string
        minLength: 1
        maxLength: 9
        pattern: ^[0-9]{1,9}$
        example: "234567656"
      bankAccountAlias:
        description: 'Bank account alias used by the customer.'
        type: string
        minLength: 1
        maxLength: 100
        pattern: ^[a-zA-Z0-9_\s]{1,100}$
        example: "99999900000069610015"
  CardPayment:
    type: object
    description: 'It includes Typecode,Expiration Month-Year,Expiration Status,PaymentCardAlias,CardNumber and Zip.'
    properties:
      typeCode:
        description: 'Type of payment Card. Examples are Visa, MasterCard, Debit etc.'
        type: string
        enum: [VISA,MASTERCARD,DEBIT,DISCOVER,AMERICANEXPRESS]
        minLength: 4
        maxLength: 15
        example: "AMERICANEXPRESS"
      expirationMonthYear:
        description: 'Expiration month and year of the card.'
        type: string
        minLength: 8
        maxLength: 8
        pattern: ^[0-9]{8}$
        example: "20280301"  
      expirationStatus:
        description: 'ExpirationStatus of the Card.'
        type: string
        minLength: 7
        maxLength: 8
        pattern: ^([a-zA-Z]{7,8})$
        example: "EXPIRED"
      paymentCardAlias:
        description: 'A nickname assigned to the customer as an alternative way to identify a stored payment card.'
        type: string
        minLength: 1
        maxLength: 100
        pattern: ^[a-zA-Z0-9_\s]{1,100}$
        example: "4000020283504448"
      zip:
        description: 'To indicate the ZipCode.'
        type: string
        minLength: 5
        maxLength: 5
        pattern: ^[0-9]{5}$
        example: "98008"
  SearchStoredPaymentInstrumentRequest:
    type: object
    description: 'It includes customerId,memberId,businessSegment,productGroup,businessUnit,programCode and billerCode.'
    properties:
      memberId:
        description: 'MSISDN of customer.'
        type: string
        minLength: 10
        maxLength: 10
        pattern: ^([0-9]{10})$
        example: "6462870172"
      customerId:
        description: 'Customers billing account number.'
        type: string
        minLength: 9
        maxLength: 9
        pattern:  ^([0-9]{9})$
        example: "110002005"
      businessSegment:
        type: string
        description: 'Businesssegment Valid values - PREPAID, POSTPAID, ANONYMOUS, B2B, DEALER, NATIONALRETAIL.'
        enum: [PREPAID,POSTPAID,ANONYMOUS,B2B,DEALER,NATIONALRETAIL]
        example: "POSTPAID"
        minLength: 3
        maxLength: 14
      productGroup:
        enum: [HardGoods,SoftGoods]
        description: 'Product grouping such as HardGoods or SoftGoods.'
        type: string
        minLength: 1
        maxLength: 100
        example: "HardGoods"
      businessUnit:
        type: string
        description: 'The T-Mobile business unit, such as TMOBILE, METROPCS.'
        example: "TMOBILE"
        enum: [TMOBILE, METROPCS]
        minLength: 6
        maxLength: 8
      programCode:
        type: string
        description: 'Program identifier such as ZCP, ZCNP, U2POSTCP, U2PRECP, U2PRECNP, U2POSTCNP, U1POSTLEGACY.'
        example: "U1POSTLEGACY"
        enum: [ZCP,ZCNP,U2POSTCP,U2PRECP,U2PRECNP,U2POSTCNP,U1POSTLEGACY]
        minLength: 3
        maxLength: 12
      billerCode:
        type: string
        description: 'Billing system identifier such as SAMSON, ERICSSON, OFSLL, EIP.'
        enum: [SAMSON, ERICSSON, OFSLL, EIP]
        example: "SAMSON"
        minLength: 3
        maxLength: 8
  VerifyPaymentMethodResponse:
    type: object
    description: 'Response details for Verify Payment Method'
    required: 
     - statusCode
     - reasonCode
     - verificationIndicator
    properties:
      statusCode:
        type: string
        description: 'Status of Transaction such as SUCCESS, FAILED, COMPLETED.'
        minLength: 3
        maxLength: 15
        example: "SUCCESS"
        pattern: ^([a-zA-Z]{3,15})$
      reasonCode:
        type: string
        description: Status reason code
        example: "2491"
        minLength: 4
        maxLength: 4
        pattern: ^([0-9]{4})$
      verificationIndicator:
        type: boolean
        description: 'To indicate if the Instrument is verified, it returns true if the Instrument is verified or it will return false.'
        example: true/false
        minLength: 4
        maxLength: 5
        pattern: ^(true|false)$
  VerifyPaymentMethodRequest:
    type: object
    description: 'It includes paymentMethodCode, businessSegment,productGroup,businessUnit,programCode,billerCode,memberId,orderTypes and paymentInstrument.'
    required: 
     - paymentMethodCode
     - memberId
     - businessSegment
     - programCode
     - businessUnit
     - billerCode
    properties:
      paymentMethodCode:
        description: 'Describes the BIN payment type such as CC - Credit Card,HD - Hybrid Debit,HC - Hybrid Credit,DC - Debit Card,PD - Prepaid Debit or PC - Prepaid Credit.'
        type: string
        example: CC
        enum: [CC, PC, HC, DC, PD, HD,CHECK]
        minLength: 2
        maxLength: 5
      programCode:
        type: string
        description: 'Program identifier such as ZCP, ZCNP, U2POSTCP, U2PRECP, U2PRECNP, U2POSTCNP, U1POSTLEGACY.'
        example: "U1POSTLEGACY"
        enum: [ZCP,ZCNP,U2POSTCP,U2PRECP,U2PRECNP,U2POSTCNP,U1POSTLEGACY]
        minLength: 3
        maxLength: 12
      businessSegment:
        type: string
        description: 'Businesssegment Valid values - PREPAID, POSTPAID, ANONYMOUS, B2B, DEALER, NATIONALRETAIL.'
        enum: [PREPAID,POSTPAID,ANONYMOUS,B2B,DEALER,NATIONALRETAIL]
        example: "POSTPAID"
        minLength: 3
        maxLength: 14
      productGroup:
        enum: [HardGoods,SoftGoods]
        description: 'Product grouping such as HardGoods or SoftGoods.'
        type: string
        minLength: 1
        maxLength: 100
        example: "HardGoods"
      businessUnit:
        type: string
        description: 'The T-Mobile business unit, such as TMOBILE, METROPCS.'
        example: "TMOBILE"
        enum: [TMOBILE, METROPCS]
        minLength: 6
        maxLength: 8
      billerCode:
        type: string
        description: 'Billing system identifier such as SAMSON, ERICSSON, OFSLL, EIP.'
        enum: [SAMSON, ERICSSON, OFSLL, EIP]
        example: "SAMSON"
        minLength: 3
        maxLength: 8
      orderTypes:
        type: string
        description: 'Type of orders such as EQUIPMENT UPGRADE, ACCESSORY PURCHASE, AALREFUND.'
        enum: [AALREFUND,EIP,EQUIPMENT UPGRADE,ACCESSORY PURCHASE]
        example: "AALREFUND"
        minLength: 3
        maxLength: 23
      memberId:
        description: 'MSISDN of customer'
        type: string
        minLength: 10
        maxLength: 10
        pattern: ^([0-9]{10})$
        example: "6462870172"
      verifyPaymentInstrument:
        $ref: '#/definitions/VerifyPaymentInstrument'
  VerifyPaymentInstrument:
    type: object
    description: 'It includes the payment instrument details for Bank and Card Payments.'
    properties:
      verifyBankPayment:
        $ref: '#/definitions/VerifyBankPayment'
      verifyCardPayment:
        $ref: '#/definitions/VerifyCardPayment'
  VerifyBankPayment:
    type: object
    description: 'It includes Routing number,Bankaccountalias and AccountNumbe.'
    properties:
      routingNumber:
        description: 'Routing Number of the bank on whom the payment instrument is drawn.'
        type: string
        minLength: 1
        maxLength: 9
        pattern: ^[0-9]{1,9}$
        example: "234567656"
      bankAccountAlias:
        description: 'Bank account alias used by the customer.'
        type: string
        minLength: 1
        maxLength: 100
        pattern: ^[a-zA-Z0-9_\s]{1,100}$
        example: "99999900000069610015"
      accountNumber:
        description: 'Account number in the bank.'
        type: string
        minLength: 512
        maxLength: 512
        pattern: ^([a-zA-Z0-9]{512})$
        example: "7B446E9908B844DE3024BE7F37F1F42CC813EDC00EFD520B29BF6B43C7F869BB7314A5DF25AB817F7FBB9F224FF4B051978E9C17DD4D5AE9F495F83BEE3C3A36D3A9A3447AC442E934A7A857EE71573C4E0E5D125505177131435721758649DAA00E641EE975FE944C3AC2FCF49893A3EDEB900457687FBCB927FED89CA0BDB5E6BD18625F4A9E00E2E09E6B43AEA43BF510EDC2ECCD9CBB88294EBACE5DA3A268831727358ACCC0D5203EF818E79C73DDD38CC930C2D8C342D40A23CFC0D1394E74201FF66CE53FC0A3C7FED8C32CD6DBC05B1AF6D45FBD11E94911F5E38448D25E193E27C01192E9397BE145A1C3EFCB7B205B6E9C3B0DDB2E8F9658CAEEE3"
  VerifyCardPayment:
    type: object
    description: 'It includes Typecode,Expiration Month-Year,Expiration Status,PaymentCardAlias,CardNumber and Zip.'
    properties:
      paymentCardAlias:
        description: 'A nickname assigned to the customer as an alternative way to identify a stored payment card.'
        type: string
        minLength: 1
        maxLength: 100
        pattern: ^[a-zA-Z0-9_\s]{1,100}$
        example: "4000020283504448"
      cardNumber:
        description: 'Payment card number.'
        type: string
        minLength: 512
        maxLength: 512
        pattern: ^([a-zA-Z0-9]{512})$
        example: "7B446E9908B844DE3024BE7F37F1F42CC813EDC00EFD520B29BF6B43C7F869BB7314A5DF25AB817F7FBB9F224FF4B051978E9C17DD4D5AE9F495F83BEE3C3A36D3A9A3447AC442E934A7A857EE71573C4E0E5D125505177131435721758649DAA00E641EE975FE944C3AC2FCF49893A3EDEB900457687FBCB927FED89CA0BDB5E6BD18625F4A9E00E2E09E6B43AEA43BF510EDC2ECCD9CBB88294EBACE5DA3A268831727358ACCC0D5203EF818E79C73DDD38CC930C2D8C342D40A23CFC0D1394E74201FF66CE53FC0A3C7FED8C32CD6DBC05B1AF6D45FBD11E94911F5E38448D25E193E27C01192E9397BE145A1C3EFCB7B205B6E9C3B0DDB2E8F9658CAEEE3"
parameters:
  AuthorizationParam:
    name: Authorization
    in: header
    description: JWT access token with the authentication type set as Bearer.
    required: true
    type: string
    minLength: 1
    pattern: .*?
    x-example: Bearer Wqwgh1Fx44D3tSbG0XWKbTfIJPh0
  ActivityIdParam:
    name: activity-id
    in: header
    description: 'Unique identifier for the service call, including the date/time'
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: tstvpsivr002-CUSP-1-2018039074905_ ACTIVATION_1526595682000
  AcceptParam:
    name: Accept
    in: header
    description: >-
      Content-Types that are acceptable is application/json (default assumed
      application/json)
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: application/json
  If-MatchParam:
    name: If-Match
    in: header
    description: >-
      The if-match header specifies an ETag-related condition to be placed on
      the execution of a request.  The request will only be executed if the ETag
      of the requested resource matches one of those specified in the if-match
      header.  Because the behavior is complex, the details for usage are
      provided at http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html .
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: xxyyzz
  If-None-MatchParam:
    name: If-None-Match
    in: header
    description: >-
      The if-none-match header specifies an ETag-related condition to be placed
      on the execution of a request.  The request will NOT be executed if the
      ETag of the requested resource matches one of those specified in the
      if-none-match header.  Because the behavior is complex, the details for
      usage are provided at
      http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: xxyyzz
  SessionIdParam:
    name: session-id
    in: header
    description: >-
      Unique ID for each session. This value will be used to track all
      transactions for a given session.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: tstvpsivr002-CUSP-1-2018039074905
  InteractionIdParam:
    name: interaction-id
    in: header
    description: >-
      This will be used to distinguish the processing of different accounts
      during the same call if a caller says, switch accounts.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: >-
      Initially the value is tstvpsivr002-CUSP-1-2018039074905_0. When caller
      says SwitchAccount, value will be changed to
      tstvpsivr002-CUSP-1-2018039074905_1
  WorkflowIdParam:
    name: workflow-id
    in: header
    description: This will be a value related to the path/experience the call receives
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: LOGIN
  ChannelIdParam:
    name: channel-id
    in: header
    description: >- 
     categorization of access or business stream
    required: true
    minLength: 1
    maxLength: 100
    type: string
    x-example: IVR
    pattern: .*?
  ApplicationIdParam:
    name: application-id
    in: header
    description: >- 
      calling application or system
    required: true
    minLength: 1
    maxLength: 100
    type: string
    x-example: REBELLION
    pattern: .*?
  SenderIdParam:
    name: sender-id
    in: header
    description: >- 
       The related business partner or T-Mobile organization
    required: true
    minLength: 1
    maxLength: 100
    type: string
    x-example: QVXP
    pattern: .*?
  StateIdParam:
    name: state-id
    in: header
    description: >- 
     Name of the table
    required: false
    minLength: 1
    maxLength: 100
    type: string
    x-example: PS5020_DM
    pattern: .*?
  ApplicationUserIdParam:
    name: application-user-id
    in: header
    description: >-
     Used to identify the calling application
    required: false
    minLength: 1
    maxLength: 100
    type: string
    x-example: ivruser
    pattern: .*?