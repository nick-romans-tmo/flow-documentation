swagger: '2.0'
info:
  description: >-
    This API will fetch configuration parameters for SIVR specific to a server name and product.
  version: 1.0.0
  title: ivr-system-configuration-v1
  contact:
    name: IVR Developers
    email: IVRDevelopers@T-Mobile.com
host: env.api.t-mobile.com
basePath: /sivr/ivr-system-configuration/v1
x-servers:
  - url: 'https://dlab01.core.op.api.t-mobile.com:443'
    description: dlab Server
schemes:
  - https
consumes:
  - application/json
produces:
  - application/json
tags:
  - name: ivr-system-configuration-v1
    description: Retrieve configuration parameters from Reference DB in IVR.
paths:
  '/search/':
    post:
      x-api-pattern: ExecuteFunction
      tags:
        - ivr-system-configuration-v1
      summary: Retrieve configuration parameters by server name and product
      description: Pass server name and product
      operationId: getAppConfiguration
      produces:
        - application/json
      parameters:
        - $ref: '#/parameters/AuthorizationParam'
        - $ref: '#/parameters/AcceptParam'
        - $ref: '#/parameters/ContentTypeParam'
        - $ref: '#/parameters/If-MatchParam'
        - $ref: '#/parameters/If-None-MatchParam'
        - $ref: '#/parameters/SessionIdParam'
        - $ref: '#/parameters/InteractionIdParam'
        - $ref: '#/parameters/WorkflowIdParam'
        - $ref: '#/parameters/ActivityIdParam'
        - $ref: '#/parameters/StateIdParam'
        - $ref: '#/parameters/ApplicationUserIdParam'
        - $ref: '#/parameters/ApplicationIdParam'
        - $ref: '#/parameters/SenderIdParam'
        - $ref: '#/parameters/ChannelIdParam'
        - name: getAppConfigurationRequest
          description: Request parameters for ivr-system-configuration
          in: body
          required: true
          schema:
            $ref: '#/definitions/GetAppConfigurationRequest'
      responses:
        '200':
          description: Ok
          schema:
            $ref: '#/definitions/GetAppConfigurationResponse'
          examples:
           application/json:
           
                      server: TSTASSIVR001
                      product : POSTPAID
                      ConfigurationValues :
                       -
                        indicator: boolean
                        configKey: HC_TimeOut_GetIMEI
                        configValue: "http://txdevasnete01:88/SIVR/Prepaid/Reference.asmx"
                     
          headers:
            Content-Length:
              description: >-
                The Content-Length header specifies the actual length of the
                returned payload.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: '1024'
            Content-Type:
              description: >-
                The Content-Length header specifies the actual length of the
                returned payload.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: application/json
            Date:
              description: >-
                The date and time that the message was originated (in HTTP-date
                format as defined by RFC 7231 Date/Time Formats).
              x-example: 'Wed, 02 May 2018 02:59:10 GMT'
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              format: date-time
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 400
              userMessage: Bad Request
              systemMessage: Bad Request
        '401':
          description: Unauthorized Access
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 401
              userMessage: Unauthorized Access
              systemMessage: Unauthorized Access
        '404':
          description: Resource not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 404
              userMessage: Resource not found
              systemMessage: Resource not found
        '405':
          description: Method not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 405
              userMessage: Method not found
              systemMessage: Method not found
          headers:
            Allow:
              description: List of HTTP methods allowed
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: 'GET, HEAD'
        '409':
          description: Conflict
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 409
              userMessage: Conflict
              systemMessage: Conflict
        '415':
          description: Unsupported Media Type
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 415
              userMessage: Unsupported Media Type
              systemMessage: Unsupported Media Type
        '500':
          description: Internal service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 500
              userMessage: Internal service error
              systemMessage: Internal service error
        '503':
          description: Backend service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 503
              userMessage: Backend service error
              systemMessage: Backend service error
      security:
        - oauth2: []              
definitions:
  ErrorResponse:
    type: object
    title: ErrorResponse
    description: error response object which contains error details
    properties:
      code:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: error code
      systemMessage:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: system provided error message
      userMessage:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: user friendly error message
      detailLink:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: link to custom information
  GetAppConfigurationResponse :
    type: object
    description: GetAppConfigurationResponse Object
    required:
      - serverName
      - product
      - configurationValues
    properties:
       serverName:
          description: Server Name
          type: string
          minLength: 1
          maxLength: 100
          pattern: ^([a-zA-Z0-9_]{1,100})$
          example: TSTASSIVR001
       product:
          type: string
          description: Product Name
          minLength: 1
          maxLength: 100
          pattern: ^([a-zA-Z0]{1,100})$
          example: POSTPAID
        
       configurationValues:
        type: array
        description: Configuration Array
        items:
          $ref: '#/definitions/ConfigurationValue'
        example :
           application/json:
                      ConfigurationValues :
                       -
                        indicator: boolean
                        configKey: HC_TimeOut_GetIMEI
                        configValue: "http://txdevasnete01:88/SIVR/Prepaid/Reference.asmx"
  GetAppConfigurationRequest:
     type: object
     description: GetAppConfigurationRequest Object
     required:
      - serverName
      - product
     properties:
        serverName :
          description: Server Name
          type: string
          minLength: 1
          maxLength: 100
          pattern: ^([a-zA-Z0-9_]{1,100})$
          example: TSTASSIVR001
          default: PRODUCTION
        product :
          type: string
          description: Product Name
          minLength: 1
          maxLength: 100
          pattern: ^([a-zA-Z0]{1,100})$
          example: POSTPAID
          
  ConfigurationValue:
    type: object
    description: ConfigurationValue object
    required:
      - indicator
      - configKey
      - configValue
    properties:
      indicator:
         type: string
         description: Indicator
         example: number
         enum : [string,number,boolean,real]
      configKey:
        type: string
        description: Configuration key.
        minLength: 1
        maxLength: 100
        pattern: ^([a-zA-Z0_]{1,100})
        example: HC_TimeOut_GetIMEI
      configValue:
        type: string
        description: Configuration value.
        pattern: ^((.*?){1,1000})$
        minLength: 1
        maxLength: 1000
        example: 'http://txdevasnete01:88/SIVR/Prepaid/Reference.asmx'
securityDefinitions:
  oauth2:
    type: oauth2
    tokenUrl: 'https://host:env/v1/oauth2/accesstoken?grant_type=client_credentials'
    flow: application
    description: |
      oauth2 Authentication 
parameters:
  AuthorizationParam:
    name: Authorization
    in: header
    description: OAuth 2.0 access token with the authentication type set as Bearer.
    required: true
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: Bearer xfhcXhZLhw51z76YP4fVGNnPtaUX
  AcceptParam:
    name: Accept
    in: header
    description: >-
      Content-Types that are acceptable is application/json (default assumed
      application/json)
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: application/json
  If-MatchParam:
    name: If-Match
    in: header
    description: >-
      The If-Match header specifies an ETag-related condition to be placed on
      the execution of a request.  The request will only be executed if the ETag
      of the requested resource matches one of those specified in the If-Match
      header.  Because the behavior is complex, the details for usage are
      provided at http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html .
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: xxyyzz
  If-None-MatchParam:
    name: If-None-Match
    in: header
    description: >-
      The If-None-Match header specifies an ETag-related condition to be placed
      on the execution of a request.  The request will NOT be executed if the
      ETag of the requested resource matches one of those specified in the
      If-None-Match header.  Because the behavior is complex, the details for
      usage are provided at
      http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: xxyyzz
  SessionIdParam:
    name: session-id
    in: header
    description: >-
      Unique ID for each session. This value will be used to track all
      transactions for a given session.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: tstvpsivr002-CUSP-1-2018039074905
  InteractionIdParam:
    name: interaction-id
    in: header
    description: >-
      This will be used to distinguish the processing of different accounts
      during the same call if a caller says, switch accounts.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: >-
      Initially the value is tstvpsivr002-CUSP-1-2018039074905_0. When caller
      says SwitchAccount, value will be changed to
      tstvpsivr002-CUSP-1-2018039074905_1
  WorkflowIdParam:
    name: workflow-id
    in: header
    description: This will be a value related to the path/experience the call receives
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: ACTIVATIONS
  ActivityIdParam:
    name: activity-id
    in: header
    description: 'Unique identifier for the service call, including the date/time'
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: tstvpsivr002-CUSP-1-2018039074905_ ACTIVATION_1526595682000
  StateIdParam:
    name: state-id
    in: header
    description: Provides the state id for telemetry purposes
    required: false
    minLength: 1
    maxLength: 100
    type: string
    x-example: PS1089_DB
    pattern: .*?    
  ApplicationUserIdParam:
    name: application-user-id
    in: header
    description: Used to identify the calling application.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: ivruser
  ApplicationIdParam:
    name: application-id
    in: header
    description: >- 
      calling application or system
    required: false
    minLength: 1
    maxLength: 100
    type: string
    x-example: REBELLION
    pattern: .*?
  SenderIdParam:
    name: sender-id
    in: header
    description: >- 
       The related business partner or T-Mobile organization
    required: false
    minLength: 1
    maxLength: 100
    type: string
    x-example: QVXP
    pattern: .*?
  ChannelIdParam:
    name: channel-id
    in: header
    description: >- 
     categorization of access or business stream
    required: false
    minLength: 1
    maxLength: 100
    type: string
    x-example: IVR
    pattern: .*?
  ContentTypeParam:
    name: Content-Type
    in: header
    description: Media type of the resource.
    required: true
    type: string
    x-example: application/json
    minLength: 1
    maxLength: 100
    pattern: .*?