swagger: '2.0'
info:
  description: chub-cross-company-customer-search service finds the company customer is assocaited with based on msisdn
  version: 1.0.0
  title: chub-cross-company-customer-search
  contact:
    name: IVR Developers
    email: IVRDevelopers@T-Mobile.com
host: env.api.t-mobile.com
basePath: /sivr/v1/chub-cross-company-customer-search
tags:
  - name: chub-cross-company-customer-search
schemes:
  - https
x-servers:
  - url: 'https://dlab01.core.op.api.t-mobile.com:443'
    description: dlab Server
produces:
  - application/json
paths:
  '/search':
    post:
      x-api-pattern: ExecuteFunction
      tags:
        - chub-cross-company-customer-search
      summary: 'Finds the company customer is assocaited with based on msisdn'
      description: Finds the company customer is assocaited with based on msisdn.
      operationId: getCrossCompanyCustomer
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - $ref: '#/parameters/AcceptParam'
        - $ref: '#/parameters/AuthorizationParam'
        - $ref: '#/parameters/ContentTypeParam'
        - $ref: '#/parameters/If-Modified-SinceParam'
        - $ref: '#/parameters/If-None-MatchParam'
        - $ref: '#/parameters/ActivityIdParam'
        - name: GetCrossCompanyCustomerRequest
          in: body
          description: Cross Company Customer Search Request
          required: true
          schema:
            $ref: '#/definitions/GetCrossCompanyCustomerRequest'
      responses:
        '200':
          description: Ok
          schema:
            $ref: '#/definitions/GetCrossCompanyCustomerResponse'
          examples:
           application/json:
                    msisdn: "6096173644"
                    companyIndicator: S
                    customerType: Individual
                    msisdnStatus: Active
          headers:
            Content-Length:
              description: >-
                The Content-Length header specifies the actual length of the
                returned payload.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: '1024'
            Content-Type:
              description: >-
                The Content-Length header specifies the actual length of the
                returned payload.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: application/json
            Date:
              description: >-
                The date and time that the message was originated (in HTTP-date
                format as defined by RFC 7231 Date/Time Formats).
              x-example: 'Wed, 02 May 2018 02:59:10 GMT'
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              format: date-time
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 400
              userMessage: Bad Request
              systemMessage: Bad Request
        '401':
          description: Unauthorized Access
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 401
              userMessage: Unauthorized Access
              systemMessage: Unauthorized Access
        '404':
          description: Resource not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 404
              userMessage: Resource not found
              systemMessage: Resource not found
        '405':
          description: Method not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 405
              userMessage: Method not found
              systemMessage: Method not found
          headers:
            Allow:
              description: List of HTTP methods allowed
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: 'GET, HEAD'
        '409':
          description: Conflict
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 409
              userMessage: Conflict
              systemMessage: Conflict
        '415':
          description: Unsupported Media Type
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 415
              userMessage: Unsupported Media Type
              systemMessage: Unsupported Media Type
        '500':
          description: Internal service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 500
              userMessage: Internal service error
              systemMessage: Internal service error
        '503':
          description: Backend service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 503
              userMessage: Backend service error
              systemMessage: Backend service error
      security:
        - oauth2: []
securityDefinitions:
  oauth2:
    type: oauth2
    tokenUrl: 'https://host:env/v1/oauth2/accesstoken?grant_type=client_credentials'
    flow: application
    description: |
      oauth2 Authentication 
definitions:
  ErrorResponse:
    type: object
    title: ErrorResponse
    description: error response object which contains error details
    properties:
      code:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: error code
      systemMessage:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: system provided error message
      userMessage:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: user friendly error message
      detailLink:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: link to custom information
  GetCrossCompanyCustomerResponse:
    type: object
    description: Cross Company Customer Search Response
    properties:
        msisdn:
          type: string
          description: Customer mobile / phone number
          example: '6096173644'
          minLength: 10
          maxLength: 10
          pattern: ^(\d{10})$
        companyIndicator:
          type: string
          description: Company Indicator. TMUS:T-Mobile, S:Sprint
          enum:
          - TMUS
          - S
        customerType:
          type: string
          description: Type of the customer
          enum:
          - Individual
          - Business
        msisdnStatus:
          type: string
          description: Status of the given MSISDN
          enum:
          - Active
          - Suspended
          - Cancelled
  GetCrossCompanyCustomerRequest:
    type: object
    description: Request to find the company customer is assocaited with based on MSISDN
    required:
     - msisdn
    properties:
        msisdn:
            type: string
            description: Customer mobile / phone number
            minLength: 10
            maxLength: 10
            pattern: ^(\d{10})$
            example: '6417810376'
parameters:
  AuthorizationParam:
    name: Authorization
    in: header
    description: OAuth 2.0 access token with the authentication type set as Bearer.
    required: true
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: Bearer Wqwgh1Fx44D3tSbG0XWKbTfIJPh0
  AcceptParam:
    name: Accept
    in: header
    description: >-
      Content-Types that are acceptable is application/json (default assumed
      application/json)
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: application/json
  If-None-MatchParam:
    name: If-None-Match
    in: header
    description: >-
      The if-none-match header specifies an ETag-related condition to be placed
      on the execution of a request. The request will NOT be executed if the
      ETag of the requested resource matches one of those specified in the
      if-none-match header.  Because the behavior is complex, the details for
      usage are provided at
      http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: xxyyzz
  ActivityIdParam:
    name: activity-id
    in: header
    description: 'Unique id alphanumeric value generated and sent by the partner to identify every the service request uniquely. Sample: XyzA:D1s5s1i-X6AFRvfiPBfs-1234 Pass as is to downstream calls.'
    required: true
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: XyzA:D1s5s1i-X6AFRvfiPBfs-1234
  If-Modified-SinceParam:
    name: If-Modified-Since
    in: header
    description: 'The If-Modified-Since header specifies date-related conditions to be placed on the execution of a request.  The request will only be executed if the requested resource has not been modified since the date-time specified.  Because the behavior is complex, the details for usage are provided at http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html'
    x-example: 'Wed, 18-Apr-2018 11:34:57 GMT'
    required: false
    type: string
  ContentTypeParam:
    name: Content-Type
    in: header
    description: Media type of the resource.
    required: true
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: application/json