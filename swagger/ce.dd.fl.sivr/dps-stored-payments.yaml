swagger: '2.0'
info:
  description: This API provides the stored payment instrument for the given customer.
  version: 1.0.0
  title: dps-stored-payments xAPI
  contact:
    name: IVR Developers
    email: IVRDevelopers@T-Mobile.com
host: env.api.t-mobile.com
basePath: /sivr/v1/dps-stored-payments
tags:
  - name: dps-stored-payments
    description: This API provides the stored payment instrument for the given customer from DPS
schemes:
  - https
x-servers:
  - url: 'https://dlab01.core.op.api.t-mobile.com:443'
    description: dlab Server
consumes:
  - application/json
produces:
  - application/json
paths:
  '/search':
    post:
      x-api-pattern: ExecuteFunction
      tags:
        - dps-stored-payments
      summary: 'Get stored payment instrument for the given customer'
      description: Pass BAN to retrieve stored payment instruments.
      operationId: searchStoredPaymentInstrument
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - $ref: '#/parameters/AuthorizationParam'
        - $ref: '#/parameters/AcceptParam'
        - $ref: '#/parameters/If-MatchParam'
        - $ref: '#/parameters/If-None-MatchParam'
        - $ref: '#/parameters/SessionIdParam'
        - $ref: '#/parameters/InteractionIdParam'
        - $ref: '#/parameters/WorkflowIdParam'
        - $ref: '#/parameters/ActivityIdParam'
        - $ref: '#/parameters/ApplicationIdParam'
        - $ref: '#/parameters/SenderIdParam'
        - $ref: '#/parameters/ChannelIdParam'
        - $ref: '#/parameters/StateIdParam'
        - name: SearchStoredPaymentInstrumentRequest
          in: body
          description: The search parameters
          required: true
          schema:
            $ref: '#/definitions/SearchStoredPaymentInstrumentRequest'
        - name: Content-Type
          in: header
          description: Media type of the resource.
          required: true
          type: string
          minLength: 1
          maxLength: 100
          pattern: .*?
          x-example: application/json
      responses:
        '200':
          description: Ok
          schema:
            $ref: '#/definitions/SearchStoredPaymentInstrumentResponse'
          examples:
           application/json:
                    specificationsName: customerBlocked
                    specificationsValue: ALL
                    payment:
                      - paymentInstrument:
                          bankPayment: null
                          cardPayment:
                            typeCode: AMERICANEXPRESS
                            expirationMonthYear: '20190201'
                            paymentCardAlias: '300020228101347'
                            zip: '03112'
                        paymentMethodStatus: VALID
                        paymentMethodCode: CC
                        walletSize: 10
                      - paymentInstrument:
                          bankPayment:
                            routingNumber: '042206503'
                            bankAccountAlias: '99999900001421573477'
                          cardPayment: null
                        paymentMethodStatus: VALID
                        paymentMethodCode: CHECK
                        walletSize: 10
                      - paymentInstrument:
                          bankPayment: null
                          cardPayment:
                            typeCode: VISA
                            expirationMonthYear: '20200701'
                            paymentCardAlias: '4000020283504448'
                            zip: '30346'
                        paymentMethodStatus: VALID/INVALID
                        paymentMethodCode: CC
                        walletSize: 10
                      - paymentInstrument:
                          bankPayment:
                            routingNumber: '321370707'
                            bankAccountAlias: '99999900000126145559'
                          cardPayment: null
                        paymentMethodStatus: VALID/INVALID
                        paymentMethodCode: CHECK
                        walletSize: 10
          headers:
            Content-Length:
              description: >-
                The Content-Length header specifies the actual length of the
                returned payload.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: '1024'
            Content-Type:
              description: >-
                The Content-Length header specifies the actual length of the
                returned payload.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: application/json
            Date:
              description: >-
                The date and time that the message was originated (in HTTP-date
                format as defined by RFC 7231 Date/Time Formats).
              x-example: 'Wed, 02 May 2018 02:59:10 GMT'
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              format: date-time
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 400
              userMessage: Bad Request
              systemMessage: Bad Request
        '401':
          description: Unauthorized Access
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 401
              userMessage: Unauthorized Access
              systemMessage: Unauthorized Access
        '404':
          description: Resource not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 404
              userMessage: Resource not found
              systemMessage: Resource not found
        '405':
          description: Method not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 405
              userMessage: Method not found
              systemMessage: Method not found
          headers:
            Allow:
              description: List of HTTP methods allowed
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: 'GET, HEAD'
        '409':
          description: Conflict
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 409
              userMessage: Conflict
              systemMessage: Conflict
        '415':
          description: Unsupported Media Type
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 415
              userMessage: Unsupported Media Type
              systemMessage: Unsupported Media Type
        '500':
          description: Internal service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 500
              userMessage: Internal service error
              systemMessage: Internal service error
        '503':
          description: Backend service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 503
              userMessage: Backend service error
              systemMessage: Backend service error
      security:
        - oauth2: []
securityDefinitions:
  oauth2:
    type: oauth2
    tokenUrl: 'https://host:env/v1/oauth2/accesstoken?grant_type=client_credentials'
    flow: application
    description: |
      oauth2 Authentication 
definitions:
  ErrorResponse:
    type: object
    title: ErrorResponse
    description: error response object which contains error details
    properties:
      code:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: error code
      systemMessage:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: system provided error message
      userMessage:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: user friendly error message
      detailLink:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: link to custom information
  SearchStoredPaymentInstrumentResponse:
    description: Response details for Payment search
    type: object
    properties:
      specificationsName:
        description: Describes Name for Name Value Pair.
        type: string
        minLength: 1
        maxLength: 100
        pattern: ^([a-zA-Z0-9]{1,100})$
        x-example: "customerBlocked"
      specificationsValue:
        description: Describes Value for Name Value Pair.
        type: string
        enum: [CHECK,CARD,ALL]
        minLength: 3
        maxLength: 5
        x-example: CHECK/CARD/ALL
      payment:
        description: Payment details
        type: array
        items:
          $ref: '#/definitions/Payment'
  
  Payment:
    type: object
    description: It includes paymentInstrument,paymentMethodStatus,paymentMethodCode,walletCapacity,walletSize,autoPayEnabledIndicator in the response.
    properties:
      paymentInstrument:
        $ref: '#/definitions/PaymentInstrument'
      paymentMethodStatus:
        description: 'Status of payment method, such as VALID, INVALID'
        type: string
        enum: [VALID,INVALID]
        minLength: 5
        maxLength: 7
        example: "INVALID"
      paymentMethodCode:
        description: 'status of the payment.'
        type: string
        example: "CC"
        enum: [CC, PC, HC, DC, PD, HD,CHECK]
        minLength: 2
        maxLength: 5
      walletCapacity:
        description: Number of payment methods allowed in the Digital Wallet.
        type: string
        minLength: 1
        maxLength: 2
        pattern: ^[0-9]{2}$
        example: 10
      walletSize:
        description: Current size of the wallet. If the capacity is 10 then size can be less than or equal to 10
        type: string
        minLength: 1
        maxLength: 2
        pattern: ^[0-9]{2}$
        example: 5
        
  PaymentInstrument:
    type: object
    description: It includes the type of payment i.e.. Bank payment or Card Paymentin the response.
    properties:
      bankPayment:
        $ref: '#/definitions/BankPayment'
      paymentCard:
        $ref: '#/definitions/CardPayment'
  BankPayment:
    type: object
    description: It includes Routing number,Bankaccountalias,Update time in the response.
    properties:
      routingNumber:
        description: Routing Number of the bank on whom the payment instrument is drawn.
        type: string
        minLength: 1
        maxLength: 9
        pattern: ^[0-9]{9}$
        example: "234567656"
      bankAccountAlias:
        description: Bank account alias used by the customer.
        type: string
        minLength: 1
        maxLength: 100
        pattern: ^[a-zA-Z0-9_\s]{1,100}$
        example: "99999900000069610015"
        
  CardPayment:
    type: object
    description: It includes typecode,Expiration Month-Year,PaymentCardAlias,Zip,Update-Time in the Payment card response.
    properties:
      typeCode:
        description: 'Type of payment Card. Examples are Visa, MasterCard, Debit etc'
        type: string
        enum: [VISA,MASTERCARD,DEBIT,DISCOVER,AMERICANEXPRESS]
        minLength: 4
        maxLength: 15
        example: "AMERICANEXPRESS"
      expirationMonthYear:
        description: Expiration month and year of the card
        type: string
        minLength: 8
        maxLength: 8
        pattern: ^[0-9]{8}$
        example: "20280301"
      paymentCardAlias:
        description: A nickname assigned by the customer as an alternative way to identify a stored payment card.
        type: string
        minLength: 1
        maxLength: 100
        pattern: ^[a-zA-Z0-9_\s]{1,100}$
        example: "4000020283504448"
      zip:
        description: To indicate the ZipCode.
        type: string
        minLength: 1
        maxLength: 5
        pattern: ^[0-9]{5}$
        example: "98008"
  SearchStoredPaymentInstrumentRequest:
    type: object
    description: It includes businessSegment,productGroup,businessUnit,programCode,billerCode,customerId in the request.
    properties:
      customerId:
        description: 'Customers billing account number'
        type: string
        minLength: 9
        maxLength: 9
        pattern:  ^([0-9]{9})$
        example: "110002005"
      businessSegment:
        type: string
        description: 'Businesssegment Valid values - PREPAID, POSTPAID, ANONYMOUS, B2B, DEALER, NATIONALRETAIL'
        enum: [PREPAID,POSTPAID,ANONYMOUS,B2B,DEALER,NATIONALRETAIL]
        example: "POSTPAID"
        minLength: 3
        maxLength: 14
      productGroup:
        enum: [HardGoods,SoftGoods]
        description: 'Product grouping such as HardGoods or SoftGoods'
        type: string
        minLength: 1
        maxLength: 100
        example: "HardGoods"
      businessUnit:
        type: string
        description: 'The T-Mobile business unit, such as TMOBILE, METROPCS'
        example: "TMOBILE"
        enum: [TMOBILE, METROPCS]
        minLength: 6
        maxLength: 8
      programCode:
        type: string
        description: 'Program identifier such as ZCP, ZCNP, U2POSTCP, U2PRECP, U2PRECNP, U2POSTCNP, U1POSTLEGACY'
        example: "U1POSTLEGACY"
        enum: [ZCP,ZCNP,U2POSTCP,U2PRECP,U2PRECNP,U2POSTCNP,U1POSTLEGACY]
        minLength: 3
        maxLength: 12
      billerCode:
        type: string
        description: 'Billing system identifier such as SAMSON, ERICSSON, OFSLL, EIP'
        enum: [SAMSON, ERICSSON, OFSLL, EIP]
        example: "SAMSON"
        minLength: 3
        maxLength: 8
parameters:
  AuthorizationParam:
    name: Authorization
    in: header
    description: OAuth 2.0 access token with the authentication type set as Bearer.
    required: true
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: Bearer Wqwgh1Fx44D3tSbG0XWKbTfIJPh0
  ActivityIdParam:
    name: activity-id
    in: header
    description: 'Unique identifier for the service call, including the date/time'
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: tstvpsivr002-CUSP-1-2018039074905_ ACTIVATION_1526595682000
  AcceptParam:
    name: Accept
    in: header
    description: >-
      Content-Types that are acceptable is application/json (default assumed
      application/json)
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: application/json
  If-MatchParam:
    name: If-Match
    in: header
    description: >-
      The if-match header specifies an ETag-related condition to be placed on
      the execution of a request.  The request will only be executed if the ETag
      of the requested resource matches one of those specified in the if-match
      header.  Because the behavior is complex, the details for usage are
      provided at http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html .
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: xxyyzz
  If-None-MatchParam:
    name: If-None-Match
    in: header
    description: >-
      The if-none-match header specifies an ETag-related condition to be placed
      on the execution of a request.  The request will NOT be executed if the
      ETag of the requested resource matches one of those specified in the
      if-none-match header.  Because the behavior is complex, the details for
      usage are provided at
      http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: xxyyzz
  SessionIdParam:
    name: session-id
    in: header
    description: >-
      Unique ID for each session. This value will be used to track all
      transactions for a given session.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: tstvpsivr002-CUSP-1-2018039074905
  InteractionIdParam:
    name: interaction-id
    in: header
    description: >-
      This will be used to distinguish the processing of different accounts
      during the same call if a caller says, switch accounts.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: >-
      Initially the value is tstvpsivr002-CUSP-1-2018039074905_0. When caller
      says SwitchAccount, value will be changed to
      tstvpsivr002-CUSP-1-2018039074905_1
  WorkflowIdParam:
    name: workflow-id
    in: header
    description: This will be a value related to the path/experience the call receives
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: LOGIN
  ChannelIdParam:
    name: channel-id
    in: header
    description: >- 
     categorization of access or business stream
    required: true
    minLength: 1
    maxLength: 100
    type: string
    x-example: IVR
    pattern: .*?
  ApplicationIdParam:
    name: application-id
    in: header
    description: >- 
      calling application or system
    required: true
    minLength: 1
    maxLength: 100
    type: string
    x-example: REBELLION
    pattern: .*?
  SenderIdParam:
    name: sender-id
    in: header
    description: >- 
       The related business partner or T-Mobile organization
    required: true
    minLength: 1
    maxLength: 100
    type: string
    x-example: QVXP
    pattern: .*?
  StateIdParam:
    name: state-id
    in: header
    description: >- 
     Name of the table
    required: false
    minLength: 1
    maxLength: 100
    type: string
    x-example: PS5020_DM
    pattern: .*?
