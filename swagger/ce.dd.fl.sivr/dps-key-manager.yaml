swagger: '2.0'
info:
  description: >-
    Key related xAPIs
        1) Get Public Key from DPS   
  version: 1.0.0
  title: dps-key-manager xAPI
  contact:
    name: IVR Developers
    email: IVRDevelopers@T-Mobile.com
host: env.api.t-mobile.com
basePath: /sivr/v1/dps-key-manager
tags:
  - name: dps-key-manager
    description:  This API provides operations for get public key from DPS.
schemes:
  - https
x-servers:
  - url: 'https://dlab01.core.op.api.t-mobile.com:443'
    description: dlab Server
consumes:
  - application/json
produces:
  - application/json
paths:
  '/public-key':
    get:
      x-api-pattern: QueryCollection
      tags:
        - dps-key-manager
      summary: Get Public Key to use for RSA encryption
      description: Get Public Key to use for RSA encryption
      operationId: getPublicKey
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - $ref: '#/parameters/AuthorizationParam'
        - $ref: '#/parameters/AcceptParam'
        - $ref: '#/parameters/If-MatchParam'
        - $ref: '#/parameters/If-None-MatchParam'
        - $ref: '#/parameters/SessionIdParam'
        - $ref: '#/parameters/InteractionIdParam'
        - $ref: '#/parameters/WorkflowIdParam'
        - $ref: '#/parameters/ActivityIdParam'
        - $ref: '#/parameters/ApplicationIdParam'
        - $ref: '#/parameters/SenderIdParam'
        - $ref: '#/parameters/ChannelIdParam'
        - $ref: '#/parameters/ForceDataRefreshIndicatorParam'
        - $ref: '#/parameters/StateIdParam'
        - name: Content-Type
          in: header
          description: Media type of the resource.
          required: true
          type: string
          minLength: 1
          maxLength: 100
          pattern: .*?
          x-example: application/json
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/GetPublicKeyResponse'
          examples:
            application/json:
              "hexKey": "30820109028201009AEC329F973EB2ADF29B414AC60AF8CA0226ACAD42B5F766C38C6E94FF98E7031432D25F76C31E471C27463D1A73BD1EC51F20E2DAAA58EEB3C8DF496BF8CC6E9FFCE10ABA0E8B76024FE94EA88AAE63EDD17F562F2C9E3B483BB442133B176F0A115C8A2BCBCF7E993F8CC3BD1B98FA1B5427A8FF907B429262AA193429B2941E70326DC5D4A198BF0FBD1AD264597FD85A3FE94A1161E44FF436C87E50C0136B00DD36B60649273B3F0889B58294B20590EE013D0DDCB602F91230BC5A4FC499123D0E4878679A3B4CE22D637B9CA54F350654E82385FD337DB5472E20EF9B46CB03E0B1DA62AD19D1A065D204C3C68CD0C8D8B44D0A6CDB745DCD3F2098A10203010001"
          headers:
            ETag:
              description: >-
                The ETag header specifies the unique entity tag value for the
                returned resource.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: Unique entity tag
            Content-Length:
              description: >-
                The Content-Length header specifies the actual length of the
                returned payload.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: '1024'
            Content-Type:
              description: >-
                The Content-Length header specifies the actual length of the
                returned payload.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: application/json
            Date:
              description: >-
                The date and time that the message was originated (in HTTP-date
                format as defined by RFC 7231 Date/Time Formats).
              x-example: 'Wed, 02 May 2018 02:59:10 GMT'
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              format: date-time
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 400
              userMessage: Bad Request
              systemMessage: Bad Request
        '401':
          description: Unauthorized Access
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 401
              userMessage: Unauthorized Access
              systemMessage: Unauthorized Access
        '404':
          description: Resource not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 404
              userMessage: Resource not found
              systemMessage: Resource not found
        '405':
          description: Method not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 405
              userMessage: Method not found
              systemMessage: Method not found
          headers:
            Allow:
              description: List of HTTP methods allowed
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: 'GET, HEAD'
        '406':
          description: Conflict
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 406
              userMessage: Not Acceptable
              systemMessage: Not Acceptable
        '500':
          description: Internal service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 500
              userMessage: Internal service error
              systemMessage: Internal service error
        '503':
          description: Backend service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              code: 503
              userMessage: Backend service error
              systemMessage: Backend service error
      security:
        - oauth2: []
securityDefinitions:
  oauth2:
    type: oauth2
    tokenUrl: 'https://host:env/v1/oauth2/accesstoken?grant_type=client_credentials'
    flow: application
    description: |
      oauth2 Authentication 
definitions:
  ErrorResponse:
    type: object
    title: ErrorResponse
    description: error response object which contains error details
    properties:
      code:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: error code
      systemMessage:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: system provided error message
      userMessage:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: user friendly error message
      detailLink:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: link to custom information
  GetPublicKeyResponse:
    type: object
    description: GetPublicKeyResponse Object
    properties:
      hexKey:
        type: string
        minLength: 1
        maxLength: 4096
        pattern: ^([a-zA-Z0-9]{1,4096})$
        description: RSA Encrypted Hex Public Key
        example: "3082010902820100AED4B683C0CD61FEF23FB0E137CFCEF1254C5B8D50F1C539AAA31D1506E95308551C304330F3A53B7F5356FDD5A2F2E0CDC3AC80E0A56ABFC75E6EE01CFE3BC8E6C6D0B076930D5F47B7FAFCA4C0D1A9E1EB568C35283F49CDABF663CB0907185F539EF1F18DE07AFA06522B60410B35AF1F8E4971C39986D9CE3C3FFD85A6FD02925C2EBC852A8273BC8E8ECF0FC29564E69A6CD686AFCE66D780B13F6CD3295F9F3140DF634DEFCC7C86DFB0DCC4834245514561C9CAB82E769508B1327167279A7A31A929092A4DADDAB1E26C452CDCC574199EEFABD0747B17446937642EB263F1AECA66C631D32B2AC64108659E1866DDEBFC6B7D137D8DD690EE6222DD0203010001"
parameters:
  AuthorizationParam:
    name: Authorization
    in: header
    description: OAuth 2.0 access token with the authentication type set as Bearer.
    required: true
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: Bearer Wqwgh1Fx44D3tSbG0XWKbTfIJPh0
  AcceptParam:
    name: Accept
    in: header
    description: >-
      Content-Types that are acceptable is application/json (default assumed
      application/json)
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: application/json
  If-MatchParam:
    name: If-Match
    in: header
    description: >-
      The if-match header specifies an ETag-related condition to be placed on
      the execution of a request.  The request will only be executed if the ETag
      of the requested resource matches one of those specified in the if-match
      header.  Because the behavior is complex, the details for usage are
      provided at http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html .
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: xxyyzz
  If-None-MatchParam:
    name: If-None-Match
    in: header
    description: >-
      The if-none-match header specifies an ETag-related condition to be placed
      on the execution of a request.  The request will NOT be executed if the
      ETag of the requested resource matches one of those specified in the
      if-none-match header.  Because the behavior is complex, the details for
      usage are provided at
      http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: xxyyzz
  SessionIdParam:
    name: session-id
    in: header
    description: >-
      Unique ID for each session. This value will be used to track all
      transactions for a given session.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: tstvpsivr002-CUSP-1-2018039074905
  InteractionIdParam:
    name: interaction-id
    in: header
    description: >-
      This will be used to distinguish the processing of different accounts
      during the same call if a caller says, switch accounts.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: >-
      Initially the value is tstvpsivr002-CUSP-1-2018039074905_0. When caller
      says SwitchAccount, value will be changed to
      tstvpsivr002-CUSP-1-2018039074905_1
  WorkflowIdParam:
    name: workflow-id
    in: header
    description: This will be a value related to the path/experience the call receives
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: LOGIN
  ActivityIdParam:
    name: activity-id
    in: header
    description: 'Unique identifier for the service call, including the date/time'
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: tstvpsivr002-CUSP-1-2018039074905_LOGIN_1526595682000
  ApplicationIdParam:
    name: application-id
    in: header
    description: >- 
      calling application or system
    required: true
    minLength: 1
    maxLength: 100
    type: string
    x-example: REBELLION
    pattern: .*?
  SenderIdParam:
    name: sender-id
    in: header
    description: >- 
       The related business partner or T-Mobile organization
    required: true
    minLength: 1
    maxLength: 100
    type: string
    x-example: QVXP
    pattern: .*?
  ChannelIdParam:
    name: channel-id
    in: header
    description: >- 
     categorization of access or business stream
    required: true
    minLength: 1
    maxLength: 100
    type: string
    x-example: IVR
    pattern: .*?
  ForceDataRefreshIndicatorParam:
    name: force-data-refresh-indicator
    in: header
    description: >-
      This will indicate when we want to do hard refresh cache
    required: false
    minLength: 4
    maxLength: 5
    type: boolean
    x-example: true
    pattern: ^(true|false)$
  StateIdParam:
    name: state-id
    in: header
    description: >- 
     Name of the table
    required: false
    minLength: 1
    maxLength: 100
    type: string
    x-example: PS5020_DM
    pattern: .*? 
