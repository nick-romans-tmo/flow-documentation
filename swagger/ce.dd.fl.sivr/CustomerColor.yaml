swagger: '2.0'
info:
  description: This API supports to read customer color from SIVR DB
  version: 1.0.0
  title: CustomerColor API
  contact:
    name: IVR Developers
    email: IVRDevelopers@T-Mobile.com
host: env.api.t-mobile.com
basePath: /sivr/v1/customers-colors
tags:
  - name: Customer Color
    description: This API supports to read customer color from SIVR DB
schemes:
  - https
x-servers:
  - url: 'https://dlab01.core.op.api.t-mobile.com:443'
    description: dlab Server
paths:
  '/{msisdn}':
    get:
      x-api-pattern: QueryResource
      tags:
        - Customer Color
      summary: Get Color for a given MSISDN
      description: Requires MSISDN
      operationId: getCustomerColor
      produces:
        - application/json
      parameters:
        - name: msisdn
          in: path
          description: msisdn
          required: true
          type: integer
          format: int64
          minLength: 10
          maxLength: 10
          x-example: 4253021000
          pattern: '^(\d{10})$'
        - $ref: '#/parameters/AuthorizationParam'
        - $ref: '#/parameters/AcceptParam'
        - $ref: '#/parameters/If-MatchParam'
        - $ref: '#/parameters/If-None-MatchParam'
        - $ref: '#/parameters/ApplicationUserIdParam'
        - $ref: '#/parameters/SessionIdParam'
        - $ref: '#/parameters/InteractionIdParam'
        - $ref: '#/parameters/WorkflowIdParam'
        - $ref: '#/parameters/ActivityIdParam'
      responses:
        '200':
          description: Ok
          schema:
            $ref: '#/definitions/GetCustomerColorResponse'
          examples:
            application/json:
              - 'color:R'
          headers:
            Cache-Control:
              description: 'No cache indicators supported at this time. Samples: no-cache'
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: no-cache
            ETag:
              description: >-
                The ETag header specifies the unique entity tag value for the
                returned resource.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: Unique entity tag
            Content-Length:
              description: >-
                The Content-Length header specifies the actual length of the
                returned payload.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: '1024'
            Content-Type:
              description: >-
                The Content-Length header specifies the actual length of the
                returned payload.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: application/json
            Expires:
              description: >-
                The Expires header gives the date-time after which the response
                is considered stale.
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: '0'
            Date:
              description: >-
                The date and time that the message was originated (in HTTP-date
                format as defined by RFC 7231 Date/Time Formats).
              x-example: 'Wed, 02 May 2018 02:59:10 GMT'
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              format: date-time
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              - 'code:400 userMessage:Bad Request systemMessage:Bad Request'
        '401':
          description: Unauthorised Access
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              - >-
                code:401 userMessage:Unauthorized Access
                systemMessage:Unauthorized Access
        '404':
          description: Resource not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              - >-
                code:404 userMessage:Resource not found systemMessage:Resource
                not found
        '405':
          description: Method not found
          schema:
            $ref: '#/definitions/ErrorResponse'
          headers:
            Allow:
              description: List of HTTP methods allowed
              type: string
              minLength: 1
              maxLength: 100
              pattern: .*?
              x-example: 'GET, HEAD'
          examples:
            application/json:
              - >-
                code:405 userMessage:Method not found systemMessage:Method not
                found
        '406':
          description: Not Acceptable
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              - 'code:406 userMessage:Not Acceptable systemMessage:Not Acceptable'
        '500':
          description: Intenal service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              - >-
                code:500 userMessage:Internal service error
                systemMessage:Internal service error
        '503':
          description: Backend service error
          schema:
            $ref: '#/definitions/ErrorResponse'
          examples:
            application/json:
              - >-
                code:503 userMessage:Backend service error systemMessage:Backend
                service error
      security:
        - oauth2: []
securityDefinitions:
  oauth2:
    type: oauth2
    tokenUrl: 'https://host:env/v1/oauth2/accesstoken?grant_type=client_credentials'
    flow: application
    description: |
      oauth2 Authentication 
definitions:
  ErrorResponse:
    type: object
    title: ErrorResponse
    description: error response object which contains error details
    properties:
      code:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: error code
      systemMessage:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: system provided error message
      userMessage:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: user friendly error message
      detailLink:
        type: string
        minLength: 1
        maxLength: 100
        pattern: .*?
        description: link to custom information
  GetCustomerColorResponse:
    type: object
    properties:
      color:
        type: string
        description: color of the input msisdn
    title: GetCustomerColorResponse
    description: response class which contains customer color
parameters:
  AuthorizationParam:
    name: Authorization
    in: header
    description: OAuth 2.0 access token with the authentication type set as Bearer.
    required: true
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: Bearer Wqwgh1Fx44D3tSbG0XWKbTfIJPh0
  AcceptParam:
    name: Accept
    in: header
    description: >-
      Content-Types that are acceptable is application/json (default assumed
      application/json)
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: application/json
  If-MatchParam:
    name: If-Match
    in: header
    description: >-
      The If-Match header specifies an ETag-related condition to be placed on
      the execution of a request.  The request will only be executed if the ETag
      of the requested resource matches one of those specified in the If-Match
      header.  Because the behavior is complex, the details for usage are
      provided at http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html .
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: xxyyzz
  If-None-MatchParam:
    name: If-None-Match
    in: header
    description: >-
      The If-None-Match header specifies an ETag-related condition to be placed
      on the execution of a request.  The request will NOT be executed if the
      ETag of the requested resource matches one of those specified in the
      If-None-Match header.  Because the behavior is complex, the details for
      usage are provided at
      http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: xxyyzz
  ApplicationUserIdParam:
    name: application-user-id
    in: header
    description: Used to identify the calling application.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: ivruser
  SessionIdParam:
    name: session-id
    in: header
    description: >-
      Unique ID for each session. This value will be used to track all
      transactions for a given session.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: tstvpsivr002-CUSP-1-2018039074905
  InteractionIdParam:
    name: interaction-id
    in: header
    description: >-
      This will be used to distinguish the processing of different accounts
      during the same call if a caller says, switch accounts.
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: >-
      Initially the value is “tstvpsivr002-CUSP-1-2018039074905_0”. When caller
      says “SwitchAccount”, value will be changed to
      “tstvpsivr002-CUSP-1-2018039074905_1”
  WorkflowIdParam:
    name: workflow-id
    in: header
    description: This will be a value related to the path/experience the call receives
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: ACTIVATIONS
  ActivityIdParam:
    name: activity-id
    in: header
    description: 'Unique identifier for the service call, including the date/time'
    required: false
    type: string
    minLength: 1
    maxLength: 100
    pattern: .*?
    x-example: tstvpsivr002-CUSP-1-2018039074905_ ACTIVATION_1526595682000
