{
  "swagger": "2.0",
  "info": {
    "version": "0.5",
    "title": "Prepaid Voucher System",
    "description": "Manages prepaid vouchers which can be redeemed one time for a specific dollar amount. Vouchers have a **face value** assigned at time of creation that does not change. They also have **status** indicating whether the voucher is available for use.",
    "contact": {
      "name": "Prepaid Development team",
      "email": "BillingDevelopment-PrepaidAll@T-Mobile.com"
    }
  },
  "host": "api.t-mobile.com",
  "basePath": "/voucher/v1/vouchers",
  "schemes": [
    "http"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json",
    "application/hal+json"
  ],
  "paths": {
    "/issuers/{issuer-id}/vouchers/{serial-number}": {
      "get": {
        "description": "Returns a voucher of the specified issuer Id and serial number",
        "operationId": "getVoucherByIssuerAndSerialNumber",
        "produces": [
          "application/hal+json"
        ],
        "parameters": [
          {
            "name": "channel-id",
            "in": "header",
            "description": "channel the request originated from",
            "required": true,
            "type": "string"
          },
          {
            "name": "application-id",
            "in": "header",
            "description": "application the request originated from",
            "required": true,
            "type": "string"
          },
          {
            "name": "store-id",
            "in": "header",
            "description": "Unique identifier for the retail store location.",
            "required": false,
            "type": "string"
          },
          {
            "name": "issuer-id",
            "in": "path",
            "description": "the issuer of the voucher",
            "required": true,
            "type": "string"
          },
          {
            "name": "serial-number",
            "in": "path",
            "description": "the serial number of the voucher",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "success response",
            "schema": {
              "type": "object",
              "required": [
                "skuDescription",
                "mediaType",
                "faceValueInCents",
                "status"
              ],
              "properties": {
                "skuDescription": {
                  "type": "string"
                },
                "mediaType": {
                  "type": "string"
                },
                "faceValueInCents": {
                  "type": "integer",
                  "format": "int32"
                },
                "status": {
                  "type": "string"
                },
                "_links": {
                  "type": "object",
                  "required": [
                    "self"
                  ],
                  "properties": {
                    "self": {
                      "type": "object",
                      "properties": {
                        "rel": {
                          "type": "string"
                        },
                        "href": {
                          "type": "string",
                          "format": "uri"
                        }
                      }
                    },
                    "redeem": {
                      "type": "object",
                      "description": "the link will be present if the voucher is in a redeemable state",
                      "properties": {
                        "rel": {
                          "type": "string"
                        },
                        "href": {
                          "type": "string",
                          "format": "uri"
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "Missing or invalid elements in the request",
            "schema": {
              "type": "object",
              "required": [
                "status",
                "message"
              ],
              "properties": {
                "status": {
                  "type": "integer",
                  "format": "int32"
                },
                "message": {
                  "type": "string"
                }
              }
            }
          },
          "404": {
            "description": "voucher not found",
            "schema": {
              "type": "object",
              "required": [
                "status",
                "message"
              ],
              "properties": {
                "status": {
                  "type": "integer",
                  "format": "int32"
                },
                "message": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/find-vouchers": {
      "post": {
        "description": "Returns a collection of vouchers by pin",
        "operationId": "getVoucherByPin",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/hal+json"
        ],
        "parameters": [
          {
            "name": "channel-id",
            "in": "header",
            "description": "channel the request originated from",
            "required": true,
            "type": "string"
          },
          {
            "name": "application-id",
            "in": "header",
            "description": "application the request originated from",
            "required": true,
            "type": "string"
          },
          {
            "name": "store-id",
            "in": "header",
            "description": "Unique identifier for the retail store location.",
            "required": false,
            "type": "string"
          },
          {
            "name": "body",
            "in": "body",
            "description": "Request to find vouchers by pin",
            "required": true,
            "schema": {
              "type": "array",
              "items": {
                "type": "integer",
                "format": "int64"
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "success response",
            "schema": {
              "type": "object",
              "required": [
                "_embedded"
              ],
              "properties": {
                "_embedded": {
                  "type": "object",
                  "required": [
                    "vouchers"
                  ],
                  "properties": {
                    "vouchers": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "required": [
                          "pin",
                          "skuDescription",
                          "mediaType",
                          "faceValueInCents",
                          "status"
                        ],
                        "properties": {
                          "pin": {
                            "type": "integer",
                            "format": "int64"
                          },
                          "skuDescription": {
                            "type": "string"
                          },
                          "mediaType": {
                            "type": "string"
                          },
                          "faceValueInCents": {
                            "type": "integer",
                            "format": "int32"
                          },
                          "redeemable": {
                            "type": "boolean",
                            "description": "Indicates if the voucher can be redeemed or not."
                          },
                          "status": {
                            "type": "string"
                          },
                          "_links": {
                            "type": "object",
                            "required": [
                              "self"
                            ],
                            "properties": {
                              "self": {
                                "type": "object",
                                "properties": {
                                  "href": {
                                    "type": "string",
                                    "format": "uri"
                                  }
                                }
                              },
                              "redeem": {
                                "type": "object",
                                "description": "the link will be present if the voucher is in a redeemable state",
                                "properties": {
                                  "href": {
                                    "type": "string",
                                    "format": "uri"
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "Missing or invalid elements in the request"
          },
          "404": {
            "description": "Voucher not found",
            "schema": {
              "type": "object",
              "required": [
                "status",
                "message"
              ],
              "properties": {
                "status": {
                  "type": "integer",
                  "format": "int32"
                },
                "message": {
                  "type": "string"
                }
              }
            }
          },
          "415": {
            "description": "Unsupported media type",
            "schema": {
              "type": "object",
              "required": [
                "status",
                "message"
              ],
              "properties": {
                "status": {
                  "type": "integer",
                  "format": "int32"
                },
                "message": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/dispense-order/{transaction-id}": {
      "put": {
        "description": "Dispense epins for T-Mobile retail",
        "operationId": "dispenseEPin",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/hal+json"
        ],
        "parameters": [
          {
            "name": "channel-id",
            "in": "header",
            "description": "channel the request originated from",
            "required": true,
            "type": "string"
          },
          {
            "name": "application-id",
            "in": "header",
            "description": "application the request originated from",
            "required": true,
            "type": "string"
          },
          {
            "name": "store-id",
            "in": "header",
            "description": "Unique identifier for the retail store location.",
            "required": false,
            "type": "string"
          },
          {
            "name": "Content-Type",
            "in": "header",
            "description": "application/json is supported",
            "required": true,
            "type": "string"
          },
          {
            "name": "transaction-id",
            "in": "path",
            "description": "A unique transaction id for each dispense epin request",
            "required": true,
            "type": "string"
          },
          {
            "name": "body",
            "in": "body",
            "description": "Request to dispense epins",
            "required": true,
            "schema": {
              "type": "object",
              "required": [
                "skuIdentifier",
                "quantity"
              ],
              "properties": {
                "skuIdentifier": {
                  "type": "string"
                },
                "quantity": {
                  "type": "integer"
                }
              }
            }
          }
        ],
        "responses": {
          "201": {
            "description": "success response. Voucher Dispensed",
            "schema": {
              "type": "object",
              "required": [
                "_embedded"
              ],
              "properties": {
                "_embedded": {
                  "type": "object",
                  "required": [
                    "vouchers"
                  ],
                  "properties": {
                    "vouchers": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "required": [
                          "pin",
                          "skuDescription",
                          "mediaType",
                          "faceValueInCents",
                          "status"
                        ],
                        "properties": {
                          "pin": {
                            "type": "integer",
                            "format": "int64"
                          },
                          "skuDescription": {
                            "type": "string"
                          },
                          "mediaType": {
                            "type": "string"
                          },
                          "faceValueInCents": {
                            "type": "integer",
                            "format": "int32"
                          },
                          "redeemable": {
                            "type": "boolean",
                            "description": "Indicates if the voucher can be redeemed or not."
                          },
                          "status": {
                            "type": "string"
                          },
                          "_links": {
                            "type": "object",
                            "required": [
                              "self"
                            ],
                            "properties": {
                              "self": {
                                "type": "object",
                                "properties": {
                                  "href": {
                                    "type": "string",
                                    "format": "uri"
                                  }
                                }
                              },
                              "redeem": {
                                "type": "object",
                                "description": "the link will be present if the voucher is in a redeemable state",
                                "properties": {
                                  "href": {
                                    "type": "string",
                                    "format": "uri"
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "Missing or invalid elements in the request"
          },
          "409": {
            "description": "Invalid parameters on retry",
            "schema": {
              "type": "object",
              "required": [
                "status",
                "message"
              ],
              "properties": {
                "status": {
                  "type": "integer",
                  "format": "int32"
                },
                "message": {
                  "type": "string"
                }
              }
            }
          },
          "415": {
            "description": "Unsupported media type",
            "schema": {
              "type": "object",
              "required": [
                "status",
                "message"
              ],
              "properties": {
                "status": {
                  "type": "integer",
                  "format": "int32"
                },
                "message": {
                  "type": "string"
                }
              }
            }
          },
          "503": {
            "description": "Service unavailable. Voucher are pending generation",
            "schema": {
              "type": "object",
              "required": [
                "status",
                "message"
              ],
              "properties": {
                "status": {
                  "type": "integer",
                  "format": "int32"
                },
                "message": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/redemption/{transaction-id}": {
      "put": {
        "description": "Update the vouchers in the request body as redeemed",
        "operationId": "updateVouchersToUsed",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/hal+json"
        ],
        "parameters": [
          {
            "name": "channel-id",
            "in": "header",
            "description": "channel the request originated from",
            "required": true,
            "type": "string"
          },
          {
            "name": "application-id",
            "in": "header",
            "description": "application the request originated from",
            "required": true,
            "type": "string"
          },
          {
            "name": "store-id",
            "in": "header",
            "description": "Unique identifier for the retail store location.",
            "required": false,
            "type": "string"
          },
          {
            "name": "Content-Type",
            "in": "header",
            "description": "application/json is supported",
            "required": true,
            "type": "string"
          },
          {
            "name": "transaction-id",
            "in": "path",
            "description": "A unique transaction id for each voucher redemption request",
            "required": true,
            "type": "string"
          },
          {
            "name": "body",
            "in": "body",
            "description": "Request to update the vouchers to used",
            "required": true,
            "schema": {
              "type": "object",
              "required": [
                "mobileNumber",
                "collectionOfPins"
              ],
              "properties": {
                "mobileNumber": {
                  "type": "string",
                  "description": "10 digit mobile number of a prepaid subscriber"
                },
                "collectionOfPins": {
                  "type": "array",
                  "items": {
                    "type": "integer",
                    "format": "int64"
                  }
                }
              }
            }
          }
        ],
        "responses": {
          "201": {
            "description": "success response"
          },
          "400": {
            "description": "Missing or invalid elements in the request"
          },
          "409": {
            "description": "If the transaction was already executed with different parameters."
          },
          "412": {
            "description": "One or more voucher in the request is not redeemable",
            "schema": {
              "type": "object",
              "required": [
                "status",
                "message"
              ],
              "properties": {
                "status": {
                  "type": "integer",
                  "format": "int32"
                },
                "message": {
                  "type": "string"
                }
              }
            }
          },
          "415": {
            "description": "Unsupported media type",
            "schema": {
              "type": "object",
              "required": [
                "status",
                "message"
              ],
              "properties": {
                "status": {
                  "type": "integer",
                  "format": "int32"
                },
                "message": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/skus": {
      "get": {
        "description": "Returns available SKU's to dispense epins",
        "operationId": "getSKU",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "channel-id",
            "in": "header",
            "description": "channel the request originated from",
            "required": true,
            "type": "string"
          },
          {
            "name": "application-id",
            "in": "header",
            "description": "application the request originated from",
            "required": true,
            "type": "string"
          },
          {
            "name": "store-id",
            "in": "header",
            "description": "Unique identifier for the retail store location.",
            "required": false,
            "type": "string"
          },
          {
            "name": "media-type",
            "in": "query",
            "description": "query String to select media type",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "success response",
            "schema": {
              "type": "object",
              "required": [
                "_embedded"
              ],
              "properties": {
                "_embedded": {
                  "type": "object",
                  "required": [
                    "skus"
                  ],
                  "properties": {
                    "skus": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "required": [
                          "skuIdentifier",
                          "skuDescription",
                          "faceValueInCents"
                        ],
                        "properties": {
                          "skuIdentifier": {
                            "type": "string"
                          },
                          "skuDescription": {
                            "type": "string"
                          },
                          "faceValueInCents": {
                            "type": "integer"
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "Missing or invalid elements in the request"
          },
          "404": {
            "description": "sku not found for the media type",
            "schema": {
              "type": "object",
              "required": [
                "status",
                "message"
              ],
              "properties": {
                "status": {
                  "type": "integer",
                  "format": "int32"
                },
                "message": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    }
  },
  "definitions": {
    "availableSkus": {
      "type": "object",
      "required": [
        "_embedded"
      ],
      "properties": {
        "_embedded": {
          "type": "object",
          "required": [
            "skus"
          ],
          "properties": {
            "skus": {
              "type": "array",
              "items": {
                "type": "object",
                "required": [
                  "skuIdentifier",
                  "skuDescription",
                  "faceValueInCents"
                ],
                "properties": {
                  "skuIdentifier": {
                    "type": "string"
                  },
                  "skuDescription": {
                    "type": "string"
                  },
                  "faceValueInCents": {
                    "type": "integer"
                  }
                }
              }
            }
          }
        }
      }
    },
    "sku": {
      "type": "object",
      "required": [
        "skuIdentifier",
        "skuDescription",
        "faceValueInCents"
      ],
      "properties": {
        "skuIdentifier": {
          "type": "string"
        },
        "skuDescription": {
          "type": "string"
        },
        "faceValueInCents": {
          "type": "integer"
        }
      }
    },
    "epinRequest": {
      "type": "object",
      "required": [
        "skuIdentifier",
        "quantity"
      ],
      "properties": {
        "skuIdentifier": {
          "type": "string"
        },
        "quantity": {
          "type": "integer"
        }
      }
    },
    "voucherRedemption": {
      "type": "object",
      "required": [
        "mobileNumber",
        "collectionOfPins"
      ],
      "properties": {
        "mobileNumber": {
          "type": "string",
          "description": "10 digit mobile number of a prepaid subscriber"
        },
        "collectionOfPins": {
          "type": "array",
          "items": {
            "type": "integer",
            "format": "int64"
          }
        }
      }
    },
    "voucher": {
      "type": "object",
      "required": [
        "skuDescription",
        "mediaType",
        "faceValueInCents",
        "status"
      ],
      "properties": {
        "skuDescription": {
          "type": "string"
        },
        "mediaType": {
          "type": "string"
        },
        "faceValueInCents": {
          "type": "integer",
          "format": "int32"
        },
        "status": {
          "type": "string"
        },
        "_links": {
          "type": "object",
          "required": [
            "self"
          ],
          "properties": {
            "self": {
              "type": "object",
              "properties": {
                "rel": {
                  "type": "string"
                },
                "href": {
                  "type": "string",
                  "format": "uri"
                }
              }
            },
            "redeem": {
              "type": "object",
              "description": "the link will be present if the voucher is in a redeemable state",
              "properties": {
                "rel": {
                  "type": "string"
                },
                "href": {
                  "type": "string",
                  "format": "uri"
                }
              }
            }
          }
        }
      }
    },
    "voucherWithPin": {
      "type": "object",
      "required": [
        "pin",
        "skuDescription",
        "mediaType",
        "faceValueInCents",
        "status"
      ],
      "properties": {
        "pin": {
          "type": "integer",
          "format": "int64"
        },
        "skuDescription": {
          "type": "string"
        },
        "mediaType": {
          "type": "string"
        },
        "faceValueInCents": {
          "type": "integer",
          "format": "int32"
        },
        "redeemable": {
          "type": "boolean",
          "description": "Indicates if the voucher can be redeemed or not."
        },
        "status": {
          "type": "string"
        },
        "_links": {
          "type": "object",
          "required": [
            "self"
          ],
          "properties": {
            "self": {
              "type": "object",
              "properties": {
                "href": {
                  "type": "string",
                  "format": "uri"
                }
              }
            },
            "redeem": {
              "type": "object",
              "description": "the link will be present if the voucher is in a redeemable state",
              "properties": {
                "href": {
                  "type": "string",
                  "format": "uri"
                }
              }
            }
          }
        }
      }
    },
    "vouchersWithPin": {
      "type": "object",
      "required": [
        "_embedded"
      ],
      "properties": {
        "_embedded": {
          "type": "object",
          "required": [
            "vouchers"
          ],
          "properties": {
            "vouchers": {
              "type": "array",
              "items": {
                "type": "object",
                "required": [
                  "pin",
                  "skuDescription",
                  "mediaType",
                  "faceValueInCents",
                  "status"
                ],
                "properties": {
                  "pin": {
                    "type": "integer",
                    "format": "int64"
                  },
                  "skuDescription": {
                    "type": "string"
                  },
                  "mediaType": {
                    "type": "string"
                  },
                  "faceValueInCents": {
                    "type": "integer",
                    "format": "int32"
                  },
                  "redeemable": {
                    "type": "boolean",
                    "description": "Indicates if the voucher can be redeemed or not."
                  },
                  "status": {
                    "type": "string"
                  },
                  "_links": {
                    "type": "object",
                    "required": [
                      "self"
                    ],
                    "properties": {
                      "self": {
                        "type": "object",
                        "properties": {
                          "href": {
                            "type": "string",
                            "format": "uri"
                          }
                        }
                      },
                      "redeem": {
                        "type": "object",
                        "description": "the link will be present if the voucher is in a redeemable state",
                        "properties": {
                          "href": {
                            "type": "string",
                            "format": "uri"
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "errorModel": {
      "type": "object",
      "required": [
        "status",
        "message"
      ],
      "properties": {
        "status": {
          "type": "integer",
          "format": "int32"
        },
        "message": {
          "type": "string"
        }
      }
    }
  }
}