{
    "swagger": "2.0",
    "info": {
        "description": "The Generic version of DEEP.io Publish Event API",
        "version": "1.0",
        "title": "DEEP.io Publish Event POST API - Generic Version"
    },
    "host": "localhost:8080",
    "basePath": "/deep/v1/events",
    "paths": {
        "/{eventType}": {
            "post": {
                "tags": [
                    "messaging-controller"
                ],
                "summary": "This Generic API shall be used by producers to publish events into DEEP.io.",
                "operationId": "sendMessageUsingPOST",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "*/*"
                ],
                "parameters": [
                    {
                        "name": "eventType",
                        "in": "path",
                        "description": "Unique name of the event",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "in": "body",
                        "name": "event",
                        "description": "Event Complex Element",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/Event"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Message Published Successfully.",
                        "schema": {
                            "type": "object",
                            "properties": {}
                        }
                    },
                    "201": {
                        "description": "Message Broker has not accepted the message but has been Saved and need resubmission from UI. Please Contact Admin to resubmit"
                    },
                    "400": {
                        "description": "Not a valid Request"
                    },
                    "401": {
                        "description": "Unauthorized - Provided token is not a valid token"
                    },
                    "403": {
                        "description": "Forbidden - ProducerId should match with TeamName"
                    },
                    "426": {
                        "description": "Upgrade Required - Access Denied as request is not coming through Deep Gateway. Please use Gateway to publish the message."
                    },
                    "429": {
                        "description": "Too Many Requests"
                    },
                    "500": {
                        "description": "Error while sending message to Queue OR Error happened inside producer"
                    },
                    "501": {
                        "description": "Rule is Not Configured For EventType"
                    },
                    "502": {
                        "description": "Contract is Not Defined"
                    },
                    "503": {
                        "description": "Contract is defined but payload posted is not valid as per uploaded contract"
                    },
                    "506": {
                        "description": "Event is not Configured."
                    }
                }
            }
        }
    },
    "definitions": {
        "AuditInfo": {
            "type": "object",
            "properties": {
                "accountNumber": {
                    "type": "string",
                    "example": "F123111",
                    "description": "The financial account number."
                },
                "batchId": {
                    "type": "string",
                    "example": "batch0",
                    "description": "Identifer of batch job."
                },
                "customerId": {
                    "type": "string",
                    "example": "Customer1",
                    "description": "Uniquely identifies the Customer."
                },
                "iamUniqueId": {
                    "type": "string",
                    "example": "deepuser",
                    "description": "Unique identifier for Identity and Access Management."
                },
                "lineId": {
                    "type": "string",
                    "example": "1234567890",
                    "description": "Uniquely identifies a  line of service."
                },
                "orderId": {
                    "type": "string",
                    "example": "order1",
                    "description": "Identifier of order."
                },
                "phoneNumber": {
                    "type": "string",
                    "example": "1234567890",
                    "description": "The phone number (MSISDN) associated with the line of service."
                },
                "universalLineId": {
                    "type": "string",
                    "example": "1234567890",
                    "description": "ULID - Universal identifier of the LineofService, as defined in Customer Hub. "
                }
            },
            "description": "Audit information used to search specific event. All elements included in this complex element will be searchable in DEEP.io.",
            "example": "null"
        },
        "HeaderReference": {
            "type": "object",
            "properties": {
                "activityId": {
                    "type": "string",
                    "example": "null",
                    "description": "Unique id alphanumeric value generated and sent by the partner to identify every service request uniquely."
                },
                "applicationId": {
                    "type": "string",
                    "example": "null",
                    "description": "Identifies the application, system or platform name that is being used to initiate the transaction  related to this request."
                },
                "applicationUserId": {
                    "type": "string",
                    "example": "null",
                    "description": "NT id of the rep who is managing the transaction from CARE/RETAIL channels. Required for assisted channels, not required for unassisted channels."
                },
                "authCustomerId": {
                    "type": "string",
                    "example": "null",
                    "description": "Customer Id of Tmo customer that is being served/using the application. (Required for non anonymous flows)"
                },
                "authFinancialAccountId": {
                    "type": "string",
                    "example": "null",
                    "description": "Financial Account Id that the customer that is being served/using the application is working on.  (Required for non anonymous flows)"
                },
                "authLineOfServiceId": {
                    "type": "string",
                    "example": "null",
                    "description": "Line of service Id , within the Financial Account of the customer that is being served/using the application is working on. (Required for non anonymous flows)"
                },
                "channelId": {
                    "type": "string",
                    "example": "null",
                    "description": "Identifies the business unit or sales channel."
                },
                "dealerCode": {
                    "type": "string",
                    "example": "null",
                    "description": "Uniquely identifies the dealer/rep user."
                },
                "interactionId": {
                    "type": "string",
                    "example": "null",
                    "description": "Alphanumeric value represent a common transaction id across all calls made from UI, while completing all business activity needs of a particular customer."
                },
                "masterDealerCode": {
                    "type": "string",
                    "example": "null",
                    "description": "Code that uniquely identifies the master dealer for a large retail partner, such as Apple or Costco. Will not be applicable for TMo channels."
                },
                "segmentationId": {
                    "type": "string",
                    "example": "null",
                    "description": "Identifier of customers primary data center."
                },
                "senderId": {
                    "type": "string",
                    "example": "null",
                    "description": "Uniquely identifies an Operation consumer/Partner."
                },
                "sessionId": {
                    "type": "string",
                    "example": "null",
                    "description": "A value populated by the sender used to track the transactions that occur during a session, a long-lasting interaction, managed by the sender. GUID generated by partner. Within one session (sessionid) for Rep/system can serve  multiple customers(interactionid) and in a given customer interation ��� multiple API calls (activityid) will be made to serve the customer. 1 sessionid -> many interactionid 1 interactionid -> many activityid"
                },
                "storeId": {
                    "type": "string",
                    "example": "null",
                    "description": "Unique identifier for the retail store location."
                },
                "terminalId": {
                    "type": "string",
                    "example": "null",
                    "description": "Unique identifier for the retail store terminal."
                },
                "tillId": {
                    "type": "string",
                    "example": "null",
                    "description": "Unique identifier for the retail store terminal till number."
                },
                "timestamp": {
                    "type": "string",
                    "format": "date-time",
                    "example": "null",
                    "description": "A timestamp provided by sender to track their workflow."
                },
                "workflowId": {
                    "type": "string",
                    "example": "null",
                    "description": "Name of business purpose/flow, for which the API is being invoked."
                }
            },
            "description": "standard HTTP header elements to be able to deliver them to consumer without interruption",
            "example": "null"
        },
        "Specifications": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string",
                    "example": "null"
                },
                "value": {
                    "type": "string",
                    "example": "null"
                }
            },
            "description": "Name value pair."
        },
        "Event": {
            "type": "object",
            "required": [
                "eventProducerId",
                "eventTime",
                "eventType",
                "payload"
            ],
            "properties": {
                "auditInfo": {
                    "$ref": "#/definitions/AuditInfo"
                },
                "eventId": {
                    "type": "string",
                    "example": "Event-123",
                    "description": "Unique identifier for the event generated by the event producer. This element will be searchable in DEEP.io UI"
                },
                "eventProducerId": {
                    "type": "string",
                    "example": "Billing",
                    "description": "Unique identifier for the producer of the event."
                },
                "eventTime": {
                    "type": "string",
                    "format": "date-time",
                    "example": "2017-03-27T16:20:11.108Z",
                    "description": "The date and time that the event occurred."
                },
                "eventType": {
                    "type": "string",
                    "example": "PortInStatusChanged",
                    "description": "Event Name that you want to publish"
                },
                "eventVersion": {
                    "type": "string",
                    "example": "1.0",
                    "description": "version of the event , will be used to distinguish different versions by a consumer"
                },
                "headerReference": {
                    "$ref": "#/definitions/HeaderReference"
                },
                "payload": {
                    "$ref": "#/definitions/Payload"
                },
                "specifications": {
                    "type": "array",
                    "example": "null",
                    "description": "added for future reference so can ignore.",
                    "items": {
                        "$ref": "#/definitions/Specifications"
                    }
                }
            }
        },
        "Payload": {
            "type": "object",
            "description": "Event specific information that can be modeled using APIv2 or inline elements.",
            "properties": {
                "portinstatuschanged": {
                    "$ref": "#/definitions/Portinstatuschanged"
                }
            }
        },
        "Portinstatuschanged": {
            "type": "object",
             "required": [
                "financialAccountNumber",
                "portInRequestNumber",
                "portInStatus"
            ],
            "properties": {
                "financialAccountNumber": {
                    "description": "The BAN associated with Port-IN request.",
                    "example": "123456789",
                    "type": "string"
                },
                "portInRequestNumber": {
                    "description": "A transaction id for the entire port-in request and NOT the line level request.  This will be used by clients to resubmit the Port-in requests.",
                    "example": "99307649",
                    "type": "string"
                },
                "portInStatus": {
                    "description": "The Port In request status.",
                    "example": "I",
                    "type": "string",
                    "enum": [
                        "I",
                        "F",
                        "S",
                        "R"
                    ]
                },
                "MSISDNdata": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/MSISDNdata"
                    }
            }
        }
    },
    "MSISDNdata": {
            "type": "object",
            "properties": {
               "portInNumber": {
                    "description": "Numbers that are going to port to TMO.",
                    "example": "3234567894",
                    "type": "string",
                    "minLength": 10,
                    "maxLength": 15,
                    "pattern": "^[0-9]+$"
                },
                "tempMsisdn": {
                    "description": "Temp MSISDN until port in request completed.",
                    "example": "6312894612",
                    "type": "string",
                    "minLength": 10,
                    "maxLength": 15,
                    "pattern": "^[0-9]+$"
                },
                "portInErrorCode": {
                    "description": "Error code - to be used for failure.",
                    "example": "6R",
                    "type": "string"
                },
                "portInUpdateDescription": {
                    "description": "Description of the error code. ",
                    "example": "I am error description for code",
                    "type": "string"
                }
            }
        }
  }
}