{
  "swagger": "2.0",
  "info": {
    "description": "Provides ability to retrieve available Sprint customer invoices in pdf format.",
    "version": "1.0.0",
    "title": "Retrieve Sprint customer invoices",
    "contact": {
      "name": "BillPresentationMicroservicesDevOps",
      "url": "http://www.t-mobile.com",
      "email": "BillPresentationMicroservicesDevOps@T-Mobile.com"
    },
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0"
    },
    "termsOfService": "urn:tos",
    "x-createdBy": {
      "dateCreated": "Mon Apr 22 05:34:35 2019",
      "createdBy": "akrishn16",
      "application": "Recite",
      "appVersion": "1.0.0.2891",
      "documentId": "83758600-21dc-435a-a729-34dff789f724",
      "status": "Conceptual - Initial",
      "classification": "7.3.3 Bill Delivery Mgmt.",
      "profile": "Core Business Capability Service",
      "serviceLayer": "Enterprise - Billing"
    }
  },
  "x-servers": [
    {
      "url": "http://127.0.0.1/",
      "description": "Test Server 1"
    }
  ],
  "host": "dlab02.core.op.api.t-mobile.com",
  "basePath": "/billing/v1/sprint-billdetails",
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/pdf"
  ],
  "paths": {
    "/accounts/{accountId}/invoices/{invoiceDate}/pdf": {
      "get": {
        "operationId": "queryPdfInvoiceInfo",
        "description": "Retrieves Sprint customer invoice information in PDF format.",
        "tags": [
          "Get Sprint Documents"
        ],
        "x-api-pattern": "QueryResource",
        "parameters": [
          {
            "$ref": "#/parameters/accountId"
          },
		  {
            "$ref": "#/parameters/accountnumber"
          },
          {
            "in": "path",
            "name": "invoiceDate",
            "required": true,
            "type": "string",
            "format": "date",
            "x-example": "2019-05-26",
            "description": "Date of invoice in YYYY-MM-DD format"
          },
          {
            "$ref": "#/parameters/Authorization"
          },
          {
            "$ref": "#/parameters/applicationId"
          },
          {
            "$ref": "#/parameters/applicationUserId"
          },
          {
            "$ref": "#/parameters/consumerId"
          },
          {
            "$ref": "#/parameters/conversationId"
          },
          {
            "$ref": "#/parameters/messageId"
          },
          {
            "$ref": "#/parameters/enterpriseMessageId"
          },
          {
            "$ref": "#/parameters/messageDateTimeStamp"
          },
          {
            "$ref": "#/parameters/AcceptParam"
          },
          {
            "in": "query",
            "name": "invoiceType",
            "required": true,
            "type": "string",
            "maxLength": 1,
            "enum": [
              "D",
              "S"
            ],
            "x-example": "D",
            "description": "Invoice Type (D - Detailed Invoice, S - Summary Invoice)"
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
			"schema": {
              "description": "Customer invoice in PDF format.",
              "title": "customerInvoiceInfoInPdfFormat",
              "type": "string",
              "format": "binary",
			  "example": "binary representation of the PDF file"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/httpErrorResponse"
            },
            "examples": {
              "errors": [
                {
                  "code": "400",
                  "message": "Bad Request",
                  "type": "ValidationException"
                }
              ]
            }
          },
          "401": {
            "description": "Unauthorized",
            "schema": {
              "$ref": "#/definitions/httpErrorResponse"
            },
            "examples": {
              "errors": [
                {
                  "code": "401",
                  "message": "Unauthorized",
                  "type": "UnauthorizedException"
                }
              ]
            }
          },
          "403": {
            "description": "Forbidden",
            "schema": {
              "$ref": "#/definitions/httpErrorResponse"
            },
            "examples": {
              "errors": [
                {
                  "code": "403",
                  "message": "Forbidden exception",
                  "type": "OAuthException"
                }
              ]
            }
          },
          "404": {
            "description": "Resource Not found",
            "schema": {
              "$ref": "#/definitions/httpErrorResponse"
            },
            "examples": {
              "errors": [
                {
                  "code": "404",
                  "message": "resource not found",
                  "type": "NotFoundException"
                }
              ]
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/httpErrorResponse"
            },
            "examples": {
              "errors": [
                {
                  "code": "500",
                  "message": "Internal Server Error",
                  "type": "ServerException"
                }
              ]
            }
          }
        },
        "summary": "Retrieves Sprint customer invoice information in PDF format.",
        "security": [
          {
            "oAuth": []
          }
        ]
      }
    }
  },
  "parameters": {
    "accountId": {
      "name": "accountId",
      "in": "path",
      "required": true,
      "type": "string",
      "maxLength": 9,
      "pattern": "^[0-9]*$",
      "x-example": "121343433",
      "description": "Billing Account Number"
    },
    "accountnumber": {
      "name": "accountnumber",
      "in": "header",
      "required": false,
      "type": "string",
      "x-example": "123456789",
      "description": "T-Mobile account number. This is required to access this API in APIGEE SaaS ennvironment.",
      "format": "string"
    },
    "applicationId": {
      "name": "applicationId",
      "in": "header",
      "required": true,
      "type": "string",
      "x-example": "ECMA",
      "description": "channelid (Incoming Request). \n Sprint Description: The ID of the application within the corporate entity from which this message originated.Eg:ECMW,ECMA,LVO,HJS,UYV,2IE",
      "format": "string"
    },
    "applicationUserId": {
      "name": "applicationUserId",
      "in": "header",
      "required": true,
      "type": "string",
      "x-example": "ECMA",
      "description": "channelid (Incoming Request). \n Sprint Description: The ID of the user of the application within the corporate entity from which the message originated.If not provided, applicationId value will be used.Eg:ECMW,ECMA,LVO,HJS,UYV,2IE.",
      "format": "string"
    },
    "Authorization": {
      "name": "Authorization",
      "in": "header",
      "description": "For accessing the API a valid JWT token must be passed in the 'Authorization' header. The following syntax must be used in the 'Authorization' header: Bearer xxxxxx.yyyyyyy.zzzzzz",
      "x-example": "Bearer xxxxxx.yyyyyyy.zzzzzz",
      "required": true,
      "type": "string",
      "minLength": 1,
      "maxLength": 256,
      "pattern": "^[\\S ]+$"
    },
    "consumerId": {
      "name": "consumerId",
      "in": "header",
      "required": false,
      "type": "string",
      "x-example": "MyTMO",
      "description": "The ID of the corporate entity or busines unit from which this message originated (e.g. Corporation code or Portal applId)."
    },
    "conversationId": {
      "name": "conversationId",
      "in": "header",
      "required": false,
      "type": "string",
      "x-example": "ECMW24379565705102019",
      "description": "Each message is part of a conversation between two or more systems in a given session.The Conversation ID is a UID, which can be used to group all messages exchanged within a session.This is field is primarily used for tracking, reporting, root-cause-analysis, and non-repudiation."
    },
    "messageId": {
      "name": "messageId",
      "in": "header",
      "required": true,
      "type": "string",
      "x-example": "ECMW243795657051019101010",
      "description": "Unique Identifier (applicationId + BAN + timestamp [MMDDYYHHMMSS]). \n Sprint Description: A unique identifier for this message, to enable tracking, reporting, and correlation.",
      "format": "string"
    },
    "enterpriseMessageId": {
      "name": "enterpriseMessageId",
      "in": "header",
      "required": false,
      "type": "string",
      "x-example": "ECMW243795657051019101010",
      "description": "Unique Identifier (applicationId + BAN + timestamp [MMDDYYHHMMSS]). \n Sprint Description: The ID of the application within the corporate entity from which message is originated along with a unique identifier for this message. Eg:ECMW243795657051019101010",
      "format": "string"
    },
    "messageDateTimeStamp": {
      "name": "messageDateTimeStamp",
      "in": "header",
      "required": true,
      "type": "string",
      "x-example": "2019-05-26T21:32:52Z",
      "description": "System Date Time (e.g 2001-10-26T19:32:52Z). Sprint Description: The dateTimeStamp of when the message was created. This can be used for tracking and reporting purposes. The value should be in a valid xsd dateTime object format and include the timezone offset.Eg:2019-06-02T18:45:18Z",
      "format": "date-time"
    },
    "AcceptParam": {
      "name": "Accept",
      "in": "header",
      "description": "Content-Types that are acceptable are only application/pdf",
      "required": false,
      "type": "string",
      "x-example": "application/pdf"
    }
  },
  "definitions": {
    "httpErrorResponse": {
      "type": "object",
      "properties": {
        "errors": {
          "type": "array",
          "description": "errors",
          "items": {
            "type": "object",
            "properties": {
              "code": {
                "type": "string",
                "description": "Code associated to the error."
              },
              "message": {
                "type": "string",
                "description": "Message associated with it error code."
              },
              "type": {
                "type": "string",
                "description": "Type of the generated error."
              }
            }
          }
        }
      }
    }
  },
  "securityDefinitions": {
    "oAuth": {
      "type": "apiKey",
      "description": "For accessing the API a valid JWT token must be passed in the 'Authorization' header. The following syntax must be used in the 'Authorization' header:  Bearer xxxxxx.yyyyyyy.zzzzzz",
      "name": "Authorization",
      "in": "header"
    }
  }
}