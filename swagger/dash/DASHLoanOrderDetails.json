swagger: '2.0'
info:
  description: DASHOrders with LoanEvents Publish API
  version: 1.0.0
  title: dash-dfs-loandetails
  contact:
    name: firstname lastname
    url: 'http://www.t-mobile.com'
    email: someone@t-mobile.com
  license:
    name: Proprietary software
    url: 'https://en.wikipedia.org/wiki/Proprietary_software'
  termsOfService: 'https://www.t-mobile.com/templates/popup.aspx?PAsset=Ftr_Ftr_TermsAndConditions'
  x-createdBy:
    dateCreated: 'Tue Aug  6 17:35:15 2019'
    createdBy: SMandal
    application: DASH
    domain: retail
    appVersion: 1.0.0.2891
    documentId: a76b041d-6c82-43e2-8898-9ddbf630d9a1
    status: Conceptual - Initial
    classification: 10.1 Frontline Analytics
    profile: Core Business Capability Service
    serviceLayer: Enterprise - CustomerManagement
x-servers:
  - url: 'http://127.0.0.1/'
    description: Test Server 1
tags:
  - name: getdashloanorderdetails
    description: This is a GET method for retrieving the list of BANs with deposit paid in AAL path.
host: dev01.api.t-mobile.com
basePath: /retail/loanorder/v1
schemes:
  - https
paths:
  '/getdashloanorderdetails':
    get:
      tags:
        - getdashloanorderdetails
      x-api-pattern: QueryCollection
      summary: This API shall be used by DFS to retrieve the DASH loan transaction details
      description: This API shall be used by DFS to retrieve the DASH loan transaction details
      operationId: getdashloanorderdetails
      consumes:
        - application/json
      produces:
        - '*/*'
      parameters:
        - name: ccid
          in: query
          description: Customer CCID
          required: false
          type: string
          pattern: '[a-zA-Z0-9]'
          x-example: 1-aSD456456
        - in: query
          name: accountnumber
          description: Event Complex Element
          required: false
          type: string
          pattern: '[0-9]'
          x-example: 963258741
        - in: query
          name: fromdate
          description: Order placed date
          required: false
          type: string
          pattern: 'YYYY-MM-DD'
          x-example: '2019-08-25'
        - name: Authorization
          in: header
          description: OAuth 2.0 access token with the authentication type set as Bearer.
          x-example: Basic YUFSbFZHQ1p5dE5KMzV6RVAzOVFneXFUS0tRM1VONHM6QmtYN0c5RmQxTnNEaHRsW
          enum:
            - Basic
            - High
          required: true
          pattern: '[a-zA-Z]'
          type: string
        - name: timestamp
          in: header
          description: Timestamp of the call
          required: false
          x-example: '2018-05-31T14:28:26.741Z'
          type: string
          format: date-time
      responses:
        '200':
          description: "Success"
          schema : 
            $ref: "#/definitions/Payload"
          examples:
           application/json:
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '400'
              userMessage: Bad Request
              systemMessage: Bad Request
              detailLink: 'http://www.tmus.com'
          headers: {}
        '401':
          description: Unauthorized
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '401'
              userMessage: Unauthorized
              systemMessage: Unauthorized
              detailLink: 'http://www.tmus.com'
          headers: {}
        '404':
          description: Resource not found
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '404'
              userMessage: Resource not found
              systemMessage: Resource not found
              detailLink: 'http://www.tmus.com'
          headers: {}
        '405':
          description: Method Not Allowed
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '405'
              userMessage: Method Not Allowed
              systemMessage: Method Not Allowed
              detailLink: 'http://www.tmus.com'
          headers:
            Allow:
              description: list of supported methods for URI
              x-example: GET
              type: string
              minLength: 1
              maxLength: 256
              pattern: '^[\S]*$'
            service-transaction-id:
              description: Request ID echoed back by server
              x-example: '23409209723'
              type: string
              minLength: 1
              maxLength: 256
              pattern: '^[\S]*$'
        '406':
          description: Mismatching data format
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '406'
              userMessage: Mismatching data format
              systemMessage: Mismatching data format
              detailLink: 'http://www.tmus.com'
          headers: {}
        '500':
          description: System Error
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '500'
              userMessage: System Error
              systemMessage: System Error
              detailLink: 'http://www.tmus.com'
          headers: {}
        '503':
          description: Service unavailable
          schema:
            $ref: '#/definitions/Error'
          examples:
            application/json:
              code: '503'
              userMessage: Service unavailable
              systemMessage: Service unavailable
              detailLink: 'http://www.tmus.com'
          headers: {}
definitions:
  Payload:
    type: object
    description: Event specific information that can be modeled using APIv2 or inline elements.
    properties:
      eventBody:
        $ref: '#/definitions/LoanDetailsEvent'
  LoanDetailsEvent:
    type: object
    description: 'Event specific body information information. '
    required:
      - commonCustomerId
      - ban
      - subscriber
      - billingResponsibleParty
    properties:
      commonCustomerId:
        description: 'customer creditcheck CCID'
        type: string
        example: 1-256ASCE
        pattern: '[A-Za-z0-9_]'
      ban:
        type: string
        example: 963258741
        pattern: '[0-9]'
        description: 'customer Account number'
      phoneNumber:
        type: string
        example: 963258741
        pattern: '[0-9]'
        description: 'customer phone number'
      binaryContent:
        description: Digital signature
        example: base64 encoded image
        type: string
        format: byte        
      subscriber:
        description: 'Minimum 1 line should be present.'
        type: array
        items:
          $ref: '#/definitions/subscriber'
      billingResponsibleParty:
          $ref: '#/definitions/billingResponsibleParty'
  subscriber:
    required:
      - activationMsisdn
      - portInInitiated
    properties:
      simNumber:
        type: string
        example: 4657981326546464
        pattern: '[0-9]'
        description: 'activated SIM number'
      simSku:
        type: string
        example: ZZZ260r060
        pattern: '[a-zA-Z0-9]'
        description: 'SIM SKU used in Activation'
      activationMsisdn:
        description: 'MSISDN assigned at activation.'
        type: string
        example: 4253259619
        pattern: '[0-9]{10}$'
      byodDeviceLoan:
        $ref: '#/definitions/byodDeviceLoan'
      portInInitiated:
        description: 'true / false '
        type: boolean
        example: Yes
        pattern: '[a-zA-Z]'
      portInNumber:
        description: 'Port in number if PortInInitiated is true else Activation MSIDN '
        type: string
        example: 4253698520
        pattern: '[0-9]{10}$'
  billingResponsibleParty:
    required:
      - firstName
      - lastName
      - primaryEmailAddress
      - activationMSISDN
      - portInInitiated
    properties:
      firstName:
        type: string
        example: sriram
        pattern: '[A-Za-z0-9 ]'
        description: 'customer first name'
      lastName:
        type: string
        example: Mandalapu
        pattern: '[A-Za-z0-9 ]'
        description: 'Customer last name'
      primaryEmailAddress:
        type: string
        example: DASHdevops@t-mobile.com
        pattern: '[a-zA-Z0-9.@]'
        description: 'customer email address'
      activationMSISDN:
        description: Activation MSIDN
        type: string
        example: 4253569852
        pattern: '[0-9]{10}$'
      portInInitiated:
        description: 'true / false '
        type: boolean
        example: Yes
        pattern: '[A-Za-z]'
      portInNumber:
        description: 'Port in number if PortInInitiated is true'
        type: string
        example: 2589631472
        pattern: '[0-9]{10}$'
  byodDeviceLoan:
    properties:
      carrier:
        description: 'Verizon /Sprint/ ATT.'
        type: string
        example: Sprint
        pattern: '[a-zA-Z]'
      devicePhoneNumber:
        description: 'Previous carrier MSISDN'
        type: string
        example: 2589631472
        pattern: '[0-9]{10}$'
      remainingBalance:
        description: 'Amount customer pays'
        type: string
        example: 100
        pattern: '[0-9.]'
      leasePayoffAmount:
        description: 'Total amount to be paid to the other carrier'
        type: string
        example: 200
        pattern: '[0-9.]'
      financedAmount:
        description: 'Amount to be paid for the device'
        type: string
        example: 650
        pattern: '[0-9]'
  Error:
    description: 'As defined in http://api-standards.apps.px-npe01.cf.t-mobile.com/http/status-codes/error-response-format/'
    type: object
    required:
      - code
      - userMessage
    properties:
      code:
        description: 'A succinct, domain-specific, human-readable text string to identify the type of error for the given status code'
        example: ProductNotFound
        type: string
        minLength: 0
        maxLength: 256
        pattern: '^[\S ]+$'
      userMessage:
        description: A human-readable message describing the error.
        example: Failed to find product.
        type: string
        minLength: 0
        maxLength: 256
        pattern: '^[\S ]+$'
      systemMessage:
        description: Text that provides a more detailed technical explanation of the error
        example: PRODUCT_NOT_FOUND
        type: string
        minLength: 0
        maxLength: 256
