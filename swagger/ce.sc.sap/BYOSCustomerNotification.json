{
  "swagger": "2.0",
  "info": {
    "description": "The Generic version of DEEP.io Publish Event API",
    "version": "1.0.1",
    "title": "DEEP.io Publish Event POST API - Generic Version",
    "contact": {
      "email": "DEEP.io@T-Mobile.com",
      "name": "Deep IO"
    }
  },
  "host": "deepio-internal.t-mobile.com",
  "basePath": "/deep/v1/events",
  "x-servers": [
    {
      "url": "https://deepio-internal-development.t-mobile.com/{environment_name}/deep/v1/events/{Your_Event_Name}",
      "description": "All Development"
    },
    {
      "url": "https://deepio-internal-npe.t-mobile.com/{environment_name}/deep/v1/events/{Your_Event_Name}",
      "description": "All NPE"
    },
    {
      "url": "https://deepio-internal-plab.t-mobile.com/{environment_name}/deep/v1/events/{Your_Event_Name}",
      "description": "All PLABs"
    }
  ],
  "tags": [
    {
      "name": "event-producer",
      "description": "Sends events to DEEP IO"
    }
  ],
  "schemes": [
    "https"
  ],
  "paths": {
    "/{event-type}": {
      "post": {
        "tags": [
          "messaging-controller"
        ],
        "x-api-pattern": "CreateInCollection",
        "summary": "This Generic API shall be used by producers to publish events into DEEP.io.",
        "description": "Publishes an event to DEEP IO",
        "operationId": "sendMessageUsingPOST",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "*/*"
        ],
        "parameters": [
          {
            "name": "event-type",
            "in": "path",
            "description": "Unique name of the event",
            "required": true,
            "type": "string",
            "x-example": "Event-123",
            "pattern": "^[A-Za-z0-9-_]+$",
            "minLength": 1,
            "maxLength": 255
          },
          {
            "in": "body",
            "name": "event",
            "description": "Event Complex Element",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Event"
            }
          },
          {
            "in": "header",
            "name": "Content-Type",
            "description": "Content type passed",
            "required": true,
            "type": "string",
            "x-example": "application/json",
            "pattern": "^[ \\S]+$",
            "minLength": 1,
            "maxLength": 30
          },
          {
            "name": "Authorization",
            "in": "header",
            "description": "Bearer token passed created by the client in order to access service api",
            "required": true,
            "type": "string",
            "x-example": "Valid Bearer token ",
            "pattern": "^[\\S ]+$",
            "minLength": 1,
            "maxLength": 500
          }
        ],
        "responses": {
          "200": {
            "description": "Message Published Successfully.",
            "examples": {
              "application/json": {
                "message": "Message Published Successfully",
                "status": "200",
                "timestamp": "Mon Apr 15 23:11:04 UTC 2019"
              }
            }
          },
          "201": {
            "description": "Message Broker has not accepted the message but has been Saved and need resubmission from UI. Please Contact Admin to resubmit",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "exception": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "path": "ServletWebRequest: uri=/development/v1/events/DemoEvent;client=198.18.53.102",
                "message": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "error": "CREATED",
                "timestamp": "Thu Jun 27 22:35:14 UTC 2019",
                "status": "201"
              }
            }
          },
          "400": {
            "description": "EventType in URL does not match with eventType in Event Payload,",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "exception": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "path": "ServletWebRequest: uri=/development/v1/events/DemoEvent;client=198.18.53.102",
                "message": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "error": "BAD_REQUEST",
                "timestamp": "Thu Jun 27 22:35:14 UTC 2019",
                "status": "400"
              }
            }
          },
          "401": {
            "description": "EventProducerId and Token senderId doesn't match to publish event in environment.",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "exception": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "path": "ServletWebRequest: uri=/development/v1/events/DemoEvent;client=198.18.53.102",
                "message": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "error": "UNAUTHORIZED",
                "timestamp": "Thu Jun 27 22:35:14 UTC 2019",
                "status": "401"
              }
            }
          },
          "403": {
            "description": "ProducerId is not authorized to publish event in env or Event not associated with any Team name in DEEP.io Registry",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "exception": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "path": "ServletWebRequest: uri=/development/v1/events/DemoEvent;client=198.18.53.102",
                "message": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "error": "FORBIDDEN",
                "timestamp": "Thu Jun 27 22:35:14 UTC 2019",
                "status": "403"
              }
            }
          },
          "422": {
            "description": "Event Not Availability check validation failed for Event.",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "exception": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "path": "ServletWebRequest: uri=/development/v1/events/DemoEvent;client=198.18.53.102",
                "message": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "error": "UNPROCESSABLE_ENTITY",
                "timestamp": "Thu Jun 27 22:35:14 UTC 2019",
                "status": "422"
              }
            }
          },
          "426": {
            "description": "Access Denied as request is not coming through Deep Gateway. Please use Gateway to publish the message.",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "exception": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "path": "ServletWebRequest: uri=/development/v1/events/DemoEvent;client=198.18.53.102",
                "message": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "error": "UPGRADE_REQUIRED",
                "timestamp": "Thu Jun 27 22:35:14 UTC 2019",
                "status": "426"
              }
            }
          },
          "500": {
            "description": "Internal Server Error.",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "exception": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "path": "ServletWebRequest: uri=/development/v1/events/DemoEvent;client=198.18.53.102",
                "message": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "error": "INTERNAL_SERVER_ERROR",
                "timestamp": "Thu Jun 27 22:35:14 UTC 2019",
                "status": "500"
              }
            }
          },
          "501": {
            "description": "Not a valid Request",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "exception": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "path": "ServletWebRequest: uri=/development/v1/events/DemoEvent;client=198.18.53.102",
                "message": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "error": "NOT_IMPLEMENTED",
                "timestamp": "Thu Jun 27 22:35:14 UTC 2019",
                "status": "501"
              }
            }
          },
          "503": {
            "description": "Routing key not defined.",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "exception": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "path": "ServletWebRequest: uri=/development/v1/events/DemoEvent;client=198.18.53.102",
                "message": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "error": "SERVICE_UNAVAILABLE",
                "timestamp": "Thu Jun 27 22:35:14 UTC 2019",
                "status": "503"
              }
            }
          },
          "506": {
            "description": "Producer has been disabled, as there is no Event Configuration configured for this Event. Please promote your Event upload valid swagger.",
            "schema": {
              "$ref": "#/definitions/Error"
            },
            "examples": {
              "application/json": {
                "exception": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "path": "ServletWebRequest: uri=/development/v1/events/DemoEvent;client=198.18.53.102",
                "message": "To read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
                "error": "VARIANT_ALSO_NEGOTIATES",
                "timestamp": "Thu Jun 27 22:35:14 UTC 2019",
                "status": "506"
              }
            }
          }
        },
        "security": [
          {
            "Oauth": [
              "post:SubmitEvent"
            ]
          }
        ]
      }
    }
  },
  "definitions": {
    "Error": {
      "type": "object",
      "properties": {
        "exception": {
          "type": "string",
          "example": "DemoEvent-- Event not available in DEEP.io -- developmentenvironment.\nTo read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
          "description": "Error message and information to help resolve the error.",
          "pattern": "^(.)*$",
          "minLength": 1,
          "maxLength": 2000
        },
        "path": {
          "type": "string",
          "example": "ServletWebRequest: uri=/development/v1/events/DemoEvent;client=198.18.53.10",
          "description": "Path information and client IP.",
          "pattern": "^(.)*$",
          "minLength": 1,
          "maxLength": 2000
        },
        "message": {
          "type": "string",
          "example": "DemoEvent-- Event not available in DEEP.io -- developmentenvironment.\nTo read more details about this error, visit https://qwiki.internal.t-mobile.com/display/DIGEE/Publisher+-+Error+codes and look for error the specific error code.",
          "description": "Error message and information to help resolve the error.",
          "minLength": 1,
          "maxLength": 2000,
          "pattern": "^(.)*$"
        },
        "error": {
          "type": "string",
          "example": "UNPROCESSABLE_ENTITY",
          "description": "Error code for the error.",
          "minLength": 1,
          "maxLength": 255,
          "pattern": "^(.)*$"
        },
        "timestamp": {
          "type": "string",
          "example": "Thu Jun 27 22:35:14 UTC 2019",
          "description": "Error message and information to help resolve the error.",
          "minLength": 10,
          "maxLength": 100,
          "format": "date-time"
        },
        "status": {
          "type": "string",
          "example": "422",
          "description": "Status code that was returned.",
          "pattern": "^[\\d]*$",
          "minLength": 3,
          "maxLength": 3
        }
      },
      "description": "Error returned"
    },
    "AuditInfo": {
      "type": "object",
      "properties": {
        "accountNumber": {
          "type": "string",
          "example": "F123111",
          "description": "The financial account number identifies the billing account for which the invoice was generated.",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d]*$"
        },
        "batchId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "Batch identification from the event producer, if the event was produced as part of a batch.",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d\\-]*$"
        },
        "customerId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "Uniquely identifies the Customer.",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d\\-]*$"
        },
        "iamUniqueId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "Unique identifier for Identity and Access Management.",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d\\-]*$"
        },
        "lineId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "Uniquely identifies a  line of service.",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d\\-]*$"
        },
        "orderId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "Identifier of order.",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d\\-]*$"
        },
        "phoneNumber": {
          "type": "string",
          "example": "1234567890",
          "description": "The phone number (MSISDN) associated with the line of service.",
          "minLength": 10,
          "maxLength": 15,
          "pattern": "^[a-zA-Z0-9]*$"
        },
        "universalLineId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "ULID - Universal identifier of the LineofService, as defined in Customer Hub. ",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d\\-]*$"
        }
      },
      "description": "Audit information used to search specific event. All elements included in this complex element will be searchable in DEEP.io.",
      "example": {
        "orderId": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
        "universalLineId": "4182f381-9736-11e9-adc6-2f8ba3ffe639"
      }
    },
    "HeaderReference": {
      "type": "object",
      "properties": {
        "activityId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "Uniquely identifies the business activity definition.",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d\\-]*$"
        },
        "applicationId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "Unique identification of a software application.",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d\\-]*$"
        },
        "applicationUserId": {
          "type": "string",
          "example": "Alexey211",
          "description": "Application user identification",
          "minLength": 3,
          "maxLength": 20,
          "pattern": "^[a-zA-Z\\d]*$"
        },
        "authCustomerId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "Customer Id of Tmo customer that is being served/using the application. (Required for non anonymous flows)",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d\\-]*$"
        },
        "authFinancialAccountId": {
          "type": "string",
          "example": "1ZY3101E9000591707",
          "description": "Financial Account Id that the customer that is being served/using the application is working on.  (Required for non anonymous flows)",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d]*$"
        },
        "authLineOfServiceId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "Line of service Id , within the Financial Account of the customer that is being served/using the application is working on. (Required for non anonymous flows)",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d\\-]*$"
        },
        "channelId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "Identifies the business unit or sales channel.",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d]*$"
        },
        "dealerCode": {
          "type": "string",
          "example": "2033443",
          "description": "A 7-character, T-Mobile-assigned code used to identify the master dealer contract for commissioning..",
          "minLength": 1,
          "maxLength": 256,
          "pattern": "^[a-zA-Z0-9\\-,\\.() ]*$"
        },
        "interactionId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "This Id is going to represent a common transaction id across all service calls made from UI, while completing all business activity needs of a particular customer. Self serve case, generated at time of login and all calls will have this same id. In Assisted channel case, each customer lookup will generate a new interaction Id and carried for all calls made while serving that customer.",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d]*$"
        },
        "masterDealerCode": {
          "type": "string",
          "example": "2033443",
          "description": "A 7-character, T-Mobile-assigned code used to identify the master dealer contract for commissioning.",
          "minLength": 1,
          "maxLength": 256,
          "pattern": "^[a-zA-Z0-9\\-,\\.() ]*$"
        },
        "segmentationId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "Segmentation Id represents the customer active data center; this is used to route messages to appropriate data center. All financial accounts under customer will be at the same data center.",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d]*$"
        },
        "senderId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "Uniquely identifies the sender.",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d]*$"
        },
        "sessionId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "A value populated by the sender used to track the transactions that occur during a session, a long-lasting interaction, managed by the sender. GUID generated by partner. Within one session (sessionid) for Rep/system can serve  multiple customers(interactionid) and in a given customer interaction multiple API calls (activityid) will be made to serve the customer. 1 sessionid -> many interactionid 1 interactionid -> many activityid",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d]*$"
        },
        "storeId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "Unique identifier for the store.",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d]*$"
        },
        "terminalId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "Uniquely identifies a payment terminal in a retail store.",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d]*$"
        },
        "tillId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "Unique identifier for the retail store terminal till number.",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d]*$"
        },
        "timestamp": {
          "type": "string",
          "format": "date-time",
          "example": "2019-06-28T14:01:40.224-07:00",
          "description": "A timestamp provided by Sender to track their workflow.  Usually set to the moment when a request is made.",
          "minLength": 1,
          "maxLength": 50
        },
        "workflowId": {
          "type": "string",
          "example": "4182f381-9736-11e9-adc6-2f8ba3ffe639",
          "description": "A value populated by the Sender used to track their workflow within the confines of a session. Senders are encouraged to develop a useful scheme. Do not use customer-sensitive information.",
          "minLength": 1,
          "maxLength": 50,
          "pattern": "^[a-zA-Z\\d]*$"
        }
      },
      "description": "standard HTTP header elements to be able to deliver them to consumer without interruption",
      "example": {
        "masterDealerCode": "2033443",
        "workflowId": "4182f381-9736-11e9-adc6-2f8ba3ffe639"
      }
    },
    "Specifications": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string",
          "example": "Key",
          "description": "Name for the name value pair",
          "minLength": 1,
          "maxLength": 255,
          "pattern": "^(.)*$"
        },
        "value": {
          "type": "string",
          "example": "Value",
          "description": "Value for the name value pair",
          "minLength": 1,
          "maxLength": 255,
          "pattern": "^(.)*$"
        }
      },
      "description": "Name value pair."
    },
    "Event": {
      "type": "object",
      "required": [
        "eventProducerId",
        "eventTime",
        "eventType",
        "payload"
      ],
      "properties": {
        "auditInfo": {
          "$ref": "#/definitions/AuditInfo"
        },
        "eventId": {
          "type": "string",
          "example": "Event-123",
          "description": "Unique identifier for the event generated by the event producer. This element will be searchable in DEEP.io UI",
          "pattern": "^[A-Za-z0-9-_]+$",
          "minLength": 1,
          "maxLength": 255
        },
        "eventProducerId": {
          "type": "string",
          "example": "Producer1",
          "description": "Unique identifier for the producer of the event.",
          "pattern": "^[A-Za-z0-9-_]+$",
          "minLength": 1,
          "maxLength": 255
        },
        "eventTime": {
          "type": "string",
          "format": "date-time",
          "example": "2017-03-27T16:20:11.108Z",
          "description": "The date and time that the event occurred.",
          "minLength": 10,
          "maxLength": 30
        },
        "eventType": {
          "type": "string",
          "example": "Activation",
          "description": "Event Name that you want to publish",
          "pattern": "^[A-Za-z0-9-_]+$",
          "minLength": 1,
          "maxLength": 255
        },
        "eventVersion": {
          "type": "string",
          "example": "1.0",
          "description": "version of the event , will be used to distinguish different versions by a consumer",
          "pattern": "^[\\d\\.]*$",
          "minLength": 1,
          "maxLength": 10
        },
        "headerReference": {
          "$ref": "#/definitions/HeaderReference"
        },
        "payload": {
          "$ref": "#/definitions/Payload"
        },
        "specifications": {
          "type": "array",
          "example": "{}",
          "description": "added for future reference so can ignore.",
          "items": {
            "$ref": "#/definitions/Specifications"
          }
        }
      },
      "example": {
        "auditInfo": "{}",
        "eventId": "Event-123",
        "headerReference": "{}",
        "payload": {},
        "eventVersion": "1.0",
        "eventTime": "2017-03-27T16:20:11.108Z",
        "event-type": "Activation",
        "eventProducerId": "Producer1",
        "specifications": "{}"
      },
      "description": "DEEP IO event."
    },
    "Payload": {
      "type": "object",
      "description": "Event specific information that can be modeled using APIv2 or inline elements.",
      "properties": {
        "Frontendorderid": {
          "description": "Frontend Order Id",
          "example": "9343846876",
          "type": "string"
        },
        "FrontendOrderDate": {
          "description": "Customer's Ordering Date in MMDDYYYY",
          "example": "08152019",
          "type": "string"
        },
        "SupplyChain": {
          "type": "object",
          "items": {
            "$ref": "#/definitions/SupplyChain"
          }
        },
        "CustomerProfile": {
          "type": "object",
          "items": {
            "$ref": "#/definitions/Customerprofile"
          }
        },
        "lineItem": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/LineItemInfoType"
          }
        },
        "OrderPricing": {
          "type": "object",
          "items": {
            "$ref": "#/definitions/Pricing"
          }
        }
      }
    },
    "SupplyChain": {
      "title": "Supply Chain",
      "description": "",
      "type": "object",
      "properties": {
        "OrderNumber": {
          "description": "Sales Order Number or Credit Memo Request number",
          "example": "7851813826",
          "type": "string"
        },
        "orderdate": {
          "description": "Sales Order creation Date Or Credit Memo Request Date in MMDDYYYY",
          "example": "16082019",
          "type": "string"
        },
        "DocumentType": {
          "description": "Sale Document Type ('ZWDR' forward Order 'ZWCR' - Credit Memo)",
          "example": "IN",
          "type": "string"
        },
        "Channelid": {
          "description": "Channelid",
          "example": "20'21",
          "type": "number"
        },
        "Orderreason": {
          "description": "Orderreason",
          "example": "NAO'AAL'L04",
          "type": "string"
        }
      }
    },
    "Customerprofile": {
      "title": "Customer Profile",
      "description": "",
      "type": "object",
      "properties": {
        "Customername": {
          "description": "Customer full Name",
          "example": "John",
          "type": "string"
        },
        "Customeremail": {
          "description": "Customer Mail id",
          "example": "John@gmail.com",
          "type": "string"
        },
        "Customerlanguage": {
          "description": "Customer Language",
          "example": "EN",
          "type": "string"
        },
        "ban": {
          "description": "Billing Account Number",
          "example": "818244063",
          "type": "string",
          "pattern": "\\d*"
        },
        "remitInfo": {
          "$ref": "#/definitions/RemitInfoType"
        },
        "billingInfo": {
          "$ref": "#/definitions/BillingInfoType"
        },
        "deliveryInfo": {
          "$ref": "#/definitions/DeliveryInfoType"
        }
      }
    },
    "Pricing": {
      "title": "Pricing",
      "description": "",
      "type": "object",
      "properties": {
        "NetAmount": {
          "description": "Net Amount excluding tax, discount, shipping",
          "example": "200.00",
          "type": "number",
          "format": "double"
        },
        "DiscountAmount": {
          "description": "DiscountAmount",
          "example": "9.50",
          "type": "number",
          "format": "double"
        },
        "ShippingTotal": {
          "description": "Shipping Cost Amount",
          "example": "0.00",
          "type": "number",
          "format": "double"
        },
        "SalesTax": {
          "description": "Sales Tax Amount",
          "example": "0.00",
          "type": "number",
          "format": "double"
        },
        "TotalAmount": {
          "description": "Total Order Amount inculde Tax, Discount, sales",
          "example": "209.50",
          "type": "number",
          "format": "double"
        }
      }
    },
    "LineItemInfoType": {
      "title": "Line Item",
      "description": "",
      "type": "object",
      "properties": {
        "lineItemNumber": {
          "description": "Line item number (SAP internal)",
          "example": "000010",
          "type": "string"
        },
        "sku": {
          "description": "SKU/Item #",
          "example": "610214659156",
          "type": "string"
        },
        "skuDesc": {
          "description": "SKU Description /Material Description",
          "example": "ASK LTE WIFI GATEWAY KIT",
          "type": "string"
        },
        "imei": {
          "description": "Device Serial#",
          "example": "345678901298765",
          "type": "string"
        },
        "sim": {
          "description": "SIM #",
          "example": "890345678901298765",
          "type": "string"
        },
        "Materialgroup": {
          "description": "Device Type of SKU",
          "example": "SIM",
          "type": "string"
        },
        "orderedQuantity": {
          "description": "Quantity Ordered",
          "example": "10",
          "type": "string"
        },
        "MSISDN": {
          "description": "MSIDDN",
          "example": "5038809505",
          "type": "string"
        },
        "ItemPricing": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Pricing"
          }
        }
      }
    },
    "RemitInfoType": {
      "title": "RemitInfo",
      "description": "",
      "type": "object",
      "properties": {
        "name": {
          "description": "Provide Name for Min 1, Max 4 people",
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "paymentAddressInfo": {
          "$ref": "#/definitions/AddressInfoType"
        }
      }
    },
    "BillingInfoType": {
      "title": "BillingInfo",
      "description": "",
      "type": "object",
      "properties": {
        "name": {
          "description": "Provide Name for Min 1, Max 4 people",
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "billingAddressInfo": {
          "$ref": "#/definitions/AddressInfoType"
        }
      }
    },
    "DeliveryInfoType": {
      "title": "DeliveryInfo",
      "description": "",
      "type": "object",
      "properties": {
        "name": {
          "description": "Provide Name for Min 1, Max 4 people",
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "deliveryAddressInfo": {
          "$ref": "#/definitions/AddressInfoType"
        }
      }
    },
    "address_AddressType": {
      "title": "address_Address",
      "description": "",
      "type": "object",
      "properties": {
        "addressLine1": {
          "description": "Address Line 1",
          "example": "SOLUTIONS LLC",
          "type": "string"
        },
        "addressLine2": {
          "description": "Address Line 2",
          "example": "2679723087",
          "type": "string"
        },
        "addressLine3": {
          "description": "Address Line 3",
          "example": "818244063",
          "type": "string"
        },
        "addressLine4": {
          "description": "Address Line 4",
          "example": "43 ABBEY RD",
          "type": "string"
        },
        "city": {
          "description": "Address City",
          "example": "Bothell",
          "type": "string"
        },
        "countryCode": {
          "description": "Address Country",
          "example": "1",
          "type": "string"
        },
        "county": {
          "description": "Address County",
          "example": "US",
          "type": "string"
        },
        "geoCode": {
          "description": "Geo Code",
          "type": "string"
        },
        "postalCode": {
          "description": "Address Postal",
          "example": "98021",
          "type": "string"
        },
        "zip4": {
          "description": "Zip+4 code",
          "example": "7841",
          "type": "string"
        },
        "province": {
          "description": "Address Province",
          "type": "string"
        },
        "stateCode": {
          "description": "Address State",
          "type": "string"
        },
        "status": {
          "description": "status of the Address",
          "type": "string"
        },
        "type": {
          "description": "type Of Address",
          "type": "string"
        }
      }
    },
    "AddressInfoType": {
      "title": "AddressInfo",
      "description": "",
      "allOf": [
        {
          "$ref": "#/definitions/address_AddressType"
        },
        {
          "type": "object"
        }
      ]
    }
  },
  "securityDefinitions": {
    "Oauth": {
      "type": "oauth2",
      "description": "JWT/Oauth2 instructions https://qwiki.internal.t-mobile.com/pages/viewpage.action?pageId=152594457",
      "tokenUrl": "https://api.enterprise.apigee.com/v1/organizations/{domain}/e/{environment}/caches/",
      "flow": "application",
      "scopes": {
        "post:SubmitEvent": "Submit event an event to DEEP IO."
      }
    }
  }
}