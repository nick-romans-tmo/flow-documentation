{
  "swagger": "2.0",
  "info": {
    "description": "Container Tracking Capability - This capability will address box receiving/dock receipt functionality within warehouse and update delivery status in T-Mobile on basis of tracking Number. It will also provide current status for Shipment and Container status on basis of tracking number/order number",
    "version": "1.0.0",
    "title": "ProjectOne APIs Container Tracking",
    "termsOfService": "https://www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsOfUse&print=true",
    "contact": {
      "name": "Created by - Mangesh Deshmukh Reviewed by - Madhav Muppa (Muppa.Madhav@T-Mobile.com)",
      "email": "Mangesh.Deshmukh2@T-Mobile.com"
    },
    "license": {
      "name": "T-Mobile",
      "url": "https://www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsOfUse&print=true"
    }
  },
  "host": "tmobileb-sb01.apigee.net",
  "basePath": "/supplychain/v2/containertracking",

  "x-servers": [
    {
      "url": "https://tmobileb-sb01.apigee.net/supplychain/v2/containertracking",
      "description": "Sandbox Server"
    },
    {
      "url": "https://api.t-mobile.com/supplychain/v2/containertracking",
      "description": "Live Server"
    }
  ],
  "tags": [
    {
      "name": "dockreceipts",
      "description": "Container Tracking Controller"
    },
    {
      "name": "shipmentstatus-trackingnumbers",
      "description": "Container Tracking Controller"
    },
    {
      "name": "shipmentstatus-ordernumbers",
      "description": "Container Tracking Controller"
    },
    {
      "name": "containerstatus-trackingnumbers",
      "description": "Container Tracking Controller"
    },
    {
      "name": "containerstatus-ordernumbers",
      "description": "Container Tracking Controller"
    }
  ],
  "schemes": ["https"],
  "paths": {
    "/dockreceipts": {"post": {
      "tags": ["dockreceipts"],
      "summary": "Container Tracking (Dock Receipt API) - This API will persist dock receipts information published by warehouse to T-Mobile",
      "description": "Container Tracking (Dock Receipt API) - A real time call will be triggered to dockreceipts API after scanning 6 sides of the box in warehouse scanning tunnel for a tracking numbers along with other required information as mentioned in the [Example] section below, which will be passed through this dockreceipts API",
      "operationId": "dockreceiptsUsingPOST",

      "consumes": [
        "application/json",
        "application/xml"
      ],
      "produces": [
        "application/json"
      ],
      "security": [{"Oauth": ["read:ContainerTracking"]}],
      "parameters": [
        {
          "name": "dockReceipt",
          "in": "body",
          "required": true,
          "description": "Request payload",
          "schema": {"$ref": "#/definitions/Dockreceipt"}
        }
      ],
      "responses": {
        "201": {
          "description": "Created",
          "headers": {
            "Location": {
              "description": "The Location header specifies the URI of a created resource, or redirects the API consumer to an alternate resource location.",
              "type": "string",
              "x-example": "URL"
            }
          },

          "schema": {"$ref": "#/definitions/Dockreceipt-response"}
        },
        "204": {
          "description": "No Content"
        },
        "400": {
          "description": "Bad Request",
          "schema": {"$ref": "#/definitions/Errors"}
        },
        "401": {
          "description": "Unauthorised Access",
          "schema": {"$ref": "#/definitions/Errors"},
          "examples": {"application/json": [{
            "code": "401",
            "userMessage": "Unauthorised Access",
            "systemMessage": "Unauthorised Access"
          }]
          }
        },
        "403": {
          "description": "Forbidden",
          "schema": {"$ref": "#/definitions/Errors"},
          "examples": {"application/json": [{
            "code": "403",
            "userMessage": "Access Denied",
            "systemMessage": "Forbidden"
          }]
          }
        },
        "404": {
          "description": "The resource not found",
          "schema": {"$ref": "#/definitions/Errors"}
        },
        "500": {
          "description": "Internal Server Error",
          "schema": {"$ref": "#/definitions/Errors"}
        },
        "503": {
          "description": "Service Unavailable",
          "schema": {"$ref": "#/definitions/Errors"}
        }
      }
    }},


    "/shipmentstatus-trackingnumbers/{trackingnumber}": {
      "get": {
        "tags": ["shipmentstatus-trackingnumbers"],
        "consumes": ["application/json"],
        "produces": ["application/json"],
        "security": [{"Oauth": ["read:ContainerTracking"]}],
        "summary": "Container Tracking (shipmentstatus-trackingnumbers API ) -  This API will provide shipment details on basis of tracking number",
        "description": "shipmentstatus-trackingnumbers API will provide shipment details from SOA on basis of tracking number",
        "operationId": "shipmentstatus-trackingnumbersUsingGET",
        "parameters": [
          {
            "in": "path",
            "name": "trackingnumber",
            "type": "string",
            "required": true,
            "description": "UPS tracking number, basis on which shipment details will be fetched from SOA is for domestic packages within the United States, will usually start with 1Z followed by a 6 character shipper number (numbers and letters), a 2 digit service level indicator, and finally 8 digits identifying the package, for a total of 18 characters",
            "maxLength": 35,
            "pattern": "[a-z0-9]{0,35}",
            "x-example": "1Z0209YY0262090221"
          },
          {"$ref": "#/parameters/If-MatchParam"},
          {"$ref": "#/parameters/If-None-MatchParam"}
        ],
        "responses": {
          "200": {
            "description": "Success",
            "headers": {
              "ETag": {
                "description": "The ETag header specifies the unique entity tag value for the returned resource.",
                "type": "string",
                "x-example": "Unique entity tag"
              },
              "Cache-Control": {
                "description": "no-cache",
                "type": "string",
                "x-example": "no-cache"
              }
            },

            "schema": {"$ref": "#/definitions/Shipmentstatustrackingnumbers-response"}

          },
          "204": {
            "description": "No Content"
          },
          "400": {
            "description": "Bad Request",
            "schema": {"$ref": "#/definitions/Errors"}
          },
          "401": {
            "description": "Unauthorised Access",
            "schema": {"$ref": "#/definitions/Errors"},
            "examples": {"application/json": [{
              "code": "401",
              "userMessage": "Unauthorised Access",
              "systemMessage": "Unauthorised Access"
            }]
            }
          },
          "403": {
            "description": "Forbidden",
            "schema": {"$ref": "#/definitions/Errors"},
            "examples": {"application/json": [{
              "code": "403",
              "userMessage": "Access Denied",
              "systemMessage": "Forbidden"
            }]
            }
          },
          "404": {
            "description": "The resource not found",
            "schema": {"$ref": "#/definitions/Errors"}
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {"$ref": "#/definitions/Errors"}
          },
          "503": {
            "description": "Service Unavailable",
            "schema": {"$ref": "#/definitions/Errors"}
          }
        }
      }
    },

    "/shipmentstatus-ordernumbers/{ordernumber}": {
      "get": {
        "tags": ["shipmentstatus-ordernumbers"],
        "consumes": ["application/json"],
        "produces": ["application/json"],
        "security": [{"Oauth": ["read:ContainerTracking"]}],
        "summary": "Container Tracking (shipmentstatus-ordernumbers API ) -  This API will provide shipment details on basis of order number",
        "description": "shipmentstatus-ordernumbers API will provide shipment details from SOA on basis of order number",
        "operationId": "shipmentstatus-ordernumbersUsingGET",
        "parameters": [
          {
            "in": "path",
            "name": "ordernumber",
            "type": "string",
            "required": true,
            "description": "Order/STO number is a numeric value, basis on which shipment details will be fetched from SOA",
            "maxLength": 35,
            "pattern": "[a-z0-9]{0,35}",
            "x-example": "6022109338"
          },
          {"$ref": "#/parameters/If-MatchParam"},
          {"$ref": "#/parameters/If-None-MatchParam"}
        ],
        "responses": {
          "200": {
            "description": "Success",
            "headers": {
              "ETag": {
                "description": "The ETag header specifies the unique entity tag value for the returned resource.",
                "type": "string",
                "x-example": "Unique entity tag"
              },
              "Cache-Control": {
                "description": "no-cache",
                "type": "string",
                "x-example": "no-cache"
              }
            },

            "schema": {"$ref": "#/definitions/Shipmentstatusordernumbers-response"}

          },
          "204": {
            "description": "No Content"
          },
          "400": {
            "description": "Bad Request",
            "schema": {"$ref": "#/definitions/Errors"}
          },
          "401": {
            "description": "Unauthorised Access",
            "schema": {"$ref": "#/definitions/Errors"},
            "examples": {"application/json": [{
              "code": "401",
              "userMessage": "Unauthorised Access",
              "systemMessage": "Unauthorised Access"
            }]
            }
          },
          "403": {
            "description": "Forbidden",
            "schema": {"$ref": "#/definitions/Errors"},
            "examples": {"application/json": [{
              "code": "403",
              "userMessage": "Access Denied",
              "systemMessage": "Forbidden"
            }]
            }
          },
          "404": {
            "description": "The resource not found",
            "schema": {"$ref": "#/definitions/Errors"}
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {"$ref": "#/definitions/Errors"}
          },
          "503": {
            "description": "Service Unavailable",
            "schema": {"$ref": "#/definitions/Errors"}
          }
        }
      }
    },

    "/containerstatus-trackingnumbers/{trackingnumber}": {
      "get": {
        "tags": ["containerstatus-trackingnumbers"],
        "consumes": ["application/json"],
        "produces": ["application/json"],
        "security": [{"Oauth": ["read:ContainerTracking"]}],
        "summary": "Container Tracking (containerstatus-trackingnumbers API ) -  This API will provide container tracking details on basis of tracking number",
        "description": "containerstatus-trackingnumbers API will provide container/box details from SOA on basis of tracking number",
        "operationId": "containerstatus-trackingnumbersUsingGET",
        "parameters": [
          {
            "in": "path",
            "name": "trackingnumber",
            "type": "string",
            "required": true,
            "description": "UPS tracking number, basis on which shipment details will be fetched from SOA is for domestic packages within the United States, will usually start with 1Z followed by a 6 character shipper number (numbers and letters), a 2 digit service level indicator, and finally 8 digits identifying the package, for a total of 18 characters",
            "maxLength": 35,
            "pattern": "[a-z0-9]{0,35}",
            "x-example": "1Z0209YY0262090221"
          },
          {"$ref": "#/parameters/If-MatchParam"},
          {"$ref": "#/parameters/If-None-MatchParam"}
        ],
        "responses": {
          "200": {
            "description": "Success",
            "headers": {
              "ETag": {
                "description": "The ETag header specifies the unique entity tag value for the returned resource.",
                "type": "string",
                "x-example": "Unique entity tag"
              },
              "Cache-Control": {
                "description": "no-cache",
                "type": "string",
                "x-example": "no-cache"
              }
            },

            "schema": {"$ref": "#/definitions/Containerstatustrackingnumbers-response"}


          },
          "204": {
            "description": "No Content"
          },
          "400": {
            "description": "Bad Request",
            "schema": {"$ref": "#/definitions/Errors"}
          },
          "401": {
            "description": "Unauthorised Access",
            "schema": {"$ref": "#/definitions/Errors"},
            "examples": {"application/json": [{
              "code": "401",
              "userMessage": "Unauthorised Access",
              "systemMessage": "Unauthorised Access"
            }]
            }
          },
          "403": {
            "description": "Forbidden",
            "schema": {"$ref": "#/definitions/Errors"},
            "examples": {"application/json": [{
              "code": "403",
              "userMessage": "Access Denied",
              "systemMessage": "Forbidden"
            }]
            }
          },
          "404": {
            "description": "The resource not found",
            "schema": {"$ref": "#/definitions/Errors"}
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {"$ref": "#/definitions/Errors"}
          },
          "503": {
            "description": "Service Unavailable",
            "schema": {"$ref": "#/definitions/Errors"}
          }
        }
      }
    },

    "/containerstatus-ordernumbers/{ordernumber}": {
      "get": {
        "tags": ["containerstatus-ordernumbers"],
        "consumes": ["application/json"],
        "produces": ["application/json"],
        "security": [{"Oauth": ["read:ContainerTracking"]}],
        "summary": "Container Tracking (containerstatus-ordernumbers API ) -  This API will provide container tracking details on basis of order number",
        "description": "containerstatus-ordernumbers API will provide container/box details from SOA on basis of order number",
        "operationId": "containerstatus-ordernumbersUsingGET",
        "parameters": [
          {
            "in": "path",
            "name": "ordernumber",
            "type": "string",
            "required": true,
            "description": "Order/STO number is a numeric value, basis on which shipment details will be fetched from SOA",
            "maxLength": 35,
            "pattern": "[a-z0-9]{0,35}",
            "x-example": "6022109338"
          },
          {"$ref": "#/parameters/If-MatchParam"},
          {"$ref": "#/parameters/If-None-MatchParam"}
        ],
        "responses": {
          "200": {
            "description": "Success",
            "headers": {
              "ETag": {
                "description": "The ETag header specifies the unique entity tag value for the returned resource.",
                "type": "string",
                "x-example": "Unique entity tag"
              },
              "Cache-Control": {
                "description": "no-cache",
                "type": "string",
                "x-example": "no-cache"
              }
            },

            "schema": {"$ref": "#/definitions/Containerstatusordernumbers-response"}

          },
          "204": {
            "description": "No Content"
          },
          "400": {
            "description": "Bad Request",
            "schema": {"$ref": "#/definitions/Errors"}
          },
          "401": {
            "description": "Unauthorised Access",
            "schema": {"$ref": "#/definitions/Errors"},
            "examples": {"application/json": [{
              "code": "401",
              "userMessage": "Unauthorised Access",
              "systemMessage": "Unauthorised Access"
            }]
            }
          },
          "403": {
            "description": "Forbidden",
            "schema": {"$ref": "#/definitions/Errors"},
            "examples": {"application/json": [{
              "code": "403",
              "userMessage": "Access Denied",
              "systemMessage": "Forbidden"
            }]
            }
          },
          "404": {
            "description": "The resource not found",
            "schema": {"$ref": "#/definitions/Errors"}
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {"$ref": "#/definitions/Errors"}
          },
          "503": {
            "description": "Service Unavailable",
            "schema": {"$ref": "#/definitions/Errors"}
          }
        }
      }
    }

  },
  "definitions": {
    "Dockreceipt": {
      "type": "object",
      "properties": {
        "carrierName": {
          "type": "string",
          "maxLength": 50,
          "description": "Name of Carrier, which delivered boxes to one return center",
          "x-required": false,
          "pattern": "[a-z]{0,50}"
        },
        "damageCode": {
          "type": "string",
          "maxLength": 15,
          "description": "Damage code is required incase boxes are delivered in damaged condition",
          "x-required": false,
          "pattern": "[a-z]{0,15}"
        },
        "documentNumber": {
          "type": "string",
          "maxLength": 35,
          "description": "Document number is a unique numeric value against which stock is transferred",
          "x-required": false,
          "pattern": "[a-z0-9]{0,35}"
        },
        "gaylordNumber": {
          "type": "string",
          "maxLength": 15,
          "description": "Gaylord number is defined by UPS to track big shipment",
          "x-required": false,
          "pattern": "[a-z0-9]{0,15}"
        },
        "packageId": {
          "type": "string",
          "maxLength": 15,
          "description": "Package Id is a unique number to identify packages",
          "x-required": false,
          "pattern": "[a-z0-9]{0,15}"
        },
        "plant": {
          "type": "string",
          "maxLength": 10,
          "description": "Plant indicates one return center",
          "x-required": true,
          "pattern": "[a-z0-9]{0,10}"
        },
        "reportDate": {
          "type": "string",
          "format": "date-time",
          "description": "Reported date is date-time when boxes were delivered to one return centerr",
          "maxLength": 20,
          "x-required": false
        },
        "scanDate": {
          "type": "string",
          "format": "date-time",
          "description": "Scan Date is when boxes were scanned",
          "maxLength": 20,
          "x-required": true
        },
        "scanimageUrl": {
          "type": "string",
          "maxLength": 100,
          "x-required": false,
          "description": "Image URL will have link for respective images caputured in scanning tunnel"
        },
        "scanStatus": {
          "type": "string",
          "maxLength": 40,
          "x-required": false,
          "pattern": "[a-z0-9]{0,40}",
          "description": "A real-time box scanning status will be captured from scanning tunnel"
        },
        "scanType": {
          "type": "string",
          "maxLength": 10,
          "x-required": false,
          "description": "Scan type will indicate how scanning was done Auto (through scanning tunnel) or Manual(using scanner wand)",
          "enum": [
            "Auto",
            "Manual"
          ]
        },
        "scanvideoUrl": {
          "type": "string",
          "maxLength": 100,
          "x-required": false,
          "description": "Video  URL will have link for respective videos caputured in scanning tunnel"
        },
        "trackingNumber": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "dcscanTrackingNo": {
                "type": "string",
                "maxLength": 215,
                "x-required": true,
                "description": "UPS tracking number, will usually start with 1Z followed by a 6 character shipper number (numbers and letters), a 2 digit service level indicator, and finally 8 digits identifying the package, for a total of 18 characters",
                "pattern": "[a-z0-9]{0,215}"
              }
            }
          }
        },
        "trailerNumber": {
          "type": "string",
          "maxLength": 20,
          "description": "This is UPS trailer registration number",
          "pattern": "[a-z0-9]{0,20}"
        },
        "transactionrefNo": {
          "type": "string",
          "maxLength": 30,
          "x-required": true,
          "description": "A reference number is a unique identifier assigned to each transaction, It helps to distinctively identify transactions in records and monitor transactions"
        }
      },
      "example": {
        "carrierName": "UPS",
        "damageCode": "DC00000001",
        "documentNumber": "Null",
        "gaylordNumber": "UPS001",
        "packageId": "UPS01",
        "plant": "DC45",
        "reportDate": "2017-11-05 16:00:00",
        "scanDate": "2017-11-05 16:00:00",
        "scanimageUrl": "URL Link for Image",
        "scanStatus": "Success",
        "scanType": "Auto",
        "scanvideoUrl": "URL Link for Video",
        "trackingNumber": "[1Z0209YY0262090221,1Z0209YY0262090222]",
        "trailerNumber": "UPS011W",
        "transactionrefNo": "111111111111111111111111111111"
      }
    },

    "Dockreceipt-response": {
      "type": "object",
      "properties": {
        "interfaceName": {
          "type": "string",
          "maxLength": 30,
          "description": "This field will show interfaceName",
          "example": "API Name - dockReceipts"
        },
        "sessionId": {
          "type": "string",
          "maxLength": 100,
          "description": "This field will show session-id"
        },
        "status": {
          "type": "string",
          "maxLength": 25,
          "description": "This field will show API status",
          "example": "Success"
        },
        "statusCode": {
          "type": "integer",
          "format": "int32",
          "minimum": 1,
          "maximum": 3,
          "description": "This field will show Status Code",
          "example": "201"
        },
        "statusText": {
          "type": "string",
          "maxLength": 100,
          "description": "This field will show Status text specific to this transaction",
          "example": "Dock receipt record created successfully"
        },
        "transactionReferenceNo": {
          "type": "string",
          "maxLength": 30,
          "description": "A reference number is a unique identifier assigned to each transaction, It helps to distinctively identify transactions in records and monitor transactions",
          "example": "111111111111111111111111111111"
        }
      }
    },

    "Shipmentstatustrackingnumbers-response": {
      "type": "object",
      "properties": {
        "interfaceName": {
          "type": "string",
          "maxLength": 30,
          "description": "This field will show API name",
          "example": "API Name - shipmentstatus-trackingnumbers/"
        },
        "sessionId": {
          "type": "string",
          "maxLength": 100,
          "description": "This field will show session-id"
        },
        "status": {
          "type": "string",
          "maxLength": 25,
          "description": "This field will show API status",
          "example": "Success"
        },
        "statusCode": {
          "type": "integer",
          "format": "int32",
          "minimum": 1,
          "maximum": 3,
          "description": "This field will show Status Code",
          "example": "200"
        },
        "statusText": {
          "type": "string",
          "maxLength": 100,
          "description": "This field will show Status text specific to this transaction",
          "example": "Shipment status details for tracking number - 1Z0209YY0262090221"
        },
        "transactionReferenceNo": {
          "type": "string",
          "maxLength": 30,
          "description": "A reference number is a unique identifier assigned to each transaction, It helps to distinctively identify transactions in records and monitor transactions",
          "example": "111111111111111111111111111111"
        },
        "deviceLineItem": {
          "type": "object",
          "properties": {
            "trackingNumber": {
              "type": "string",
              "maxLength": 35,
              "example": "1Z0209YY0262090221"
            },
            "carriertype": {
              "type": "string",
              "maxLength": 25,
              "example": "UPS"
            },
            "carriername": {
              "type": "string",
              "maxLength": 50,
              "example": "UPS"
            },
            "scantype": {
              "type": "string",
              "maxLength": 30,
              "enum": [
                "Auto",
                "Manual"
              ],
              "example": "Auto"
            },
            "scandate": {
              "type": "string",
              "format": "date-time",
              "maxLength": 20,
              "example": "2018-02-12T16:00:00.000Z"
            },
            "ordernumber": {
              "type": "string",
              "maxLength": 35,
              "example": "5123899455"
            },
            "ordercreateddate": {
              "type": "string",
              "format": "date-time",
              "maxLength": 20,
              "example": "2018-02-12T16:00:00.000Z"
            },
            "ordershippeddate": {
              "type": "string",
              "format": "date-time",
              "maxLength": 20,
              "example": "2018-02-12T16:00:00.000Z"
            }
          }
        }
      }
    },

    "Shipmentstatusordernumbers-response": {
      "type": "object",
      "properties": {
        "interfaceName": {
          "type": "string",
          "maxLength": 30,
          "description": "This field will show API name",
          "example": "API Name - shipmentstatus-ordernumbers/"
        },
        "sessionId": {
          "type": "string",
          "maxLength": 100,
          "description": "This field will show session-id"
        },
        "status": {
          "type": "string",
          "maxLength": 25,
          "description": "This field will show API status",
          "example": "Success"
        },
        "statusCode": {
          "type": "integer",
          "format": "int32",
          "minimum": 1,
          "maximum": 3,
          "description": "This field will show Status Code",
          "example": "200"
        },
        "statusText": {
          "type": "string",
          "maxLength": 100,
          "description": "This field will show Status text specific to this transaction",
          "example": "Shipment status details for order number - 6022109338"
        },
        "transactionReferenceNo": {
          "type": "string",
          "maxLength": 30,
          "description": "A reference number is a unique identifier assigned to each transaction, It helps to distinctively identify transactions in records and monitor transactions",
          "example": "111111111111111111111111111111"
        },
        "deviceLineItem": {
          "type": "object",
          "properties": {
            "trackingNumber": {
              "type": "string",
              "maxLength": 35,
              "example": "1Z0209YY0262090221"
            },
            "carriertype": {
              "type": "string",
              "maxLength": 25,
              "example": "UPS"
            },
            "carriername": {
              "type": "string",
              "maxLength": 50,
              "example": "UPS"
            },
            "scantype": {
              "type": "string",
              "maxLength": 30,
              "enum": [
                "Auto",
                "Manual"
              ],
              "example": "Auto"
            },
            "scandate": {
              "type": "string",
              "format": "date-time",
              "maxLength": 20,
              "example": "2018-02-12T16:00:00.000Z"
            },
            "ordernumber": {
              "type": "string",
              "maxLength": 35,
              "example": "6022109338"
            },
            "ordercreateddate": {
              "type": "string",
              "format": "date-time",
              "maxLength": 20,
              "example": "2018-02-12T16:00:00.000Z"
            },
            "ordershippeddate": {
              "type": "string",
              "format": "date-time",
              "maxLength": 20,
              "example": "2018-02-12T16:00:00.000Z"
            }
          }
        }
      }
    },

    "Containerstatustrackingnumbers-response": {
      "type": "object",
      "properties": {
        "interfaceName": {
          "type": "string",
          "maxLength": 30,
          "description": "This field will show API name",
          "example": "API Name - containerstatus-trackingnumbers"
        },
        "sessionId": {
          "type": "string",
          "maxLength": 100,
          "description": "This field will show session-id"
        },
        "status": {
          "type": "string",
          "maxLength": 25,
          "description": "This field will show API status",
          "example": "Success"
        },
        "statusCode": {
          "type": "integer",
          "format": "int32",
          "minimum": 1,
          "maximum": 3,
          "description": "This field will show Status Code",
          "example": "200"
        },
        "statusText": {
          "type": "string",
          "maxLength": 100,
          "description": "This field will show Status text specific to this transaction",
          "example": "Container/Box status details for tracking number - 1Z0209YY0262090221"
        },
        "transactionReferenceNo": {
          "type": "string",
          "maxLength": 30,
          "description": "A reference number is a unique identifier assigned to each transaction, It helps to distinctively identify transactions in records and monitor transactions",
          "example": "111111111111111111111111111111"
        },
        "deviceLineItem": {
          "type": "object",
          "properties": {
            "trackingNumber": {
              "type": "string",
              "maxLength": 35,
              "example": "1Z0209YY0262090221"
            },
            "carriertype": {
              "type": "string",
              "maxLength": 25,
              "example": "UPS"
            },
            "carriername": {
              "type": "string",
              "maxLength": 50,
              "example": "UPS"
            },
            "scantype": {
              "type": "string",
              "maxLength": 30,
              "enum": [
                "Auto",
                "Manual"
              ],
              "example": "Auto"
            },
            "scandate": {
              "type": "string",
              "format": "date-time",
              "maxLength": 20,
              "example": "2018-02-12T16:00:00.000Z"
            },
            "ordernumber": {
              "type": "string",
              "maxLength": 35,
              "example": "5123899455"
            },
            "ordercreateddate": {
              "type": "string",
              "format": "date-time",
              "maxLength": 20,
              "example": "2018-02-12T16:00:00.000Z"
            },
            "ordershippeddate": {
              "type": "string",
              "format": "date-time",
              "maxLength": 20,
              "example": "2018-02-12T16:00:00.000Z"
            }
          }
        }
      }
    },

    "Containerstatusordernumbers-response": {
      "type": "object",
      "properties": {
        "interfaceName": {
          "type": "string",
          "maxLength": 30,
          "description": "This field will show API name",
          "example": "API Name - containerstatus-ordernumbers"
        },
        "sessionId": {
          "type": "string",
          "maxLength": 100,
          "description": "This field will show session-id"
        },
        "status": {
          "type": "string",
          "maxLength": 25,
          "description": "This field will show API status",
          "example": "Success"
        },
        "statusCode": {
          "type": "integer",
          "format": "int32",
          "minimum": 1,
          "maximum": 3,
          "description": "This field will show Status Code",
          "example": "200"
        },
        "statusText": {
          "type": "string",
          "maxLength": 100,
          "description": "This field will show Status text specific to this transaction",
          "example": "Container/Box status details for order number - 6022109338"
        },
        "transactionReferenceNo": {
          "type": "string",
          "maxLength": 30,
          "description": "A reference number is a unique identifier assigned to each transaction, It helps to distinctively identify transactions in records and monitor transactions",
          "example": "111111111111111111111111111111"
        },
        "deviceLineItem": {
          "type": "object",
          "properties": {
            "trackingNumber": {
              "type": "string",
              "maxLength": 35,
              "example": "1Z0209YY0262090221"
            },
            "carriertype": {
              "type": "string",
              "maxLength": 25,
              "example": "UPS"
            },
            "carriername": {
              "type": "string",
              "maxLength": 50,
              "example": "UPS"
            },
            "scantype": {
              "type": "string",
              "maxLength": 30,
              "enum": [
                "Auto",
                "Manual"
              ],
              "example": "Auto"
            },
            "scandate": {
              "type": "string",
              "format": "date-time",
              "maxLength": 20,
              "example": "2018-02-12T16:00:00.000Z"
            },
            "ordernumber": {
              "type": "string",
              "maxLength": 35,
              "example": "6022109338"
            },
            "ordercreateddate": {
              "type": "string",
              "format": "date-time",
              "maxLength": 20,
              "example": "2018-02-12T16:00:00.000Z"
            },
            "ordershippeddate": {
              "type": "string",
              "format": "date-time",
              "maxLength": 20,
              "example": "2018-02-12T16:00:00.000Z"
            }
          }
        }
      }
    },

    "Errors": {
      "description": "a collection of errors",
      "type": "array",
      "items": {"$ref": "#/definitions/Responseerror"}
    },

    "Responseerror": {
      "description": "Used to pass error information in a response.",
      "type": "object",
      "properties": {
        "code": {
          "description": "Used to pass error codes",
          "type": "string"
        },
        "userMessage": {
          "description": "Use to pass human friendly information to the user.",
          "type": "string"
        },
        "systemMessage": {
          "description": "Used to pass system information.",
          "type": "string"
        }
      }
    }
  },
  "parameters": {

    "If-MatchParam": {
      "name": "If-Match",
      "in": "header",
      "description": "The If-Match header specifies an ETag-related condition to be placed on the execution of a request.  The request will only be executed if the ETag of the requested resource matches one of those specified in the If-Match header.  Because the behavior is complex, the details for usage are provided at http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html .",
      "required": false,
      "type": "string",
      "x-example": "xxyyzz"
    },

    "If-None-MatchParam": {
      "name": "If-None-Match",
      "in": "header",
      "description": "The If-None-Match header specifies an ETag-related condition to be placed on the execution of a request.  The request will NOT be executed if the ETag of the requested resource matches one of those specified in the If-None-Match header.  Because the behavior is complex, the details for usage are provided at http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html",
      "required": false,
      "type": "string",
      "x-example": "xxyyzz"
    }

  },
  "securityDefinitions": {"Oauth": {
    "type": "oauth2",
    "description": "When you sign up for an account, you are given your first API key.\nTo do so please follow this link: https://www.t-mobile.com/site/signup/\nAlso you can generate additional API keys, and delete API keys (as you may\nneed to rotate your keys in the future).\n",
    "tokenUrl": "https://tmobileb-sb01.apigee.net/v1/oauth2/accesstoken?grant_type=client_credentials",
    "flow": "application",
    "scopes": {"read:ContainerTracking": "Container tracking information from SOA for the given tracking number/orderId."}
  }}
}