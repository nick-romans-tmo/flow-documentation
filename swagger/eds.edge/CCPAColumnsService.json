{
    "swagger": "2.0",
    "info": {
        "title": "CCPAColumnsService",
        "description": "This service support the use of the CCPA standard as defined in Collibra repository",
        "version": "1.0.0",
        "contact": {
            "name": "Hari Manan",
            "email": "Hari.Manan2@T-Mobile.com"
        },
        "x-createdBy": {
            "dateCreated": "Tue Nov 19 23:41:26 2019",
            "createdBy": "cburnet17",
            "application": "Recite",
            "appVersion": "1.2.1.3071",
            "documentId": "0cfac480-5750-4fcb-b11c-be2e4bbf8ec5",
            "status": "Conceptual - Initial",
            "businessCapabilityModel": "5.0",
            "classification": "13.4.1 GRC Delivery Enablement",
            "profile": "OpenWeb or B2B API",
            "serviceLayer": "Resource - BusinessIntelligence"
        }
    },
    "x-servers": [
        {
            "url": "http://10.77.4.13:8081/",
            "description": "Test Server 1"
        }
    ],
    "tags": [
        {
            "name": "QueryCollection",
            "description": "The QueryCollection design pattern as defined by T-Mobile Digital Architecture."
        },
        {
            "name": "queryColumnsCollection",
            "description": ""
        }
    ],
    "host": "dev01.api.t-mobile.com",
    "basePath": "/edge/v1/standards",
    "schemes": [
        "https"
    ],
    "definitions": {
        "BusinessTerms": {
            "description": "An array of the business terms related to this column.",
            "type": "object",
            "properties": {
                "businessTerm": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 256,
                    "pattern": "^[a-zA-Z0-9\\-,\\.() ]*$",
                    "description": "Name of Collibra business term",
                    "example": "Individual Identification"
                },
                "ccpaDataCategoryName": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 256,
                    "pattern": "^[a-zA-Z0-9\\-,\\.() ]*$",
                    "description": "Name of CCPA data category",
                    "example": "Personal Identifiers"
                }
            }
        },
        "Columns": {
            "description": "Array of database colmns affected by CCPA standard in selected system",
            "type": "object",
            "properties": {
                "columnName": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 256,
                    "pattern": "^[\\S ]*$",
                    "description": "Name of the database column",
                    "example": "Kyobi.Evolution.dbo > ParticipantPrize > ParticipantId"
                },
                "tableName": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 256,
                    "pattern": "^[\\S ]*$",
                    "description": "Name of the database table",
                    "example": "Kyobi.Evolution.dbo > ParticipantPrize"
                },
                "schemaName": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 256,
                    "pattern": "^[a-zA-Z0-9\\-,\\.() ]*$",
                    "description": "Name of the database schema"
                },
                "databaseName": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 256,
                    "pattern": "^[a-zA-Z0-9\\-,\\.() ]*$",
                    "description": "Name of database",
                    "example": "Kyobi.Evolution"
                },
                "dataType": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 50,
                    "pattern": "^[a-zA-Z0-9_\\-]*$",
                    "description": "Data type of the column storing the data"
                },
                "businessTerms": {
                    "type": "array",
                    "description": "An array of the business terms related to this column.",
                    "items": {
                        "$ref": "#/definitions/BusinessTerms"
                    }
                },
                "controlName": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 256,
                    "pattern": "^[a-zA-Z0-9\\-,\\.() ]*$",
                    "description": "Name of the applicable control",
                    "example": "Not exempt from Deletion"
                }
            }
        },
        "ColumnsCollection": {
            "description": "A collection of CCPA Standards",
            "type": "object",
            "properties": {
                "size": {
                    "type": "integer",
                    "format": "int32",
                    "minimum": 0,
                    "pattern": "^[\\-+]?[0-9]+$",
                    "description": "Size of array",
                    "example": 1
                },
                "applicationId": {
                    "type": "integer",
                    "format": "int32",
                    "pattern": "^[\\-+]?[0-9]+$",
                    "description": "Asset knowledge management identifier (AKM ID)",
                    "example": 102532
                },
                "standardId": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 50,
                    "pattern": "^[a-zA-Z\\d\\-]*$",
                    "description": "Unique Collibra asset identifier for the standard",
                    "example": "d42bb11e-1184-4360-bcc7-202a54863a1a"
                },
                "standardName": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 256,
                    "pattern": "^[a-zA-Z0-9\\-,\\.() ]*$",
                    "description": "CCPA standard name",
                    "example": "Data Subject Delete Standard"
                },
                "systemName": {
                    "type": "string",
                    "minLength": 1,
                    "maxLength": 256,
                    "pattern": "^[a-zA-Z0-9\\-,\\.() ]*$",
                    "description": "System name",
                    "example": "T-Mobile Tuesdays"
                },
                "columns": {
                    "type": "array",
                    "description": "Array of database colmns affected by CCPA standard in selected system",
                    "items": {
                        "$ref": "#/definitions/Columns"
                    }
                }
            }
        },
        "Error": {
            "description": "As defined in http://api-standards.apps.px-npe01.cf.t-mobile.com/http/status-codes/error-response-format/",
            "type": "object",
            "required": [
                "code",
                "userMessage"
            ],
            "properties": {
                "code": {
                    "type": "string",
                    "pattern": "^[\\S ]+$",
                    "description": "A succinct, domain-specific, human-readable text string to identify the type of error for the given status code",
                    "example": "ProductNotFound"
                },
                "userMessage": {
                    "type": "string",
                    "pattern": "^[\\S ]+$",
                    "description": "A human-readable message describing the error.",
                    "example": "Failed to find product."
                },
                "systemMessage": {
                    "type": "string",
                    "description": "Text that provides a more detailed technical explanation of the error",
                    "example": "PRODUCT_NOT_FOUND"
                },
                "detailLink": {
                    "type": "string",
                    "description": "link to custom information providing greater detail on error or errors",
                    "example": "http://aaa.bbb.ccc/"
                }
            }
        }
    },
    "paths": {
        "/columns": {
            "get": {
                "tags": [
                    "queryColumnsCollection",
                    "QueryCollection"
                ],
                "x-api-pattern": "QueryCollection",
                "summary": "A get for the columns resource using Digital Archecture's QueryCollection pattern.",
                "description": "The base resource path with a GET method and query parameters defines a search for an array of columns objects that match the given criteria, or an empty array if none are found.",
                "operationId": "queryColumnsCollection",
                "consumes": [
                    "application/json",
                    "application/xml"
                ],
                "produces": [
                    "application/json",
                    "application/xml"
                ],
                "parameters": [
                    {
                        "name": "application-id",
                        "in": "query",
                        "description": "Asset knowledge management identifier (AKM ID)",
                        "required": true,
                        "type": "integer",
                        "format": "int32",
                        "pattern": "^[\\-+]?[0-9]+$",
                        "x-example": 101822
                    },
                    {
                        "name": "standard-id",
                        "in": "query",
                        "description": "Unique Collibra asset identifier for the standard",
                        "required": true,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 50,
                        "pattern": "^[a-zA-Z\\d\\-]*$",
                        "x-example": "64b25e65-9cae-41c7-b917-120c9c832940"
                    },
                    {
                        "name": "Accept",
                        "in": "header",
                        "description": "Preferred content type _ default to application/json",
                        "x-example": "application/json",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[ \\S]+$"
                    },
                    {
                        "name": "Accept-Encoding",
                        "in": "header",
                        "description": "List of acceptable encodings. See HTTP compression.",
                        "x-example": "gzip,deflate",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S ]*$"
                    },
                    {
                        "name": "Accept-Language",
                        "in": "header",
                        "description": "List of acceptable human languages for response",
                        "x-example": "en-us,en;q=0.5",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S ]*$"
                    },
                    {
                        "name": "Authorization",
                        "in": "header",
                        "description": "The HTTP Authorization request header contains the credentials to authenticate a user agent with a server.",
                        "x-example": "Bearer mF_9.B5f-4.1JqM",
                        "required": true,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S ]+$"
                    },
                    {
                        "name": "Date",
                        "in": "header",
                        "description": "The Date general HTTP header contains the date and time at which the message was originated.",
                        "x-example": "Mon, 05 Mar 2018 16:38:08 GMT",
                        "required": false,
                        "type": "string",
                        "format": "date-time",
                        "pattern": "^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), [0-3]{1}[0-9]{1} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [1-3][0-9]{3} [0-2][0-9]:[0-6][0-9]:[0-6][0-9] GMT$"
                    },
                    {
                        "name": "activity-id",
                        "in": "header",
                        "description": "Unique id alphanumeric value generated and sent by the partner to identify every the service request uniquely. Sample: XyzA:D1s5s1i-X6AFRvfiPBfs-1234  Pass as is to downstream calls.",
                        "x-example": "c34e7acd-384b-4c22-8b02-ba3963682508",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "interaction-id",
                        "in": "header",
                        "description": "Alphanumeric value represent a common transaction id across all calls made from UI, while completing all business activity needs of a particular customer.",
                        "x-example": "cc688d54-50d1-49d4-8c5d-95459d1172e8",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "service-transaction-id",
                        "in": "header",
                        "description": "Request ID echoed back by server",
                        "x-example": "23409209723",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "session-id",
                        "in": "header",
                        "description": "A value populated by the sender used to track the transactions that occur during a session, a long-lasting interaction that ismaintained by the sender.",
                        "x-example": "350b91ec-4a64-4b10-a3f3-a78c8db3924a",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "timestamp",
                        "in": "header",
                        "description": "ms since 1/1/70 adjusted to GMT",
                        "x-example": "1524599536",
                        "required": false,
                        "type": "integer",
                        "format": "int64",
                        "minimum": 0,
                        "maximum": 999999999999,
                        "pattern": "^[\\d]*$"
                    },
                    {
                        "name": "workflow-id",
                        "in": "header",
                        "description": "Business Use Case",
                        "x-example": "ACTIVATION",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "x-auth-originator",
                        "in": "header",
                        "description": "API chain initiating callers access token",
                        "x-example": "c34e7acd-384b-4c22-8b02-ba3963682508",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S]*$"
                    },
                    {
                        "name": "x-authorization",
                        "in": "header",
                        "description": "Contains Proof of Possession token generated by API caller",
                        "x-example": "c34e7acd-384b-4c22-8b02-ba3963682508",
                        "required": false,
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 256,
                        "pattern": "^[\\S ]*$"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/ColumnsCollection"
                        },
                        "examples": {
                            "application/json": {
                                "size": 1,
                                "applicationId": "102532",
                                "standardId": "d42bb11e-1184-4360-bcc7-202a54863a1a",
                                "standardName": "Data Subject Delete Standard",
                                "systemName": "T-Mobile Tuesdays",
                                "columns": [
                                    {
                                        "databaseName": "Kyobi.Evolution.dbo",
                                        "dataType": "",
                                        "controlName": "Not-exempt from Deletion",
                                        "businessTerms": [
                                            {
                                                "ccpaDataCategoryName": "Personal Identifiers",
                                                "businessTerm": "Individual Identification"
                                            }
                                        ],
                                        "columnName": "Kyobi.Evolution.dbo > ParticipantPrize > ParticipantId",
                                        "tableName": "Kyobi.Evolution.dbo > ParticipantPrize"
                                    }
                                ]
                            }
                        },
                        "headers": {
                            "Cache-Control": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 256,
                                "pattern": "^[\\S]*$",
                                "description": "Cache policy supported for the resource by the server",
                                "x-example": "no-cache"
                            },
                            "Content-Encoding": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 15,
                                "pattern": "^[\\S]*$",
                                "description": "The type of encoding used on the data",
                                "x-example": "gzip,deflate"
                            },
                            "Content-Length": {
                                "type": "integer",
                                "format": "int64",
                                "minimum": 0,
                                "pattern": "^[\\d]*$",
                                "description": "Length of the response body in octets; not supported if Transfer-Endoding = chunked",
                                "x-example": 9112300000003
                            },
                            "Content-Type": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 256,
                                "pattern": "^[ \\S]+$",
                                "description": "The MIME type of this content",
                                "x-example": "text/xml"
                            },
                            "Date": {
                                "type": "string",
                                "format": "date-time",
                                "pattern": "^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), [0-3]{1}[0-9]{1} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [1-3][0-9]{3} [0-2][0-9]:[0-6][0-9]:[0-6][0-9] GMT$",
                                "description": "The Date general HTTP header contains the date and time at which the message was originated.",
                                "x-example": "Mon, 05 Mar 2018 16:38:08 GMT"
                            },
                            "ETag": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 256,
                                "pattern": "^[\\S]*$",
                                "description": "The ETag header specifies the unique entity tag value for the returned resource.",
                                "x-example": "*"
                            },
                            "Transfer-Encoding": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 15,
                                "pattern": "^[\\S]*$",
                                "description": "The form of encoding used to safely transfer the entity to the user. Currently defined methods are: chunked, compress, deflate, gzip, identity.",
                                "x-example": "chunked"
                            },
                            "service-transaction-id": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 256,
                                "pattern": "^[\\S]*$",
                                "description": "Request ID echoed back by server",
                                "x-example": "23409209723"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "400",
                                "userMessage": "Bad Request",
                                "systemMessage": "Bad Request",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "401": {
                        "description": "Unauthorized",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "401",
                                "userMessage": "Unauthorized",
                                "systemMessage": "Unauthorized",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "404": {
                        "description": "Resource not found",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "404",
                                "userMessage": "Resource not found",
                                "systemMessage": "Resource not found",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "405": {
                        "description": "Method Not Allowed",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "405",
                                "userMessage": "Method Not Allowed",
                                "systemMessage": "Method Not Allowed",
                                "detailLink": "http://www.tmus.com"
                            }
                        },
                        "headers": {
                            "Allow": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 256,
                                "pattern": "^[\\S]*$",
                                "description": "list of supported methods for URI",
                                "x-example": "GET"
                            },
                            "service-transaction-id": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 256,
                                "pattern": "^[\\S]*$",
                                "description": "Request ID echoed back by server",
                                "x-example": "23409209723"
                            }
                        }
                    },
                    "406": {
                        "description": "Mismatching data format",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "406",
                                "userMessage": "Mismatching data format",
                                "systemMessage": "Mismatching data format",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "500": {
                        "description": "System Error",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "500",
                                "userMessage": "System Error",
                                "systemMessage": "System Error",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    },
                    "503": {
                        "description": "Service unavailable",
                        "schema": {
                            "$ref": "#/definitions/Error"
                        },
                        "examples": {
                            "application/json": {
                                "code": "503",
                                "userMessage": "Service unavailable",
                                "systemMessage": "Service unavailable",
                                "detailLink": "http://www.tmus.com"
                            }
                        }
                    }
                },
                "security": [
                    {
                        "JWT": []
                    }
                ]
            }
        }
    },
    "securityDefinitions": {
        "JWT": {
            "type": "apiKey",
            "description": "JWT is a means of representing claims to be transferred between two parties. The claims in a JWT are encoded as a JSON object that is digitally signed using JSON Web Signature (JWS) and/or encrypted using JSON Web Encryption (JWE).",
            "name": "Authorization",
            "in": "header"
        }
    },
    "security": [
        {
            "JWT": []
        }
    ]
}