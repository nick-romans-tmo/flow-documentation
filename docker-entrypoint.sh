#!/bin/bash

STAT=0
echo "starting distillery"
node node_modules/tmi-distillery/dist/index.js build content
if [ $? -eq 0 ] && [ -d /clearwater/content-distillery/public ]; then
   echo "distillery completed."
   if [ -f "content-distillery/.nocache" ]; then
      echo "S3 full sync starting"
      aws s3 sync content-distillery s3://"$S3prefix"clearwater.sda.content --only-show-errors --delete
   else
      echo "S3 delta sync starting"
      aws s3 sync content-distillery s3://"$S3prefix"clearwater.sda.content --only-show-errors 
   fi
   if [ $? -eq 0 ]; then
      echo "S3 sync completed"
   else
      STAT=1
   fi
else
   STAT=1
fi
if [ ${STAT} -eq 0 ]; then
   echo "Successful completion"
else
   echo "One or more steps failed"
   exit -1
fi
