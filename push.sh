#!/usr/bin/env bash

if [ -z "$1" ]; 
then
	echo "User name to login to registry is required"
    exit -1
fi
uname=$1

if [ -z "$2" ]; 
then
	echo "Password login to registry is required"
    exit -1
fi
pwd=$2

docker login artifactory.corporate.t-mobile.com --username $uname --password $pwd

make push
