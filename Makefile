push:
	docker tag sda/flows-build:latest artifactory.corporate.t-mobile.com/sda-docker-snapshot-local/sda/flows-build:latest
	docker push artifactory.corporate.t-mobile.com/sda-docker-snapshot-local/sda/flows-build:latest
sync:
	docker run --rm -e AWS_ACCESS_KEY_ID=${KEY_ID} -e AWS_SECRET_ACCESS_KEY=${ACCESS_KEY} sda/flows-build
troubleshoot:
	docker run --rm --name flows-troubleshoot --entrypoint tail sda/flows-build -f /dev/null
test:
	rm -rf stage
	mkdir -p stage
	git log -1 --name-status --oneline | sed '1d;/^D/d' | awk '{print $$2}' | xargs -I{} cp {} stage
	docker build -t sda/flows-build -f Dockerfile.local .
