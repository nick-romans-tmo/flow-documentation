# Flow Documentation

## Overview

This repository holds the documentation for the major "customer flows" for P&T and is regularly published to https://flows.corporate.t-mobile.com/.  The goal here is to understand and document the dependencies into a series of sequence diagrams:

1. This will build a solid understanding of how the subsystems interact, as the base for product documentation.
2. Project requirements can be better scoped and understood by having solid documentation.
3. These sequence diagrams can serve as the basis for operational processes, reducing MTTR and facilitating troubleshooting.
4. This process will serve as a catalyst for teams changing how they interact -- for each flow, or sub-flow, the accountable engineers can be identified, and a "mini-JAD" can be organized around just that area.

## Quick Start

1. Please request access to this repo via the [#edp-support](https://t-mo.slack.com/messages/C60G34X7C) Slack channel
1. Initially Clone this repo (or a fork of it) using git. If git reports untrusted or self-signed certificate errors, then open the  repo documentation file at ./docs/rootcert-instructions.md using your browser, and follow it's instructions.
1. After making your changes, commit them.  Then execute 'make test'.  This will copy your changes into a docker container and test to make sure that distillery will be able to properly process them.  If this is successful, you can push your changes and submit a pull request.

## Goals

1. Sprints will be organized across teams around flows / subflows.  
1. Dependencies will be understood early.
1. Integration will be ongoing around each sprint, not at the end of the project.
1. Teams will groom user stories for sprints in partnership with their dependent teams.
1. Sprint demos will be attended by all stakeholders to facilitate information exchange and communication.
1. All parties will participate in swagger construction / reviews.
1. All parties will participate in sequence diagramming.

## Process

From a high level, the process will be:

1. A flow will be identified during iterative design or according to a Journey.
1. An architect will identify the as-is flow documentation to be used as a starting point.  If one does not exist, it will be created.
1. An ephemeral team will be organized around that flow, with representation from each of the systems.
1. The "to-be" flow will be designed by the architect with input from the ephemeral team, which will be signed off by that team.  
    1. This will be built in a separate branch of this repo.
    1. The work will be done in a fork, with Pull Requests capturing the dialog from the team members.
    1. When all agree, the PR will be merged into the branch.  This leaves us with the as-is in the master branch, and the to-be in the feature branch.
    1. Lather, rinse, repeat.
1. APIs will be designed around that flow.
    1. Each API will be a swagger.
    1. Swaggers must live in source code control.
    1. PR's will be approved by the impacted parties (i.e., the callers of the API)
    1. Once a base set of swaggers have been defined and approved, stubs will be built.
    1. Stubs should be deployed directly into production.  This will enforce API versioning, deployment automation, and front-load any networking issues that need to be overcome.
    1. Lather, rinse, repeat
1. Once the systems are released, the documentation feature branches will be merged back into master and tagged as the baseline for the next iteration.

### Process Responsibilities

* **Architects** are responsible for:
  * "Current state" workflows and documentation of APIs they own
  * "To-be" Swagger designs for APIs they own
  * Creating pull requests and responding to feedback provided
* **TPMs/TPOs** of API consumers are responsible for:
  * Ensuring their team is represented for each flow and the APIs they consume
  * Prioritizing Swagger reviews on the team
* **Developers** are responsible for:
  * Reviewing proposed Swaggers for APIs they consume in pull requests

## Tools

### PlantUML

PlantUML is an open-source, easy-to-use tool that generates sequence diagrams.  More importantly, the "language" for expressing a sequence diagram is a simple text file.

This gives us the following benefits:

1. The layouts are automatic, so time is spent on function, not form.
1. The text files are very conducive to diff'ing in a commit / PR.  This makes it easy to see at-a-glance what is changing.
1. The tooling for creating robust sequence diagrams is free, simple and cross-platform.  See the `[docs](docs)` subfolder in this project for details.
1. The process for updating is a reflection of the process for coding, which means that it's easy to associate the code with the documentation that was used to build it.
1. The process promotes asynchronous, open communication, which will increase velocity and promote collaboration.  For tracking, we know who-changed-what, when, automatically, and it's easy to rollback to a previous version.

### Linter

The Linter will automatically check your swaggers to see how awesome they are.  As a reminder, if you define a field as a `string`, it doesn't give the consumer a whole lot of information about what it should look like.  But if you add a `pattern` to that field definition, then you could specify that it has to be exactly five digits -- like so: `^\d{5}$`.

The Linter can be accessed [here](https://api-linter.devcenter.t-mobile.com/).

## Taxonomy

We're going to need some way to identify the flows, and to be able to reliably link them together in a web format.  For now, let's go with a simple taxonomy and a flat directory structure.

### Naming

See the [Add Diagrams page](docs/crt/how-to/add-diagrams.md).

## Resources

* [Clearwater Grab-and-Go](https://tmobileusa.sharepoint.com/teams/IDSS/Shared%20Documents/Initiatives/Clearwater/Clearwater%20Resource%20Kit/clearwater_grab-n-go_v3%20branded.asd.pdf)
* [Frequently Asked Questions (FAQ)](https://tmobileusa.sharepoint.com/teams/IDSS/Shared%20Documents/Initiatives/Clearwater/Clearwater%20Resource%20Kit/clearwater_FAQ.pdf)
* [API Reference Portal](https://developer.t-mobile.com/reference)
* [#Clearwater on Slack](https://t-mo.slack.com/archives/C8D77CK7H)


