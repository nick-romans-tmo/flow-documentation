#!/usr/bin/env bash

if [ -z "$1" ]; then
   echo "Access Key Id is required"
   exit -1
fi

if [ -z "$2" ]; then
   echo "Secret Access Key is required"
   exit -1
fi

make sync KEY_ID=$1 ACCESS_KEY=$2
