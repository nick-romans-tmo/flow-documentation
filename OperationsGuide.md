## Basic architecture

All content exists in an S3 bucket located [here.](http://clearwater.sda.content.s3-website-us-west-2.amazonaws.com/public/)  It is not publicly accessible.  There is a nginx proxy container which runs in sd.marathon.ccp.corporate.t-mobile.com.  It is in the sda namespace and it is called flow-documentation.  The configuration for CCP is in the [deploy](https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/deploy) folder.  The scripts to create the proxy container are in the [nginx](https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/nginx) folder.  Since CCP is configured for HAPROXY\_HTTPS=true, CCP maintains the SSL certificate for flows.corporate.t-mobile.com.  Nginx still requires an SSL certificate to proxy correctly so it just uses a localhost certificate.  It will be created automatically when the container is created / recreated.

## Build Process

Jenkins is configured to poll the repo every 5 minutes to see if a new build is required.  It is also configured to only allow 1 build to run at a time.

##### [Pre-build]

As mentioned above, a container built on nginx which is configured as a reverse proxy to the S3 http endpoint.  This is built and deployed to CCP once and should not need to be touched again.  Image size 16MB.

##### [Pre-build]

A container based on alpine:3.7.  This container installs all the necessary apk packages to run awscli and Distillery.  It also runs npm install –production.  This container is pushed to Artifactory and does not need to be rebuilt unless the tmi-distillery package is updated.  The build scripts for this are in the [distillery](https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/distillery) folder. Image size 453MB

First all content is checked out.  Then a script called git-restore-mtime-bare is executed.  This is required to get around features in git and Distillery.  Distillery has a built-in cache mechanism so that it will only distill new files.  Unfortunately, it uses file modified date instead of a hash or checksum to determine if a file is newer.  Git has no concept of time.  When you execute a git clone command, all files will have the time stamp of when the files where cloned, not when they were committed.  This script will change the unix mtime of all files to be the commit time plus one hour.  The extra hour is to prevent a race condition where someone checks in a modification to an existing file while a build is already in progress.  As long as builds take less than one hour, this provides a safety margin.  It does mean that some files may needlessly be re-distilled but that should be a relatively small number.

The Jenkins job has two modes.  Normally a cached build is executed to keep build times down.  If a cached build is running, then the previous build contents from S3 are synced into a directory named content-distillery.  If it is a no-cache build, then just an empty content-distillery folder is created.  A no-cache build is executed nightly at 23:11 to ensure nothing was missed during the cached builds.

Next a Docker build is executed.  This build uses the prebuilt Distillery image as a base image.  It copies in the docs, swagger, sequence\_diagrams, and content-distillery directories.  This image is approximately 1.2 GB in size and will grow as content is added.  It is not pushed to Artifactory because it is a single use image.  It does not contain any S3 credentials.

After the Docker image is created, it is then run.  Jenkins will pass the S3 credentials as run time variables.  While running, it will run Distillery across the content.  If Distillery completes successfully, it will push the content to S3.  When it finishes, the image is removed from the build server.

## Pull Request Linter

A user FAQ for the linter is located [here.](https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/PRLinting)
The Pull Request webhook is configured [here.](https://bitbucket.service.edp.t-mobile.com/plugins/servlet/webhooks/repository/EITCODEDOC/flow-documentation/settings)
It is currently pointed to a service running in PKS [here.](https://gitprgate.px-prd1103.pks.t-mobile.com/events/pullrequest)
The code for the linter is located [here.](https://bitbucket.corporate.t-mobile.com/projects/SDA/repos/git-pr-gate/browse)
