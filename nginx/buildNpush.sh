#!/bin/bash

case "$1" in
'stg')
  echo Configuring container for Staging environment..
  cp default-stg.conf default.conf
  DOCKER_REGISTRY=sda-docker-snapshot-local
  ;; 
'prod')
  echo Configuring container for Production environment..
  cp default-prod.conf default.conf
  DOCKER_REGISTRY=sda-docker-release-local
  ;;
*)
  echo A parmater must be passed and the value must be stg or prod
  exit -1
  ;;
esac

cd ssl
./createCert.sh
cd ..
docker build -t artifactory.corporate.t-mobile.com/${DOCKER_REGISTRY}/sda/flow-proxy:latest .
docker push artifactory.corporate.t-mobile.com/${DOCKER_REGISTRY}/sda/flow-proxy:latest
