#!/bin/bash
##

echo Processing SSL requirements with ENVIRONMENT $1
PFX_PATH=certificate.p12

STAT=0
openssl pkcs12 -in ${PFX_PATH} -clcerts -nokeys -out certs/stg-flows.corp.com.crt 
if [ $? -eq 0 ]; then
    echo Extracted certificate.
else
    STAT=1
fi
openssl pkcs12 -in ${PFX_PATH} -nocerts -out certs/stg-flows.corp.com.pem -nodes 
if [ $? -eq 0 ]; then
    echo Extracted private key.
else
    STAT=1
fi
openssl pkcs12 -in ${PFX_PATH} -nodes -nokeys -cacerts -chain -out certs/stg-flows.corp.com_bundle.crt 
if [ $? -eq 0 ]; then
    echo Extracted chain bundle.
else
    STAT=1
fi
openssl pkcs12 -in ${PFX_PATH} -nokeys -cacerts -out certs/stg-flows.corp.comchain.pem 
if [ $? -eq 0 ]; then
    echo Extracted CA chain.
else
    STAT=1
fi
if [ ${STAT} -eq 0 ]; then
    echo Successfully prepared certificate files
else
    echo One or more of the install steps failed!
fi
