# Usage
To enable these hooks issue the following command. This changes your hook path for your local repository only.

`git config core.hooksPath ./.githooks`

# Backing out
`git config --edit` will show you your local config with what ever editor you have - vi most likely so remember `:q!`. `i` for insert, `esc` to get out of insert `:wq` to save changes.

`git config --unset core.hooksPath`

Git looked for and found `pre-commit` file by name (no extension) in ./.githooks. You could rename it

# Bypass
`git commit --no-verify -m "my awesome idea that doesn't need to be verified"` will bypass the pre-commit hook.

# What it does
By issuing this `git config core.hooksPath ./.githooks` you have told git to look for its hooks in the `./.githooks` directory (the standard location is `.git/hooks`. These files aren't versioned so we have added the `./.githooks` directory)

## pre-commit
We only use the `pre-commit` hook. The `pre-commit` file is in bash (works in git bash on Windows) that checks for non-ascii characters in the file names being committed anywhere and then runs pre-commit.py with python3.

## pre-commit.py
Checks that you have a supported version of plantuml installed on your machine at `/usr/local/bin/`. (Do not know what to do for windows - meaning untested - **need your help**). The manifest in plantuml is wrong so you can't execute it without `java -jar` which needs the full qualified path.

```python
    location = ""
    if ('Darwin' in platform.system()):
        location = '/usr/local/bin/'
    pc = Pre_Commit(location +
                    'plantuml.jar')
```

It assumes that the PlantUML version is atleast *1.2018.13*. If you want to change it change this line `ret_ver = pc.check_puml('1.2018.13')` to the version that you want it to be. The version comparison works like this:

`if StrictVersion(version) < StrictVersion(expected):`

It assumes that anything and everything checked into an directory under sequence_diagram is a sequence_diagram.

It will build each and every sequence diagram that is staged

`git diff --cached --name-only`

I assume that all the files in the commit can be built using the single line version of plantuml with `-nbthread auto`. The benefit is speed. Drawbacks are:
- that I can blow through the max length of the command line. Try OS X `getconf ARG_MAX`. On my machine this is 256 kB. 
- The verbose log is garbled.

`java -Djava.awt.headless=true -jar /usr/local/bin/plantuml.jar -v -nbthread auto ./sequence_diagrams/BD/inventorysales.puml ./sequence_diagrams/BD/raotf.puml -o  /Users/janithajayaweera/Documents/Repositories/flow-documentation/generated_png. `
# Why pre-commit
The premise is that you do not commit garbage. If you roll back you always have a working copy. If you do not care you could use pre-push e.g. If you squash your commits before you push
see https://stackoverflow.com/questions/5189560/squash-my-last-x-commits-together-using-git

# Garbage left behind 
I create a directory called `generated_png` in the current working directory. I do not clean this up. This is so that the user can verify their output whe we fail. But I do leave it behind just in case I got it wrong. This does mean that the next run git status is going to have noise.

You can work around this by doing this

`git reset --hard HEAD^`

# Why not use the standard directory
All hooks that git knows about reside in `./.git/hooks` directory. Nothing in `.git` is versioned. There is the option to ask you to run a script each time we want you to update the file. We have made the assumption that a one time change where updates will be captured is easier. If you clone the repository a new you will need to set the hooksPath again.

# Hacks - erring on the side of caution
PlantUML doesn't always exit with an error code of 0, I found this to be an incovenience e.g. a file was not found.

The verbose output according to Arnaud Roques is not gauranteed to be consistent between versions.

 I parse the verbose output. In this hook scenario file not found is an unlikely. I needed it for testing. I also built the list by eye balling Log.info and Log.error in the plantuml source. 

```python
        error_hints = ['error', 'errors', 'Error', 'Errors', 'cannot', 'Found 0 files','Exception', 'Warning: no image in']
        matching = [s for s in stdout_lines if any(
            xs in s for xs in error_hints)]
```
I used pprint instead of log because it looks prettier and doesn't crowd the window. The log is a list

# FAQ - IDE Specific

There's a checkbox on the commit (and also on the push) dialog box, named "Run Git hooks" - https://intellij-support.jetbrains.com/hc/en-us/community/posts/206339759-How-to-skip-git-pre-commit-hooks-in-PhpStorm-
