from pprint import pprint
import subprocess
import logging
import sys
import platform
from distutils.version import StrictVersion
import os
from pathlib import Path


class Pre_Commit:
    java_jar = "java -Djava.awt.headless=true -jar "

    def __init__(self, puml, output_dir=os.path.join(Path(os.getcwd()), 'generated_png')):
        self.puml = puml
        self.output_dir = output_dir

    def check_puml(self, expected):
        command = self.java_jar + self.puml + " -version"
        p_puml = subprocess.Popen(
            [command], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
        retval = p_puml.wait()
        line = p_puml.stdout.readlines()[0]
        version = line.split(" ")[2].strip()
        try:
            if StrictVersion(version) < StrictVersion(expected):
                pprint("Error: I tried to execute this: " + command + " I was expecting the version number to be at least: " + str(StrictVersion(expected)) + ". I got this instead: '" + line +
                       "You do not appear to have the plantuml.jar version in the location that I am looking in. I need this so I can verify that your sequence diagrams in this commit, build without errors. This is so that you do not have to wait for 3 hours to see that the build broke, and to prevent others from breaking the build")
                return 1
            else:
                return 0
        except:
            ValueError
            pprint("I ran this command: " + command +
                   " I got this response " + line)
            return 1

    def generate_png(self, input, options="-v -nbthread auto"):
        command = self.java_jar + self.puml + " " + \
            options + " " + input + " -o " + self.output_dir
        pprint("I am going to run the following command: " + command +
               ". This is so that I can verify that no errors will occur on the build that will publish everyone's diagrams. For the sake of speed I am running multi threaded. The tradeoff is the sequence of events in the log. If debugging omit the -nbthread auto in the command")
        p_generate = subprocess.Popen(
            [command], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
        retval = p_generate.wait()
        stdout_lines = p_generate.stdout.readlines()
        pprint(stdout_lines)
        if retval > 0:
            pprint("I think something is wrong with your commit - very likely a syntax error, you can open the generated file if there was one per the log above. The files are located in " + self.output_dir)
            return retval
        error_hints = ['error', 'errors', 'Error', 'Errors', 'cannot', 'Found 0 files',
                       'Exception', 'Warning: no image in']
        matching = [s for s in stdout_lines if any(
            xs in s for xs in error_hints)]
        if len(matching) > 0:
            pprint("I found atleast one message that implies a failure. My best guess is that the @startuml @enduml are missing. If this is a false positive help fix it, DIY or please reach out on #ClearWater, please include the file and the error log: " +
                   '\n'.join(matching))
            retval = 1
        return retval

    def get_files_to_be_committed(self):
        command = 'git diff --cached --name-only'
        p_files_added = subprocess.Popen(
            [command], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
        retval = p_files_added.wait()
        stdout_lines = p_files_added.stdout.readlines()
        return stdout_lines

    def get_list_of_files_I_care_about(self, i_care_location, files_to_be_committed):
        check_these_files = [
            path for path in files_to_be_committed if i_care_location in path]
        return check_these_files

    def get_input_files_as_one_line(self, file_list):
        input = " ".join("./"+f.strip() for f in file_list)
        return input


if __name__ == "__main__":
    pprint("My working Directory is: " + str(os.getcwd()))
    pc = Pre_Commit(str(Path('./lib/plantuml.jar')))
    ret_ver = pc.check_puml('1.2018.13')
    if ret_ver > 0:
        pprint("The version is not what I expected. I am exiting with an error. I cannot help you commit files without errors. You can by pass all checks by using --no-verify")
        exit(ret_ver)

    file_list = pc.get_files_to_be_committed()
    sequence_diagrams = pc.get_list_of_files_I_care_about(
        "sequence_diagrams", file_list)
    if(len(sequence_diagrams) == 0):
        pprint("There were no sequence diagrams in this commit")
        exit(0)
    input = pc.get_input_files_as_one_line(sequence_diagrams)
    ret_png = pc.generate_png(input)
    if ret_png > 0:
        pprint("There were errors while attempting to generate pngs from your sequence diagrams. You should not commit your files in the state they are in. You can by pass all checks by using --no-verify")
        exit(ret_png)
    exit(0)
