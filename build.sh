#!/usr/bin/env bash

docker build --no-cache --build-arg ENVIRONMENT=$1 -t sda/flows-build .
