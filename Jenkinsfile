#!groovy

properties(
        [
                [
                        $class  : 'jenkins.model.BuildDiscarderProperty',
                        strategy: [
                                $class      : 'LogRotator',
                                numToKeepStr: '200',
                                daysToKeepStr: '30'
                        ]
                ],
                parameters(
                    [
                       booleanParam(name: 'nocache', description: 'whether distillery should build without a cache', defaultValue: false)
                    ]
                ),
                disableConcurrentBuilds(),
                pipelineTriggers(
                    [
                        [
                            $class: "SCMTrigger", scmpoll_spec: "H/5 * * * *"
                        ],
                    ]
                )
        ]
)
timestamps {
    node('mesos_alp_sda_hugo') {
        try{
            //notify build has started
            notifyBuildStatus('STARTED')

            def credId = "56033728-ea90-4415-8be3-5a460ea05814"
            stage ('Log Environment'){
                sh 'docker version'
                sh 'docker info'
                sh 'env'
            }
            stage('Checkout') {
                    checkout ([
                        $class: 'GitSCM',
                        branches: scm.branches,
                        extensions: [
                                        [$class: 'PruneStaleBranch'],
                                        [$class: 'CleanCheckout'],
                                        [$class: 'WipeWorkspace']
                        ],
                        userRemoteConfigs: scm.userRemoteConfigs
                    ])
            }

            //Build Stage
            stage('Build') {
                if (env.BRANCH_NAME == 'master') {
                   sh './git-restore-mtime-bare'
                   if (params.nocache) {
                      echo "Creating empty cache folder"
                      sh 'mkdir content-distillery'
		      sh 'touch content-distillery/.nocache'
                   } else {
                      withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: 'S3keys']]) {
                         sh 'aws s3 sync s3://clearwater.sda.content ./content-distillery --only-show-errors --exclude "public/*"'
                      }
                   }
                   sh './build.sh'
                   withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: credId, passwordVariable: 'pass', usernameVariable: 'user']]) {
                    // skipping for now to save artifactory space  sh './push.sh ' + user + " '" + pass + "'"
                   }
                } else {
                    if (env.BRANCH_NAME == 'feature/dev') {
                       sh './git-restore-mtime-bare'
                       if (params.nocache) {
                          echo "Creating empty cache folder"
                          sh 'mkdir content-distillery'
                       } else {
                          withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: 'S3keys']]) {
                             sh 'aws s3 sync s3://dev-clearwater.sda.content ./content-distillery --only-show-errors --exclude "public/*"'
                          }
                       }
                       sh './build.sh dev-'
                    }
                }
            }

            //Deploy
            stage('Deploy') {
                if (env.BRANCH_NAME == 'master' || env.BRANCH_NAME == 'feature/dev') {    
                   withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'AWSKey', passwordVariable: 'pass', usernameVariable: 'user']]) {
                      sh './sync.sh ' + user + " '" + pass + "'"
                   }
                } else {
                   echo "Skipping Deployment"
                }
            }
        }
        catch(ex) {
            echo ex.toString()
            currentBuild.result = "FAILURE"
        } finally {
            notifyBuildStatus(currentBuild.result)
        }
    }
}

def notifyBuildStatus(String buildStatus = 'STARTED') {
    // build status of null means successful
    buildStatus =  buildStatus ?: 'SUCCESSFUL'

    // Default values
    def colorName = 'RED'
    def colorCode = '#FF0000'
    def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
    def summary = "${subject} (${env.BUILD_URL})"
    def details = """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    <p>Check console output at "<a href="${env.BUILD_URL}">${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>"</p>"""

    // Override default values based on build status
    if (buildStatus == 'STARTED') {
        color = 'YELLOW'
        colorCode = '#FFFF00'
        if (params.nocache) {
          colorCode = '#0000FF'
          summary = "${subject} w/NOCACHE (${env.BUILD_URL})"
        }
    } else if (buildStatus == 'SUCCESSFUL') {
        color = 'GREEN'
        colorCode = '#00FF00'
    } else {
        color = 'RED'
        colorCode = '#FF0000'
    }
 
    withCredentials([string(credentialsId: 'cwslacktoken', variable: 'cwslacktoken')]) {
        // Send notifications
        slackSend (channel: "#clearwaterdevops", color: colorCode, message: summary, teamDomain: 't-mo', token: cwslacktoken)
    }


    // send to email
    emailext (
      subject: "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
      body: """<html><body><p>${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    <p>Check console output at "<a href="${env.BUILD_URL}">${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>"</p></body></html>""",
      mimeType: 'text/html',
      recipientProviders: [[$class: 'DevelopersRecipientProvider']], to: 'ClearwaterDevOps@T-Mobile.com'
    )
}

