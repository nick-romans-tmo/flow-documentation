# WPC (legacy Web Product Catalog)

### Documentation

Currently (and likely) the only diagrams are related to Bazaarvoice ratings/reviews.

* [Outgoing](bazaarvoiceRatingsFlowP1.puml) Creation of the product catalog file sent ***to*** Bazaarvoice.
  * [Rebellion Integration](bazaarvoiceRatingsRebellionIntegration.puml) Further details on combining the two catalogs
* [Incoming](bazaarvoiceRatingsFlowP2.puml) Ratings/Reviews (average/count) ***from*** Bazaarvoice in batch.
* [Live vs. Batch ratings](bazaarvoiceRatingsFlowP3.puml) How a mix of Live and Batch ratings causes incongruency.
  * Prefer Live everywhere if possible.
* [Shared vs. Owned ratings](bazaarvoiceRatingsFlowP4.puml) What happens when sharing is broken. 
  * Historically because of problems with Rebellion data
  * Could also happen from a change in IDs in the catalog file without proper migration efforts.

