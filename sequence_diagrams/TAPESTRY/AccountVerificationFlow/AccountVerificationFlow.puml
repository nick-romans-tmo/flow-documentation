@startuml
title <size:18>__Tapestry - Verify U1/U2 Customer__</size>
actor "Frontline Rep" as Rep
participant Tapestry as App
participant OKTA as Okta
participant Repdash
participant "API Gateway" as GW
participant "Rebellion Experience Services" as ES
Participant  "RSP/Tibco: \n Provide customer verfication details" as RSP
Participant  "QVXP: \n AccountOverview" as QVXP
participant CHUB

Rep -> App : Login to Tapestry
App -> Okta : Login Credentials
App <-- Okta : oktaToken
Rep -> App : Customer Lookup
App -> CHUB : searchCustomer(msisdn, email, accountNumber)
App <-- CHUB : searchResults
Rep -> App : Select an account
==U1 Customer==
note over App
        - Perform U1 Customer Verification Process
        - Launch QVXP
end note
group U1 Customer
    
    RP -> App :  Account search and verification in Tapestry
note over RP: Tapestry will perform account \nsearch and customer verfification
note over RSP: We are going to levarage exist \nRSPRS rest service to interact with RSP
App -> CHUB : Perform account search passing required to value CHUB
CHUB -> App : Provide the result to Tapestry
App -> App  : Display the search result
App -> App  : Rep select particluar account \nwhich can be U1/U2 customer

group Customer Verfication U1 Account
    group postpaid customer
         App -> RSP : Request to validate if account is bigBAN(RSP Service getSubscriberList)
         RSP -> App : Response from subscriber count.
         App -> RSP : Request for customer verification methods and details(RSP Service getAccountVerificatioData).
         RSP -> App : Response for details of verification methods and details.
    end group
   
    group prepaid customer
         App -> App : Using customer search response \nwhich have Pin verification
    end group

    App -> App: Display the verfication method.
    group postpaid Customer
        App -> RSP : Request to validate customer(RSP Service logAccountVerificationData)
        RSP -> App : Response for customer validation
        App -> RSP : Create a memo (RSP Service insertMemo)
        RSP -> App : Response from create memo
    end group
end group

group U1 Customer AccountOverview
    App->App  : On customer verfication view displayed \nTapestry check/validate is U1 customer \nthen call restApi to QVXP
    App->QVXP : Pass required details for QVXP to \ninitiate call to "RSP/Tibco"
    QVXP->RSP : Call respective service to get account details.
    RSP -> QVXP : Respective response from RSP/Tibco
    QVXP ->QVXP : Save the response in cache (Coherence)
    App -> QVXP : Once customer is verified then \nhandoff to QVXP \nAccountOverview Screen.
    QVXP -> QVXP: AccountOverview screen is loaded with \ndetails which are already reterviewd and \ncached in QVXP (Coherence)
end group
end group

==U2 Customer==
note over App
    Rebellion ES Api's need the JWT token created
    by API_998. The token contains the Rep entitlements,
    which is used by ES when making backend API calls
end note

group Get Rebellion ES JWT Token
    App -> GW : API_998_postRepProfile(OKTA JWT Token)
    GW -> ES : API_998_postRepProfile(OKTA JWT Token)
    ES -> OKTA : Validate OKTA JWT Token
    ES <-- OKTA : Validation Success
    ES -> ESP : UserSecurityEnterprise.getUserDetails(repNTId, [MACID, Device ID])
    ES <-- ESP : Return Rep Profile (name, dealer code, storeId, channelSegmentation
    note over ES
        If multiple roles are returned, use first role
        to get entitlements
    end note
    ES -> ESP : UserSecurityEnterprise.getUserEntitlements(userName, userRoles[0])
    ESP -> OES : getUserEntitlements(userName, userRoles[0])
    ESP <-- OES : Return ABFs (entitlements)
    ES <-- ESP : Return ABFs (entitlements)

    ES -> ES : Create JWT token with embedded Entitlements
    GW <-- ES : Return RepProfile with JWT, OKTA JWT
end group

group Verification Page Load
    
    group Fraud Check
        /' #Fraud '/
        App -> GW:api_2214_lookupUnAuthFraudFlag (ccid,FAN, billingSystem, MSISDN , bscscustomerId)
        GW -> ES: api_2214_lookupUnAuthFraudFlag (ccid,FAN, billingSystem, MSISDN , bscscustomerId)
        ES -> CHUB: UnauthorisedCallsService.getUnauthorizedCalls (ccid,FAN, billingSystem, MSISDN , bscscustomerId)
        ES <-- CHUB:  return fraudFlag, threshholdForUnAuth, totalUnauthAccess
        GW <-- ES: return fraudFlag, threshholdForUnAuth, totalUnauthAccess
        App <-- GW : return fraudFlag, threshholdForUnAuth, totalUnauthAccess
        alt if fraudFLag is true or threshholdForUnAuth >= limit infom Rep
            App -> App: Paint PopUp ( NO Hard Stop)
        end
    end group

    group Authorised Users based on FAN
        App -> GW : api_2064_getAuthorizedUsers(accountNumber, billingSystem, bscsCustomerID)
        GW -> ES : api_2064_getAuthorizedUsers(accountNumber, billingSystem, bscsCustomerID)
        ES -> WSIL : CustomerSummaryWSIL.getAccessibleParties(accountNumber)
        ES <-- WSIL: authorized user list
        ES -> ESP : CustomerNoteEnterprise.getCustomerNote (accountNumber, bscsCustomerID)
        ES <-- ESP: account level note
        GW <-- ES : Authorized users, specialInstruction
        App <-- GW : Authorized users, specialInstruction
    end
    
    App -> GW : api_2056_getEligiblePrimaryMobileNumbers(BscsId)
    GW -> ES : api_2056_getEligiblePrimaryMobileNumbers(BscsId)
    ES -> IAM : Get Primary Number and Preferences(UID:customerNumber)
    ES <-- IAM : profile info with emailVerified flag
    GW <-- ES : profile info with emailVerified flag
    App <-- GW : profile info with emailVerified flag
    App -> App : Show Verification Page
    alt if emailVerified="P" and primary msisdn available
        App -> App : Enable Temporary PIN section
    end
end group

==Verification Process==
group Verification
    note over App
        Verification Methods: **PIN, Temporary PIN, Bypass, Photo Id** \n
        No Verification     : **UnauthorizedAccess**
    end note

    group **Verification Methods**
        Rep -> App: Rep Choose **PIN, Temporary PIN, Bypass, Photo Id**

        group PIN Validation
            Rep -> App : Rep enters PIN and click **Verify** CTA
            App -> GW : api_2007_customerDetailsSearch (accountNumber, bscsCustomerID, ccid, securityPIN)
            GW -> ES : api_2007_customerDetailsSearch (accountNumber, bscsCustomerID, ccid, securityPIN)
            ES -> CHUB : CustomerPINManagementService.ValidatePIN (accountNumber, bscsCustomerID, ccid, securityPIN)
            CHUB -> IAM : ValidatePIN
            CHUB <-- IAM: validation result
            ES <-- CHUB : validation result
            GW <-- ES : validation result
            App <-- GW : validation result

            alt if pinStatus="VALID"
                App -> GW : api_2016(205)_postCustomerVerification (accountNumber, bscsCustomerId, verificationIndicator=true, verificationType="pin", dealerCode, repNTId)
                GW -> ES : api_2016(205)_postCustomerVerification (accountNumber, bscsCustomerId, verificationIndicator=true, verificationType="pin", dealerCode, repNTId)
                ES -> ESP : EventEnterprise.createCustomerInteractionEvent
                ES <-- ESP: response
                GW <-- ES : response
                App <-- GW : response
                App -> App : Launch **Rebellion Microapp**
            end
        end group

        group Temporary PIN
            Rep -> App : Rep clicks **Send Temporary PIN** CTA
            App -> App : Display phone number and enable **Send** CTA
            Rep -> App : Rep clicks **Send** CTA

            App -> GW : API_2213_generateTempPIN (bscsCustomerId, msisdn, UUID)
            GW -> ES : API_2213_generateTempPIN (bscsCustomerId, msisdn, UUID)
            ES -> IAM : GenerateTempPIN
            ES <-- IAM : encrypted Temp PIN, expiration time
            GW <-- ES : encrypted Temp PIN, expiration time
            App <-- GW : encrypted Temp PIN, expiration time

            Rep -> App : Rep enters Temp PIN gathered from Customer \nclicks **Verify** CTA

            App -> GW : api_2212_validateTempPIN (bscsCustomerId, email, UUID, tempPin)
            GW -> ES : api_2212_validateTempPIN (bscsCustomerId, email, UUID, tempPin)
            ES -> IAM : ValidateTempPIN
            ES <-- IAM : status
            GW <-- ES : status
            App <-- GW : status

            alt if status="Success"
                App -> GW : api_2016(205)_postCustomerVerification (accountNumber, bscsCustomerId, verificationIndicator=true, verificationType="One-time-pin", dealerCode, repNTId)
                GW -> ES : api_2016(205)_postCustomerVerification (accountNumber, bscsCustomerId, verificationIndicator=true, verificationType="One-time-pin", dealerCode, repNTId)
                ES -> ESP : EventEnterprise.createCustomerInteractionEvent
                ES <-- ESP: response
                GW <-- ES : response
                App <-- GW : response
                App -> App : Launch **Rebellion Microapp**
            end
        end group

        group Bypass, PhotoId
            App -> GW : api_2016(205)_postCustomerVerification (accountNumber, bscsCustomerId, verificationIndicator, verificationType, dealerCode, repNTId)
            GW -> ES : api_2016(205)_postCustomerVerification (accountNumber, bscsCustomerId, verificationIndicator, verificationType, dealerCode, repNTId)
            ES -> ESP : EventEnterprise.createCustomerInteractionEvent
            ES <-- ESP: response
            GW <-- ES : response
            App <-- GW : response
            App -> App : Launch **Rebellion Microapp**
        end group
    end group


    group  **No Verification**

        Rep -> App: Rep Choose **UnauthorizedAccess**

        group Unauthorized Access
            App -> GW: api_2215_logUnAuthorizedCall (accountNumber, msisdn, billingSystem, ccid, bscsCustomerId, userName)
            GW -> ES: api_2215_logUnAuthorizedCall (accountNumber, msisdn, billingSystem, ccid, bscsCustomerId, userName)
            ES -> CHUB :UnauthorizedCallsService.writeUnauthorizedCalls (accountNumber, msisdn, billingSystem, ccid, bscsCustomerId, userName)
            CHUB -> REEF: CreateEvent (accountNumber, msisdn, billingSystem, ccid, bscsCustomerId, userName)
            CHUB <-- REEF : response
            ES <-- CHUB : response
            GW <-- ES : response
            App <-- GW : response

            App -> GW : api_2016(205)_postCustomerVerification (accountNumber, bscsCustomerId, verificationIndicator, verificationType, dealerCode, repNTId)
            GW -> ES : api_2016(205)_postCustomerVerification (accountNumber, bscsCustomerId, verificationIndicator, verificationType, dealerCode, repNTId)
            ES -> ESP : EventEnterprise.createCustomerInteractionEvent
            ES <-- ESP: response
            GW <-- ES : response
            App <-- GW : response
            App -> App : Launch **Rebellion Microapp**
        end group
    end group
end group

==Launch Rebellion==
note over App
    Tapestry needs to create CV&F Token with customer details
    and verification method, status.

    Repdash will use the CV&F token to read the customer context
    , saves info StateStore and launches Rebellion
end note
App -> GW : api_2223_generateEnterpriseToken (customer and verification data)
GW -> ES : api_2223_generateEnterpriseToken (customer and verification data)
ES -> ESP : EnterpriseToken.generateToken (customer and verification data)
ES <-- ESP: token
GW <-- ES : token
App <-- GW : token

App --> Repdash : launchUrl (Rep Okta JWT, CV&F Token)
Repdash -> Repdash : Initialize Repdash, Update StateStore, Launch Rebellion

@enduml
