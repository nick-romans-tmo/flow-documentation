@startuml
title: __**CR1024 POS Store Operations**__ - Rebellion Unification Sequence Diagram
' Based on L3 Store Management in EA Repo
legend top left
	| Legend of colors |
	|<#ffc2b3> **CR1024** specific changes |
'	|<#ffffcc> BAU or no changes for CR1024 |
	|<#ffffff> BAU or no changes for CR1024 |
	|<#e6f2ff> A separate section within same alt or group |
	|<#fbfb77> Comments |
endlegend

' This is a comment on a single line
' autonumber
' You can use the skinparam command to change colors and fonts for the drawing
skinparam sequenceArrowThickness 2
skinparam roundcorner 20
skinparam maxmessagesize 250

'skinparam sequenceParticipant underline
skinparam handwritten true
skinparam ParticipantPadding 10
'skinparam BoxPadding 10
'skinparam sequenceGroupBackgroundColor #A9DCDF
skinparam sequenceLifeLineBorderColor blue
skinparam sequenceLifeLineBackgroundColor #A9DCDF

actor "Rep" as rep
participant "\n Rebellion UI" as reb #ff66b3
' end box
participant "API \n Gateway" as GW #ff9900
participant "Experience \n Services" as ES #ff9900
participant "\n E// WSIL" as wsil #LightBlue
participant "\n TPOS" as tpos #ff66b3

participant "\n ESP-BW" as esp #ff66b3
participant "\n Active Cache" as cache #ff66b3
participant "<<TMO-U1>>\n TMAG" as tmag #ff66b3
participant "U1 \n POS DB" as posdb #ff66b3

'participant "\n IDIA" as idia #ff66b3
'participant "\n DTS" as dts #ff66b3
'participant "\n JPay" as jpay #ff66b3
'participant "\n REEF" as reef #ff66b3
'participant "\n PDM" as pdm #ff66b3


esp -> tmag: Call Watson DB to build Mac/Device ID <- -> Store Relation()
note over cache: Cache to help know \n which Mac Id is tied \n to which Store Id
esp -> cache: Build Cache()
esp -> posdb: Get SAP files data to build Cache()
esp -> cache: Build Cache()

group #ffc2b3 New for CR1024
    reb -> reb: Define a new toggle switch configuration to turn off or on **CR1024** changes.
    note right of reb: When this toggle switch is off, \n none of the CR1024 experience would be seen in Rebellion. \n Follow the Rebellion Architecture guidelines in defining the configuration.
end 'group #1 'New for CR1024


group Orchestration of API calls

else #e6f2ff part 1; if storeId missing in input
reb -> GW: API_2700_storeMacAddress (applicationUserId, macAdd)
    GW -> ES: API_2700_storeMacAddress (applicationUserId, macAdd)
    ES -> esp: searchStoreCodeCoreCall (macAdd) /storecode/search  [CAAS.getStoreCode]
    esp -> ES: searchStoreCodeResponse

    group _
    else storeAllocation data present
         GW -> GW: storeId
    else if storeAllocation missing
        GW -> reb: throw error
   end group ' parse storeAllocation
else part 2

    ES -> esp: getStoreDetails (storeId)  /{storeId}  [CAAS.getStoreDetails]
    ES <- esp: store address, phoneNumber and STORECAPABILITIES

    group #ffc2b3 New for CR1024
        ES -> ES: Return a **new field** locationType. \n For a given store, for specificationGroup @name = "STORECAPABILITIES", __if specificationValue @name="MOBILE_TRUCK_STORE"__ exists then set **locationType=MOBILE_TRUCK**. \n \n Otherwise set locationType as empty value.
    end 'group #2 'New for CR1024

else #e6f2ff part 3
    ES -> esp: getTillStatus (storeId etc, POSTILL) /{storeId}/till/status  [StoreOperationsEnterprise.getStoreTillStatus]
    ES <- esp: storeStatus, terminal details


else part 4
    ES -> esp: getTillStatus (storeId etc, MOBILITYTILL) /{storeId}/till/status  [StoreOperationsEnterprise.getMobilityTillStatus]
    ES <- esp: storeStatus, terminal details
    ES -> GW: retailStoreData
    GW -> reb: retailStoreData
else #e6f2ff part 5
    reb -> GW: API_2701_networkCashDrawer (storeId)
    GW -> ES: API_2701_networkCashDrawer (storeId)
    ES -> esp: getNetworkCashDrawers (storeId) /{storeId}/networkcashdrawers [StoreOperationsEnterprise.getNetworkCashDrawers]
    ES <- esp:
    ES -> GW: terminalId, barcodeId
    GW -> reb: terminalId, barcodeId
end group 'Orchestration of  API calls



note right of reb: **isTablet** is determined based on navigator.userAgent string
alt
else isTablet a.k.a REMO
    group #ffc2b3 New for CR1024-UC01A
        reb -> reb: Hide "**Mobility cash drawer**" blade
        reb -> reb: Enable "**Store Operations**" blade.
        note right of reb: When the rep selects this blade, \n experience is the same as for a rep using a desktop \n except any specific changes requested \n (including presence of a warning if the store or tills are not open).
        reb -> reb: Hide "**Open PosTill** / **ClosePosTill**" inside the "Store Operations" blade.
        reb -> reb: Hide "**Change store Location**" inside the "Store Operations" blade.
        reb -> reb: Hide "**nosale**" inside the "Store Operations" blade.
    end 'group #3 'New for CR1024-UC01A

    group #ffc2b3 New for CR1024-UC01B
        reb -> reb: Keep "**Print Reports**" enabled inside the "Store Operations" blade.
        note right of reb: Rep selects a group of reports or an individual report to print, per existing UI. \n Report prints in same layout/size as from desktop
    end 'group #4 'New for CR1024-UC01B
else #e6f2ff A POS workstation
    reb -> reb: (No Changes) - Print Reports functionality works without any changes.
end alt 'isTablet

rep -> reb: Click on Store Operations blade
reb -> reb: Blade expands
group Open / Close Store
else storeStatus=Closed
    rep -> reb: Click **Open Store**
    group New for CR1024-UC02 - Set Store Address
    else #ffc2b3 if locationType=MOBILE_TRUCK
        reb -> reb: Display current store address as obtained from API_2700_storeMacAddress
        reb -> reb: Display entry fields to set mobile store’s address. \n Rep is required to enter a street address and ZIP code for the mobile store
        rep -> reb: Input ZIP code and move focus away from the textbox (using key board or mouse)
        reb -> GW: API_2004_addressValidation
        GW -> ES: API_2004_addressValidation
        ES -> wsil: AddressWSIL.validateAddress
        wsil -> ES: City and State
        ES -> GW: City and State
        GW -> reb: City and State
        reb -> reb: Populate the City and State textboxes with the first result and keep them editable for changes.
        rep -> reb: Complete remaining address inputs and click **Continue**
        reb -> GW: API_2004_addressValidation
        GW -> ES: API_2004_addressValidation
        ES -> wsil: AddressWSIL.validateAddress
        wsil -> ES: list of matching results of address and usageContext for each
        ES -> GW: list of matching results of address and usageContext for each
        GW -> reb: List of matching results of address and usageContext for each
        group #ffc2b3 Parse validateAddress results
        else if No matches found
            reb -> reb: Show error and let Rep correct the input address
        else if matches found
            note right of reb: When the rep has entered a valid address: \n *	System displays match(es) for the address. \n *	If only one match, that radio button is pre-selected. \n *	If more than one match, rep is required to select an address.
            reb -> reb: Display suggested addresses
            rep -> reb: Confirm a matched address and Cick **Open Store** button

            reb -> GW: API_2718_setTempStoreAddress
            GW -> ES: API_2718_setTempStoreAddress
            ES -> tpos: /setTempStoreAddress
            tpos -> ES: SetTempStoreAddressResponse
            ES -> GW: SetTempStoreAddressResponse
            GW -> reb: SetTempStoreAddressResponse

            group Parse SetTempStoreAddressResponse
            else if SetTempStoreAddressResponse is success
                note over reb: Proceed with Open Store functionality

                group if isTablet a.k.a REMO show modal
                    note right of reb: Once the rep has selected an address (if more than one option) and clicked **Open store**: \n * System displays a modal instructing rep to log out of Tapestry and back in. \n * This modal cannot be dismissed.
                end group 'modal for logout of Tapestry
        else if SetTempStoreAddressResponse is success
                note over reb: Do NOT Open Store.
            end 'group Parse SetTempStoreAddressResponse

        end group ' validateAddress results

    else locationType is NOT MOBILE_TRUCK
        note over reb: Continue existing experience same as before CR1024
    end group 'locationType

else storeStatus=Open
    rep -> reb: Click **Change Store Address** inside **Store Operations**
    group New for CR1024-UC02 - Change Store Address
    else #ffc2b3 if locationType=MOBILE_TRUCK
        reb -> GW: API_2717_getTempStoreAddress (storeId)
        GW -> ES: API_2717_getTempStoreAddress (storeId)
        ES -> tpos: /getTempStoreAddress (storeId)
        tpos -> ES: GetTempStoreAddressResponse
        ES -> GW: GetTempStoreAddressResponse
        GW -> reb: GetTempStoreAddressResponse

        reb -> reb: Display **current store address** as obtained from Api_2717_getTempStoreAddress
        reb -> reb: Display entry fields to change mobile store’s address. \n Rep is required to enter a street address and ZIP code for the mobile store
        rep -> reb: Input ZIP code and move focus away from the textbox (using key board or mouse)
        reb -> GW: API_2004_addressValidation
        GW -> ES: API_2004_addressValidation
        ES -> wsil: AddressWSIL.validateAddress
        wsil -> ES: City and State
        ES -> GW: City and State
        GW -> reb: City and State
        reb -> reb: Populate the City and State textboxes with the first result and keep them editable for changes.
        rep -> reb: Complete remaining address inputs and click **Continue**
        reb -> GW: API_2004_addressValidation
        GW -> ES: API_2004_addressValidation
        ES -> wsil: AddressWSIL.validateAddress
        wsil -> ES: list of matching results of address and usageContext for each
        ES -> GW: list of matching results of address and usageContext for each
        GW -> reb: List of matching results of address and usageContext for each
        group #ffc2b3 Parse validateAddress results
        else if No matches found
            reb -> reb: Show error and let Rep correct the input address
        else if matches found
            note right of reb: When the rep has entered a valid address: \n *	System displays match(es) for the address. \n *	If only one match, that radio button is pre-selected. \n *	If more than one match, rep is required to select an address.
            reb -> reb: Display suggested addresses
            rep -> reb: Confirm a matched address and Cick **Open Store** button

            reb -> GW: API_2718_setTempStoreAddress
            GW -> ES: API_2718_setTempStoreAddress
            ES -> tpos: /setTempStoreAddress
            tpos -> ES: SetTempStoreAddressResponse
            ES -> GW: SetTempStoreAddressResponse
            GW -> reb: SetTempStoreAddressResponse

            group Parse SetTempStoreAddressResponse
            else if SetTempStoreAddressResponse is success
                note over reb: Keep the Store Open. No expicit changes.

                group if isTablet a.k.a REMO show modal
                    note right of reb: Once the rep has selected an address (if more than one option) and clicked **Open store**: \n * System displays a modal instructing rep to log out of Tapestry and back in. \n * This modal cannot be dismissed.
                end group 'modal for logout of Tapestry
        else if SetTempStoreAddressResponse is success
                note over reb: Keep the Store Open. No expicit changes.
            end 'group Parse SetTempStoreAddressResponse

        end group ' validateAddress results

else storeStatus=Open and Rep clicks **Close Store**
    rep -> reb: Click **Close Store** inside **Store Operations**
    group New for CR1024-UC02 - Close Store
    else #ffc2b3 if locationType=MOBILE_TRUCK
        reb -> GW: API_2719_updateTempStoreAddressStatus (storeId, addrsStatus=INACTIVE)
        GW -> ES: API_2719_updateTempStoreAddressStatus
        ES -> tpos: updateTempStoreAddressStatus (storeId)
        tpos -> ES: UpdateTempStoreAddressStatusResponse
        ES -> GW: UpdateTempStoreAddressStatusResponse
        GW -> reb: UpdateTempStoreAddressStatusResponse
                note over reb: Close Store
    else locationType is NOT MOBILE_TRUCK
        note over reb: Continue existing experience same as before CR1024
    end group 'locationType
end group 'Open / Close Store

@enduml