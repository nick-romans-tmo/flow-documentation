@startuml
title Memo And Follow up

participant "Rebellion UI" as UI
participant "API Gateway" as GW
participant "Experience Services" as ES
participant "Experience Services Cache" as ESCH
participant "AEM" as AEM
participant "WSIL" as WSIL
/'participant "Notification Domain" as NotificationDomain'/
participant "Event Enterprise" as EventEnterprise



note over UI: When Rep Dash is loaded


/'****************************************************'/
UI -> GW : API_2021_getRapDashLookupCriteria(Type=Activity)
GW -> ES : API_2021_getRapDashLookupCriteria(Type=Activity)
ES -> AEM : GetEventReferenceData
ES <-- AEM

ES -> EventEnterprise : EventEnterprise.getActivityReferenceData
ES <-- EventEnterprise

ES -> WSIL : CaseWSIL.getCaseReasonCodes
ES <-- WSIL

GW <-- ES
UI <-- GW


/'****************************************************'/
UI->UI: Rep login() and verifies customer
UI->UI: Rep clicks create Memo CTA

UI -> GW : API_2020_postSaveMemoAndFollowUp(FAN, RepId, InteractionId..)
GW -> ES : API_2020_postSaveMemoAndFollowUp(FAN, RepId, InteractionId..)

ES -> EventEnterprise : EventEnterprise.createEvent
ES <-- EventEnterprise
alt Create Follow up
ES -> WSIL : CaseWSIL.createCase
ES <-- WSIL
end
GW <-- ES
UI <-- GW

/'****************************************************'/


@enduml