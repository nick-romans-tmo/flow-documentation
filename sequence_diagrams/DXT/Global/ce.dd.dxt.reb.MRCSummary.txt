@startuml
title Rep Dash MRC Summary

participant "Rebellion UI" as UI
participant "API Gateway" as GW
participant "Experience Services" as ES
participant "Experience Services Cache" as ESCH
participant "WSIL" as WSIL


note over UI: Rebellion Application is initialized
UI->UI: Rep login()
UI->UI: Customer/Order lookup and verification
UI->UI: Initiate MRC Summary by clicking on $ CTA

/'****************************************************'/

UI -> GW : API_2006_customerAccountBillingSummary(CCID, AccountNumber)
GW -> ES : API_2006_customerAccountBillingSummary(CCID, AccountNumber)
ES -> WSIL : CustomerSummaryWSIL.getAggregatedCustomerInfo
ES <-- WSIL
GW <-- ES
UI <-- GW
/'****************************************************'/

UI -> GW : API_2008_customerAccountLineBillingDetails(AccountNumber, CCID, BillCycleDates...)
GW -> ES : API_2008_customerAccountLineBillingDetails(AccountNumber, CCID, BillCycleDates...)
ES -> WSIL : LineOfServiceDetailsWSIL.getLineOfServiceDetails
ES <-- WSIL
GW <-- ES
UI <-- GW

/'****************************************************'/




@enduml