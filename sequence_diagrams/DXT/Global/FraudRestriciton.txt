@startuml
title Fraud Restrictions

participant "Rebellion UI" as UI
participant "API Gateway" as GW
participant "Experience Services" as ES
participant "Experience Services Cache" as ESCH
participant "WSIL" as WSIL
/'participant "Notification Domain" as NotificationDomain'/
participant "Event Enterprise" as EventEnterprise



note over UI: When Rep Dash is loaded


/'****************************************************'/
UI -> UI: getCustomerDetails
alt if Fraud Exists at customer/FAN/Line
UI-> ES: getRestrictions
ES->WSIL : getRestricitonsWSIL
end alt
ES->Tibco: GetCustomerentitlements
ES -> ES : DecisionManager API ( Restrictions, Entitlement)
ES-> UI: Permissions Object.

/'****************************************************'/
UI->UI: Rep login() and verifies customer
UI->UI: Rep clicks create Memo CTA

UI -> GW : API_2020_postSaveMemoAndFollowUp(FAN, RepId, InteractionId..)
GW -> ES : API_2020_postSaveMemoAndFollowUp(FAN, RepId, InteractionId..)

ES -> EventEnterprise : EventEnterprise.createEvent
ES <-- EventEnterprise
alt Create Follow up
ES -> WSIL : CaseWSIL.createCase
ES <-- WSIL
end
GW <-- ES
UI <-- GW

/'****************************************************'/


@enduml