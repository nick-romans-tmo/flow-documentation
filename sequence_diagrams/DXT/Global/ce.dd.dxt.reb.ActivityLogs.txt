@startuml
title Activity Logs

participant "Rebellion UI" as UI
participant "API Gateway" as GW
participant "Experience Services" as ES
participant "Experience Services Cache" as ESCH
participant "WSIL" as WSIL
/'participant "Notification Domain" as NotificationDomain'/
participant "Event Enterprise" as EventEnterprise



note over UI: Rebellion Application is initialized
UI->UI: Rep login()

UI->UI: Customer/Order lookup

UI->UI: Rep clicks Activity Log CTA

/'****************************************************'/
UI -> GW : API_2041_getAccountActivitySummary(profileId, accountId, BSCSId..)
GW -> ES : API_2041_getAccountActivitySummary(profileId, accountId, BSCSId..)
ES -> EventEnterprise : EventEnterpriseWSIL.getCustomerActivities
ES <-- EventEnterprise
GW <-- ES
UI <-- GW


/'****************************************************'/

alt Customer Activity Log
UI -> GW : API_2041_getAccountActivitySummary(profileId, accountId, BSCSId, custRole..)
GW -> ES : API_2041_getAccountActivitySummary(profileId, accountId, BSCSId, custRole..)
ES -> EventEnterprise : EventEnterpriseWSIL.getCustomerActivities
ES <-- EventEnterprise
GW <-- ES
UI <-- GW
end
/'****************************************************'/


@enduml