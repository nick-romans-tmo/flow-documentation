@startuml
title <size:18>__Manage Financial Account Settings__</size>
actor "Agent or Customer" as User
participant "Rebellion" as UI
participant "API Gateway" as GW
participant "Experience Services" as ES
participant WSIL
participant ON
participant OM
participant BSCS

User -> UI : User navigates to **Manage Profile** Page
group Change Billing Address
    User -> UI : User selects **Addresses** - **Billing Address** section

    group  Retrieve Billing Address
        UI -> GW : api_2062_getBillingAddress(BscsId, FAN)
        GW -> ES : api_2062_getBillingAddress(BscsId, FAN)
        ES -> WSIL: FinancialAccountDetailsWSIL.getFinancialAccountDetails(BscsId, FAN)
        ES <-- WSIL: returns billing address with sequence/contact id
        GW <-- ES : returns billing address with sequence/contact id
        UI <-- GW : returns billing address with sequence/contact id
    end

    User -> UI : User updates the address

    User -> UI : User enters a valid zipCode
    alt Get City & State based on postal/zipcode
        UI -> GW : api_2004_addressValidation(zipCode)
        GW -> ES : api_2004_addressValidation(zipCode)
        ES -> WSIL : AddressWSIL.validateAddress (zipCode)
        ES <-- WSIL: return City Name and State Code for the zipCode
        GW <-- ES : return City Name and State Code for the zipCode
        UI <-- GW : return City Name and State Code for the zipCode
        UI -> UI : Validate and update City and State fields
    end

    User -> UI : User clicks **Continue**

    group Validate Billing Address
        UI -> GW : api_2004_addressValidation(newAddress, usageContext)
        GW -> ES : api_2004_addressValidation(newAddress, usageContext)
        ES -> WSIL: AddressWSIL.validateAddress (newAddress, usageContext)
        ES -> ES : cache address data and generate address id
        ES <-- WSIL: return list of addresses with conflicts
        GW <-- ES : return list of addresses with conflicts
        UI <-- GW : return list of addresses with conflicts
    end
    note over UI
    UI receives list of addresses. All addresses returned by validateAddress are normalized.

    If the address object has overrideIndicator flag as TRUE then this is Original Address
    (also called as given address or user entered address) enhanced by the normalized fields.
    There will be only one address as overrideIndicator flag as TRUE,
    rest all will have overrideIndicator flag as FALSE
    UI will also read additional field from the response for each address i.e. resultNumber.
    This will be present for all suggested and original address
    end note

    User -> UI : User selects either the original or suggested address

    group Update Billing Address
        UI -> GW : API_2063_updateAddress(bscsId, FAN, usageContext=BILLING)
        GW -> ES : API_2063_updateAddress (bscsId, FAN, usageContext=BILLING)
        ES -> ES : Retrieve address data based on the address id from 2004 cache
        ES -> WSIL: ContactWSIL.updateContact(bscsId, FAN, address, usageContext=BILLING)
        ES <-- WSIL: response
        GW <-- ES : response
        UI <-- GW : response
    end

    alt Address update has conflict with Plans & Services
    User -> UI : Start CPS Flow
    User -> UI : Complete CPS Flow, submit MANAGERATEPLAN order with \n'Update billing address' action

    end
end

group Change bill Delivery Method
    group  Get Bill Delivery Preferences
        UI -> GW : api_2036_customerAccountBillingPreferences(bscsId, FAN)
        GW -> ES : api_2036_customerAccountBillingPreferences(bscsId, FAN)
        ES -> WSIL: FinancialAccountDetailsWSIL.getFinancialAccountDetails(bscsId, FAN)
        ES <-- WSIL: return billing preferences
        GW <-- ES : return billing preferences
        UI <-- GW : return billing preferences
    end
    UI -> UI: Display current Bill Delivery method (Paperless/Mail),\nBill Type choices (Summary/Detail)
    User -> UI : User selects delivery method
    alt Delivery Method=Paperless
        group Get Terms & Conditions
            UI -> GW : api_2806_termsAndConditions(contentType:HTML, \nrequestType:LEGAL_TEXT, documentSubType:PAPERLESS_BILL)
            GW -> ES : api_2806_termsAndConditions(contentType:HTML, \nrequestType:LEGAL_TEXT, documentSubType:PAPERLESS_BILL)
            ES -> WSIL: DocumentManagementWSIL.createDocuments(contentType:HTML, \nrequestType:LEGAL_TEXT, documentSubType:PAPERLESS_BILL)
            ES <-- WSIL: return T&C
            GW <-- ES : return T&C
            UI <-- GW : return T&C
        end
        UI -> UI : Display Terms and Conditions
        UI -> UI : Hide Bill Type section
    end

    alt Delivery Method=Bill in the mail
        UI -> UI : Show Bill Type section
    end
    User -> UI : User clicks **Agree and Submit**

    group Save Billing Preferences
        UI -> GW : api_2037_updateCustomerAccountBillingPreferences(bscsId, FAN, preferences)
        GW -> ES : api_2037_updateCustomerAccountBillingPreferences(bscsId, FAN, preferences)
        ES -> WSIL: FinancialAccountWSIL.updateFinancialAccount(bscsId, FAN, preferences)
        ES <-- WSIL: return order confirmation
        GW <-- ES : return order confirmation
        UI <-- GW : return order confirmation
    end

end

group Change bill language
    group  Get Billing Language
        UI -> GW : api_2036_customerAccountBillingPreferences(bscsId, FAN)
        GW -> ES : api_2036_customerAccountBillingPreferences(bscsId, FAN)
        ES -> WSIL: FinancialAccountDetailsWSIL.getFinancialAccountDetails(bscsId, FAN)
        ES <-- WSIL: return billing language
        GW <-- ES : return billing language
        UI <-- GW : return billing language
    end
    UI -> UI: Display current language
    User -> UI : User selects English or Spanish, and clicks **Save**

    group Save Billing Language
        UI -> GW : api_2037_updateCustomerAccountBillingPreferences(bscsId, FAN, language)
        GW -> ES : api_2037_updateCustomerAccountBillingPreferences(bscsId, FAN, language)
        ES -> WSIL: FinancialAccountWSIL.updateFinancialAccount(bscsId, FAN, language)
        ES <-- WSIL: return order confirmation
        GW <-- ES : return order confirmation
        UI <-- GW : return order confirmation
    end


end

group Change billing cycle
    note over User
    NOT SUPPORTED for Prepaid
    end note
end


group Segmentation
    User -> UI : Click the blade **Segmentation**

    group Page Load - Get Lookup Values for the Drop down
        UI -> GW: API_2021_getLookupValues (type=segmentationReference)
        GW -> ES: API_2021_getLookupValues (type=segmentationReference)
        ES -> WSIL: CustomerDataWSIL.getCustomerDomainReference
        WSIL -> BSCS : Channel Segmentation
        WSIL <-- BSCS: response
        ES <-- WSIL: Return reference data
        GW <-- ES: Return reference data
        UI <-- GW: Return reference data
        UI -> UI: Populate drop down controls
    end
    group Display Account Segmentation Info
        UI -> UI : Display Account Segmentation info from \n **API_2006_accountSummary** cache
        alt IF Cache information is not available
            UI -> GW : API_2006_accountSummary (CCID, FAN, MsisdnInFocus)
            GW -> ES : API_2006_accountSummary (CCID, FAN, MsisdnInFocus)
            ES -> WSIL: CustomerSummaryWSIL.getAggregatedCustomerInfo (FAN)
            ES <-- WSIL : Return aggregated customer info
            note over WSIL
            Billing block info
            Line(s) info - MSISDN, Line Designation, Line Status, Plan
            end note
            ES -> WSIL : FinancialAccountDataWSIL.getMonthlyRecurringCharges(FAN)
            ES <-- WSIL : Return estimated monthly recurring \ncharges for the next bill cycle
            GW <-- ES : Return account summary
            UI <-- GW : Return account summary
        end
    end
    User -> UI : Click **Edit** to change Segmentation information, change values\nclick **Check**

    group Check Segmentation Compatibility
        UI -> GW: API_2067_getSegmentationCompatibility(BscsId, SegmentationAttributes)
        GW -> ES: API_2067_getSegmentationCompatibility(BscsId, SegmentationAttributes)
        ES -> WSIL: CustomerDataWSIL.checkSegmentationCompatibility(BscsId, SegmentationAttributes)
        WSIL -> ON : checkSegmentationCompatibility(BscsId, SegmentationAttributes)
        WSIL <-- ON : return warnings and discount information
        ES <-- WSIL: return warnings and discount information
        GW <-- ES: return warnings and discount information
        UI <-- GW: return warnings and discount information
        UI -> UI : Display warning messages, enable **Check Again** and **Save** CTAs
    end

    User -> UI : Click **Save** to update Segmentation information

    group Save Segmentation Attribute Changes
            UI -> GW: api_2068_updateAccountSegmention(BscsId, FAN, SegmentationAttributes)
            GW -> ES: api_2068_updateAccountSegmention(BscsId, FAN, SegmentationAttributes)
            ES -> WSIL: FinancialAccountWSIL.updateFinancialAccount(BscsId, FAN, SegmentationAttributes)
            WSIL -> OM : submitOrder(BscsId, FAN, SegmentationAttributes)
            OM -> BSCS : updateFinancialAccount(BscsId, FAN, SegmentationAttributes)
            OM <-- BSCS : return response
            WSIL <-- OM : return orderId and orderStatus
            ES <-- WSIL: return orderId and orderStatus
            GW <-- ES: return orderId and orderStatus
            UI <-- GW: return orderId and orderStatus
    end
end
@enduml
