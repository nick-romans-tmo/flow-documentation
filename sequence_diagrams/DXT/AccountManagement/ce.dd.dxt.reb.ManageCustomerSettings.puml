@startuml
title <size:18>__Manage Customer Settings__</size>
actor "Agent or Customer" as User
participant "Rebellion" as UI
participant "API Gateway" as GW
participant "Experience Services" as ES
participant WSIL
participant OM
participant BSCS
participant EMA
participant IAM

User -> UI : Select **My Profile** from Global Navigation

UI -> AEM : Get My-Profile.content.json
UI -> UI : Render page

group Profile Information
    User -> UI : Click the blade **Profile Information**
    note over UI
        Manage Names, Email, My Numbers, Password, PIN,
        Language Preference, Security Questions
    end note

    UI -> UI : Display Name, Email from **API_2007_getCustomerDetails** cache

    group Get My Numbers
        UI -> GW : API_2056_getEligiblePrimaryMobileNumbers(BscsId)
        GW -> ES : API_2056_getEligiblePrimaryMobileNumbers(BscsId)
        ES -> IAM : Get Primary Number and Preferences(UID:customerNumber)
        ES <-- IAM : return current Primary Number \nand all eligible Primary Numbers
        GW <-- ES : return current Primary Number \nand all eligible Primary Numbers
        UI <-- GW : return current Primary Number \nand all eligible Primary Numbers

        alt Primary Number is NOT available
            note over UI
            use api_2025_getAlerts to get primary number not available alert text
            end note
            group IAM ProfileId is available
                UI-> UI: Display message "Your previous Primary Number is no longer valid. \nPlease select a new one so we’ll know where to contact you.\n And show option to EDIT"
            end
            group IAM ProfileId is Partial or NOT available
                UI-> UI: Display message "In order to change your primary number, \nplease register a T-Mobile ID on my.t-mobile.com"
            end
        end
    end

    alt Web Only or For Assisted Channel where API_2007 returns ProfileID - Get Security Questions
        note over UI
        IAM profile returns
        - Customer Profile Info
        - Customer Chosen Security Questions (Question Id)
        - Linked Numbers
        end note
        UI -> GW : API_2055_getCustomerSecurityProfile(profileId)
        GW -> ES : API_2055_getCustomerSecurityProfile(profileId)
        ES -> IAM : QueryByIAMProfile (profileId)
        ES <-- IAM : return IAM Profile Info (Linked Numbers, Security Question ID & encrypted answer)
        GW <-- ES : return IAM Profile Info (Linked Numbers, Security Question ID & encrypted answer)
        UI <-- GW : return IAM Profile Info (Linked Numbers, Security Question ID & encrypted answer)
        UI -> UI : Render **Language Preference** section

        UI -> GW : API_2054_getSecurityQuestions
        GW -> ES : API_2054_getSecurityQuestions
        ES -> IAM : querySecurityQuestions
        ES <-- IAM : return List of Security Questions (Id, Text, Active, etc)
        GW <-- ES : return List of Security Questions (Id, Text, Active, etc)
        UI <-- GW : return List of Security Questions (Id, Text, Active, etc)
        UI -> UI : Render **Security Questions** section
    end


    group  Update Profile Information

    User -> UI : User edited profile details \nand clicks **Save** button

    note over UI
    All IAM API calls require UID.
    **UID**:[**email**:abc@gmail.com | **userName**:xyz | **userId**:U-123 | **customerNumber**:C123]
    Generally any one of UID attribute is enough but some cases require specific attribute.
    See the profile attribute section below for more info.
    end note
        UI -> GW : API_2053_updateCustomerProfile(profileDetails)
        GW -> ES : API_2053_updateCustomerProfile(profileDetails)

        group Customer Name, Email
            ES -> WSIL: ContactWSIL.updateContact(BscsId, FirstName, LastName)
                group E/// Process
                    WSIL -> OM : SubmitOrder(OrderType: UpdateContact)
                    WSIL <-- OM :
                    alt Update Name
                        OM -> BSCS : Update Name()
                        OM <-- BSCS :
                        OM -> EMA: OM Provision Name Update to IAM()
                        EMA -> IAM : Provision Name
                        EMA <-- IAM :
                        OM <-- EMA:
                    end
                    alt Update Email
                        OM -> BSCS : Update Email()
                        OM <-- BSCS :
                        OM -> EMA: OM Provision Email Update to IAM()
                        EMA -> IAM : Provision Email
                        EMA <-- IAM :
                        OM <-- EMA:
                    end
                end
            ES <-- WSIL: return Order Id
        end

        group Customer Nick Name
            ES -> WSIL: ContactWSIL.updateContact(BscsId, profileDetails)
            ES <-- WSIL: response
        end

        group Primary Number
            ES -> IAM: setPrimaryMSISDN(UID:userId, MSISDN)
            ES <-- IAM: response
        end

        group Language Preference - Customer Profile Level
        ES -> IAM: updateAnIAMProfile(UID:<any>, languagePreference)
        ES <-- IAM: response
        end

        alt CR 726.1 UC 8 - Language Preference at FAN/Line Level
            group Language Preference - FAN Level
            ES -> WSIL: FinancialAccountWSIL.updateFinancialAccount(BscsId, languagePreference)
            ES <-- WSIL: response
            end

            group Language Preference - Line Level
            ES -> WSIL: LineOfServiceUpdateWSIL.updateLineOfService(BscsId, languagePreference)
            ES <-- WSIL: response
            end
        end

        group Security Question/Answer
        ES -> IAM: updateAnIAMProfile(UID:userId, SecurityQuestion/Answers)
        ES <-- IAM: response
        end


        group PIN
            alt This call is required until CR 726.1 UC17 is complete
                note right ES
                    CR 726.1 UC17 - PIN Management in IAM
                    BSCS no longer stores Customer PIN
                end note
                ES -> WSIL: ContactWSIL.updateContact(BscsId, encryptedPIN)
                ES <-- WSIL: response
            end
        ES -> IAM: updateLoginProfile(UID:userId, encryptedPIN)
        ES <-- IAM: response
        end

        group Password
        ES -> IAM: updateLoginProfilePassword(UID:userId, profilePassword)
        ES <-- IAM: response
        end
        GW <-- ES : return response
        UI <-- GW : return response
    end
end

group Privacy Settings - Insights
    User -> UI : Click the blade **Notifications & Privacy Center,**\nthen **Your privacy settings**

    group Get privacy settings
        alt IAM profile is Partial or not available
            UI -> UI : Disable T-Mobile Insights input controls
            UI -> UI: Display message "In order to change your Insights preference, \nplease register a T-Mobile ID on my.t-mobile.com"
        end

        UI -> GW : api_2059_getInsightOptn(UID:userId)
        GW -> ES : api_2059_getInsightOptn(UID:userId)
        ES -> IAM : Query Offers (UID:userId)
        ES <-- IAM : return offerId: Insights, preferenceName: opt-in/opt-out)
        GW <-- ES : return True/False
        UI <-- GW : return True/False
        UI -> UI : Render **Insights** options

    end

    group Update Insights Preference
        User -> UI : User selects **Opt-In** or **Opt-Out** ans clicks **Save**
        UI -> GW : api_2060_saveInsightOptn(UID:userId, opt-in/opt-out)
        GW -> ES : api_2060_saveInsightOptn(UID:userId, opt-in/opt-out)
        ES -> IAM : Update Offers (UID:userId, OfferId=Insights, opt-in/opt-out)
        ES <-- IAM : return response
        GW <-- ES : return response
        UI <-- GW : return response
    end

end

group Segmentation
    User -> UI : Click the blade **Segmentation**

    group Page Load - Get Lookup Values for the Drop down
        UI -> GW: API_2021_getLookupValues (type=segmentationReference)
        GW -> ES: API_2021_getLookupValues (type=segmentationReference)
        ES -> WSIL: CustomerDataWSIL.getCustomerDomainReference
        WSIL -> BSCS : Channel Segmentation
        WSIL <-- BSCS: response
        ES <-- WSIL: Return reference data
        GW <-- ES: Return reference data
        UI <-- GW: Return reference data
        UI -> UI: Populate drop down controls
    end
    group Display Customer Segmentation Info
        UI -> UI : Display Customer Segmentation info from \n **API_2065_getCustomerDetails** cache
        alt IF Cache information is not available
            UI -> GW: API_2065_getCustomerDetails (BscsId)
            GW -> ES: API_2065_getCustomerDetails (BscsId)
            ES -> WSIL: CustomerDetailsWSIL.getCustomerDetails (BscsId)
            ES <-- WSIL: Return Customer Details
            GW <-- ES: Return Customer Details
            UI <-- GW: Return Customer Details
        end
    end
    User -> UI : Click **Edit** to change Segmentation information, change values\nclick **Save**

    group Save Segmentation Attribute Changes
            UI -> GW: API_2069_updateCustomerSegmentation(BscsId, SegmentationAttributes)
            GW -> ES: API_2069_updateCustomerSegmentation(BscsId, SegmentationAttributes)
            ES -> WSIL: CustomerUpdateWSIL.updateCustomer(BscsId, SegmentationAttributes)
            WSIL -> OM : submitOrder(BscsId, SegmentationAttributes)
            OM -> BSCS : updateCustomer(BscsId, SegmentationAttributes)
            OM <-- BSCS : return response
            WSIL <-- OM : return orderId and orderStatus
            ES <-- WSIL: return orderId and orderStatus
            GW <-- ES: return orderId and orderStatus
            UI <-- GW: return orderId and orderStatus
    end
end

@enduml
