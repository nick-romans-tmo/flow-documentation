@startuml
title <size:18>__U2.0 Unification Cart Payment - Retail __</size>
actor Rep
participant Rep as Rep
participant Rebellion as ui
participant Ingenico as ingenico
participant "Desktop drawer" as drawer
participant "API Gateway" as proxy
participant ES as es
participant Cache as cache
participant ESP as esp
participant DPS as DPS
participant WSIL as wsil
participant Vesta as vesta

ui->proxy : API_2006_customer billing summary \n (customerSummaryWSIL.getAggregatedCustomerInfo)
proxy->es : API_2006_customer billing summary \n (customerSummaryWSIL.getAggregatedCustomerInfo)
es->wsil : API_2006_customer billing summary \n (customerSummaryWSIL.getAggregatedCustomerInfo)
wsil->es :response API_2006_customer billing summary \n (customerSummaryWSIL.getAggregatedCustomerInfo)
es->proxy : response API_2006_customer billing summary \n (customerSummaryWSIL.getAggregatedCustomerInfo)
proxy->ui : response API_2006_customer billing summary \n (customerSummaryWSIL.getAggregatedCustomerInfo)

ui->proxy : API_2615_getAccountFundingOptions
proxy->es : API_2615_getAccountFundingOptions
es-> wsil : getPaymentOptions (PaymentWSIL.getEligiblePaymentMethods \n PaymentWSIL.getSoftgoodFundingOptions)
wsil-->es : Response getPaymentOptions (PaymentWSIL.getEligiblePaymentMethods \n PaymentWSIL.getSoftgoodFundingOptions)
es-->proxy : Response API_2615_getAccountFundingOptions
proxy-->ui : Response API_2615_getAccountFundingOptions

alt existing balance as funding options
    ui -> proxy : API_2607_updateExistingBalance
    proxy -> es : API_2607_updateExistingBalance
    alt If Cart info is Cached
        es -> cache : get latest WSIL cart response
        cache --> es : response with latest WSIL cart response
    end
    es -> wsil : updateCart
    wsil --> es : response
    es -> cache : store updated WSIL cart response to cache
    es --> proxy : Response API_2607_updateExistingBalance
    proxy --> ui : Response API_2607_updateExistingBalance
end

alt refill card (ePin or voucher) as funding options
    Rep -> ui : ** rep enters voucher details **
    ui -> proxy : API_2621_postValidatePrepaidRefillCard
    proxy -> es : API_2621_postValidatePrepaidRefillCard
    es -> wsil : VoucherWSIL.checkVoucherEligibility \n VoucherWSIL.getVoucherDetails \n VoucherWSIL.validateActivationCode
    wsil --> es : Response (VoucherWSIL.checkVoucherEligibility \n VoucherWSIL.getVoucherDetails \n VoucherWSIL.validateActivationCode)
    es --> proxy : Response API_2621_postValidatePrepaidRefillCard
    proxy --> ui : Response API_2621_postValidatePrepaidRefillCard
    ui --> ui : Enables ** "Apply" ** button on success response
    ui --> ui : UI displays added voucher with a ** "Remove" ** clickable text

    alt rep clicks on ** "Apply" ** or ** "Remove" **
        ui -> es : API_2608_updateRefillCard
        es -> cache : get latest WSIL cart response from cache
        cache --> es : Response latest WSIL cart response from cache
        es -> wsil : updateCart
        wsil --> es : response
        es -> cache : store updated WSIL cart response to cache
        es --> proxy : Response API_2608_updateRefillCard
        proxy --> ui : Response API_2608_updateRefillCard
    end
end

alt add extra amount (pay extra)
    Rep -> ui : rep enters extra amount (>= $1 AND <=$300)
    ui -> proxy : API_2609_updatePayExtraAmount
    proxy -> es : API_2609_updatePayExtraAmount
    alt If productOfferingId is not sent in request
        es -> es : API_2100_retrieveProducts to get D2A sku and offeringId
        es -> es : Response API_2100_retrieveProducts
    end
    es -> cache : get latest WSIL cart response from cache
    cache --> es : Response with latest WSIL cart response from cache
    es -> wsil : updateCart
    wsil --> es : Response
    es -> cache : store updated WSIL cart response to cache
    es --> proxy : Response API_2609_updatePayExtraAmount
    proxy --> ui : Response API_2609_updatePayExtraAmount
end

note over ui: Rep Selects the payment method
note over ui: For Split-Payment repeat the below payment flow till the due amount is $00.00\n For split tender Due balance is calculated by the UI

alt if REP selects ** Card ** Credit / Debit
    ui->ingenico : ui call java Script to Ingenico
    ingenico->ingenico : Customer swipes the card
    ingenico --> ui : card details

    alt P2P off
        alt If Legacy (non-digital) Store
        note over vesta : Vesta
        ui-> vesta : java script lib for encryption
        vesta --> ui : Response Temporary Token generated
        end

        alt If Digital Store
        note over DPS : DPS
                ui->proxy : API_2612_postKeyManager
                proxy ->DPS :API_2612_postKeyManager
                DPS --> proxy : Response API_2612_postKeyManager
                proxy-->ui : Response EncryptionKey
         end
   end

    ui->proxy : API_2614_postValidatePaymentMethod
    proxy ->es : API_2614_postValidatePaymentMethod
    es->DPS : API_2614_postValidatePaymentMethod
    DPS --> es : Response API_2614_postValidatePaymentMethod
    es-->proxy : Response API_2614_postValidatePaymentMethod
    proxy-->ui : Response Card Validation Result
    note over ingenico: If card is invalid: "Prompt User to swipe/insert a new card"

    alt Debit card
        note over ingenico : payment device request to provide customer pin
        note over ingenico : amount will be shown to customer on payment device
        ingenico->ingenico : customer clicks ok and enters PIN
        ingenico->ui : PIN details
        ui->proxy : API_2150_submitOrder(Payment details)
        proxy ->es : API_2150_submitOrder(Payment details)
        es->wsil : CartWSIL.createCheckout(Payment Details)
        wsil-->es : Response CartWSIL.createCheckout(Payment Details)
        es-->proxy : Response API_2150_submitOrder(Payment details)
        proxy-->ui : Response API_2150_submitOrder(Payment details)

        alt  If pin is invalid, customer can retry not greater than 3 times
            note over ingenico : payment device request to provide customer pin
            note over ingenico : amount will be shown to customer on payment device
            ingenico->ingenico : customer clicks OK button
            ingenico->ui : PIN details
            ui->proxy : API_2150_submitOrder(Payment details)
            proxy ->es : API_2150_submitOrder(Payment details)
            es->wsil : CartWSIL.createCheckout(Payment Details)
            wsil-->es : Response CartWSIL.createCheckout(Payment Details)
            es-->proxy : Response API_2150_submitOrder(Payment details)
            proxy-->ui : Response API_2150_submitOrder(Payment details)
        end
    end

    alt Credit card
        note over ingenico : Displays Signature page for Tablet/ ingenco Displays for desktop
        note over ingenico : customer signs & agree
        ingenico->ui: java script lib
        note over ui:Signature is displayed on ui and rep clicks on "Accept Signature"
         ui->proxy : API_2150_submitOrder(Payment details)
         proxy ->es : API_2150_submitOrder(Payment details)
         es->wsil : CartWSIL.createCheckout(Payment Details)
         wsil-->es : Response CartWSIL.createCheckout(Payment Details)
         es-->proxy : Response API_2150_submitOrder(Payment details)
         proxy-->ui : Response API_2150_submitOrder(Payment details)

        alt if Partial Authorization
        ui->ingenico: Transaction is Partially authorized
            alt customer accepts partial authorization
            ingenico->ingenico :Capture the signature
            ingenico->ui: send the details to UI
            ui->proxy: API_2150_SubmitOrder(Partial_Auth indicator,paymentId, paySig)
            proxy ->es : API_2150_SubmitOrder(Partial_Auth indicator,paymentId, paySig)
            es->wsil : CartWSIL.createCheckout(Payment Details)
            wsil-->es : Response CartWSIL.createCheckout(Payment Details)
            es-->proxy : Response API_2150_submitOrder(Partial_Auth indicator,paymentId, paySig)
            proxy-->ui : Response API_2150_submitOrder(Partial_Auth indicator,paymentId, paySig)

            note over ui : UI will calculate the $ amount due difference
            else Customer Declines
            ingenico ->ingenico : Customer Declines the partial Payments
            ingenico->ui : Send decline.
            note over ui : UI Calls API_2150 to remove the partially authorized tender
            ui->proxy : API_2150_submitOrder(Payment Details)
            proxy ->es : API_2150_submitOrder(Payment Details)
            es->wsil : CartWSIL.createCheckout(Payment Details)
            wsil-->es : Response CartWSIL.createCheckout(Payment Details)
            es-->proxy : Response API_2150_submitOrder(Partial_Auth indicator,paymentId, paySig)
            proxy-->ui : Response API_2150_submitOrder(Partial_Auth indicator,paymentId, paySig)
            end
        end
    end
end

alt cash
Rep->ui : Enter $ amount (Cash)
Rep-> ui: Select Tender type as ** Cash ** and click Add Tender
ui -->ui : Displays change due
ui ->ui : Rep clicks ** Place order **
    alt Desktop
    ui->proxy : API_2701_getNetworkCashdrawer
    proxy-> es : API_2701_getNetworkCashdrawer
    es->esp : getNetworkCashdrawer
    esp->es : response
    es->ui : Networkdrawer/Physical drawer
        alt Physical drawer
        note over ui : uses ingenco Lib to open cash drawer
        else Network cash drawer
        ui->proxy : API_2702_postOpenCashdrawer
        proxy -> es : API_2702_postOpenCashdrawer
        es->esp : postOpenCashdrawer
        esp->es : Response
        es->ui : Response
        ui-->ui : Open cash drawer
        end
        note over ui : Rep collects the cash and closes drawer
    end

    alt  Tablet
        alt Generate Bar Code
        ui->ui : Generates the barcode
        ui ->proxy : API_2704_postStoreBarcode(barcode,actioncode=create)
        proxy ->es : API_2704_postStoreBarcode(barcode,actioncode=create)
        es ->cache : cache the barcode with status=="NEW"
        es-->proxy : Response API_2704_postStoreBarcode(barcode,actioncode=create)
        proxy -->ui : Response API_2704_postStoreBarcode(barcode,actioncode=create)
        drawer ->drawer : Scans the tablet barcode using Desktop scanner

            alt Cash collected button
                drawer -> es : API_2704_postStoreBarcode(barcode,actionCode=Update_cash,Term,Till)
                es->es : Update Cache with Status =="Cash collected"
                es -->drawer : Response API_2704_postStoreBarcode(barcode,actionCode=Update_cash,Term,Till)
                else : cash Cancel button
                drawer ->es : postStoreBarcode(barcode,actionCode=Update_cancel,Term,Till)
                es->es : Update Cache with Status =="NEW"
                es -->drawer : Response
           end

        drawer ->es : API_2704_postStoreBarcode(barcode,actionCode=validate)
        es->es : Validates the barcode and updates cache status =="validated"
        es-->drawer : Response API_2704_postStoreBarcode(barcode,actionCode=validate)

        note over drawer: validation Successful
        drawer->proxy : API_2701_getNetworkCashDrawer
        proxy-> es : API_2701_getNetworkCashDrawer
        es->esp : getNetworkCashDrawer
        esp-->es : Response API_2701_getNetworkCashDrawer
        es-->proxy : Response API_2701_getNetworkCashDrawer
        proxy -->drawer : Response API_2701_getNetworkCashDrawer

        alt if Physical drawer
        note over ui: Use Ingenico Lib to open cash drawer
            else Network cash drawer
            drawer->proxy :API_2702_postOpenCashDrawer
            proxy -> es : API_2702_postOpenCashDrawer
            es->esp : postOpenCashDrawer
            esp-->es : Response API_2702_postOpenCashDrawer
            es-->proxy : Response API_2702_postOpenCashDrawer
            proxy-->drawer : Open cash drawer
        end

        ui->es : API_2705_fetchPhysicalCashDrawerDetails(barcode,actionCode=Retrieve_cache)
        es-->ui : Desktop Terminal Num, DesktopTill, Status

        alt status == "New"
        ui-> ui : Error Message--"Please scan the barcode and collect Cash"

        else status=="validated"
        ui->ui: Error Message- Please collect the cash and try again

        else status == "Cash collected"
        ui->proxy : API_2150_submitOrder(Payment details)
        proxy ->es : API_2150_submitOrder(Payment details)
        es->wsil : CartWSIL.createCheckout(Payment Details)
        wsil-->es : Response CartWSIL.createCheckout(Payment Details)
        es-->proxy : Response API_2150_submitOrder(Payment details)
        proxy-->ui : Response API_2150_submitOrder(Payment details)

        else Status == "Blank or Barcode"
        ui->proxy : API_2705_fetchPhysicalCashDrawerDetails(storeCode,actionCode=Retrieve_POS)
        proxy-> es : API_2705_fetchPhysicalCashDrawerDetails(storeCode,actionCode=Retrieve_POS)
        es->esp : getAvailPhysicalDeskDrawer
        esp-->es : physical drawer terminal Num, Till(getAvailPhysicalDeskDrawer)
        es-->proxy : Response physical drawer terminal Num, Till
        proxy --> ui : physical drawer terminal Num, Till

        Note over ui: From the drop down, Rep selects the drawer, cash is collected

        ui->proxy : API_2150_submitOrder(Payment details)
        proxy ->es : API_2150_submitOrder(Payment details)
        es->wsil : CartWSIL.createCheckout(Payment Details)
        wsil-->es : Response CartWSIL.createCheckout(Payment Details)
        es-->proxy : Response API_2150_submitOrder(Payment details)
        proxy-->ui : Response API_2150_submitOrder(Payment details)
        end
     end

        alt  Network drawer
        ui->ui : Scan the Network barcode from the tablet internally connected to Till
        ui->proxy : API_2702_postOpenCashDrawer
        proxy-> es : API_2702_postOpenCashDrawer
        es->esp : postOpenCashdrawer
        esp->es : Response API_2702_postOpenCashDrawer
        es--> proxy : Response API_2702_postOpenCashDrawer
        proxy-->ui : open cash drawer

        ui->proxy : API_2150_submitOrder(Payment details)
        proxy ->es : API_2150_submitOrder(Payment details)
        es->wsil : CartWSIL.createCheckout(Payment Details)
        wsil-->es : Response CartWSIL.createCheckout(Payment Details)
        es-->proxy : Response API_2150_submitOrder(Payment details)
        proxy-->ui : Response API_2150_submitOrder(Payment details)
        end
    end
end
note over ui:If tender is debit/credit and Fraud Check = "Reject" then prompt the customer to use new card

ui-> proxy : API_2806_termsAndConditions
proxy -> es : API_2806_termsAndConditions
es -> wsil: CustomerDocumentWSIL.getDocumentReference
wsil --> es : return T&C
es --> proxy : return T&C
proxy --> ui : return T&C
ui --> ingenico : Display Terms & Conditions

ui->proxy : API_2150_submitOrder(Payment details, confirmation flag)
proxy ->es : API_2150_submitOrder(Payment details, confirmation flag)
es->wsil : CartWSIL.createCheckout(Payment details, confirmation flag)
wsil-->es : Response CartWSIL.createCheckout(Payment Details)
es-->proxy : Response API_2150_submitOrder(Payment details)
proxy-->ui : Response API_2150_submitOrder(Payment details)
@enduml