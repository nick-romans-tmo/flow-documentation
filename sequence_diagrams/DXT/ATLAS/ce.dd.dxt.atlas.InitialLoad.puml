@startuml
title <size:18>__Atlas Initial Load__</size>
actor "Frontline Rep" as Rep
participant "Browser - Repdash UI" as UI
participant "Browser - OKTA UI" as OKTAUI
participant "Repdash Server" as RD
participant "S3/CDN" as CDN
participant "API Gateway" as GW
participant "Rebellion Experience Services" as ES
participant "OKTA API GW" as OKTA
participant ESP
participant OES
participant WSIL

Rep -> UI : User requests Launchpad URL
UI -> RD : Request Repdash UI
alt User Login
    OKTAUI <-- RD : OKTA SDK checks user authentication, \nredirects to OKTA Login Page
    OKTAUI -> OKTA : Submit Login credentials
    UI <-- OKTA : Login verified, redirect to Repdash UI with Auth Code
    UI -> RD : Request Repdash UI with Auth Code
end

group Rep Profile and Permissions
    RD -> OKTA : Validate auth code, request JWT Token
    RD <-- OKTA : OKTA JWT Token
    RD -> GW : API_998_postRepProfile(OKTA JWT Token)
    GW -> ES : API_998_postRepProfile(OKTA JWT Token)
    ES -> OKTA : Validate OKTA JWT Token
    ES <-- OKTA : Validation Success
    ES -> ESP : UserSecurityEnterprise.getUserDetails(repNTId, [MACID, Device ID])
    ES <-- ESP : Return Rep Profile (name, dealer code, storeId, channelSegmentation
    note over ES
        If multiple roles are returned, use first role
        to get entitlements
    end note
    ES -> ESP : UserSecurityEnterprise.getUserEntitlements(userName, userRoles[0])
    ESP -> OES : getUserEntitlements(userName, userRoles[0])
    ESP <-- OES : Return ABFs (entitlements)
    ES <-- ESP : Return ABFs (entitlements)

    ES -> ES : Create JWT token with embedded Entitlements
    GW <-- ES : Return RepProfile with JWT, OKTA JWT
end group

GW -> GW : Register JWT as OAuth Bearer Token
RD <-- GW : Return RepProfile with JWT

alt Rep has multiple roles
    UI <-- RD : Respond with Role Selector Page
    UI -> UI : Show Role Selector page
    Rep -> UI : User selects a Role and clicks **Continue**
    UI -> RD : Request UI with OKTA JWT, selectedRole

    group Rep Profile and Permissions
        RD -> GW : API_998_postRepProfile(OKTA JWT, selectedRole)
        GW -> ES : API_998_postRepProfile(OKTA JWT, selectedRole)
        ES -> ES : Verify selectedRole (Compare with Rep Roles from JWT)
        ES -> ESP : UserSecurityEnterprise.getUserDetails(repNTId, [MACID, Device ID])
        ES <-- ESP : Return Rep Profile (name, dealer code, storeId, channelSegmentation
        note over ES
            Rep had selected a role
        end note
        ES -> ESP : UserSecurityEnterprise.getUserEntitlements(userName, selectedRole)
        ESP -> OES : getUserEntitlements(userName, selectedRole)
        ESP <-- OES : Return ABFs (entitlements)
        ES <-- ESP : Return ABFs (entitlements)
        ES -> ES : Create JWT token with embedded Entitlements
        GW <-- ES : Return RepProfile with JWT
    end group

    GW -> GW : Register JWT as OAuth Bearer Token
    RD <-- GW : Return RepProfile with JWT
end

alt For U2 OES Roles Only
    RD -> CDN: Get Rebellion MicroApp Care Index HTML
    RD <-- CDN:  <<html>>
end
RD -> RD : HTML Overlay \n(RepDash HTML +  [Rebellion MicroApp Care Index HTML] +  \n Rep Profile, JWT, OKTA JWT and Permissions)
UI <-- RD : Repdash w/ MicroApp HTML
note over UI
    For Legacy Rep Roles (U1), Show Splash Page instead
end note

UI -> UI : State Store Initialized, JWT and OKTA JWT set to State Store
UI -> RD : Request Repdash UI dependencies (js, css, etc)
RD -> CDN : Request Repdash UI dependencies (js, css, etc)
RD <-- CDN: <<files>>
UI <-- RD: <<files>>
Rep <-- UI : <<Repdash Guest Account Experience>>

group Startup Data Load
    group Channel=Care, Permission=FOLLOW_UPS
        UI -> GW : API_2222_getFollowUpAndAdjustmentCount(transaction Code: ZTFO, ZTCR, ZTAD)
        GW -> ES : API_2222_getFollowUpAndAdjustmentCount(transaction Code: ZTFO, ZTCR, ZTAD)
        ES -> WSIL : CaseWSIL.searchCaseDocuments(transaction Code: ZTFO, ZTCR, ZTAD)
        ES <-- WSIL : <<case summary>>
        GW <-- ES:  <<case summary>>
        UI <-- GW:  <<case summary>>
        UI -> UI : Display Followups Total
    end

    UI -> GW : API_2021_getRepDashLookupCriteria(type=customer)
    GW -> ES : API_2021_getRepDashLookupCriteria(type=customer)
    ES -> AEM : get[Care/Retail]RepDashLookupCriteria.config.json
    ES <-- AEM : <<lookup criteria>>
    GW <-- ES:  <<lookup criteria>>
    UI <-- GW:  <<lookup criteria>>
    UI -> UI : Binding lookup criteria data

    UI -> GW : API_2021_getRepDashLookupCriteria(type=u1memotemplates,u1memotypes)
    GW -> ES : API_2021_getRepDashLookupCriteria(type=u1memotemplates,u1memotypes)
    ES -> AEM : memoTemplates.content.json, memoTypes.content.json
    ES <-- AEM : <<memo types, template>>
    GW <-- ES:  <<memo types, template>>
    UI <-- GW:  <<memo types, template>>
    UI -> UI : Bind memo types, template data
end
@enduml