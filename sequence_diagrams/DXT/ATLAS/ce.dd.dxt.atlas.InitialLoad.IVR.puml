@startuml
title <size:18>__Atlas Initial Load (IVR Flow)__</size>
actor "Customer" as Cust
actor "Frontline Rep" as Rep
participant IVR
participant ICM
participant "Context Store" as cStore
participant Avaya
participant HAS
participant "One-X Agent Station" as 1x

participant "Browser" as UI
participant "Repdash Server" as RD
participant "S3/CDN" as CDN
participant "API Gateway" as GW
participant "Rebellion Experience Services" as ES
participant "OKTA API GW" as OKTA
participant ESP
participant RSP
participant TIBCO
participant OES
participant CHUB
participant WSIL

Cust -> IVR : Customer is routed to IVR
IVR -> ICM: Send standard call variables
ICM -> cStore: Update QueueVDN, VDN Label, \nLanguagePreference, SkillGroup
ICM -> Avaya: Choose skill and deliver call data
Avaya -> 1x: Call reaches agent in skill group
HAS -> 1x : HAS sees the call and grabs 16-digit uuid
HAS -> UI : Launch Atlas (Param: uuid)
UI -> RD : Request Atlas UI \n(Param: uuid)
alt User Login
    UI <-- RD : OKTA SDK checks user authentication, \nredirects to OKTA Login Page
    UI -> UI: Displays Login UI
    UI -> OKTA : Submit Login credentials
    UI <-- OKTA : Login verified, redirect to Repdash UI with Auth Code
    UI -> RD : Request Repdash UI with Auth Code \n(Customer Context)
end

group Rep Profile and Permissions
    RD -> OKTA : Validate auth code, request JWT Token
    RD <-- OKTA : OKTA JWT Token
    RD -> GW : API_998_postRepProfile(OKTA JWT Token)
    GW -> ES : API_998_postRepProfile(OKTA JWT Token)
    ES -> OKTA : Validate OKTA JWT Token
    ES <-- OKTA : Validation Success
    note over ES
        We no longer use OKTA JWT Token after this point
        The JWT created in ES, with entitlements will be used as OAuth bearer token
    end note
    ES -> ESP : UserSecurityEnterprise.getUserDetails(repNTId, [MACID, Device ID])
    ES <-- ESP : Return Rep Profile (name, dealer code, storeId, channelSegmentation
    note over ES
        If multiple roles are returned, use first role
        to get entitlements
    end note
    ES -> ESP : UserSecurityEnterprise.getUserEntitlements(userName, userRoles[0])
    ESP -> OES : getUserEntitlements(userName, userRoles[0])
    ESP <-- OES : Return ABFs (entitlements)
    ES <-- ESP : Return ABFs (entitlements)

    ES -> ES : Create JWT token with embedded Entitlements
    GW <-- ES : Return RepProfile with JWT
end group

GW -> GW : Register JWT as OAuth Bearer Token
RD <-- GW : Return RepProfile with JWT

alt Rep has multiple roles
    UI <-- RD : Respond with Role Selector Page
    UI -> UI : Show Role Selector page
    Rep -> UI : User selects a Role and clicks **Continue**
    UI -> RD : Request UI with oamAuthCookie, selectedRole

    group Rep Profile and Permissions
        RD -> GW : API_998_postRepProfile(JWT, selectedRole)
        GW -> ES : API_998_postRepProfile(JWT, selectedRole)
        ES -> ES : Verify selectedRole (Compare with Rep Roles from JWT)
        ES -> ESP : UserSecurityEnterprise.getUserDetails(repNTId, [MACID, Device ID])
        ES <-- ESP : Return Rep Profile (name, dealer code, storeId, channelSegmentation
        note over ES
            Rep had selected a role
        end note
        ES -> ESP : UserSecurityEnterprise.getUserEntitlements(userName, selectedRole)
        ESP -> OES : getUserEntitlements(userName, selectedRole)
        ESP <-- OES : Return ABFs (entitlements)
        ES <-- ESP : Return ABFs (entitlements)
        ES -> ES : Create JWT token with embedded Entitlements
        GW <-- ES : Return RepProfile with JWT
    end group

    GW -> GW : Register JWT as OAuth Bearer Token
    RD <-- GW : Return RepProfile with JWT
end

group Get Avaya Context
    RD -> GW : API_2014_getAvayaContext(uuid)
    GW -> ES : API_2014_getAvayaContext(uuid)
    ES -> cStore: Get Avaya Context (uuid)
    ES <-- cStore: <avaya context response>
    ES -> RSP: GetReferenceData
    ES <-- RSP: <reference data>
    ES -> RSP: CustomerInteractionService.getHistory(MSISDN, NumberOfDays)
    ES <-- RSP: <call log data>
    GW <-- ES : Return Avaya Context Response
    RD <-- GW : Return Avaya Context Response
end

group Customer Search
    RD -> GW : API_2005_customerSummarySearch (msisdn)
    GW -> ES : API_2005_customerSummarySearch (msisdn)
    ES -> CHUB : CustomerSummarySearch (msisdn)
    ES <-- CHUB : <Search Results>
    GW <-- ES : <Search Results>
    RD <-- GW : <Search Results>
    RD -> RD : Select first item from the result
end

group Get Authorized Users
    RD -> GW : api_2064_getAuthorizedUsers(accountNumber, billingType, billingSystem, msisdn)
    GW -> ES : api_2064_getAuthorizedUsers(accountNumber, billingType, billingSystem, msisdn)
    alt if billingSystem is Ericsson
        ES -> WSIL: CustomerSummaryWSIL.getAccessibleParties(accountNumber)
        ES <-- WSIL: <getAccessibleParties response>
        ES -> ESP: CustomerNoteEnterprise.getCustomerNote(accountNumber)
        ES <-- ESP: <getCustomerNote response>
    else if billingType is Samson
        ES -> RSP : UserInfoService.getUserInfo(userId, systemIdsToRetrieve="Samson")
        ES <-- RSP : User info from Samson
        ES -> Cache : Read & Cache systemUserId, firstName & lastName

        alt if BioPushFeatureEnabled
            ES -> RSP: PostPaidSubscriberService.getSubscribersInfo(accountNumber, msisdn, flags)
            ES <-- RSP : subscriber role [PAH or FULL]
            group subscriber role is PAH or FULL
                ES -> BRASS: CheckPushEligibility(msisdn)
                ES <-- BRASS : eligibilityStatus
            end group
        end
    end

    ES -> RSP: PostPaidAccountService.getAccountVerificationData(accountNumber, includeSpecialInstruction = true)
    ES <-- RSP : account detail and authorised user info

    note over ES
        Create footprint memo
    end note
    ES -> RSP: MemoService.CreateMemo(accountNumber, code=CFPM, manualMemoText, systemMemoText, userId)
    ES <-- RSP : response

    ES -> RSP : EnterpriseToken.generateToken(accountNumber, msisdn, customerVerificationStatus = NotVerified)
    ES <-- RSP : token

    ES -> ES : Build Authorised User Detail Response
    GW <-- ES : <Authorized users + special instruction>
    RD <-- GW : <Authorized users + special instruction>
end group

group For U2 OES Roles Only
    RD -> CDN: Get Rebellion MicroApp Care Index HTML
    RD <-- CDN:  <html>
end

RD -> RD : HTML Overlay \n(RepDash HTML + \n[Rebellion MicroApp Care Index HTML] + \nRep Profile, JWT and Permissions + \n**Customer Verification Screen** )
UI <-- RD : Repdash Index HTML w/ **Customer Verification Screen**

note over UI
    For Legacy Rep Roles (U1), Show Splash Page instead
end note

UI -> UI : State Store Initialized, Rep JWT set to State Store
UI -> RD : Request Repdash UI dependencies (js, css, etc)
RD -> CDN : Request Repdash UI dependencies (js, css, etc)
RD <-- CDN: <files>
UI <-- RD: <files>
Rep <-- UI : <Repdash Guest Account Experience>

@enduml