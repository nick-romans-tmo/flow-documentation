@startuml
actor "Administrators"
box "Console and Data Service" #Magenta
  boundary "Admin Console"
  boundary "Admin Settings Service"
  database "Admin Settings DynamoDB"
  collections "Admin Settings Change Notifications Kafka Topic"
end box
entity "Nearly every SMPD Service"
control "Supervisor-type Services"
entity "File Upload Service"

group Administrator Check Page/Section Access (ACA)
  group Get UI JSON
    "Admin Console" --> "Admin Settings Service" : GET /settings/_interface
    "Admin Settings Service" --> "Admin Settings DynamoDB" : Get data
    "Admin Settings DynamoDB" --> "Admin Settings Service" : Interface JSON
    "Admin Settings Service" --> "Admin Console" : Interface JSON
  end
  loop Get settings for any white-list group that has access to the settings being updated
    "Admin Console" --> "Admin Settings Service" : GET /settings/{appName}
    "Admin Settings Service" --> "Admin Settings DynamoDB" : Get data
    "Admin Settings DynamoDB" --> "Admin Settings Service" : Corresponding JSON
    "Admin Settings Service" --> "Admin Console" : Corresponding JSON
  end
end

group Save Settings Subroutine (SSS)
  group Save Settings
    "Admin Console" --> "Admin Settings DynamoDB" : Save data
    "Admin Settings DynamoDB" --> "Admin Console" : Success message
  end
  group Notify SMPD Services
    "Admin Console" --> "Admin Settings Change Notifications Kafka Topic" : Settings update
    "Admin Settings Change Notifications Kafka Topic" --> "Nearly every SMPD Service" : Settings update
  end
end

group Administrator Actions
  group Access UI Page
    "Administrators" --> "Admin Console" : GET /console-{pagePath} (NT-Login)
    "Admin Console" --> "Admin Console" : Perform ACA on page
    "Admin Console" --> "Administrators" : UI HTML
    loop Get settings for each page section
      "Admin Console" --> "Admin Settings Service" : GET /settings/{appName}
      "Admin Settings Service" --> "Admin Settings DynamoDB" : Get data
      "Admin Settings DynamoDB" --> "Admin Settings Service" : Corresponding JSON
      "Admin Settings Service" --> "Admin Console" : Corresponding JSON
      "Admin Console" --> "Administrators" : Data and features presented in UI
    end
  end
  group Save Section
    "Administrators" --> "Admin Console" : PUT /settings/{appName} (Settings JSON)
    "Admin Console" --> "Admin Console" : Perform ACA on page/section
    "Admin Console" --> "Admin Console" : Perform SSS on section
    "Admin Console" --> "Administrators" : Success message
  end
  group Upload File
    "Administrators" --> "Admin Console" : POST /upload/{appName} (Settings JSON)
    "Admin Console" --> "Admin Console" : Perform ACA on page/section
    "Admin Console" --> "Admin Console" : Create UUID for Upload ID
    group Send File to File Upload Service
      "Admin Console" --> "File Upload Service" : File data and upload ID
      "File Upload Service" --> "Admin Console" : Upload started message
      "Admin Console" --> "Administrators" : Started message (upload ID)
    end
    loop Admin Console front-end checks upload status according to upload field settings
      "Admin Console" --> "Admin Console" : GET /upload/{appName}/{uploadId}
      "Admin Console" --> "File Upload Service" : GET /upload/status/{uploadId}
      "File Upload Service" --> "Admin Console" : File upload status information
      "Admin Console" --> "Administrators" : File upload status information
    end
  end
end

group Supervisor Services Update Settings
  "Supervisor-type Services" --> "Admin Console" : POST /oauth/settings/{appName} (Settings JSON + OAuth1 Header)
  group Check OAuth Header in Main JSON
    "Admin Console" --> "Admin Settings Service" : GET /settings/msg-admin-console
    "Admin Settings Service" --> "Admin Settings DynamoDB" : Get data
    "Admin Settings DynamoDB" --> "Admin Settings Service" : Settings JSON
    "Admin Settings Service" --> "Admin Console" : Settings JSON
    "Admin Console" --> "Admin Console" : Check OAuth1 header against access list
  end
  "Admin Console" --> "Admin Console" : Perform SSS on section
  "Admin Console" --> "Supervisor-type Services" : Success message
end
@enduml