@startuml
title
<font color=red size="50"><u:black>NFS Sequence Diagram</u></font>
end title

skinparam SequenceMessageAlign center

participant "API Consumer" as consumer
participant "APIGEE GATEWAY" as gateway
participant "PCF API (orders-finance-submit)" as API
participant "CHUB" as CCID
participant "createLoans" as CL

consumer->gateway: post-orders - \nClient Request/Authentication

alt if transcationsubtype=LOAN
gateway->API: PostOrders:URL:/ordermanagement/v4/orders/

alt fulfillmentType=SHIP
        API -> API: validate ship to fullfilment
	alt if shipTo validation success and chubCCID is null
		API -> CCID : call searchCHUBforCCID
                alt ccid not found
                   CCID -> API: ccid not found
                   API -> gateway: error response message
                   gateway -> consumer : error response
                else
                  CCID -> API : ccid in response
                end               
	end 	
end
API -> API : Process Request
	API -> CL : createLoans \n(OFS - oracle finance system)
        CL -> API : ofs createloans response
        API -> gateway : Response from PCF API
end
gateway->consumer: Response to calling client
@enduml