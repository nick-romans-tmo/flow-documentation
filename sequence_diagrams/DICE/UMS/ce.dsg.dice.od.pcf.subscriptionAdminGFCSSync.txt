@startuml
title order management post subscription API flow
participant "Api Consumer PEL" as UI
participant "APIGEE Gateway" as gateway
participant "Subscription Order Preprocessor" as sop
participant "Rabbit MQ Exchange queue" as MQ
participant "subscription order data access" as Api
database "Gemfire" as GF
participant "GF CS Admin Sync" as gc
database "Cassandra" as Cassandra

UI -> gateway : (POST) ordermanagement/v1/subscription-orders
gateway -> gateway : validate OAuth JWT token
gateway -> sop : (POST) ordermanagement/v1/subscription-orders
sop -> MQ : (POST) Rabbit MQ Exchange Queue

Api -> MQ : Listen from Rabbit MQ Exchange Queue
Api -> GF : Persist order segmentation
Api -> GF : Persist order payload
gc -> GF : Take Persited payload
gc -> Cassandra : Persist into cassandra
@enduml