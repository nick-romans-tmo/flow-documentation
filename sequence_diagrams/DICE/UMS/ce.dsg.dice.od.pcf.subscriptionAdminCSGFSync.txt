@startuml
title order management post subscription API flow
participant "Api Consumer PEL" as UI
participant "APIGEE Gateway" as gateway
participant "Subscription Order Preprocessor" as sop
participant "Rabbit MQ Exchange queue" as MQ
participant "subscription order data access" as Api
database "Cassandra" as Cassandra
participant "CS GF Admin Sync" as gc
database "Gemfire" as GF

UI -> gateway : (POST) ordermanagement/v1/subscription-orders
gateway -> gateway : validate OAuth JWT token
gateway -> sop : (POST) ordermanagement/v1/subscription-orders
sop -> MQ : (POST) Rabbit MQ Exchange Queue

Api -> MQ : Listen from Rabbit MQ Exchange Queue
Api -> Cassandra : Persist order segmentation
Api -> Cassandra : Persist order payload
gc -> Cassandra : Take Persited payload
gc -> GF : Persist into gemfire
@enduml