@startuml

title ** __Customer Profile (CPD) Service getCustomerProfileDetails Operation__ **\n

actor TIBCO

actor RSP

TIBCO->CustomerProfileService: getCustomerProfileDetails Request (XML over Http) [[https://servicecatalog.internal.t-mobile.com/servicecat/cp/2016-12-14/wsdl_api/html/CustomerProfileService.html Service Description]]

RSP->CustomerProfileService: getCustomerProfileDetails Request (XML over Http) [[https://servicecatalog.internal.t-mobile.com/servicecat/cp/2016-12-14/wsdl_api/html/CustomerProfileService.html Service Description]]

CustomerProfileService->profileManager: load method

profileManager-->profileManager: getProfileIdAndSource method

profileManager-->profileManager: if serviceType==NONCUSTOMER set source= "TXN"

profileManager-->profileManager: if processName==NULL and ban/msisdn found in profileQueue set source="QUEUE"

profileManager-->profileManager: else if account level then 

profileManager–>customerProfileDao: lookupAccount (Ban) set source="TXN"

customerProfileDao-–>profileManager: lookupAccount result

profileManager-->profileManager: else if subscriber level then

profileManager–>customerProfileDao: getSubscriber (msisdn) set source="TXN"

customerProfileDao-–>profileManager: getSubscriber result

profileManager-->profileManager: if source==null set default attributes to response

profileManager->contactManager: getCustomerAttributes (load customer attributes)

contactManager-->profileManager: return customer attributes

profileManager->attributeManager: getDefaultVisibleAttributes (based on app, channel and attribute level)

attributeManager-->profileManager: return default visible attributes

profileManager->contactManager: getCustomerEmail (load customer emails)

contactManager-->profileManager: return customer Emails

profileManager->contactManager: getCustomerPhone (load customer phones)

contactManager-->profileManager: return customer phones

profileManager->contactManager: getCustomerAddress (load customer Addresses)

contactManager-->profileManager: return customer addresses

profileManager->contactManager: getCustomerOfferCode (load customer Offers)

contactManager-->profileManager: return customer offers

profileManager-->profileManager: map to customerProfileDetails response

profileManager-->CustomerProfileService: return customerProfileDetails response

CustomerProfileService-->RSP: return customerProfileDetails response

CustomerProfileService-->TIBCO: return customerProfileDetails response

@enduml