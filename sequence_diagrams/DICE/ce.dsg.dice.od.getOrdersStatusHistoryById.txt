@startuml
Actor Client
Client-> OM_API_GetOrdersStatusById : GET /ordermanagement/v4/orders/{orderid}/status
database OrderRepository

OM_API_GetOrdersStatusById -> OM_API_GetOrdersStatusById : Validate OAuth
OM_API_GetOrdersStatusById-> OrderRepository: orderID
OrderRepository-> OM_API_GetOrdersStatusById: order payload with latest status and status history
OM_API_GetOrdersStatusById ->Client : Order Status payload
@enduml
