@startuml
Actor Client
Client-> OM_API_GetShipmentTracker : GET /ordermanagement/v1/shipping-history?tracking-number={tracking-number1, tracking-number2}
OM_API_GetShipmentTracker -> OM_API_GetShipmentTracker : Validate OAuth

OM_API_GetShipmentTracker-> UPS: trackingNumber
UPS-> OM_API_GetShipmentTracker : return order shipment tracking details


alt Request is based on order-id
Client-> OM_API_GetShipmentTracker : GET /ordermanagement/v1/shipping-history?order-id={orderid}
OM_API_GetShipmentTracker-> OrderRepository: orderID
OrderRepository-> OM_API_GetShipmentTracker : return order shipment tracking details
end
database OrderRepository
actor UPS
@enduml