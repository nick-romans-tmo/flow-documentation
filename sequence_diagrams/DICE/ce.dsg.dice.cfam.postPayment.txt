@startuml
Client->CapabilityAPI:[[http://stash.internal.t-mobile.com:7990/projects/TCE/repos/finance/browse/serviceLoan/Service%20Descriptors/customerfinance.LoanRest-Installment.json#13,15,114,116,183 Swagger]]post-payment-Update EIP
CapabilityAPI->TIBCO_CE: TIBCOEIPProcessPayment-Update EIP
TIBCO_CE->EIP: InstallmentHistory.getInstallmentHistory-get Installment History
EIP-->TIBCO_CE: InstallmentHistory.getInstallmentHistory-get Installment History
alt payment already exists error
TIBCO_CE->EIP: InstallmentHistory.getInstallmentHistory-get Installment History
EIP-->TIBCO_CE: InstallmentHistory.getInstallmentHistory-get Installment History
end
TIBCO_CE->EIP: InstallmentHistory.getInstallmentHistory-Update Payment details
EIP-->TIBCO_CE: InstallmentHistory.getInstallmentHistory-get Installment History
TIBCO_CE->EIP: Installment.makePayment-Update Payment details
EIP-->TIBCO_CE: Installment.makePayment-Update Payment details
TIBCO_CE-->CapabilityAPI: Update Payment details
CapabilityAPI-->Client: Update Payment details
@enduml