@startuml
group Customer domain-Retrieve Customer Information
DCP->Capability_MS: [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.cust Custom Domain]]Query Customer data-POST/lines/search
Capability_MS->Customer_Data_Grid_Cassandra:Query Customer data 
alt data not available in cassandra
Capability_MS->CHUB: Retrieve customer data-CustomerAccountLookUpService
end
Customer_Data_Grid_Cassandra->Capability_MS: Retrieve customer data
Capability_MS-->DCP: Customer data-Ban details, Billing method, Address, Msisdn information
end
@enduml
