@startuml
title ** __DSPA Selfcare PCF: ResetCustomerPassword__ **\n

actor Client #Magenta

participant SelfCareServiceWSController
participant SelfCareService
participant SelfCareUtil
participant IAMManager
participant IAMDao
participant LdapService
participant IAM_LDAP #DarkSeaGreen
queue PUBLISH_TOPIC #Magenta
participant Provisioning as "<b><color:white>Provisioning" #Magenta

skinparam sequenceGroupBorderColor blue 
group#Magenta <b><color:white> resetCustomerPassword 

Client -> SelfCareServiceWSController : <font color=blue> resetCustomerPassword [[https://servicecatalog.internal.t-mobile.com/servicecat/dspa/2016-10-14/wsdl_api/wsdl/SelfCareService.wsdl WSDL]]\n (JAX-RPC XML Request over Http)

note left
Clients: TIBCO_ESOA, MyTmo, SIVR, StreamLine
URI: /webservice/SelfCareService
endnote

alt try
SelfCareServiceWSController->SelfCareService: <font color=blue><b>resetCustomerPassword(msisdn,customerType,billingZipCode,applicationId) 

alt customerType is null
SelfCareService-> SelfCareService: lookupCustomerType(msisdn,null): returns customerType


SelfCareService-> SelfCareUtil: validateMsisdn(msisdn)
alt msisdn doesn't match msisdnPattern
SelfCareUtil-> SelfCareService : throw InvalidRequestException
end
SelfCareService-> SelfCareUtil: validateServicePartnerIdValue(servicePartnerId)
alt servicePartnerId is not MKB or TMO
SelfCareUtil-> SelfCareService : throw InvalidRequestException
end
SelfCareService-> SelfCareService : getProfileCommon(msisdn, servicePartnerId, null)
SelfCareService-> IAMManager: getProfile(msisdn)
IAMManager-> IAMDao: getProfile(msisdn)
IAMDao-> LdapService : ldapSearch(searchCriteria, filterAttributes, searchControls)
note left: with msisdn in searchCriteria
LdapService -> IAM_LDAP : search(rootSuffix, searchCriteria, searchControls, handler)
IAM_LDAP -> LdapService : SearchResult List
LdapService-> IAMDao : SearchResult List

alt searchResult is Empty
IAMDao->IAMManager: throws CustomerNotFoundException
IAMManager->SelfCareService: throws CustomerNotFoundException
SelfCareService->SelfCareServiceWSController : throws NoSuchMsisdnException
else byPassNoAttributesException disabled, filterAttributes length greater than 0 and serachResult size is 0
IAMDao->SelfCareServiceWSController : throws IAMException 
end

alt searchResultList size greater than 1 for a msisdn
IAMDao->IAMDao : Log Error
note left : "More than one object found for search criteria"
end
IAMDao--> IAMManager : return customerProfile
IAMManager--> SelfCareService : return customerProfile
alt customerType is not PREPAID

group#DarkSeaGreen <b>Topic: TMOBILE.<env>.DSPA.TOPIC.PUBLISH, event: 'setMLType' and MsgType: 'Publish' 
SelfCareService[#blue]--> PUBLISH_TOPIC : [[../../provisioningems/ce.dsg.dice.prov.ems.pcf.publishtopic.setmltype.svg Provisioning_AsncFlow_setMLType]]
end

alt error during posting event
SelfCareService -> SelfCareServiceWSController : throws Exception
end

end
alt servicePartnerId is not null 
SelfCareService-> SelfCareUtil: isValidServicePartnerId(accType,accSubType,servicePartnerId, isMkbTMOFilterFlagEnabled) 
SelfCareUtil--> SelfCareService: return isValidServicePartnerId flag
alt isValidServicePartnerId is false
SelfCareService -> SelfCareServiceWSController : throw NoSuchMsisdnException
end
else
SelfCareService-> SelfCareUtil: isValidServicePartnerId(accType,accSubType,isMkbTMOFilterFlagEnabled) 
note left
TMO is assumed as partnerId
endnote
SelfCareUtil--> SelfCareService: return isValidServicePartnerId flag
alt isValidServicePartnerId is false
SelfCareService -> SelfCareServiceWSController : throw NoSuchMsisdnException
end
end
SelfCareService --> SelfCareService : getProfileCommon() returns CustomerProfile
SelfCareService --> SelfCareService : lookupCustomerType() returns customerType from CustomerProfile

else
SelfCareService-> SelfCareUtil: validateRequest(msisdn,customerType)
alt msisdn doesn't match msisdnPattern
SelfCareUtil-> SelfCareService : throw InvalidRequestException
end
alt customerType not in (POSTPAID,TAKECONTROL,PREPAID)
SelfCareUtil-> SelfCareService : throw InvalidRequestException
else customerType  is PREPAID and isPagActive is false
SelfCareUtil-> SelfCareService : throw InvalidRequestException
end
end

SelfCareService-> SelfCareService: resetCustomerPasswordCommon(msisdn, msisdnToSendPwd, billingZipCode, applicationId)

note left
if applicationId is null, then applicationId is set to SELFCARE
endnote

SelfCareService-> IAMManager: getCustomerProfileDetails(msisdn)
IAMManager-> IAMDao: getCustomerProfileDetails(msisdn)
IAMDao-> LdapService : ldapSearch(searchCriteria, filterAttributes, searchControls)
note left: with msisdn in searchCriteria
LdapService -> IAM_LDAP : search(rootSuffix, searchCriteria, searchControls, handler)
IAM_LDAP --> LdapService : SearchResult List
LdapService-> IAMDao : SearchResult List

alt searchResult is Empty
IAMDao->IAMManager: throws CustomerNotFoundException
IAMManager->SelfCareService: throws CustomerNotFoundException
SelfCareService->SelfCareServiceWSController : throws NoSuchMsisdnException
else byPassNoAttributesException disabled, filterAttributes length greater than 0 and serachResult size is 0
IAMDao->SelfCareServiceWSController : throws IAMException 
end

alt searchResultList size greater than 1 for a msisdn
IAMDao->IAMDao : Log Error
note left : "More than one object found for search criteria"
end
IAMDao--> IAMManager : return custProfileDetails
IAMManager--> SelfCareService : return custProfileDetails
alt custProfileDetails.getAccountStatus() is A
SelfCareService -> SelfCareServiceWSController: throws InvalidCustomerStatusException
end

alt custProfileDetails.getRegistrationStatus() is N
SelfCareService -> SelfCareServiceWSController: throws InvalidCustomerStatusException
end

alt custProfileDetails.getLoginStatus() is DISABLED
SelfCareService -> SelfCareServiceWSController: throws InSufficientPrivilegeException
end

alt billingZipCode not null and billingZipCode is not custProfileDetails.getZipCode()
SelfCareService -> SelfCareServiceWSController: throws InvalidZipCodeException
end

note over SelfCareService
hasPasswordResetLimit is true when resetPwdCount is not 0 and >=  
passwordResetRequestLimit and initPasswordResetDate is not null and 
initPasswordResetDate <= passwordResetTimeLimitInMillis
endnote

note over SelfCareService
isPasswordResetWhitelist returns false if clientId is null,
returns true if clientId is not in passwordResetWhiteList
endnote

alt msisdnToSendPwd is not null and hasPasswordResetLimit and !isPasswordResetWhitelist
SelfCareService -> SelfCareServiceWSController: throws PasswordResetException
end


note over SelfCareService 
if initPasswordResetDate is null or currentTime-passwordResetTime > passwordResetTimeLimit, 
then set custProfileDetails.tempPasswordResetCount to 1
otherwise Increment custProfileDetails.tempPasswordResetCount by one
endnote

SelfCareService -> SelfCareUtil: generateTemporaryPassword()
SelfCareUtil--> SelfCareService: returns tempPassword

SelfCareService -> SelfCareUtil: getEncryptedPassword(tempPassword)
SelfCareUtil--> SelfCareService: returns encryptedPassword

alt msisdnToSendPwd is not null
SelfCareService -> SelfCareUtil: constructDSPMessage("passwordReset")
SelfCareUtil--> SelfCareService: returns dspMessage
note right
Set customerProfile to dspMessage
endnote

group#DarkSeaGreen <b>Topic: TMOBILE.<env>.DSPA.TOPIC.PUBLISH, event: 'passwordReset' and MsgType: 'Publish' 
SelfCareService[#blue]--> PUBLISH_TOPIC : [[../../provisioningems/ce.dsg.dice.prov.ems.pcf.publishtopic.passwordreset.svg Provisioning_AsncFlow_passwordReset]]
end

alt error during posting event
SelfCareService -> SelfCareServiceWSController : throws Exception
end
end

note left
if initPasswordResetDate is not added, set it to current date
endnote

SelfCareService-> IAMManager: setPassword(msisdn,password,passwordTimeStamp,mustChangePwd as false)
IAMManager-> IAMDao: setPassword(msisdn,password,passwordTimeStamp,mustChangePwd)
IAMDao-> LdapService: ldapSearch(searchCriteria, filterAttributes, searchControls)
note left: with msisdn in searchCriteria
LdapService -> IAM_LDAP : search(rootSuffix, searchCriteria, searchControls, handler)
IAM_LDAP -> LdapService : SearchResult List
LdapService-> IAMDao : SearchResult List

alt searchResult is Empty
IAMDao->IAMManager: throws CustomerNotFoundException
IAMManager->SelfCareService: throws CustomerNotFoundException
SelfCareService->SelfCareServiceWSController : throws NoSuchMsisdnException
else byPassNoAttributesException disabled, filterAttributes length greater than 0 and serachResult size is 0
IAMDao->SelfCareServiceWSController : throws IAMException 
end

alt searchResultList size greater than 1 for a msisdn
IAMDao->IAMDao : Log Error
note left : "More than one object found for search criteria"
end
IAMDao-> LdapService:ldapUpdate(dn,modificationItems) updating **tmuserpassword,tmmustchangepassword,tminitpwresettime**
LdapService->IAM: modifyAttributes(dn,modificationItems)
IAM--> LdapService: return void
LdapService-->IAMDao: return void
IAMDao-->IAMManager: return void
IAMManager-->SelfCareService: return void


SelfCareService-> IAMManager: unlockCustomerForPasswordReset(custProfileDetails)
IAMManager-> IAMDao: unlockCustomerForPasswordReset(custProfileDetails)
IAMDao-> LdapService: ldapSearch(searchCriteria, filterAttributes, searchControls)
note left: with msisdn in searchCriteria
LdapService -> IAM_LDAP : search(rootSuffix, searchCriteria, searchControls, handler)
IAM_LDAP -> LdapService : SearchResult List
LdapService-> IAMDao : SearchResult List

alt searchResult is Empty
IAMDao->IAMManager: throws CustomerNotFoundException
IAMManager->SelfCareService: throws CustomerNotFoundException
SelfCareService->SelfCareServiceWSController : throws NoSuchMsisdnException
else byPassNoAttributesException disabled, filterAttributes length greater than 0 and serachResult size is 0
IAMDao->SelfCareServiceWSController : throws IAMException 
end

alt searchResultList size greater than 1 for a msisdn
IAMDao->IAMDao : Log Error
note left : "More than one object found for search criteria"
end

IAMDao-> LdapService:ldapUpdate(dn,modificationItems) updating **customerProfile**
LdapService->IAM_LDAP: modifyAttributes(dn,modificationItems)
IAM_LDAP-> LdapService: return void
LdapService-->IAMDao: return void
IAMDao-->IAMManager: return void
IAMManager-->SelfCareService: return void

alt msisdnToSendPwd is not null
SelfCareService[#blue]-->Provisioning: send password to the customer\n[[../../provisioning/ce.dsg.dice.prov.provisioning.pcf.soap.sendsms.svg Provisioning_SendSMS]] 
Provisioning->SelfCareService: return response(true/false)
else
alt application is MYCOP
SelfCareService[#blue]-->Provisioning: send password to the customer with contentId=254\n[[../../provisioning/ce.dsg.dice.prov.provisioning.pcf.soap.sendsms.svg Provisioning_SendSMS]]
Provisioning->SelfCareService: return response(true/false)
else
SelfCareService[#blue]-->Provisioning: send password to the customer\n[[../../provisioning/ce.dsg.dice.prov.provisioning.pcf.soap.sendsms.svg Provisioning_SendSMS]] 
Provisioning->SelfCareService: return response(true/false)
end
end
else
alt custProfileDetails.getAccountSubType() is I4 or I5
SelfCareService -> SelfCareUtil: getPrefBillingLang(ban)
SelfCareUtil--> SelfCareService : returns prefBillLang
note left
if prefBillLang is sp, then prefBillLang is set to ms
otherwise prefBillLang is set to mk
endnote
SelfCareService -> SelfCareUtil:sendTemporaryPasswordMKB(msisdn, tempPassword, prefBillLang,contentId as "")
alt languageCode is ms
SelfCareService[#blue]-->Provisioning: send password to the customer\n[[../../provisioning/ce.dsg.dice.prov.provisioning.pcf.soap.sendsms.svg Provisioning_SendSMS]]
Provisioning->SelfCareService: return response(true/false)

else
SelfCareService[#blue]-->Provisioning: send password to the customer\n[[../../provisioning/ce.dsg.dice.prov.provisioning.pcf.soap.sendsms.svg Provisioning_SendSMS]]
Provisioning->SelfCareService: return response(true/false)
end

else
SelfCareService[#blue]-->Provisioning: send password to the customer\n[[../../provisioning/ce.dsg.dice.prov.provisioning.pcf.soap.sendsms.svg Provisioning_SendSMS]] 
Provisioning->SelfCareService: return response(true/false)
end

alt sendSuccessful is false
SelfCareService -> SelfCareServiceWSController: throw IAMException
end
SelfCareService --[#green]> SelfCareServiceWSController: return void
SelfCareServiceWSController--[#green]> Client : return void 

else 
SelfCareServiceWSController-> Client : throws InvalidRequestException
else
SelfCareServiceWSController-> Client : throws InvalidCustomerStatusException
else
SelfCareServiceWSController-> Client : throws InSufficientPrivilegeException
else
SelfCareServiceWSController-> Client : throws InvalidZipCodeException
else
SelfCareServiceWSController-> Client : throws PasswordResetException
else
SelfCareServiceWSController-> Client : throws NoSuchMsisdnException
else
SelfCareServiceWSController-> Client : throws SelfCareSystemException
end
end
@enduml