@startuml
Client->PCF:[[http://stash.internal.t-mobile.com:7990/projects/DM/repos/devices_tce/browse/deviceService/Service%20Descriptors -Provide IMEI handset data]]post-devices-search
PCF->TIBCO_CE: deviceWarrantyLookupOrch-Provide handset  data
TIBCO_CE->TIBCO: QueryIMEIWarrantyPortType.queryImeiWarranty-Provide handset  data
TIBCO->SAP_HANA: HANA.DEVICE.QUERYIMEIWARRANTY to get device details
SAP_HANA-->TIBCO: get device details
alt DMW serialization 
TIBCO->DMW: SERIALIZATION.SUBSCRIBER.IMEIWARRANTY.CSI.QUERY-Provides warranty details
DMW-->TIBCO: Response with HandsetInfo-Provides warranty details
end
alt apple IMEI 
TIBCO->GSX: Apple warranty details
GSX-->TIBCO: Apple warranty details
end
TIBCO-->TIBCO_CE: QueryIMEIWarrantyPortType.queryImeiWarranty-Provide handset  data
TIBCO_CE-->PCF: deviceWarrantyLookupOrch-Provide handset  data
PCF-->Client: post-devices-search-Provide IMEI handset data
@enduml