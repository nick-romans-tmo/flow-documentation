@startuml
group Trade in domain-Customer creates the Order
Client->CapabilityAPI:[[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dm.td CreateQuote]]post/tradeInQuotes 
CapabilityAPI->QuoteRespository:post/tradeInQuotes 
QuoteRespository->RSP:Account eligibility check 
RSP->Samson:evGtCustInfo, evGtBanHdr Account eligibility
RSP-->QuoteRespository:Account details 
QuoteRespository-->QuoteRespository:Account eligibility check 
alt Serial number is provided in the request Device eligibility check
QuoteRespository->QuoteRespository:Lunh Check
QuoteRespository->RSP:(Device details-LookUpEquipment)
RSP->SAP_PI:(TMO_RSP_SI_Blocking_Status_Check_Outbound_Sync Block/Unblock eligibility)
SAP_PI-->RSP:(TMO_RSP_SI_Blocking_Status_Check_Outbound_Sync Block/Unblock eligibility)
RSP->EIR:Retrieve IMSI
EIR-->RSP:IMSI retrieved to validate IMEI in next steps
RSP->SAP_HANA:Retrieve IMEI status
SAP_HANA-->RSP:Retrieved IMEI 
opt data not available in Hana
RSP->DMW:Retrieve IMEI status
DMW-->RSP:Retrieved IMEI 
RSP->HLR:Validate IMEI 
HLR-->RSP: Validate IMEI 
RSP->SAMSON:csSrchSub, evGtSubSum, evGbEipInv and evGbEipChgCrd-Balance history 
SAMSON-->RSP:Balance history 
RSP->EIP:-getInstallmentHistory Balance history 
EIP-->RSP:getInstallmentHistory Balance history 
RSP-->QuoteRespository:Device eligibility details 
QuoteRespository->HSO:Call for Warranty details
HSO-->QuoteRespository:Warranty Summary information
QuoteRespository-->QuoteRespository:Warranty eligibility validation
QuoteRespository-->Client:Warranty eligibility
QuoteRespository-->QuoteRespository:Device eligibility check-Based on data retrieved from RSP
end
end
QuoteRespository-->QuoteRespository:Subscriber device is eligible for trade in
QuoteRespository->Assurant:GetDevicePrice
Assurant-->QuoteRespository:GetDevicePrice
QuoteRespository-->CapabilityAPI: (Provide a device offer estimate)
CapabilityAPI-->Client: (Provide a device offer estimate)
end
@enduml