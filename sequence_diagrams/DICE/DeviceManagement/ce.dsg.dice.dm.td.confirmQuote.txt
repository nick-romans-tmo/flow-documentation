@startuml
group Trade In Domain-Customer submits the Order
Client->QuoteRespository: [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dm.td Confirm Quote]]API PUT-TradeInQuotes
QuoteRespository->Assurant: setAcceptOffer
Assurant-->QuoteRespository: setAcceptOffer
QuoteRespository-->Client: API Put-TradeInQuotes-Response
end
@enduml

