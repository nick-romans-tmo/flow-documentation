@startuml
participant DEVICE_TRADE_IN_MASTER
participant DR_TRADE_IN_EXCEPTIONS
participant DR_NON_EVALUATION_EVENTS
participant DR_EVALUATION_EVENTS
participant STG_DR_LOAD_EVENT_PROCESS
database QuoteRepository
QuoteRepository-> STG_DR_LOAD_EVENT_PROCESS: [[https://bitbucket.service.edp.t-mobile.com/projects/DM/repos/database/browse/ORACLE_DB_SCRIPTS/event_file_process.sql]] DB batch job process file and load to staging table
STG_DR_LOAD_EVENT_PROCESS -> DR_NON_EVALUATION_EVENTS :DB ETL validates and loads Event 0 for Exception Flow
DR_NON_EVALUATION_EVENTS-> DR_TRADE_IN_EXCEPTIONS: ETL processes exception records for customer decision
alt if aging date > current date
DR_NON_EVALUATION_EVENTS-> DEVICE_TRADE_IN_MASTER: Updates TradeInSubStatus as CUSTOMER_RESPONSE_PENDING
end
alt if customer accepts exception
STG_DR_LOAD_EVENT_PROCESS -> DR_EVALUATION_EVENTS: Assurant Sends Event 1 for final Bill Credit
DR_EVALUATION_EVENTS-> DEVICE_TRADE_IN_MASTER : ETL Process Event 1 for BillCredit and EIP adjustments
end
alt if customer din't decide until aging date
STG_DR_LOAD_EVENT_PROCESS -> DR_EVALUATION_EVENTS: Assurant Sends Event 1 as Auto Accept
DR_EVALUATION_EVENTS -> DEVICE_TRADE_IN_MASTER : ETL Process Event 1 for BillCredit and EIP adjustments
end
@enduml