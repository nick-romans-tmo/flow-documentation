@startuml
participant DEVICE_TRADE_IN_MASTER
participant DR_EVALUATION_EVENTS
participant STG_DR_LOAD_EVENT_PROCESS
database QuoteRepository
QuoteRepository-> STG_DR_LOAD_EVENT_PROCESS: [[https://bitbucket.service.edp.t-mobile.com/projects/DM/repos/database/browse/ORACLE_DB_SCRIPTS/event_file_process.sql]]AssuarntEventFile dbjob processes the file and load to QR staging table
STG_DR_LOAD_EVENT_PROCESS -> DR_EVALUATION_EVENTS: DB ETL validates and loads Event 1 Data 
DR_EVALUATION_EVENTS-> DEVICE_TRADE_IN_MASTER : ETL Process Event 1 for final BillCredit and EIP adjustments
alt if trade-in substatus=TRADE_IN_INITIATED
DR_EVALUATION_EVENTS-> DEVICE_TRADE_IN_MASTER: Updates TradeInSubStatus as ASSURANT_RECEIVED_DEVICE
end
alt if trade-in substatus=CUSTOMER_RESPONSE_PENDING
DR_EVALUATION_EVENTS-> DEVICE_TRADE_IN_MASTER: Updates TradeInSubStatus as CUSTOMER_ACCEPTED_EXCEPTION
end
alt if offer status=ACCEPTED
DR_EVALUATION_EVENTS-> DEVICE_TRADE_IN_MASTER: Updates TradeInSubStatus as BILL_CREDIT_POSTED
end
alt if offer already processed 
DR_EVALUATION_EVENTS-> DR_EVALUATION_EVENTS: Data stays in Evaluation table and wont get re-processed
end
alt if Assurant send Bulk Transaction
STG_DR_LOAD_EVENT_PROCESS -> DR_EVALUATION_EVENTS: Loads the Bulk Transactions with Event type as 1
DR_EVALUATION_EVENTS-> DEVICE_TRADE_IN_MASTER :Inserts the new bulk offers to Master table for further processing
end
@enduml