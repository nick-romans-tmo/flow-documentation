@startuml
group Trade In Domain-Customer submits the Order
Client->OMS:SubmitOrder
OMS->PCF_orders_oms_tradein_handler:Child Order
PCF_orders_oms_tradein_handler->QuoteRespository: [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dm.td Confirm Quote]]API setDisposition
QuoteRespository->Assurant: setAcceptOffer
Assurant-->QuoteRespository: setAcceptOffer
QuoteRespository-->PCF_orders_oms_tradein_handler: API setDisposition
opt EIP
OMS->PCF_orders_oms_tradein_handler:Loan Child Order
PCF_orders_oms_tradein_handler->EIP:Tibco createInstallmentPlan
PCF_orders_oms_tradein_handler->OMS:Approved Status (oms-eer-events updates GF also)
opt OffLine Loan Disclosure
RSP->PCF_orders_events_document_status:DEEP.io ESignCompleted
PCF_orders_events_document_status->OMS:ESIG_SIGNED Status (oms-eer-events updates GF also)
alt ESig Timeout  After 96 Hrs
RSP->PCF_orders_events_document_status:DEEP.io ESignCancelled
PCF_orders_events_document_status->OMS:ESIG_CANCELLED Status (oms-eer-events updates GF also)
OMS->PCF_orders_oms_tradein_handler:ChildOrder Cancel Trade In
PCF_orders_oms_tradein_handler->QuoteRespository:API setDisposition
QuoteRespository->Assurant: setRejectOffer
Assurant-->QuoteRespository: setRejectOffer
QuoteRespository-->PCF_orders_oms_tradein_handler: API setDisposition
OMS->OMS:Cancel customer order
end
end 
end
OMS->PCF_orders_events_publisher:
PCF_orders_events_publisher->DEEP.io:Produce OrderCreated event
DEEP.io->QuoteRespository:Consume OrderCreated event
QuoteRespository->TIBCO_REEF:Post Associate Qualify event
TIBCO_REEF-->QuoteRespository:Post Associate Qualify event
QuoteRespository->QuoteRespository:Associate Qualify event in QR DB 
alt Product is not qualified
QuoteRespository->TIBCO_REEF:Post Log OptOut event
TIBCO_REEF-->QuoteRespository:Post Log OptOut event
QuoteRespository->QuoteRespository: Log OptOut event in QR DB 
end
DEEP.io->Notification_Gateway:Order Confirmation EMail / SMS
DEEP.io->QuoteRespository:Pub/Sub
alt Backorder Flow
SAP->TIBCO_EER:CreateOrderEvent Backordered
TIBCO_EER->PCF_orders_eer_events:EMS
PCF_orders_eer_events->OMS:BackOrder Status (oms-eer-events updates GF also)
OMS->PCF_orders_events_publisher:
PCF_orders_events_publisher->DEEP.io:OrderBackOrdered event
end
alt Cancellation Flow
SAP->TIBCO_EER:CreateOrderEvent OrderCancelled
TIBCO_EER->PCF_orders_eer_events:EMS
PCF_orders_eer_events->OMS:Cancel Status (oms-eer-events updates GF also)
OMS->PCF_orders_oms_tradein_handler:ChildOrder Cancel Trade In
PCF_orders_oms_tradein_handler->QuoteRespository:API setDisposition
QuoteRespository->Assurant: setRejectOffer
Assurant-->QuoteRespository: setRejectOffer
QuoteRespository-->PCF_orders_oms_tradein_handler: API setDisposition
OMS->OMS:Cancel customer order
OMS->PCF_orders_events_publisher:
PCF_orders_events_publisher->DEEP.io:OrderCancelled event
end
alt CC Payment Failed Flow
SAP->TIBCO_EER:CreateOrderEvent OrderOnHold
TIBCO_EER->PCF_orders_eer_events:EMS
PCF_orders_eer_events->OMS:OrderOnHold Status (oms-eer-events updates GF also)
OMS->PCF_orders_events_publisher:
PCF_orders_events_publisher->DEEP.io:OrderOnHold event
end
SAP->UPS:via Tibco to Warehouse
SAP->TIBCO_EER:CreateOrderEvent OrderShipped
TIBCO_EER->PCF_orders_eer_events:EMS
TIBCO_EER->QuoteRespository:RMAExpiration Updated
QuoteRespository->Assurant:Set_RMAExpiration
Assurant-->QuoteRespository:Set_RMAExpiration
QuoteRespository->PCF_orders_events_tradein:DEEP.io RMAExpirationUpdate
PCF_orders_events_tradein->OMS:RMA date update
Assurant->QuoteRespository:Event 1-DeviceReceived event file
QuoteRespository->QuoteRespository:Update QR Master table as DeviceReceived 
QuoteRespository->Notification_EMAIL_DST
QuoteRespository->Notification_SMS_INFORM
QuoteRespository->PCF_orders_events_tradein:DEEP.io AssurantDeviceReceived
PCF_orders_events_tradein->OMS:ASSURANT_RECEIVED_DEVICE Status
alt Device assessment returned with exception
Assurant->QuoteRespository:Event 0-DeviceReceived with exception event file
QuoteRespository->QuoteRespository:Update QR Master table as DeviceReceived with exception
QuoteRespository->Client:Update details to clients with getTradeInDetails
QuoteRespository->Notification_EMAIL_DST
QuoteRespository->Notification_SMS_INFORM
opt Customer had rejected the exception
Client->QuoteRespository:updateCustomerDecision
QuoteRespository->Assurant:setRMADecision
Assurant->QuoteRespository:setRMADecision
Assurant->QuoteRespository:Event 10-DeviceReturnReceived event file
QuoteRespository->QuoteRespository:Update DB tables with event
QuoteRespository->Client:Update tracking details to clients with getTradeInDetails
QuoteRespository->Notification_EMAIL_DST
QuoteRespository->Notification_SMS_INFORM
opt Customer had accepted the exception
Client->QuoteRespository:updateCustomerDecision
QuoteRespository->Assurant:setRMADecision
Assurant->QuoteRespository:setRMADecision
Assurant->QuoteRespository:Event 1-DeviceReceived event file
QuoteRespository->QuoteRespository:Update DB tables with event
QuoteRespository->Client:Update acceptance details to clients with getTradeInDetails
QuoteRespository->Notification_EMAIL_DST
QuoteRespository->Notification_SMS_INFORM
QuoteRespository->QuoteRespository:Review the customer preferences
opt Customer had preferred to adjust towards EIP
QuoteRespository->EIP:InstallmentAccountPayment.makePayments
EIP-->QuoteRespository:InstallmentAccountPayment.makePayment
QuoteRespository->QuoteRespository:Update the Master table with settlement amount 
QuoteRespository->Notification_EMAIL_DST
QuoteRespository->Notification_SMS_INFORM
QuoteRespository->QuoteRespository:Review the customer preferences
end
opt Customer had preferred to adjust to bill credit
QuoteRespository->SAMSON:saveBanAdjustments
SAMSON-->QuoteRespository:saveBanAdjustments
QuoteRespository->QuoteRespository:Update the Master table with settlement amount 
QuoteRespository->Notification_EMAIL_DST
QuoteRespository->Notification_SMS_INFORM
QuoteRespository->QuoteRespository:Review the customer preferences
end
QuoteRespository->PCF_orders_events_tradein:DEEP.io BillCreditCompleted
PCF_orders_events_tradein->OMS:BILL_CREDIT_POSTED Status
end
end
end
QuoteRespository->Notification_EMAIL_DST
QuoteRespository->Notification_SMS_INFORM
OMS->PCF_orders_events_publisher:Customer Order Completed
PCF_orders_events_publisher->DEEP.io:OrderCompleted event
DEEP.io->PCF_orders_events_billing_handler:
end
@enduml