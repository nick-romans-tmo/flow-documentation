@startuml
group Trade in domain-Update MSISDN in ACTIVATION and AAL path
CCO->QuoteRespository: OrderLine id is passed per line to identify MSISDN
QuoteRespository->CCO: Return confirmation 
CCO->SAP: OrderLine id is passed per line to identify MSISDN
SAP->CCO: Return confirmation
SAP->SAP: Device shipped notification
SAP->BWSAP_R3: OrderLine id is passed per line to identify MSISDN
group Finance type is EIP 
BWSAP_R3->EMS: Produce Queue the request to be processed by BW-RETAIL
activate EMS
EMS->BW_RETAIL: Consume Queue the request to be processed by BW-RETAIL
activate BW_RETAIL
BW_RETAIL->RSP: OrderLine id is passed per line to identify MSISDN
RSP->BW_RETAIL: OrderLine id is passed per line to identify MSISDN
BW_RETAIL->EMS: Publish to Queue the request to be processed by BW-RETAIL for EIP
BW_RETAIL-[#GREEN]>EMS: Publish to Error queue and Retry to post the message
destroy BW_RETAIL
EMS-[#BLUE]>TIBCO_CE: Publish to TIBCO CE location 
activate TIBCO_CE
TIBCO_CE-[#BLUE]>TIBCO_CE: Process will update the MSISDN based on the orderline ID.
destroy TIBCO_CE
DESTROY EMS
end 
alt Any issues with processing the activation file or specific account type/sub-types
BWSAP_R3->EMS: Produce Queue the request to be processed by StarTek
activate EMS
EMS->StarTek: Consume Queue the request to be processed by BWSAP_R3
activate StarTek
StarTek->EMS: Publish to Queue the request to be processed by BWSAP_R3
BW_RETAIL-[#GREEN]>EMS: Publish to Error queue and Retry to post the message
DESTROY StarTek
EMS-[#BLUE]>TIBCO_CE: Publish to TIBCO CE location 
activate TIBCO_CE
TIBCO_CE-[#BLUE]>TIBCO_CE: Process will update the MSISDN based on the orderline ID.
destroy TIBCO_CE
DESTROY EMS
end
@enduml

