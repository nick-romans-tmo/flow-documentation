@startuml
group Trade in domain-Retrieve customer reject reasons
QuoteRespository->Assurant: Get_OfferCancelReasons
Assurant-->QuoteRespository: Retrieve data from Assurant once per day-Offline
QuoteRespository-->QuoteRespository:Store data in Cassandra
QuoteRespository->Assurant: Get_OfferRejectReasons
Assurant-->QuoteRespository: Retrieve data from Assurant once per day-Offline
QuoteRespository-->QuoteRespository:Store data in Cassandra
Client->CapabilityAPI:[[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dm.td GET/TradeInQuote/Reasons]]
CapabilityAPI->QuoteRespository: GET reference data for Devices
QuoteRespository-->CapabilityAPI: (Provide reasons to return/reject the offer)
CapabilityAPI-->Client: (Provide reasons to return/reject the offer)
end
@enduml