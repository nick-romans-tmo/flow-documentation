@startuml
participant DR_OFFER_CANCEL_REASONS
participant DR_REFERENCE_LOAD_STATUS_LOG
participant STAGING_DR_OFFER_CANCEL_RESNS
participant DR_REFERENCE_DATA_HISTORY
database QuoteRepository
participant TIBCO_SCHEDULER
participant Assurant


Assurant-> TIBCO_SCHEDULER : AssurantReferenceloader pulls the GetOfferCancelReasons payload
TIBCO_SCHEDULER-> QuoteRepository: Loads the offer cancel reason payload to QR DB
QuoteRepository-> DR_REFERENCE_DATA_HISTORY: Loads the payload to the QR table
DR_REFERENCE_DATA_HISTORY-> STAGING_DR_OFFER_CANCEL_RESNS: [[https://bitbucket.service.edp.t-mobile.com/projects/DM/repos/database/browse/ORACLE_DB_SCRIPTS/drp_reference_load_process.sql]] DB ETL parses xml data to staging table
STAGING_DR_OFFER_CANCEL_RESNS-> DR_OFFER_CANCEL_REASONS: ETL loads offer cancel reasons reference data

alt if load success
DR_REFERENCE_DATA_HISTORY-> DR_REFERENCE_LOAD_STATUS_LOG: Updates final DATA_LOAD_STATUS as Success_INSERT_OFFER_CANCEL_REASONS
end
alt if load fails
DR_REFERENCE_DATA_HISTORY-> DR_REFERENCE_LOAD_STATUS_LOG: Updates final DATA_LOAD_STATUS as Failed_INSERT_OFFER_CANCEL_REASONS
end
@enduml