@startuml
participant DR_ELIGIBLE_DEVICES
participant DR_DEVICE_TAC_CODES
participant DR_DEVICE_SKUS
participant DR_DEVICE_OEM_MODELS
participant DR_DEVICE_QUESTIONS
participant DR_REFERENCE_LOAD_STATUS_LOG
participant STAGING_DR_ELIGI_DEVICE_INFO
participant DR_REFERENCE_DATA_HISTORY
database QuoteRepository
participant TIBCO_SCHEDULER
participant Assurant


Assurant-> TIBCO_SCHEDULER : AssurantReferenceloader pulls the GetDevicesNeeded payload
TIBCO_SCHEDULER-> QuoteRepository: Loads the device payload to QR DB
QuoteRepository-> DR_REFERENCE_DATA_HISTORY: Loads the payload to the QR table
DR_REFERENCE_DATA_HISTORY-> STAGING_DR_ELIGI_DEVICE_INFO : [[https://bitbucket.service.edp.t-mobile.com/projects/DM/repos/database/browse/ORACLE_DB_SCRIPTS/drp_reference_load_process.sql]] DB ETL parses xml data to staging table
STAGING_DR_ELIGI_DEVICE_INFO -> DR_ELIGIBLE_DEVICES:DB ETL loads tradein devices reference data
STAGING_DR_ELIGI_DEVICE_INFO -> DR_DEVICE_TAC_CODES: ETL loads devices'associated tac-codes
STAGING_DR_ELIGI_DEVICE_INFO -> DR_DEVICE_SKUS: ETL loads devices'associated skus
STAGING_DR_ELIGI_DEVICE_INFO -> DR_DEVICE_OEM_MODELS: ETL loads devices'associated OEM Models
STAGING_DR_ELIGI_DEVICE_INFO -> DR_DEVICE_QUESTIONS: ETL loads devices'associated question names

alt if load success
DR_REFERENCE_DATA_HISTORY-> DR_REFERENCE_LOAD_STATUS_LOG: Updates DATA_LOAD_STATUS as Success_INSERT_ELIGIBLE_DEVICES
end
alt if load fails
DR_REFERENCE_DATA_HISTORY-> DR_REFERENCE_LOAD_STATUS_LOG: Updates DATA_LOAD_STATUS as Failed_INSERT_ELIGIBLE_DEVICES
end
@enduml