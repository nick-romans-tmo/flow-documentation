@startuml
participant DR_DEVICE_QUESTION_ANSWERS
participant DR_DEVICE_QUESTIONS
participant DR_REFERENCE_LOAD_STATUS_LOG
participant STAGING_DR_DEVICE_QUES_ANSWR
participant DR_REFERENCE_DATA_HISTORY
database QuoteRepository
participant TIBCO_SCHEDULER
participant Assurant


Assurant-> TIBCO_SCHEDULER : AssurantReferenceloader pulls the GetDeviceEvaluationQuestions payload
TIBCO_SCHEDULER-> QuoteRepository: Loads the device questions payload to QR DB
QuoteRepository-> DR_REFERENCE_DATA_HISTORY: Loads the payload to the QR table
DR_REFERENCE_DATA_HISTORY-> STAGING_DR_DEVICE_QUES_ANSWR : [[https://bitbucket.service.edp.t-mobile.com/projects/DM/repos/database/browse/ORACLE_DB_SCRIPTS/drp_reference_load_process.sql]] DB ETL parses xml data to staging table
STAGING_DR_DEVICE_QUES_ANSWR-> DR_DEVICE_QUESTIONS: ETL loads question info (english/spanish)
STAGING_DR_DEVICE_QUES_ANSWR-> DR_DEVICE_QUESTION_ANSWERS: ETL loads answers associated with questions

alt if load success
DR_REFERENCE_DATA_HISTORY-> DR_REFERENCE_LOAD_STATUS_LOG: Updates final DATA_LOAD_STATUS as Success_INSERT_DEVICE_EVAL_QUESTIONS
end
alt if load fails
DR_REFERENCE_DATA_HISTORY-> DR_REFERENCE_LOAD_STATUS_LOG: Updates final DATA_LOAD_STATUS as Failed_INSERT_DEVICE_EVAL_QUESTIONS
end
@enduml