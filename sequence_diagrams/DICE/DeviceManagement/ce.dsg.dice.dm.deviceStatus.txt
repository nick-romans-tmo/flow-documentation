@startuml
title <size:18>__Retrieve Device Block status from SAP and finance type from EIP system__</size>
actor Client
participant  APIGEE
participant  DICE_DM
participant  SAP
participant  EIP
GROUP V3
Client->APIGEE:[[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dm/devices-v3.json  Device Status-V3]]
END
GROUP V4
Client->APIGEE:[[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dm/devices-v4.json  Device Status-V4]]
END
APIGEE-> DICE_DM: deviceImei
DICE_DM -> SAP : imeiBlockStatusCheck
SAP-> DICE_DM: imeiBlockStatus
DICE_DM-> EIP : searchIMEI [[https://servicecatalog.internal.t-mobile.com/servicecat/eip/2017-10-21/wsdl_api/wsdl/SearchImei.wsdl wsdl]]
EIP-> DICE_DM: searchIMEIResult
GROUP V3
DICE_DM-> APIGEE : deviceStatus
APIGEE -> Client : deviceStatus
END
GROUP V4 With additional blocked information
DICE_DM-> APIGEE : deviceStatus
APIGEE -> Client : deviceStatus
END
@enduml