@startuml
title BillPayService.getPaymentHistory
actor Client

group <font color=#FF0090><b> getPaymentHistory

Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/BillPayService.html#getPaymentHistory BillPayService.getPaymentHistory]]
TIBCO->TIBCO:Validate the request

alt if ban presents in request

                alt if GV RefactoredGetPaymentHistory is TRUE
                TIBCO->Samson: Call <b>evGbBanPym</b> Tuxedo for given BAN, Start & End Dates 
                                Samson->TIBCO:Samson returns Payment History details
    else Otherwise
                                TIBCO->RSP:call <b>PaymentHistory.getPaymentHistory()</b>
                                RSP->TIBCO:RSP returns Payment History details
    end

else Otherwise (PaymentID exists)
                TIBCO->Jpayment:call <b>paymentHistoryDetails()</b> operation for given paymentID
    Jpayment->TIBCO: JPayment returns Payment History details
end

TIBCO->Client:Tibco sends the PaymentHistory Response
end

@enduml
