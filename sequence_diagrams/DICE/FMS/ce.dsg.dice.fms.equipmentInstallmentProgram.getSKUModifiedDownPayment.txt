@startuml
actor Client
actor RSP

Client->RSP: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/PROD/wsdl_api/html/EquipmentInstallmentProgramService.html#getSKUModifiedDownPayment EquipmentInstallmentProgram.getSKUModifiedDownPayment]]

group if operation.getSKUModifiedDownPayment.supported
RSP -> RSP : Retrurn error resposne as OPERATION_NOT_SUPPORTED
end

group else 

group Validate request
RSP->RSP: Validate the getSKUModifiedDownPayment Request
group If validation error
RSP->Client: Send back response with respective status code
end
end

RSP->EIP: Invokes [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/eip/GetSkuDownPaymentDetails/resources GetSkuDownPaymentDetails]]  GetSkuDownPaymentDetails

EIP -> RSP: Returns getSKUModifiedDownPayment
end

@enduml