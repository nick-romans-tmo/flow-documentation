@startuml
actor Client
Client->RSP: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/PROD/wsdl_api/html/EquipmentInstallmentProgramService.html#pollCreateEipPlan  EquipmentInstallmentProgram.pollCreateEipPlan]]
RSP->RSPDB: Call RSP DB Poll function to create AsyncRecord request object by setting AsyncToken and PartnerId \n [asyncComponentManager.poll(pollRequest)]
RSPDB->RSP: Based on query fetch and return the record as a response object

group if record.getResponseXml() is not Empty \n then unmarshall the record as record.getResponseXml().
RSP->RSP: initialize the pollingResponse object from (EipCreatePlanResponse) responseElement.getValue()
end


group if record.getResponseXml() is Empty then unmarshall the record.getRequestXml()
RSP->RSP: intialize asyncRequest object from (AsyncEipCreatePlanRequest) responseElement.getValue() 
RSP->RSP: Map the response object by mapPollResponse(asyncRequest.getEipCreatePlanRequest(), pollingResponse) function
group if record (isInProcess or isReceived)
RSP->RSP: then set statusReport as AsyncInProgress
end
group if record isProcessFailed
RSP->RSP: then set statusReport as AsyncFailure
end
RSP->RSP: set the success status in response with token in pollingResponse object
end


RSP->Client: return pollingResponse object to clients
@enduml