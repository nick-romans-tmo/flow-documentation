
@startuml
title JPaymentService.updatePaymentStatus
actor Client

group <font color=#FF0090><b> updatePaymentStatus     
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/Tentative_Ru_TIBCO/wsdl_api/html/JPaymentService.html#updatePaymentStatus updatePaymentStatus]]
group if request contain updated queued samson payment status

TIBCO->HTTP: HTTP is call is invoked to Update PaymentStatus_Queue
HTTP->TIBCO:Return response
end
group otherwise
TIBCO->HTTP: HTTP is call is invoked to UpdatePaymentStatus_Queue-submitted
HTTP->TIBCO:Return response
end
TIBCO->Client:returns response
end
@enduml



