@startuml
title EquipmentInstallment.UpdateLeaseInfo
actor Client

group <font color=#FF0090><b> UpdateLeaseInfo

Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-03-16/wsdl_api/wsdl/Installment.wsdl UpdateLeaseInfo]]


TIBCO -> JMS : post request to  <b>TMOBILE.*.SAMSON.INSTALLMENT.LEASE.INFO.UPDATE</b>
JMS -> TIBCO: returns  response 

TIBCO->Client: TIBCO returns resoponse to client
@enduml1
