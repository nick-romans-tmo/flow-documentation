@startuml
title 
<font size=20>[[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/sid/html/Receipt.html#CreateReceipt CreateReceipt]]</font>
end title
actor ClientNone

group <font color=#FF0090><b> CreateReceipt

ClientNone->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/sid/html/Receipt.html#CreateReceipt CreateReceipt]]
TIBCO->JMS: Request (TMOBILE.{ENV}.DOCUMENTUM\n.RECEIPT.CREATERECEIPT.SEND) 
JMS->TIBCO: Response
end
group <font color=#FF0090><b> CreateReceiptJmsProcess
JMS->TIBCO_DocumentAsync: Receive Message (TMOBILE.{ENV}.DOCUMENTUM.\nRECEIPT.CREATERECEIPT.SEND) 

TIBCO_DocumentAsync->Documentum: createreceipt request receives from\nreceiptservice and Calls Documentum\n<b>[[https://flows.corporate.t-mobile.com/sequence_diagrams/CFS/DOCUMENTS/DIGITAL%20DOCUMENT%20FLOW/CUP_Receipts_sequence_diagram.puml.svg CUPReceiptIRService.ingestReceipt()]]</b>
Documentum->TIBCO_DocumentAsync : Documentum returns the  response.

end
left footer

<font color=black size=15>**This operation creates a receipt in Documentum**</font>


<font size=15>[[https://bitbucket.service.edp.t-mobile.com/projects/TCM/repos/receiptservice/browse ReceiptService Bitbucket]]</font>
<font size=15>[[https://splunk-ss.t-mobile.com:8000/en-GB/app/asm_mw/tibco_cefms_dashboard?form.time_range.earliest=-7d%40h&form.time_range.latest=now&form.orgname_tok=*&form.appname_tok=*&form.service_tok=Receipt&form.operation_tok=* Splunk Dashboard]]</font>
end footer
@enduml