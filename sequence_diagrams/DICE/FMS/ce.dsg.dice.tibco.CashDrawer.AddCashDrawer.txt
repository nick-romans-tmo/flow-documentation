@startuml

title <size:25>**CashDrawerService.addCashDrawer**
actor Client #pink

activate TIBCO
Client -[#blue]> TIBCO : <color:blue>** 	addCashDrawerRequest**
database QueryStoreIDSQL
activate QueryStoreIDSQL
TIBCO -> QueryStoreIDSQL :    Post StoreID to ** DB : sqlmanager/query/getStoreValidity**

note over QueryStoreIDSQL #aqua
	Fetches StoreIDValidation
    Status (Valid / Invalid)
end note

QueryStoreIDSQL -[#green]> CashDrawer : <color:green>  OnSuccess, if **StoreIDValidation/Status=VALID**
QueryStoreIDSQL -[#purple]> TIBCO : <color:purple> Otherwise :: **StoreIDValidation/Status=INVALID, responseStatusCode-102**
deactivate QueryStoreIDSQL

activate CashDrawer
participant CashDrawer
    group Invoke cashDrawer for Configuration
activate TCP    
CashDrawer -> TCP: Open TCP Connection
CashDrawer -> TCP: Write TCP Data
TCP -> CashDrawer: Close TCP Connection
    end
    deactivate TCP
database InsertCashDrawerConfigSQL    
    CashDrawer -[#brown]> InsertCashDrawerConfigSQL : <color:brown>log all data into configuration **DB : sqlmanager/query/insertCashDrawerConfiguration** 
activate InsertCashDrawerConfigSQL

    InsertCashDrawerConfigSQL -[#brown]> CashDrawer : <color:brown>	 							returns Status
    
deactivate InsertCashDrawerConfigSQL  
deactivate CashDrawer
    
TIBCO <-[#red]> TCP : <color:red>     				if any Failure (DB error or TCP Connect/Read/Write Failed) :  **responseStatusCode-102**

TIBCO <-[#green]> InsertCashDrawerConfigSQL : <color:green>						OnSuccess :: when ESP invokes CashDrawer Successfully for configuration and add all data into configuration DB, **responseStatusCode-100** 
TIBCO -[#blue]> Client : <color:blue>**Returns addCashDrawerResponse**
deactivate TIBCO

@enduml
