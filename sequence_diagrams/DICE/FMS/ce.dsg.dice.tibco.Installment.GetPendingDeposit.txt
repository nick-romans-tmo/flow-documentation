@startuml

title 
<font size=20>[[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/sid/html/Installment.html#GetPendingDeposit GetPendingDeposit]]</font>
end title

participant "Client App" as CA
participant "TIBCO" as T
participant "SAMSON"

CA -> T: Request to get Pending deposit Info()\n by sending BAN & MSISDN as input fields
T -> T: validate the request


T->SAMSON: Call <b>evGtDepoDue00() Tuxedo API\n by passing BAN & MSISDN
SAMSON->T:Gets Desposit Amount as response



T->CA: Sends Desposit Amount as the response

left footer

<font color=black size=15>**When a Customer is ready to make a purchase at a point of sale, it is important for T-Mobile to check if there are any open deposits due**</font>
<font color=black size=15>**so the appropriate amount can be collected from him. This Operation takes the Billing Account Number (BAN) or a non-cancelled MSISDN**</font>
<font color=black size=15>**as input and returns the total deposit amount due at the Account level.**</font>


<font size=15>[[https://bitbucket.service.edp.t-mobile.com/projects/TCM/repos/installmentservice/browse InstallmentService Bitbucket]]</font>
<font size=15>[[https://splunk-ss.t-mobile.com:8000/en-US/app/asm_mw/dfm_tibcoce_prd03c?form.time_range.earliest=-24h%40h&form.time_range.latest=now&form.orgname_tok=DSG-DigitalFinanceManagement&form.spacename_tok=TibcoCE_Prod&form.appname_tok=installment-tms-green&form.service_tok=*&form.operation_tok=* Splunk Dashboard]]</font>
end footer

@enduml