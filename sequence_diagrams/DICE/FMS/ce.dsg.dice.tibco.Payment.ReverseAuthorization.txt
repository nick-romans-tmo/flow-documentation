@startuml
title Payment.ReverseAuthorization
actor Client

group <font color=#FF0090><b> ReverseAuthorization

Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/Payment.html#ReverseAuthorization ReverseAuthorization]]

TIBCO->JPayment:calls ReverseAuthorization invokes JPayment 
JPayment->Tsafe: calls <b>Tsafe.StoreCreditCardOperation</b>
group if Tsafe returns success response	
JPayment->JPayment:calls <b>JPaymentSoapPortType.sendAuthorizePayment()</b>
JPayment->JPayment:returns response
end
group if csiCreditCardPaymentInput settlePayment as true
JPayment->JPayment:calls<b>JPaymentSoapPortType.getAuthorizePaymentResults</b>
JPayment->JPayment:returns response.
end
JPayment->JPayment: calls <b>JPaymentSoapPortType.getAuthorizePaymentResults</b>
JPayment->TIBCO:returns response
end
@enduml