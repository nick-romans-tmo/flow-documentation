@startuml
title CreditCard.ValidatePaymentMethod
actor Client

group <font color=#FF0090><b> ValidatePaymentMethod

Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/CreditCard.html#ValidatePaymentMethod ValidatePaymentMethod]]
TIBCO->TIBCO:Validate the request
group Jpayment call is invoked
group Validate the BankAccountData
 alt if ECP or paymentAliasCheckRequest contain bankAccountNumber  and TSAFEMultiDomainLookup = true()
  JPayment->TSAFE:calls <b> TSAFERetrieveBankAccount</b> to RetrieveBankAccount_SAP
  TSAFE->JPayment:returns bank account details 
    group else if TMO-ENV-DPS-ENABLE=true() and userName != $TMO-ENV-DPS-DPS_UserName
group is if access token sucess 
JPayment->DPS:invoke http-DPS-API client 
DPS->JPayment:returns response
end
end
  JPayment->TSAFE:calls <b> TSAFERetrieveBankAccount</b> to RetrieveBankAccount_Jpayment
  TSAFE->JPayment:returns bank account details
    group else if TMO-ENV-DPS-ENABLE=true() and userName != $TMO-ENV-DPS-DPS_UserName
group is if access token sucess 
JPayment->DPS:invoke http-DPS-API client 
DPS->JPayment:returns response
end
end
    JPayment->TSAFE:calls <b> TSAFERetrieveBankAccount</b> to RetrieveBankAccount_SAMSON
  TSAFE->JPayment:returns bank account details
    group else if TMO-ENV-DPS-ENABLE=true() and userName != $TMO-ENV-DPS-DPS_UserName
group is if access token sucess 
JPayment->DPS:invoke http-DPS-API client 
DPS->JPayment:returns response
end
end
  else otherwise
  JPayment->TSAFE:calls <b> TSAFE.RetrieveBankAccountOperation()</b>
  TSAFE->JPayment:reruns response
  group else if TMO-ENV-DPS-ENABLE=true() and userName != $TMO-ENV-DPS-DPS_UserName
group is if access token sucess 
JPayment->DPS:invoke http-DPS-API client 
DPS->JPayment:returns response
end
end
JPayment->JPayment:calls <b>JPaymentSoapPortType.performBankAccountForBan</b>
JPayment->JPayment:reruns response
  end
  alt else if ccp or paymentAliasCheckRequest contain creditcard number and ValidCreditCard = false() and TSAFEMultiDomainLookup = true()
   JPayment->TSAFE:calls <b> TSAFERetrieveBankAccount</b> to Retrievecreditcard_SAP
  TSAFE->JPayment:returns bank account details 
    group else if TMO-ENV-DPS-ENABLE=true() and userName != $TMO-ENV-DPS-DPS_UserName
group is if access token sucess 
JPayment->DPS:invoke http-DPS-API client 
DPS->JPayment:returns response
end
end
  JPayment->TSAFE:calls <b> TSAFERetrieveBankAccount</b> to RetrieveBcreditcard_Jpayment
  TSAFE->JPayment:returns bank account details
    group else if TMO-ENV-DPS-ENABLE=true() and userName != $TMO-ENV-DPS-DPS_UserName
group is if access token sucess 
JPayment->DPS:invoke http-DPS-API client 
DPS->JPayment:returns response
end
end
    JPayment->TSAFE:calls <b> TSAFERetrieveBankAccount</b> to Retrievecreditcard_SAMSON
  TSAFE->JPayment:returns bank account details
    group else if TMO-ENV-DPS-ENABLE=true() and userName != $TMO-ENV-DPS-DPS_UserName
group is if access token sucess 
JPayment->DPS:invoke http-DPS-API client 
DPS->JPayment:returns response
end
end
else otherwise
   JPayment->TSAFE:calls <b> TSAFE.RetrieveCreditCardOperation()</b>
  TSAFE->JPayment:reruns response
  group else if TMO-ENV-DPS-ENABLE=true() and userName != $TMO-ENV-DPS-DPS_UserName
group is if access token sucess 
JPayment->DPS:invoke http-DPS-API client 
DPS->JPayment:returns response
end
end
JPayment->JPayment:calls <b>JPaymentSoapPortType.performBankAccountForBan</b>
JPayment->JPayment:reruns response
JPayment->TIBCO:returns response
end
end
end
TIBCO->Client:returns response
end
@enduml