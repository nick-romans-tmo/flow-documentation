@startuml
actor Client
participant RSP
database RSPDB
collections RabbitMQ
participant RSP_PROCESSOR
Client-> RSP:Invokes[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/PROD/wsdl_api/html/EquipmentInstallmentProgramService.html#asyncGetEipEstimate EquipmentInstallmentProgram.asyncGetEipEstimate]] 
RSP->RSP: Generates an AsyncRecord by setting the serialized XML and set a key/uniqueId
RSP->RSPDB: Inserts the asyncGetEipEstimate in RSP DB 
activate RSP
activate RSPDB 

activate RabbitMQ
RSP -> RabbitMQ : Push the asyncGetEipEstimate to RabbitMQ queue. 
deactivate RSPDB
RabbitMQ -> RSP_PROCESSOR : asyncGetEipEstimate is processed
note over  of RSP_PROCESSOR #aqua
   [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/sequence_diagrams/Dice/ EquipmentInstallmentProgram.getEipEstimate]] is called 
end note
RSP -> Client : RSP retruns to client asyncResponse  with async key
deactivate RSP
@enduml