@startuml
title GetEasyPay.GetEasyPay     
actor Client

group <font color=#FF0090><b> GetEasyPay

Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/GetEasyPay.html#GetEasyPay GetEasyPay]]

TIBCO->SAMSON:calls tuxedo<b>gnGtLgclDt</b> to get logical date
SAMSON->TIBCO:returns logical date
TIBCO->SQLMgrHTTP:calls <b>SQLMgrHTTPClient</b> to store the logical date
SQLMgrHTTP->TIBCO:returns response
TIBCO->SAMSON:Tuxedo is invoked <b>arGtCrdCd</b>
SAMSON->TIBCO: returns the credit card data.
TIBCO->Client:returns response
end
@enduml