
@startuml
title PublicSecureCreditCardService.PublicSecureStoreCreditCardOperation
actor Client

group <font color=#FF0090><b> PublicSecureStoreCreditCardOperation     
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/PublicSecureRetrieveBankAccount.html#PublicSecureStoreCreditCardOperation PublicSecureStoreCreditCardOperation]]
group if IsAlias in request is true
TIBCO->TSafe: Call TSafe - <b>retrievecreditcard</b> operation for all 3 domains (SAP/Samson/JPayment)
TSafe->TIBCO: Tsafe returns card details for existing domains if any
group if card already exist for the given domain in request
TIBCO->Tsafe: Collect the Alias provided in request itself
Tsafe->TIBCO: Tsafe returns response.
else if credit card already NOT exist for the given domain in request
TIBCO->TSafe:Call TSafe - <b>storecreditcard</b> operation for given domain, acct & routing#
TSafe->TIBCO: TSafe provides the Alias which is created for given details
end
else if IsAlias in request is NOT true
TIBCO->TSafe:Call TSafe - <b>storecreditcard</b> operation for given domain, acct & routing#
TSafe->TIBCO: TSafe provides the Alias which is created for given details
end	
group else if IsAlias in request is NOT true
TIBCO->TIBCO:validate the CreditCard data
group if Creditcard is valid
group if it is Authorized 
TIBCO->TSafe: authorized sends the request to Jpayment for the Details to check the CreditCard Details with Tsafe
TSafe->TIBCO:TSafe returns CreditCard Details 
end
group else:if it is Not Authorized 
TIBCO->TSafe: authorized sends the request to Tsafe and returns CreditCardAlias
TSafe->TIBCO:Tsafe returns CreditCardAlias details
end
end
end
TIBCO->Client:TIBCO returns response to client
end

@enduml



