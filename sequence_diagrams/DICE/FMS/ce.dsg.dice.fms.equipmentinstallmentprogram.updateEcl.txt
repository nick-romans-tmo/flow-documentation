@startuml
actor Client
Client->RSP: [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/equipmentinstallmentprogram/browse/src/main/resources EquipmentInstallmentProgram.updateEcl]] 
RSP->SAMSON: Call Tuxedo [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/welcome.html evGtCustInfo]] to get basic account details
SAMSON->RSP: Samson returns basic account details
RSP->ESOA: Invokes tibco service [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/esoa/CreditRisk CreditRiskServicePortType.getCreditRiskProfileInfo]] 
ESOA->RSP:- Returns the credit risk profile info
RSP->SAMSON: Call [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/welcome.html evGbEIPInfo]] tuxedo evGbEIPInfo to get SubscriberEIPInfo
SAMSON->RSP: Samson returns SubscriberEIPInfo
RSP->SAMSON: Call tuxedo [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/welcome.html evGbEIPInfo]] to get SubscriberEIPInfo
SAMSON->RSP: SubscriberEIPInfo returned by Samson
RSP->EIP: Invokes [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/eip/TempDoubleEIPECL/resources TempDoubleEIPECL.tempDoubleECL]] 
RSP->Client:EclUpdateResponse to client
@enduml
