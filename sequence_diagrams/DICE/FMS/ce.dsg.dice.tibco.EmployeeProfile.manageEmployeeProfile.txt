@startuml
title 
<font size=20>[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-03-16/wsdl_api/wsdl/EmployeeProfileService.wsdl ManageEmployeeProfile]]</font>
end title
actor WorkDay

group <font color=#FF0090><b> ManageEmployeeProfile

WorkDay->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-03-16/wsdl_api/wsdl/EmployeeProfileService.wsdl ManageEmployeeProfile]]

group if manageEmployeeProfileRequest contains profileUpdateType and IsMIREnabled=false()


TIBCO->JMS: Posts request to  <b>TMOBILE.*.ESS.EMPLOYEEPROFILE.UPDATE.QUEUE</b>
JMS->TIBCO: returns response

group if getTransTrackingEnabled_RV
TIBCO->SQLMgrHttpClient:  calls <b>SQLMgrHttpClient:</b> sqlmanager/query/GetTransTrackingEnabled 
SQLMgrHttpClient->TIBCO:  returns response 
end
end 

alt if manageEmployeeProfileRequest contains profileUpdateType or profileFormType

TIBCO->JMS: posts request to <b>TMOBILE.*.DOCUMENTUM.EMPLOYEEPROFILE.CREATE.QUEUE</b>
JMS->TIBCO_DocumentAsync: Request
  alt If DocuSign Enabled
	                    TIBCO_DocumentAsync->Documentum : invokes Document
	                    Documentum-> TIBCO_DocumentAsync : Response
                    else Otherwise
                        TIBCO_DocumentAsync->xECM: [[https://cupreceipts-ilab02.test.px-npe01b.cf.t-mobile.com Invokes xECM Api]]
						xECM->TIBCO_DocumentAsync: xECM Response
                    end 
TIBCO_DocumentAsync->JMS: response
JMS->TIBCO: returns response
else otherwise if WorkforceManagement-OpenText_Flow = "TRUE"
TIBCO->JMS: posts request to <b>TMOBILE.*.OPENTEXT.EMPLOYEEPROFILE.CREATE.QUEUE</b>
JMS->TIBCO: returns response
end 

TIBCO->WorkDay: TIBCO returns response to client


left footer


<font color=black size=15>**Provide the means to store employee records**</font>

<font size=15>[[https://bitbucket.service.edp.t-mobile.com/projects/TCM/repos/employeeprofileservice/browse Bitbucket - EmployeeProfileService]]</font>
<font size=15>[[https://splunk-ss.t-mobile.com:8000/en-GB/app/asm_mw/tibco_cefms_dashboard?form.time_range.earliest=-7d%40h&form.time_range.latest=now&form.orgname_tok=*&form.appname_tok=*&form.service_tok=Receipt&form.operation_tok=* SplunkDashBoard - Need to Create]]</font>
end footer
@enduml