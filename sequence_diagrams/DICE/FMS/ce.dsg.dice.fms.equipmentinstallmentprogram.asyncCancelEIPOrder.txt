@startuml
actor Client
participant RSP
database RSPDB
collections RabbitMQ
Client-> RSP: Invokes [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/equipmentinstallmentprogram/browse/src/main/resources EquipmentInstallmentProgram.asyncCancelEIPOrder]]
RSP->RSP: Generates an AsyncRecord by setting the serialized XML and set a key/uniqueId
RSP->RSPDB: Inserts the AsyncRecord in DB for the uniqueId as key 
note over  of RSPDB #aqua
    Store Request in DB
end note
RSP->RabbitMQ: Pushes the AsyncRecord to RabbitMQ
RSP -> Client : AsyncResponse sent back with key set as Token in response
group Rabbit Listener AsyncProcessor
RabbitMQ->RSP: RabbitMQ Listener retrieves the AsyncRecord
group when the AsyncRequest has multiple CancelEIPOrders
loop each cancelEIPOrders
RSP->RSP: Creates each AsyncRecord for each cancelEIPOrder and set a uniqueId as key
RSP->RSPDB: Inserts the AsyncRecord with created uniqueId as key in DB
RSP->RabbitMQ: Publish each AsyncRecord to RabbitMQ
end
RSP->RSPDB: Updates the status AsyncRecord with multiple cancelEIPOrder as SUCCESS
end
group when AsyncRequest has only one cancelEIPOrder
RSP->RSP: Invokes [[http://flows.corporate.t-mobile.com/all-diagrams/sequence_diagrams/dice/ EquipmentInstallmentProgram.CancelEIPOrder]] 
RSP->RSPDB: Updates the status of AsyncRecord and the responseXML from synchronous call
end
end
deactivate RSP
@enduml