@startuml
actor Client
participant RSP

Client->RSP:Invokes [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/PROD/wsdl_api/html/EquipmentInstallmentProgramService.html#resendFinanceAgreement FinanceDocumentService.resendFinanceAgreement]] 

group If orderId is not null
RSP->RSPDB : Invoke RSP DB selectFinancialAgreementInfoByTransactionID
RSPDB->RSP : returns FinancialAgreementInfo
end

group If agreementId is not null
RSP->RSPDB : Invoke RSP DB selectFinancialAgreementInfoByAgreementId
RSPDB->RSP : returns FinancialAgreementInfo
end

group If financeId is not null
RSP->RSPDB : Invoke RSP DB selectFinancialAgreementInfoByFinanceId
RSPDB->RSP : returns FinancialAgreementInfo
end

group validate FinancialAgreementInfo 
group if envelopeId == null
RSP->Client: Return error respose to client as ENVELOPE_ID_IS_NULL
end
end

RSP->DOCUSIGN: Invoke docusign [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/docusign/dsapi/resources correctAndResendEnvelope]] with FinancialAgreementInfo

RSP->RSPDB: Invoke RSP DB updateEmailAddressByEnvelopeId to update FinancialAgreementInfo

RSP -> Client : RSP sends status to client
deactivate RSP
@enduml