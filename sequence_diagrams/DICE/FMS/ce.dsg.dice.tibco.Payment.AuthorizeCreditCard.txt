@startuml
title Payment.AuthorizeCreditCard
actor Client

group <font color=#FF0090><b> AuthorizeCreditCard

Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/Payment.html#AuthorizeCreditCard AuthorizeCreditCard ]]
TIBCO->TIBCO :Validate the request
alt if processOrderIndicator is false ,csiAuthorizeCreditCardRequest/isAliasRequired is true and AuthorizeCreditCardRequest/fraudCheck/banActivationDate is greater than zero
TIBCO->TIBCO:returns GetDateDifferenceInDays 

Else Otherwise if Request Authorize Payment  WithoutCVV

alt if csiAuthorizePaymentWithResultRequest contain accountNunberAlias or applicationId=POS and cardType=PINDEBIT and easyPayIndicator =true and cardNumber =0
TIBCO->JPayment: calls payment.getpaymentid()
group if csiAuthorizePaymentWithResultRequest contain accountNunberAlias or applicationId=POS and cardType=PINDEBIT and easyPayIndicator =true and cardNumber =0 
JPayment->TsafeorDPS: calls <b>Tsafe.RetrieveCreditCardOperation()</b>
alt if Tsafe returns response success
group if csiAuthorizePaymentWithResultRequest isCreditCardAliasRequired = true() and cardTypeIndicator = "1" or  cardTypeIndicator =0
group if TMO-ENV-DPS-ENABLE = true() and Start/userName != TMO-ENV-DPS-DPS_UserName and valid access token 
JPayment->TsafeorDPS:Invoke DPS call
TsafeorDPS->JPayment:returns response
end
end
end
Else Otherwise 
JPayment->TsafeorDPS: calls <b>Tsafe.StoreCreditCardOperation</b>
group if Tsafe returns success response	
JPayment->JPayment:calls <b>JPaymentSoapPortType.sendAuthorizePayment()</b>
JPayment->JPayment:returns response
end
group if csiCreditCardPaymentInput settlePayment as true
JPayment->JPayment:calls<b>JPaymentSoapPortType.getAuthorizePaymentResults</b>
JPayment->JPayment:returns response.
end
end
Else Otherwise 
JPayment->JPayment: calls <b>JPaymentSoapPortType.getAuthorizePaymentResults</b>
JPayment->JPayment:returns response
end

 
TIBCO->JPayment: calls <b>JPaymentSoapPortType.sendAuthReversalPayment()</b>
JPayment->TIBCO:Return response
group if jPayReverseTransactionId is exists 
TIBCO->JPayment::calls<b>JPaymentSoapPortType.getAuthReversalPaymentResults</b>
JPayment->TIBCO:Return response
Else Otherwise 
group if if csiAuthorizeCreditCardRequest paymentTypeCode is 504 and channelId = MPCSWEB and csiAuthorizeCreditCardResponse contain authorizationCode
TIBCO->METRO:calls CallMetroHTTPRequest
METRO->TIBCO:returns response
alt if createPaymentResponse is success
TIBCO->JMS:Update he request to JMS 
JMS->TIBCO:Return response
else otherwise failure
TIBCO->JMS:Send message to retry
JMS->TIBCO:returns response
end
end

alt  if Request Authorize Payment With Fraud Check WithoutCVV
TIBCO ->DB:Tibco will invokes the DB call
DB->TIBCO:Return response 

Else Otherwise
TIBCO->JPayment: calls <b>JPaymentSoapPortType.sendAuthReversalPayment()</b>
JPayment->TIBCO:Return response
group if jPayReverseTransactionId is exists 
TIBCO->JPayment::calls<b>JPaymentSoapPortType.getAuthReversalPaymentResults</b>
JPayment->TIBCO:Return response

Else Otherwise 
group if csiAuthorizeCreditCardRequest paymentTypeCode is 504 and channelId = MPCSWEB and csiAuthorizeCreditCardResponse contain authorizationCode
TIBCO->METRO:calls CallMetroHTTPRequest
METRO->TIBCO:returns response
end
alt if createPaymentResponse is success
TIBCO->JMS:Update he request to JMS 
JMS->TIBCO:Return response
else otherwise failure
TIBCO->JMS:Send message to retry
JMS->TIBCO:returns response
end
end

group if csiAuthorizeCreditCardRequest contain accountNbr equal to 9
TIBCO->SAMSON: Call <b>evGbbanPym</b> Tuxedo for given BAN info
SAMSON->TIBCO:returns ban details
end

alt if csiFraudCheckRequest contain permanentToken
group FraudCheckVesta
TIBCO->vesta:calls VESTA.FRAUDCHECK
vesta->TIBCO:returns response
end

else otherwise GetCreditCardNumber_SAPDomain								   
alt if paymentAliasCheckRequest contain bankAccountNumber
TIBCO->TIBCO:validating bank account details
group if CheckBankAlias/IsAlias = true()
TIBCO->TsafeorDPS:calls <b>TSafe.RetrieveBankAccountOperation</b>
TsafeorDPS->TIBCO:returns response
end
group else if TMO-ENV-DPS-ENABLE=true() and userName != $TMO-ENV-DPS-DPS_UserName
group is if access token sucess 
TIBCO->TsafeorDPS:invoke http-DPS-API client 
TsafeorDPS->TIBCO:returns response
end
end
end
end		  
group else if GetCreditCardNumber_JPAYDOMAIN
group if paymentAliasCheckRequest contain bankAccountNumber
TIBCO->TIBCO:validating bank account details
group if CheckBankAlias/IsAlias = true()
TIBCO->TsafeorDPS:calls <b>TSafe.RetrieveBankAccountOperation</b>
TsafeorDPS->TIBCO:returns response
end
group else if TMO-ENV-DPS-ENABLE=true() and userName != $TMO-ENV-DPS-DPS_UserName
group is if access token sucess 
TIBCO->TsafeorDPS:invoke http-DPS-API client 
TsafeorDPS->TIBCO:returns response
end
end
end
end

													   
Else Otherwise
TIBCO->SOAP:calls <b>ProcessFraudCheckSOAP.SOAPRequestReply</b>
SOAP->TIBCO:returns response
TIBCO->HSO:calls<b>HSOITransactionProcessor.runTransaction</b>
HSO->TIBCO:returns response
TIBCO->CARE:CAREITransactionProcessor.runTransaction
CARE->TIBCO:returns response
TIBCO->MPCS:calls<b>MPCSITransactionProcessor.runTransaction</b>
MPCS->TIBCO:returns response
TIBCO->RETAIL:calls <b>RETAILITransactionProcessor.runTransaction</b>
RETAIL->TIBCO:returns response
end
						
alt  if csiFraudCheckResponse decision  is REJECT/REVIEW 
TIBCO->DB : Invoke db call
DB->TIBCO:returns respons
else otherwise 
TIBCO->JMS:Invoke JMS QUEUE
JMS->TIBCO:Return respons 														  
end
Else otherwise 
group DMErrorTransactionInsertSQL
TIBCO->DB:Db call is invoked
DB->TIBCO:error code will send to respons
end
end
group FraudCheckVesta
 TIBCO->vesta:calls <b> ProcessFraudCheckSOAP.ProcessFraudCheck()</b>
 vesta->TIBCO:returns response details
 group if vesta returns success response
 vesta->DB:calls DB to store the date <b>DecisionManagerLog</b>
group else StartorDMDecision = REJECT  and UpdateCreditClassRequest transactionType = NEW POSTPAID BAN
 vesta->JMS:Send the details to JMS Queue
 end
vesta->TIBCO:returns response
 end
 end


group if Fraud check is success
TIBCO->JPayment:calls <b>JPaymentSoapPortType.getAuthorizePaymentResults()</b>
JPayment->TIBCO:returns the response
end

TIBCO->Client:Tibco sends the response back to the client.
end
end
@enduml