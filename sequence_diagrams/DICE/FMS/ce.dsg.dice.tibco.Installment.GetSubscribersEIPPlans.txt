@startuml
title Installment.GetSubscribersEIPPlans
actor Client

group <font color=#FF0090><b> GetSubscribersEIPPlans

Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-03-16/wsdl_api/wsdl/Installment.wsdl GetSubscribersEIPPlans]]


TIBCO -> EIP : Calls <b>QueryActiveInstallmentPlans</b>
EIP -> TIBCO: returns queryActiveInstallmentPlansResponse 

TIBCO->Client: TIBCO returns resoponse to client
@enduml1
