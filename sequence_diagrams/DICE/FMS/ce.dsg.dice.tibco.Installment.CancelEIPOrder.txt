@startuml
title Installment.CancelEIPOrder
actor Client

group <font color=#FF0090><b> CancelEIPOrder

Client->TIBCO: Client calls <b>[[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/Installment.html#CancelEIPOrder Installment.cancelEIPOrder()]]</b>
TIBCO->RSP:Invokes RSP - <b>[[https://flows.corporate.t-mobile.com/all-diagrams/sequence_diagrams/dice/fms/ce.dsg.dice.fms.equipmentinstallmentprogram.canceleiporder.txt/ EquipmentInstallmentProgram.cancelEIPOrder()]]</b> by\nincluding equipmnet details like ban, reasonCode, transactionId,etc
RSP->TIBCO:Returns the Service Status, DocumentID & tarnsactionID.
TIBCO->Client:TIBCO retuns the response back to the client.
end
@enduml
