@startuml

title <size:25>**CashDrawerService.lookupCashDrawer**
actor Client #pink

activate TIBCO
Client -[#blue]> TIBCO : <color:blue>** 	lookupCashDrawerRequest**
database QueryCashDrawerConfigSQL
activate QueryCashDrawerConfigSQL
TIBCO -> QueryCashDrawerConfigSQL :    Post request to ** DB : sqlmanager/query/getCashDrawerConfiguration**

note over QueryCashDrawerConfigSQL #aqua
	Fetches configured 
    cashDrawer details
end note

QueryCashDrawerConfigSQL -[#green]> CashDrawer : <color:green>  if **lookupCashDrawerRequest/getNetworkStatus=true()** \n <color:green> 	and **exists(QueryCashDrawerConfigSQL/ROW)**

note over CashDrawer #aqua
	ESP invokes cashDrawer to
    get Physical Status if
    getNetworkStatus=true()
end note
deactivate QueryCashDrawerConfigSQL

activate CashDrawer
participant CashDrawer
    group For-Each QueryCashDrawerConfigSQL/ROW
activate TCP    
CashDrawer -> TCP: Open TCP Connection
CashDrawer -> TCP: Write TCP Data
CashDrawer -> TCP: Read TCP Data
TCP -> CashDrawer: Retrun Status & close TCP Connection
end
deactivate TCP
    CashDrawer -[#red]> TIBCO : <color:red>	 if any Failure (DB error or TCP Connect/Read/Write Failed) : **responseStatusCode-102**
    	
    
deactivate CashDrawer
    
TIBCO <-[#green]> TCP : <color:green>     							OnSuccess :: when ESP invokes cashDrawer successfully to get physical Status, **responseStatusCode-100**

TIBCO -[#blue]> Client : <color:blue>**Returns lookupCashDrawerResponse**
deactivate TIBCO

@enduml
