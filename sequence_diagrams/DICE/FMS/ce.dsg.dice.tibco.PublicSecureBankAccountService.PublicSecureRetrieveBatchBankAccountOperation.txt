
@startuml
title PublicSecureRetrieveBankAccount.PublicSecureRetrieveBatchBankAccountOperation
actor Client

group <font color=#FF0090><b> PublicSecureRetrieveBatchBankAccountOperation     
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/PublicSecureRetrieveBankAccount.html#PublicSecureRetrieveBatchBankAccountOperation PublicSecureRetrieveBatchBankAccountOperation]]
TIBCO->TSafe:Username,TransactionId
alt <font color=#FF8C00> Username,TransactionId are not Valid
TSafe-> TIBCO: Sent Error Response
TIBCO->Client:Sent Error Response
else Username,TransactionId Valid
TSafe->TIBCO: Response with BankAccount details
TIBCO->Client:Response
end
end 




@enduml



