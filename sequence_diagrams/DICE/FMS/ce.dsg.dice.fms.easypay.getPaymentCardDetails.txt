@startuml
actor Client

Client->RSP: Invokes [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/easypay/browse/src/main/resources/service-api-definition/EasyPayService.wsdl getPaymentCardDetails]] EasyPayService.getPaymentCardDetails
group Validate request
RSP->RSP: Validate  PaymentCardDetails Request
group If validation error
RSP->Client: Send back response with respective status code
end
RSP->Tibco:Invokes [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/tibco/PaymentLookup.wsdl getEasyPayCardDetails]]PaymentLookupPort.getEasyPayCardDetails
Tibco->RSP: Tibco returns EasyPayCardDetails info
RSP->Client: EasyPayCardDetailsResponse to client
@enduml