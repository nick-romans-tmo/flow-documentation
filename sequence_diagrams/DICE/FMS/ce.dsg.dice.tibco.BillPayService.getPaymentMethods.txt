@startuml
title BillPayService.getPaymentMethods
actor Client

group <font color=#FF0090><b> getPaymentMethods
Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/BillPayService.html#getPaymentMethods getPaymentMethods]]
TIBCO->TIBCO:validate the request
TIBCO->JPayment:calls <b>calls StoredPaymentMethodSoap.getStoredPaymentMethod</b>
JPayment->TIBCO:Return response
group if GetPaymentMethodDetails PaymentMethodDetails=CREDIT/DEBIT and CHECK/DEBIT and TMO-App-JPayment-checkCreditcardType = true
group if csiLookupRequest contain CardNumber and applicationId =POS and KIOSK
vesta->vesta:validate the CardNumber
group ValidCreditCard = false()
vesta->Tsafe:calls<b>Tsafe.RetrieveCreditCardOperation()</b>
Tsafe->vesta:reruns response
end
group else if TMO-ENV-DPS-ENABLE=true() and userName != $TMO-ENV-DPS-DPS_UserName
group is if access token sucess 
vesta->DPS:invoke http-DPS-API client 
vesta->TIBCO:returns response
end
vesta->TIBCO:reruns  bank account details.
end
end
end
  alt if ECP or paymentAliasCheckRequest contain bankAccountNumber  and TSafeMultiDomainLookup = true()
  JPayment->Tsafe:calls <b> TSafeRetrieveBankAccount</b> to RetrieveBankAccount_SAP
  Tsafe->JPayment:returns bank account details 
  JPayment->Tsafe:calls <b> TSafeRetrieveBankAccount</b> to RetrieveBankAccount_Jpayment
  Tsafe->JPayment:returns bank account details
    JPayment->Tsafe:calls <b> TSafeRetrieveBankAccount</b> to RetrieveBankAccount_SAMSON
  Tsafe->JPayment:returns bank account details
  else otherwise
  JPayment->Tsafe:calls <b> Tsafe.RetrieveBankAccountOperation()</b>
  Tsafe->JPayment:reruns response
  group else if TMO-ENV-DPS-ENABLE=true() and userName != $TMO-ENV-DPS-DPS_UserName
group is if access token sucess 
JPayment->DPS:invoke http-DPS-API client 
DPS->JPayment:returns response
end
TIBCO->JPayment:calls <b>JPaymentSoapPortType.performBankAccountForBan</b>
JPayment->TIBCO:reruns response
  end
  group else if ccp or paymentAliasCheckRequest contain creditcard number and ValidCreditCard = false() and TSafeMultiDomainLookup = true()
   JPayment->Tsafe:calls <b> TSafeRetrieveBankAccount</b> to Retrievecreditcard_SAP
  Tsafe->JPayment:returns bank account details 
  JPayment->Tsafe:calls <b> TSafeRetrieveBankAccount</b> to RetrieveBcreditcard_Jpayment
  Tsafe->JPayment:returns bank account details
    JPayment->Tsafe:calls <b> TSafeRetrieveBankAccount</b> to Retrievecreditcard_SAMSON
  Tsafe->JPayment:returns bank account details
  
  JPayment->Tsafe:calls <b>Tsafe.RetrieveCreditCardOperation</b>
  Tsafe->JPayment:returns credit card details
  
  JPayment->JPayment: calls <b>JPaymentSoapPortType.getPmtCardAccountAndBan()</b>
  JPayment->JPayment:return response

 
 TIBCO->JPayment:calls <b>JPaymentSoapPortType.getBankAccounts</b>
 JPayment->TIBCO:reruns response
 end
TIBCO->Client:Tibco sends the response back to the client
end
end
@enduml