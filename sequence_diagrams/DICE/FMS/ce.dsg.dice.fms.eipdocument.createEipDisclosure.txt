@startuml
actor Client
Client->RSP: [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/eipdocument/browse/src/main/resources EquipmentInstallmentProgram.createEIPDisclosure]]
group Validate request
RSP->RSP: validate  Request
group If validation error
RSP->Client: Send back response with respective status code
end
end
RSP->RSPDB:RSPDB is invoked to select EIPDisclosureInfo By TransactionId
RSPDB->RSP:RSPDB  returns EIPDisclosureInfo details
group if EIPDisclosureInfo from RSPDB is not null
group if CANCEL_STATUS of selected disclosure is not null
RSP->Client: Send error response back to client
end
group if request contains emailAddress
RSP->RSP:set PreviousEmailAddress to eipDisclosureInfoDTO
group if emailaddress in request is not equal to emailaddress from RSPDB for the selected disclosure
RSP->RSPDB: Update email address against the respective disclosure
end
end
group else if emailAddress in request is null
RSP->RSP:Set the emailaddress from RSPDB to prevEmailAddress in application variable
end
group if request contain DocumentId and patnerid is EmailRequestEnabled partner and email option != EMAIL_DISCLOSURE_REQUEST
RSP->Docusign:Calls [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/docusign/dsapi/resources DSAPIServiceSoap.voidEnvelope]]
Docusign->RSP:Docusign return response
end
end
group if email option in request is EMAIL_DISCLOSURE_REQUEST
RSP->TIBCO:-Invoke tibco call [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/tibco/Notification.wsdl Notification.sendEmail]] to send emails
TIBCO->RSP:Tibco  returns send EmailResponse
group if sendEmail response is SUCCESS and EIPDisclosureInfo queried from RSPDB is null
RSP->RSPDB:RSPDB is invoked to insert disclosure document details
end
group if Finance Type in request is LOAN
RSP->RSPDB: Inset Esign and FPP document details in RSPDB
end
RSP->Client: Send the response to client
end
group if partner is RetailPartner and  transactionId in request doesnt contains "async" and request contain DocumentId and patnerid is EmailRequestEnabled partner and email option != EMAIL_DISCLOSURE_REQUEST
RSP->RSP: Generates an AsyncRecord by setting the serialized XML and set a key/uniqueId
RSP->RSPDB: Inserts the AsyncRecord in DB for the uniqueId as key 
note over  of RSPDB #aqua
    Store Request in DB
end note
RSP->RabbitMQ: Pushes the AsyncRecord to RabbitMQ
RSP -> Client : response set back to client
group Rabbit Listener AsyncProcessor
RabbitMQ->RSP: RabbitMQ Listener retrieves the AsyncRecord
RSP->RSP: Creates a sync asyncCreateEIPDisclosureRequest from AsyncRecord
RSP->RSP:invokes [[[[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/eipdocument/browse/src/main/resources EquipmentInstallmentProgram.createEIPDisclosure]]
RSP->RSP: Sets the createEIPDisclosureResponse XML in the AsyncRecord
RSP->RSPDB: Updates the status as Success in DB and sets the responseXML
end
end
note right 
RetailPartner is POS
end note

group if  partnerId is Docusign enabled one
group if request contain DocumentId and patnerid is EmailRequestEnabled partner and email option != EMAIL_DISCLOSURE_REQUEST
RSP->RSPDB:RSPDB is invoked to select EipDisclosureChannelDetails
RSPDB->RSP:RSPDB  returns EipDisclosureChannel Details
group if client is non Responsive and financeType in request is LOAN
RSP->OpenTextSS: Calls [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/opentext/streamserver JobSubmitContentBase64PortType.submitJobsContent]] for  Esign TC document creation
OpenTextSS->RSP: Returns Esign TC PDF content and tabs from OT
RSP->OpenTextSS: Calls [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/opentext/streamserver JobSubmitContentBase64PortType.submitJobsContent]] for Financial Privacy Policy document creation
OpenTextSS->RSP: Returns FPP Byte Content from OT
end
group if size of EipOrderInfo in request is 1
group if financeType in request is LEASE
RSP->OpenTextSS: [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/opentext/streamserver JobSubmitContentBase64PortType.submitJobsContent]] for LeaseAgreement PDF creation
OpenTextSS->RSP: Lease agreement PDF content and info are returned
end
group else if financeType !- Lease
RSP->OpenTextSS: [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/opentext/streamserver JobSubmitContentBase64PortType.submitJobsContent]] for EIP disclosure PDF creation
OpenTextSS->RSP: Disclosure PDF content and info are returned
end
end
group else if size of EipOrderInfo in request is > 1
RSP->OpenTextSS: [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/opentext/streamserver JobSubmitContentBase64PortType.submitJobsContent]] parallel calls to OT for disclosure PDF creation
OpenTextSS->RSP: Disclosure PDF content and info are returned and aggregated
end
RSP->RSP: Reorder the documentInfo returned from OTSS (in the order of Esign, FPP, Disclosure, LeaseAgreement)
RSP->Docusign: Calls [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/docusign/dsapi/resources dsapi.createEnvelopeAndFormsTemplate]] for document presentment
group if request contain (EMAIL_WITH_LINK or EMAIL_WITH_PIN or DIRECT_LINK_NO_EMAIL)
RSP->Docusign: Invokes [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/docusign/dsapi/resources dsapi.requestRecipientToken]] for document URL
Docusign->RSP: documentURL is returned
end
end
group else if !(request contain DocumentId and patnerid is EmailRequestEnabled partner and email option != EMAIL_DISCLOSURE_REQUEST)
group if emailOption in request is EMAIL_WITH_LINK or EMAIL_WITH_PIN
RSP->Docusign: Invokes [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/docusign/dsapi/resources dsapi.correctAndResendEnveloper]] for document content and Id
Docusign->RSP: document content and Id is returned
end
group else if emailOption in request is not equal to EMAIL_WITH_LINK or EMAIL_WITH_PIN
RSP->Docusign: Invokes [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/docusign/dsapi/resources dsapi.requestRecipientToken]] for document URL
Docusign->RSP: documentURL is returned
end
end
end

RSP->RSPDB: Inserts/Update the document content in RSPDB


group if request contain partnerId and isEmailRequestEnabledPartner 
RSP->RSPDB: RSPDB is invoked to update documentId in EIPDisclosureInfo 
end

RSP->Client:createEIPDisclosure response is send to client
@enduml