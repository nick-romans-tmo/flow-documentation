@startuml
title PaymentService.applyPayment
actor Client

group <font color=#FF0090><b> applyPayment
                Client->TIBCO : Client Calls <b>[[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/PaymentService.html#applyPayment PaymentService.applyPayment]]</b> 
                
      
                TIBCO -> Ericsson_WSIL: Call queryAPIservice.getCustomerStatus()
                Ericsson_WSIL -> TIBCO : Returns customer Migration Status
                                
                alt If MigrationStatus = "J" (Migration is in Progress)
    TIBCO->TIBCO: Call MigrationPaymentEnterprise.\nsubmitSubscriberPaymentDetails() over HTTP
                note right of TIBCO: which will inoke DB function of \nINSERT_MIGRATION_PAYMENT_QUE
    
    else Otherwise
                
                alt if       If senderID in Request Header is "2IP" or "BSP" or "UVM"
                                                TIBCO -> ACC : Call RechargeAccount()
                                                ACC -> TIBCO : Returns RechargeAccount response

                                else Otherwise
               TIBCO -> RPX : Call ApplyPayment()
                                                RPX -> TIBCO : return ApplyPayment response
                                end 
                end
    TIBCO->TIBCO: Response Data Transformation
    TIBCO->Client: applyPayment() Response

end 
@enduml
