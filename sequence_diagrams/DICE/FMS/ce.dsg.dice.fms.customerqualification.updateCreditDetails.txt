@startuml
actor Client
Client->RSP:[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/PROD/wsdl_api/html/CustomerQualificationService.html#updateCreditDetails  customerqualification.updateCreditDetails]]
group if    overrideReason as null or empty in request
RSP->FICO : Invokes [[https://as-test06.ficoanalyticcloud.com:443/om_apm_webservices/BAN UpdateCreditBanDetails]] to update CreditBanDetails
end 
group else overrideReason is not null or empty
RSP->FICO:Invokes [[https://as-test06.ficoanalyticcloud.com:443/om_apm_webservices/Update updateCreditDetails]] to update CreditDetails
FICO->RSP:FICO returns status 
end
RSP->RSPDB:RSPDB is invoked to select commonCustomerId
RSPDB->RSP:RSPDB returns commonCustomerId
group if request contain companyName

group if CHUB REST call disabled
RSP->CHUB: Invokes [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/chub/OrganizationService/resources OrganizationService.updateOrganizationCreditDetails]] to update Organization Credit Details
end
group else CHUB REST call enabled
RSP->CHUB:Invokes[[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/customerqualification/browse/src/main/resources/swaggers?at=refs%2Fheads%2Frelease%2FR2018.08.25 BusinessCustomerService.UpdateBusinessCustomerCreditProfileService]] to update Organization CreditDetails
end
end
group else
RSP->CHUB:Invokes [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/chub/PersonService/resources PersonService.updatePersonCreditDetails]] to update Person Credit Details
end
RSP->Client:update CreditDetailsResponse returns to client
@enduml