@startuml
title CustomerQualificationService.updateGraduatedCreditEligibility
actor Client

group <font color=#FF0090><b> UpdateGraduatedCreditEligibility

Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/CustomerQualificationService.html#updateGraduatedCreditEligibility CustomerQualificationService.UpdateGraduatedCreditEligibility]]
TIBCO->JMS:TIBCO will call the <b>JMS QUEUE</b> and post the message
JMS->METRO: Metro consume the message from queue  <b>METRO.GRADUATEDCREDITELIGIBILITY.UPDATE</b>
METRO-JMS:Returns response
JMS->TIBCO:Returns response
end

@enduml