@startuml
title Installment.GetInstallmentDetails
actor Client

group <font color=#FF0090><b> GetInstallmentDetails

Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/Installment.html#GetInstallmentDetails GetInstallmentDetails]]

ESP->TUXEDO:ESP (TIBCO) invokes the Samson Tuxedo api evGtSubSum to retrieve planType, plancontractTerm.
ESP->TUXEDO:ESP (TIBCO) invokes the Samson Tuxedo api csGtSub to retrieve subsidy type, subsidy date.
ESP->TUXEDO:ESP (TIBCO) invokes the Samson Tuxedo api evLsDepo to retrieve deposit info.
TUXEDO->TIBCO:Samson returns data requested.
TIBCO->Client:ESP (TIBCO) compiles the data returned from different Samson Tuxedos and sends it back to the client.
end
@enduml