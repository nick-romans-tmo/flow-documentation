@startuml
group if job is enabled (batch scheduler runs every 15 mins)
RSP->RSPDB: Invoke the RSPDB to select Activation Event log Info for events created within 48 hours
group if activationEventLogList is non null
loop over each activationEventLog pick form activationEventLogList
loop over each lineDetail picke from activationEventLog
group If getActivationType code is DVU from lineDetailLog
RSP->RSP:set ActivationType code as DVU to hotidetails
end
group else 
RSP->RSP:set ActivationType code as PAC to hotidetails
end
group validate is ValidImei
RSP->RSPDB:Invoke the RSPDB to select imei status
group if backendSystems contains ALL are isGrayMarketDevice and  not isGrayMarketDevice
RSP->RSP:The IMEI is not recognized by T-Mobile. It is gray market device
end
group else if backendSystems contains ALL are isNewDevice and not isNewDevice
RSP->RSP:The IMEI was already used on T-Mobile network
end
group else if backendSystems contains ALL are isBlockedDevice and isBlockedDevice
RSP->RSP:The IMEI is blocked
end
group else if backendSystems contains ALL are isEipActive and isEipActive
RSP->RSP:Active EIP exists for this IMEI
end 
group else if backendSystems contains ALL are isClassicEligible and isClassicEligible
RSP->RSP:The Subscriber is on classic plan with no BTV
end
group else
RSP->RSP: No Matching records found
end
end
RSPDB->RSP:RSPDB retruns  respons to RSP
RSP->SAMSON:Invokes [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/welcome.html evUpdSubsidy]] Tuxedo API evUpdSubsidy to Update the hoti value
RSP->RSPDB:Invoke the RSPDB to update hotidetails and ActivationEvent logs
end
end
end
end
@enduml