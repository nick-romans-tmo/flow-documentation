@startuml
title Installment.GetIneligibleSKUs
actor Client

group <font color=#FF0090><b> GetIneligibleSKUs

Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/Installment.html#GetIneligibleSKUs GetIneligibleSKUs]]
Client->TIBCO:Client applications (HSO) will invoke TIBCO service GetIneligibleSKUs
ESP->EIP:ESP (TIBCO) invokes the EIP service
EIP->TIBCO:EIP returns the in eligible SKUs details to TIBCO.
TIBCO->Client:ESP (TIBCO) returned sends it back to the client
end
@enduml
