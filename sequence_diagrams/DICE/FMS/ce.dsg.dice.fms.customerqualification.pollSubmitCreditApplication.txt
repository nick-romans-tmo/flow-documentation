@startuml
actor Client
Client->RSP: [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/customerqualification/browse/src/main/resources/service-api-definition customerqualification.pollSubmitCreditApplication]]
RSP->RSPDB: Call RSP DB Poll function to create AsyncRecord request object by setting AsyncToken and PartnerId \n [asyncComponentManager.poll(pollRequest)]
RSPDB->RSP: Based on query fetch and return the record as a response object

group if record.getResponseXml() is not Empty \n then unmarshall the record as record.getResponseXml().
RSP->RSP: initialize the pollingResponse object from (CustomerQualificationResponse) responseElement.getValue()
end


group if record.getResponseXml() is Empty then unmarshall the record.getRequestXml()
RSP->RSP: intialize asyncRequest object from (AsyncCustomerQualificationRequest) responseElement.getValue() 
RSP->RSP: Map the response object by mapPollResponse(asyncRequest.getCustomerQualificationRequest(), pollingResponse) function
group if record (isInProcess or isReceived)
RSP->RSP: then set statusReport as AsyncInProgress
end
group if record isProcessFailed
RSP->RSP: then set statusReport as AsyncFailure
end
RSP->RSP: set the success status in response with token in pollingResponse object
end


RSP->Client: return pollingResponse object to client
@enduml
