@startuml
group if job.isEnabled is true (scheduled batch job running every 30 mins)
RSP->RSPDB: Select all disclosures to dispose from RSP DB based on partner expiry logic

loop over each of the selected expired orders
RSP->DOCUSIGN :Invoke [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/docusign/dsapi/resources voidEnvelope]] to dispose envelopes
RSP->RSPDB: Update RSPDB with status for disposed envelopes

RSP->RSPDB: Select all ExpiredDocumentTransactionDetails
RSP->TIBCO: Invoke [[https://bitbucket.service.edp.t-mobile.com/projects/RSPCLOUD/repos/clientjargeneration/browse/tibco/Notification.wsdl sendEmail]] to send email to salesRep ExpiryMail
end

end
@enduml