@startuml
title Installment.CancelEIPOrderAsync
actor Client

group <font color=#FF0090><b> CancelEIPOrderAsync

Client->TIBCO: Client calls <b>[[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/Installment.html#CancelEIPOrderAsync Installment.cancelEIPOrderAsync()]]
TIBCO->RSP:Invokes RSP - <b>[[https://flows.corporate.t-mobile.com/all-diagrams/sequence_diagrams/dice/fms/ce.dsg.dice.fms.equipmentinstallmentprogram.asynccanceleiporder.txt/ EquipmentInstallmentProgram.asyncCancelEIPOrder()]]</b>\nby including equipmnet details like ban, reasonCode, transactionId,etc.
RSP->TIBCO:Returns the Service Status & referenceID
TIBCO->Client:Retuns the response back to the client.
end
@enduml
