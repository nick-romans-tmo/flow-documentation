@startuml

title <size:25>**CashDrawerService.manageCashDrawer**
actor Client #pink

activate TIBCO
Client -[#blue]> TIBCO : <color:blue>** 	manageCashDrawerRequest**
database QueryCashDrawerConfigSQL
activate QueryCashDrawerConfigSQL
TIBCO -> QueryCashDrawerConfigSQL :Post request to Configuration DB : \n **sqlmanager/query/getCashDrawerConfiguration**

note over QueryCashDrawerConfigSQL #aqua
	Fetches configured 
    cashDrawer details
end note

QueryCashDrawerConfigSQL -[#green]> UpdateConfigDB : <color:green> if **manageCashDrawerRequest/activity=ENABLE/DIABLE**
QueryCashDrawerConfigSQL -[#green]> CashDrawer : <color:green>  	Otherwise :: when **manageCashDrawerRequest/activity=OPEN** \n<color:green>		and **QueryCashDraw/ROW/DRAWER_ENABLED=1**

note over CashDrawer #aqua
	ESP invokes cashDrawer
    for OPEN Activity
end note
deactivate QueryCashDrawerConfigSQL

activate CashDrawer
participant CashDrawer
    group For-Each ROW
activate TCP    
CashDrawer -> TCP: Open TCP Connection
CashDrawer -> TCP: Write TCP Data
CashDrawer -> TCP: Read TCP Data
TCP -> CashDrawer: Retrun Status-close TCPConnection
end
deactivate TCP
database InsertCashDrawerConfigSQL
    CashDrawer -[#brown]> InsertCashDrawerConfigSQL : <color:brown>			adds transaction details performed on CashDrawer \n<color:brown>	into **transaction DB : sqlmanager/query/insertCashDrawerConfiguration**
activate InsertCashDrawerConfigSQL

    InsertCashDrawerConfigSQL -[#brown]> CashDrawer : <color:brown>	 							returns Status
    
deactivate InsertCashDrawerConfigSQL 
deactivate CashDrawer
    InsertCashDrawerConfigSQL <-[#red]> TIBCO : <color:red>					 if any Failure (DB error or TCP Connect/Read/Write Failed) : **responseStatusCode-102**
    
InsertCashDrawerConfigSQL <-[#green]> TIBCO : <color:green>     						OnSuccess :: when ESP invokes CashDrawer for Open activity successfully and adds transaction details performed on Cash Drawer into transaction DB successfully : **responseStatusCode-100**

TIBCO -[#blue]> Client : <color:blue>**Returns manageCashDrawerResponse**
deactivate TIBCO

@enduml
