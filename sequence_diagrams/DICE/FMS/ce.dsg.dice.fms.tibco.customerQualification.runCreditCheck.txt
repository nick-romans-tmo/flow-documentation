@startuml
title CustomerQualificationService.runCreditCheck
actor Client

group <font color=#FF0090><b> RunCreditCheck

Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/CustomerQualificationService.html#runCreditCheck CustomerQualificationService.RunCreditCheck]]

TIBCO->SAMSON: Tibco calls samson tuxedo [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/welcome.html csSrchSub]] to get associated accounts for the msisdn passed.


TIBCO->RSP: Call RSP [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/PROD/wsdl_api/html/CustomerQualificationService.html#runCreditCheck CustomerQualificationService.runCreditCheck]]
RSP->TIBCO: Returns runCreditCheck response.

TIBCO->Client: Tibco returns response back to clients.

end

@enduml
