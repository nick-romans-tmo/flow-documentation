@startuml
title Installment.CreateInstallmentPlan
actor Client

group <font color=#FF0090><b> CreateInstallmentPlan

Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/Installment.html#CreateInstallmentPlan CreateInstallmentPlan]]
HSO->TIBCO:HSO will invoke ESP (TIBCO) service CreateInstallmentPlan (Order detail ID should be passed as additional input parameter and eipPlan will be send as true if the subscriber/rateplan is EIP Eligible)
group If POIP is passed in Financetype
ESP->EIP:ESP will invoke EIP CreateResidualLoan Service
end
group If city and state are not passed in Input  
ESP->SAMSON:ESP will call Samson (csSrchBan) operation which returns city and State.
end
group if if BAN exists
ESP->ESP:ESP(TIBCO) calls GetEIPParameters ESP service for Account, Deposit and Subsidy Information related to an Account 
end
group if BAN does not exit
TIBCO->Tuxedo:TIBCO will call arInCstBan tuxedo to create a Tentative BAN and pass it to EIP.
TIBCO->TIBCO:ESP (TIBCO) invokes createEIPDis SOAP call by calling Retail RSP with the equipmnet details like eipOrderInfo, eipBuyerInfo, etc.
end
TIBCO->ESP:ESP(TIBCO) calls GetAddressForBan ESP service for State and City Information related to an Account when applicationId='NAT' or channelName='NAT' with city or state being null in CreateInstallmentPlanRequest.
ESP->EIP:ESP (TIBCO) will invoke EIP applications CreateEIP.createEIPPlan web service.
ESP->SAMSON:ESP (TIBCO) will invoke Samson Tuxedo API (arHdEipInd) to update EIP indicator as Y and pass the EIP period. If there is any error during creating installment plan then ESP will invoke EIP applications voidPlan web service.
group
HSO->ESP:HSO will invoke ESP (TIBCO) service CreateInstallmentPlan (earlyUpgrade is passed in request)
group If earlyUpgrade is true
ESP->HSO: migrationFeeTenure should be '-1'in the response
end
group else
ESP->HSO:migrationFeeTenure value will be taken from Samson.
end
end
Client->ESP:Client will invoke ESP(TIBCO) CreateInstallmentPlan.If InstallmentPlanId is passed in the request then PlanId will be created in EIP during estimate Installment
Client->ESP:Client will invoke ESP (TIBCO) service EstimateInstallmentPlan. Client will pass the the additional tier related information for when the price type is SCORE
group TIBCO will check transactionType
group If transactionType = ‘JUMP’,
TIBCO->EIP:TIBCO will call EIP service createJumpPlan
end
group else If transactionType is not ‘JUMP’
TIBCO->EIP:TIBCO will invoke EIP service CreateEIP.createEIPPlan, and follow the existing process.
end
end
TIBCO->TIBCO:TIBCO to invoke InstallmentPlanEstimate. getInstallmentPlanEstimate web service
TIBCO->TIBCO:ESP (TIBCO) internally stores the CustomerIdentification with created EIP plan id for enabling ESP to send the CustomerIdentification details to TDFADS system through SFTP
TIBCO->TIBCO:ESP (TIBCO) calls GetIMEIStatus ESP(TIBCO) Service to set Description of each PurchasedEquipment in CreateInstallmentPlanResponse.
TIBCO->TIBCO:ESP (TIBCO) returns a response to indicate installment plan has been created (Refresh CreateEIP. createEIPPlan EIP WSDL and map lastAmountOfPayment, lastPaymentDueDate in confirmationStatementParams)only for POS..
end
@enduml