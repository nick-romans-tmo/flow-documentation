@startuml
title EquipmentInstallment.UpdateEIPWithMSISDN
actor Client

group <font color=#FF0090><b> UpdateEIPWithMSISDN

Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-03-16/wsdl_api/wsdl/Installment.wsdl UpdateEIPWithMSISDN]]

group if accessoriesFinanced = "true"
TIBCO->SAPPI: calls <b>SubscriberOrderDetailI</b>
SAPPI->TIBCO: returns SubscriberOrderDetailIDsResponse

group if :EIPRelevantFlag = "Y" in SubscriberOrderDetailIDsResponse
TIBCO->EIP: Calls <b> UpdateEquipment.updateEquipment</b>
EIP->TIBCO:
end 

end

alt else otherwise
TIBCO->EIP: Calls <b> UpdateEquipment.updateEquipment</b>
EIP->TIBCO:

TIBCO->Client: TIBCO returns resoponse to client
@enduml1
