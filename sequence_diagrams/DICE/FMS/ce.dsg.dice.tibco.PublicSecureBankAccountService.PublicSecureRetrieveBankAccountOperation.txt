
@startuml
title PublicSecureRetrieveBankAccount.publicSecureRetrieveBankAccountOperation
actor Client

group <font color=#FF0090><b> publicSecureRetrieveBankAccountOperation     
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/PublicSecureRetrieveBankAccount.html#publicSecureRetrieveBankAccountOperation publicSecureRetrieveBankAccountOperation]]
TIBCO->TSafe:Retrieve Bank Account
TSafe->TIBCO: Response with BankAccount Number,Transaction Id
TIBCO->Client:Response

end 

@enduml



