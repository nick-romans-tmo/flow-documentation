
@startuml

title ** __Customer Profile (CPD) Service saveProvisionData Operation__ **\n

actor TIBCO

TIBCO->CustomerProfileService: saveProvisionData Request (XML over Http) [[https://servicecatalog.internal.t-mobile.com/servicecat/cp/2016-12-14/wsdl_api/html/CustomerProfileService.html Service Description]]

CustomerProfileService->provisionManager: provisionPayload (Request)

provisionManager->profileDetailsDao: saveProvisionData

provisionManager-->provisionManager: if (activate/enhance/resync)

provisionManager-->provisionManager: provisionContactDetails()

provisionManager-->provisionManager: create profile with requested data

provisionManager->profileManager: saveCustomerProfile returns profileId

profileManager-->provisionManager: returns profileId

provisionManager->profileDetailsDao: getProvisionEnhanceQueueDetails (if enhance event)

profileDetailsDao-->provisionManager: returns enhanceQueue response

provisionManager->profileDetailsDao: deleteProvisionEnhanceQueueDetails (enhanceQueue)

provisionManager-->CustomerProfileService: return profileId as Response

CustomerProfileService-->CustomerProfileService: map to saveProvisionData response

CustomerProfileService-->TIBCO: return saveProvisionData Response

@enduml