@startuml
title Customer Details Search End to End Flow
participant Rebellion
participant "SAP Care"
participant WSIL
participant APIGEE
participant "CustomerDetailsSearch PCF API"
participant "CHUB Replica Tables"
participant "SAMSON Tuxedo evGtAuthSub"
participant "IAM validatePIN API"
participant "CHUB CustomerDetailsLookupService"
participant "CFAM Customer Credit Profile Service API"
 
Rebellion->Rebellion : Get Customer Details
"SAP Care"->"SAP Care" : Get Customer Details
Rebellion->APIGEE : /customer/v1/customers/customerDetails/search
"SAP Care"-> WSIL : CustomerDetailsWSIL.getCustomerDetails
WSIL->APIGEE : /customer/v1/customers/customerDetails/search
APIGEE->"CustomerDetailsSearch PCF API" : /customer/v1/customers/customerDetails/search
group Customer PIN Validation is not required
alt securitryPIN is null ->Customer PIN validation not Required
 "CustomerDetailsSearch PCF API"->"CHUB CustomerDetailsLookupService": lookupCustomerDetails
    "CHUB CustomerDetailsLookupService"-->"CustomerDetailsSearch PCF API" : CustomerDetails Response
    "CustomerDetailsSearch PCF API"-> "CustomerDetailsSearch PCF API": get CCID and Credit Class from lookupCustomerDetails Response
    "CustomerDetailsSearch PCF API"-> APIGEE: POST /customer-credit/v1/credit-profiles/personal
     APIGEE->"CFAM Customer Credit Profile Service API": POST /customer-credit/v1/credit-profiles/personal for Equipment Credit Limits (MPEC,LCA,CECL,ECA and ECB) based on CCID and Credit Class
    "CFAM Customer Credit Profile Service API"--> APIGEE: Provide Equipment Credit Limits
     APIGEE->"CustomerDetailsSearch PCF API" : Provide Equipment Credit Limits
     note left 
              Ignore if 
              CFAM Customer Credit Profile Service API
              call fails
    end note
"CustomerDetailsSearch PCF API"-->APIGEE:send Customer Details Response
else 
end
end
group Customer PIN Validation in U2.0
	alt securityPIN is not null & BSCSCustomerID is not null
	"CustomerDetailsSearch PCF API"->"IAM validatePIN API": validatePIN
    "IAM validatePIN API"-->"CustomerDetailsSearch PCF API": Provide validatePIN response
	alt PIN validation failure
	"CustomerDetailsSearch PCF API"-->APIGEE: Send error Response that PIN validation failed
	else 
    alt PIN validation is sucessful
    "CustomerDetailsSearch PCF API"->"CHUB CustomerDetailsLookupService": lookupCustomerDetails
    "CHUB CustomerDetailsLookupService"-->"CustomerDetailsSearch PCF API" : CustomerDetails Response
    "CustomerDetailsSearch PCF API"-> "CustomerDetailsSearch PCF API": get CCID and Credit Class from lookupCustomerDetails Response
    "CustomerDetailsSearch PCF API"-> APIGEE: POST /customer-credit/v1/credit-profiles/personal
     APIGEE->"CFAM Customer Credit Profile Service API": POST /customer-credit/v1/credit-profiles/personal for Equipment Credit Limits (MPEC,LCA,CECL,ECA and ECB) based on CCID and Credit Class
    "CFAM Customer Credit Profile Service API"--> APIGEE: Provide Equipment Credit Limits
     APIGEE->"CustomerDetailsSearch PCF API" : Provide Equipment Credit Limits
     note left 
              Ignore if 
              CFAM Customer Credit Profile Service API
              call fails
    end note
    "CustomerDetailsSearch PCF API"-> "CustomerDetailsSearch PCF API": Enhance reponse with Equipment Credit Limits
    "CustomerDetailsSearch PCF API"-->APIGEE: Send Customer Details Response
    end

else
end
end
end
 
group Customer PIN Validation in U1.0
	alt securityPIN is not null & BSCSCustomerID is null & AccountNumber is not null
	"CustomerDetailsSearch PCF API"->"CHUB Replica Tables" : get PIN, Account Type Sub Type based on BAN
    "CustomerDetailsSearch PCF API"->"CustomerDetailsSearch PCF API" : validate PIN provided in the input with PIN available in the table
    alt PIN validation failure
	"CustomerDetailsSearch PCF API"-->APIGEE: Send error Response that PIN validation failed
    else
    end
    alt PIN validation is sucessful
	"CustomerDetailsSearch PCF API"->"CHUB CustomerDetailsLookupService" : lookupCustomerDetails by mapping CCID, Account Type and Sub Type (This is for Credit Limits)
	"CHUB CustomerDetailsLookupService"-->"CustomerDetailsSearch PCF API" : CustomerDetails Response
     "CustomerDetailsSearch PCF API"-> "CustomerDetailsSearch PCF API": get CCID and Credit Class from lookupCustomerDetails Response
    "CustomerDetailsSearch PCF API"->"CFAM Customer Credit Profile Service API": get Equipment Credit Limits (MPEC,LCA,CECL,ECA and ECB) based on CCID and Credit Class
    "CFAM Customer Credit Profile Service API"--> "CustomerDetailsSearch PCF API" : Provide Equipment Credit Limits
     note left 
              Ignore if 
              CFAM Customer Credit Profile Service API
              call fails
    end note
    "CustomerDetailsSearch PCF API"-> "CustomerDetailsSearch PCF API": Enhance reponse with Equipment Credit Limits
    "CustomerDetailsSearch PCF API"->"SAMSON Tuxedo evGtAuthSub" : get Authorized users based on BAN provided in the input
    "SAMSON Tuxedo evGtAuthSub"-->"CustomerDetailsSearch PCF API": Provide Authorized user Details
    note left 
              Ignore if 
              SAMSON Tuxedo
              call fails
    end note
    alt if Authorized users info. is available
	"CustomerDetailsSearch PCF API"->"CustomerDetailsSearch PCF API" : Enhance the reponse mapping with Authorized Users of the BAN
    else
    end
	"CustomerDetailsSearch PCF API"-->APIGEE :Send Customer Details Response
	else
	end
    end
end

APIGEE-->Rebellion : Route Customer Details Response
APIGEE-->WSIL : Route Customer Details Response
WSIL-->"SAP Care" : Provide Customer Details Response
@enduml
 
 