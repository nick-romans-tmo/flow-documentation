@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> RatePlanManagement.validateServiceChange Flow</font>
actor client
participant "RSP"
participant SAMSONTUXEDO
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/RatePlanManagementService.html#validateServiceChange validateServiceChange]]RatePlanManagement.validateServiceChange
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* validateServiceChange statusReport.isNotGood 
end note
alt Validation for validateServiceChange failed
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> SAMSONTUXEDO :Calling SAMSONTUXEDO validateServiceChange 
activate SAMSONTUXEDO
"RSP" <-- SAMSONTUXEDO : validateServiceChange 
deactivate SAMSONTUXEDO
"RSP" --> "client": validateServiceChange response
deactivate
@enduml