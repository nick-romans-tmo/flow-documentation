@startuml
title CustomerPlanService.changePlanAndServices
actor Client
group <font color=#FF0090><b> changePlanAndServices
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-04-13/sid/html/CustomerPlanService.html#changePlanAndServices changePlanAndServicesRequest]]     
              group <b> If servicePartnerId='METRO' and pin is not present in the request
              TIBCO -> AAM: SubscriberSv_V5_HttpService.changeDeviceAndServiceOp
	          AAM-> AAM: [[http://localhost:9080/AmdocsSubscriberMed_V5Web/sca/SubscriberSv_V5]]
	          AAM-> TIBCO : changeDeviceAndServiceOpResponse
              end
              group <b>  If servicePartnerId='METRO' and pin is present in the request           
              TIBCO -> AAM: SubscriberSv_V5_HttpService.secureChangeDeviceAndServicesOp
	          AAM-> AAM: [[http://localhost:9080/AmdocsSubscriberMed_V5Web/sca/SubscriberSv_V5]]
	          AAM-> TIBCO : secureChangeDeviceAndServicesOpResponse
              end
              group <b> Otherwise
               group <b> If senderId or channelId or applicationId is 'VINLI' and serviceChange is present in the request
                  TIBCO -> SAMSON: Call GetBasicAccountInfoForBANTux
	          SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evGtCustInfo.html evGtCustInfo]]
	          SAMSON -> TIBCO : RfcResponse_evGtCustInfo_Output
                  TIBCO -> SAMSON: Call ValidateServiceAgreementTux
	          SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evValSrvAgr.html evValSrvAgr]]
	          SAMSON -> TIBCO : RfcResponse_evValSrvAgr_Output
                  TIBCO->RSP:RatePlanManagementService.saveServiceChange saveServiceChangeRequest
                  RSP-> RSP: [[http://localhost:7001/eProxy/service/RatePlanManagementService_SOAP_V1]]
                  RSP->TIBCO: saveServiceChangeResponse
                  end
              group <b> Otherwise
                  TIBCO->RSP:PostpaidAccountService.getBasicAccountDetails getBasicAccountDetailsRequest
                  RSP-> RSP: [[http://localhost:7001/eProxy/service/PostPaidAccountService_SOAP_V1]]
                  RSP->TIBCO: getBasicAccountDetailsResponse
                    TIBCO->RSP:RatePlanManagementService.saveServiceChange saveServiceChangeRequest
                  RSP-> RSP: [[http://localhost:7001/eProxy/service/RatePlanManagementService_SOAP_V1]]
                  RSP->TIBCO: saveServiceChangeResponse
              end        
                   end
TIBCO -> Client : changePlanAndServicesResponse
end
@enduml