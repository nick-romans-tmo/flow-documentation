@startuml
title AccountLookup.GetAccountDetails
actor Client
group <font color=#FF0090><b> GetAccountDetails
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-05-18/sid/html/AccountLookup.html#GetAccountDetails esiGetAccountDetailsRequest]] 
    group <b> if version >= 3
        group GetAccountsByMSISDN [if ban does not exists and msisdn exists]
            TIBCO -> TIBCO : SamsonAccountService.GetAccountsByMSISDN
                group SamsonAccountService.GetAccountsByMSISDN [<font color=#FF0090> /SAMSON/ACCOUNT/ACCOUNTSEARCH/GETACCOUNTSBYMSISDN/]
                    TIBCO -> SAMSON : csiGetAccountsByMSISDNRequest
                    SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/csSrchSub.html csSrchSub]] 
                    SAMSON -> TIBCO : csiGetAccountsByMSISDNResponse
                end
            TIBCO --> TIBCO : csiGetAccountsByMSISDNResponse
        end
        group GetSubscribersForBan [if subscriberListOnly = "true"]
            TIBCO -> TIBCO : SamsonAccountService.GetSubscribersForBan
                group SamsonAccountService.GetSubscribersForBan [<font color=#FF0090> /SAMSON/SUBSCRIBER/SUBSFORBAN/GET/]
                    TIBCO -> SAMSON : csiGetSubscribersForBanRequest
                    SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/csLsBanSub.html csLsBanSub]] [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evLsSubInfo.html evLsSubInfo]]
                    SAMSON -> TIBCO : csiGetSubscribersForBanResponse
                end
            TIBCO --> TIBCO : csiGetAccountsByMSISDNResponse
        end
    end
    group <b> otherwise
        group GetAccountDetails
            TIBCO -> TIBCO : SamsonAccountService.GetAccountDetails
            group <b> SamsonAccountService.GetAccountDetails [<font color=#FF0090> /SAMSON/ACCOUNT/ACCOUNTLOOKUP/GETACCOUNTDETAILS]
                TIBCO -> SAMSON : GetAccountDetails - csiGetAccountDetailsRequest
                SAMSON -> SAMSON: [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evGtCustType.html evGtCustType]]
                SAMSON --> TIBCO :csiGetAccountDetailsResponse
            end
            TIBCO -> TIBCO : csiGetAccountDetailsResponse
        end
    end
    group GetSubscriberStatusCount
            TIBCO -> TIBCO : SamsonSubsQueryService.GetSubscriberStatusCount
            group <b> SamsonSubsQueryService.GetSubscriberStatusCount [<font color=#FF0090> /SAMSON/SUBSCRIBER/STATUSCOUNT/GET/]
                TIBCO -> SAMSON : GetSubscriberStatusCount - csiGetSubscriberStatusCountRequest
                SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/csSubStCnt.html csSubStCnt]]
                SAMSON --> TIBCO :csiGetSubscriberStatusCountResponse
            end
            TIBCO -> TIBCO : csiGetSubscriberStatusCountResponse
    end
TIBCO -> Client : esiGetAccountDetailsResponse
end
@enduml