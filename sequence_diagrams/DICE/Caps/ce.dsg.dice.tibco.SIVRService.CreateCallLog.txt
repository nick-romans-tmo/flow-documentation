@startuml
title SIVRService.CreateCallLog
actor Client
group <font color=#FF0090><b> CreateCallLog [<b> /speechivr/ivr/ivr/callhistory/create]
Client->TIBCO: CreateCallLogRequest
    group Acount-Activity-Event-Sender
        TIBCO -> JMS : [<b>QueueName</b> = TMOBILE.<font color=#FF0090>$TMO-Env-Env_Name_IVR</font>.IVR.CALLRECORD.CREATE]
    end
TIBCO -> Client : saveCallHistory
end
@enduml