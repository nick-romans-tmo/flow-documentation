@startuml
title AccountSearch.GetAccountsBySSN
actor Client
group <font color=#FF0090><b> GetAccountsBySSN
Client->AccountSearchService: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/AddressService.html#getAccountsBySSN GetAccountsBySSN]]  AccountSearch.GetAccountsBySSN
AccountSearchService->ESP:Receiving request
ESP->SAMSON:ESP sending to the samson spoke
SAMSON->SAMSON :validate the input And Pass the SSN value to tuxedo service - csLsBnSsn
alt
SAMSON->ESP:Return account summary details in response
SAMSON->AccountSearchService: if any validation error occurs, It will throw a fault message to client
end group
ESP->AccountSearchService:ESP send the account summary details as success response
AccountSearchService->Client:GetAccountsBySSN Response to client
end group
@enduml