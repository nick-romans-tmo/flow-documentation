@startuml
title Address.GetAddrLatLong	
actor Client
group <font color=#FF0090><b> GetAddrLatLong
Client->AddressService: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/AddressService.html#validateAddress GetAddrLatLong]]  AddressService.GetAddrLatLong

AddressService->SAMSON:Samson Billing system get the Lattitude,values based on the the address that is passed in the request by calling the gnGtLatLong Samson Tuxedo service
alt
SAMSON->AddressService:return gnGtLatLong Samson Tuxedo response
SAMSON->AddressService:If there is any failure, error will be thrown in the response status
end group
AddressService->Client:getAddrLatLong response to client
end group
@enduml