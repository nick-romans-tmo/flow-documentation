@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> PrePaidService.getPrePaidSubscriberProfile Flow</font>
actor client
participant "RSP"
participant RPCDB
participant RSPDB
participant SamsonTuxedo
participant LineOfServiceDetailsWSIL
participant FinancialAccountWSIL
participant CustomerSummaryWSIL
participant CustomerDetailsWSIL
participant CustomerProfile
participant HttpServicesPort
participant CheckPrepaidSubscriberServicePortType
participant MSISDNSSNLookupService
participant SERVER
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/PrePaidService.html#getPrePaidSubscriberProfile getPrePaidSubscriberProfile]]PrePaidService.getPrePaidSubscriberProfile
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* getPrePaidSubscriberProfileRequest Request
end note
alt usn is must
"RSP" --> "client": ERRORMESSAGE
end
activate RPCDB
"RSP" -> RPCDB :Calling RetailProductCatalog.getMarketCode
"RSP" <-- RPCDB : getMarketCode
deactivate 
activate SamsonTuxedo
"RSP" -> SamsonTuxedo :Calling Tuxedo_icGtSit
"RSP" <-- SamsonTuxedo : retrieveImsiForSim
deactivate
activate LineOfServiceDetailsWSIL
"RSP" -> LineOfServiceDetailsWSIL :Calling LineOfServiceDetails
"RSP" <-- LineOfServiceDetailsWSIL : GetLineOfServiceDetails
deactivate
activate FinancialAccountWSIL
"RSP" -> FinancialAccountWSIL :Calling getPrepaidBalance
"RSP" <-- FinancialAccountWSIL : getPrepaidBalance
deactivate
activate CustomerSummaryWSIL
"RSP" -> CustomerSummaryWSIL :Calling searchCustomer
"RSP" <-- CustomerSummaryWSIL : searchCustomer
deactivate
activate CustomerDetailsWSIL
"RSP" -> CustomerDetailsWSIL :Calling getCustomerDetails
"RSP" <-- CustomerDetailsWSIL : getCustomerDetails
deactivate
activate HttpServicesPort
"RSP" -> HttpServicesPort :Calling executeActivateTrialSubscriber
"RSP" <-- HttpServicesPort : ActivateTrialSubscriber
deactivate
activate CheckPrepaidSubscriberServicePortType
"RSP" -> CheckPrepaidSubscriberServicePortType :Calling checkPrepaidSubscriberAccountForDeviceUpgradeEligibility
"RSP" <-- CheckPrepaidSubscriberServicePortType : checkPrepaidSubscriberAccountForDeviceUpgradeEligibility
deactivate
activate MSISDNSSNLookupService
"RSP" -> MSISDNSSNLookupService :Calling lookupMSISDNSSN
"RSP" <-- MSISDNSSNLookupService : lookupMSISDNSSN
deactivate
activate "SERVER"
RSP -> SERVER: if gemfire enabled sending PARTNERID from Request
alt GEMFIRE ENABLED
SERVER -> RSP: getProfileDetails
deactivate SERVER
else GEMFIRE DISABLED
activate RSPDB
RSP -> RSPDB :Calling getPartnerProfileDetails sql call with requested inputs-sending PARTNERID from Request
RSP <-- RSPDB : getPartnerProfileDetails
deactivate RSPDB
end
"RSP" --> "client": PrePaidSubscriberProfile response
@enduml











