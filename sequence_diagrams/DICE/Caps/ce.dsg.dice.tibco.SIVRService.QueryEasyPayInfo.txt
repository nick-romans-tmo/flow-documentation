@startuml
title SIVRService.QueryEasyPayInfo
actor Client
group <font color=#FF0090><b> QueryEasyPayInfo [<b>/ivr/sivr/subscriber/easypayinfo/query]
Client->TIBCO: SivrQueryEasyPayInfoInput
    group SamsonPaymentService.SamsonQueryDirectDebit [<font color=#FF0090><b>/SAMSON/DIRECTDEBIT/QUERY/</font>]
        TIBCO -> SQLManager : BAN
        SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetEasyPayIndicator&action=query&keyValue= sqlmanager/query/GetEasyPayIndicator]]
        TIBCO <- SQLManager : GetEasyPayIndicator
        group QueryDirectDebit [if AUTO_GEN_PYM_TYPE != "R" or APPLICATION_ID="ESERVICE"]
            TIBCO -> SAMSON : DirectDebit
            SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/205/services/arGtCrdCd.html arGtCrdCd]]  
            SAMSON -> TIBCO : DirectDebit
        end
    end   
TIBCO -> Client : SivrQueryUsageOutput
end
@enduml