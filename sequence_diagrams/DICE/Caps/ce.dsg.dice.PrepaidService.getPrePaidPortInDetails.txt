@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> PrepaidService.getPrePaidPortInDetails Flow</font>
actor client
participant "RSP"
participant HttpServicesPort
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/PrepaidService.html#getPrePaidPortInDetails getPrePaidPortInDetails]]PrepaidService.getPrePaidPortInDetails
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* getPrePaidPortInDetailsRequest Request
end note
alt usn is must
"RSP" --> "client": ERRORMESSAGE
end
activate HttpServicesPort
"RSP" -> HttpServicesPort :Calling HttpServices
"RSP" <-- HttpServicesPort : queryPortInDetails
deactivate 
"RSP" --> "client": queryPortInDetails  response
@enduml











