@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> Offers.getClassicToValue Flow</font>
actor client
participant "RSP"
participant SAMSONTUXEDO
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/OfferService.html#getClassicToValue getClassicToValue]]Offer.getClassicToValue
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* getClassicToValue request :activitivity Detail
end note
alt Invalid Input, For the activity MSISDN is required.
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> SAMSONTUXEDO :Calling GetEligiblePricePlanTypeService
activate SAMSONTUXEDO
"RSP" <-- SAMSONTUXEDO : getClassicToValue 
deactivate SAMSONTUXEDO
"RSP" --> "client": getClassicToValue response
deactivate
@enduml