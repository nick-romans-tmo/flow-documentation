@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> RatePlanManagement.validateAndSaveServiceChange Flow</font>
actor client
participant "RSP"
participant TIBCO
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/RatePlanManagementService.html#validateAndSaveServiceChange validateAndSaveServiceChange]]RatePlanManagement.validateAndSaveServiceChange
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* validateAndSaveServiceChange statusReport.isNotGood 
end note
alt provide mandatory details
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> TIBCO :Calling TIBCO validateAndSaveServiceChange 
activate TIBCO
"RSP" <-- TIBCO : validateAndSaveServiceChange 
deactivate TIBCO
"RSP" --> "client": validateAndSaveServiceChange response
deactivate
@enduml