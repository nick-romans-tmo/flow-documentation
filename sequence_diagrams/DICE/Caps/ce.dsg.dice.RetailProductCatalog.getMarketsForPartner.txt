@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> RetailProductCatalog.getMarketsForPartner Flow</font>
actor client
participant "RSP"
participant RSPDB
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/RetailProductCatalogService.html#getMarketsForPartner getMarketsForPartner]]RetailProductCatalog.getMarketsForPartner
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* getMarketsForPartner request.getPartnerId
end note
alt No such partner exist.
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> RSPDB :Calling RetailProductCatalog.getMarketsForPartner sql call with requested inputs
activate RSPDB
"RSP" <-- RSPDB : getMarketsForPartner
deactivate
"RSP" --> "client": marketsForPartner Response
@endumln