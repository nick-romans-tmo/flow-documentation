@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> Portin.activateFailedPortIn Flow</font>
actor client
participant "RSP"
participant ActivateFailedPortIn
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/PortInService.html#activateFailedPortIn activateFailedPortIn]]PortInService.activateFailedPortIn

activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* activateFailedPortinRequest request.getCityName
end note
alt Invalid Input, City name is required 
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> ActivateFailedPortIn :Calling HttpServicePort service
activate ActivateFailedPortIn
"RSP" <-- ActivateFailedPortIn : activateFailedPortIn
deactivate
"RSP" --> "client": activateFailedPortIn response
deactivate
@enduml


