@startuml
title CustomerPlanService.validatePlanAndServices
actor Client
group <font color=#FF0090><b> validatePlanAndServices
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-04-13/sid/html/CustomerPlanService.html#validatePlanAndServices validatePlanAndServicesRequest]]     
              group <b> If senderId or channelId or applicationId is 'VINLI' in the request
                TIBCO -> SAMSON: Call GetBasicAccountInfoForBANTux
	          SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evGtCustInfo.html evGtCustInfo]]
	          SAMSON -> TIBCO : RfcResponse_evGtCustInfo_Output
               TIBCO -> SAMSON: Call ValidateServiceAgreementTux
	          SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evValSrvAgr.html evValSrvAgr]]
	          SAMSON -> TIBCO : RfcResponse_evValSrvAgr_Output
              end
             group <b> Otherwise
             TIBCO->RSP:PostpaidAccountService.getBasicAccountDetails getBasicAccountDetailsRequest
                  RSP-> RSP: [[http://localhost:7001/eProxy/service/PostPaidAccountService_SOAP_V1]]
                  RSP->TIBCO: getBasicAccountDetailsResponse
               TIBCO->RSP:RatePlanManagementService.validateServiceChange validateServiceChangeRequest
                  RSP-> RSP: [[http://localhost:7001/eProxy/service/RatePlanManagementService_SOAP_V1]]
                  RSP->TIBCO: validateServiceChangeResponse
              end              
TIBCO -> Client : validatePlanAndServicesResponse
end
@enduml