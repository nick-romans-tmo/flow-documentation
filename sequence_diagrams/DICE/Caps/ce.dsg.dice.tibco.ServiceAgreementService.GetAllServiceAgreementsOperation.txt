@startuml
title ServiceAgreementService.GetAllServiceAgreementsOperation
actor Client
group <font color=#FF0090><b> GetAllServiceAgreementsOperation
Client -> TIBCO : [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-05-18/sid/html/GetServiceAgreement.html#GetAllServiceAgreementsOperation GetAllServiceAgreementsRequest]]
group <b> RetrievePHPCosts [if csiGetAllServiceAgreementsRequest/retrievePHPCosts = true]
TIBCO -> SQLManager: [[sqlmanager/query/getPHPSocInsurance GetPHPSocInsurance]]
TIBCO <- SQLManager:  PHPSocInsurance
end
group <b> Otherwise (GetAllServiceAgreements)
TIBCO -> TIBCO : csiGetAllServiceAgreementsRequest
group <b> SamsonSubsAgreementService.GetAllServiceAgreements [<font color=#FF0090> SAMSON/SUBSCRIBER/SERVICEAGREEMENT/GETALL/]
       TIBCO -> SAMSON : csiGetAllServiceAgreementsRequest
        SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/csSrchSub.html csSrchSub]] [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evLsSubSrvSum.html evLsSubSrvSum]] [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evLsPpSrvTp.html evLsPpSrvTp]] [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evGtSubSum.html evGtSubSum]]
            TIBCO -> SQLManager : SQLManager.getUsageRate
            TIBCO <- SQLManager : SQLManager.getUsageRate
            TIBCO -> TIBCO : [[http://localhost:8085/ProductLookup.asmx ProductLookup.GetCompatibleServicesForRatePlanAndDevice]]
            TIBCO -> TIBCO : [[http://localhost:8085/ProductLookup.asmx ProductLookup.GetWeb2GoDeviceServiceCompatibility]]
            TIBCO -> SQLManager : SQLManager.getClassicToValueSocDetails
            TIBCO <- SQLManager : SQLManager.getClassicToValueSocDetails
            SAMSON --> TIBCO :csiGetAllServiceAgreementsRequest
end
TIBCO <- TIBCO : csiGetAllServiceAgreementsResponse
end
TIBCO -> Client : GetAllServiceAgreementsResponse
end
@enduml