@startuml
title EnterpriseUsageService.UpdateSubscriberDataStash
actor Client
group <font color=#FF0090><b> UpdateSubscriberDataStash
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-04-13/sid/html/Usage.html#UpdateSubscriberDataStash esiUpdateSubscriberDataStashRequest]] 
TIBCO -> TIBCO : Call SamsonUsage.UpdateSubscriberDataStash
group <b> SamsonUsage.UpdateSubscriberDataStash[<font color=#FF0090> /SAMSON/USAGE/UPDATESUBSCRIBERDATASTASH</font>] : csiUpdateSubscriberDataStashRequest
    TIBCO -> SAMSON: Call UpdateSubscriberDataStashTux
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evStshDataAdj.html evStshDataAdj]]
	SAMSON -> TIBCO : csiUpdateSubscriberDataStashResponse
end
TIBCO -> TIBCO : esiUpdateSubscriberDataStashResponse
TIBCO -> Client : esiUpdateSubscriberDataStashResponse
end
@enduml