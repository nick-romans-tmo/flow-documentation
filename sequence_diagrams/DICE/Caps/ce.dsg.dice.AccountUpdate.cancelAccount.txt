@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> AccountUpdate.cancelAccount Flow</font>
actor client
participant "RSP"
participant SAMSONTUXEDO
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/AccountUpdateService.html#cancelAccount cancelAccount]]Activation.cancelAccount
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* cancelAccount request.getCustomerByBAN
end note
alt validateQueryBillFormatRequest:CustomerData not Found
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> SAMSONTUXEDO :Calling RSPCreateMemoService
activate SAMSONTUXEDO
"RSP" <-- SAMSONTUXEDO : cancelAccount
deactivate 
"RSP" --> "client": cancelAccount response
deactivate
@enduml