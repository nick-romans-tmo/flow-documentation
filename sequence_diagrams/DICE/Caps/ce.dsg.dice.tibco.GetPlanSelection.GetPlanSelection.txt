@startuml
title GetPlanSelection.GetPlanSelection
actor Client
group <font color=#FF0090><b> esiGetPlanSelectionRequest
Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-04-13/sid/html/GetPlanSelection.html#GetPlanSelectionOperation esiGetPlanSelectionRequest]]
TIBCO -> TIBCO : Call SamsonSubsQuery.GetPlanSelection[<font color=#FF0090> /SAMSON/SUBSCRIBER/PLANSELECTION/QUERY</font>] : esiGetPlanSelectionRequest to csiGetPlanSelectionRequest
TIBCO -> TIBCO : GetPlanSelectionOrch
               group <b> GetPlanSelectionOrch
               TIBCO -> TIBCO :GetSubscriberAndLogicalDate
               TIBCO -> TIBCO :GetPricePlanServiceAgreement
               TIBCO -> TIBCO :GetPlanSelections
               TIBCO -> TIBCO :FilterPlansForPlanType
               group <b> If version from request is >=2 and for both eligileplans and Ineligileplans planID>0 and SocIndicator = "NEO" or SocIndicator = "FPN" or SocIndicator = "CLS" or SocIndicator = "FPC"
               TIBCO->SAMSON:  GetLogicalDateTux,GetFeatureCategoryForPricePlanTux
               SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/evLsPpSrvTp.html evLsPpSrvTp]]
               SAMSON->TIBCO: RfcResponse_evLsPpSrvTp_Output
               TIBCO -> TIBCO : csiGetPlanSelectionResponse to esiGetPlanSelectionResponse               
               end
               group <b> Otherwise
               TIBCO -> TIBCO : csiGetPlanSelectionResponse to esiGetPlanSelectionResponse              
               end                 
                end
group <b> GetSubscriberAndLogicalDate 
               group <b> If ban is present in request
               TIBCO->SQLMGR: getaccountdetails
               SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetAccountDetails_Dual&action=query&keyValue=]]
		SQLMGR->TIBCO: Customer
                end
                group <b> Otherwise
                TIBCO->SQLMGR: getSubscriberDetails
                SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetSubscriberDetails_Dual&action=query&keyValue= GetSubscriberDetails]]
		SQLMGR->TIBCO: ListOfSubscriber
                end
                group <b> If EnableMKBValidation = "Y" and servicePartnerId is present in the request
                TIBCO->SAMSON:  GetLogicalDateTux,GetServicePartnerTux
                SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/evGtCustType.html evGtCustType]]
                SAMSON->TIBCO: RfcResponse_evGtCustType_Output
                end
                group <b> If checkSocEligibility=true
                TIBCO->SQLMGR: getBillInfo
                SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetBillInfo&action=query&keyValue=]]
		SQLMGR->TIBCO: OneFullPayment
                end
                group <b> If checkSocEligibility=true and credit class is not present
                TIBCO->SQLMGR: getaccountdetails
                SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetAccountDetails_Dual&action=query&keyValue=]]
		SQLMGR->TIBCO: Customer                
                end  
                end
group <b> GetPricePlanServiceAgreement
    TIBCO->TIBCO : SamsonQueryServiceAgreementABP
end
             group <b> SamsonQueryServiceAgreementABP
              group <b> If EnableGetServiceAgreementTux GV is set as true
              TIBCO->SAMSON:  GetLogicalDateTux,GetServiceAgreementTux
              SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/evLsSubSrvSum.html evLsSubSrvSum]]
              SAMSON->TIBCO: RfcResponse_evLsSubSrvSum_Output
              end 
              group <b> Otherwise
              TIBCO->SQLMGR: getserviceagreement
              SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetServiceAgreement_Dual&action=query&keyValue=]]
	      SQLMGR->TIBCO: ListOfServiceAgreement
              end 
              group <b> If exists ProductCatalogInd and socIndicators
              TIBCO->SAMSON:  ProductLookup
              SAMSON -> SAMSON : [[http://localhost:8085/ProductLookup.asmx ProductLookup.GetWeb2GoDeviceServiceCompatibility]]
              SAMSON->TIBCO: GetWeb2GoDeviceServiceCompatibilityResponse
              end 
              group <b> Otherwise
              TIBCO->SAMSON:  ProductLookup
              SAMSON -> SAMSON : [[http://localhost:8085/ProductLookup.asmx ProductLookup.GetCompatibleServicesForRatePlanAndDevice]]
              SAMSON->TIBCO: GetCompatibleServicesForRatePlanAndDeviceResponse
              end   
              group <b> If exists PopulateUsageRate
              TIBCO->SQLMGR: GetUsageRate
              SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetUsageRate&action=query&keyValue=]]
	      SQLMGR->TIBCO: ListOfUsageRate
              end  
              group <b> SamsonQueryRatedFeatureTarASF
              TIBCO->SQLMGR: GetRatedFeature
              SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetRatedFeature&action=query&keyValue=]]
	      SQLMGR->TIBCO: ListOfRatedFeature
              end 
              group <b> If exists PopulateUsageBucket
              TIBCO->SQLMGR: GetUsageBuckets
              SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetUsageBuckets&action=query&keyValue=]]
	      SQLMGR->TIBCO: ListOfUsageBucket 
              TIBCO->JAVA: SamsonCalculateBucketSummary
              JAVA->SAMSON: ListOfUsageBucket
	      SAMSON->JAVA: UsageBucketSummary
              JAVA->TIBCO:  UsageBucketSummary            
              end  
              group <b> If exists PopulateVoiceMail
              TIBCO->SQLMGR: GetServiceFeature
              SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetServiceFeature&action=query&keyValue=]]
	      SQLMGR->TIBCO: ListOfServiceFeature              
              end 
              group <b> QueryPricePlanDetailsABP
              TIBCO->SQLMGR: GetPlanDetails
              SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetPlanDetails&action=query&keyValue=]]
	      SQLMGR->TIBCO: Soc
              end
              group <b> If version>=2
              TIBCO->SAMSON:  GetLogicalDateTux,GetSubscriberDetailsTux
              SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csGtSub.html csGtSub]]
              SAMSON->TIBCO: RfcResponse_csGtSub_Output
              end
              group <b> If returnSocDetails = true() 
              TIBCO->SQLMGR: getClassicToValueSocDetails
              SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=getClassicToValueSocDetails&action=query&keyValue=]]
	      SQLMGR->TIBCO: ListOfSoc
              end 
end 
     group <b> GetPlanSelections 
        TIBCO->TIBCO :QueryPricePlanForSale
              group <b> If exists socIndicators
              TIBCO->SAMSON:  ProductLookup
              SAMSON -> SAMSON : [[http://localhost:8085/ProductLookup.asmx ProductLookup.GetWeb2GoDeviceServiceCompatibility]]
              SAMSON->TIBCO: GetWeb2GoDeviceServiceCompatibilityResponse
              end 
              group <b> Otherwise
              TIBCO->SAMSON:  ProductLookup
              SAMSON -> SAMSON : [[http://localhost:8085/ProductLookup.asmx ProductLookup.GetCompatibleServicesForRatePlanAndDevice]]
              SAMSON->TIBCO: GetCompatibleServicesForRatePlanAndDeviceResponse
              end 
              group <b> QuerySocRollupPriceTarASF
               TIBCO->SQLMGR: GetRollUpPrice
              SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetRollUpPrice&action=query&keyValue=]]
	      SQLMGR->TIBCO: ListOfSoc
              end  
             group <b> If exists ListOfSoc
               TIBCO->SQLMGR: GetSocEligibility
              SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetSocEligibility&action=query&keyValue=]]
	      SQLMGR->TIBCO: ListOfSoc
              end 
        group <b> GetUsageRate
               TIBCO->SQLMGR: GetUsageRate
              SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetUsageRate&action=query&keyValue=]]
	      SQLMGR->TIBCO: ListOfUsageRate
              end 
       group <b> GetUsageBucket
        TIBCO->SQLMGR: GetUsageBuckets
              SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetUsageBuckets&action=query&keyValue=]]
	      SQLMGR->TIBCO: ListOfUsageBucket 
          end
  group <b> If retrieveDataThrottleAttributes = true()
              TIBCO->SQLMGR: GetSocThrottleDetails
              SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetSocThrottleDetails&action=query&keyValue=]]
	      SQLMGR->TIBCO: csiGetSocThrottleDetailsResponse
              end
end 
     group <b> QueryPricePlanForSale
     group <b> If serviceagreement does not exist for the customer
     TIBCO->TIBCO: SamsonQueryServiceAgreementABP
     end 
     group <b> If serviceagreement exist for the customer
     TIBCO->SQLMGR: GetSubscriberSubMarket
              SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetSubscriberSubMarket&action=query&keyValue=]]
	      SQLMGR->TIBCO: SubMarketCode
              end 
     group <b> SamsonGetBanDetailsTarASF
     TIBCO->SAMSON:  GetLogicalDateTux,SamsonGetBanDetailsTarASF
              SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/arGtCstBanExt.html arGtCstBanExt]]
              SAMSON->TIBCO: RfcResponse_arGtCstBanExt_Output
              end
       group <b> If new customer
      TIBCO->SQLMGR: GetPricePlanForSale
              SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetPricePlanForSale&action=query&keyValue=]]
	      SQLMGR->TIBCO: ListOfSoc
              end 
      group <b> SamsonQueryPlanClassificationCodeTarASF
      TIBCO->SQLMGR: getPlanClassificationCode
              SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=getPlanClassificationCode&action=query&keyValue=]]
	      SQLMGR->TIBCO: SocCode
              end 
     group <b> If MultlilineIndicator exists
    TIBCO->SAMSON:  GetLogicalDateTux,LastQualifyingGSMLineTux
              SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csValNonRj11.html csValNonRj11]]
              SAMSON->TIBCO: RfcResponse_csValNonRj11_Output
              end
    group <b> If lastQualifyingLine = "N"
   TIBCO->SAMSON:  GetLogicalDateTux,SamsonGetMaxMRCTarASF
              SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csGtMaxRc.html csGtMaxRc]]
              SAMSON->TIBCO: RfcResponse_csGtMaxRc_Output
              end
 end   
                     
 TIBCO->Client: TIBCO response to client
end
@enduml