@startuml
title SIVRService.GetLastQualifyingGSMLineOnAccount
actor Client
group <font color=#FF0090><b> GetLastQualifyingGSMLineOnAccount [<b> /sivr/subscriber/servicechange/islastqualifyinggsmline]
Client->TIBCO: csiLastQualifyingGSMLineRequest
    group SamsonSubsQueryService.LastQualifyingGSMLine [<font color=#FF0090><b>/SAMSON/SUBSCRIBER/SERVICECHANGE/LASTQUALIFYINGGSMLINE</font>]
        group LastQualifyingGSMLine
            TIBCO -> SAMSON : csiLastQualifyingGSMLineRequest
            SAMSON -> SAMSON :[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/205/services/csValNonRj11.html csValNonRj11]] 
            TIBCO <- SAMSON :csiLastQualifyingGSMLineResponse
        end
    end
TIBCO -> Client : csiLastQualifyingGSMLineResponse
end
@enduml