@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> WebRegistration.resendPassword Flow</font>
actor client
participant "RSP"
participant SelfCareServicePortStub
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/WebRegistration.html#resendPassword resendPassword]]WebRegistration.resendPassword
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* resendPasswordRequest Request
end note
alt CustomerType is mandatory
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> SelfCareServicePortStub :Calling selfcareservice
activate SelfCareServicePortStub
"RSP" <-- SelfCareServicePortStub : resendPassword
deactivate
"RSP" --> "client": resendPassword response
deactivate
@enduml











