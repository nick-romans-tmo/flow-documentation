
@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> CustomerProfileService.getCustomerProfile Flow</font>
actor client
participant "RSP"
participant SubscriberDetailsPort
participant CustomerProfileServiceSoap
participant  BANLookupServiceCHUB
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/CustomerProfileService.html#getCustomerProfile getCustomerProfile]]CustomerProfileService.getCustomerProfile
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* getCustomerProfileRequest Request
end note
alt INVALID_INPUT - Msisdn value needs to be passed, when Profile Level is chosen as Subscriber
"RSP" --> "client": ERRORMESSAGE
end
activate SubscriberDetailsPort
"RSP" -> SubscriberDetailsPort :Calling HttpServices
"RSP" <-- SubscriberDetailsPort : getCustomerProfile
deactivate 
activate CustomerProfileServiceSoap
"RSP" -> CustomerProfileServiceSoap :Calling HttpServices
"RSP" <-- CustomerProfileServiceSoap : getCustomerProfile
deactivate 
activate BANLookupServiceCHUB
"RSP" -> BANLookupServiceCHUB :Calling BANLookupService
"RSP" <-- BANLookupServiceCHUB : banlookup
deactivate 
"RSP" --> "client": getCustomerProfile response
@enduml

























