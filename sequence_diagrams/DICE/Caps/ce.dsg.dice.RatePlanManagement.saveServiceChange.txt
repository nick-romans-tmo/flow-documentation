@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> RatePlanManagement.saveServiceChange Flow</font>
actor client
participant "RSP"
participant SAMSONTUXEDO
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/RatePlanManagementService.html#saveServiceChange saveServiceChange]]RatePlanManagement.saveServiceChange
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* saveServiceChange statusReport.isNotGood 
end note
alt Validation for saveServiceChange failed
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> SAMSONTUXEDO :Calling SAMSONTUXEDO saveServiceChange 
activate SAMSONTUXEDO
"RSP" <-- SAMSONTUXEDO : saveServiceChange 
deactivate SAMSONTUXEDO
"RSP" --> "client": saveServiceChange response
deactivate
@enduml