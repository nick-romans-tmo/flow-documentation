@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> Activation.asyncActivateSubscribers Flow</font>
actor client
participant "RSP"
participant RSPDB
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/ActivationService.html#asyncActivateSubscribers asyncActivateSubscribers]]Activation.asyncActivateSubscribers
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
*asyncActivationRequest request.getBan(
end note
alt ban is must
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> RSPDB :Calling AsyncRecord.insertAsyncRecord sql query
activate RSPDB
"RSP" <-- RSPDB : asyncActivateSubscribers
deactivate
"RSP" --> "client": asyncActivateSubscribers response
deactivate
@enduml


