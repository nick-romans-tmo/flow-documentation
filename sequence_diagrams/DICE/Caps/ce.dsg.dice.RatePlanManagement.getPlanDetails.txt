@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> RatePlanManagement.getPlanDetails Flow</font>
actor client
participant "RSP"
participant RatePlanManagementService
participant SAMSONTUXEDO
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/RatePlanManagementService.html#getPlanCountByCategory getPlanCountByCategory]]RatePlanManagement.getPlanCountByCategory
activate "RSP"
activate "RatePlanManagementService"
RSP -> RatePlanManagementService: if activation enabled/true
alt RatePlanManagementService ENABLED
RatePlanManagementService -> RSP: getPlans
deactivate RatePlanManagementService
else activation DISABLED
RSP -> SAMSONTUXEDO :Calling getPlanDetails 
activate SAMSONTUXEDO
RSP <-- SAMSONTUXEDO : getPlans
end
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* getPlanDetailsRequest request.getPlanCode
end note
alt The input plan count is more than two
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> SAMSONTUXEDO :Calling RatePlanManagement.getPlanDetails getAalServicesForRateplan
"RSP" <-- SAMSONTUXEDO : getPlanDetails
deactivate
"RSP" --> "client": getPlanDetails response
deactivate
@enduml