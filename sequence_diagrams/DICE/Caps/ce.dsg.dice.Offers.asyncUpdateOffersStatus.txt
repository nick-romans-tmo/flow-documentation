@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> Offers.asyncUpdateOffersStatus Flow</font>
actor client
participant "RSP"
participant RSPDB
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/OfferService.html#asyncGetOffers asyncGetOffers]]Offer.asyncGetOffers
activate "RSP"
"RSP" -> RSPDB :Calling AsyncRecord.asyncUpdateOffersStatus sql call
activate RSPDB
"RSP" <-- RSPDB : asyncUpdateOffersStatus 
deactivate RSPDB
"RSP" --> "client": asyncUpdateOffersStatus response
deactivate
@enduml