@startuml
title SIVRService.QueryIMEI
actor Client
group <font color=#FF0090><b> QueryIMEI [<b> /ivr/sivr/subscriber/imei/query]
Client->TIBCO: SivrQueryIMEIInput
    group HANA Flow [TMO-App-HANA-EnableHANAFlow = "true"]
        TIBCO -> SQLManager : MSISDN,IMEI
        SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=getIMEIStatus&action=query&keyValue=  sqlmanager/query/getIMEIStatus]]
        TIBCO <- SQLManager : IMEIStatus
    end
    group otherwise 
        TIBCO -> SQLManager : MSISDN,IMEI
        SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=DmwGetImeiStatus&action=query&keyValue=  sqlmanager/query/DmwGetImeiStatus]]
        TIBCO <- SQLManager : IMEIStatus
    end
    group QueryIMEICodes [if IMEIStatus exists]
        TIBCO -> SQLManager : make,model
        SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetHandSetMakeModel&action=query&keyValue=  sqlmanager/query/GetHandSetMakeModel]]
        TIBCO <- SQLManager : IMEIStatus
    end
TIBCO -> Client : SivrQueryIMEIOutput
end
@enduml