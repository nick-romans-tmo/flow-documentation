@startuml
title ServiceAccountService.ValidateAccount
actor Client
group <font color=#FF0090><b> ValidateAccount
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-05-18/sid/html/ValidateAccount.html#ValidateAccount ValidateAccountInput]] 
group <b> GetCustActData
        TIBCO -> TIBCO : SamsonSubsQueryService.QueryAccountSubscriber
                    group <b> SamsonService.QueryAccountSubscriber [<font color=#FF0090> /SAMSON/ACCOUNT/DETAILS/QUERY]
                        TIBCO -> SQLManager : getSubDetails
                        SQLManager -> SQLManager : [[sqlmanager/query/GetSubscriberDetails sqlmanager/query/GetSubscriberDetails]]
                        SQLManager --> TIBCO : SubscriberDetails
                        TIBCO -> SQLManager : getAccountDetails
                        SQLManager -> SQLManager : [[sqlmanager/query/GetAccountDetails sqlmanager/query/GetAccountDetails]]
                        SQLManager --> TIBCO : AccountDetails
                        TIBCO -> SQLManager : getBillInfo
                        SQLManager -> SQLManager : [[sqlmanager/query/GetBillInfo sqlmanager/query/GetBillInfo]]
                        SQLManager --> TIBCO : BillInfo
                    end
        TIBCO --> TIBCO : SamsonSubsQueryService.QueryAccountSubscriber
end
TIBCO -> Client : ValidateAccountInput
end
@enduml