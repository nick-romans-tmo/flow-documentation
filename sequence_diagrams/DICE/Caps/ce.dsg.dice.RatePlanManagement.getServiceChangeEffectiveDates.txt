@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> RatePlanManagement.getServiceChangeEffectiveDates Flow</font>
actor client
participant "RSP"
participant SAMSONTUXEDO
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/RatePlanManagementService.html#getServiceChangeEffectiveDates getServiceChangeEffectiveDates]]RatePlanManagement.getServiceChangeEffectiveDates
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* getServiceChangeEffectiveDates statusReport.isNotGood 
end note
alt Validation for getServiceChangeEffectiveDates failed
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> SAMSONTUXEDO :Calling SAMSONTUXEDO getServiceChangeEffectiveDates 
activate SAMSONTUXEDO
"RSP" <-- SAMSONTUXEDO : getServiceChangeEffectiveDates 
deactivate SAMSONTUXEDO
"RSP" --> "client": getServiceChangeEffectiveDates response
deactivate
@enduml