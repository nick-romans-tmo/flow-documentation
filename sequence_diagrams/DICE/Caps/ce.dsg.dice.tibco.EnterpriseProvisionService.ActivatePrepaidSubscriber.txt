@startuml
title EnterpriseProvisionService.ActivatePrepaidSubscriber
actor Client
group <font color=#FF0090><b> ActivatePrepaidSubscriber
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-04-13/sid/html/Provision.html#ActivatePrepaidSubscriber ActivatePrepaidSubscriberRequest]] 
    group <b> Is VanityURL? [ Universe_Bridging_Flag = TRUE and applicationId=$TMO-App-Enterprise-Provision-ApplicationId and dealerCode exists]
              group <b> GetCustomerSegmentationSOAP [ If GetProcessCBStatus/cbStatus = "ON" and GetProcessCBStatus/statusCode = "100)"]
                TIBCO->RSP: CustomerLookupService.GetCustomerSegmentation
                RSP-> RSP: [[http://localhost:7001/eProxy/service/CustomerLookupService_SOAP_V1]]
                RSP->TIBCO: GetCustomerSegmentationResponse
              end
    group <b> In trialMarket ? [ If GetCustomerSegmentation/trialMarket = TRUE]
        group <b> ActivatePrepaidSubscriberU2Orch
            group <b> If ZipCode exists
                TIBCO -> TIBCO : SamsonMisc.GetNgpNpaByZip
                group <b> SamsonMisc.GetNgpNpaByZip [<font color=#FF0090> /SAMSON/NUMBERMGMT/NGPNPABYZIP/GET/]
                    TIBCO -> SAMSON : GetNgpNpaByZip - csiGetNgpNpaByZipRequest
                    SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evGbNpaByZip.html evGbNpaByZip]]
                    SAMSON --> TIBCO : csiGetNgpNpaByZipResponse
                end
            end
            TIBCO -> TIBCO : SamsonMisc.UpdateMsisdnStatus
                group <b> SamsonMisc.UpdateMsisdnStatus [<font color=#FF0090> /SAMSON/NUMBERMGMT/MSISDN/UPDATE/]
                    TIBCO -> SAMSON : UpdateMsisdnStatus - csiUpdateMsisdnStatusRequest
                    SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evChgCtnSts.html evChgCtnSts]]
                    SAMSON --> TIBCO : csiUpdateMsisdnStatusResponse
                end
            group <b> GetPostalCode [if personalInfo/zip and zipCode not exists]
                TIBCO -> CaaS_DB : (Connection-id : "REBELLION", SpaceName : "ZIPCODE", key["NPA"])
                TIBCO <- CaaS_DB : Caas Response
            end
            group <b> GetSIMDtls [If MSISDN exists]
                TIBCO -> TIBCO : SamsonService.GetSIMDtls
                group <b> SamsonService.GetSIMDtls [<font color=#FF0090> /SAMSON/DEVICE/SIMDETAILS/GET/]
                    TIBCO -> SAMSON : GetSIMDetails - csiGetSIMDetailsRequest
                    SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/icGtSit.html icGtSit]]
                    SAMSON --> TIBCO : csiGetSIMDetailsResponse
                end
            end
                group <b> RatePlanDBLookup
                     TIBCO -> CaaS_DB :  (Connection-id : "REBELLION", SpaceName : "RATEPLAN")
                    TIBCO <- CaaS_DB : SQL LookupOutput
                end
            end
        end
    end
    group <b> otherwise
        group <b> Get NGP/NPA [If zipCode exists and portInInfo not exists]
            group <b> SkipWHSLRef [$TMO-App-Activation-SkipWHSLRef = "true"]
                group <b> UseDefaultNGP [$TMO-App-Activation-UseDefaultNGP not equals to True]
                    TIBCO -> TIBCO : SamsonMisc.GetNgpNpaByZip
                    group <b> SamsonMisc.GetNgpNpaByZip [<font color=#FF0090> /SAMSON/NUMBERMGMT/NGPNPABYZIP/GET/]
                        TIBCO -> SAMSON : GetNgpNpaByZip - csiGetNgpNpaByZipRequest
                        SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evGbNpaByZip.html evGbNpaByZip]]
                        SAMSON --> TIBCO : csiGetNgpNpaByZipResponse
                    end
                end
            end
            group <b> GetNGP-for-Zip
                TIBCO -> SQL_MANAGER : sqlmanager/query/GetWholesaleMktNgpForZip
                TIBCO <-- SQL_MANAGER : Resonse
            end
            group <b> GetNgpNpaWithAvailableCtns
                    TIBCO -> TIBCO : SamsonMisc.GetNgpNpaWithAvailableCtns
                    group <b> SamsonMisc.GetNgpNpaWithAvailableCtns [<font color=#FF0090> /SAMSON/NUMBERMGMT/NGPNPAWITHCTNS/GET]
                        TIBCO -> SAMSON : GetNgpNpaWithAvailableCtns - csiGetNgpNpaWithAvailableCtnsRequest
                        SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/csGetSubNm.html csGetSubNm]]
                        SAMSON --> TIBCO : csiGetNgpNpaWithAvailableCtnsResponse
                    end
                    TIBCO -> TIBCO : csiGetNgpNpaWithAvailableCtnsResponse
            end
            group <b> ReserveMSISDN
                    TIBCO -> TIBCO : SamsonMisc.ReserveMSISDN
                    group <b> SamsonMisc.ReserveMSISDN [<font color=#FF0090> /SAMSON/NUMBERMGMT/MSISDN/RESERVE]
                        TIBCO -> SAMSON : ReserveMSISDN - csiReserveMSISDNRequest
                        SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/nmChgCtnSts.html nmChgCtnSts]] [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/csGetSubNm.html csGetSubNm]]
                        SAMSON --> TIBCO : csiReserveMSISDNResponse
                    end
                    TIBCO -> TIBCO : csiReserveMSISDNResponse
            end
            group <b> ActivatePrepaidSubscriberOrch
                group <b> PortIn ? [If portInInfo exists and portInInfo/transitionalInfo not exists]
                    TIBCO -> RPX : submitPortInRequest
                    RPX -> RPX : httpServices.submitPortIn [[https://localhost:9090/rpxHttpService]]
                    TIBCO <-- RPX : submitPortInResponse
                end
                group <b> otherwise
                    group <b> VINLI Customer [ if csiActivatePrepaidSubscriberRequest/requestHeader/senderId = "VINLI"]
                        TIBCO -> RPX : activateSubscriberForPartnerRequest
                        RPX -> RPX : httpServices.activateSubscriberForPartner [[https://localhost:9090/rpxHttpService]]
                        TIBCO <-- RPX : activateSubscriberForPartnerResponse
                    end
                    group <b> otherwise
                        TIBCO -> RPX : ActivatePrepaidSubscriberRequest
                        RPX -> RPX : httpServices.ActivatePrepaidSubscriber [[https://localhost:9090/rpxHttpService]]
                        TIBCO <-- RPX : ActivatePrepaidSubscriberResponse
                    end
                end
            group <b> TSD Enabled ? [if webRegistrationIndicator = true and csiActivatePrepaidSubscriberResponse/ns1:status/ns1:statusCode = "0" and $TMO-App-DSPA-TSD_Changes_Enabled = "true" and personalInfo/emailInfo[pfx:emailType = "DataNotify"][1]/pfx:emailAddress exists]
                group <b> GetSIMDtls
                    TIBCO -> TIBCO : SamsonService.GetSIMDtls
                    group <b> SamsonService.GetSIMDtls [<font color=#FF0090> /SAMSON/DEVICE/SIMDETAILS/GET/]
                        TIBCO -> SAMSON : GetSIMDetails - csiGetSIMDetailsRequest
                        SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/icGtSit.html icGtSit]]
                        SAMSON --> TIBCO : csiGetSIMDetailsResponse
                    end
                end
                group <b> DSPASubscriberProvision
                    TIBCO -> JMS : DSPA.SUBSCRIBER.PROVISION
                end
            end
            group <b> webRegistrationIndicator [webRegistrationIndicator = true and $TMO-App-DSPA-TSD_Changes_Enabled = "false" and personalInfo/pfx:emailInfo[pfx:emailType = "DataNotify"][1]/pfx:emailAddress exists]
                TIBCO -> DSPA : SelfCareService.registerCustomerFromDevice [[http://dspa-selfcare.apps.px-prd01.cf.t-mobile.com:80/webservice/SelfCareService]]
                TIBCO <- DSPA : csiRegisterCustomerFromDeviceResponse
            end
        end
        group <b> otherwise
            group <b> PortIn ? [If portInInfo exists and portInInfo/transitionalInfo not exists]
                TIBCO -> RPX : submitPortInRequest
                RPX -> RPX : httpServices.submitPortIn [[https://localhost:9090/rpxHttpService]]
                TIBCO <-- RPX : submitPortInResponse
            end
            group <b> otherwise
                group <b> VINLI Customer [ if csiActivatePrepaidSubscriberRequest/requestHeader/senderId = "VINLI"]
                    TIBCO -> RPX : activateSubscriberForPartnerRequest
                    RPX -> RPX : httpServices.activateSubscriberForPartner [[https://localhost:9090/rpxHttpService]]
                    TIBCO <-- RPX : activateSubscriberForPartnerResponse
                end
                group <b> otherwise
                    TIBCO -> RPX : ActivatePrepaidSubscriberRequest
                    RPX -> RPX : httpServices.ActivatePrepaidSubscriber [[https://localhost:9090/rpxHttpService]]
                    TIBCO <-- RPX : ActivatePrepaidSubscriberResponse
                end
            end
        end
    end
end
TIBCO -> Client : ActivatePrepaidSubscriberResponse
end
@enduml