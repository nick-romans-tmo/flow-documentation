@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> Offers.getOfferHistory Flow</font>
actor client
participant "RSP"
participant EPIPHANY
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/OfferService.html#getOfferHistory getOfferHistory]]Offer.getOfferHistory
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* getOfferHistory request :isCheckEligibility
end note
alt Atleast One Subscriber Info should be present when checkEligibility is true."
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> EPIPHANY :Calling getOfferHistory
activate EPIPHANY
"RSP" <-- EPIPHANY : getOfferHistory 
deactivate EPIPHANY
"RSP" --> "client": getOfferHistory response
deactivate
@enduml