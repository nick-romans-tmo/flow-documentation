@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> PrePaidService.activatePrePaidSubscriber Flow</font>
actor client
participant "RSP"
participant RPCDB
participant RSPDB
participant SamsonTuxedo
participant OrderUpdateWSIL
participant HttpServicesPort
participant CustomerProfile
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/PrePaidService.html#activatePrePaidSubscriber activatePrePaidSubscriber]]PrePaidService.activatePrePaidSubscriber
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* activatePrePaidSubscriberRequest Request
end note
alt ban is must
"RSP" --> "client": ERRORMESSAGE
end
activate RPCDB
"RSP" -> RPCDB :Calling PrePaidService
"RSP" <-- RPCDB : activatePrePaidSubscriber
deactivate 
activate SamsonTuxedo
"RSP" -> SamsonTuxedo :Calling Tuxedo_icGtSit
"RSP" <-- SamsonTuxedo : retrieveImsiForSim
"RSP" -> SamsonTuxedo :Calling Tuxedo_evChgCtnSts
"RSP" <-- SamsonTuxedo : GetCtnStatus
"RSP" -> SamsonTuxedo :Calling Tuxedo_evChkPortIn
"RSP" <-- SamsonTuxedo : verifyaddress
"RSP" -> SamsonTuxedo :Calling Tuxedo_evValdateAdr
"RSP" <-- SamsonTuxedo : verifyaddress
deactivate
activate RSPDB
"RSP" -> RSPDB :Calling retrieveZipForCity
"RSP" <-- RSPDB : getNpaNxxByMarket
"RSP" -> RSPDB :Calling NpaNxx.getNpaNxxByMarket
"RSP" <-- RSPDB : insertAsyncRecord
"RSP" -> RSPDB :Calling AsyncRecord.insertAsyncRecord
"RSP" <-- RSPDB : publishActivationEligibility
deactivate
activate OrderUpdateWSIL
"RSP" -> OrderUpdateWSIL :Calling submitCompleteOrder
"RSP" <-- OrderUpdateWSIL : SubmitCompleteOrderResponse
deactivate
activate HttpServicesPort
"RSP" -> HttpServicesPort :Calling HttpService
"RSP" <-- HttpServicesPort : SubmitPortIn
"RSP" -> HttpServicesPort :Calling HttpService
"RSP" <-- HttpServicesPort : activatePrepaidSubscriber
deactivate
activate CustomerProfile 
"RSP" -> CustomerProfile :Calling HttpService
"RSP" <-- CustomerProfile : CustomerProfile
deactivate
"RSP" --> "client": activatePrePaidSubscriber eresponse
@enduml











