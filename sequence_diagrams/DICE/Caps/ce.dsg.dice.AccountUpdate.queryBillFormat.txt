@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> AccountUpdate.queryBillFormat Flow</font>
actor client
participant "RSP"
participant QueryBillFormatPort
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/AccountUpdateService.html#queryBillFormat queryBillFormat]]Activation.queryBillFormat
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* QueryBillFormat request.eligibileBillFormat
end note
alt validate QueryBillFormatRequest:CustomerData not Found
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> QueryBillFormatPort :Calling QueryBillFormatPort.QueryBillFormat
activate QueryBillFormatPort
"RSP" <-- QueryBillFormatPort : QueryBillFormat
deactivate 
"RSP" --> "client": QueryBillFormat response
deactivate
@enduml