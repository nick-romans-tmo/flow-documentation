@startuml
title CustomerProfileService.SaveCustomerProfile
actor Client
group <font color=#FF0090><b> SaveCustomerProfile
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-04-13/sid/html/CustomerProfile.html#SaveCustomerProfile SaveCustomerProfileRequest]]   
                  
                  group <b> If PhysicalAddress exists with OverrideFlag = N in the request and TurnOff_AddressValidation GV is N
                  TIBCO-> TIBCO : Call SamsonAccount.SamsonBatchValidateAddress
                  group <b> SamsonAccount.SamsonBatchValidateAddress [<font color=#FF0090> /SAMSON/BATCHADDRESS/DETAILS/VALIDATE/</font>] : asiSaveCustomerInput
                  TIBCO->SAMSON:  GetLogicalDateTux,VerifyZipTux
                SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/gnMtchZipAdr.html gnMtchZipAdr]]
                SAMSON->TIBCO: RfcResponse_gnMtchZipAdr_Output
                 TIBCO->SAMSON: VerifyAddressTux
                SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/gnMatchAdr.html gnMatchAdr]]
                SAMSON->TIBCO: RfcResponse_gnMatchAdr_Output
                 TIBCO->SAMSON: CompareGeoCodeTux
                SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/gnGtGeoCd.html gnGtGeoCd]]
                SAMSON->TIBCO: RfcResponse_gnGtGeoCd_Output
                TIBCO->SAMSON: GetLogicalDateTux,GetDynamicDescTux
                SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/rfGtDynDesc.html rfGtDynDesc]]
                SAMSON->TIBCO: RfcResponse_rfGtDynDesc_Output
                end
                  TIBCO-> CustomerProfile: CustomerProfileService.SaveCustomerProfile asiCPSaveCustomerProfileInput
                  CustomerProfile-> CustomerProfile: [[http://customerprofile-qlab02.test.px-npe01.cf.t-mobile.com/cp/CustomerProfileService]]
                  CustomerProfile-> TIBCO: asiCPSaveCustomerProfileOutput
                  end
                  group <b> Otherwise
                  TIBCO-> CustomerProfile: CustomerProfileService.SaveCustomerProfile asiCPSaveCustomerProfileInput
                  CustomerProfile-> CustomerProfile: [[http://customerprofile-qlab02.test.px-npe01.cf.t-mobile.com/cp/CustomerProfileService]]
                  CustomerProfile-> TIBCO: asiCPSaveCustomerProfileOutput
                  end                  
                           
TIBCO -> Client : SaveCustomerProfileResponse
end
@enduml