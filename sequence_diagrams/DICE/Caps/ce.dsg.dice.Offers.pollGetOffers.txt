@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> Offers.pollGetOffers Flow</font>
actor client
participant "RSP"
participant RSPDB
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/OfferService.html#pollGetOffers pollGetOffers]]Offer.pollGetOffers
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* pollGetOffers request : get AsyncRecord
end note
alt Record not found in database for async token and PartnerId provided
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> RSPDB :Calling AsyncRecord.selectAsyncRecordByPrimaryKey sql call
activate RSPDB
"RSP" <-- RSPDB : pollGetOffers 
deactivate RSPDB
"RSP" --> "client": pollGetOffers response
deactivate
@enduml