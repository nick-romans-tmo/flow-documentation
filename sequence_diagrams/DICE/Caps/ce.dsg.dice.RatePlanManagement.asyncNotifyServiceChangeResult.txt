@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> RatePlanManagement.asyncNotifyServiceChangeResult Flow</font>
actor client
participant "RSP"
participant RSPDB
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/RatePlanManagementService.html#asyncNotifyServiceChangeResult asyncNotifyServiceChangeResult]]RatePlanManagement.asyncNotifyServiceChangeResult
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* asyncValidateAndSaveServiceChangeRequest.getValidateAndSaveServiceChangeRequest
end note
alt Validation for getValidateAndSaveServiceChangeRequest failed
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> RSPDB :Calling  AsyncRecord.insertAsyncRecord sql query
activate RSPDB
"RSP" <-- RSPDB :asyncValidateAndSaveServiceChange
deactivate RSPDB
"RSP" --> "client": asyncValidateAndSaveServiceChange response
deactivate
@enduml 