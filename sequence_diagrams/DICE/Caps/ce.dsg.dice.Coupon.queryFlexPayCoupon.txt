@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> Coupon.queryFlexPayCoupon Flow</font>
actor client
participant "RSP"
participant HttpServicesPort
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/Coupon.html#queryFlexPayCoupon queryFlexPayCoupon]]Coupon.queryFlexPayCoupon
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* queryFlexPayCouponRequest Request
end note
alt couponpin is mandatory
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> HttpServicesPort :Calling HttpService
activate HttpServicesPort
"RSP" <-- HttpServicesPort : queryFlexPayCoupon
deactivate
"RSP" --> "client": queryFlexPayCoupon response
deactivate
@enduml











