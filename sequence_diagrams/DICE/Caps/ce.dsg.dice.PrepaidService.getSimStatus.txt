@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> PrepaidService.getSimStatus Flow</font>
actor client
participant "RSP"
participant HttpServicesPort
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/PrepaidService.html#getSimStatus getSimStatus]]PrepaidService.getSimStatus
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* getSimStatusRequest Request
end note
alt usn is must
"RSP" --> "client": ERRORMESSAGE
end
activate HttpServicesPort
"RSP" -> HttpServicesPort :Calling HttpService
"RSP" <-- HttpServicesPort : validatePrepaidKitItems
deactivate 
"RSP" --> "client": getSimStatus  response
@enduml











