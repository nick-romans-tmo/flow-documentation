@startuml
title CustomerAppView.BillPreferences
actor Client
group <font color=#FF0090><b> BillPreferences
Client->TIBCO:[[/rest/v1/customerappview/getbillpreferences /rest/v1/customerappview/getbillpreferences]] 
     group GetBillPreferences
        TIBCO -> SAMSON : getBillPreferencesRequest
        SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evGtPymBlInf.html evGtPymBlInf]] 
        SAMSON --> TIBCO : getBillPreferencesResponse
     end
TIBCO -> Client : billPreferencesResponse
end
@enduml
