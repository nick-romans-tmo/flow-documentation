@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> PostPaidAccount.getSubscriberStatusCount Flow</font>
actor client
participant "RSP"
participant SAMSONTUXEDO
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/PostPaidAccountService.html#getSubscriberStatusCount getSubscriberStatusCount]]PostPaidAccount.getSubscriberStatusCount
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* getSubscriberStatusCountRequest Request
end note
alt ban is  must
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> SAMSONTUXEDO :Calling Tuxedo_csSubStCnt
activate SAMSONTUXEDO
"RSP" <-- SAMSONTUXEDO : getSubscriberStatusCount
deactivate
"RSP" --> "client": getSubscriberStatusCount response
deactivate
@enduml











