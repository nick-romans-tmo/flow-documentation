@startuml
title DiscountService.GetAvailableDiscounts
actor Client
group <font color=#FF0090><b> GetAvailableDiscounts
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-04-13/sid/html/DiscountService.html#getAvailableDiscounts getAvailableDiscountsRequest]] 
    TIBCO->TIBCO: getAvailableDiscountsOrch
group <b> getAvailableDiscountsOrch
        TIBCO -> SAMSON: Call GetLogicalDateTux
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/gnGtLgclDt.html gnGtLgclDt]]
	SAMSON -> TIBCO : LogicalDate
        TIBCO -> SAMSON: Call GetAvailableDiscountPlansTux
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evLsAvailDPs.html evLsAvailDPs]]
	SAMSON -> TIBCO : getAvailableDiscounts
end
TIBCO -> Client : getAvailableDiscountsResponse
end
@enduml