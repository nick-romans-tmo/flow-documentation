@startuml
title SIVRService.QueryUsage
actor Client
group <font color=#FF0090><b> QueryUsage [<b> /sivr/ivr/usage/summary/query]
Client->TIBCO: SivrQueryUsageInput
    group SamsonAccountService.SamsonQueryAccountDetails [<font color=#FF0090><b>/SAMSON/ACCOUNT/DETAILS/QUERY</font>]
        group querySubcriber [if BAN does not exists]
            TIBCO -> SQLManager : SubscriberNo
            SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetSubscriberDetails&action=query&keyValue= sqlmanager/query/GetSubscriberDetails]]
            TIBCO <- SQLManager : ListOfSubscribers
        end
        group queryAccountDetailsForBan
            TIBCO -> SQLManager : Ban
            SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetAccountDetails&action=query&keyValue= sqlmanager/query/GetAccountDetails]]
            TIBCO <- SQLManager : Customer
        end
        group checkSocEligibility [if CheckSocEligibility = true]
            group getAccountDetails [if CreditClass does not exists]
                TIBCO -> SQLManager : Ban
                SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetAccountDetails&action=query&keyValue= sqlmanager/query/GetAccountDetails]]
                TIBCO <- SQLManager : Customer
            end
            group queryOneFullPayment
                TIBCO -> SQLManager : Ban
                SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetBillInfo&action=query&keyValue= sqlmanager/query/GetBillInfo]]
                TIBCO <- SQLManager : OneFullPayment
            end
        end
    end
TIBCO -> Client : SivrQueryUsageOutput
end
@enduml