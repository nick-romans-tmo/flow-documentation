
@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> Adjustment.getMinuteCreditHistory Flow</font>
actor client
participant "RSP"
participant GetMinuteCreditHistory
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/Adjustment.html#getMinuteCreditHistory getMinuteCreditHistory]]Adjustment.getMinuteCreditHistory
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* GetMinuteCreditHistory Request
end note
alt Inavlid Input: usn is must
"RSP" --> "client": ERRORMESSAGE
end
activate GetMinuteCreditHistory
"RSP" -> GetMinuteCreditHistory :Calling Tibco GetMinuteCreditHistory service 
"RSP" <-- GetMinuteCreditHistory : getMinuteCreditHistory
deactivate 
"RSP" --> "client": getMinuteCreditHistory response
@enduml

























