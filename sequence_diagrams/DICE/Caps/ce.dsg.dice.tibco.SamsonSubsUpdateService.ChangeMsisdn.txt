@startuml
title SamsonSubsUpdateService.ChangeMsisdn
actor Client
group ChangeMsisdn [<font color=#FF0090><b>/SAMSON/SUBSCRIBER/MSISDN/UPDATE/</font>]
Client->TIBCO: ChangeMSISDNRequest
    group is not porting? [isPorting =! true()]
        group GetSubscriberDetails
            TIBCO -> SQLManager : SubscriberNo, if getReserved = true then pGetReserved is "Y" else "N"
            SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetSubscriberDetails_Dual&action=query&keyValue= sqlmanager/query/GetSubscriberDetails]]
            TIBCO <- SQLManager : ListOfSubscribers
        end
        group GetSubscriberInventory [if ChangeMsisdnInput/NEW_MSISDN exists]
            TIBCO -> SQLManager : SubscriberNo, if getReserved = true then pGetReserved is "Y" else "N"
            SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetSubscriberInventory&action=query&keyValue= sqlmanager/query/GetSubscriberInventory]]
            TIBCO <- SQLManager : CTN
        end
    end
    group getSubDetails
        TIBCO -> SAMSON : ChangeMsisdnInput
        SAMSON -> SAMSON :[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/205/services/csGtSub.html csGtSub]] 
        TIBCO <- SAMSON :ChangeMsisdnOutput
    end
    group getServiceAgreement
        group SamsonGetServiceAgreement
            group GetServiceAgreementTux [ if $TMO-App-Samson-Subscriber-EnableGetServiceAgreementTux = true]
                TIBCO -> SAMSON : evLsSubSrvSum-input
                SAMSON -> SAMSON :[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/205/services/evLsSubSrvSum.html evLsSubSrvSum]] 
                TIBCO <- SAMSON :ListOfServiceAgreements
            end
            group Otherwise GetServiceAgreement
                TIBCO -> SQLManager : EffectiveDate, SubscriberNo
                SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetServiceAgreement_Dual&action=query&keyValue= sqlmanager/query/GetServiceAgreement]]
                TIBCO <- SQLManager : ListOfServiceAgreements
            end
        end
            group queryProductCatalog [if ProductCatalogInd  is true or (APPLICATION_ID != "TMO" and ChannelID exists ) ]
                group if socIndicators exists
                TIBCO -> TIBCO : [[http://localhost:8085/ProductLookup.asmx ProductLookup.GetWeb2GoDeviceServiceCompatibility]]
                end
                group Otherwise
                TIBCO -> TIBCO : [[http://localhost:8085/ProductLookup.asmx  ProductLookup.GetCompatibleServicesForRatePlanAndDevice]]
                end
            end
            group getUsageRate [if PopulateUsageRate  is true]
                TIBCO -> SQLManager : ListOfSoc
                SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetUsageRate&action=query&keyValue= sqlmanager/query/GetUsageRate]]
                TIBCO <- SQLManager : ListOfUsageRate
            end
            group getRatedFeature 
                TIBCO -> SQLManager : SOC
                SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetRatedFeature&action=query&keyValue=  sqlmanager/query/GetRatedFeature]]
                TIBCO <- SQLManager : ListOfRatedFeature
            end
            group getUsageBucket [if PopulateUsageBucket is true]
                TIBCO -> SQLManager : SOC
                SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetUsageBuckets&action=query&keyValue= sqlmanager/query/GetUsageBuckets]]
                TIBCO <- SQLManager : ListOfUsageBucket
            end
            group getVoiceMail [if getVoiceMail is true]
                TIBCO -> SQLManager : BAN, effectiveDate,SUBSCRIBER_NO
                SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetServiceFeature_Dual&action=query&keyValue= sqlmanager/query/GetServiceFeature]]
                TIBCO <- SQLManager : ListOfServiceFeature
            end
    end
    group getServiceFeature
        TIBCO -> SQLManager : BAN, effectiveDate,SUBSCRIBER_NO
        SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetServiceFeature_Dual&action=query&keyValue= sqlmanager/query/GetServiceFeature]]
        TIBCO <- SQLManager : ListOfServiceFeature
    end
    group getPhysicalDevice
        TIBCO -> SQLManager : BAN, logicalDate,SUBSCRIBER_NO,APPLICATION_ID,OPERATOR_ID
        SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetPhysicalDeviceByBanMsisdn_Dual&action=query&keyValue= sqlmanager/query/GetPhysicalDeviceByBanMsisdn]]
        TIBCO <- SQLManager : ListOfPhysicalDevice
    end
    group [if ChangeMsisdnInput/REASON_CODE != "PR"]
        TIBCO -> SAMSON : csGtOcAmt00_Input
        SAMSON -> SAMSON :[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/205/services/csGtOcAmt.html csGtOcAmt]] 
        TIBCO <- SAMSON :csGtOcAmt00_Output
    end
    group SamsonReserveSubscriber [if ReserveReleaseFlag = true]
        TIBCO -> SAMSON : arGtCstBan_Input
        SAMSON -> SAMSON :[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/205/services/arGtCstBan.html arGtCstBan]] 
        TIBCO <- SAMSON :arGtCstBan_Output
        TIBCO -> SAMSON : csRsvSub_Input
        SAMSON -> SAMSON :[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/205/services/csRsvSub.html csRsvSub]] 
        TIBCO <- SAMSON :csRsvSub_Output
    end
    group getBanDetails
        TIBCO -> SAMSON : arGtCstBan_Input
        SAMSON -> SAMSON :[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/205/services/arGtCstBan.html arGtCstBan]] 
        TIBCO <- SAMSON :arGtCstBan_Output
    end
    group changePhone
        TIBCO -> SAMSON : csChgSub_Input
        SAMSON -> SAMSON :[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/205/services/csChgSub.html csChgSub]] 
        TIBCO <- SAMSON :csChgSub_Output
    end
    group Acount-Activity-Event-Sender
        TIBCO -> JMS : [<b>QueueName</b> = TMOBILE.<font color=#FF0090>$TMO.Env.Env_Name</font>.EVENT.ACCOUNTACTIVITY.CREATE]
    end
TIBCO -> Client : ChangeMSISDNResponse
end
@enduml