@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> RatePlanManagement.validatePlanPrimaryLinesChange Flow</font>
actor client
participant "RSP"
participant SAMSONTUXEDO
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/RatePlanManagementService.html#validatePlanPrimaryLinesChange validatePlanPrimaryLinesChange]]RatePlanManagement.validatePlanPrimaryLinesChange
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* validatePlanPrimaryLinesChange statusReport.isNotGood 
end note
alt Validation for validatePlanPrimaryLinesChange failed
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> SAMSONTUXEDO :Calling SAMSONTUXEDO validatePlanPrimaryLinesChange 
activate SAMSONTUXEDO
"RSP" <-- SAMSONTUXEDO : validatePlanPrimaryLinesChange 
deactivate SAMSONTUXEDO
"RSP" --> "client": validatePlanPrimaryLinesChange response
deactivate
@enduml