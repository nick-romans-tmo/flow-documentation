@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> RetailProductCatalog.getRatePlanDetails Flow</font>
actor client
participant "RSP"
participant SERVER
participant SAMSONTUXEDO
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/RetailProductCatalogService.html#getRatePlanDetails getRatePlanDetails]]RetailProductCatalog.getRatePlanDetails
activate "RSP"
activate "SERVER"
RSP -> SERVER: if gemfire enabled sending PARTNERID from Request
alt GEMFIRE ENABLED
SERVER -> RSP: getProfileDetails
deactivate SERVER
else GEMFIRE DISABLED
RSP -> SAMSONTUXEDO :Calling RetailProductCatalog.getRatePlanDetails sql call with requested inputs-sending PARTNERID from Request
activate SAMSONTUXEDO
RSP <-- SAMSONTUXEDO : getPartnerProfileDetails
end
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* RetailProductCatalogSearchInfo request.getAccountTypeSubType
end note
alt The request did not contain an account-type-sub-type. Please select one or more valid account-type-sub-types from the enumeration
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> SAMSONTUXEDO :Calling RetailProductCatalog.getRatePlanDetails sql call with requested inputs
"RSP" <-- SAMSONTUXEDO : getRatePlanDetails
deactivate
"RSP" --> "client": rateplanServices response
deactivate
@enduml