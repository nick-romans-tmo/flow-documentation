@startuml
title AccountSearch.GetAccountForBAN
actor Client
group <font color=#FF0090><b> GetAccountForBAN
Client->AccountSearchService: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/AddressService.html#getAccountForBAN GetAccountForBAN]]  AccountSearch.GetAccountForBAN
AccountSearchService->ESP:Receiving request
ESP->SAMSON:Samson spoke receive the service and valdiate the input request data
SAMSON->SAMSON :validate the input And Pass the MSISDN value to tuxedo service - csSrchSub
alt
SAMSON->ESP:Return account summary details 
SAMSON->AccountSearchService: if any validation error occurs, It will throw a fault message to client
end group
ESP->AccountSearchService:Return ESP send the account summary details as success response
AccountSearchService->Client:GetAccountForBAN Response to client
end group
@enduml