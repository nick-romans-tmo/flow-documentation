@startuml
title CustomerPlanService.getServiceChangeEffectiveDates
actor Client
group <font color=#FF0090><b> getServiceChangeEffectiveDates
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-04-13/sid/html/CustomerPlanService.html#getServiceChangeEffectiveDates getServiceChangeEffectiveDatesRequest]] 
    TIBCO->TIBCO: Call RSP RatePlanManagementService.getServiceChangeEffectiveDates getServiceChangeEffectiveDatesRequest
group <b> RatePlanManagementService.getServiceChangeEffectiveDates
              TIBCO->RSP: RatePlanManagementService.getServiceChangeEffectiveDates
              RSP-> RSP: [[http://localhost:7001/eProxy/service/RatePlanManagementService_SOAP_V1]]
              RSP->TIBCO: getServiceChangeEffectiveDatesResponse
              end 
TIBCO -> Client : getServiceChangeEffectiveDatesResponse
end
@enduml