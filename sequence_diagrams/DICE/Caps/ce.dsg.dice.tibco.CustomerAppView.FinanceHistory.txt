@startuml
title CustomerAppView.FinanceHistory
actor Client
group <font color=#FF0090><b> FinanceHistory
Client->TIBCO:[[/rest/v1/customerappview/getfinancehistory /rest/v1/customerappview/getfinancehistory]] 
     group GetActiveBalance
        TIBCO -> EIP : getActiveBalance
        EIP -> EIP : [[http://localhost:7001/tmobile-eip/QueryActiveInstallmentPlans getActiveBalance]] 
        EIP --> TIBCO : getActiveBalanceResponse
     end
TIBCO -> Client : financeHistoryResponse
end
@enduml
