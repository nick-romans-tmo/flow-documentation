@startuml
title CustomerService.getSubscribersInfo
actor Client


group <font color=#FF0090><b> getSubscribersInfo

Client->HAProxy_APIX: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/CustomerService.html#getSubscribersInfo getSubscribersInfo]]  CustomerService.getSubscribersInfo
HAProxy_APIX -> HAProxy_APIX :Authenticate and Authorize
HAProxy_APIX->CustomerService: getSubscribersInfo REQ
CustomerService->CustomerService: Validate AccountNumber( should be 9 digits and not null)
alt <b><color:darkorange>infoToRetrieve contains 'Account'</color>
group <font color=#FF8C00>
alt InvokeRefactored is "Y"
CustomerService->SAMSON:Invoke-GetBasicAccountDetails retrieve the GetBasicAccountDetails
SAMSON->CustomerService:GetBasicAccountDetails SAMSON Response
else
CustomerService->RSP:InvokeRSP PostPaidAccountService-GetBasicAccountDetails retrieve the GetBasicAccountDetails[[PostPaid.html#Postpaid GetBasicAccountDetails]]  Postpaid.GetBasicAccountDetails
RSP->CustomerService:  GetBasicAccountDetails Response
end group
note right 
GetBasicAccountDetails
endnote
group <font color=#FF8C00><b> MSISDN is NOTNULL
CustomerService->RSP:[[Invoke getSubscriberInfo]]
RSP->CustomerService:retrieve GetPostpaidSubscribersInfo.
end group
note right 
POSTPAID.Subscriber.GetSubscribersInfo MSISDN is NOTNULL
endnote
group <font color=#FF8C00><b> SENDER ID is NOT ODAM
CustomerService->SAMSON: InvokeSamson- evGtAddPrm00- GetContractServiceEndDateTux 
SAMSON->CustomerService:Samson retrieve the Contarct Service end Date Information
end group
note right 
GetContractServiceEndDateTux- senderId!='ODAM'
endnote
CustomerService->CustomerService: Transform-Payload Returns service response
CustomerService->HAProxy_APIX: Transform-Payload Returns service response
HAProxy_APIX->Client:getSubscribersInfo response to client
end group


@enduml
