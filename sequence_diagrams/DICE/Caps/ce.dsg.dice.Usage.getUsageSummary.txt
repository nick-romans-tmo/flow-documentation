
@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> Usage.getUsageSummary Flow</font>
actor client
participant "RSP"
participant UsagePort
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/Usage.html#getUsageSummary getUsageSummary]]Usage.getUsageSummary
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* getUsageSummary Request
end note
alt For POSTPAID/POSTPAIDDATAPASSV2 accounts pass bill start period and end period or usage cycle
"RSP" --> "client": ERRORMESSAGE
end
activate UsagePort
alt isFlexPay Enabled
RSP -> UsagePort: Calling Tibco UsagePort service if equals "POSTPAIDDATAPASSV2" in case of DATAPASS Request
UsagePort -> RSP: LookupUsageSummary Response
RSP -> UsagePort: Calling Tibco UsagePort service if equals "POSTPAIDDATAPASSV2" in case of POSTPAID Request 
UsagePort -> RSP: LookupUsageSummary Response
else isFlexPay Disabled
RSP -> UsagePort : Calling Tibco UsagePort service if not equals "POSTPAIDDATAPASSV2" 
RSP <-- UsagePort : LookupUsageSummary Response
deactivate UsagePort
end 
"RSP" --> "client": create Usage response
@enduml



























