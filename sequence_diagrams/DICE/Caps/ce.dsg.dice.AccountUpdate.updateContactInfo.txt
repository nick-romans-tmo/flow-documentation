@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> AccountUpdate.updateContactInfo Flow</font>
actor client
participant "RSP"
participant HttpServicesPort
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/AccountUpdateService.html#updateContactInfo updateContactInfo]]Activation.updateContactInfo
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* updateContactInfoRequest request.getReasonCode 
end note
alt Reason code is mandatory for prepaid accounts,The prepaid has one contact information,Invalid contact number
"RSP" --> "client": ERRORMESSAGE
end
alt Prepaid
"RSP" -> HttpServicesPort :Calling HttpServicesPort.updateContactInfo for PREPAID 
activate HttpServicesPort
"RSP" <-- HttpServicesPort : updateContactInfo
else Postpaid
"RSP" -> HttpServicesPort :Calling HttpServicesPort.updateContactInfo for POSTPAID
activate HttpServicesPort
"RSP" <-- HttpServicesPort : updateContactInfo
deactivate 
"RSP" --> "client": updateContactInfo response
deactivate
@enduml