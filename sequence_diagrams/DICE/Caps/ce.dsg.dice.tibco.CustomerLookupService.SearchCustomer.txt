@startuml
title CustomerLookupService.searchCustomer Service
actor Client
group <font color=#FF0090><b> searchCustomer     
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-12-08/sid/html/CustomerLookupService.html#searchCustomer searchCustomer]]
TIBCO -> TIBCO: searchCustomerSOAP 
group <b> RSP Search[<font color=#FF0090> searchCustomerSOAP.process</font>] 
    TIBCO -> RSP: Call searchCustomerSOAP
end
group <b> IF (searchCustomerResponse-status = "A") and exists(MSISDN) and (RSP-searchCustomerResponse-deviceInfo-:imei, 1, 8) = "85555554") and applicationId = "ODAM"
	TIBCO -> TIBCO : searchCustomerByStages
	group ProcessRSPResponse
		TIBCO -> TIBCO: getEligibleFeaturesImpl
		group GetAccount
			TIBCO -> TIBCO : getAccountOrch
			group IF accountType = "POSTPAID"
				TIBCO -> TIBCO : GetAccountByMsisdnOrch	
				TIBCO -> SAMSON: GetAccountsByMsisdnRequest
				SAMSON -> SAMSON: csSrchSub 
				SAMSON -> TIBCO: GetAccountsByMsisdnResponse
			end
			group IF $Start/accountType = "UNKNOWN" or string-length(tib:trim($Start/accountType)) =  0
				TIBCO -> TIBCO: GetAccountTypeByMsisdn
				TIBCO -> SAMSON: GetCurrentAllocationRequest
				SAMSON -> SAMSON: evGtSubInfo 
				SAMSON -> TIBCO: GetCurrentAllocationProxyResponse
			end			
		end
		group IF accountType = "POSTPAID"
			TIBCO -> TIBCO: getEligibleFeaturesOrch
			group getIMEI
				TIBCO -> SQL: getIMEIWarranty
			end
			TIBCO -> TIBCO: IMEIWarranty
			group getSocsForSale
				TIBCO -> SAMSON: getSocsForSaleRequest
				SAMSON -> SAMSON: csGtEqpInd,csLsBanSub,evChkPromoElg,evGtCustType
				SAMSON -> TIBCO: getSocsForSaleResponse
			end
			TIBCO -> TIBCO: getSocsForSaleResponse
			group if ($getSocsForSale/pfx5:csiGetSocsForSaleResponse/pfx5:planInfo/pfx5:planType) != "NEO"
				TIBCO -> TIBCO: GetSocParameterProxy
				TIBCO -> SAMSON: getSocParameterRequest
				SAMSON -> SAMSON: csSrchSub,evGtAddPrm,evGtDataElig 
				SAMSON -> TIBCO: getSocParameterResponse
			end
			TIBCO -> TIBCO: getDataContractResponse
			group getSocCategory
				TIBCO -> TIBCO: getSocClassificationSvc
				TIBCO -> SAMSON: getSocClassificationRequest
				SAMSON -> SAMSON: evLsSocClTpCd 
				SAMSON -> TIBCO: getSocClassificationResponse
			end
			TIBCO -> TIBCO: getSocCategoryResponse			
			TIBCO -> TIBCO: mergeSocs
			group IF count($mergeSocs/pfx5:csiGetSocsForSaleResponse/pfx5:listOfSoc/ns9:soc[string-length(ns9:serviceCategory) > 0 and contains($TMO-App-Handset-Handset_Postpay_DataSoc_Cat_List, ns9:serviceCategory) or string-length(ns9:socIndicator) > 0 and contains(concat(string($TMO-App-Handset-Handset_Postpay_ContractData_SocInd_List), ",", string($TMO-App-Handset-Handset_Postpay_NonContractData_SocInd_List)), ns9:socIndicator)]) > 0
				TIBCO-> TIBCO: GetSocDataParametersSvc
				TIBCO -> SAMSON: GetSocDataParametersRequest
				SAMSON -> SAMSON: gnGtLgclDt,rfGtSocDtPrm 
				SAMSON -> TIBCO: GetSocDataParametersResponse
			end
			TIBCO -> TIBCO: GetSocDataParametersResponse
		end
		TIBCO -> TIBCO: getEligibleFeaturesResponse
		group otherwise
			TIBCO -> TIBCO: GetPrepaidSubscriberEligibleFeaturesSvc
			TIBCO -> RPX: csiGetPrepaidSubscriberEligibleFeaturesRequest
			RPX -> RPX: querySubscriberFeatureEligibility
			RPX -> TIBCO: csiGetPrepaidSubscriberEligibleFeaturesResponse
		end
		TIBCO -> TIBCO: csiGetPrepaidSubscriberEligibleFeaturesResponse
		TIBCO -> TIBCO: getEligibleFeaturesResponse
	end 
end 
TIBCO -> TIBCO : searchCustomerResponse
TIBCO -> Client : searchCustomerResponse
end
@enduml