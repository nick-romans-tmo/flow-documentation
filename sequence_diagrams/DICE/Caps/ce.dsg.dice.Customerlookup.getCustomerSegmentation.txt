@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> Customerlookup.getCustomerSegmentation Flow</font>
actor client
participant "RSP"
participant TBILL
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/CustomerLookupService.html#getCustomerSegmentation getCustomerSegmentation]]Offer.getCustomerSegmentation
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* getCustomerSegmentation request :
end note
alt INVALID_INPUT_PROVIDED,"dealerLookupRequired should be sent as true if associated sub market code needs to be retrieved for a given dealer code
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> TBILL :Calling B2BBANListLookupService 
activate TBILL
"RSP" <-- TBILL : getCustomerSegmentation 
deactivate TBILL
"RSP" --> "client": getCustomerSegmentation response
deactivate
@enduml