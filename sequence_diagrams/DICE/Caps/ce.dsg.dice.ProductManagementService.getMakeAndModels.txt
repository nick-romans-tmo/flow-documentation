@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> ProductManagementService.getMakeAndModels Flow</font>
actor client
participant "RSP"
participant ProductManagementServicePortType
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/ProductManagementService.html#getMakeAndModels getMakeAndModels]]ProductManagementService.getMakeAndModels
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* getMakeAndModelsRequest Request
end note
alt usn is must
"RSP" --> "client": ERRORMESSAGE
end
activate ProductManagementServicePortType
"RSP" -> ProductManagementServicePortType :Calling ProductManagementService
"RSP" <-- ProductManagementServicePortType : getMakeAndModels
deactivate 
@enduml











