@startuml
title ServiceAgreementService.GetSubscriberAgreement
actor Client
group <font color=#FF0090><b> GetSubscriberAgreement
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-05-18/sid/html/GetServiceAgreementWSC.html#GetSubscriberAgreement GetSubscriberAgreementInput]] 
   group <b> GetAccountDetails
        TIBCO -> TIBCO : SamsonAccount.GetAccountDetails
        group <b> SamsonAccount.GetAccountDetails [<font color=#FF0090> /SAMSON/ACCOUNT/ACCOUNTLOOKUP/GETACCOUNTDETAILS]
            TIBCO -> SAMSON : GetAccountDetails - csiGetAccountDetailsRequest
            SAMSON -> SAMSON: [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evGtCustType.html evGtCustType]]
            SAMSON --> TIBCO :csiGetAccountDetailsResponse
        end
        TIBCO -> TIBCO : csiGetAccountDetailsResponse
    end
     group <b> QueryServiceAgreement
        TIBCO -> TIBCO : SamsonSubsAgreementService.QueryServiceAgreement
        group <b> SamsonSubsAgreementService.QueryServiceAgreement
            TIBCO -> SAMSON : GetServiceAgreement - QueryServiceAgreementInput
            SAMSON -> SAMSON: [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/csGtSub.html csGtSub]]
            SAMSON -> TIBCO : QueryServiceAgreementOutput
        end
        TIBCO -> TIBCO : QueryServiceAgreementOutput
    end
    TIBCO -> Client : GetSubscriberAgreementOutput
end
@enduml