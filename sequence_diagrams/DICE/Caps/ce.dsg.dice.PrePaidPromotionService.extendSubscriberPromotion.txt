@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> PrePaidPromotionService.extendSubscriberPromotion Flow</font>
actor client
participant "RSP"
participant HttpServicesPort
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/PrePaidPromotionService.html#extendSubscriberPromotion extendSubscriberPromotion]]PrePaidPromotionService.extendSubscriberPromotion
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* extendSubscriberPromotionRequest Request
end note
alt Invalid input, For promotionId only numeric Values are allowed
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> HttpServicesPort :Calling HttpServices
activate HttpServicesPort
"RSP" <-- HttpServicesPort : extendSubscriberPromotion
deactivate
"RSP" --> "client": extendSubscriberPromotion response
deactivate
@enduml











