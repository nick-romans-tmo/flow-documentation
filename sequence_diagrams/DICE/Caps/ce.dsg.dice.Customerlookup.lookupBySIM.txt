@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> Customerlookup.lookupBySIM Flow</font>
actor client
participant "RSP"
participant SERVER
participant SAMSONTUXEDO
participant RSPDB
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/CustomerLookupService.html#lookupBySIM lookupBySIM]]Offer.lookupBySIM
activate "RSP"
activate "SERVER"
RSP -> SERVER: if gemfire enabled sending PARTNERID from Request
alt GEMFIRE ENABLED
SERVER -> RSP: getProfileDetails
deactivate SERVER
else GEMFIRE DISABLED
RSP -> RSPDB :Calling Customerlookup.lookupBySIM sql call with requested inputs-sending PARTNERID from Request
activate RSPDB
RSP <-- RSPDB : getPersonalProfileDetails
end
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* CustomerlookupBySIMRequest request
end note
alt INVALID_PARTNERID && Sim details
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> SAMSONTUXEDO :Calling Customerlookup.lookupBySIM 
activate SAMSONTUXEDO
"RSP" <-- SAMSONTUXEDO : lookupBySIM
deactivate
"RSP" --> "client": lookupBySIM response
deactivate
@enduml