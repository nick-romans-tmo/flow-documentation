@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> PrepaidService.getPrePaidRatePlans Flow</font>
actor client
participant "RSP"
participant RSPDB
participant SERVER
participant HttpServicesPort
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/PrepaidService.html#getOffersReferenceData getOffersReferenceData]]PrepaidService.getOffersReferenceData
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* getOffersReferenceDataRequest Request
end note
alt dealer code  is must
"RSP" --> "client": ERRORMESSAGE
end
activate HttpServicesPort
"RSP" -> HttpServicesPort :Calling HttpService
"RSP" <-- HttpServicesPort : queryEligiblePrepaidRatePlans
deactivate 
activate "SERVER"
RSP -> SERVER: if gemfire enabled sending PARTNERID from Request
alt GEMFIRE ENABLED
SERVER -> RSP: getProfileDetails
deactivate SERVER
else GEMFIRE DISABLED
activate RSPDB
RSP -> RSPDB :Calling getPartnerProfileDetails sql call with requested inputs
RSP <-- RSPDB : getPartnerProfileDetails
end
deactivate RSPDB
"RSP" --> "client": queryEligiblePrepaidRatePlans  response
@enduml











