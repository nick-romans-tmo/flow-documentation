@startuml
title AccountLookup.GetAccountSIMHistory
actor Client
group <font color=#FF0090><b> GetAccountSIMHistory
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-05-18/sid/html/AccountLookup.html#GetAccountSIMHistory esiGetAccountSIMHistoryRequest]] 
     group GetAccountSIMHistory
            TIBCO -> TIBCO : SamsonAccountService.GetAccountSIMHistory
                group SamsonAccountService.GetAccountSIMHistory [<font color=#FF0090> /SAMSON/ACCOUNT/ACCOUNTLOOKUP/GETACCOUNTSIMHISTORY/]
                    TIBCO -> SAMSON : csiGetAccountSIMHistoryRequest
                    SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/gnLsEsnHst.html gnLsEsnHst]] 
                    SAMSON -> TIBCO : csiGetAccountSIMHistoryResponse
                end
            TIBCO --> TIBCO : csiGetAccountSIMHistoryResponse
        end
TIBCO -> Client : esiGetAccountSIMHistoryResponse
end
@enduml