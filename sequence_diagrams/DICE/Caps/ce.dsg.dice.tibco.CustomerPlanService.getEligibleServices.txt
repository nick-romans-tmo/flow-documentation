@startuml
title CustomerPlanService.getEligibleServices
actor Client
group <font color=#FF0090><b> getEligibleServices
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-05-18/sid/html/CustomerPlanService.html#getEligibleServices getEligibleServicesRequest]] 
    group <b> only ban
        TIBCO -> SAMSON : csiGetSubscribersForBanRequest
        SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/csLsBanSub.html csLsBanSub]] 
        SAMSON --> TIBCO : csiGetSubscribersForBanResponse
    end
    group <b> QueryAccountDetails
        group <b> no BAN?
        TIBCO -> SQLManager : getSubDetails
        SQLManager -> SQLManager : [[sqlmanager/get/GetSubscriberDetails sqlmanager/get/GetSubscriberDetails]]
        SQLManager -> TIBCO : ListOfSubscribers
        end
        TIBCO -> SQLManager : getAccountDetails
        SQLManager -> SQLManager : [[sqlmanager/get/GetAccountDetails sqlmanager/get/GetAccountDetails]]
        SQLManager -> TIBCO : Customer
        group <b> ValidateCustomer
            group <b> Enbale MKB Val?
                TIBCO -> SAMSON : csiGetServicePartnerRequest
                SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evGtCustType.html evGtCustType]] 
                SAMSON --> TIBCO : csiGetServicePartnerResponse
            end
            group <b> otherwise checkSocEligibility
                group <b> has creditClass?
                    TIBCO -> SQLManager : getAccountDetails
                    SQLManager -> SQLManager : [[sqlmanager/get/GetAccountDetails sqlmanager/get/GetAccountDetails]]
                    SQLManager -> TIBCO : Customer
                end
                group <b> otherwise
                    TIBCO -> SQLManager : getBillInfo
                    SQLManager -> SQLManager : [[sqlmanager/get/GetBillInfo sqlmanager/get/GetBillInfo]]
                    SQLManager -> TIBCO : OneFullpayment
                end
            end
        end
    end
    group if Subscriber exists and revisedVersion >= "2"
        TIBCO -> SQLManager : GetSubscriberSubMarket
        SQLManager -> SQLManager : [[sqlmanager/get/GetSubscriberSubMarket sqlmanager/get/GetSubscriberSubMarket]]
        SQLManager -> TIBCO : SubscriberSubMarket
    end
    group otherwise
        TIBCO -> SQLManager : GetServiceAgreement
        SQLManager -> SQLManager : [[sqlmanager/get/GetServiceAgreement sqlmanager/get/GetServiceAgreement]]
        SQLManager -> TIBCO : ServiceAgreement
    end
    group GetServicesForSaleRevised [if revisedVersion >= "2"]
        TIBCO -> SAMSON : csiGetEquipmentInstallmentIndicatorsRequest
        SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/csGtEqpInd.html csGtEqpInd]] 
        SAMSON --> TIBCO : csiGetEquipmentInstallmentIndicatorsResponse
        group socInd
            TIBCO -> SQLManager : getSocForSale
            SQLManager -> SQLManager : [[sqlmanager/query/GetServicesForSale sqlmanager/query/GetServicesForSale]]
            SQLManager -> TIBCO : SocForSale
        end
        group queryProductCatalog
            group exist socInd?
                TIBCO -> TIBCO : [[http://localhost:8085/ProductLookup.asmx ProductLookup.GetWeb2GoDeviceServiceCompatibility]]
            end
            group otherwise
                TIBCO -> TIBCO : [[http://localhost:8085/ProductLookup.asmx ProductLookup.GetCompatibleServicesForRatePlanAndDevice]]
            end
        end
        group Get Rel Socs [if (SOC_IND exists and TMO-App-Samson-Soc-PromoSocsIndicators contains SOC_IND and relatedSocType = ("S","BS")) or (retrieveRelatedService is true and relatedSocCount >0)]
            group getRelatedSocs
                TIBCO -> SQLManager : csiGetRelatedSocsRequest
                SQLManager -> SQLManager : [[sqlmanager/get/GetSocRelationShipAll sqlmanager/get/GetSocRelationShipAll]]
                SQLManager -> TIBCO : RelatedSocs
            end
            group GetPromoEligibility [if TMO-App-Samson-Soc-PromoSocsIndicators contains csiGeRelatedSocsResponse/pfx8:soc/pfx8:relatedSoc/ns27:socIndicator)]
                TIBCO -> SAMSON : promosoc
                SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evChkPromoElg.html evChkPromoElg]] 
                SAMSON --> TIBCO : promosoc
            end
        end
    end
    group otherwise
        group if socInd exists
            TIBCO -> SQLManager : getSocForSale
            SQLManager -> SQLManager : [[sqlmanager/query/GetServicesForSale sqlmanager/query/GetServicesForSale]]
            SQLManager -> TIBCO : SocForSale
        end
        group queryProductCatalog
            group exist socInd?
                TIBCO -> TIBCO : [[http://localhost:8085/ProductLookup.asmx ProductLookup.GetWeb2GoDeviceServiceCompatibility]]
            end
            group otherwise
                TIBCO -> TIBCO : [[http://localhost:8085/ProductLookup.asmx ProductLookup.GetCompatibleServicesForRatePlanAndDevice]]
            end
        end
        group Get Rel Socs [if (SOC_IND exists and TMO-App-Samson-Soc-PromoSocsIndicators contains SOC_IND and relatedSocType = ("S","BS")) or (retrieveRelatedService is true and relatedSocCount >0)]
            group getRelatedSocs
                TIBCO -> SQLManager : csiGetRelatedSocsRequest
                SQLManager -> SQLManager : [[sqlmanager/get/GetSocRelationShipAll sqlmanager/get/GetSocRelationShipAll]]
                SQLManager -> TIBCO : RelatedSocs
            end
            group GetPromoEligibility [if TMO-App-Samson-Soc-PromoSocsIndicators contains csiGeRelatedSocsResponse/pfx8:soc/pfx8:relatedSoc/ns27:socIndicator)]
                TIBCO -> SAMSON : promosoc
                SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evChkPromoElg.html evChkPromoElg]] 
                SAMSON --> TIBCO : promosoc
            end
        end
        TIBCO -> SAMSON : csiGetEquipmentInstallmentIndicatorsRequest
        SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/csGtEqpInd.html csGtEqpInd]] 
        SAMSON --> TIBCO : csiGetEquipmentInstallmentIndicatorsResponse
    end
TIBCO -> Client : getEligibleServicesResponse
end
@enduml