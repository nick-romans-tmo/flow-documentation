@startuml
title SIVRService.QueryBillFormat
actor Client
group <font color=#FF0090><b> QueryBillFormat [<b> /sivr/ivr/bill/format/query]
Client->TIBCO: SivrQueryBillFormatRequest
    group SamsonPaymentService.SamsonQueryBillFormat [<font color=#FF0090><b>/SAMSON/BILL/FORMAT/QUERY</font>]
        group querySubcriber [if BAN does not exists]
            TIBCO -> SQLManager : SubscriberNo
            SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetSubscriberDetails&action=query&keyValue= sqlmanager/query/GetSubscriberDetails]]
            TIBCO <- SQLManager : ListOfSubscribers
        end
        group queryAccountDetailsForBan
            TIBCO -> SQLManager : Ban
            SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetAccountDetails&action=query&keyValue= sqlmanager/query/GetAccountDetails]]
            TIBCO <- SQLManager : Customer
        end
        group checkSocEligibility [if CheckSocEligibility = true]
            group getAccountDetails [if CreditClass does not exists]
                TIBCO -> SQLManager : Ban
                SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetAccountDetails&action=query&keyValue= sqlmanager/query/GetAccountDetails]]
                TIBCO <- SQLManager : Customer
            end
            group queryOneFullPayment
                TIBCO -> SQLManager : Ban
                SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetBillInfo&action=query&keyValue= sqlmanager/query/GetBillInfo]]
                TIBCO <- SQLManager : OneFullPayment
            end
        end
        group Check queryCurrentBillFormatIndicator [if queryCurrentBillFormatFlag is true]
            group GetSubscriberDetails [ban does not exists]
                TIBCO -> SQLManager : SubscriberNo, if getReserved = true then pGetReserved is "Y" else "N"
                SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetSubscriberDetails_Dual&action=query&keyValue= sqlmanager/query/GetSubscriberDetails]]
                TIBCO <- SQLManager : ListOfSubscribers
            end
            group PBGetDetails
                TIBCO -> SQLManager : BAN
                SQLManager -> SQLManager : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetPaperlessBillingInfoByBan&action=query&keyValue= sqlmanager/query/GetPaperlessBillingInfoByBan]]
                TIBCO <- SQLManager : PaperlessBillReply
            end
        end
        group QueryPaperBillEligibility [if queryEligibilityBillFormatFlag true]
            TIBCO -> SAMSON : rfGtCdsElig-input
            SAMSON -> SAMSON :[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/205/services/rfGtCdsElig.html rfGtCdsElig]] 
            TIBCO <- SAMSON :rfGtCdsElig-Output
        end
        group Call QueryBillFormat [PaperlessBillResponse/PB_STATUS != "A" or PaperlessBillResponse/EXPIRATION_DATE exists]
            TIBCO -> SAMSON : csGtBlSupDtl-input
            SAMSON -> SAMSON :[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/205/services/csGtBlSupDtl.html csGtBlSupDtl]] 
            TIBCO <- SAMSON :csGtBlSupDtl-Output
        end
    end
TIBCO -> Client : SivrQueryBillFormatResponse
end
@enduml