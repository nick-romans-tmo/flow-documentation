@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> Offers.getOffers Flow</font>
actor client
participant "RSP"
participant EPIPHANY
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/OfferService.html#getOffers getOffers]]Offer.getOffers
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* getOffers request.getBan and other Mandatory fields are missing
end note
alt Invalid Input
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> EPIPHANY :Calling EPIPHANY offers
activate EPIPHANY
"RSP" <-- EPIPHANY : getOffers 
deactivate EPIPHANY
"RSP" --> "client": getOffers response
deactivate
@enduml