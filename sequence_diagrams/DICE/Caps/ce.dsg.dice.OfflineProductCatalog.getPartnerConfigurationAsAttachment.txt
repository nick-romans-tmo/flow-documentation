@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> OfflineProductCatalog.getPartnerConfigurationAsAttachment Flow</font>
actor client
participant "RSP"
participant SftpRemoteFileTemplate
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/OfflineProductCatalogService.html#getPartnerConfigurationAsAttachment getPartnerConfigurationAsAttachment]]OfflineProductCatalogService.getPartnerConfigurationAsAttachment
activate "RSP"
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* partnerConfigurationAsZipAttachmentRequest request.getParnerid and effective date
end note
alt The request did not contain a configured partner Id. Please enter a configured partner Id and also The request did not contain a effective date. Please select a valid date
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> SftpRemoteFileTemplate :Calling SftpRemoteFileTemplate .getPartnerConfigurationAsAttachment 
activate SftpRemoteFileTemplate
"RSP" <-- SftpRemoteFileTemplate : getPartnerConfigurationAsAttachment
deactivate 
"RSP" --> "client": getPartnerConfigurationAsAttachment response
deactivate
@enduml