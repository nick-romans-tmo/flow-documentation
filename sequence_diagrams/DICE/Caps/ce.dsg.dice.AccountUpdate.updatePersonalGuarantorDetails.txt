@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title font size=18 color=#b40d63 AccountUpdate.updatePersonalGuarantorDetails Flowfont
actor client
participant RSP
participant SAMSONTUXEDO
client - RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/AccountUpdateService.html#updatePersonalGuarantorDetails updatePersonalGuarantorDetails]]Activation.updatePersonalGuarantorDetails
activate RSP
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* updatePersonalGuarantorDetails request.getPersonalGuarantorInfo
end note
alt Guarantor should be minimum 18 years of age,If ID Type is DL or ID, then ID Issuing State should be passed in the request and SSN provided in the request should be of 9 digits and valid
"RSP" --> "client": ERRORMESSAGE
end
updatePersonalGuarantorDetails - SAMSONTUXEDO Calling updatePersonalGuarantorDetails
activate SAMSONTUXEDO
updatePersonalGuarantorDetails -- SAMSONTUXEDO  updatePersonalGuarantorDetails 
deactivate SAMSONTUXEDO
updatePersonalGuarantorDetails -- client updatePersonalGuarantorDetails response
deactivate
@enduml