@startuml
title Promotion.GetPrepaidPromotions
actor Client
group <font color=#FF0090><b> GetPrepaidPromotions
Client -> TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-05-18/sid/html/Promotion.html#GetPrepaidPromotions getPrepaidPromotionsRequest]]     
              
                  
                  TIBCO -> RPX: Call queryPrepaidPromotionsRequest
	          RPX -> RPX: [[http://targetrpxsoapproxy-qlab01-dam.test.px-npe02a.cf.t-mobile.com/rpxHttpService]]
	          RPX -> TIBCO : queryPrepaidPromotionsResponse
              
                 
TIBCO -> Client : getPrepaidPromotionsResponse
end
@enduml