@startuml
title CustomerService.getSubscriberInfo
actor Client
group <font color=#FF0090><b> getSubscriberInfo
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-05-18/sid/html/CustomerService.html#getSubscriberInfo getSubscriberInfoRequest]]
    group GetContractServiceEndDateTux [if senderId is not equals to ODAM]
        TIBCO -> SAMSON : GetContractServiceEndDateRequest
        SAMSON -> SAMSON :[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/205/services/evGtAddPrm.html evGtAddPrm]] 
        TIBCO <- SAMSON : GetContractServiceEndDateResponse
    end
    group GetSubscriberInfo [If MSISDN is exists in the request]
        TIBCO -> RSP : getSubscribersInfoRequest
        RSP -> RSP :[[http://localhost:7001/eProxy/service/PostPaidSubscriberService_SOAP_V1 PostPaidSubscriberService.getSubscribersInfo]] 
        TIBCO <- RSP : getSubscribersInfoResponse
    end
        group GetBasicAccountDetails [If TMO-App-CustomerManagement-CustomerService-getSubscriberInfo_InvokeRefactored = "Y"]
            group GetCustomerVerificationInfoTux [If LAST_UPDATE_DATE and LAST_UPDATE_STAMP are not present]
                TIBCO -> SAMSON : csiGetCustomerVerificationInfoRequest
                SAMSON -> SAMSON :[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/205/services/evGtVerInfo.html evGtVerInfo]] 
                TIBCO <- SAMSON : csiGetCustomerVerificationInfoResponse
            end
            group GetBasicAccountInfoForBAN
                TIBCO -> SAMSON : csiGetBasicAccountInfoRequest
                SAMSON -> SAMSON :[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/205/services/evGtCustInfo.html evGtCustInfo]] 
                TIBCO <- SAMSON : csiGetBasicAccountInfoResponse
            end
            group GetBillingInfo [retrieveAdditionalBillingInfo = "true"]
                TIBCO -> SAMSON : csiGetAdditionalBillingInfoRequest
                SAMSON -> SAMSON :[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/205/services/evGtBanHdr.html evGtBanHdr]] 
                TIBCO <- SAMSON : csiGetAdditionalBillingInfoResponse
            end
            group GetCreditClass [If CreditClassEligibleApplication and header/application present]
                TIBCO -> SAMSON : csiGetAccountCreditClassRequest
                SAMSON -> SAMSON :[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/205/services/evCrdClsElg.html evCrdClsElg]] 
                TIBCO <- SAMSON : csiGetAccountCreditClassResponse
            end
            group GetPaperlessBillDetails [retrievePaperlessBillDetails = "true"]
                TIBCO -> SAMSON : csiGetPaperlessBillDetailsRequest
                SAMSON -> SAMSON :[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/205/services/csLsPBInfo.html csLsPBInfo]] 
                TIBCO <- SAMSON : csiGetPaperlessBillDetailsResponse
            end
        end
TIBCO -> Client : getSubscriberInfoResponse
end
@enduml