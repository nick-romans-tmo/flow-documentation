@startuml
title AccountManagement.CreateTentativeBan
actor Client
group <font color=#FF0090><b> CreateTentativeBan
Client->AccountManagementService: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/AccountManagement.html#createTentativeBan CreateTentativeBan]]  AccountManagement.CreateTentativeBan
AccountManagementService->TIBCO:Receiving request
TIBCO->SAMSON:Tibco invokes the TUX API gnMtchZipAdr to validate the zip code
TIBCO->SAMSON:Tibco invokes the TUX API gnMatchAdr to validate the address
TIBCO->SAMSON:Tibco invokes the TUX API gnGtGeoCd to validate the geoCode
TIBCO->SAMSON:Tibco invokes the TUX API arInCstBan to create Tentative BAN
TIBCO->SAMSON:Tibco invokes the TUX API arUpBan to update the Collection Waiver info
SAMSON->TIBCO:Return response back
TIBCO->AccountManagementService:Return response back to the client. 
AccountManagementService->Client:CreateTentativeBan Response to client
end group
@enduml