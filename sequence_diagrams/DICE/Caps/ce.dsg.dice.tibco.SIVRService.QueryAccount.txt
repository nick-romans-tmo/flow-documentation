@startuml
title SIVRService.QueryAccount
actor Client
group <font color=#FF0090><b> QueryAccount [<b>/sivr/ivr/customer/bankaccounts/query]
Client->TIBCO: SIVRQueryAccountInput
    group SamsonAccountService.SIVR_getqueryaccount [<font color=#FF0090><b>/SAMSON/SAMSONSQL/CUSTOMER/PAYMENTINFO/QUERY</font>]
        group CallPaymentInfo
            TIBCO -> SQLManager : Ban
            SQLManager -> SQLManager : GetPaymentInfo
            TIBCO <- SQLManager : PaymentInfo
        end
    end
TIBCO -> Client : SIVRQueryAccountOutput
end
@enduml
