@startuml
title EnterpriseAddressService.RetrieveAllSubsAddress
actor Client
group <font color=#FF0090><b> RetrieveAllSubsAddress     
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-04-13/sid/html/RetrieveAllSubsAddress.html#RetrieveAllSubsAddress esiRetrieveAllSubscriberAddressRequest]] 
TIBCO -> TIBCO : Call SamsonSubsQuery.RetrieveAllSubsAddress
group <b> SamsonSubsQuery.RetrieveAllSubsAddress [<font color=#FF0090> /SAMSON/SUBSCRIBER/ADDRESS/RETRIEVEALL</font>] : csiRetrieveAllSubscriberAddressRequest
    TIBCO -> SAMSON: Call RetrieveAllSubsAddressTux
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/csLsSubAdrs.html csLsSubAdrs]]
	SAMSON -> TIBCO : csiRetrieveAllSubscriberAddressResponse
end
TIBCO -> TIBCO : esiRetrieveAllSubscriberAddressResponse
TIBCO -> Client : esiRetrieveAllSubscriberAddressResponse
end
@enduml