@startuml
title AccountSearch.GetPrimaryMSISDNForBAN
actor Client
group <font color=#FF0090><b> GetPrimaryMSISDNForBAN
Client->AccountSearchService: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/AccountSearch.html#getPrimaryMSISDNForBAN GetPrimaryMSISDNForBAN]]  AccountSearch.GetPrimaryMSISDNForBAN
AccountSearchService->ESP:Sending request
ESP->SAMSON:ESP sending to the samson spoke
SAMSON->SAMSON :validate the input And Pass the BAN value to tuxedo service - csLsBanSub to recieve the Primary MSISDN
alt
SAMSON->ESP: Return success response
SAMSON->ESP: if any validation error occurs, It will throw a fault message to client
end group
ESP->AccountSearchService:ESP send the success response
AccountSearchService->Client:GetPrimaryMSISDNForBAN Response to client
end group
@enduml
