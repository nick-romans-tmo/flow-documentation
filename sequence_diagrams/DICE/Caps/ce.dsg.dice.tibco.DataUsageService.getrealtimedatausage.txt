@startuml
title DataUsageService.GetRealTimeDataUsage
actor Client
group <font color=#FF0090><b> GetRealTimeDataUsage
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-04-13/sid/html/DataUsage.html#RealTimeDataUsage esiGetRealTimeDataUsageRequest]] 

                  TIBCO-> DUS: Call GetRealTimeDataUsageHTTP
                  DUS-> DUS: csiGetRealTimeDataUsageRequest/CustomerTransaction[[https://ecc.internal.t-mobile.com:8081/]]  
                  DUS->TIBCO: csiGetRealTimeDataUsageResponse

                  group <b> If includeDataPassInfo = true in the request
                  TIBCO->TIBCO: Call SamsonSubsQuery.getPassAgreement 
                  group <b> SamsonSubsQuery.getPassAgreement [<font color=#FF0090> /SAMSON/SUBSCRIBER/PASSAGREEMENT/GET</font>] : getPassAgreementRequest
                  TIBCO -> SAMSON: Call GetPassUsageSummaryTux
	          SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/evLsSubPasAgr.html evLsSubPasAgr]]
	          SAMSON -> TIBCO : getPassAgreementResponse
                  end 
                  end               
                       

TIBCO -> TIBCO : esiGetRealTimeDataUsageResponse
TIBCO -> Client : esiGetRealTimeDataUsageResponse
end
@enduml