@startuml
skinparam ParticipantPadding 10
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
title <font size="18" color=#b40d63> RatePlanManagement.getServicesForSale Flow</font>
actor client
participant "RSP"
participant SAMSONTUXEDO
client -> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/RatePlanManagementService.html#getServicesForSale getServicesForSale]]RatePlanManagement.getServicesForSale
activate "RSP"
activate SAMSONTUXEDO
RSP -> SAMSONTUXEDO: if Activation enabled/true calling getServicesForSale
alt Activated 
SAMSONTUXEDO -> RSP: getServicesForSale data
else Activation disabled/false
RSP -> SAMSONTUXEDO :Calling getSocsForSaleOperation
RSP <-- SAMSONTUXEDO : getSocsForSaleOperation data
end
"RSP" -> "RSP" : Validate the Request
note over of "RSP" #f399c7
<b>Validations</b>
* RetailProductCatalogSearchInfo request.getServiceLevel && Ban
end note
alt BAN is mandatory. Search by accountTypeSubType is not supported && Subscriber number is mandatory for SUBSCRIBER/ALL level request
"RSP" --> "client": ERRORMESSAGE
end
"RSP" -> SAMSONTUXEDO :Calling getAalServicesForRateplan
"RSP" <-- SAMSONTUXEDO : getAalServicesForRateplan data
deactivate
"RSP" --> "client": getServicesForSale response
deactivate
@enduml