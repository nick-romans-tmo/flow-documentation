@startuml
title CustomerProfileService.GetCustomerProfile
actor Client
group <font color=#FF0090><b> GetCustomerProfile
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2019-04-13/sid/html/CustomerProfile.html#GetCustomerProfile GetCustomerProfileRequest]]   
                  
                  group <b> If CHUBEnableIndicator GV is true
                      group <b> CHUB call
                          group <b> CHUBU1-ENABLE
                           group <b> If BAN exists
                            TIBCO-> CustomerHub: ContactInfoLookupService.lookupBANContactInfo lookupBANContactInfo_Input
                            CustomerHub -> CustomerHub: [[http://devchub002.unix.gsm1900.org:7777/eai_enu/start.swe?SWEExtSource=WebService&SWEExtCmd=Execute&WSSOAP=1]]
                            CustomerHub -> TIBCO: lookupBANContactInfo_Output
                            end
                             group <b> If MSISDN exists
                            TIBCO-> CustomerHub: ContactInfoLookupService.lookupMSISDNContactInfo lookupMSISDNContactInfo_Input
                            CustomerHub -> CustomerHub: [[http://devchub002.unix.gsm1900.org:7777/eai_enu/start.swe?SWEExtSource=WebService&SWEExtCmd=Execute&WSSOAP=1]]
                            CustomerHub -> TIBCO: lookupMSISDNContactInfo_Output
                            end
                            group <b> If profileInfo = MARKETING_PRIMARY
                            TIBCO-> CustomerHub: PrimaryEmailLookupService.lookupPrimaryEmail lookupPrimaryEmail_Input
                            CustomerHub -> CustomerHub: [[http://devchub002.unix.gsm1900.org:7777/eai_enu/start.swe?SWEExtSource=WebService&SWEExtCmd=Execute&WSSOAP=1]]
                            CustomerHub -> TIBCO: lookupPrimaryEmail_Output
                            end
                          end
                          group <b> Otherwise
                          group <b> If BAN exists
                            TIBCO-> CustomerHub: ContactInfoLookupU2Service.lookupBANContactInfo lookupBANContactInfo_Input
                            CustomerHub -> CustomerHub: [[http://webserver/eai_lang/start.swe?SWEExtSource=WebService&SWEExtCmd=Execute&WSSOAP=1]]
                            CustomerHub -> TIBCO: lookupBANContactInfo_Output
                            end
                             group <b> If MSISDN exists
                            TIBCO-> CustomerHub: ContactInfoLookupU2Service.lookupMSISDNContactInfo lookupMSISDNContactInfo_Input
                            CustomerHub -> CustomerHub: [[http://webserver/eai_lang/start.swe?SWEExtSource=WebService&SWEExtCmd=Execute&WSSOAP=1]]
                            CustomerHub -> TIBCO: lookupMSISDNContactInfo_Output
                            end
                            group <b> If profileInfo = MARKETING_PRIMARY
                            TIBCO-> CustomerHub: PrimaryEmailLookupU2Service.lookupPrimaryEmail lookupPrimaryEmail_Input
                            CustomerHub -> CustomerHub: [[http://webserver/eai_lang/start.swe?SWEExtSource=WebService&SWEExtCmd=Execute&WSSOAP=1]]
                            CustomerHub -> TIBCO: lookupPrimaryEmail_Output
                            end
                          end
                           
                        end
                      group <b> CP call

                        group <b> 1st MSISDN to 5th MSISDN exists
                  TIBCO-> CustomerProfile: CustomerProfileService.GetCustomerProfile csiGetCustomerProfileRequest
                  CustomerProfile-> CustomerProfile: [[http://customerprofile-qlab02.test.px-npe01.cf.t-mobile.com/cp/CustomerProfileService]]
                  CustomerProfile-> TIBCO: csiGetCustomerProfileResponse
                        end 
                        group <b> MSISDN not exists
                           TIBCO-> CustomerProfile: CustomerProfileService.GetCustomerProfile csiGetCustomerProfileRequest
                  CustomerProfile-> CustomerProfile: [[http://customerprofile-qlab02.test.px-npe01.cf.t-mobile.com/cp/CustomerProfileService]]
                  CustomerProfile-> TIBCO: csiGetCustomerProfileResponse
                        end

                       end
                   end
                  group <b> Otherwise
                    group <b> If ServiceType = NONCUSTOMER in the request
                       TIBCO-> CustomerProfile: CustomerProfileService.GetNonCustomer SearchCustomerProfileRequest
                  CustomerProfile-> CustomerProfile: [[http://customerprofile-qlab02.test.px-npe01.cf.t-mobile.com/cp/CustomerProfileService]]
                  CustomerProfile-> TIBCO: SearchCustomerProfileResponse
                    end
                   group <b> Otherwise
                       TIBCO-> CustomerProfile: CustomerProfileService.GetProfileFromQueue GetProfileFromQueueRequest
                  CustomerProfile-> CustomerProfile: [[http://customerprofile-qlab02.test.px-npe01.cf.t-mobile.com/cp/CustomerProfileService]]
                  CustomerProfile-> TIBCO: GetProfileFromQueueResponse
                     group <b> If profileId found in ProfileQueueResponse and Source in request doesn't contains SAVE
                       TIBCO-> CustomerProfile: CustomerProfileService.GetCustomerAddress GetProfileAddressInfoRequest
                  CustomerProfile-> CustomerProfile: [[http://customerprofile-qlab02.test.px-npe01.cf.t-mobile.com/cp/CustomerProfileService]]
                  CustomerProfile-> TIBCO: GetProfileAddressInfoResponse
                       TIBCO-> CustomerProfile: CustomerProfileService.GetCustomerPhone GetProfilePhoneInfoRequest
                  CustomerProfile-> CustomerProfile: [[http://customerprofile-qlab02.test.px-npe01.cf.t-mobile.com/cp/CustomerProfileService]]
                  CustomerProfile-> TIBCO: GetProfilePhoneInfoResponse
                       TIBCO-> CustomerProfile: CustomerProfileService.GetCustomerEmail GetProfileEmailInfoRequest
                  CustomerProfile-> CustomerProfile: [[http://customerprofile-qlab02.test.px-npe01.cf.t-mobile.com/cp/CustomerProfileService]]
                  CustomerProfile-> TIBCO: GetProfileEmailInfoResponse
                       TIBCO-> CustomerProfile: CustomerProfileService.GetCustomerOfferCode GetProfileOfferCodeInfoRequest
                  CustomerProfile-> CustomerProfile: [[http://customerprofile-qlab02.test.px-npe01.cf.t-mobile.com/cp/CustomerProfileService]]
                  CustomerProfile-> TIBCO: GetProfileOfferCodeInfoResponse
                       TIBCO-> CustomerProfile: CustomerProfileService.GetCustomerAttribute GetAttributeDefaultRequest
                  CustomerProfile-> CustomerProfile: [[http://customerprofile-qlab02.test.px-npe01.cf.t-mobile.com/cp/CustomerProfileService]]
                  CustomerProfile-> TIBCO: GetAttributeDefaultResponse
                      end
                     group <b> Otherwise
                      group <b> If AttributeLevel = 1 in the request
                      TIBCO-> CustomerProfile: CustomerProfileService.GetCustomer GetProfileSubscriberInfoRequest
                  CustomerProfile-> CustomerProfile: [[http://customerprofile-qlab02.test.px-npe01.cf.t-mobile.com/cp/CustomerProfileService]]
                  CustomerProfile-> TIBCO: GetProfileSubscriberInfoResponse
                      end
                     group <b> Otherwise
                      TIBCO-> CustomerProfile: CustomerProfileService.GetAccount GetProfileAccountInfoRequest
                  CustomerProfile-> CustomerProfile: [[http://customerprofile-qlab02.test.px-npe01.cf.t-mobile.com/cp/CustomerProfileService]]
                  CustomerProfile-> TIBCO: GetProfileAccountInfoResponse
                      end
                     end
                    
                  
                    end
                  end
                 
TIBCO -> Client : GetCustomerProfileResponse
end
@enduml