@startuml

title PromotionCouponEnterprise.generatePromotionCoupon Service \nThis service retrieves the next available single use promotional coupons for the specified promotion from RPX

participant "Rebellion" as Client
actor Client

group <font color=#FF0090><b> generatePromotionCoupon
Client->TIBCO: [[https://servicecatalog.devcenter.t-mobile.com/servicecat/tibco/U2-NationalRelease/asciidoc/Uprising/generatePromotionCoupon.html generatePromotionCouponRequest]]
TIBCO -> TIBCO : Validate Headers[senderId, \nchannelId and applicationId are required]
TIBCO -> TIBCO : Validate promotionInfo[promotionID, promotionSystem]
alt sucess
TIBCO -> RPX: InvokeRPX (getOnDemandPromotionalCoupon Request)
RPX->TIBCO:getOnDemandPromotionalCouponResponse
TIBCO->TIBCO: Transform Output
TIBCO->Client:generatePromotionCouponResponse

else throw errors

TIBCO->TIBCO: Validation Errors

end

group <font color=#FF8C00><b>  Error Response
RPX->TIBCO : (RPX or TIBCO) Errors

TIBCO->Client : Error Details

end
end group
@enduml