@startuml

title PromotionCouponEnterprise.updatePromotionCoupoStatus Service \nThis service updates the status in RPX for one promotion coupon in the request

participant "Ericsson" as Client
actor Client

group <font color=#FF0090><b> updatePromotionCoupoStatus
Client->TIBCO: [[https://servicecatalog.devcenter.t-mobile.com/servicecat/tibco/U2-NationalRelease/asciidoc/Uprising/updatePromotionCouponStatus.html updatePromotionCoupoStatusRequest]]
TIBCO -> TIBCO : Validate Headers[senderId, \nchannelId and applicationId are required]
TIBCO -> TIBCO : Validate promotionInfo[serialNumber required & length should be \nmin 8, max 11, And statusCode shouldbe AVAILABLE\REDEEM\RESERVE]
alt sucess

group <font color=#FF0090><b>promotionInfo/status/@statusCode = "AVAILABLE"
TIBCO -> RPX: InvokeUpdateToAvailableSoap \n(updatePromotionalCouponToAvailable Request)
RPX->TIBCO:updatePromotionalCouponToAvailable Response
end

group <font color=#FF0090><b>promotionInfo/status/@statusCode = "REDEEM"
TIBCO -> RPX: InvokeUpdateToRedeemSoap\n (updatePromotionalCouponToRedeemed Request)
RPX->TIBCO:updatePromotionalCouponToRedeemed Response
end

group <font color=#FF0090><b>promotionInfo/status/@statusCode = "RESERVE"
TIBCO -> RPX: InvokeUpdateToReserveSoap \n(updatePromotionalCouponToReserverd Request)
RPX->TIBCO:updatePromotionalCouponToReserverd Response
end

TIBCO->TIBCO: Merge Response
TIBCO->Client:updatePromotionCoupoStatus Response

else throw errors

TIBCO->TIBCO: Validation Errors

end

group <font color=#FF8C00><b>  Error Response
RPX->TIBCO : (RPX or TIBCO) Errors

TIBCO->Client : Error Details

end
end group
@enduml