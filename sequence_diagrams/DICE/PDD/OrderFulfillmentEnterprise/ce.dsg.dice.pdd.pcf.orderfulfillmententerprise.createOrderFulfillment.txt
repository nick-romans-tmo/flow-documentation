@startuml
title OrderFulfillmentEnterprise.createOrderFulfillment Service \nThis service will be used by Ericsson OM for initiating fulfillment of an order to SAPPI.


actor Client

group <font color=#FF0090><b> createOrderFulfillment
Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/U2-NationalRelease/asciidoc/Uprising/createOrderFulfillment.html createOrderFulfillmentRequest]]
TIBCO -> TIBCO : Validate Headers[senderId, channelId and applicationId are required]
TIBCO -> TIBCO : getCountryCodes
TIBCO -> TIBCO : csom:createOrderFulfillmentRequest to mt_OrderCreateRequest
TIBCO->SAPPI : Call InvokeSAPPI [WSDL:si_UniversalSalesOrderCreateRequest.OrderCreate] SAPPI one way service.
SAPPI->TIBCO: No response is expected.

TIBCO->ReeF: if OrderType=RETURN, process "ReturnShipmentTrackingNumberCreated" event to "UPS", "OpenText" and "SAPPI" eventhanlders

TIBCO->Client: createOrderFulfillmentResponse

group <font color=#FF8C00><b>  Error Response
SAPPI->TIBCO: (SAPPI or TIBCO) Errors
TIBCO->Client: createOrderFulfillmentResponse with Error details
end
end group
@enduml