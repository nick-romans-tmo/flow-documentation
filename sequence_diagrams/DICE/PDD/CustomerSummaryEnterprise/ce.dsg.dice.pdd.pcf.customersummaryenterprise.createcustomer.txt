@startuml
participant Client order 10
participant TIBCO order 20
participant CHUB order 60
title CustomerSummaryEnterprise.customerCustomer Service \nThis service will be invoked by Ericsson to create customer details in CustomerHub.

actor Client

group <font color=#FF0090><b> createCustomer
Client->TIBCO: [[https://servicecatalog.devcenter.t-mobile.com/servicecat/tibco/U2-NationalRelease/asciidoc/Uprising/createCustomer.html createCustomerRequest]]
TIBCO -> TIBCO : Validate Headers[senderId, \nchannelId and applicationId are required]
Alt Sucess
TIBCO -> TIBCO : Tranform csom:createCustomerRequest to createPrepaidCustomer_Input
TIBCO->CHUB : Call CreatePrepaidCustomerSoap [WSDL:PrepaidCustomerService.createPrepaidCustomer] CustomerHub service
CHUB->TIBCO: CHUB createPrepaidCustomer_Output Response

TIBCO->TIBCO: Transform createPrepaidCustomer_Output to csom:createCustomerResponse

TIBCO->Client: createCustomerResponse
end

group <font color=#FF8C00><b>  Failure Response

TIBCO->TIBCO: Validation Errors
CHUB->TIBCO:CHUB Error Flow
TIBCO->Client : createCustomerResonpse with TIBCO Error

end
end group
@enduml