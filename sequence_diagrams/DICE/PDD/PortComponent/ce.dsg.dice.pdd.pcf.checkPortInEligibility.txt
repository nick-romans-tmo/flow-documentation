@startuml
title PortComponent.CheckPortInEligibility Service \nThis service will be used by Rebellion UI and SAP Care UI to check the Port Validations.


actor Client

group <font color=#FF0090><b> checkPortInEligibility
Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/U2-NationalRelease/asciidoc/Uprising/checkPortInEligibility.html#CheckPortInEligibility checkPortInEligibilityRequest]]
TIBCO -> TIBCO : Validate Headers[senderId, channelId and applicationId are required]
TIBCO -> TIBCO : checkPortInEligibilityRequest to WLNPRequest
TIBCO->SAMSON : Call CheckPortability
SAMSON->SAMSON: [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/210/services/evChkPortIn.html evChkPortIn]]
SAMSON->TIBCO: Success Response

TIBCO -> TIBCO : WLNPRequest to checkPortInEligibilityRespone
TIBCO->Client: checkPortInEligibilityRespone

group <font color=#FF8C00><b>  Error Response
SAMSON->TIBCO: error
TIBCO->Client: Error Response
end
end group
@enduml