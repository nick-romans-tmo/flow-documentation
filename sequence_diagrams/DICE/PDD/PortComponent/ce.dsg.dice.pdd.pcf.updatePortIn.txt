@startuml
title PortComponent.UpdatePortIn Service \nThis service will be used by Ericsson to update port in status of a subscriber.

actor Ericsson

group <font color=#FF0090><b> UpdatePortIn
Ericsson->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/U2-NationalRelease/asciidoc/Uprising/updatePortIn.html UpdatePortInRequest]] <font color=#FF0090>

TIBCO -> TIBCO : Validate Headers[senderId, channelId and applicationId are required]
TIBCO -> TIBCO : updatePortInRequest to csiUpdateAutomatedPortInRequest
TIBCO->SAMSON : Call InvokeUpdateAutomatedPortInOrch

group <b> InvokeUpdateAutomatedPortInOrch

group <b> GetLogicalDate
		TIBCO -> SAMSON: Call QueryLogicalDateTux
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/210/services/gnGtLgclDt.html gnGtLgclDt]]
		SAMSON -> TIBCO : Logical Date
		group <b> if SAMSON Error
		TIBCO -> SQLMGR : Call getLogicalDateSQL
		SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
		SQLMGR -> TIBCO : Logical Date 
		end
	end
TIBCO -> TIBCO : GetLogicalDate

group <b> getPortInDetails
TIBCO -> TIBCO : GetLogicalDate
	
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/210/services/csGtAutoPort.html Invoke csGtAutoPort]]
SAMSON -> TIBCO : GetPortInRequest response		
end

	group <b> IF INTER (csGtAutoportOutput.CURRENT__SERVICE__PROVIDER='NT' and updateActivityType = 'FALLOUTUPDATE')
	group <b> get SP
		TIBCO -> TIBCO : GetLogicalDate
		TIBCO -> SAMSON: applicationID, logialDate, opratorID, REQ_NO
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/210/services/csGtPortRequest.html csGtPortRequest]]
		SAMSON -> TIBCO : serviceProvider response
	end
	
	group <b> getCarrier
		TIBCO -> SAMSON: applicationID, logialDate, opratorID
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/210/services/rfLsCarrInfo.html rfLsCarrInfo]]
		SAMSON -> TIBCO : CarrierInfo
	end
	group <b> CheckPortInDueDateTux
		TIBCO -> SAMSON: CheckPortInDueDate request
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/210/services/csChkDueDate.html csChkDueDate]]
		SAMSON -> TIBCO : DueDate response		
	end
	group <b> Update PortIn
		TIBCO -> TIBCO : GetLogicalDate
		TIBCO -> SAMSON: csiUpdateAutomatedPortInRequest
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/210/services/csUpAutoPort.html csUpAutoPort]]
		SAMSON -> TIBCO : portDueDate response
	end
end
group <b> IF INTRA  (csGtAutoportOutput.CURRENT__SERVICE__PROVIDER!='NT')
	group <b> Update PortIn
		TIBCO -> TIBCO : GetLogicalDate
		TIBCO -> SAMSON: csiUpdateAutomatedPortInRequest
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/210/services/csUpAutoPort.html csUpAutoPort]]
		SAMSON -> TIBCO : portDueDate response
	end
	end
end

TIBCO -> TIBCO : csiUpdateAutomatedPortInResponse to updatePortInResponse
TIBCO->Ericsson: updatePortInResponse

group <font color=#FF8C00><b>  Error Response
SAMSON->TIBCO: Tuxedo error
SQLMGR->TIBCO: SQL error
TIBCO->Ericsson: updatePortInResponse
end
end group
@enduml