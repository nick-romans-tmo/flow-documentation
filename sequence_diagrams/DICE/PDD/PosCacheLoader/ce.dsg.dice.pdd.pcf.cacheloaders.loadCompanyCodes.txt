@startuml
title LoadCompanyCodes Service \n As part of cache updates, this service will be invoked daily once by scheduler job(posLoadSapInfoTimer) through uri: "/pos/cachemanagement/companycodes.sync/v1" and used to push Tender details to GemFire CaaS after pulling the Company details from POS DB.

actor POSLoadSapInfoTimer

group [[https://servicecatalog.devcenter.t-mobile.com/servicecat/tibco/U2-NationalRelease/asciidoc/Uprising/posLoadSapInfoTimer.html scheduleRequest]] <font color=#FF0090>
	Scheduler->TIBCO: schedulerRequest
	
	group <b> getAllCompanyCodesInfoJDBC
	database POSDB
	TIBCO -> POSDB : stored procedure db call "REBELLION_STORE_OPERATIONS.GETALLCMPYCODESINFO" to retreive Company details 
	TIBCO -> TIBCO : Company details
	end
	
	TIBCO -> TIBCO : Transform For AS 
	 
	group <b> EnterpriseCacheDats
	TIBCO -> GemFire : tender details in CaaSRequest(connectionId:REBELLION, SpaceName:COMPANYCODESDETAILS) as EnterpriseCacheData 		
	end
	
	group <b> DeleteOldRecords
	TIBCO -> GemFire : Delete old Company details and maintain delta details through to CaaSRequest(connectionId:REBELLION, SpaceName:COMPANYCODESDETAILS, date:timestamp-FlushoutThresholdTimeInMillis)	
	end

group <font color=#FF8C00><b>  Error Response
GemFire->TIBCO: GemFire CaaS Errors
POSDB->TIBCO: POSDB Errors
TIBCO->TIBCO: TIBCO Errors
TIBCO->POSLoadSapInfoTimer: Error details
end

end group
@enduml