@startuml
title LoadStore Service \n As part of cache updates, this service will be invoked once by a scheduler job(posLoadSapInfoTimer) on uri: "/pos/cachemanagement/store.sync/v1" and used to push store details to GemFire CaaS after pulling the store details from POS DB.

actor POSLoadSapInfoTimer

group [[https://servicecatalog.devcenter.t-mobile.com/servicecat/tibco/U2-NationalRelease/asciidoc/Uprising/posLoadSapInfoTimer.html scheduleRequest]] <font color=#FF0090>
	Scheduler->TIBCO: schedulerRequest
	
	group <b> StoreInfo
	database POSDB
	TIBCO -> POSDB : stored procedure db call "REBELLION_STORE_OPERATIONS.GETALLSTORESINFO" to retreive store details 
	TIBCO -> TIBCO : store details		
	end
	
	group <b> StoreCapabilities
	TIBCO -> POSDB : stored procedure db call "REBELLION_STORE_OPERATIONS.GET_STORE_CAPABILITIES" to retreive store capabilities 
	TIBCO -> TIBCO : store capabilities		
	end

	TIBCO -> TIBCO : Transform For AS(Store details + store capabilities) 
	 
	group <b> EnterpriseCacheDats
	TIBCO -> GemFire : store details in CaaSRequest(connectionId:REBELLION, SpaceName:STOREDETAILS) as EnterpriseCacheData 		
	end
	
	group <b> DeleteOldRecords
	TIBCO -> GemFire : Delete old store details and maintain delta store details through to CaaSRequest(connectionId:REBELLION, SpaceName:STOREDETAILS, date:timestamp-FlushoutThresholdTimeInMillis)	
	end


group <font color=#FF8C00><b>  Error Response
GemFire->TIBCO: GemFire CaaS Errors
POSDB->TIBCO: POSDB Errors
TIBCO->TIBCO: TIBCO Errors
TIBCO->POSLoadSapInfoTimer: Error details
end

end group
@enduml