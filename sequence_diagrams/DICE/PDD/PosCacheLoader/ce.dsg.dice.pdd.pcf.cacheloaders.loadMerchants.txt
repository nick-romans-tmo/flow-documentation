@startuml
title LoadMerchants Service \n As part of cache updates, this service will be invoked daily once by a scheduler job(posLoadSapInfoTimer) through uri: "/pos/cachemanagement/merchants.sync/v1" and used to push merchant details to GemFire CaaS after pulling the Merchant data from POS DB.

actor POSLoadSapInfoTimer

group [[https://servicecatalog.devcenter.t-mobile.com/servicecat/tibco/U2-NationalRelease/asciidoc/Uprising/posLoadSapInfoTimer.html scheduleRequest]] <font color=#FF0090>
	Scheduler->TIBCO: schedulerRequest
	
	group <b> getAllMerchantsInfoJDBC
	database POSDB
	TIBCO -> POSDB : stored procedure db call "REBELLION_STORE_OPERATIONS.GETALLMERCHANTSINFO" to retreive merchant details 
	TIBCO -> TIBCO : merchant details		
	end
	
	TIBCO -> TIBCO : Transform For AS 
	 
	group <b> EnterpriseCacheDats
	TIBCO -> GemFire : merchant details in CaaSRequest(connectionId:REBELLION, SpaceName:MERCHANTSDETAILS) as EnterpriseCacheData 		
	end
	
	group <b> DeleteOldRecords
	TIBCO -> GemFire : Delete old merchant details and maintain delta details through to CaaSRequest(connectionId:REBELLION, SpaceName:MERCHANTSDETAILS, date:timestamp-FlushoutThresholdTimeInMillis)	
	end

group <font color=#FF8C00><b>  Error Response
GemFire->TIBCO: GemFire CaaS Errors
POSDB->TIBCO: POSDB Errors
TIBCO->TIBCO: TIBCO Errors
TIBCO->POSLoadSapInfoTimer: Error details
end

end group
@enduml