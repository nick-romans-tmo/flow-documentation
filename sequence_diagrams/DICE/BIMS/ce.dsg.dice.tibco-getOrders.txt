@startuml
title OrderEvent.getOrders
actor Client

group <font color=#FF0090><b> getOrders
Client->TIBCO: GetOrdersRequest[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-07-21/sid/html/OrderEvent.html#getOrders getOrdersRequest]] 
group <b> If msisdn exist
TIBCO->SAMSON  : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/csSrchSub.html csSrchSub]]  GetAccountsByMSISDNReq 
SAMSON->SAMSON: Tuxedo_csSrchSub
SAMSON->TIBCO: csiGetAccountsByMSISDNResponse
end
TIBCO->TIBCO: ValidateRequest
group <b> Validation is SUCCESS
group <b> If msisdn and BAN exist and 
TIBCO->IsPAH : Call SelfCareService
IsPAH->IsPAH: SelfCareService.isPAH
IsPAH->SQLMGR :isPAHRespose 
SQLMGR->SQLMGR : GetOrders
end
group <b> else
TIBCO->SQLMGR: Call GetOrders
SQLMGR->SQLMGR: GetOrders
end
group <b> If sapOrderNumber exist
TIBCO->SQLMGR: Call Event Collection
SQLMGR->SQLMGR: InsertViewedEvents
SQLMGR->TIBCO: Status
end
TIBCO->TIBCO : GetOrderDetailsResponse
end
group <b> else
TIBCO->TIBCO: Error[GenerateValidationError]
end
TIBCO->Client: GetOrderDetailsResponse
end
group <b> Error
TIBCO->Client: error respose
IsPAH->TIBCO: error
SQLMGR->TIBCO: error
end group
@enduml