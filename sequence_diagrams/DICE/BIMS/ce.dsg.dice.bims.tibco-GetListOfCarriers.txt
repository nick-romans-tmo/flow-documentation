@startuml
title PortableAssetService.GetListOfCarriers Service
actor Client

group <font color=#FF0090><b> GetListOfCarriersRequest
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/sid/html/PortableAsset.html GetListOfCarriersRequest]]

TIBCO -> TIBCO :GetListOfCarriersRequest to csiGetListOfCarriersRequest
TIBCO -> TIBCO :SamsonMisc "/SAMSON/WLNP/CARRIERINFO/GET/"
TIBCO -> SAMSON : Call QueryLogicalDateTux
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/gnGtLgclDt.html gnGtLgclDt]]
SAMSON -> TIBCO : Logical Date
group <b> if SAMSON Error
TIBCO -> SQLMGR : Call getLogicalDateSQL
SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
SQLMGR -> TIBCO : Logical Date 
end
TIBCO -> SAMSON : Call rfLsCarrInfo Tux
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/rfLsCarrInfo.html rfLsCarrInfo]]

group <b> if Wholesale Rest Call is enabled
TIBCO -> Wholesale : http endpoint "http://proesp-whl.unix.gsm1900.org:8503/WHOLESALE/WLNP/CARRIERINFO/GET"
end

group <b> otherwise
TIBCO -> Wholesale : EMS Server "TMOBILE.PROD_STG.WHOLESALE.WLNP.CARRIERINFO.GET"
end

TIBCO -> TIBCO : csiGetListOfCarriersRequest to GetListOfCarriersResponse
TIBCO->Client: GetListOfCarriersResponse

group <font color=#FF8C00><b>  Error Response
SQLMGR -> TIBCO :error
SAMSON -> TIBCO :error
TIBCO -> Client :error response
end
end group
@enduml