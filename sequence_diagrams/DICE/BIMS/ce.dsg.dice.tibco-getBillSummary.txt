@startuml
title BillService.GetBillSummary Service
actor Client

group <font color=#FF0090><b> getBillSummary     
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-06-09/sid/html/BillService.html#getBillSummary esiGetBillSummaryRequest]]

TIBCO -> TIBCO : Validate accountNumber
TIBCO->TIBCO: esiGetBillSummaryRequest to csiGetBillSummaryRequest

TIBCO -> SAMSON: Call QueryLogicalDateTux
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/gnGtLgclDt.html gnGtLgclDt]]
SAMSON -> TIBCO : Logical Date
group <b> if SAMSON Error
TIBCO -> SQLMGR : Call getLogicalDateSQL
SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
SQLMGR -> TIBCO : Logical Date 
end

TIBCO -> SAMSON : GetBillHistoryTux[includes LogicalDate]
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/197/services/evGbBillHist.html evGbBillHist]]
SAMSON -> TIBCO : Samson getBillHist Response

group <b> If BillSeqNo provided | BillHist__List exists
TIBCO -> SAMSON : Call GetAdjustmentHistoryTux[includes LogicalDate]
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/197/services/evGbAdjHis.html evGbAdjHis]]
SAMSON -> TIBCO : Samson getAdjustmentHistory Response
end

TIBCO->TIBCO: csiGetBillSummaryResponse to esiGetBillSummaryResponse
TIBCO-> Client :esiGetBillSummaryResponse

group <font color=#FF8C00><b>  Error Response
SQLMGR -> TIBCO:error
SAMSON -> TIBCO:error
TIBCO -> Client:error response
end
end
@enduml

