@startuml
title AddressLookupService.lookupCityState
actor Client

group <font color=#FF0090><b> lookupCityState
Client->HAProxy_APIX: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/AddressLookupService.html#lookupCityState lookupCityState]] AddressLookupService.lookupCityState
HAProxy_APIX -> HAProxy_APIX :Authenticate and Authorize
HAProxy_APIX->AddressLookupService: lookupCityState REQ
AddressLookupService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/gnMtchZipAdr.html gnMtchZipAdr]]  Tuxedo_gnMtchZipAdr 
SAMSON->AddressLookupService: Samson returns lookupCityState 
AddressLookupService->HAProxy_APIX: Returns service response
HAProxy_APIX->Client: lookupCityState response to client
end group


@enduml

