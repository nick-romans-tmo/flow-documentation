@startuml
title MobileNumber.getCurrentAllocation
actor Client
group <font color=#FF0090><b> getCurrentAllocation
Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-06-09/sid/html/MobileNumber.html#GetCurrentAllocation esiGetCurrentAllocationRequest]]
TIBCO -> TIBCO : ValidateRequest[ApplicationID,OperatorID,MSISDN mandatory]
TIBCO -> TIBCO : esiGetCurrentAllocationRequest to csiGetCurrentAllocationRequest
group <b> Logical Date
	TIBCO->SAMSON: CallLogicalDate
	SAMSON->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/204/services/gnGtLgclDt.html gnGtLgclDt]]
	SAMSON->TIBCO: LogicalDate
	group <b> SAMSON Error
		TIBCO->SQLMGR: Call GetLogicalDateSQL
		SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
		SQLMGR->TIBCO: LogicalDate 
	end
end
TIBCO -> SAMSON : Call GetCurrentAllocationTux
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/204/services/evGtSubInfo.html evGtSubInfo]]
SAMSON -> TIBCO : Samson response
TIBCO -> TIBCO : csiGetCurrentAllocationResponse
TIBCO -> Client: esiGetCurrentAllocationResponse

end group
@enduml