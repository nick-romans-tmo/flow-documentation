@startuml
title PortInService.NotifyPortActivity Service
actor Client

group <font color=#FF0090><b> notifyPortActivity     
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-06-09/sid/html/PortInService.html#notifyPortActivity notifyPortActivityRequest]] 

group <b> If MetroFlow
TIBCO -> AAM :portOutDeactivationRequest
AAM -> AAM : ChangeSubStsServicesService.portOutDeactivation
AAM -> TIBCO : portOutDeactivationResponse
end

group <b> If ACC Flow
	group <b> If ProvisioningStatus
	TIBCO -> ACC : ProvisioningStatus
	ACC -> ACC : acc.ProvisioningStatus
	ACC -> TIBCO :ProvisioningStatusResponse
	end
	group <b> If PortOutDeactivation
	TIBCO -> TIBCO : GetLogicalDate
	TIBCO -> SAMSON: [operatorID, applicationID, logicalDate]
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/197/services/rfLsCarrInfo.html rfLsCarrInfo]]
	SAMSON -> TIBCO : CarrierInfo
	
	TIBCO ->SQLMGR: GetWholesalePartnerDetails
	SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetWholesalePartnerDetails&action=query&keyValue= GetWholesalePartnerDetails]]
	SQLMGR -> TIBCO : partnerDetails
	end

end

group <b> If TargetEF
	group <b> If BE EF
	TIBCO -> TIBCO_BE :eventDetails
	end
	group <b> Otherwise ESOA EF
	TIBCO -> ESOA :ESPEvent
	ESOA -> ESOA : PublishEventService.postEvent
	ESOA -> TIBCO : ESPEventResponse
	end
end
TIBCO -> TIBCO : targetResponse to notifyPortActivityResponse

TIBCO -> Client : notifyPortActivityResponse

end
@enduml