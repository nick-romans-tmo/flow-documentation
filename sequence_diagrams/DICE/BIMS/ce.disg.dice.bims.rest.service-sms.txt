@startuml
title <size:15>__Service SMS__</size>
actor Client
participant APIGEE
participant ServiceSMS_API
participant RabbitMQ
participant SMSEndPoint

group Sync Call
Client-> APIGEE: call (/sms/sendsms)
APIGEE-> APIGEE: validation
APIGEE-> ServiceSMS_API: Call sendsms (/sms/sendsms)
ServiceSMS_API-> ServiceSMS_API: validation
group IF sdg.sms.isenabled is TRUE 
ServiceSMS_API->SMSEndPoint: Invoke the client configured for sdg.sms.endpoint
SMSEndPoint->ServiceSMS_API: Return
ServiceSMS_API->APIGEE: Return
APIGEE->Client: Success
end

group IF sdg.sms.isenabled is FALSE 
ServiceSMS_API->SMSEndPoint: Invoke the client configured for inform.sms.endpoint
SMSEndPoint->ServiceSMS_API: Return
ServiceSMS_API->APIGEE: Return
APIGEE->Client: Success
end
end

group Async call
Client-> APIGEE: call (/sendsms/async)
APIGEE-> APIGEE: validation
APIGEE-> ServiceSMS_API: Call sendSMSAsyncREST (/sendsms/async)
ServiceSMS_API->RabbitMQ: Publishing the message to SERVICE.SMS.ASYNC.RECEIVE
end

group Asynchronous Listener
ServiceSMS_API->ServiceSMS_API: sendSMSAsyncMessage
ServiceSMS_API-> RabbitMQ: Listens the messages from SERVICE.SMS.ASYNC.RECEIVE
group IF sdg.sms.isenabled is TRUE 
ServiceSMS_API->SMSEndPoint: Invoke the client configured for sdg.sms.endpoint
SMSEndPoint->ServiceSMS_API: Return
end

group IF sdg.sms.isenabled is FALSE 
ServiceSMS_API->SMSEndPoint: Invoke the client configured for inform.sms.endpoint
SMSEndPoint->ServiceSMS_API: Return
end
end
@enduml