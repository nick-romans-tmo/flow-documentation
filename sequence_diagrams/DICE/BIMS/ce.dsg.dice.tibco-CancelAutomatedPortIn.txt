@startuml
title PortableAssetService.CancelAutomatedPortIn Service
actor Client

group <font color=#FF0090><b> CancelAutomatedPortInRequest
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/sid/html/PortableAsset.html CancelAutomatedPortInRequest]]

TIBCO -> TIBCO :CancelAutomatedPortInRequest to csiCancelAutomatedPortInRequest
TIBCO -> TIBCO :SamsonMisc "/SAMSON/WLNP/AUTOMATEDPORTIN/CANCEL/"

TIBCO -> SAMSON : Call QueryLogicalDateTux
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/gnGtLgclDt.html gnGtLgclDt]]
SAMSON -> TIBCO : Logical Date
group <b> if SAMSON Error
TIBCO -> SQLMGR : Call getLogicalDateSQL
SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
SQLMGR -> TIBCO : Logical Date 
end
TIBCO -> SAMSON : Call csUpAutoPort Tux
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csUpAutoPort.html csUpAutoPort]]

TIBCO -> TIBCO : csiCancelAutomatedPortInRequest to CancelAutomatedPortInResponse
TIBCO->Client: CancelAutomatedPortInResponse

group <font color=#FF8C00><b>  Error Response
SQLMGR -> TIBCO :error
SAMSON -> TIBCO :error
TIBCO -> Client :error response
end
end group
@enduml