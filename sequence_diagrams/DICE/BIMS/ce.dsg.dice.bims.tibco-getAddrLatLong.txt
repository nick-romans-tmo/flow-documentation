@startuml
title Address.getAddrLatLong Service
actor Client

group <font color=#FF0090><b> getAddrLatLong 
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/sid/html/Address.html#GetAddrLatLong esiGetAddrLatLongRequest]] 

TIBCO -> TIBCO :Input Validation[ApplicationID,OperatorID, ClientID are mandatory]
TIBCO -> TIBCO : esiGetAddrLatLongRequest to csiAddrLatLongRequest

TIBCO -> SAMSON: Call QueryLogicalDateTux
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/208/services/gnGtLgclDt.html gnGtLgclDt]]
SAMSON -> TIBCO : Logical Date
group <b> if SAMSON Error
TIBCO -> SQLMGR : Call getLogicalDateSQL
SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
SQLMGR -> TIBCO : Logical Date 
end
TIBCO ->SAMSON : gnMtchZipAdr
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/208/services/gnGtLatLong.html gnGtLatLong]]
TIBCO ->SAMSON : rfGtDynDesc
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/208/services/rfGtDynDesc.html rfGtDynDesc]]

TIBCO -> TIBCO : csiAddrLatLongRequest to esiGetAddrLatLongRequest
TIBCO -> Client : esiGetAddrLatLongRequest

group <font color=#FF8C00><b>  Error Response
SQLMGR -> TIBCO :error
SAMSON -> TIBCO :error
TIBCO -> Client :error response
end
end
@enduml