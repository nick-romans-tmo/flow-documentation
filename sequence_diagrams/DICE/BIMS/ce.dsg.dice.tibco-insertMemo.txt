@startuml
title PublicInsertMemo.InsertMemo
actor Client
group <font color=#FF0090><b> InsertMemo
Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-06-09/sid/html/PublicInsertMemo.html#InsertMemo InsertMemo]]InsertMemo

group <b> If ApplicationID = METRO and MemoType = UNLK OR MemoType = RDAT
	TIBCO -> TIBCO : csiCreateMemoRequest
	TIBCO -> METRO : CreateMemoJMSQueueSender[METRO.MEMO.CREATE]
	TIBCO -> TIBCO : csiCreateMemoResponse
end
group <b> If ApplicationID = BSP OR 2IP OR UVM and Call_CreateMemo = true
	TIBCO -> TIBCO : csiCreateMemoRequest
	TIBCO -> ACC : InsertMemoJMSQueueSender[ACC.MEMO.INSERT]
	TIBCO -> TIBCO : csiCreateMemoResponse
end
group <b> If Otherwise
	group <b> If BAN exists
	TIBCO -> TIBCO : csiCreateMemoRequest
	TIBCO -> TIBCO : Call SamsonCreateMemoABP
	TIBCO -> TIBCO : MessageID
	end
	group Otherwise
		TIBCO -> TIBCO : Call GetSubscriberDetails
		TIBCO -> TIBCO : ListofSubscriber
		TIBCO -> TIBCO : Call SamsonCreateMemoABP
	end
end

group <b> If GetSubscriberDetails
	TIBCO -> SQLMGR : Call GetSubscriberDetails[SubscriberNo]
	SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetSubscriberDetails_Dual&action=query&keyValue= GetSubscriberDetails]]
	SQLMGR -> TIBCO : SubjectName
end
group <b> SamsonCreateMemoABP
	TIBCO->TIBCO : Validate [OperatorID AND AppicationID required]
	TIBCO->TIBCO : Call GetLogicalDate
	TIBCO->SAMSON : MemoRequest
	SAMSON->SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/arGtCstBan.html arGtCstBan]]
	SAMSON->TIBCO : Samson Response
	group <b> if MEMO_ID Exists
		TIBCO->SAMSON : MemoRequest & GetBanDetails
		SAMSON->SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/mmUpMemo.html mmUpMemo]]
		SAMSON->TIBCO : Samson Response
	end
	group <b> if insert
		TIBCO->SAMSON : MemoRequest
		SAMSON->SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/mmUpMemo.html mmInMemo]]
		SAMSON->TIBCO : Samson Response
	end
end
group <b> GetLogicalDate
	TIBCO -> SAMSON: Call QueryLogicalDateTux
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/gnGtLgclDt.html gnGtLgclDt]]
	SAMSON -> TIBCO : Logical Date
	group <b> if SAMSON Error
	TIBCO -> SQLMGR : Call getLogicalDateSQL
	SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
	SQLMGR -> TIBCO : Logical Date 
	end
end
	TIBCO->TIBCO : csiCreateMemoResponse
	TIBCO->Client : MemoResponse
end
group <font color=#FF8C00><b>  Error Response
SQLMGR -> TIBCO :error
TIBCO -> Client :error response
end
@enduml
