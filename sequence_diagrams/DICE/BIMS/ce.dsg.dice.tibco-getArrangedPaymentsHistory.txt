@startuml
title Bill.GetArrangedPaymentsHistory Service
actor Client

group <font color=#FF0090><b> getArrangedPaymentsHistory
Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-07-21/sid/html/Bill.html#GetArrangedPaymentsHistory getArrangedPaymentsHistoryRequest]]
TIBCO -> TIBCO :GetArrangedPaymentsHistoryRequest 
TIBCO -> TIBCO :ValidateData[Valid BAN]
TIBCO -> TIBCO:GetArrangedPaymentsHistoryRequest to csiGetArrangedPaymentsHistoryRequest
group <b> Logical Date
TIBCO->SAMSON: CallLogicalDate
SAMSON->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/gnGtLgclDt.html gnGtLgclDt]]
SAMSON->TIBCO: LogicalDate
group <b> SAMSON Error
TIBCO->SQLMGR: Call GetLogicalDateSQL
SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
SQLMGR->TIBCO: LogicalDate
end
end
TIBCO-> SAMSON: Call GetArrangedPaymentsHistoryTux
SAMSON->SAMSON: [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/evGbSchPymHst.html evGbSchPymHst]]
SAMSON->SAMSON: [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/evGbPaPymHst.html evGbPaPymHst]]
SAMSON->TIBCO: Response
TIBCO->Client: csiGetArrangedPaymentsHistoryResponse
group <font color=#FF8C00><b>  Error Response
SAMSON->TIBCO: error
SQLMGR->TIBCO: error
TIBCO->Client: Error Response
end
end group
@enduml
