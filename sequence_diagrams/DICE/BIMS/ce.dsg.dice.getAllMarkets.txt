@startuml
title NumberManagementService.getAllMarkets
actor Client

group <font color=#FF0090><b> getAllMarkets
Client->HAProxy_APIX: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/NumberManagementService.html#getAllMarkets getAllMarkets]]  NumberManagementService.getAllMarkets
HAProxy_APIX -> HAProxy_APIX :Authenticate and Authorize
HAProxy_APIX->NumberManagementService: getAllMarkets REQ
NumberManagementService->NumberManagementService: Validate REQ
NumberManagementService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evGbAllMkts.html evGbAllMkts]]  Tuxedo_evGbAllMkts 
SAMSON->NumberManagementService: Samson returns getAllMarkets details 
NumberManagementService->HAProxy_APIX: Returns service response
HAProxy_APIX->Client: getAllMarkets response to client
end group

@enduml

