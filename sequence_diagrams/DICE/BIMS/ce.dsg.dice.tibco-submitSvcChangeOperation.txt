@startuml
title SubmitSvcChange.SubmitSvcChangeOperation Service
actor Client

group <font color=#FF0090><b> SubmitSvcChangeOperation     
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-06-09/sid/html/SubmitSvcChange.html#SubmitSvcChangeOperation esiSubmitServiceChangeRequest]] 
TIBCO->TIBCO : esiSubmitServiceChangeRequest to csiSubmitServiceChangeRequest	
group <b> if Enable_SIMOnly_Eligibilty='Y' AND ban exists AND msisdn !='0000000000'
	TIBCO -> TIBCO : Call getEligiblityOrch
	TIBCO -> TIBCO : Call SubmitServiceChangeProxy
end
group <b> Otherwise
	group <b> if servicePartnerId is METRO
		TIBCO -> ESOA : changePlanAndServicesRequest
		ESOA -> TIBCO : changePlanAndServicesResponse
	end
	group <b> Otherwise
		TIBCO -> TIBCO : Call SubmitServiceChangeProxy
	end
end
group <b> getEligiblityOrch
	group <b> BAN not exists
		TIBCO -> TIBCO : Validate Request[SSN,Application ID and OperatorID are required]
		TIBCO -> TIBCO : Call GetLogicalDate
		TIBCO -> SAMSON : csiGetAccountsByMSISDNRequest
		SAMSON->SAMSON  : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csSrchSub.html csSrchSub]]
		SAMSON->TIBCO : Response
		TIBCO -> TIBCO : csiGetAccountsBySSNResponse
	end
		TIBCO -> TIBCO : csiGetbasicAccountInfoRequest
			group <b> if subscriberOnly is TRUE
				TIBCO -> TIBCO : Call GetBasicAccountInfoForSubTux
			end
			group <b> if Otherwise
				group <b> if CustomerInfoIndicator is true
					TIBCO -> TIBCO : Call GetBasicAccountInfoForSubTux
				end
				TIBCO -> TIBCO : Call GetBasicAccountInfoForBANTux
			end
		TIBCO -> TIBCO : csiGetHandsetUpgradeEligibilityRequest
		TIBCO -> HSO : GetOrganicChannelEligibility
		HSO -> TIBCO : GetOrganicChannelEligibilityResult
		TIBCO -> TIBCO : GetOrganicChannelEligibilityResponse
end
group <b> SubmitServiceChangeProxy
	TIBCO -> TIBCO : csiSubmitServiceChangeRequest
	group <b> if servicePassChange exists
		TIBCO -> TIBCO : Call GetBasicAccountInfoForBANTux
		TIBCO -> SAMSON : saveServiceChangeRequest
		SAMSON->SAMSON  : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/evSvSrvAgr.html evSvSrvAgr]]
		SAMSON->TIBCO : Response
		TIBCO -> TIBCO : saveServiceChangeResponse
		group <b> if promotionalPassPurchaseDetails exists
			TIBCO -> SAMSON :  csiSubmitServiceChangeRequest
			SAMSON->SAMSON  : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/evUpODAPromo.html evUpODAPromo]]
			SAMSON->TIBCO : Response
			TIBCO -> TIBCO : csiSubmitServiceChangeResponse
		end
	end
	group <b> Otherwise
		group <b> if MultiSubscribers
			TIBCO -> TIBCO : Call SamsonSubmitServiceChangeOrch
		end
		group <b> Otherwise
			TIBCO -> TIBCO : Call SamsonSubmitServiceChangeOrch
		end
	end
end
group <b> SubmitServiceChangeESOAProxy
	TIBCO -> ESOA : csiSubmitServiceChangeRequest
	ESOA -> TIBCO : csiSubmitServiceChangeResponse
end
group <b> GetBasicAccountInfoForSubTux
	TIBCO -> TIBCO : Call GetLogicalDate
	TIBCO-> SAMSON : csiGetbasicAccountInfoRequest
	SAMSON->SAMSON  : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/evGtSubSum.html evGtSubSum]]
	SAMSON->TIBCO : Response
	TIBCO -> TIBCO : csiGetbasicAccountInfoResponse
end
group <b> GetBasicAccountInfoForBANTux
	TIBCO -> TIBCO : Call GetLogicalDate
	TIBCO-> SAMSON : csiGetbasicAccountInfoRequest
	SAMSON->SAMSON  : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/evGtCustInfo.html evGtCustInfo]]
	SAMSON->TIBCO : Response
	TIBCO -> TIBCO : csiGetbasicAccountInfoResponse
end

group <b> SamsonSubmitServiceChangeOrch
	TIBCO -> TIBCO : Validate Input [ApplicationID, OperatorID, MSISDN/BAN, plan/service code]
	TIBCO -> TIBCO : Validate Input [Only one operation is allowed add or remove for code]
	TIBCO -> TIBCO : Call GetLogicalDate
	group <b> If BAN not exists and MSISDN exists
		TIBCO -> SQLMGR : Call GetSubscriberDetails[SubscriberNo]
		SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetSubscriberDetails_Dual&action=query&keyValue= GetSubscriberDetails]]
		SQLMGR -> TIBCO : Subscriber
	end
	TIBCO -> TIBCO : Call GetLogicalDate
	TIBCO -> TIBCO : GetBanDetailsRequest
	group <b> If  version >= 3
		TIBCO -> SAMSON : GetBanDetailsRequest
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/arGtCstBanExt.html arGtCstBanExt]]
		SAMSON -> TIBCO :  Response
	end
	group <b> Otherwise
		TIBCO -> SAMSON : GetBanDetailsRequest
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/arGtCstBan.html arGtCstBan]]
		SAMSON -> TIBCO :  Response
	end
	TIBCO -> TIBCO : GetBanDetailsResponse
	TIBCO -> TIBCO : Call GetLogicalDate
	TIBCO -> SAMSON : listOfSubscribers
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evLsSubSrvSum.html evLsSubSrvSum]]
	SAMSON -> TIBCO :  ListOfServiceAgreement
	TIBCO -> TIBCO : Call ValidateSubmitServiceChangeRequest
	group <b> If No Conflict Errors
		TIBCO -> TIBCO : Call SubmitServiceChangeRequest
	end
	group <b> If Event.Enable_AAE is Y
		TIBCO -> AAE  : csiCreateEventRequest[EVENT.ACCOUNTACTIVITY.CREATE]
		AAE ->  TIBCO :	csiCreateEventResponse
	end
end
group <font color=#FF0090><b> ValidateSubmitServiceChangeRequest 
		group <b> Validate?
					group <b> QueryPlanDetails?
							TIBCO -> SQLMGR : Call GetPlanDetailsSQL
							SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetPlanDetails&action=query&keyValue= GetPlanDetails]]
							SQLMGR -> TIBCO : Soc
					end
					group <b> SubLevelRemoval
						group <b> isOperationalCheck
							SQLMGR -> TIBCO : Call QueryAccountDetails	
						end 
						group <b> Remove?
							TIBCO -> TIBCO : GetLogicalDate
							TIBCO -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/rfGtSocInd.html rfGtSocInd]]
							SAMSON -> TIBCO :  Response
						end 
						group <b> Revised?
							TIBCO -> TIBCO : Call ValidateSalesService
						end 
						group <b> Otherwise
							TIBCO -> TIBCO : Call QueryForSale
						end 
							TIBCO -> TIBCO : Call RetriveFromCacheForAddSoc
						group <b> Otherwise	
							TIBCO -> TIBCO : GetLogicalDate
							TIBCO -> SAMSON : csiGetSocParameterRequest
							SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evGtAddPrm.html evGtAddPrm]]
							SAMSON -> TIBCO :  csiGetSocParameterResponse
						end
					end
					group <b> AddSoc?
					 group <b> getBANDetails
							SQLMGR -> TIBCO : Call QueryAccountDetails	
							TIBCO -> SAMSON : GetBanDetailsRequest
							SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/arGtCstBan.html arGtCstBan]]
							SAMSON -> TIBCO :  Response
					 end
					 group <b> PCCheck?
							TIBCO -> RatePlanAndDevice :  GetCompatibleServicesForRatePlanAndDevice
							RatePlanAndDevice -> TIBCO : GetCompatibleServicesForRatePlanAndDeviceResponse
							TIBCO -> Web2GoDevice :  GetWeb2GoDeviceServiceCompatibility
							Web2GoDevice -> TIBCO :  GetWeb2GoDeviceServiceCompatibilityResponse
					 end
					 group <b> Revised?
						TIBCO -> TIBCO : Call ValidateSalesService
					 end
					  group <b> Otherwise
						TIBCO -> TIBCO : Call QueryForSale
					 end
					  group <b> incompatibleCheck
						TIBCO -> TIBCO : Call SOCConflicts
					 end
					 end
					 group <b> PP?
							SQLMGR -> TIBCO : Call QueryAccountDetails	
							TIBCO -> SAMSON : GetBanDetailsRequest
							SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/arGtCstBan.html arGtCstBan]]
							SAMSON -> TIBCO :  Response
							TIBCO -> TIBCO : GetLogicalDate
							group <b> PCCheck?
								TIBCO -> RatePlanAndDevice :  GetCompatibleServicesForRatePlanAndDevice
								RatePlanAndDevice -> TIBCO : GetCompatibleServicesForRatePlanAndDeviceResponse
								TIBCO -> Web2GoDevice :  GetWeb2GoDeviceServiceCompatibility
								Web2GoDevice -> TIBCO :  GetWeb2GoDeviceServiceCompatibilityResponse
							end
							TIBCO -> SQLMGR : Call GetPricePlanForSale
							SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetPricePlanForSale&action=query&keyValue= GetPricePlanForSale]]
							SQLMGR -> TIBCO : ListOfSoc
							TIBCO -> TIBCO : Call SOCConflicts
					end
						group <b> ExpiredSocs?
						TIBCO -> TIBCO : Call GetLogicalDate
							group <b> isPromo
							TIBCO -> SQLMGR : Call GetReducedSocRelationShipSQL
							SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetReducedSocRelationShip&action=query&keyValue= GetReducedSocRelationShip]]
							SQLMGR -> TIBCO : SocRelation
							end 
						group <b> PromoEligibilityCheck
							TIBCO -> SAMSON : ListOfServiceAgreement
							SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evChkPromoElg.html evChkPromoElg]]
							SAMSON -> TIBCO :  PromoSoc
						end
					end
	end
	group <b> CheckConflicts?
			group <b> isBANLevel
				TIBCO -> TIBCO : Call SOCConflicts
			end
			group <b> isSublevel
				TIBCO -> SQLMGR : Call GetSubscriberSubMarketSQL
				SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetSubscriberSubMarket&action=query&keyValue== GetSubscriberSubMarketSQL]]
				SQLMGR -> TIBCO : SubMarketCode
				TIBCO -> TIBCO : Call SOCConflicts
			end
	end
	group <b> RemoveConflictsSocs
		group <b> SA
			TIBCO -> TIBCO : GetLogicalDate
			TIBCO -> SQLMGR : Call GetServiceAgreementSQL
			SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetServiceAgreement_Dual&action=query&keyValue= GetServiceAgreement]]
			SQLMGR -> TIBCO : ListOfServiceAgreement
			group <b> Pooling
				TIBCO -> SQLMGR : Call GetServiceAgreementSQL
				SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetServiceAgreement_Dual&action=query&keyValue= GetServiceAgreement]]
				SQLMGR -> TIBCO : ListOfServiceAgreement
			end
		end
		group <b> PP?
			group <b> SubChng
							TIBCO -> TIBCO : Call GetLogicalDate
							group <b> isPromo
							TIBCO -> SQLMGR : Call GetReducedSocRelationShipSQL
							SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetReducedSocRelationShip&action=query&keyValue= GetReducedSocRelationShip]]
							SQLMGR -> TIBCO : SocRelation
							end 
							group <b> PromoEligibilityCheck
								TIBCO -> SAMSON : ListOfServiceAgreement
								SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evChkPromoElg.html evChkPromoElg]]
								SAMSON -> TIBCO :  PromoSoc
							end
							TIBCO -> SAMSON : Call SocFeaturea
							SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/rfLsSocFtrs.html rfLsSocFtrs]]
							SAMSON -> TIBCO : Response
			end
			group <b> Otherwise
				   TIBCO -> SQLMGR : Call GetServiceFeatureSQL
					SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetServiceFeature_Dual&action=query&keyValue= GetServiceFeature]]
					SQLMGR -> TIBCO : ListOfServiceFeature
					group <b> Change PP?
						TIBCO -> SQLMGR : Call GetPricePlanFeatureConflictListSQL
						SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetPricePlanFeatureConflictList&action=query&keyValue= GetPricePlanFeatureConflictList]]
						SQLMGR -> TIBCO  : ListOfSocConflicts
					end
					group <b> Add/Expire?
						TIBCO -> SQLMGR : Call GetRegularFeatureConflictsSQL
						SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetRegularFeatureConflicts&action=query&keyValue= GetRegularFeatureConflicts]]
						SQLMGR -> TIBCO  : ListOfSocConflicts
					end
			end
			
		end
		group <b> AddReg
					TIBCO -> TIBCO : Call GetLogicalDate
					group <b> isPromo
						TIBCO -> SQLMGR : Call GetReducedSocRelationShipSQL
						SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetReducedSocRelationShip&action=query&keyValue= GetReducedSocRelationShip]]
						SQLMGR -> TIBCO : SocRelation
					end 
					group <b> PromoEligibilityCheck
						TIBCO -> SAMSON : ListOfServiceAgreement
						SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evChkPromoElg.html evChkPromoElg]]
						SAMSON -> TIBCO :  PromoSoc
					end
					TIBCO -> SQLMGR : Call GetServiceFeatureSQL
					SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetServiceFeature_Dual&action=query&keyValue= GetServiceFeature]]
					SQLMGR -> TIBCO : ListOfServiceFeature
					TIBCO -> SAMSON : Call SocFeaturea
					SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/rfLsSocFtrs.html rfLsSocFtrs]]
					SAMSON -> TIBCO : Response
					group <b> Change PP?
						TIBCO -> SQLMGR : Call GetPricePlanFeatureConflictListSQL
						SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetPricePlanFeatureConflictList&action=query&keyValue= GetPricePlanFeatureConflictList]]
						SQLMGR -> TIBCO  : ListOfSocConflicts
					end
					group <b> Add/Expire?
						TIBCO -> SQLMGR : Call GetRegularFeatureConflictsSQL
						SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetRegularFeatureConflicts&action=query&keyValue= GetRegularFeatureConflicts]]
						SQLMGR -> TIBCO  : ListOfSocConflicts
					end
		end
		group <b> expire
				TIBCO -> TIBCO : Call GetLogicalDate
					group <b> isPromo
						TIBCO -> SQLMGR : Call GetReducedSocRelationShipSQL
						SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetReducedSocRelationShip&action=query&keyValue= GetReducedSocRelationShip]]
						SQLMGR -> TIBCO : SocRelation
					end 
					group <b> PromoEligibilityCheck
						TIBCO -> SAMSON : ListOfServiceAgreement
						SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evChkPromoElg.html evChkPromoElg]]
						SAMSON -> TIBCO :  PromoSoc
					end
				TIBCO -> SQLMGR : Call GetServiceFeatureSQL
					SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetServiceFeature_Dual&action=query&keyValue= GetServiceFeature]]
					SQLMGR -> TIBCO : ListOfServiceFeature	
					group <b> Change PP?
						TIBCO -> SQLMGR : Call GetPricePlanFeatureConflictListSQL
						SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetPricePlanFeatureConflictList&action=query&keyValue= GetPricePlanFeatureConflictList]]
						SQLMGR -> TIBCO : ListOfSocConflicts
					end
					group <b> Add/Expire?
						TIBCO -> SQLMGR : Call GetRegularFeatureConflictsSQL
						SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetRegularFeatureConflicts&action=query&keyValue= GetRegularFeatureConflicts]]
						SQLMGR -> TIBCO : ListOfSocConflicts
					end
		end
		group <b> Pooling
			TIBCO -> SAMSON : GetBanDetailsRequest
		    SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/arGtCstBan.html arGtCstBan]]
			SAMSON -> TIBCO :  Response
			TIBCO -> SAMSON : GetBanDetailsRequest
			SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/arSvBanSvc.html arSvBanSvc]]
			SAMSON -> TIBCO :  Response
			TIBCO -> SAMSON : GetBanDetailsRequest
			SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csGtSub.html csGtSub]]
			SAMSON -> TIBCO :  Response
		end 
		group <b> Sub
			TIBCO -> SAMSON : GetBanDetailsRequest
		    SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/arGtCstBan.html arGtCstBan]]
			SAMSON -> TIBCO :  Response
			TIBCO -> SAMSON : GetBanDetailsRequest
		    SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csSvSubAgr.html csSvSubAgr]]
			SAMSON -> TIBCO :  Response
			TIBCO -> SAMSON : GetBanDetailsRequest
			SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csGtSub.html csGtSub]]
			SAMSON -> TIBCO :  Response
		end 
	end


group <b> SOCConflicts
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csChkSocElg.html csChkSocElg]]
		SAMSON -> TIBCO :  Response
		TIBCO -> SAMSON : ListOfServiceAgreement
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csLsEliCnf.html csLsEliCnf]]
		SAMSON -> TIBCO :  Response
		TIBCO -> SAMSON : ListOfServiceAgreement
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csChkUittElg.html csChkUittElg]]
		SAMSON -> TIBCO :  Response
end
group <b> QueryAccountDetails
		group <b> if BAN?
			TIBCO -> SQLMGR : Call GetSubscriberDetailsSQL
			SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetSubscriberDetails_Dual&action=query&keyValue= GetSubscriberDetails]]
			SQLMGR -> TIBCO : ListOfSubscribers
		end
		group <b> if LogicalDate
			TIBCO -> TIBCO : Call GetLogicalDate
		end
end

group <b> ValidateSalesService
	TIBCO -> SQLMGR : Call ValidateServicesForSaleSQL
	SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=ValidateServicesForSale&action=query&keyValue= ValidateServicesForSale]]
	SQLMGR -> TIBCO : ListOfSoc
end

group <b> QueryForSale
	TIBCO -> SQLMGR : Call GetSocForSale
	SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetSocForSale&action=query&keyValue= GetSocForSale]]
	SQLMGR -> TIBCO : ListOfSoc
end

group <b> RetriveFromCacheForAddSoc
	TIBCO -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/rfGtSocInd.html rfGtSocInd]]
	SAMSON -> TIBCO :  Response
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/rfGtSocInd.html rfGtSocInd]]
	SAMSON -> TIBCO :  Response
end
end

group <font color=#FF0090><b> SubmitServiceChangeRequest 
group <b> FlexPay?
	group <b> ExpSubLevel
	 TIBCO -> TIBCO : Call SamsonMultiSubscriberServiceChange
	end
	group <b> ExpBANLevel
	 TIBCO -> TIBCO : Call SamsonSubmitServiceChangeHelper
	end
	group <b> PricePlanChange
	 TIBCO -> TIBCO : Call SamsonSubmitServiceChangeHelper
	end
	group <b> AddSubLevel
	 TIBCO -> TIBCO : Call SamsonMultiSubscriberServiceChange
	end
	group <b> AddBanLevel
	 TIBCO -> TIBCO : Call SamsonSubmitServiceChangeHelper
	end
end
group <b> Non-Polled
	TIBCO -> TIBCO : Call SamsonMultiSubscriberServiceChange
end
group <b> Polled AND SOC_LEVEL_CODE=C AND EXPIRATION_DATE exists
	TIBCO -> TIBCO : Call SamsonMultiSubscriberServiceChange
	group <b> if BAN Level
		TIBCO -> TIBCO : SamsonSubmitServiceChangeHelper
	end
	group <b> if AddSubSoc
		TIBCO -> TIBCO : SamsonMultiSubscriberServiceChange
	end
end
group <b> SamsonMultiSubscriberServiceChange
	TIBCO -> TIBCO : Call SamsonSubmitServiceChangeHelper
end

group <b> SamsonSubmitServiceChangeHelper
group <b> SA
			TIBCO -> TIBCO : GetLogicalDate
			TIBCO -> SQLMGR : Call GetServiceAgreementSQL
			SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetServiceAgreement_Dual&action=query&keyValue= GetServiceAgreement]]
			SQLMGR -> TIBCO : ListOfServiceAgreement
			group <b> Pooling
				TIBCO -> SQLMGR : Call GetServiceAgreementSQL
				SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetServiceAgreement_Dual&action=query&keyValue= GetServiceAgreement]]
				SQLMGR -> TIBCO : ListOfServiceAgreement
			end
		end
		group <b> PP?
			group <b> SubChng
							TIBCO -> TIBCO : Call GetLogicalDate
							group <b> isPromo
							TIBCO -> SQLMGR : Call GetReducedSocRelationShipSQL
							SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetReducedSocRelationShip&action=query&keyValue= GetReducedSocRelationShip]]
							SQLMGR -> TIBCO : SocRelation
							end 
							group <b> PromoEligibilityCheck
								TIBCO -> SAMSON : ListOfServiceAgreement
								SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evChkPromoElg.html evChkPromoElg]]
								SAMSON -> TIBCO :  PromoSoc
							end
							TIBCO -> SAMSON : Call SocFeaturea
							SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/rfLsSocFtrs.html rfLsSocFtrs]]
							SAMSON -> TIBCO : Response
			end
			group <b> Otherwise
				   TIBCO -> SQLMGR : Call GetServiceFeatureSQL
					SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetServiceFeature_Dual&action=query&keyValue= GetServiceFeature]]
					SQLMGR -> TIBCO : ListOfServiceFeature
					group <b> Change PP?
						TIBCO -> SQLMGR : Call GetPricePlanFeatureConflictListSQL
						SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetPricePlanFeatureConflictList&action=query&keyValue= GetPricePlanFeatureConflictList]]
						SQLMGR -> TIBCO  : ListOfSocConflicts
					end
					group <b> Add/Expire?
						TIBCO -> SQLMGR : Call GetRegularFeatureConflictsSQL
						SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetRegularFeatureConflicts&action=query&keyValue= GetRegularFeatureConflicts]]
						SQLMGR -> TIBCO  : ListOfSocConflicts
					end
			end
			
		end
		group <b> AddReg
					TIBCO -> TIBCO : Call GetLogicalDate
					group <b> isPromo
						TIBCO -> SQLMGR : Call GetReducedSocRelationShipSQL
						SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetReducedSocRelationShip&action=query&keyValue= GetReducedSocRelationShip]]
						SQLMGR -> TIBCO : SocRelation
					end 
					group <b> PromoEligibilityCheck
						TIBCO -> SAMSON : ListOfServiceAgreement
						SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evChkPromoElg.html evChkPromoElg]]
						SAMSON -> TIBCO :  PromoSoc
					end
					TIBCO -> SQLMGR : Call GetServiceFeatureSQL
					SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetServiceFeature_Dual&action=query&keyValue= GetServiceFeature]]
					SQLMGR -> TIBCO : ListOfServiceFeature
					TIBCO -> SAMSON : Call SocFeaturea
					SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/rfLsSocFtrs.html rfLsSocFtrs]]
					SAMSON -> TIBCO : Response
					group <b> Change PP?
						TIBCO -> SQLMGR : Call GetPricePlanFeatureConflictListSQL
						SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetPricePlanFeatureConflictList&action=query&keyValue= GetPricePlanFeatureConflictList]]
						SQLMGR -> TIBCO  : ListOfSocConflicts
					end
					group <b> Add/Expire?
						TIBCO -> SQLMGR : Call GetRegularFeatureConflictsSQL
						SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetRegularFeatureConflicts&action=query&keyValue= GetRegularFeatureConflicts]]
						SQLMGR -> TIBCO  : ListOfSocConflicts
					end
		end
		group <b> expire
				TIBCO -> TIBCO : Call GetLogicalDate
					group <b> isPromo
						TIBCO -> SQLMGR : Call GetReducedSocRelationShipSQL
						SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetReducedSocRelationShip&action=query&keyValue= GetReducedSocRelationShip]]
						SQLMGR -> TIBCO : SocRelation
					end 
					group <b> PromoEligibilityCheck
						TIBCO -> SAMSON : ListOfServiceAgreement
						SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evChkPromoElg.html evChkPromoElg]]
						SAMSON -> TIBCO :  PromoSoc
					end
				TIBCO -> SQLMGR : Call GetServiceFeatureSQL
					SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetServiceFeature_Dual&action=query&keyValue= GetServiceFeature]]
					SQLMGR -> TIBCO : ListOfServiceFeature	
					group <b> Change PP?
						TIBCO -> SQLMGR : Call GetPricePlanFeatureConflictListSQL
						SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetPricePlanFeatureConflictList&action=query&keyValue= GetPricePlanFeatureConflictList]]
						SQLMGR -> TIBCO : ListOfSocConflicts
					end
					group <b> Add/Expire?
						TIBCO -> SQLMGR : Call GetRegularFeatureConflictsSQL
						SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetRegularFeatureConflicts&action=query&keyValue= GetRegularFeatureConflicts]]
						SQLMGR -> TIBCO : ListOfSocConflicts
					end
		end
		group <b> BanLevel
			TIBCO -> TIBCO : Call UpdatePoolingServiceAgreementTarASF
		end 
		group <b> Sub
			TIBCO -> TIBCO : Call SamsonUpdateServiceAgreementTartASF
		end 
end

group <b> UpdatePoolingServiceAgreementTarASF
			TIBCO -> SAMSON : GetBanDetailsRequest
		    SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/arGtCstBan.html arGtCstBan]]
			SAMSON -> TIBCO :  Response
			TIBCO -> SAMSON : GetBanDetailsRequest
			SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/arSvBanSvc.html arSvBanSvc]]
			SAMSON -> TIBCO :  Response
			TIBCO -> SAMSON : GetBanDetailsRequest
			SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csGtSub.html csGtSub]]
			SAMSON -> TIBCO :  Response
end
group <b> SamsonUpdateServiceAgreementTartASF
			TIBCO -> SAMSON : GetBanDetailsRequest
		    SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/arGtCstBan.html arGtCstBan]]
			SAMSON -> TIBCO :  Response
			TIBCO -> SAMSON : GetBanDetailsRequest
			SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csSvSubAgr.html csSvSubAgr]]
			SAMSON -> TIBCO :  Response
			TIBCO -> SAMSON : GetBanDetailsRequest
			SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csGtSub.html csGtSub]]
			SAMSON -> TIBCO :  Response
end
end
group <b> GetLogicalDate
	TIBCO -> SAMSON: Call QueryLogicalDateTux
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/gnGtLgclDt.html gnGtLgclDt]]
	SAMSON -> TIBCO : Logical Date
	group <b> if SAMSON Error
	TIBCO -> SQLMGR : Call getLogicalDateSQL
	SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
	SQLMGR -> TIBCO : Logical Date 
	end
end
end
@enduml

