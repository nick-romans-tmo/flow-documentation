@startuml
title SubscriberContractService.renewContract
actor Client

group <font color=#FF0090><b> renewContract
Client->HAProxy_APIX: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/SubscriberContractService.html#renewContract renewContract]]  SubscriberContractService.renewContract
HAProxy_APIX -> HAProxy_APIX :Authenticate and Authorize
HAProxy_APIX->SubscriberContractService: renewContract REQ
SubscriberContractService->SubscriberContractService: Validate REQ

group <font color=#FF8C00><b> IF renewContractRequest/LastUpdateDate is NULL
SubscriberContractService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evGtVerInfo.html evGtVerInfo]]  Tuxedo_evGtVerInfo
SAMSON->SubscriberContractService: Samson returns BanLockInfo with LastUpdateDate/LastUpdateStamp 
end group

SubscriberContractService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csCntRenwl.html csCntRenwl]]  Tuxedo_csCntRenwl 
SAMSON->SubscriberContractService: Samson extend contract period 
SubscriberContractService->HAProxy_APIX: Returns service response
HAProxy_APIX->Client: renewContract response to client
end group

@enduml

