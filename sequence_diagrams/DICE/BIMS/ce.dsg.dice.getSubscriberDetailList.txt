@startuml
title SubscriberDetailService.getSubscriberDetailList
actor Client

group <font color=#FF0090><b> getSubscriberDetailList
Client->HAProxy_APIX: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/SubscriberDetailService.html#getSubscriberDetailList getSubscriberDetailList]]  SubscriberDetailService.getSubscriberDetailList
HAProxy_APIX -> HAProxy_APIX :Authenticate and Authorize
HAProxy_APIX->SubscriberDetailService: getSubscriberDetailList REQ

group <font color=#FF8C00><b> BAN and MSISDN Validation
SubscriberDetailService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evGtSubsByBan.html evGtSubsByBan]]  Tuxedo_evGtSubsByBan 
SAMSON->SubscriberDetailService: Samson retrieve list of subscriber associated with the BAN passed in request
end group



alt <b><color:darkorange>BAN or MSISDN Validation Failed</color>
SubscriberDetailService->HAProxy_APIX: Returns service response with failure
HAProxy_APIX->Client: getSubscriberDetailList response to client
else <b><color:darkorange>BAN and MSISDN Validation Successful</color>

loop <b><color:darkorange>subscriberNo</color>
SubscriberDetailService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csGtSub.html csGtSub]]  Tuxedo_csGtSub 
SAMSON->SubscriberDetailService: Samson return Subscriber details based on MSISDN and BAN.

SubscriberDetailService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csLsSubEsn.html csLsSubEsn]]  Tuxedo_csLsSubEsn 
SAMSON->SubscriberDetailService: Samson return existing Serial Item information details based on MSISDN and BAN.

SubscriberDetailService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evGtAddPrm.html evGtAddPrm]]  Tuxedo_evGtAddPrm 
SAMSON->SubscriberDetailService: Samson return tmobile complete indicator of the subscriber.

group <font color=#FF8C00><b> IF subscriberDetail/SIM is NOTNULL
SubscriberDetailService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/icGtSit.html icGtSit]]  Tuxedo_icGtSit 
SAMSON->SubscriberDetailService: Samson return existing new Serial Item information details based on unit ESN.
end group
note right 
IF (simDetails.getEquType() == (byte) 'S' && simDetails.getSwStateInd() == (byte) 'Y')
THEN (subscriberDetail/SIM is NOTNULL)
endnote
group <font color=#FF8C00><b> IF WIFI_ACCOUNTTYPE_SUBTYPE is NOTNULL
SubscriberDetailService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evGtVerInfo.html evGtVerInfo]]  Tuxedo_evGtVerInfo 
SAMSON->SubscriberDetailService: Samson return account verification data.
end group
note right 
Production wifi.accountType.subType=I_A|S_K|S_C|B_C|S_D|S_Y|S_V|I_U|I_X|
I_F|G_F|I_G|I_M|B_M|B_O|B_N|B_P|I_W|I_R|G_R|B_L|S_T|S_I|S_E
endnote
group <font color=#FF8C00><b> IF partnerId is in SUBSCRIBERDETAIL_ZBB_PARTNERS
SubscriberDetailService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csLsSrvAgr.html csLsSrvAgr]]  Tuxedo_csLsSrvAgr 
SAMSON->SubscriberDetailService: Samson return subscriber agreement for all types.
end group
note right 
Current property subscriberdetail.zbb.partners is missing in Production
endnote

end

SubscriberDetailService->HAProxy_APIX: Returns service response
HAProxy_APIX->Client: getSubscriberDetailList response to client
end


end group

@enduml

