@startuml
title <size:15>__Service Email__</size>
actor Client
participant APIGEE
participant ServiceEmail_API
participant RabbitMQ
participant Sentry

group Sync Call
Client-> APIGEE: call (email/sendemail)
APIGEE-> APIGEE: validation
APIGEE-> ServiceEmail_API: Call sendEmail (email/sendemail)
ServiceEmail_API-> ServiceEmail_API: validation
ServiceEmail_API->Sentry: Invoke the client configured for schemas.email.endpoint
Sentry->ServiceEmail_API: Return
ServiceEmail_API->APIGEE: Return with Success(If email != null && email.isReceived() == true)/Error(Else)
APIGEE->Client: Return
end

group Async call
Client-> APIGEE: call (/sendemail/async)
APIGEE-> APIGEE: validation
APIGEE-> ServiceEmail_API: Call sendEmailAsync (/sendemail/async)
ServiceEmail_API->RabbitMQ: publish to TMOBILE.DLAB02.SERVICE.EMAIL.ASYNC queue
end

group Asynchronous Listener
ServiceEmail_API->ServiceEmail_API: sendSMSAsyncMessage
ServiceEmail_API-> RabbitMQ: Listens the queue SERVICE.EMAIL.ASYNC(email.incomingqueue)
ServiceEmail_API-> ServiceEmail_API: validation
ServiceEmail_API->Sentry: Invoke the client configured for schemas.email.endpoint
Sentry->ServiceEmail_API: Return with Success(If email != null && email.isReceived() == true)/Error(Else)
end
@enduml