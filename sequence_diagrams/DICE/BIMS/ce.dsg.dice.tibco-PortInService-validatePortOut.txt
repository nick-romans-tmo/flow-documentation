@startuml
title PortInService.validatePortOut Service
actor Client

group <font color=#FF0090><b> validatePortOut     
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-06-09/sid/html/PortInService.html#validatePortOut validatePortOutRequest]] 

group <b> If GetCarrierInfo
	
	TIBCO -> TIBCO : GetLogicalDate
	TIBCO -> SAMSON: [operatorID, applicationID, logicalDate]
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/197/services/rfLsCarrInfo.html rfLsCarrInfo]]
	SAMSON -> TIBCO : CarrierInfo
	
	TIBCO ->SQLMGR: GetWholesalePartnerDetails
	SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetWholesalePartnerDetails&action=query&keyValue= GetWholesalePartnerDetails]]
	SQLMGR -> TIBCO : partnerDetails
	
end
TIBCO -> ACC : PortOutRequest
ACC -> ACC :ACC_WS.PortOutRequest
ACC -> TIBCO :PortOutRequestResponse

TIBCO -> SAMSON : UpdateAutomatedPortIn
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/197/services/csUpAutoPort.html csUpAutoPort]]
SAMSON -> TIBCO : UpdateAutomatedPortInResponse

TIBCO -> TIBCO : UpdateAutomatedPortInResponse to validatePortOutResponse

group <b> GetLogicalDate
	TIBCO -> SAMSON: Call QueryLogicalDateTux
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/gnGtLgclDt.html gnGtLgclDt]]
	SAMSON -> TIBCO : Logical Date
	group <b> if SAMSON Error
	TIBCO -> SQLMGR : Call getLogicalDateSQL
	SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
	SQLMGR -> TIBCO : Logical Date 
	end
end

TIBCO -> Client : validatePortOutResponse
end
@enduml



