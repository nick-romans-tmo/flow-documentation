@startuml
title PortableAssetService.CheckPortInEligibility Service
actor Client

group <font color=#FF0090><b> checkPortInEligibility
Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-06-09/sid/html/PortableAsset.html#CheckPortInEligibility esiCheckPortInEligibilityInput]]
TIBCO -> TIBCO : Validate Input[Application_Id, Operator_Id and Port-In Request Number are required]
TIBCO -> TIBCO : esiCheckPortInEligibilityInput to csiCheckPortInEligibilityInput
TIBCO -> TIBCO : csiCheckPortInEligibilityInput to WLNPRequest
group <b> Logical Date
	TIBCO->SAMSON: CallLogicalDate
	SAMSON->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/gnGtLgclDt.html gnGtLgclDt]]
	SAMSON->TIBCO: LogicalDate
	group <b> SAMSON Error
		TIBCO->SQLMGR: Call GetLogicalDateSQL
		SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
		SQLMGR->TIBCO: LogicalDate
	end
end
TIBCO->SAMSON : Call CheckPortability
SAMSON->SAMSON: [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csChkPortIn.html csChkPortIn]]
SAMSON->TIBCO: Success Response

TIBCO -> TIBCO : WLNPRequest to csiCheckPortInEligibilityOutput
TIBCO->TIBCO: csiCheckPortInEligibilityOutput to esiCheckPortInEligibilityOutput
TIBCO->Client: esiCheckPortInEligibilityOutput

group <font color=#FF8C00><b>  Error Response
SAMSON->TIBCO: error
SQLMGR->TIBCO: error
TIBCO->Client: Error Response
end
end group
@enduml