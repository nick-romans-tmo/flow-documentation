@startuml
title PortableAssetService.NotifyPortActivityCallback Service
actor Client

group <font color=#FF0090><b> notifyPortActivityCallback
Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-11-04/sid/html/PortableAsset.html#NotifyPortActivityCallback notifyPortActivityCallbackRequest]]
TIBCO -> TIBCO : notifyPortActivityCallbackRequest to csiNotifyPortActivityCallbackRequest

TIBCO -> TIBCO : GetLogicalDate
TIBCO -> SAMSON: getPortInRequestInput
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csGtAutoPort.html csGtAutoPort]]
SAMSON -> TIBCO : eligibleBillFormat
group <b> IF INTER
	group <b> getCarrier
		TIBCO -> SAMSON: applicationID, logialDate, opratorID
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/rfLsCarrInfo.html rfLsCarrInfo]]
		SAMSON -> TIBCO : CarrierInfo
	end
	group <b> Get SP 
		TIBCO -> SAMSON: csiPortInActivityDetails
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csGtPortReq.html csGtPortReq]]
		SAMSON -> TIBCO : portInActivityDetails	
	end
	group <b> CheckPortInDueDateTux
		TIBCO -> SAMSON: CheckPortInDueDate request
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csChkDueDate.html csChkDueDate]]
		SAMSON -> TIBCO : DueDate response		
	end
end
group <b> INTRA
		TIBCO -> SAMSON: csiUpdateAutomatedPortInRequest
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csUpAutoPort.html csUpAutoPort]]
		SAMSON -> TIBCO : portDueDate response
end

group <b> GetLogicalDate
	TIBCO -> SAMSON: Call QueryLogicalDateTux
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/gnGtLgclDt.html gnGtLgclDt]]
	SAMSON -> TIBCO : Logical Date
	group <b> if SAMSON Error
	TIBCO -> SQLMGR : Call getLogicalDateSQL
	SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
	SQLMGR -> TIBCO : Logical Date 
	end
end
TIBCO -> TIBCO : csiUpdateAutomatedPortInResponse to notifyPortActivityCallbackResponse
TIBCO->Client: notifyPortActivityCallbackResponse

group <font color=#FF8C00><b>  Error Response
SAMSON->TIBCO: error
SQLMGR->TIBCO: error
TIBCO->Client: Error Response
end
end group
@enduml