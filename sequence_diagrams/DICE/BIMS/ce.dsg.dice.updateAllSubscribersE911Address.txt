@startuml
title AddressUpdateService.updateAllSubscribersE911Address
actor Client


group <font color=#FF0090><b> updateAllSubscribersE911Address
Client->HAProxy_APIX: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/AddressUpdateService.html#updateAllSubscribersE911Address updateAllSubscribersE911Address]]  AddressUpdateService.updateAllSubscribersE911Address
HAProxy_APIX -> HAProxy_APIX :Authenticate and Authorize
HAProxy_APIX->AddressUpdateService: updateAllSubscribersE911Address REQ
group <font color=#FF8C00><b>  Validate REQ
AddressUpdateService->SAMSON: [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evValdateAdr.html evValdateAdr]]  Tuxedo_evValdateAdr
AddressUpdateService->SAMSON: [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/arGtCstBan.html arGtCstBan]]  Tuxedo_arGtCstBan
AddressUpdateService->SAMSON: [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csLsBanSub.html csLsBanSub]]  Tuxedo_csLsBanSub 
end
note left 
Validate address, ban and msisdn
endnote
alt <b><color:darkorange>Validation Failed</color>
AddressUpdateService->AddressUpdateService: Validation Failed
AddressUpdateService->HAProxy_APIX: Returns service response with failure
HAProxy_APIX->Client: updateAllSubscribersE911Address response to client
else <b><color:darkorange>Validation Successful</color>

AddressUpdateService->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-07-21/sid/html/UpdateAllSubsAddress.html#UpdateAllSubsAddressOperation UpdateAllSubsAddressOperation]] UpdateAllSubsAddress.UpdateAllSubsAddressOperation
note right 
TIBCO Saves subscriber e911address
endnote
AddressUpdateService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/mmInMemo.html mmInMemo]] Tuxedo_mmInMemo
note right 
TUXEDO InsertMemo
endnote
AddressUpdateService->HAProxy_APIX: Returns service response
HAProxy_APIX->Client: updateAllSubscribersE911Address response to client
end
end group

@enduml