@startuml
title Bill.GetPaymentArrangement Service
actor Client

group <font color=#FF0090><b> getPaymentArrangement     
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-06-09/sid/html/Bill.html#GetPaymentArrangement esiGetPaymentArrangementRequest]]
TIBCO->TIBCO: BAN Validation
TIBCO->TIBCO: esiGetPaymentArrangementRequest to csiGetPaymentArrangementRequest

TIBCO -> SAMSON: Call QueryLogicalDateTux
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/gnGtLgclDt.html gnGtLgclDt]]
SAMSON -> TIBCO : Logical Date
group <b> if SAMSON Error
TIBCO -> SQLMGR : Call getLogicalDateSQL
SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
SQLMGR -> TIBCO : Logical Date 
end

TIBCO -> SAMSON : csiGetPaymentArrangementRequest[includes LogicalDate] 
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/197/services/evGtCurPaPac.html evGtCurPaPac]]
SAMSON -> TIBCO :  Samson Response

TIBCO->TIBCO: csiGetPaymentArrangementResponse to esiGetPaymentArrangementResponse 
TIBCO -> Client: esiGetPaymentArrangementResponse


group <font color=#FF8C00><b>  Error Response
SAMSON -> TIBCO :error
SQLMGR -> TIBCO :error
TIBCO -> Client :error response
end
end
@enduml