@startuml
title Adjustment.GetPendingMonetaryAdjustments Service
actor Client

group <font color=#FF0090><b> getPendingMonetaryAdjustments
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-07-21/sid/html/Adjustment.html#getPendingMonetaryAdjustments esiGetPendingMonetaryAdjustmentsRequest]]
TIBCO -> TIBCO :esiGetPendingMonetaryAdjustmentsRequest to csiGetPendingMonetaryAdjustmentsRequest
TIBCO -> SAMSON: Call QueryLogicalDateTux
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/gnGtLgclDt.html gnGtLgclDt]]
SAMSON -> TIBCO : Logical Date
group <b> if SAMSON Error
TIBCO -> SQLMGR : Call getLogicalDateSQL
SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
SQLMGR -> TIBCO : Logical Date
end
TIBCO->SQLMGR: Call GetPendingBanAdjustmentHistorySQL
SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetPendingBanAdjustmentHistory&action=query&keyValue= GetPendingBanAdjustmentHistory]]
SQLMGR->TIBCO: Response
TIBCO -> TIBCO: csiGetPendingMonetaryAdjustmentsResponse to esiGetPendingMonetaryAdjustmentsResponse
TIBCO->Client: esiGetPendingMonetaryAdjustmentsResponse
group <font color=#FF8C00><b>  Error Response
SQLMGR->TIBCO: SQLError
SAMSON->TIBCO: error
TIBCO->Client: Error Response
end
end group
@enduml