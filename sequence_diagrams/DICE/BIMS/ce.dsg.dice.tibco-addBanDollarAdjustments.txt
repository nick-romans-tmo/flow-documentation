@startuml
title AddBANDollarAdjustment Service
actor Client

group <font color=#FF0090><b> addBANDollarAdjustment     
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-07-21/sid/html/AddBANDollarAdjustment.html#addBANDollarAdjustment AddBANDollarAdjustmentSubscriberRequest]]
TIBCO->TIBCO: ValidateData [Valid BAN,MSISDN,DollarAmount is Mandatory]
TIBCO-> SQLMGR : Call GetAdjustmentReasonCodeSQL
SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetAdjustmentReasonCode&action=query&keyValue= GetAdjustmentReasonCode]]
SQLMGR -> TIBCO: GetAdjustmentReasonCodeResponse
group <b> Logical Date
TIBCO->SAMSON: CallLogicalDate
SAMSON->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/gnGtLgclDt.html gnGtLgclDt]]
SAMSON->TIBCO: LogicalDate
group <b> SAMSON Error
TIBCO->SQLMGR: Call GetLogicalDateSQL
SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
SQLMGR->TIBCO: LogicalDate
end
end
TIBCO->SAMSON: Call GetBanDetails[SamsonSaveBANAdjustmentsInput]
SAMSON->SAMSON: [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/arGtCstBan.html arGtCstBan]]  Tuxedo_arGtCstBan 
SAMSON->TIBCO: GetBanDetails Response
TIBCO->SAMSON: Call SaveAdjustmentsForBan
SAMSON->SAMSON: [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/arAdjBan.html arAdjBan]]  Tuxedo_arAdjBan
SAMSON->TIBCO: Success Response
TIBCO->TIBCO: SamsonSaveBANAdjustmentsOutput
TIBCO->TIBCO: PublicAddBANDollarAdjustmentResponse
TIBCO->Client: AddBANDollarAdjustmentSubscriberResponse
group <font color=#FF8C00><b>  Error Response
SAMSON->TIBCO: error
SQLMGR->TIBCO: error
TIBCO->Client: Error Response
end
end group
@enduml