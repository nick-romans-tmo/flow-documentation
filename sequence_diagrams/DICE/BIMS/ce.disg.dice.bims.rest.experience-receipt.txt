@startuml
title <size:15>__ExperienceReceipt__</size>
actor Client
participant APIGEE
participant ExperienceReceiptPCF_API
participant generatequote_API
participant receipt_API
participant Samson

Client-> APIGEE: [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dice.bims/Experience-Receipt/ExperienceReceipts.json createReceipt Request]]
APIGEE-> APIGEE: validation
APIGEE-> ExperienceReceiptPCF_API: createReceipt
ExperienceReceiptPCF_API-> ExperienceReceiptPCF_API: validation
ExperienceReceiptPCF_API-> generatequote_API: [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dice.bims/Experience-Receipt/ServiceQuotes.json serviceQuote]]
generatequote_API-> ExperienceReceiptPCF_API: serviceQuote

ExperienceReceiptPCF_API-> Samson: [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dice.bims/Experience-Receipt/evGtPrimSub.json EvGtPrimSubInput]]
Samson-> ExperienceReceiptPCF_API: EvGtPrimSubOutput.getPrimarySubscriberNumber();

ExperienceReceiptPCF_API-> Samson: [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dice.bims/Experience-Receipt/evGtCustInfo.json EvGtCustInfoInput]]
Samson-> ExperienceReceiptPCF_API: EvGtCustInfoOutput.getPrimarySubscriberName();

ExperienceReceiptPCF_API-> receipt_API: [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dice.bims/Experience-Receipt/Receipts.json PostReceiptRequest]]
receipt_API-> ExperienceReceiptPCF_API: PostReceiptResponse
ExperienceReceiptPCF_API-> APIGEE: createReceiptResponse
APIGEE-> Client: createReceipt Response
@enduml