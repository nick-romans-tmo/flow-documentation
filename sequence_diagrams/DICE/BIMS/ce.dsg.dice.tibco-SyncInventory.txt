@startuml
title OrderFulfillmentService.SyncInventory
actor Client
group <font color=#FF0090><b> OrderFulfillmentService.SyncInventory 
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-11-04/sid/html/OrderFulfillmentService.html# SyncInventory]]  

group <font color=darkorange><b> Trans_receipt
group <b> If RetrieveFromCache = false
TIBCO -> SQLMGR : Call GetTrackingEnableValue
SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=getTransTrackingEnabled&action=query&keyValue=  getTransTrackingEnabled]]
SQLMGR -> TIBCO : Returns tracking value.
note over TIBCO, SQLMGR
Store data in cache
end note
end group
note right
if Tracking='Y' than SendStepToJMS(TransactionTracker-JMS) -JMS call
end note
end group

TIBCO -> TIBCO :esiSyncInventoryRequest to csiSyncInventoryRequest

group <font color=darkorange><b> SyncInventory
group <b> SAPR3SyncInventory
group <b> Transaction_Tracking_Receipt_Success
note over TIBCO, SQLMGR
Invoking the logic mentioned in group Trans_receipt with required input
end note
end group
TIBCO->TIBCO :csiSyncInventoryRequest to syncInventoryRequest
group <b> SyncInventorySFTPPutData
note over TIBCO, SQLMGR
Invoking SFTPPutData java method
end note
end group
group <b> Transaction_Tracking_Send_Success
note over TIBCO, SQLMGR
Invoking the logic mentioned in group Trans_receipt with required input
end note
end group
end group

group <b> JMS SyncInventoryACK
note over TIBCO, SQLMGR
JMS_SyncInventory_Ack_Sender  -JMS call
end note
end group

end group


group <font color=darkorange><b> Trans_send
group <b> If RetrieveFromCache = false
TIBCO -> SQLMGR : Call GetTrackingEnableValue
SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=getTransTrackingEnabled&action=query&keyValue=  getTransTrackingEnabled]]
SQLMGR -> TIBCO : Returns tracking value.
note over TIBCO, SQLMGR
Store data in cache
end note
end group
note right
if Tracking='Y' than SendStepToJMS(TransactionTracker-JMS) -JMS call
end note
end group




TIBCO->Client: TIBCO response to client

end group

@enduml