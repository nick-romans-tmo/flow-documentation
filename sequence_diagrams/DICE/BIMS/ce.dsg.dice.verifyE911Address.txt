@startuml
title AddressVerificationService.verifyE911Address
actor Client

group <font color=#FF0090><b> verifyE911Address
Client->HAProxy_APIX: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/AddressVerificationService.html#verifyE911Address verifyE911Address]]  AddressVerificationService.verifyE911Address
HAProxy_APIX -> HAProxy_APIX :Authenticate and Authorize
HAProxy_APIX->AddressVerificationService: verifyE911Address REQ
group <font color=#FF8C00><b> verifyAddress
AddressVerificationService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evValdateAdr.html evValdateAdr]]  Tuxedo_evValdateAdr 
end 
alt <b><color:darkorange>verifyAddress Validation Failed</color>
AddressVerificationService->HAProxy_APIX: Returns service response with failure
HAProxy_APIX->Client: verifyE911Address response to client
else <b><color:darkorange>verifyAddress Validation Successful</color>
AddressVerificationService->HAProxy_APIX: Returns service response
HAProxy_APIX->Client: verifyE911Address response to client
end
note left 
<b> verifyAddress is valid/invalid
endnote
end group


@enduml