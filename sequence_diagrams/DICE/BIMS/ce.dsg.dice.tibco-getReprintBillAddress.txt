@startuml
title Bill.GetReprintBillAddress Service
actor Client

group <font color=#FF0090><b> getreprintBillAddress     
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-06-09/sid/html/Bill.html#GetReprintBillAddress esiGetReprintBillAddressRequest]]

TIBCO->TIBCO: esiGetReprintBillAddressRequest to csiGetReprintBillAddressRequest
TIBCO->TIBCO: Input Validation[ApplicationId,OperatorId,BAN or MSISDN,pageNo or pageSize]
group <b> Has No BAN

TIBCO -> SAMSON: Call QueryLogicalDateTux
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/gnGtLgclDt.html gnGtLgclDt]]
SAMSON -> TIBCO : Logical Date
group <b> if SAMSON Error
TIBCO -> SQLMGR : Call getLogicalDateSQL
SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
SQLMGR -> TIBCO : Logical Date 
end
TIBCO -> SAMSON : Call GetBANForMSISDNTux[includes LogicalDate]
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/197/services/csSrchSub.html csSrchSub]]
SAMSON -> TIBCO : SAMSON Response
group <b> If BAN subStatus!=A or S
TIBCO -> TIBCO : Call ReprintCallOrch Group
end
group <b> else
TIBCO -> SAMSON :Call IsFirstFullCycleTux[includes LogicalDate]
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/197/services/arCkFrstCyc.html arCkFrstCyc]]
SAMSON -> TIBCO : Samson Response
TIBCO -> TIBCO: If isFirstFullCycle = true then Call ReprintCallOrch Group Else Call HasBANOrch
end
end 
group <b> HasBANOrch
group <b> if Version>3
TIBCO -> SAMSON : Call GetBanDetailsTux[includes BAN and LogicalDate]
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/197/services/arGtCstBanExt.html arGtCstBanExt]]
end 
group <b> else
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/197/services/arGtCstBan.html arGtCstBan]]
end
SAMSON -> TIBCO : Samson Response
group <b> If isFirstFullCycle invoked? Or Cancelled BAN (Status=C or N)
TIBCO -> TIBCO : Call ReprintCallOrch
end
group <b> else
TIBCO -> SAMSON : Call IsFirstFullCycleTux[includes LogicalDate]
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/197/services/arCkFrstCyc.html arCkFrstCyc]]
SAMSON -> TIBCO : Samson Response
TIBCO -> SAMSON : csiGetReprintBillAddressRequest [includes LogicalDate]
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/197/services/csLsBanSub.html csLsBanSub]]
SAMSON -> TIBCO : SAMSON Response
end
end
group <b> ReprintCallOrch
group <b> reprintAddressFlag set? Or BAN_STATUS='N' Or subStatus='C'
TIBCO -> SAMSON : csiGetReprintBillAddressRequest [includes LogicalDate]
SAMSON -> SAMSON : csGtRpntAddrs
SAMSON -> TIBCO :Samson Response
end
group <b> Else
TIBCO -> SAMSON : csiGetReprintBillAddressRequest [includes Samson's LogicalDate]
SAMSON -> SAMSON :[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/197/services/csGtReprntAdr.html csGtReprntAdr]]
SAMSON -> TIBCO :Samson Response
end
end
TIBCO -> TIBCO : csiGetReprintBillAddressResponse to esiGetReprintBillAddressResponse
TIBCO -> Client : getreprintBillAddressResponse

group <font color=#FF8C00><b>  Error Response
SQLMGR -> TIBCO: error
SAMSON -> TIBCO :error
TIBCO -> Client: error response
end
end
@enduml