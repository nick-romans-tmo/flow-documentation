@startuml
title SubscriberLookup.GetPrepaidSubscriberDetails    
actor Client
group <font color=#FF0090><b> GetPrepaidSubscriberDetails

Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-08-25/sid/html/SubscriberLookup.html#GetPrepaidSubscriberDetails GetPrepaidSubscriberDetails]]  SubscriberLookup.GetPrepaidSubscriberDetails  request

TIBCO -> TIBCO :esiValidatePortOutRequest to csiValidatePortOutRequest

group <b> If Universe_Bridging_Flag=TRUE getCustomerStatus

TIBCO -> TBILL  :Call QueryBridgingStatus.getCustomerStatus
TBILL -> TIBCO  :getCustomerStatus Response

group <b> If  QueryBridgingStatusOutput/MigrationStatus=TMO-App-TBILL-TBILL-TITAN or TMO-App-TBILL-TBILL-POLARIS and applicationId = "NRPREPAID" 

TIBCO -> TBILL  :Call GetLineOfServiceDetails
TBILL -> TIBCO  : getLineOfServiceDetails Response

TIBCO -> TBILL  :Call GetPrepaidBalance
TBILL -> TIBCO  : GetPrepaidBalance Response
end group
alt  
TIBCO -> RPX : Call querySubscriberAccountInfo
RPX -> TIBCO :  querySubscriberAccountInfo response.

TIBCO -> RPX : Call QuerySubscriberFirstChargedEvent
RPX -> TIBCO :  QuerySubscriberFirstChargedEvent response.

end
end group
alt  <b> If Universe_Bridging_Flag=FALSE
TIBCO -> RPX : Call querySubscriberAccountInfo
RPX -> TIBCO :  querySubscriberAccountInfo response.

TIBCO -> RPX : Call QuerySubscriberFirstChargedEvent
RPX -> TIBCO :  QuerySubscriberFirstChargedEvent response.
end




TIBCO -> TIBCO : csiValidatePortOutResponse to esiValidatePortOutResponse

 

TIBCO->Client: TIBCO response to client

end group

@enduml