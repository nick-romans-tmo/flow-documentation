@startuml
title Bill.GetFuturePaymentsCancelReasonCodes Service
actor Client

group <font color=#FF0090><b> getFuturePaymentsCancelReasonCodes
Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-06-09/sid/html/Bill.html#GetFuturePaymentsCancelReasonCodes getFuturePaymentsCancelReasonCodesRequest]]
TIBCO -> TIBCO :getFuturePaymentsCancelReasonCodesRequest to csiGetFuturePaymentsCancelReasonCodesRequest
TIBCO -> SAMSON : Call GetFuturePaymentsCancelReasonCodesTux
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/evGbDDCnclRsn.html evGbDDCnclRsn]]
SAMSON -> TIBCO : Response
TIBCO -> TIBCO : CSI Response to esiGetFuturePaymentsCancelReasonCodesResponse
TIBCO -> Client: esiGetFuturePaymentsCancelReasonCodesResponse
group <font color=#FF8C00><b>  Error Response
SAMSON->TIBCO: error
TIBCO->Client: Error Response
end
end group
@enduml