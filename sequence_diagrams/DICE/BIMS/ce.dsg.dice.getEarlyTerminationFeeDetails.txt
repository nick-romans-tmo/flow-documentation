@startuml
title SubscriberDetailService.getEarlyTerminationFeeDetails
actor Client

group <font color=#FF0090><b> getEarlyTerminationFeeDetails
Client->HAProxy_APIX: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/SubscriberDetailService.html#getEarlyTerminationFeeDetails getEarlyTerminationFeeDetails]]  SubscriberDetailService.getEarlyTerminationFeeDetails
HAProxy_APIX -> HAProxy_APIX :Authenticate and Authorize
HAProxy_APIX->SubscriberDetailService: getEarlyTerminationFeeDetails REQ
SubscriberDetailService->SubscriberDetailService: Validate REQ
SubscriberDetailService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csGtEtfAmt.html csGtEtfAmt]]  Tuxedo_csGtEtfAmt 
SAMSON->SubscriberDetailService: Samson retrieve early termination fee details based on MSISDN or BAN passed in request 
SubscriberDetailService->HAProxy_APIX: Returns service response
HAProxy_APIX->Client: getEarlyTerminationFeeDetails response to client
end group

@enduml

