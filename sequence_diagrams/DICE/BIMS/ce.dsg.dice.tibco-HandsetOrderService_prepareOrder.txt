@startuml
title HandsetOrderService.PrepareOrder
actor Client
group <font color=#FF0090><b> prepareOrder
Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-11-04/sid/html/HandsetOrderService.html#prepareOrder prepareOrderRequest]]

TIBCO -> TIBCO : Validate Request

group <b> If MSISDN exists : CreateSession
	TIBCO -> VESTA : SessionCreateRequest
	VESTA -> VESTA :[[https://10.134.115.20:6503/TMobileUS_Service/Service Service.SessionCreate]]
	VESTA -> TIBCO : SessionCreateResponse
	
	group <b> If registeredPaymentMethodIdentifier exists
		TIBCO -> VESTA : PaymentDeviceOnFileListRequest
		VESTA -> VESTA :[[https://10.134.115.20:6503/TMobileUS_Service/Service Service.PaymentDeviceOnFileList]]
		VESTA -> TIBCO : PaymentDeviceOnFileListResponse		
	end
	group <b> If ExistingCard
		TIBCO -> VESTA : ChargeOrderPrepareWithPDOFIDRequest
		VESTA -> VESTA :[[https://10.134.115.20:6503/TMobileUS_Service/Service Service.ChargeOrderPrepareWithPDOFID]]
		VESTA -> TIBCO : ChargeOrderPrepareWithPDOFIDResponse		
	end
	group <b> If ExistingCheck
		TIBCO -> VESTA : CheckOrderPrepareWithPDOFIDRequest
		VESTA -> VESTA :[[https://10.134.115.20:6503/TMobileUS_Service/Service Service.CheckOrderPrepareWithPDOFID]]
		VESTA -> TIBCO : CheckOrderPrepareWithPDOFIDResponse		
	end
	group <b> If NewCard
		TIBCO -> VESTA : ChargeOrderPrepareWithNewCardRequest
		VESTA -> VESTA :[[https://10.134.115.20:6503/TMobileUS_Service/Service Service.ChargeOrderPrepareWithNewCard]]
		VESTA -> TIBCO : ChargeOrderPrepareWithNewCardResponse		
	end
	group <b> If NewCheck
		TIBCO -> VESTA : CheckOrderPrepareWithNewCheckRequest
		VESTA -> VESTA :[[https://10.134.115.20:6503/TMobileUS_Service/Service Service.CheckOrderPrepareWithNewCheck]]
		VESTA -> TIBCO : CheckOrderPrepareWithNewCheckResponse		
	end	
	group <b> If SessionCreateResponse/SessionKey exists
		TIBCO -> VESTA : IAPNAme, SessionKey
		VESTA -> VESTA :[[https://10.134.115.20:6503/TMobileUS_Service/Service Service.SessionEnd]]
		VESTA -> TIBCO : SessionEndResponse		
	end	
end
group <b> Otherwise
	TIBCO -> VESTA : CalsulateTaxAndFeesRequest
	VESTA -> VESTA :[[https://10.134.115.20:6503/TMobileUS_Service/Service Service.CalsulateTaxAndFees]]
	VESTA -> TIBCO : CalsulateTaxAndFeesResponse
end

TIBCO -> Client : prepareOrderResponse

end
@enduml	