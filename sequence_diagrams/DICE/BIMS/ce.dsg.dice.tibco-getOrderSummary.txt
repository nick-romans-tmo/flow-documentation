@startuml
title OrderEvent.getOrderSummary
actor Client

group <font color=#FF0090><b> getOrderSummary
Client->TIBCO: getOrderSummaryRequest[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-07-21/sid/html/OrderEvent.html#getOrderSummary getOrderSummaryRequest]] 
TIBCO->TIBCO: ValidateRequest ban/SapOrderNumber/OrderNumber Exists
group <b> Valid GetOrderSummaryRequest 
group <b> If orderNumber/sapOrderNumber Not exist
TIBCO->SQLMGR: Call GetOrdersSQL
SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetOrders&action=query&keyValue= GetOrders]]
end
TIBCO->SQLMGR: Call OrderDetailsSQL
SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetOrderDetails&action=query&keyValue= GetOrderDetails]]
group <b> sap_order_number or src_order_number exist
group <b> If msisdn exist
SQLMGR->IsPAH: getOrderSummaryRequest
IsPAH->IsPAH: ISelfCareService.isPAH[Ignore DSPA Error]
IsPAH-> TIBCO: isPAHResponse
end
SQLMGR->TIBCO: GetOrdersOutput[eventId]
TIBCO->TIBCO: ValidateData - Check Order is associated with account
TIBCO->SQLMGR: Call GetOrderItemsSQL
SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetOrderItems&action=query&keyValue= GetOrderItems]]
group <b> isD360Dormant=false
SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetOrderNotificationConfigSelect&action=query&keyValue= GetOrderNotificationConfigSelect]]
group <b> If SEND_SMS='G' or SEND_EMAIL='G'
SQLMGR->OM: GetChildOrders
OM->OM: Invoke NodeHistory
OM->TIBCO: MapResponse
end
end
SQLMGR->SQLMGR:[[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetOrderHistory&action=query&keyValue= GetOrderHistory]]
group <b> If retrieveRMANumber='true'
SQLMGR->SQLMGR:[[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=getOrderItemsForOrderShipped&action=query&keyValue= getOrderItemsForOrderShipped]]
group <b> If MSISDN not available
SQLMGR->SQLMGR:[[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=getRMANumberFromOrderId&action=query&keyValue= getRMANumberFromOrderId]]
SQLMGR->TIBCO: MapResponse[Ignore GetRMA Error]
end
group <b> Otherwise
SQLMGR->TIBCO: MapResponse
end
end
group <b> else
SQLMGR->TIBCO: MapResponse
end
end
end
group  <b> MapResponse
group <b> If sap_order_number and sendId exist
TIBCO->SQLMGR: Call InsertViewedEventsToEER
SQLMGR->SQLMGR: [[qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=InsertViewedEvents&action=query&keyValue= InsertViewedEvents]]
SQLMGR->TIBCO: getOrderSummaryResponse
end
group <b> else
TIBCO->TIBCO: getOrderSummaryResponse
end
end 
TIBCO->Client: getOrderSummaryResponse
end
group <b> ErrorResponse
OM->TIBCO: error
IsPAH->TIBCO: error
SQLMGR->TIBCO: error
TIBCO->Client: errorResponse
end group
@enduml