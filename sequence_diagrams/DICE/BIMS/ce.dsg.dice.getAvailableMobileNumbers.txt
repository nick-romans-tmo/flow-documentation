@startuml
title NumberManagementService.getAvailableMobileNumbers
actor Client

group <font color=#FF0090><b> getAvailableMobileNumbers
Client->HAProxy_APIX: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/NumberManagementService.html#getAvailableMobileNumbers getAvailableMobileNumbers]]  NumberManagementService.getAvailableMobileNumbers
HAProxy_APIX -> HAProxy_APIX :Authenticate and Authorize
HAProxy_APIX->NumberManagementService: getAvailableMobileNumbers REQ
NumberManagementService->NumberManagementService: Validate REQ
NumberManagementService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csGetSubNm.html csGetSubNm]]  Tuxedo_csGetSubNm 
SAMSON->NumberManagementService: Samson returns getAvailableMobileNumbers
NumberManagementService->HAProxy_APIX: Returns service response
HAProxy_APIX->Client: getAvailableMobileNumbers response to client
end group

@enduml

