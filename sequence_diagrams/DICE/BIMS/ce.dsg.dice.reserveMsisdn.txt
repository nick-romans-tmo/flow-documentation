@startuml
title NumberManagementService.reserveMsisdn
actor Client

group <font color=#FF0090><b> reserveMsisdn
Client->HAProxy_APIX: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/NumberManagementService.html#reserveMsisdn reserveMsisdn]]  NumberManagementService.reserveMsisdn
HAProxy_APIX -> HAProxy_APIX :Authenticate and Authorize
HAProxy_APIX->NumberManagementService: reserveMsisdn REQ
NumberManagementService->NumberManagementService: Validate REQ

alt <b><color:darkorange>IF msisdnReservationRequest/NumberLocationType equales PREPAID ("PR")</color>

alt <b><color:darkorange>IF msisdnReservationRequest/Ngp is NULL</color>

alt <b><color:darkorange>IF msisdnReservationRequest/Msisdn is NOTNULL</color>
database RSP_DB
NumberManagementService->RSP_DB: [[RSP_GetNgpByNpaNxx]]  NpaNxx.getNgpByNpaNxx
RSP_DB->NumberManagementService: Extract NGP from given MSISDN.
else <b><color:darkorange>IF msisdnReservationRequest/Nxx is NOTNULL</color>
database RSP_DB
NumberManagementService->RSP_DB: [[RSP_GetNgpByNpaNxx]]  NpaNxx.getNgpByNpaNxx
RSP_DB->NumberManagementService: Extract NGP from given Nxx.
else 
database RSP_DB
NumberManagementService->RSP_DB: [[RSP_GetNgpByNpa]]  NpaNxx.getNgpByNpa
RSP_DB->NumberManagementService: Extract NGP from given npa.
end
NumberManagementService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/nmChgCtnSts.html nmChgCtnSts]]  Tuxedo_nmChgCtnSts 
SAMSON->NumberManagementService:Samson reserve PREPAID Msisdn

else <b><color:darkorange>IF msisdnReservationRequest/Ngp is NOTNULL</color>
NumberManagementService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/nmChgCtnSts.html nmChgCtnSts]]  Tuxedo_nmChgCtnSts 
SAMSON->NumberManagementService:Samson reserve PREPAID Msisdn
end

else
NumberManagementService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evHldNum.html evHldNum]]  Tuxedo_evHldNum 
SAMSON->NumberManagementService:Samson reserve NumberLocationType POSTPAID/WHOLESALE Msisdn
end

NumberManagementService->HAProxy_APIX: Returns service response
HAProxy_APIX->Client: reserveMsisdn response to client
end group

@enduml

