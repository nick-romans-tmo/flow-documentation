@startuml
title BillDetailsService.getRecurringCharges
actor Client

group <font color=#FF0090><b> getRecurringCharges
Client->HAProxy_APIX: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/BillDetailsService.html#getRecurringCharges getRecurringCharges ]]  BillDetailService.getRecurringCharges
HAProxy_APIX -> HAProxy_APIX :Authenticate and Authorize
HAProxy_APIX->BillDetailService:  getRecurringCharges REQ
BillDetailService->BillDetailService:  Validate REQ
BillDetailService->SAMSON: [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evGbBilCrg.html evGbAdjHis]]  Tuxedo_evGbBilCrg 
SAMSON->BillDetailService: Samson returns RecurringChargesResponse 
BillDetailService->HAProxy_APIX: Returns service response
HAProxy_APIX->Client: getRecurringCharges response to client
end group

@enduml