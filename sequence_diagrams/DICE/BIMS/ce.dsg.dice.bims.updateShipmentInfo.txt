@startuml
skinparam ParticipantPadding 30
skinparam sequence {
        ArrowColor #b40d63
        ParticipantBorderColor #E20074
ParticipantBackgroundColor white
        ParticipantFontSize 14
LifeLineBorderColor #b40d63
        LifeLineBackgroundColor white
  }
  
title <font size="18" color=#b40d63> Update Shipment Info</font>
actor client
participant "RSP"
database RSPDB
database RPCDB
participant TIBCO
participant DEEPIO
client ->RSP: OrderFulfillmentService.updateShipmentInfo

RSP<-RSPDB: Getting UniversalOrderId & orderCreationDate from RSPDB and check for IsNPIEnabledAndInFlightOrder.


group If notification exist in shipmentInfoUpdateRequest payload
activate "RSPDB"
RSP<-RSPDB: Getting Hso Order Id based on notification string.
RSP->RSPDB: Updating HsoOrderDetailId based on salesDocumentNumber in the ProcessOrderResponse data
deactivate "RSPDB"
end

group If notification is not exist in shipmentInfoUpdateRequest payload
RSP<-RSPDB: Get universalOrderId, partnerId, bulkorder & validationStatus
RSP->RSPDB: Update ShipmentInfo

group If Order Creation date is older than configured 'NOD.DEPLOY.DATE'
RSP->RSPDB: If isNPIEnabledAndInFlightOrder is true, update BulkOrderStatus with orderStatus as 'STATUS_READY_FOR_PROCESS' 
end

group  If configured 'NOD.DEPLOY.DATE is older than Order Creation date '
RSP<-RSPDB: Fetching CancelStatus and E-sign Date details EipDisclosureInfo table from RSP DB
end

RSP->RSP: If Cancel status is empty & e-signDate is present then eSignStatus as 'Signed' & orderStatus as 'Released' accordingly
RSP->RSP: If Cancel status is exist then eSignStatus & orderStatus setting as 'Cancelled'.
RSP->DEEPIO: If (Cancel status is exist or e-signDate is present) and (SalesOrderNumber is present in request but SIM & IMEI not present) then push to DEEP (UpdateOrderB2B)

group This event is created when the order is errored out after reaching SAP and as RSP gets an error message about the order failure (If errorMessage exist in request)
RSP<-RPCDB: Get PartnerProfile from RPCDB to check CreateOrderEventEnabled flag
RSP->TIBCO: If CreateOrderEventEnabled is true, then calling Tibco service(https://tugs-events-pub.internal.t-mobile.com:443/Event/Publish/OrderEvent) for creating order event
end

end

client<-RSP: Return Response
@enduml