@startuml
title SubscriberUpdateService.cancelSubscriber
actor Client

group <font color=#FF0090><b> cancelSubscriber
Client->HAProxy_APIX: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/SubscriberUpdateService.html#cancelSubscriber cancelSubscriber]]  SubscriberUpdateService.cancelSubscriber
HAProxy_APIX -> HAProxy_APIX :Authenticate and Authorize
HAProxy_APIX->SubscriberUpdateService: cancelSubscriber REQ
SubscriberUpdateService->SubscriberUpdateService: Validate REQ
alt <b><color:darkorange>IF cancelSubscriberRequest/SubscriberInfo size > 100</color>
loop <b><color:darkorange>outerSize</color>
SubscriberUpdateService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csCnclSub.html csCnclSub]]  Tuxedo_csCnclSub
end
note right 
int maxMsisdnsPerCall=100;
int outerSize = (int) Math.ceil(cancelSubscriberRequest.getSubscriberInfo().size() / (double) maxMsisdnsPerCall);
endnote 
else
SubscriberUpdateService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/csCnclSub.html csCnclSub]]  Tuxedo_csCnclSub 
end
SubscriberUpdateService->HAProxy_APIX: Returns service response
HAProxy_APIX->Client: cancelSubscriber response to client
end group

@enduml

