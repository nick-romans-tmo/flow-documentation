@startuml
title BillDetailsService.getAdjustments
actor Client

group <font color=#FF0090><b> getAdjustments
Client->HAProxy_APIX: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/BillDetailsService.html#getAdjustments getAdjustments]]  BillDetailsService.getAdjustments
HAProxy_APIX -> HAProxy_APIX :Authenticate and Authorize
HAProxy_APIX->BillDetailService: getAdjustments REQ 
BillDetailService->BillDetailService: Validate REQ
BillDetailService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evGbAdjHis.html evGbAdjHis]]  Tuxedo_evGbAdjHis
SAMSON->BillDetailService: Samson returns AdjustmentsResponse
BillDetailService->HAProxy_APIX: Returns service response
HAProxy_APIX->Client: getAdjustments response to client
end group

@enduml

