@startuml
title UpsQuantumJob.ExecuteUpsJob Service
actor Client

group <font color=#FF0090><b> executeUpsJob

group <b> Loop isLastPageReached = true()

TIBCO -> TIBCO : QuantumViewRequest

	group <b> IF First Page Retrieve
		TIBCO->SQLMGR: Call GetUPSJobInfoSQL
		SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetUPSJobConfigTable&action=query&keyValue= GetUPSJobConfigTable]]
		SQLMGR->TIBCO: UpsJobCollection
	end
	group <b> check if another job running already
		TIBCO -> TIBCO : send Mail "UPS job has not been started since the previous job is still running"
	end
	group <b> Otherwise
		group <b> IF First Page Run
			TIBCO -> TIBCO : UpsJobCollection  to CreateRequestObject
		end
		group <b> Otherwise
			TIBCO -> TIBCO : parse QuantumViewRequest 
		end
		TIBCO -> UPS : AccessRequest and QuantumViewRequest 
		UPS -> UPS : [[https://wwwcie.ups.com:443/ups.app/xml/QVEvents UPSQuantumView]]
		UPS -> TIBCO :QuantumViewResponse
		group <b> for each QuantumViewResponse/QuantumViewEvents/*/SubscriptionFile
			group <b> For each SubscriptionFile/*/Delivery
				TIBCO->SQLMGR: Call GetLastTrackingNumberEventSQL
				SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLastTrackingNumberEvent&action=query&keyValue= GetLastTrackingNumberEvent]]
				SQLMGR->TIBCO: CustomerEventCollection
				group <b> IF TrackingNumber
					TIBCO -> TIBCO_BE : OrderEventPublisher
				end
				group <b> OtherWise
					TIBCO -> TIBCO_BE : PublishtoREEF
				end
			end
			group <b> For each SubscriptionFile/*/Exception
				group <b> IF Attempted Delivery Desc found
					TIBCO->SQLMGR: Call GetLastTrackingNumberEventSQL
					SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLastTrackingNumberEvent&action=query&keyValue= GetLastTrackingNumberEvent]]
					SQLMGR->TIBCO: CustomerEventCollection
				end
				group <b> IF Tracking number found and Latest Event
					TIBCO -> TIBCO_BE : OrderEventPublisher
				end
				group <b> Otherwise
					TIBCO -> TIBCO_BE : PublishtoREEF
				end
			end
			group <b> IF processReturnInTransit='true' Loop on SubscriptionFile/*/Origin
				TIBCO -> SQLMGR : GetReturnTrackingNumberEventSQL
				SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetReturnTrackingNumberEvent&action=query&keyValue= GetReturnTrackingNumberEvent]]
				SQLMGR -> TIBCO : CustomerEventCollection
				group <b> IF ReturnInTransit Found
					TIBCO -> SQLMGR : GetLastTrackingNumberEventSQL
					SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLastTrackingNumberEvent&action=query&keyValue= GetLastTrackingNumberEvent]]
					SQLMGR->TIBCO: CustomerEventCollection
				end
				group <b> Check Duplicates
					TIBCO -> TIBCO_BE : OrderEventPublisher
				end
			end
			group <b> IF originScanEvent Loop on SubscriptionFile/*/Origin
					TIBCO -> SQLMGR : GetLastTrackingNumberEventSQL
					SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLastTrackingNumberEvent&action=query&keyValue= GetLastTrackingNumberEvent]]
					SQLMGR->TIBCO: CustomerEventCollection
					group <b> CreateEvent
						TIBCO -> TIBCO_BE : OrderEventPublisher
					end
			end
		end
		group <b> IF Last Page Reached
			TIBCO -> SQLMGR : MergeUPSJobInfoSQL[STATUS=success/failure]
			SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=MergeUPSJobInfo&action=query&keyValue= MergeUPSJobInfo]]
			SQLMGR -> TIBCO : sqlmgr response
		end
		group <b> OtherWise
			TIBCO -> SQLMGR : MergeUPSJobInfoSQL[STATUS=running]
			SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=MergeUPSJobInfo&action=query&keyValue= MergeUPSJobInfo]]
			SQLMGR -> TIBCO : sqlmgr response		
		end
end
end
group <font color=#FF8C00><b>  Error Response
UPS -> TIBCO : error
SQLMGR->TIBCO: error
end
end group
@enduml