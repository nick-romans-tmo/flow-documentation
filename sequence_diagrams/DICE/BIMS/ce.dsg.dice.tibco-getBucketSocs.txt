@startuml
title BillStatement.GetBucketSocs Service
actor Client

group <font color=#FF0090><b> getBucketSocs     
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-06-09/sid/html/BucketSocs.html#GetBucketSocs esiGetBucketSocRequest]]

TIBCO -> TIBCO :Input Validation[ApplicationID,OperatorID, ClientID are mandatory]
TIBCO -> TIBCO : esiGetBucketSocRequest to csiGetBucketSocRequest

TIBCO -> SAMSON: Call QueryLogicalDateTux
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/gnGtLgclDt.html gnGtLgclDt]]
SAMSON -> TIBCO : Logical Date
group <b> if SAMSON Error
TIBCO -> SQLMGR : Call getLogicalDateSQL
SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
SQLMGR -> TIBCO : Logical Date 
end
TIBCO ->SAMSON : GetSocBucketListTux
SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/evLsBktSocs.html evLsBktSocs]]


TIBCO -> TIBCO : csiGetBucketSocResponse to esiGetBucketSocResponse
TIBCO -> Client : esiGetBucketSocResponse

group <font color=#FF8C00><b>  Error Response
SQLMGR -> TIBCO :error
SAMSON -> TIBCO :error
TIBCO -> Client :error response
end
end
@enduml