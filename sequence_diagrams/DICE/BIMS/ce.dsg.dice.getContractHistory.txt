@startuml
title SubscriberContractService.getContractHistory
actor Client

group <font color=#FF0090><b> getContractHistory
Client->HAProxy_APIX: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/SubscriberContractService.html#getContractHistory getContractHistory]]  SubscriberContractService.getContractHistory
HAProxy_APIX -> HAProxy_APIX :Authenticate and Authorize
HAProxy_APIX->SubscriberContractService: getContractHistory REQ
SubscriberContractService->SubscriberContractService: Validate REQ
SubscriberContractService->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/196/services/evGbCnrtHist.html evGbCnrtHist]]  Tuxedo_evGbCnrtHist 
SAMSON->SubscriberContractService: Samson returns Contract History details 
SubscriberContractService->HAProxy_APIX: Returns service response
HAProxy_APIX->Client: getContractHistory response to client
end group

@enduml

