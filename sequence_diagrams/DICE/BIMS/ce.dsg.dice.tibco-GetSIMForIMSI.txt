@startuml
title PublicQuerySIM.GetSIMForIMSI Service
actor Client

group <font color=#FF0090><b> GetSIMForIMSI     
Client->TIBCO:[[hhttps://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-06-09/sid/html/PublicQuerySIM.html#GetSIMForIMSI PublicGetSIMInput]] 
TIBCO -> TIBCO : Validate Input [IMSI Is Required]
TIBCO -> TIBCO : PublicGetSIMInput to SamsonGetSIMInput

group <b> if cbStatus="OFF"
	TIBCO ->SQLMGR: IMSI
	SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=getSIMForIMSI&action=query&keyValue= getSIMForIMSI]]
	SQLMGR -> TIBCO : SIM
end

TIBCO -> TIBCO : SamsonGetSIMOutput
TIBCO -> Client : PublicGetSIMOutput

group <font color=#FF8C00><b>  Error Response
SQLMGR -> TIBCO :error
TIBCO -> Client :error response
end
end
@enduml

