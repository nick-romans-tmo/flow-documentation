@startuml

autonumber
Client-> RSP:-[[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/OrderFulfillmentService.html#getOrderStatus getOrderStatus]]OrderFulFillment.getOrderStatus

RSP->RSPDB:- OrderFulFillment.getOrderStatus Get Bulk Order Details based on Universal Order Id
alt When Order contains EIP_PLAN_ID 
RSP->RSPDB:- Call EIP_DISCLOSURE_INFO to get EIP Status
end

alt When Order contains SAP_ORDER_ID
RSP [#5B2C6F]->SAPPI: Call SAPPI SAPPI_miGetOrderStatus to get Order status
end

alt When Order contains SAMSON_ORDER_ID 
RSP [#5B2C6F]->SAMSON: [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/evGbCopOrdInf.html evGbCopOrdInf]]  Call SAMSON Tuxedo_evGbCopOrdInf 
end


alt When Order contains HSO ORDER ID 
RSP [#5B2C6F]->HSO: [[http://qatwshsox531.gsm1900.org:7543/QueryOrderHistory.asmx GetOrderDetails]]  Call Call HSO service GetOrderDetails 
end

RSP-> Client:- OrderFulFillment.getOrderStatus Response

@enduml