@startuml
database RSPDB
autonumber
RSP->RSPDB: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/OrderFulfillmentService.html#processOrder processOrder]]OrderFulFillment.processOrder job picks all the order with status "Ready to Process"
loop each Order picked by Process
alt When Order contains EIP_PLAN_ID 
RSP->EIP:- [[https://servicecatalog.internal.t-mobile.com/servicecat/eip/2018-05-15/wsdl_api/html/CreateEIP.html createEipPlan]]  Eip_createEIPPlanDetails get EIPPlanDetails call Eip_createEIPPlanDetails
EIP->RSP:- EIP posts EIPPlanDetails to RSP
end
group When ORDER TYPE is ACTIVATION,ACCESSORIES,HANDSET,HANDSET_ACCESSORIES
group When MANUAL_ACTIVATION FLAG is set to TRUE for Orders
RSP->SAP:-Post the Order Details to SAP for Activation
SAP->STARTEK: SAP in turn invokes STARTEK and passes necessary order info
STARTEK->SAMSON:- [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/evCrVarActOrd.html evCrVarActOrd]]  Tuxedo_evCrVarActOrd STARTEK invokes SAMSON for Activation
SAP->RSP: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/OrderFulfillmentService.html#updateShipmentInfo updateShipmentInfo]] SAP posts SAP ORDER ID to RSP
RSP->RSPDB:Update SAP_ORDER_ID in b2b_bulk_order_details table


SAP->RSP: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/OrderFulfillmentService.html#updateShipmentInfo updateShipmentInfo]] SAP posts IMEI & SIM to RSP
RSP->RSPDB:Updates SIM & IMEI in b2b_bulk_order_details table
end
group MANUAL_ACTIVATION FLAG is set to FALSE
alt When Order Status "Ready to Process" & Not Validated by SAMSON
autonumber 4
RSP [#5B2C6F]->SAMSON: [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/evCrVarActOrd.html evCrVarActOrd]]  Tuxedo_evCrVarActOrd Post the Order details to Samson for validations
RSP [#5B2C6F]->RSPDB: Updates Order status to "Submitted to SAMSON" in b2b_bulk_order_details
SAMSON [#5B2C6F]->RSP: Samson Ack with SAMSON ORDER ID 
RSP [#5B2C6F]->RSPDB: Samson Order Id is updated in b2b_bulk_order_details and response payload in b2b_channel_order table.
SAMSON [#5B2C6F]->RSP: Samson posts the confirmation on validation complete to RSP 
RSP [#5B2C6F]->SAMSON: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-02-24/sid/html/RatePlanManagementService.html#notifyServiceChangeResult notifyServiceChange]]RPM.notifyServiceChange app is invoked by asyncListener and in turn invokes SAMSON and pulls Order Validation Status
RSP [#5B2C6F]->RSPDB: RPM.notifyServiceChange Updates the validation status in b2b_bulk_order_details and updates Order status as "Ready to Process"
else When Order Status "Ready to Process" and Validated by SAMSON & Not Submitted to SAP
alt When ACTIVATION is FAILURE
RSP [#5B2C6F]->RSP: MANUAL_ACTIVATION flow.
else When ACTIVATION is SUCCESS
autonumber 11
RSP [#5B2C6F]->SAP: Posts Order details to SAP
RSP [#5B2C6F]->RSPDB: Updates status to "Submitted to SAP".
SAP [#5B2C6F]->RSP: SAP Acks back
RSP [#5B2C6F]->RSPDB: SAP response stored in b2b_channel_order
SAP [#5B2C6F]->RSP: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2017-02-25/wsdl_api/html/OrderFulfillmentService.html#updateShipmentInfo updateShipmentInfo]] Posts SAP Order Id
RSP [#5B2C6F]->RSPDB: Updates SAP Order ID in b2b_bulk_order_details
SAP [#5B2C6F]->RSP: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/OrderFulfillmentService.html#updateShipmentInfo updateShipmentInfo]] posts SIM & IMEI
RSP [#5B2C6F]->RSPDB: Updates SIM & IMEI in b2b_bulk_order_details & Updates Order Status to "Ready to Process"
else When Order Status is "Ready to Process" & SIM & IMEI exists in b2b_bulk_order_details table
autonumber 11
RSP [#5B2C6F]->SAMSON: [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/195/services/evCrVarActOrd.html evCrVarActOrd]]  Tuxedo_evCrVarActOrd Submits SIM & IMEI to SAMSON for Activation
SAMSON [#5B2C6F]->RSP: Samson ACKs back
RSP [#5B2C6F]->RSPDB: Ack payload is stored in RSPDB & Order status to "Submitted to Samson"
SAMSON [#5B2C6F]->RSP: Invokes RPM.asyncNotifyServiceChange with status of Activation.
RSP [#5B2C6F]->SAMSON: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-02-24/sid/html/RatePlanManagementService.html#notifyServiceChangeResult notifyServiceChange]]RPM.notifyServiceChange  sync process pulls the MSISDN from SAMSON
RSP [#5B2C6F]->RSPDB: Updates MSISDN in b2b_bulk_order_details
RSP [#5B2C6F]->SAP: Calling SAP to update Msisdn.
RSP [#5B2C6F]->RSPDB: SAP response stored in b2b_channel_order
RSP [#5B2C6F]->RSPDB:- Updates Order status to "SUBMITTED_TO_SAPPI" in b2b_bulk_order_details

alt When Bulk_Order_Details contains EquipmentDetailId  
RSP->EIP:- [[https://servicecatalog.internal.t-mobile.com/servicecat/eip/2018-05-15/wsdl_api/html/UpdateEquipment.html UpdateEquipment]]  EIP_updateEquipmentAsync call EIP to UpdateEquipment with MSISDN 
RSP [#5B2C6F]->RSPDB:- Updates Order status to "SUBMITTED_TO_EIP" in b2b_bulk_order_details
end


end
end
end

end
note right
If ACTIVATION_TYPE="PORT_IN" works as per the existing Functionality.
endnote 

group When ORDER TYPE is ACTIVATION_SIM_ONLY
autonumber 4
RSP [#5B2C6F]->SAMSON: Submits SIM & IMEI to SAMSON for Activation
SAMSON [#5B2C6F]->RSP: Samson ACKs back
RSP [#5B2C6F]->RSPDB: Ack payload is stored in RSPDB & Order status to "Submitted to Samson"
RSP [#5B2C6F]->RSPDB: Samson Order Id is updated in b2b_bulk_order_details and response payload in b2b_channel_order table.

alt When Order with ACCESSORY 

RSP [#5B2C6F]->SAP: Posts Order details to SAP
RSP [#5B2C6F]->RSPDB: Updates status to "Submitted to SAP".
SAP [#5B2C6F]->RSP: SAP Acks back
RSP [#5B2C6F]->RSPDB: SAP response stored in b2b_channel_order
SAP->RSP: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/OrderFulfillmentService.html#updateShipmentInfo updateShipmentInfo]] SAP posts SAP ORDER ID to RSP
RSP->RSPDB:Update SAP_ORDER_ID in b2b_bulk_order_details table


end

end

group When ORDER TYPE is RETURN
autonumber 4
RSP->SAP:-Post the Order Details to SAP for Return 

RSP [#5B2C6F]->RSPDB: Updates status to "Submitted to SAP".
SAP [#5B2C6F]->RSP: SAP Acks back
RSP [#5B2C6F]->RSPDB: SAP response stored in b2b_channel_order
SAP->RSP: [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-08-25/sid/html/OrderFulfillmentService.html#updateShipmentInfo updateShipmentInfo]] SAP posts SAP ORDER ID to RSP
RSP->RSPDB:Update SAP_ORDER_ID in b2b_bulk_order_details table

end
end
@enduml