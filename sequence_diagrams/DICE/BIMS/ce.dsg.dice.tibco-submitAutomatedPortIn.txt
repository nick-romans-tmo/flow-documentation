@startuml
title PortableAssetService.SubmitAutomatedPortIn Service
actor Client

group <font color=#FF0090><b> submitAutomatedPortIn
Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-11-04/sid/html/PortableAsset.html#SubmitAutomatedPortIn submitAutomatedPortInRequest]]
TIBCO -> TIBCO : submitAutomatedPortInRequest to csiSubmitAutomatedPortInRequest

TIBCO -> TIBCO : csiSubmitAutomatedPortInResponse to submitAutomatedPortInResponse
TIBCO -> Client : submitAutomatedPortInResponse

group <b> GetLogicalDate
	TIBCO -> SAMSON: Call QueryLogicalDateTux
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/gnGtLgclDt.html gnGtLgclDt]]
	SAMSON -> TIBCO : Logical Date
	group <b> if SAMSON Error
	TIBCO -> SQLMGR : Call getLogicalDateSQL
	SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
	SQLMGR -> TIBCO : Logical Date 
	end
end
group <b> If Non INTRA
	group <b> getCarrier
		TIBCO -> SAMSON: applicationID, logialDate, opratorID
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/rfLsCarrInfo.html rfLsCarrInfo]]
		SAMSON -> TIBCO : CarrierInfo
	end
	group <b> CheckPortInDueDateTux
		TIBCO -> SAMSON: CheckPortInDueDate request
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csChkDueDate.html csChkDueDate]]
		SAMSON -> TIBCO : DueDate response		
	end
end
group <b> SubmitAutomatedPortInTux
	TIBCO -> TIBCO : GetLogicalDate
	TIBCO -> SAMSON: csiSubmitAutomatedPortInRequest
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csInAutoPort.html csInAutoPort]]
	SAMSON -> TIBCO : portIn response
end
group <font color=#FF8C00><b>  Error Response
SAMSON->TIBCO: error
SQLMGR->TIBCO: error
TIBCO->Client: Error Response
end
end group
@enduml