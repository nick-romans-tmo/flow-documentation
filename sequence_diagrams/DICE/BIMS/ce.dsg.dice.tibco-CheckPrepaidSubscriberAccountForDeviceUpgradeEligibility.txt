@startuml
title CheckPrepaidSubscriberService.CheckPrepaidSubscriberAccountForDeviceUpgradeEligibility
actor Client
group <font color=#FF0090><b> checkPrepaidSubscriberAccountForDeviceUpgradeEligibility
Client->TIBCO : [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-11-04/sid/html/CheckPrepaidSubscriberService.html#CheckPrepaidSubscriberAccountForDeviceUpgradeEligibility  CheckPrepaidSubscriberAccountForDeviceUpgradeEligibility]] CheckPrepaidSubscriberService.CheckPrepaidSubscriberAccountForDeviceUpgradeEligibility

TIBCO -> SQLMGR : Call GetPrepaidSocsSQL
SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetPrepaidSocs&action=query&keyValue=  GetPrepaidSocs]]
SQLMGR -> TIBCO : Call Returns Prepaid Socs. 

TIBCO->Client : CheckPrepaidSubscriberAccountForDeviceUpgradeEligibility response to client
end group
@enduml