@startuml
title <size:15>__Capability Memo__</size>
actor Client
participant APIGEE
participant CapabilityMemo_API
participant RabbitMQ
participant Samson

Client-> APIGEE: createMemo (/eventmanagement/v2/events or /billing/v1/memos)
APIGEE-> APIGEE: validation
APIGEE-> CapabilityMemo_API: createMemo (/eventmanagement/v2/events or /billing/v1/memos)
CapabilityMemo_API-> CapabilityMemo_API: validation
CapabilityMemo_API-> RabbitMQ: Push to queue(CAPABILITY.MEMO.EVENTS.ASYNC or CAPABILITY.MEMO.ASYNC)
RabbitMQ->CapabilityMemo_API: Return
CapabilityMemo_API->APIGEE: Return
APIGEE->Client: Success

CapabilityMemo_API-> CapabilityMemo_API: Async Listener
CapabilityMemo_API-> RabbitMQ: processEvent(Listen from queue CAPABILITY.MEMO.ASYNC)
CapabilityMemo_API-> RabbitMQ: processMemo(Listen from queue CAPABILITY.MEMO.EVENTS.ASYNC)
group When getMemoSystemTxt ,getMemoSource and getMemoSource not Null
CapabilityMemo_API-> Samson: [[https://service-samson-qlab02.test.px-npe01.cf.t-mobile.com/v2/api-docs?group=mmInMemo mmInMemo]]
Samson->CapabilityMemo_API: Return Success
end

group When getMemoSystemTxt ,getMemoSource and getMemoSource Null
CapabilityMemo_API-> Samson: [[https://service-samson-qlab02.test.px-npe01.cf.t-mobile.com/v2/api-docs?group=evCrMemo evCrMemo]]
Samson->CapabilityMemo_API: Return Success
end
@enduml