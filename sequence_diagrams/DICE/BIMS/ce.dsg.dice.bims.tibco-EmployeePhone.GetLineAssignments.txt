@startuml
title EmployeePhone.GetLineAssignments Service
actor Client

group <font color=#FF0090><b> GetLineAssignments 
Client->TIBCO:[[getLineAssignmentsRequest_v1]] 

TIBCO -> TIBCO :Input Validation[pNumber is mandatory]
TIBCO -> TIBCO : getLineAssignmentsRequest_v1 to getLineAssignmentsRequest

TIBCO -> HSO: Call GetAccountsForLineAssignments

TIBCO -> TIBCO : getLineAssignmentsResponse to getLineAssignmentsResponse_v1
TIBCO -> Client : getLineAssignmentsResponse_v1

group <font color=#FF8C00><b>  Error Response
HSO -> TIBCO :error
TIBCO -> Client :error response
end
end
@enduml