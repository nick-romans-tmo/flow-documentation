@startuml
title ChangeMSISDN.ChangeMSISDNOperation
actor Client
group <font color=#FF0090><b> changeMSISDNOperation
Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-11-04/sid/html/ChangeMSISDN.html#ChangeMSISDNOperation ChangeMSISDNRequest]]

TIBCO -> TIBCO :esiChangeMSISDNRequest to csiChangeMSISDNRequest

group <b> If ChangeMSISDNRequest has portInFlag = "Y" 
	TIBCO -> TIBCO : SamsonCheckPortEligibilityTarASF
end
group <b> Otherwise|INTER[portInResponse/reason != ("TM","MT")] ChangeMSISDNSvc
	group <b> If portInFlag = "Y" 
		group If portIn First?
			TIBCO -> TIBCO : SubmitPortInOrch
		end
		group <b> If Eligible
			TIBCO -> TIBCO : getNL, GetDynamicDescTux
			TIBCO ->TIBCO : changeMSISDN, SamsonChangeMsisdnTarASF[isPorting=true]
		end
	end
	group <b> Otherwise
		TIBCO -> TIBCO : SamsonChangeMsisdnTarASF[isPorting=false]
		TIBCO -> TIBCO : SamsonGetBanDetailsTarASF
		TIBCO -> TIBCO : GetPendingChargesTux
	end
end

group <b> SubmitPortInOrch
	TIBCO -> TIBCO :GetLogicalDate
	TIBCO -> TIBCO : SamsonCheckPortEligibilityTarASF
	group <b> IF eligible and has phoneDetails
		TIBCO -> TIBCO : getCarrier,GetCarrierInfoTux
		group <b> If No BAN
			TIBCO -> TIBCO : SamsonCreateAccountTarASF
		end
		TIBCO -> TIBCO : CheckPortInDueDateTux
		TIBCO -> TIBCO : SubmitPortInReqTux
	end
end 
group <b> SamsonCheckPortEligibilityTarASF
	TIBCO -> TIBCO : GetLogicalDate
	TIBCO->SAMSON:  Call WLNPRequest,GetLogicalDate
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csChkPortIn.html csChkPortIn ]]
	SAMSON->TIBCO: WLNPRequest
end
group <b> GetCarrierInfoTux
	TIBCO -> SAMSON:  GetCarrierInfoRequest
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/rfLsCarrInfo.html rfLsCarrInfo ]]
	SAMSON->TIBCO: CarrierInfo
end
group <b> SamsonCreateAccountTarASF
	TIBCO -> SAMSON:  CreateAccountRequest
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/arInCstBan.html arInCstBan ]]
	SAMSON->TIBCO: Samson Response
	group <b> IF WaiveCollections
		TIBCO -> SAMSON:  UpdateCollectionWaiverInfo
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/arUpBan.html arUpBan ]]
		SAMSON->TIBCO: Samson Response
	end
	TIBCO -> TIBCO : BAN from arInCstBanTux
end
group <b> CheckPortInDueDateTux
	TIBCO -> SAMSON : CheckDueDate Request
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csChkDueDate.html csChkDueDate]]
	SAMSON -> TIBCO : validDueDateTime,dateChangedInd
end
group <b> SubmitPortInReqTux
	TIBCO -> SAMSON : getBanDetails
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/arGtCstBan.html arGtCstBan]]
	SAMSON -> TIBCO : Samson response
	TIBCO -> SAMSON : csInPortReq
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csInPortReq.html csInPortReq]]
	SAMSON -> TIBCO : Samson response	
end
group <b> GetDynamicDescTux
	TIBCO -> SAMSON : GetDynamicDesc Request
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/rfGtDynDesc.html rfGtDynDesc]]
	SAMSON -> TIBCO : Samson response
end
group <b> SamsonGetBanDetailsTarASF
	TIBCO -> SAMSON : GetBanDetailsRequest,GetLogicalDate
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/arGtCstBanExt.html arGtCstBanExt]]or[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/arGtCstBan.html arGtCstBan]]
	SAMSON -> TIBCO : Samson response
end
group <b> GetPendingChargesTux
	TIBCO -> SAMSON : GetDynamicDesc Request
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/blLsPndCrg.html blLsPndCrg]]
	SAMSON -> TIBCO : charges	
end
group <b> SamsonChangeMsisdnTarASF
	TIBCO -> TIBCO : GetLogicalDate
	group <b> IF isPorting = false
		TIBCO->SQLMGR: ChangeMsisdnInput
		SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetSubscriberDetails_Dual&action=query&keyValue= GetSubscriberDetails]]
		SQLMGR->TIBCO: ListOfSubscriber	
			group <b> If exists NEW_MSISDN
				TIBCO->SQLMGR: SubscriberNo
				SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetSubscriberInventory&action=query&keyValue= GetSubscriberInventory]]
				SQLMGR->TIBCO: Ctn 				
			end
			TIBCO -> TIBCO : Validate InputRequest
	end
	TIBCO -> SAMSON : GetDynamicDesc Request
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csGtSub.html csGtSub]]
	SAMSON -> TIBCO : Samson response
	TIBCO -> TIBCO : SamsonQueryServiceAgreementABP
	TIBCO->SQLMGR: SamsonQueryVoiceMailFeature
	SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetServiceFeature&action=query&keyValue= GetServiceFeature]]
	SQLMGR->TIBCO: ListOfServiceFeature
	TIBCO->SQLMGR: SamsonQueryPhysicalDeviceTarASF
	SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetPhysicalDeviceByBanMsisdn&action=query&keyValue= GetPhysicalDeviceByBanMsisdn]]
	SQLMGR->TIBCO: ListOfPhysicalDevice
	group <b> reserve MSISDN and ChangePhone 
		group <b> ReserveSub?
			TIBCO -> SAMSON : getBanDetails
			SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/arGtCstBan.html arGtCstBan]]
			SAMSON -> TIBCO : Samson response		
			TIBCO -> SAMSON : ReserveSubscriber Request
			SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csRsvSub.html csRsvSub]]
			SAMSON -> TIBCO : Samson response	
		end
		group <b> REASON_CODE !="PR"
			TIBCO -> SAMSON : TIBCO Request
			SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csGtOcAmt.html csGtOcAmt]]
			SAMSON -> TIBCO : Samson response	
		end
		TIBCO -> SAMSON : getBanDetails
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/arGtCstBan.html arGtCstBan]]
		SAMSON -> TIBCO : Samson response
		TIBCO -> SAMSON : ChangePhone Request
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csChgSub.html csChgSub]]
		SAMSON -> TIBCO : Samson response
		group <font color=#FF8C00><b> ON Error
			TIBCO -> SAMSON : getBanDetails
			SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/arGtCstBan.html arGtCstBan]]
			SAMSON -> TIBCO : Samson response		
			TIBCO -> SAMSON : releaseMsisdn Request
			SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/csRelsSub.html csRelsSub]]
			SAMSON -> TIBCO : Samson response
		end
	end
end
group <b> SamsonQueryServiceAgreementABP
	group <b> If no effectiveDate
		TIBCO -> TIBCO : GetLogicalDate
	end
	group <b> SamsonGetServiceAgreement
		TIBCO -> SAMSON : GetServiceAgreement Request
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/evLsSubSrvSum.html evLsSubSrvSum]]
		SAMSON -> TIBCO : Samson response		
	end
	group <b> IF Device?
		group <b> If PriceplanorSocExists
			group <b> exist socInd?
				TIBCO -> WPC : Web2GoDeviceServiceRequest
				WPC -> WPC : [[|http://pcw.t-mobile.com/ProductLookup/GetWeb2GoDeviceServiceCompatibility ProductLookup.GetWeb2GoDeviceServiceCompatibility]]
				WPC -> TIBCO : Web2GoDeviceServiceResponse 
			end
			group <b> Otherwise
				TIBCO -> WPC : GetCompatibleServicesForRatePlanAndDevice
				WPC -> WPC : [[|http://pcw.t-mobile.com/ProductLookup/GetCompatibleServicesForRatePlanAndDevice ProductLookup.GetCompatibleServicesForRatePlanAndDevice]]
				WPC -> TIBCO : GetCompatibleServicesForRatePlanAndDeviceResponse 				
			end
		end
	end
	group <b> PopulateUsageRate?
		TIBCO->SQLMGR: SamsonQueryUsageRateTarASF
		SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetUsageRate&action=query&keyValue= GetUsageRate]]
		SQLMGR->TIBCO: ListOfUsageRate	
	end
	group <b> bucket?
		TIBCO -> TIBCO : ListOfUsageBucketLookup
	end
	group <b> PopulateVoiceMail
		TIBCO->SQLMGR: SamsonQueryVoiceMailFeature
		SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetServiceFeature&action=query&keyValue= GetServiceFeature]]
		SQLMGR->TIBCO: ListOfServiceFeature	
	end
	group <b> PopulateUsageBucket?
		TIBCO->SQLMGR: SamsonQueryUsageBucketTarASF
		SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetUsageBuckets&action=query&keyValue= GetUsageBuckets]]
		SQLMGR->TIBCO: UsageBucket	
		TIBCO -> TIBCO : SamsonCalculateBucketSummary
	end
	TIBCO->SQLMGR: SamsonQueryRatedFeatureTarASF
	SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetRatedFeature&action=query&keyValue= GetRatedFeature]]
	SQLMGR->TIBCO: ListOfRatedFeature
	group <b> IF PopulateUsageBucket, SamsonCalculateBucketSummary
		TIBCO -> JAVA : CalculateBucketMinutes
	end
end

group <b> Logical Date
	TIBCO->SAMSON: CallLogicalDate
	SAMSON->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/gnGtLgclDt.html gnGtLgclDt]]
	SAMSON->TIBCO: LogicalDate
	group <b> SAMSON Error
		TIBCO->SQLMGR: Call GetLogicalDateSQL
		SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
		SQLMGR->TIBCO: LogicalDate 
	end
end

TIBCO -> TIBCO : csiChangeMSISDNResponse to esiChangeMSISDNResponse
TIBCO->Client: TIBCO response to client


group <font color=#FF8C00><b>  Error Response
JAVA -> TIBCO :error
WPC -> TIBCO :error
SQLMGR -> TIBCO :error
SAMSON -> TIBCO :error
TIBCO -> Client :error response
end
end group
@enduml