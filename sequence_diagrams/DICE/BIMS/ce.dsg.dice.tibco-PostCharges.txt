@startuml
title ChargeService.PostCharges
actor Client
group <font color=#FF0090><b> PostCharges
Client->TIBCO : [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-11-04/sid/html/ChargeService.html#PostCharges  PostCharges]] ChargeService.PostCharges
TIBCO->TIBCO :esiPostChargesRequest to csiPostChargesRequest 
TIBCO -> SAMSON : Call QueryLogicalDateTux
SAMSON ->SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/gnGtLgclDt.html  gnGtLgclDt]]
SAMSON -> TIBCO : Returns Logical Date.
group <b> if SAMSON Error
TIBCO -> SQLMGR : Call getLogicalDateSQL
SQLMGR -> SQLMGR : [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue=  GetLogicalDate]]
SQLMGR -> TIBCO : Call Returns Logical Date. 
end group
TIBCO -> SAMSON : Call SamsonPostAccountChargesTarASF
SAMSON ->SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/arGtCstBan.html  arGtCstBan]]
SAMSON -> TIBCO : Returns the BAN Details.
TIBCO->TIBCO : csiPostChargesResponse to esiPostChargesResponse
TIBCO->Client : PostCharges response to client
end group
@enduml