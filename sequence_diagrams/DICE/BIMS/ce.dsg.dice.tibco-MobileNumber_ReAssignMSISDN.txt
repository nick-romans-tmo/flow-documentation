@startuml
title MobileNumber.ReAssignMSISDN
actor Client
group <font color=#FF0090><b> reAssignMSISDN
Client->TIBCO: [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-11-04/sid/html/MobileNumber.html#ReAssignMSISDN esiReassignMSISDNRequest]]

TIBCO -> TIBCO : Validate MSISDN, OperatorID
TIBCO -> TIBCO : esiReassignMSISDNRequest to csiReassignMSISDNResponse
	TIBCO -> SAMSON : GetNumberMgmtTux Request
	SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/csGtSubInv.html csGtSubInv]]
	SAMSON -> TIBCO : Samson QueryCtnOutput response
	
	group <b> If QueryCtnOutput/statusData/statusCode='S' 
		TIBCO -> SAMSON : MobileNumberMgmtInput
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/pnmImAoage.html pnmImAoage]]
		SAMSON -> TIBCO : Samson QueryCtnOutput response	
	
	end
	group <b> If isReassignNL="true" 
		TIBCO -> SAMSON : MobileNumberMgmtInput
		SAMSON -> SAMSON : [[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/203/services/pnmImDReas.html pnmImDReas]]
		SAMSON -> TIBCO : Samson response	
	
	end	
	
group <b> Logical Date
	TIBCO->SAMSON: CallLogicalDate
	SAMSON->SAMSON:[[http://devsam31.unix.gsm1900.org:7020/WebAppTuxApi/198/services/gnGtLgclDt.html gnGtLgclDt]]
	SAMSON->TIBCO: LogicalDate
	group <b> SAMSON Error
		TIBCO->SQLMGR: Call GetLogicalDateSQL
		SQLMGR->SQLMGR: [[http://qattbco534.unix.gsm1900.org:8101/sqlloader/displaySQL.do?qName=GetLogicalDate&action=query&keyValue= GetLogicalDate]]
		SQLMGR->TIBCO: LogicalDate 
	end
end

TIBCO -> TIBCO : csiReassignMSISDNResponse
TIBCO -> Client: esiReassignMSISDNResponse

end group
@enduml