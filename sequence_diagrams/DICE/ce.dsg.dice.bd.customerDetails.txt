@startuml
Client->TIBCO:[[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2017-12-09/wsdl_api/html/CustomerService.html#getSubscriberInfo Subscriber Info]]CustomerService.getSubscriberInfo-Retrieve customer details-Customer status,Type, Bill, Address, preferences
TIBCO->RSP:PostPaidAccount.getBasicAccountDetails-Retrieve customer details per ban
alt Subscriber is available 
TIBCO->RSP: Retrieve customer details per ban and MSISDN-PostPaidSubscriber.getSubscribersInfo
end
TIBCO->SAMSON:evGtAddPrm-Retrieve Block Ind
TIBCO->Client:Customer Details retrieved
@enduml