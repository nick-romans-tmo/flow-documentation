
@startuml my service name - operation name
title AAL_EventService
SAMSON -> TIBCO : AccountActivityLogService
TIBCO -> Queue : sentAALUpdate
Queue -> CHUB : subscribeQueue
CHUB -> CHUB : processMessage
CHUB -> CHUB : validateBAN_TMOIdisCustomer
CHUB -> CHUB : EndIfNotBMCustomer
CHUB -> CHUB : WriteEventstoBankPUBEventTable
PCF -> CHUB : ScheduleJob(aaleventservice) 
CHUB -> PCF : RetreivePublishedEvents
PCF -> CHUB : lookupForBAN/TMoId
CHUB -> CHUB : count(numberOfLines[Active/Suspended])
CHUB -> CHUB : getPAH,RoleToSetPAHFlag
CHUB -> PCF : ReturnCustomerAccountLineInfo
PCF -> PCF : CheckIfPAHisSetORSingleLine
PCF -> SAMSON: if'PAH'NotSet; getBRP
SAMSON -> PCF: RetrieveBRPfromSAMSONtuxedoAPI
PCF -> PCF :SetPAHIfBRPMatchesMSISDN
PCF -> PCF : filterBasedOnPAH/non-PAH
PCF -> SAMSON: getSOCsinfo
SAMSON -> PCF: RetreiveSOCsinfo
PCF -> Deep.io : sentCustomerDetails 
Deep.io->BankMobile : Handler-customerServicesProcessor
@enduml
