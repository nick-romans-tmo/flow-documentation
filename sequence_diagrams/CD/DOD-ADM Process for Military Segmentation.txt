@startuml
 DOD->CHUB: ACTIVE_DUTY DAILY PROCESS
 CHUB->CHUB: Upload Data to CHUB
 note right of CHUB       
   For every record where BAN/CUSTOMER level ADM evaluates to 'Y'
   Call the AddIndicator Process to update VerificationResult = "true" at BAN level
 end note 
 CHUB[#0000FF]->DEEP.IO: [Async] IF VERIFICATIONRESULT = "true" Send affiliation Data to Deep.io for BAN/MSISDN 
"CONSUMERS(Billing/Notifications/Promotions)"[#0000FF]->DEEP.IO: [Async] Subscribe to affiliation Data from Deep.io
@enduml