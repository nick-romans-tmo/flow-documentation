@startuml
CLIENT->APIGEE: Get OAUTH Token
note left of CLIENT
MYTMO.com 
OTHER UI's /IHAPS
end note
Activate APIGEE #FFBBBB
APIGEE-->CLIENT: Receive OAUTH Token

CLIENT->APIGEE: Request - Inquire Affiliation Request
note left of CLIENT
inputs

customerAccountId
msisdn
affiliationType
end note
APIGEE->CHUB: Request - Inquire Affiliation Request
CHUB->CHUB: Lookup Customer verification information
CHUB->APIGEE: Response - Inquire Affiliation Request
note left of CLIENT
Outputs

customerAccountId
msisdn
affiliationType
affiliationSubType
sourceofAffiliation
verificationResult
--
verificationRequestId
--
uploadStatus
verificationCreationDate
verificationApprovalDate
verificationExpiryDate

If verificationRequestId is null then 
   Invoke Customer Verification Request
else 
    Invoke Customer Verification Document Upload
end note

APIGEE->CLIENT: Response - Inquire Affiliation Request
...

CLIENT->APIGEE: Customer Verification Request
note left of CLIENT 
inputs

APIGEE OAUTH Token, 
AFFILIATIONTYPE, AFFILIATIONSUBTYPE, BAN, MSISDN, EMAIL,
FIRST_NAME, LAST_NAME, BIRTH_DATE,STATUS_START_DATE,
RELATIONSHIP

MIDDLE_NAME, LAST_NAME, FULL_NAME, COMPANY_NAME, 
ID_NUMBER, JOB_TITLE , USERNAME, ADDRESS1, ADDRESS2, 
CITY, STATE, POSTAL_CODE, PHONE_NUMBER, SSN, SSN_LAST4, 
STATUS_START_DATE, SUFFIX, IP_ADDRESS
end note
Activate APIGEE #DarkSalmon
APIGEE->CHUB: Customer Verification Request
Activate CHUB
CHUB->Verification_Vendor:Customer Verification Request 
note left of Verification_Vendor
Get Customer Verification data
end note
Verification_Vendor-->CHUB:Customer Verification Response
CHUB-->APIGEE:Customer Verification Response
note left of CLIENT
Outputs
customerAccountId
msisdn
affiliationType
affiliationSubType
sourceofAffiliation
--
verificationRequestId
verificationResult
--
verificationCreationDate
verificationApprovalDate
verificationExpiryDate

If verificationResult is false then 
    Invoke Customer Verification Document Upload
end note

APIGEE-->CLIENT:Customer Verification Response
CHUB->CHUB: Store Customer verification information
CHUB[#0000FF]->DEEP.IO: [Async] IF VERIFICATIONRESULT = "true" Send Verification Data to Deep.io for BAN/MSISDN 
"CONSUMERS(Billing/Notifications/Promotions)"[#0000FF]->DEEP.IO: [Async] Subscribe to Verification Data from Deep.io
deActivate CHUB
...
...
CLIENT->APIGEE: Upload Verification Document
note left of CLIENT 
inputs

"APIGEE OAUTH Token"
verificationRequestId
customerAccountId
msisdn
affiliationType
affiliationSubType
files (1 or more file(s) to upload)
end note
'Activate APIGEE #DarkSalmon
APIGEE->CHUB: Upload Verification Document
Activate CHUB
CHUB->Verification_Vendor:Request Document Upload Token
Verification_Vendor->CHUB:Receive Document Upload Token
Alt if Token Received Successfully
CHUB->Verification_Vendor:Upload Verification Document
Verification_Vendor->CHUB: Document Upload Status
CHUB->CHUB: Store Customer Document Upload Status information
else if "Token not Received" or "Documet Upload Failed"
CHUB->APIGEE: Error: Request User to Retry
APIGEE->CLIENT: Error: Request User to Retry
else if document uploaded successfully
CHUB->APIGEE: Success: Document Uploaded Successfully
APIGEE->CLIENT: Success: Document Uploaded Successfully
end
...
alt if Document Review Passed
Verification_Vendor->Customer: Send Document Review Success Email to Customer
else if Document Review failed
Verification_Vendor->Customer: Send Document Review failure email to Customer with details and next steps
end
Verification_Vendor[#0000FF]->CHUB: on success/failure of Document Review. send original RequestId
CHUB->Verification_Vendor: Request Verification Enquiry (inputs RequestId) 
Verification_Vendor->CHUB: Response Verification Enquiry
CHUB->CHUB: Store Customer verification information (Result)
CHUB[#0000FF]->DEEP.IO: [Async] IF VERIFICATIONRESULT = "true" Send Verification Data to Deep.io for BAN/MSISDN 
"CONSUMERS(Billing/Notifications/Promotions)"[#0000FF]->DEEP.IO: [Async] Subscribe to Verification Data from Deep.io

deActivate APIGEE #DarkSalmon
deActivate APIGEE
@enduml