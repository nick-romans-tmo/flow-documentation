@startuml
participant SAPCare
participant Rebellion
alt SAPCare Flow
SAPCare->WSIL: "writeUnauthorizedCalls"
WSIL->APIGEE:/customer-identity/v1/unauthorized-calls/ POST
APIGEE->"UnauthorizedCallsService":/customer-identity/v1/unauthorized-calls/ POST
"UnauthorizedCallsService"->"CHUB UnauthorizedCallsUpdateService":writeUnauthorizedCallsRequest
"CHUB UnauthorizedCallsUpdateService"-->"UnauthorizedCallsService": writeUnauthorizedCallsResponse
"UnauthorizedCallsService"-->APIGEE:writeUnauthorizedCalls Response
APIGEE-->WSIL:writeUnauthorizedCalls Response
WSIL-->SAPCare: writeUnauthorizedCalls Response
"UnauthorizedCallsService"->"REEF EventEnterprise.createEvent":createEventRequest
"REEF EventEnterprise.createEvent"-->"UnauthorizedCallsService":createEventResponse
note left
Calling REEF EventEnterprise is Async
end note
end

alt Rebellion Flow
Rebellion->APIGEE:/customer-identity/v1/unauthorized-calls/ POST
APIGEE->"UnauthorizedCallsService":/customer-identity/v1/unauthorized-calls/ POST
"UnauthorizedCallsService"->"CHUB UnauthorizedCallsUpdateService":writeUnauthorizedCallsRequest
"CHUB UnauthorizedCallsUpdateService"-->"UnauthorizedCallsService": writeUnauthorizedCallsResponse
"UnauthorizedCallsService"-->APIGEE:writeUnauthorizedCalls Response
APIGEE-->Rebellion:writeUnauthorizedCalls Response
"UnauthorizedCallsService"->"REEF EventEnterprise.createEvent":createEventRequest
"REEF EventEnterprise.createEvent"-->"UnauthorizedCallsService":createEventResponse
note left
Calling REEF EventEnterprise is Async
end note
end
@enduml


