@startuml
CLIENT->APIGEE: Get Token
note left of CLIENT
Feeney (External)
AOP (External)
Venicom (External)
Sensitek (External)
Samsung (External)
ASOP
SFDC 
BOSS
BOSSi
COP
QV Retail
QV Care
QVXP Retail
Self-Care (Web)
SIVR
end note
Activate APIGEE #FFBBBB
APIGEE-->CLIENT: Receive Token
CLIENT->APIGEE: PinLookup Request
note left of APIGEE 
input Token,Ban 
end note
Activate APIGEE #DarkSalmon
APIGEE->CHUB: PinLookup Request
Activate CHUB
CHUB->CHUB: Lookup BAN/PIN details & Account Type/Subtype
note right of CHUB 
lookup acctype/subtype crossreference
configuration for pin management 
eligibility and if pin exists 
and is "numeric or alphanumeric" flags
end note
alt "if pin is not null" and "pin is not alphanumeric" and "pin length between 6 and 15"
CHUB->apigee:PinFormatValidation Request
apigee->SAMSON
note left of SAMSON 
Validate PIN Format
end note
SAMSON-->apigee:PinFormatValidation Response
apigee-->CHUB:PinFormatValidation Response
else Call to Samson Failed
CHUB->CHUB: Perform Pin Validation internally
end
CHUB-->APIGEE:PinLookup Response
deActivate CHUB
APIGEE-->CLIENT:PinLookup Response
deActivate APIGEE #DarkSalmon
deActivate APIGEE
@enduml