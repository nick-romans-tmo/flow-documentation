@startuml 
box "MyTMO Order Tracking and Reporting Dashboard" #LightGrey
    participant API_Consumer
    participant Jazz_AWS_API_Gateway
    participant Lambda
    participant DynamoDB
end box
 
alt GET event call
API_Consumer -> Jazz_AWS_API_Gateway: GET /api/order-flow/events
Jazz_AWS_API_Gateway -> Lambda: Trigger lambda to get order events
Lambda -> DynamoDB: GET all Order events where isComplete=false
DynamoDB --> Lambda: data/error
Lambda --> Jazz_AWS_API_Gateway: Return list of order events
Jazz_AWS_API_Gateway --> API_Consumer: HTTP 200 with List of Orders
end

@end