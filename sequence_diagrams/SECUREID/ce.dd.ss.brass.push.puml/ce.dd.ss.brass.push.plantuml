@startUML
autonumber
title PUSH Bio

actor "T-Mobile Customer" as sub
participant "App/ASDK" as app

participant "Push Client" as client
box "BRASS Services"
    participant "PUSH Service\n([[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dd.ss.brass/push_v1.json?at=refs%2Fheads%2Fsecureid_push swagger]])" as push 
    participant "Pushbroom Service" as pbs
    participant "BAS Service" as bas
end box 
participant IAM as iam

note over client, push
    Assumptions:
    * Subscriber has a valid push token registered in PBS
    * Subscriber has notifications enabled for T-Mobile App
    * Subscriber has biometrics enabled (IAM has tracked this)
end note

sub -> client : Customer invokes Client's services, which requires Push authentication

group Checking for PUSH eligibility
    note right of client
        POST /push/v1/check
        Authorization: Bearer mF_9.B5f-4.1JqM
        Content-Type: application/json
        {
            msisdn : 111
        }
    end note
    push <--> iam : Get IAM Profile, Checks if biometric is enabled for user's device, if PIN exists for user.
    push -> push : Add authentiction methods that user is eligible for into response body.
    
    alt If incorrect role
        push --> client : Error response, Bio PUSH cannot be sent for the provided user, with reason provided.
        note left of push
            Response:
            HTTP 400 Bad Request
            {
                "errorCode": "<Error Code>",
                "errorDescription": "<Detailed reason for bad request>"
            }
        end note
    end alt

    push <--> pbs : Verifies a valid push token exists for MSISDN provided
    alt If No Push Token exists
        push --> client : Error response, Bio PUSH cannot be sent for the provided user, with reason provided.
        note left of push
            Response:
            HTTP 400 Bad Request
            {
                "errorCode": "<Error Code>",
                "errorDescription": "<Detailed reason for bad request>"
            }
        end note
    end alt

    push -> client : 200 OK Response
    note left of push
        Response:
        HTTP 200 OK
        {
            msisdn : 111,
            authenticationMethods : ["bio", "pin"] //user is eligible for bio and pin.
        }
    end note

end

group Create PUSH request with user validating the request
client -> push : Send push request with Customer's MSISDN/Email
    note right of client
        POST /push/v1/create
        Authorization: Bearer mF_9.B5f-4.1JqM
        Content-Type: application/json
        {
            msisdn : 111
            noticeTitle : "Title Shown in Notification"
            noticeText : "Detailed message shown in notification",
            pushRequestorId : "IVR"
            authenticationOrder: ["bio"] //Client wants to perform bio push.
        }
    end note
    push <--> iam : Get IAM Profile, Checks if biometric is enabled for user's device, etc.
    alt If Bio is not enabled, incorrect roles, etc
        push --> client : Error response, Bio PUSH cannot be sent for the provided user, with reason provided.
        note left of push
            Response:
            HTTP 400 Bad Request
            {
                "errorCode": "<Error Code>",
                "errorDescription": "<Detailed reason for bad request>"
            }
        end note
    end alt

    push <--> pbs : Verifies a valid push token exists for MSISDN provided
    alt If No Push Token exists
        push --> client : Error response, Bio PUSH cannot be sent for the provided user, with reason provided.
        note left of push
            Response:
            HTTP 400 Bad Request
            {
                "errorCode": "<Error Code>",
                "errorDescription": "<Detailed reason for bad request>"
            }
        end note
    end alt

    push -> push : Creates record of the push request (Request ID) and persists this ID for future reference.
    push --> client : Responds with Request ID
    note left of push
        Response:
        HTTP 200 OK
        {
            msisdn : 111,
            requestId: "12dA2f7cD2fd21098D"
        }
    end note

    push -> app : PUSH sends push notification (via Google FCM)
    app -> sub : Notifies user of new notification
    sub -> app : Opens Notification message

    app -> push : Checks that PUSH Request ID is valid, retrieves txt1 and txt2.
    note right of app
        GET /push/v1/{request-id}
        authorization : DAT Token
        x-authorization : POP Token
    end note

    push --> app : Responds with request id details and status
    note left of push
        Response:
        HTTP 200 OK
        {
            "result": {
                "requestId": "12dA2f7cD2fd21098D",
                "status": "not_received",
                "notificationTitle": "string",
                "notificationText": "string"
            }
        }
    end note

    app -> sub : Displays Bio authentication screen
    sub --> app : Subscriber provides fingerprint
    app -> bas : Biometric verification
    bas --> app : Ack
    app --> sub : Ack
end

app -> push : Updates Push Request ID that authentication was approved.
note right of app
    POST /push/v1/update
    authorization : DAT Token
    x-authorization : POP Token
    {
        requestId : 111
        status : "approved"
    }
end note
push --> app : Ack
note left of push
    Response:
    HTTP 200 OK
    {
        "status": 200,
        "result": {
            "requestId": "U2FsdGVkX1$AES2B$zaGS5n$AES2B$C2C0XI1OE4SPlK",
            "status": "Approved"
        }
    }
end note

group Client Retrieves PUSH status
    client -> push : GET Request ID status
    note right of client
        GET /push/v1/{request-id}/status
        Authorization: Bearer mF_9.B5f-4.1JqM
    end note
    push --> client : Status of Push request
    note left of push
        Response:
        HTTP 200 OK
        {
            "status": 200,
            "result": {
                "requestId": "Request id.",
                "status": "Approved"
            }
        }
    end note
end

client --> sub : Client has PUSH authenticated subscriber.

@enduml