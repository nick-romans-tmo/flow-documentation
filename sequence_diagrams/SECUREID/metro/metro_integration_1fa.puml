@startUML
autonumber
title Metro Integration - First Factor for Edge

actor "Metro Customer" as cst
participant "Metro Dealer" as dlr
participant "Edge" as edg
participant "Apigee" as apg
box "BRASS Microservice"
    participant "New Microservice" as brs
    participant "TMS Microservice" as tms
end box
participant "IAM" as iam

note over edg, apg
    Notes:
     * Some Requests/Responses below are subject to change after design review.
     * There may be a lot more API calls happening between BRASS and IAM.
     * MDN = MSISDN = Phone Number
     * A new microservice will be created to handle first factor authentication.
end note
group Establishing Edge application token with Apigee
    edg -> apg : Register app with T-Mobile Apigee
    apg -> edg : Provides client id and client secret
    edg -> apg : GET /v1/oauth2/accesstoken?grant_type=client_credentials -H "authorization:Basic ..."
    apg --> edg : Response
    note left of apg
        Reference: https://developer.t-mobile.com/prod-reference
        Response:
        {
            "issued_at": "1492119111853",
            "expires_in": "3599",
            "token_type": "BearerToken",
            "access_token": "r4KHptxVij8Dkf8hkTkmMQF0xFRo",
            ...
        }

    end note
end

cst -> dlr : Customer visits dealer
dlr -> edg : Dealer logs in to edge, searches customer detals
dlr -> edg : Dealer successfully finds customer, invokes first factor authentication
edg -> apg : REST Call to APIGEE/auth/v1/authenticate/otp/send (provide access token, pop token + BRASS API parameters)

apg -> apg : Verifies access token, PoP token
alt If access token/PoP token validation fails
    apg --> dlr : HTTP 401 Unauthorized
end
apg -> brs : REST call to brass.account.t-mobile.com/auth/v1/authenticate/otp/send\n(Please see swagger for lower environment paths and API details)

brs -> brs : Verify request
alt If access token is invalid
    brs --> dlr : HTTP 401 Unauthorized
end
alt If missing required parameters, etc
    brs --> mtr : HTTP 4xx (Bad Request, etc)
end

brs -> iam : /authorize?login_hint=<MDN>
alt No TMOID linked to MDN
    iam --> brs : HTTP 200 OK, Action: Signup, OperatorId=4000 (Metro operator id)
    brs -> brs : Detects METRO MDN, return 400 error msisdn not metro, magenta, etc.
    brs -> iam : POST /linkmsisdn (using username "metro_<ban>")
    alt if iam profile not found
        brs -> iam : create profile
        iam --> brs : HTTP 200 OK
        brs -> iam : POST /linkmsisdn
    end
    iam --> brs : HTTP 200 OK, Responds with UUID.
    brs -> iam : /authorize?login_hint=<MDN>
end
iam --> brs : HTTP 200 OK, Action: Signin, auth_method=SMS_OTP, OperatorId=4000
alt TMOID exists, but is currently locked
    brs --> edg : Error, account is locked.
end

brs -> brs : Detect METRO operator id with signin and auth method SMS OTP
brs -> iam : POST /oauth2/v1/generatetemppin
note right of brs
    Request body:
    {
        "user_id":"tel:<MDN>", 
        "notification":“tel:<MDN>" //TODO from IAM - making this optional
        "strategy" : "reuse" //to account for any timing issues
    }
end note
alt Error encountered (ie Server Error)
    iam --> edg : HTTP 5xx (via BRASS, Apigee)
end
alt If IAM sends PIN via SMS
    iam --> cst : Delivers One Time Password to customer's MDN via SMS.
    iam --> edg : HTTP 200 OK (via BRASS, Apigee)
else If Metro requests for PIN returned
    iam --> brs : Responds with encrypted PIN.
    brs -> brs : Decrypts PIN
    brs -> edg : 200 OK, returns PIN
end

edg -> dlr : Prompts dealer for User's OTP sent via SMS.
note  over dlr, edg
    Dealer must now ask customer to provide the OTP sent via sms.
end note
cst -> dlr : Provides SMS OTP verbally
dlr -> edg : Submits OTP
edg -> apg : REST Call to APIGEE/auth/v1/authenticate/otp/verify<MDN> (provide access token, pop token + BRASS API parameters)
apg -> apg : Verifies access token, pop token
alt If access token is invalid
    apg --> dlr : HTTP 401 Unauthorized
end
apg -> brs : POST brass.account.t-mobile.com/auth/v1/authenticate/otp/verify\n(Please see swagger for lower environment paths and API details)
brs -> brs : Verify request
alt If access token is invalid
    brs --> dlr : HTTP 401 Unauthorized
end
alt If missing required parameters, etc
    brs --> mtr : HTTP 4xx (Bad Request, etc)
end

brs -> iam : POST /oauth2/v1/validate2ndfactoranswers
note right of brs
    There is some work on IAM to support context="authorize" but no iam_session_id is passed in the request.
    Request:
    {
        "user_id":"tel:<MDN>",
        "context":"authorize",  //value TBD
        "answer_type":"temp_pin"
        "answer": <OTP> encrypted by BRASS
    }
end note
iam -> iam : Validates OTP
alt Error encountered
    iam --> edg : HTTP 4xx/5xx (via BRASS, Apigee)
    edg -> dlr : Provides message of error, dealer can retry if applicable.
    note over brs, iam
        User will be locked out after five incorrect attempts
    end note
end
iam --> brs : HTTP 200 OK, Returns an IAM Session ID (TODO - Returning an IAM Session ID will need to change on IAM)
brs -> iam : GET /authorize (Passes IAM Session ID)

iam --> brs : HTTP 200 OK, responds with authorization code and iam session id.
brs -> tms : POST TMS/v3/token, provides authorization code and required oauth parameters.
tms -> tms : Generates id token
alt if high security
    tms --> brs : 200 OK, responds with loa0 id token
    note left of tms
        Example id token
        {
            "iat": 1556132646,
            "exp": 1556136245, //iat + 30 min
            "iss": "https://uat.brass.account.t-mobile.com",
            "aud": "MetroEdgePoC",
            "acr": "loa0metro" 
            "amr": [
                "sms_otp"
            ],
            "usn": "922f5542e3d10cd6"
            "dlr": "<dealer id>",
            "ctx" : <session iam session id>,
            "action": validate_2nd_factor
            "auth_methods": [
               "security_question"
            ]
        }
    end note
else if normal user
    tms --> brs : 200 OK, responds with id token
    note left of tms
        Example id token
        {
            "iat": 1556132646,
            "exp": 1556136245,
            "iss": "https://uat.brass.account.t-mobile.com",
            "aud": "MetroEdgePoC",
            "auth_time": 1556132513,"
            "AT": "01.USR.2HwMZu5qNxysYMNwX",
            "sub": "U-d6675d13-5eec-4e95-92e3-b84535998e0b",
            "acr": "loa2" /first factor
            "amr": [
                "sms_otp"
            ],
            "usn": "922f5542e3d10cd6"
            "dlr": "<dealer id>",
            "ctx" : <iam session id>
        }
    end note
end
brs --> edg : HTTP 200 OK, responds with id token
edg -> edg : Keeps track of id token
note over edg
    The id token shall only be used for the specific customer being validated and should be invalidated after the customer has been helped.
end note
edg -> dlr : Acknowledged OTP validation is successful.

@enduml