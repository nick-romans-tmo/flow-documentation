@startuml
    title Validate PUSH Pin API
    autonumber "<color:red><b>00 "
    skinparam backgroundColor #FFFFFF
    skinparam sequence {
        TitleFontSize 20
        ActorFontSize 12
        ParticipantFontSize 12
        ParticipantPadding 40
        ArrowFontSize 12
    }

    actor  "        User       " as USR
    participant "  EUI2-App-Services " as App_Services
    participant "   PUSH   " as PUSH
    participant  "   TMO_SERVICES    " as TMO_SERVICES
    participant  "   IAM    " as IAM
    participant  "   APIGEE    " as APIGEE

    USR -> App_Services: Invoke POST /svr/validatePushPin API
    note right of USR
        {
            "requestId": "U2FsdGVkX19VTQDAyObz",
            "pin": "123123",
            "remTransID": "PUSH_VERIFY_PIN",
            "status": "Approved"
        }
    end note

    == Validate Request ==

    alt If request does not contain requestId, pin, status
        App_Services -> USR:     <b> 400 Bad Request</b>
        note left of App_Services: { \n    "status": "400" \n    "apiResponse": { \n        "error": "invalid_request" \n        "error_description": "Uh-oh, it looks like we have \n           our wires crossed. Please try again later." \n     } \n }
    end

    == Middleware Invoke PUSH Search ==

    App_Services -> App_Services: Build Push Search request
    App_Services -> PUSH: Invoke push/v1/requestId (Push Search Service of PUSH micro service)

    alt If push record exists
        PUSH -> App_Services: <b> 200 OK </b>
        note left of PUSH: { \n     "status": "200", \n     "result": { \n     "status": "Approved", \n     "statusDescription": "This is just starting", \n     "authCode": "02.USR.TMlYJWe5ZD4yTS5l9", \n     "contextSessionId": "TestcontextSessionId" \n    }\n }
        App_Services -> App_Services: Invokes next middleware
    else If record not exists
        PUSH -> App_Services: <b> 404 Not Found </b>
        note left of PUSH: { \n     "status": "404", \n     "result": "Push Record not found", \n     "errorCode": "PUSH_RECORD_NOT_FOUND", \n     "errorDescription": "Push Record not found" \n }
        App_Services -> USR: Returns failure response
        note left of App_Services: { \n    "status": "400" \n    "apiResponse": { \n        "error": "invalid_request" \n        "error_description": "Uh-oh, it looks like we have \n           our wires crossed. Please try again later." \n     } \n }
    else If  isExpired is true
        PUSH -> App_Services: <b> 200 OK </b>
        note left of PUSH: { \n     "status": "200", \n     "result": { \n     "status": "Approved", \n     "statusDescription": "This is just starting", \n     "authCode": "02.USR.TMlYJWe5ZD4yTS5l9", \n     "contextSessionId": "TestContextSessionId" \n     "isExpired": "true" \n }\n }
        App_Services -> USR: <b> 400 Bad Request </b>
        note left of App_Services: { \n    "status": "400" \n    "apiResponse": { \n        "error": "invalid_request" \n        "error_description": "Something went wrong. We weren't \n          able to load this page. Please try again later." \n     } \n }
    end

    == Middleware Invoke Validate PUSH PIN & SAMSON ==

    App_Services -> TMO_SERVICES:  <b> POST TMOServices/rest/security/encrypt </b>
    alt If encryption fails
        TMO_SERVICES -> App_Services:  <b> 500 Internal Server Error </b>
        note right of App_Services: Unable to encrypt the given pass phrase
        ref over App_Services
            Send error response
        end ref
    else
        TMO_SERVICES -> App_Services:  <b> 200 OK </b>
        note right of App_Services: { "encryptedText": "{text: "..."}"}
        App_Services -> IAM:  <b> POST /oauth2/v2/validatepin </b>
        note right of App_Services: { \n    "remTransId": "..." \n    "pin": "encryptedPin" \n    "user_id": "user_id" \n }
        alt If validate with IAM success
            IAM -> App_Services:  <b> 200 OK </b>
            note right of App_Services: Validated pin calls next middleware
        else If PIN doesn't exists (User is PAH, Legacy & Postpaid) Calls Samson to Validate pin
            IAM -> App_Services:  <b> 400 If PIN with IAM fails </b>
            App_Services -> APIGEE:  <b> POST /customer/v1/auth </b>
            alt Success
            IAM -> App_Services:  <b> 200 OK </b>
            note right of App_Services: Validated pin calls next middleware
            else Invalid request
            APIGEE -> App_Services:  <b> 400 Bad Request </b>
            App_Services -> USR: <b> 400 Bad Request </b>
            note left of App_Services: { \n    "status": "400" \n    "apiResponse": { \n        "error": "invalid_request" \n        "error_description": "Uh-oh, it looks like we have \n           our wires crossed. Please try again later." \n     } \n }
        end
    end

    == Middleware Invoke PUSH Update Service to Update status ==

    App_Services -> App_Services: Build Push Search request
    App_Services -> PUSH: Invoke push/v1/update

    alt If push record updated
        PUSH -> App_Services: <b> 200 OK </b>
        note left of PUSH: { \n     "status": "200", \n     "result": { \n       "status": "Approved" }\n }
        App_Services -> USR: { \n     "statusCode": "200", \n     "apiResponse": { \n       "message": "Great! You're all set." }\n }
    else If update fails
        PUSH -> App_Services: <b> 404 Not Found </b>
        note left of PUSH: { \n     "status": "404", \n     "result": "Push Record not found", \n     "errorCode": "PUSH_RECORD_NOT_FOUND", \n     "errorDescription": "Push Record not found" \n }
        App_Services -> USR: Returns failure response
        note left of App_Services: { \n    "status": "500" \n    "apiResponse": { \n        "error": "server_error" \n        "error_description": "Uh-oh, it looks like we have \n           our wires crossed. Please try again later." \n     } \n }
    end

@enduml