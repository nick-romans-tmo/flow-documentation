@startuml
title ** __ICA API: GetProspects and GetProspectsById__ **\n

participant "Client" as client #LightGreen
participant "ICA API" as api
participant "IcaAuthHandler" as apiauth
participant "IcaPermissionsHandler" as apiperm
participant "IcaProspectReqHandler" as apiprospect
database "Aurora" as aurora #Red

skinparam SequenceGroupBorderColor blue
group#Magenta <b><color:white> GET /prospects

alt try
note over client, api #FFAAAA
"Request/Response": [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dd.fl.isp/ce.dd.fl.isp.ica-api.managing-prospects.yaml Swagger]]
endnote

client -> api: getProspects

note left
Clients: ICA and SFDC
endnote

alt tooManyRequests
api -> client: return 429 TooManyRequests
end

api -> apiauth: validateApiKey

alt apiKeyExpiredOrInvalid
apiauth -> api: throw Unauthorized
api -> client: return 401 Unauthorized
end

apiauth -> apiperm: validatePermissions
note left
Check for permissions flag
endnote

alt checkPermissionsFlagExists
apiperm -> api: throw BadRequest
api -> client: return 400 BadRequest
else checkPermissions
apiperm -> api: throw Forbidden
api -> client: return 403 Forbidden
end

apiperm -> apiprospect: getProspects
note left
Check for correct headers/payload
endnote

alt validateRequest
apiprospect -> api: throw BadRequest
api -> client: return 400 BadRequest

end

apiprospect -> apiprospect: Apply field permissions and build query
apiprospect -> aurora: getProspects
aurora -[#green]> apiprospect: return prospect list (0 or more records)
apiprospect -[#green]> api: build Success response { prospects: [{ msisdn: "string" }, ...] }
api -[#green]> client: return 200 { prospects: [{ ... }, ...]}

end
end

skinparam SequenceGroupBorderColor blue
group#Magenta <b><color:white> GET /prospects/{id}

alt try
client -> api: getProspectsById(msisdn)

note left
Clients: ICA and SFDC
endnote

alt tooManyRequests
api -> client: return 429 TooManyRequests
end

api -> apiauth: validateApiKey

alt apiKeyExpiredOrInvalid
apiauth -> api: throw Unauthorized
api -> client: return 401 Unauthorized
end

apiauth -> apiperm: validatePermissions
note left
Check for permissions flag
endnote

alt checkPermissionsFlagExists
apiperm -> api: throw BadRequest
api -> client: return 400 BadRequest
else checkPermissions
apiperm -> api: throw Forbidden
api -> client: return 403 Forbidden
end

apiperm -> apiprospect: getProspectsById(msisdn)
note left
Check for correct headers/payload
endnote

alt validateRequest
apiprospect -> api: throw BadRequest
api -> client: return 400 BadRequest
end

apiprospect -> apiprospect: Apply field permissions and build query
apiprospect -> aurora: getProspectsById(msisdn)
aurora -[#green]> apiprospect: return prospect list (0 or more records)
apiprospect -> apiprospect: If > 1 prospect found, return first
note left
Log warning
endnote
apiprospect -[#green]> api: build Success response { msisdn: "string", ... }
api -[#green]> client: return 200 { msisdn: "string", ... }

end

' skinparam SequenceGroupBorderColor red
' group#Magenta <b><color:white> getProspectById

end

@enduml
