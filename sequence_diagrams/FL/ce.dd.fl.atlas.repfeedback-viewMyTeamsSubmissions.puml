@startuml
actor Rep
participant  "Feedback App (TMA)" as Feedback_App
participant  "Feedback Service (TMS)" as Feedback_Svc
participant  "Jira Service" as Jira_Svc
participant  "Feedback Admin" as Feedback_Admin_Svc
participant "EIT Service" as EIT_Svc

Rep -> Feedback_App: Clicks on "View my team's submissions" link


Feedback_App -> EIT_Svc: Get reportees to <NTID> \nPOST /retrieve-care-rep-details
EIT_Svc [#green]--> Feedback_App: List of Reportees \nLoad Filter (NTID)
note right of Feedback_App
The service call to get reportees is used to load the NTID filter as well.
end note
alt
EIT_Svc [#red]--> Feedback_App: No NTIDs returned. \nDisplay message: No reporting NTIDs found
note right of Feedback_App
If the service doesn't return any NTID, the flow ends here. No feedback data will be loaded.
end note
end alt
Feedback_App -> Feedback_Svc: Get default load (Current Experience) for reportees
Feedback_Svc -> Jira_Svc: POST /rest/api/2/search
Jira_Svc [#green]--> Feedback_Svc: Search results
Feedback_Svc [#green]--> Feedback_App: Default load (Current Experience)
Feedback_App -> Feedback_Svc: Get Filter (Experience)
Feedback_Svc -> Feedback_Admin_Svc: GET feedback_admin/experiences
Feedback_Admin_Svc [#green]-->Feedback_Svc: Experiences data
Feedback_Svc [#green]--> Feedback_App: Load Filter (Experience)

Rep -> Feedback_App: Clicks "New ideas" tab
Feedback_App -> Feedback_Svc: Get Ideas submitted by reportees (New ideas)
Feedback_Svc -> Jira_Svc: POST /rest/api/2/search
Jira_Svc [#green]--> Feedback_Svc: Search results
Feedback_Svc [#green]--> Feedback_App: New ideas submitted by reportees

Rep -> Feedback_App: Clicks "Reported issues" tab
Feedback_App -> Feedback_Svc: Get Ideas submitted by reportees (Reported issues)
Feedback_Svc -> Jira_Svc: POST /rest/api/2/search
Jira_Svc [#green]--> Feedback_Svc: Search results
Feedback_Svc [#green]--> Feedback_App: Issues submitted by reportees

|||
alt <font color=green> Apply Filter
Rep -> Feedback_App: Clicks on "Filter" icon
note right of Rep
The following filter options available
1. Submission date
2. Experience
3. NTID
end note
Feedback_App -> Feedback_Svc: Get submissions by reportees for \n1.selected tab, and \n2.selected filter criteria
Feedback_Svc -> Jira_Svc: POST /rest/api/2/search
Jira_Svc [#green]--> Feedback_Svc: Search results
Feedback_Svc [#green]--> Feedback_App: Submission by reportees
Feedback_Svc [#green]--> Feedback_App: Display each filter criteria with CTA in "Filtered by" section
note right of Rep
Every filter criteria applied to the results will be displayed under "Filtered by" section with CTA "X"
The Rep can clear filter(s) by clicking "X" CTA for each filter criteria or by clicking "Clear all" CTA
end note
end
|||
alt <font color=green> Remove Filter
Rep -> Feedback_App: Clicks on CTA "X" on criteria displayed in the "Filtered by" section
Feedback_App -> Feedback_App: Filter criteria removed from "Filtered by" section
Feedback_App -> Feedback_Svc: Get submissions by reportees for \n1.selected tab, and \n2.selected filter criteria
Feedback_Svc -> Jira_Svc: POST /rest/api/2/search
Jira_Svc [#green]--> Feedback_Svc: Search results
Feedback_Svc [#green]--> Feedback_App: Submission by reportees
end
|||
alt <font color=green> Clear all filters
Rep -> Feedback_App: Clicks on "Clear all" CTA in the "Filtered by" section
Feedback_App -> Feedback_App: "Filtered by" section cleared
Feedback_App -> Feedback_Svc: Get submissions by reportees for \n1.selected tab
Feedback_Svc -> Jira_Svc: POST /rest/api/2/search
Jira_Svc [#green]--> Feedback_Svc: Search results
Feedback_Svc [#green]--> Feedback_App: Submission by reportees
end
|||
alt <font color=green> Sort
Rep -> Feedback_App: Clicks on "Sort" icon
note right of Rep
The following sort options available
1. Newest to Oldest
2. Oldest to newest
3. NTID (A to Z)
4. NTID (Z to A)
5. Experience (A to Z)
6. Experience (Z to A)
end note
note right of Feedback_App
Based on design, the UI will be loaded with max 50 (configurable) records only.
When the user scrolls to the bottom of the list, the next 50 will be loaded.
In a scenario, where only up to 50 records are displayed at any given time,
sorting applied on 50 records will be misleading. So, it has been decided to call
the service to load the sorted records every time the user applies "Sort" and display
up to 50 sorted records.
end note
Feedback_App -> Feedback_Svc: Get submissions by reportees for \n1.selected tab, \n2.selected filter criteria, and \n3.selected sort order
Feedback_Svc -> Jira_Svc: POST /rest/api/2/search
Jira_Svc [#green]--> Feedback_Svc: Search results
Feedback_Svc [#green]--> Feedback_App: Submission by reportees
end
|||
@enduml