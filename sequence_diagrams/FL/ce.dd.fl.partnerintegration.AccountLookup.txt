@startuml


title <font size=28 color=red><b>Account Lookup API workflow</b></font>\n\n

'Apple Server
participant "Apple Server" as Apple

'boundary Proxy
participant "Apigee SaaS" as SaaS

'Partner Experience Layer (PEL)
participant PEL
participant "Apigee\nOnPrem" as OnPrem
participant IDM
participant "Order API" as OM
participant RSP
participant TIBCO
participant CHUB
participant "DMW/SAP HANA" as DMW
note over DMW
 SAP HANA is the new source 
 system but production might 
 point to DMW in case of issues
 with SAP HANA
end note
participant Samson


Apple -> SaaS: Account Lookup /accountlookup
activate SaaS
    SaaS -> PEL: Invoke PEL /accountlookup
    par
        activate PEL
            'Get Dealer Code for Store ID
            PEL -> OnPrem: Lookup Dealer code using Store ID /dealer-management/v1/dealers
            activate OnPrem
                OnPrem -> IDM: Invoke IDM /dealer-management/v1/dealers
                IDM-> OnPrem: Dealer info (Dealer Code, Master Dealer Code)
                OnPrem->PEL: Dealer info 
            deactivate OnPrem

            'Authenticate customer
            PEL->OnPrem: authenticate customer /customer/v1/auth [[http://flows.corporate.t-mobile.com/all-swaggers/swagger/ce.dsg.cd/cvf_auth_swagger.json/ authenticateCustomer]]
            activate OnPrem
                OnPrem->CHUB: Invoke /customer/v1/auth
                CHUB->OnPrem: Authentication Result
                OnPrem->PEL: Authentication Result
            deactivate OnPrem
    end par

    |||

    alt authentication success
    activate PEL
        par Account Lookup Details
            'Generate Order ID
            PEL-> OnPrem: Generate Order ID /ordermanagement/v4/orders/orderid [[http://flows.corporate.t-mobile.com/all-swaggers/swagger/ce.dsg.om/order.json/ post-order-orderid]]
            activate OnPrem
                OnPrem->OM: Generate Order ID 
                OM->OnPrem: Return Order ID 
                OnPrem->PEL: Return Order ID
            deactivate OnPrem

            'Get Account Details
            PEL->RSP: Invoke PostpaidAccountService.getBasicAccountDetails [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-07-21/wsdl_api/html/PostPaidAccountService.html#getBasicAccountDetails getBasicAccountDetails]]
            activate RSP
                RSP->Samson: Get Account Details (evGtCustInfo, other tuxedo's?)
                Samson->RSP: Return Account Details
                RSP->PEL: Account Details
            deactivate RSP
            
            'Get Subscriber Details (Summary)
            PEL->RSP: Invoke PostPaidSubscriberService.getSubscribersInfo (Summary) [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-07-21/wsdl_api/html/PostPaidSubscriberService.html#getSubscribersInfo getSubscribersInfo]]
            activate RSP
                RSP->Samson: Get Subscriber Details (evGtSubSum, evGtAddPrm, others?)
                Samson->RSP: Return Subscriber Details
                |||
                RSP->IAM: Get PAH and MyTMO Access Levels
                IAM->RSP: Return PAH and MyTMO Access Levels

                RSP->PEL: Subscriber Details
            deactivate RSP

            'Get Subscriber Details (Details)
            PEL->RSP: Invoke PostPaidSubscriberService.getSubscribersInfo (Detail) [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-07-21/wsdl_api/html/PostPaidSubscriberService.html#getSubscribersInfo getSubscribersInfo]]
            activate RSP
                RSP->Samson: Get Subscriber Details (evLsSubInfo, others?)
                Samson->RSP: Return Subscriber Details

                RSP->PEL: Subscriber Details
            deactivate RSP
            
            'Get current Customer IMEI and description
            PEL->RSP: Invoke HandsetService.getHandsetInfo [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-07-21/wsdl_api/html/HandsetService.html#getHandsetInfo getHandsetInfo]]
            activate RSP
                RSP->DMW: Get Customer IMEI and description
                DMW->RSP: Return Customer IMEI and description
                RSP->PEL: Customer IMEI and description
            deactivate RSP
            
            'Check if Customer has PHP. If customer MRC's returned have EquipmentProtection MRC>0, customer has PHP
            PEL->RSP: Invoke PostPaidSubscriberService.getMonthlyRecurringChargeSummary [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-07-21/wsdl_api/html/PostPaidSubscriberService.html#getMonthlyRecurringChargeSummary getMonthlyRecurringChargeSummary]]
            activate RSP
                RSP->Samson: Get Customer MRC
                Samson->RSP: Return customer MRC
                RSP->PEL: Customer MRC
            deactivate RSP


            'Check if Customer has EIP and/or Lease. If customer ECB>0, customer has EIP or Lease
            PEL->TIBCO: Invoke Installment.GetCreditLineSummary [[https://servicecatalog.internal.t-mobile.com/servicecat/tibco/2018-07-21/wsdl_api/html/Installment.html#GetCreditLineSummary GetCreditLineSummary]]
            activate TIBCO
                TIBCO->Samson: Get EIP Info (evGbEipInfo)
                Samson->TIBCO: Return EIP Info
                TIBCO->EIP: Get Credit Line Summary
                EIP->TIBCO: Return Credit Line Summary
                TIBCO->PEL: Credit Line Summary
            deactivate TIBCO
            
            'Generate JWT Tokens (carrierReferenceId, carrierAuthToken)
            PEL->PEL: Generate JWT Tokens
            
            PEL->SaaS: Return Account Lookup Details

            SaaS->Apple: Account Lookup Details

            group Async process to log memo for Apple Account lookup 
            
                PEL->RSP: (Async) Invoke PostpaidAccountService.logAccountVerificationData [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-07-21/wsdl_api/html/PostPaidAccountService.html#logAccountVerificationData logAccountVerificationData]]
                activate RSP
                    RSP->Samson: Invoke Samson tuxedo evSvVerInfo
                    Samson->RSP: Success/Error
                    RSP->PEL: Success/Error
                deactivate RSP
            end
            |||
        end par Account Lookup Details

        |||
    else authentication failed
            PEL->SaaS: Return HTTP 200 with Authentication Failed error code
        
            SaaS->Apple: HTTP 200 with with Authentication Failed error code
                |||
    end alt
    deactivate PEL

deactivate SaaS

@enduml
