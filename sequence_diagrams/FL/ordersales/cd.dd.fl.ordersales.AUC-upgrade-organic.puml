@startuml


Actor Rep
participant QVFlex as QVF
participant QVXP as QVXP
participant "Tapestry" as T
participant OMNI as O
participant "DICE\nTIBCO\RSP" as MW
participant "TPOS" as POS
participant "OpenText\nxECM" as OT


    note over QVXP
       The flow is to capture flow changes for upgrades as impacted by the project AUC
        Channels - Retail, Care (organic only)
        Possible customer roles:
             Flow initiated from QVFlex - BRP, OTHER, AU1, AU2, AU
             Flow Initiated from QVXP - BRP, AU1, AU2
    end note

    alt Flow Initiated from QVFlex
    Rep -[#Black]> QVF : Rep accesses QV Flex on desktop
        QVF -> QVF: Verify User\nDetermine ROLE
        QVF -> QVXP: Pass ROLE in toggle hand-off to QVXP

        QVXP -> MW: RSP/<i>getBasicAccountDetails</i>
        QVXP <-- MW: Return account details
        Note over QVXP #lightgrey
           QVXP will extract from getbasicAccountDetails response:
            1) AU(s) name(s) to populate on UI, as applicable
            2) account-type/subtype to determine if in-scope account (
        end note

        QVXP -> QVXP: Rep selects device and navigates to new device pricing screen

        alt account is out-scope (account-type is E/B/G/N)
            QVXP -> QVXP: Treat New Device Pricing screen experience as-is
            QVXP --> O: pass Role as-received from QVFLEX toggle in updatepackage API request
            QVXP <-- O: Success response
        end

        Note over QVXP #lightgrey
            Alternate flows below are assuming in-scope account
        end note

        alt Pricing option selected is EIP or Lease
            alt Role received from QVFlex toggle is AU or AU1 or AU2
                QVXP -> QVXP: Prompt rep to enter first-name and last-name of AU1/AU2
                QVXP --> O: pass Role (AU1 or AU2 only) and AU name in updatepackage API request
                QVXP <-- O: Success response
            end
            alt Role received from QVFlex toggle is BRP or OTHER
                QVXP -> QVXP: Treat New Device Pricing screen experience as-is
                QVXP --> O: pass Role as-received from QVFLEX toggle in updatepackage API request
                QVXP <-- O: Success response
            end
        end

        alt Pricing option selected is FRP
            QVXP -> QVXP: Treat New Device Pricing screen experience as-is
            QVXP --> O: pass Role as-received from QVFLEX toggle in updatepackage API request
            QVXP <-- O: Success response
        end

    end

    alt Flow Initiated from QVXP (retail tablet)
    Rep -[#Black]> QVXP : Rep accesses QVXP on tablet
        QVXP -> QVXP: Verify customer and set customer Role as BRP or AU1 or AU2

        QVXP -> MW: RSP/<i>getBasicAccountDetails</i>
        MW --> QVXP: Return account details
        Note over QVXP #lightgrey
           QVXP will extract from getbasicAccountDetails response:
            1) AU(s) name(s) to populate on UI, as applicable
            2) account-type/subtype to determine if in-scope account (
        end note

        QVXP -> QVXP: ME selects device and navigates to new device pricing screen
        QVXP --> T: Invoke infinea callback function //getIdCaptureEnabled// to check if ID Capture functionality is allowed for store/tablet
        QVXP <-- T: Value of indicator received as a boolean value

        alt account is out-scope (account-type is E/B/G/N)
            QVXP -> QVXP: Treat New Device Pricing screen experience as-is
            QVXP --> O: pass Role as-is in updatepackage API request
            QVXP <-- O: Success response
        end

        Note over QVXP #lightgrey
            Alternate flows below are assuming in-scope account
        end note

        alt Pricing option selected is EIP or Lease
            alt Role is AU1 or AU2
                QVXP -> QVXP: Prompt rep to enter first-name and last-name of AU1/AU2
                QVXP --> O: pass Role (AU1 or AU2) and AU name in updatepackage API request
                QVXP <-- O: Success response
            end
            alt Role is BRP
                QVXP -> QVXP: Treat New Device Pricing screen experience as-is
                QVXP --> O: pass Role as-is in updatepackage API request
                QVXP <-- O: Success response
            end

            alt ID Capture is allowed
                alt Customer provided ID is military ID or customer does not consent for ID Capture
                    QVXP --> MW: Pass secondary ME details in //queryAuthenticationAndAuthorization//
                    QVXP <-- MW: Credentials validation response
                    QVXP -> QVXP: allow ME to proceed with adding device to cart
                else Customer consents to ID Capture
                    QVXP --> T: Launch ID Capture modal to digitally capture customer ID
                    QVXP <-- T: Retrieve idImageKey for the digitally captured ID (once per transaction)
                    QVXP -> QVXP: allow ME to proceed with adding device to cart
                    alt in-store flow
                        QVXP --> POS: pass idImageKey in handoff [mposJump toggle] upon toggle to TPOS
                    else ship-to flow
                        QVXP --> T: pass idImageKey along with transaction data in payload upon order confirmation
                    end
                end
            else ID Capture is not allowed
                QVXP -> QVXP: ME adds device to cart and proceeds as-is without ID Capture
            end
        end

        alt Pricing option selected is FRP
            QVXP -> QVXP: Treat New Device Pricing screen experience as-is
            QVXP --> O: pass Role as-is in updatepackage API request
            QVXP <-- O: Success response
        end

    end

    Note over QVXP
        Subsequent flow is as-is from QVXP perspective.
        For in-store flows, rep will toggle to mPOS for payment and to complete the transaction.
        For ship-to flows, rep will enter payment details and then confirm the order on QVXP.
    end note

    O --> MW: pass role and name based on whether actor is BRP/AU
    MW --> OT: pass name based on whether actor is BRP/AU \n to display in EIP disclosure agreement

@enduml