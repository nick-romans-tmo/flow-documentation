@startuml
POS -> RSP: manageDocument Request
RSP -> OpenText
OpenText -> RSP
RSP -> Docusign
Docusign -> RSP

RSP -> POS: manageDocument Response with URL
POS -> Docusign: Redirect to Docusign
alt successful Esign
Docusign -> POS: onComplete Response
Docusign -> RSP: notifyDisclosureAcceptance request
RSP ->> OpenText: Save Document
RSP -> Docusign: notifyDisclosureAcceptance response

else Decline Esign
Docusign -> POS: OnDecline Response
alt Wet Signature
POS -> RSP: notifyDisclosureAcceptance with signatureOnFile=true
RSP ->> OpenText: Save Document
RSP -> POS: notifyDisclosureAcceptance Response

else Cancel Transaction
POS -> POS: Exit out of transaction
end


end
@enduml