@startuml

title <font size=28 color=red><b>Activate API workflow</b></font>\n\n

'Apple Server
participant "Apple Server" as Apple

'boundary Proxy
participant "Apigee SaaS" as SaaS

'Partner Experience Layer (PEL)
participant PEL
participant "Apigee OnPrem" as OnPrem
participant IDM
participant "Order API" as OM
participant "OMS" as OMS
participant "Order Fulfillment" as OF
participant "Apigee OnPrem" as OnPrem2
participant "Billing API" as Billing
participant "servicesamson" as DICE
participant Samson
participant DEEP.io


note over Apple
	Apple will call activate with statusCheck=false 
    to first submit activate request
	and then call activate again with 
    statusCheck=true to get activation status
endnote

== Apple sends request to activate new SIM ==
||||
'Apple calls activate with statusCheck=false
Apple -> SaaS: Activate /activate with statusCheck=false


activate SaaS
  SaaS -> PEL: Invoke PEL /activate

  activate PEL

	'Get Dealer Code for Store ID
    PEL -> OnPrem: Lookup Dealer code using Store ID /dealer-management/v1/dealers
    activate OnPrem
      OnPrem -> IDM: Invoke IDM /dealer-management/v1/dealers
      IDM-> OnPrem: Dealer info (Dealer Code, Master Dealer Code)
      OnPrem->PEL: Dealer info 
    deactivate OnPrem
    |||
    'Save Apple Activate request as Upgrade Order in OM
    PEL->OnPrem: Invoke Order Submit /ordermanagement/v4/orders [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.om/Order.json post-orders]] 
    activate OnPrem

      note over SaaS, PEL: "PEL returns response Apple without waiting for OM API success/error response"
      PEL->SaaS: Return Order ID with Status as "In Progress"
      SaaS->Apple: Order ID with Status as "In Progress"

      OnPrem->OM: Invoke Order Submit [[http://flows.corporate.t-mobile.com/all-diagrams/sequence_diagrams/omd/ce.dsg.od.api.postorder.puml/ Submit Order Flow]]
      activate OM
        OM->OM: Save Order to process asynchronously
        OM->OnPrem: Return Order ID

        OnPrem->PEL: Order ID
        'OM will process order asynchonously hence OM is not deactivated here
    deactivate OnPrem

    
    
  deactivate PEL
deactivate SaaS


== Async Order Processing ==
||||

'async processing of Order in OM
OM->OMS: Save and Process Order
activate OMS
  OMS->OF: Process order for fulfillment
  activate OF

    'OF complete SIM change and publishes completed order details to DEEP.io

    OF->OnPrem2: Invoke SIM change API /billing-experience/v1/sim-assignments/{transaction-id} [[http://flows.corporate.t-mobile.com/all-swaggers/swagger/billing.samson/billing-experience-assign-sim-v0.1.json/ Change Sim on a line of service]]
    activate OnPrem2
      OnPrem2->Billing: Invoke SIM Change API [[http://flows.corporate.t-mobile.com/all-diagrams/sequence_diagrams/bd/ce.dd.bd.sim_change.txt/ SIM Change flow]]
      activate Billing

        'Billing calls Samson tuxedo's using REST service wrapper created by DICE
    
        Billing->DICE: Invoke Get Verificaiton info Tux REST service to retrieve BAN Last update date/timestamp

        activate DICE
          DICE->Samson: Invoke evGtVerInfo tux
          Samson->DICE: Success/Error
          DICE->Billing: Success/Error
        deactivate DICE

        Billing->DICE: Invoke SIM change Tux REST service

        activate DICE
          DICE->Samson: Invoke evChgSim tux
          Samson->DICE: Success/Error
          DICE->Billing: Success/Error
        deactivate DICE

        Billing->OnPrem2: SIM Change Success/Error
      deactivate Billing

      OnPrem2->OF: SIM Change Success/Error
    deactivate OnPrem2

    OF->OMS: SIM Change Success/Error
    OMS->OM: SIM Change Success/Error
    OF->DEEP.io: Publish Completed Order details
  deactivate OF
deactivate OMS
deactivate OM

|||
== Apple requests SIM activation status ==
|||

loop Apple will call /activate with statusCheck=true X times

'Apple calls activate with statusCheck=true
Apple -> SaaS: Activate /activate with statusCheck=true
activate SaaS
  SaaS -> PEL: Invoke PEL /activate

  activate PEL

    PEL->OnPrem: Invoke Get Order Status /ordermanagement/v4/orders/{id}/status [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.om/Order.json Get Order Status]]
    activate OnPrem
      OnPrem->OM: Invoke Get Order Status [[http://flows.corporate.t-mobile.com/all-diagrams/sequence_diagrams/omd/ce.dsg.dice.od.api.getordersstatushistorybyid.txt/ Get Order Status]]
      OM->OnPrem: Return Order Status
      OnPrem->PEL: Order Status
    deactivate OnPrem

    PEL->SaaS: Return Order Status
    SaaS->Apple: Return Order Status 
  deactivate PEL
deactivate SaaS
end

@enduml