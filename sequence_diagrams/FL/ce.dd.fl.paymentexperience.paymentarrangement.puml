@startuml
Title Payment Experience
actor "Mobile Expert" as Rep

' Global Navigation Boomerang UI
participant "Atlas \nGlobal Navigation" as ATLAS



BOX "Payment Experience MicroApp" #LightGray
    participant "TMA: Application \nUI" as TMA
    participant "TMS: Services \nManager" as TMS
end BOX

participant "API Gateway" as APIGateway

participant "Account Overview TMS" as AO_TMS

participant "RepDashStore" as Store

participant "RSP" as RSP

participant "Data security service" as DataSecurityService

' Note, there is APIGEE gateway between all TMA<->TMS transactions
' However this was removed for simplicity
participant "ApiGee Gateway" as APIGEE

participant "Tibco" as TIBCO



activate Rep
Rep->ATLAS: Rep login \nand search \ncustomer account

activate ATLAS

ATLAS->TMA: Toggle to \nPayment Arrangement \nfrom Global Nav
activate TMA

group #CornSilk PreLoad Validation for ATLAS
TMA -> Store: getRepToken()
activate Store
Store -> TMA: Rep Token
deactivate Store
TMA -> APIGateway: /createSession (pass Rep token)
activate APIGateway
APIGateway -> TMA: Response
deactivate APIGateway
TMA -> Store: getCustomerInFocusToken()
activate Store
Store -> TMA: CV&F Token
deactivate Store

TMA -> TMS: /extractCVFToken (pass CV&F token)
activate TMS
TMS -> DataSecurityService: getCustomerTokenData (token)
activate DataSecurityService
DataSecurityService -> TMS: UserData Payload
TMS -> TMA: (Pass customerTokenDetails)
deactivate DataSecurityService

deactivate TMS

end ' End Group
group #CornSilk PreLoad Validation for non-ATLAS
TMA -> TMS: /getCustomerTokenDetails (pass customerData details)
activate TMS
TMS -> DataSecurityService: populateHash (customerTokenDetails)
activate DataSecurityService
DataSecurityService -> TMS: populates customerTokenDetails hash
TMS -> TMA: (Pass customerTokenDetails)
deactivate DataSecurityService

deactivate TMS

end ' End Group

group #Lavender  On Landing Page

group #CornSilk User Regular Permissions Data
TMA<-> APIGateway: /user/permissions
activate TMS
APIGateway<->RSP: RSP getUserProfilesandPermissions
note left: Regular Permissions
activate RSP
deactivate RSP
deactivate TMS
end  ' End Group

group #CornSilk User FDP Permissions Data
TMA<-> TMS: /paymentexperience/getUserPermissions
activate TMS
TMS<->RSP: RSP getUserProfilesandPermissions
note left: FDP Permissions
activate RSP
deactivate RSP
deactivate TMS
end  ' End Group

group #CornSilk Get Dormancy Feature Flag
activate TMS
TMA<->TMS: /paymentexperience/uiConfigurationData
deactivate TMS

end  ' End Group

par  #AliceBlue TMA Triggers All The Following In Parallel

group #MistyRose  Payment Arrangements Data
TMA<->TMS: /paymentexperience/paymentarrangements
activate TMS
par #CornSilk Parallel Service Calls
TMS<->RSP: RSP PostpaidAccountService.getBasicAccountDetails
note left: Quick Scan Indicators\n Billing Balances
activate RSP

TMS<->RSP: RSP PostpaidAccountService.getAccountVerificationData
note left: Writeoff Account Details

deactivate RSP

TMS<->APIGEE: APIGEE getBillSummary (/bill/v1/bill-summary)
note left: Billing Balances
activate APIGEE

TMS<->APIGEE: APIGEE quoteSummary (/billing/v1/servicequotes?expand=servicequote)
note left: Quote Id and amounts

deactivate APIGEE

deactivate TMS
end  ' End Par

end  ' End Group

group #MistyRose Make Payment CTA
TMA<-> APIGateway: /v1/tms-serve-accountreview/postpaidaccount/makePayment
activate AO_TMS
APIGateway<->AO_TMS: /postpaidaccount/makePayment
note left: Make payment CTA rules
deactivate AO_TMS
end  ' End Group

group #MistyRose Collections Data
TMA<->TMS: /paymentexperience/collectioninfo
activate TMS

TMS<->APIGEE: APIGEE getCollectionInfo (collection/v1/collection-info)
activate APIGEE
note left: Collection History\n Collection Status
deactivate APIGEE
deactivate TMS

end  ' End Group

group #MistyRose Eligibility And Active PA Data
TMA->TMS: /paymentexperience/eligibilityactive
activate TMS

TMS<->TIBCO: Tibco Bill.getPaymentArrangement
note left: PA Eligibility\n Active PA Details
activate TIBCO
deactivate TIBCO
deactivate TMS

end  ' End Group

group #MistyRose Recent Payments History Data
TMA<-> TMS: /paymentexperience/recentpayments
activate TMS

TMS<->APIGEE: APIGEE getRecentPayments (/billing-financial/v1/payments/history)
note left: Most Recent Three\n Payment Activities
activate APIGEE
deactivate APIGEE
deactivate TMS

end  ' End Group

group #MistyRose Payment Arrangment History Data
TMA<->TMS: /paymentexperience/history
activate TMS
TMS<->TIBCO: Tibco Bill.getPaymentArrangementHistory
note left: History of Past PA
activate TIBCO
deactivate TIBCO
deactivate TMS

end  ' End Group


deactivate TMS

end ' End Par


end ' End Group


group #Lavender  Secured Setup Payment Arrangement (Payment Method App)

group #Lavender  Stored Payment Method Tab

group #CornSilk Stored Payment Methods (on load of Payment method App)
TMA<-> APIGateway: /paymentexperience/getStoredPaymentMethods
activate APIGateway
APIGateway<->APIGEE: APIGEE POST /payments/v4/storedpayments/search
note left: Retrieve stored \npayment methods (CARE Only)
activate APIGEE
deactivate APIGEE
deactivate APIGateway
end  ' End Group

group #CornSilk Verify Stored Payment Methods (on click of Radio button)
TMA<-> APIGateway: /paymentexperience/verifypaymentmethod
activate APIGateway
APIGateway<->APIGEE: APIGEE POST /payments/v4/storedpayments/verifypaymentmethod
note left: One time verify stored \npayment methods after Ban to Line level Wallet migration (CARE Only)
activate APIGEE
deactivate APIGEE
deactivate APIGateway
end  ' End Group

group #CornSilk Update Stored Payment Methods (on click of Edit CTA)
TMA<-> APIGateway: /paymentexperience/managePaymentMethod
activate APIGateway
APIGateway<->APIGEE: APIGEE POST /payments/v4/mpi
note left: Update stored \npayment methods (CARE Only)
activate APIGEE
deactivate APIGEE
deactivate APIGateway
end  ' End Group

group #CornSilk Delete Stored Payment Methods (on click of Delete CTA)
TMA<-> APIGateway: /paymentexperience/managePaymentMethod
activate APIGateway
APIGateway<->APIGEE: APIGEE DELETE /payments/v4/mpi
note left: Delete stored \npayment methods (CARE Only)
activate APIGEE
deactivate APIGEE
deactivate APIGateway
end  ' End Group

end  ' End Group

group #Lavender  New Card Payment Method Tab
group #CornSilk Payment card validation (On entering initial 6 digits of card)
TMA<-> APIGateway: /paymentexperience/cardtypevalidation
activate APIGateway
APIGateway<->APIGEE: APIGEE /payments/v4/card-type-validation
note left: Retrieve card type, card \nand CVV length
activate APIGEE
deactivate APIGEE
deactivate APIGateway
end  ' End Group

group #CornSilk Payment Method Encryption Key (On clicking New card or New Bank tab)
TMA<-> APIGateway: /paymentexperience/encryptionkey
activate APIGateway
APIGateway<->APIGEE: APIGEE /payments/key/v4/publickey
note left: Returns encryption key \nto encrypt payment method
activate APIGEE
deactivate APIGEE
deactivate APIGateway
end  ' End Group

group #CornSilk Validate zip code (After entering 5 digit zip code)
TMA<-> APIGateway: /paymentexperience/zipcodevalidation
activate APIGateway
APIGateway<->RSP: RSP /v1/address/citystate
note left: Returns zip code validity
activate RSP
deactivate RSP
deactivate APIGateway
end  ' End Group

end  ' End Group

group #Lavender  New bank Payment Method Tab
group #CornSilk Bank routing number validation (On entering 9 digits routing number)
TMA<-> APIGateway: /paymentexperience/routingnumbervalidation
activate APIGateway
APIGateway<->APIGEE: APIGEE /payments/abarouting/v4/abaroutingnumbers/{routingNumber}?programCode=U1POSTLEGACY
note left: validate routing number
activate APIGEE
deactivate APIGEE
deactivate APIGateway
end  ' End Group
end  ' End Group

group #CornSilk Payment method Card/Bank validation (on click of Validate CTA)
TMA<-> APIGateway: /paymentexperience/managePaymentMethod
activate APIGateway
APIGateway<->APIGEE: APIGEE /payments/v4/mpi
note left: Payment method validation and/or Save payment method (CARE Only)
activate APIGEE
deactivate APIGEE
deactivate APIGateway
end  ' End Group

end ' End Group

group #Lavender Subscriber profile (on click of 'Continue' to Review screen)
TMA<-> APIGateway: /paymentexperience/subscriberprofile
activate APIGateway
APIGateway<->TIBCO: TIBCO /v1/subscriber/profilev2
note left: Retrieve PAH MSISDN and Email
activate TIBCO
deactivate TIBCO
deactivate APIGateway
end  ' End Group

group #MistyRose Account unchanged (on click of 'Continue' to T&C)
TMA<-> APIGateway: /paymentexperience/verifyAccountUnchanged
activate APIGateway
APIGateway<->RSP: v1/account/unchanged
note left: Retrieve Ban lock status
activate RSP
deactivate RSP
deactivate APIGateway
end  ' End Group

group #Lavender Submit PA (on click of 'Submit' in Setup PA flow)
TMA<-> APIGateway: /paymentexperience/setpa
activate APIGateway
APIGateway<->APIGEE: APIGEE /payments/v4/mpi
note left: Creates Secure/Unsecured payment arrangement
activate APIGEE
deactivate APIGEE
deactivate APIGateway
end  ' End Group

group #MistyRose Edit PA (on click of 'Update' in Edit PA flow)
TMA<-> APIGateway: /paymentexperience/managePaymentArrangement
activate APIGateway
APIGateway<->APIGEE: APIGEE /payments/v4/mpi
note left: Update Secured/Unsecured PA \npayment method
activate APIGEE
deactivate APIGEE
deactivate APIGateway
end  ' End Group

group #MistyRose Delete PA (on click of 'Submit' in Delete PA flow)
TMA<-> APIGateway: /paymentexperience/deletepaymentarrangement
activate APIGateway
APIGateway<->APIGEE: APIGEE DELETE /payments/v4/mpi
note left: Delete Secured/Unsecured PA \npayment arrangement
activate APIGEE
deactivate APIGEE
deactivate APIGateway
end  ' End Group

group #Lavender Send email notification PA (on success of Setup PA)
TMA<-> APIGateway: /paymentexperience/setpa
activate APIGateway
APIGateway<->TIBCO: TIBCO /qv/v1/receipts
note left: Send payment arrangement email notification
activate TIBCO
deactivate TIBCO
deactivate APIGateway
end  ' End Group

deactivate ATLAS
deactivate Rep

@enduml
