@startuml

title <font size=16 color=red><b>Validate Device API workflow</b></font>\n\n


'Apple Server
participant "Apple Server" as Apple

'boundary Proxy
participant "Apigee SaaS" as SaaS

'Partner Experience Layer (PEL)
participant PEL
participant "Apigee OnPrem" as OnPrem
participant IDM
participant RSP
participant TIBCO
participant "SAP OER" as OER
participant Samson


Apple -> SaaS: Validate Device /validateDevice
activate SaaS
  SaaS -> PEL: Invoke PEL /validateDevice
  activate PEL

    'Get Dealer Code for Store ID
    PEL -> OnPrem: Lookup Dealer code using Store ID /dealer-management/v1/dealers
    activate OnPrem
      OnPrem -> IDM: Invoke IDM /dealer-management/v1/dealers
      IDM-> OnPrem: Dealer info (Dealer Code, Master Dealer Code)
      OnPrem->PEL: Dealer info 
    deactivate OnPrem
  |||
  par if dealer code look is successful
    'check if IMEI is lost/stolen
    PEL->RSP: Invoke Equipment.lookupEquipment [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-07-21/wsdl_api/html/EquipmentService.html#lookupEquipment lookupEquipment]]
    
    activate RSP
      RSP->OER: Check for Lost/Stolen
      OER->RSP: Lost/Stolen status result
      RSP->PEL: Lost/Stolen status result
    deactivate RSP
    
    PEL->PEL: If IMEI Lost/Stolen, Set error code to Device not found

    |||

    'check if SIM is available for activation
    PEL->TIBCO: Invoke Device.getSimDtls [[https://servicecatalog.internal.t-mobile.com/servicecat/tibmicro/production/wsdl_api/html/Device.html#getSIMDtls getSimDtls]]
    
    activate TIBCO
      TIBCO->Samson: Get SIM details
      Samson->TIBCO: Return SIM details
      TIBCO->PEL: SIM details
    deactivate TIBCO
    
    PEL->PEL: Set error code to Invalid SIM if SIM not in AA status
    |||
  end
    PEL->SaaS: IMEI and SIM validation result
  deactivate PEL

  SaaS->Apple: IMEI and SIM validation result
deactivate SaaS


@enduml