@startuml
actor Customer
actor CSR
participant "Care Apps - QVFlex"
participant "PIN Management App" 
participant "APIGEE"
participant "CHUB"
participant "Samson"

Customer->CSR: Call Care
CSR->"Care Apps - QVFlex": Access Care Apps
"Care Apps - QVFlex"->"Care Apps - QVFlex":Verify Customer
alt No PIN Exists

"Care Apps - QVFlex"->APIGEE:PasswordLookup API
activate APIGEE
APIGEE->CHUB:PasswordLookup API
activate CHUB
CHUB-->APIGEE:No PIN Found
deactivate CHUB
APIGEE-->"Care Apps - QVFlex":No PIN Found
deactivate APIGEE

else PIN found but does not meet standard

"Care Apps - QVFlex"->APIGEE:PasswordLookup API
activate APIGEE
APIGEE->CHUB:PasswordLookup API
activate CHUB
CHUB->Samson:PINFormatvalidation
activate Samson
Samson-->CHUB:Invalid PIN
deactivate Samson
CHUB-->APIGEE:Invalid PIN
deactivate CHUB
APIGEE-->"Care Apps - QVFlex":Invalid PIN
deactivate APIGEE

end

== QVFlex PIN Management using PIN Management Microapp ==
	"Care Apps - QVFlex"->"Care Apps - QVFlex":Generate and Verify OTP
    "Care Apps - QVFlex"->"PIN Management App":Invokes PIN Management App
	"PIN Management App"->"PIN Management App":Prompt Customer to Add/ Change PIN
	CSR -> Customer:Collect New PIN
	Customer->CSR:Provide PIN
	CSR->"PIN Management App": Enters New PIN
	"PIN Management App"->"PIN Management App":Enter New PIN
    "PIN Management App"->"PIN Management App":Re-Enter PIN
	alt Enter PIN and Re-enter PIN Match
		"PIN Management App"->"PIN Management App":Submit PIN
		alt PIN Management Successful
			"PIN Management App"->APIGEE:Send PIN to Samson for Storage (PINManagement API)
			activate APIGEE
			APIGEE->Samson:PINManagement API
			activate Samson
			Samson-->APIGEE:Success
			deactivate Samson
			APIGEE-->"PIN Management App":Success
			deactivate APIGEE
			"PIN Management App"->"PIN Management App":Display Confirmation Screen
			Samson->Customer:Send SMS
			Samson->Samson:Create Memos
			Samson->Samson:Create AccountActivityLog
			else PIN Management Failed	
			"PIN Management App"->APIGEE:Send PIN to Samson for Storage (PINManagement API)
			activate APIGEE
			APIGEE->Samson:PINManagement API
			activate Samson
			Samson-->APIGEE:PIN does not meet standard
			deactivate Samson
			APIGEE-->"PIN Management App":PIN does not meet standard
			deactivate APIGEE
			"PIN Management App" -> "PIN Management App": Display Error Message
		end
		else Enter PIN and Re-enter PIN Do Not Match
    	"PIN Management App"->"PIN Management App": Display PIN Do Not Match Error Message
	end
@enduml
