@startuml
actor Customer
participant "SIVR"
participant "APIGEE"
participant "CHUB"
participant "Samson"

Customer->SIVR: Calls SIVR          
SIVR->SIVR: Verify Customer 

alt No PIN Exists

SIVR->APIGEE:PasswordLookup API
activate APIGEE
APIGEE->CHUB:PasswordLookup API
activate CHUB
CHUB-->APIGEE:No PIN Found
deactivate CHUB
APIGEE-->SIVR:No PIN Found
deactivate APIGEE

else PIN found but does not meet standard

SIVR->APIGEE:PasswordLookup API
activate APIGEE
APIGEE->CHUB:PasswordLookup API
activate CHUB
CHUB->Samson:PINFormatvalidation
activate Samson
Samson-->CHUB:Invalid PIN
deactivate Samson
CHUB-->APIGEE:Invalid PIN
deactivate CHUB
APIGEE-->SIVR:Invalid PIN
deactivate APIGEE

end

	SIVR->SIVR:Invokes Add/ChangePIN Flow
	SIVR->SIVR:Prompt Customer to Add/ Change PIN
	SIVR->SIVR:Generate and Verify OTP
    Customer->SIVR:Enter New PIN
	SIVR->SIVR:New PIN Entered
	
	alt PIN Validation Successful
	
		SIVR->APIGEE:Validate Password API
		activate APIGEE
		APIGEE->Samson:Validate Password API
		activate Samson
		Samson-->APIGEE:PIN Validation Successful
		deactivate Samson
		APIGEE-->SIVR:PIN Validation Successful
		deactivate APIGEE
		
        Customer->SIVR:Re-Enter New PIN
        SIVR->SIVR:New PIN Re-Entered
        alt Enter PIN and Re-enter PIN Match
        SIVR->SIVR:Submit PIN
        alt PIN Management Successful
			"SIVR"->APIGEE:Send PIN to Samson for Storage (PINManagement API)
			activate APIGEE
			APIGEE->Samson:PINManagement API
			activate Samson
			Samson-->APIGEE:Success
			deactivate Samson
			APIGEE-->"SIVR":Success
			deactivate APIGEE
			Samson->Customer:Send SMS
			Samson->Samson:Create Memos
			Samson->Samson:Create AccountActivityLog
			else PIN Management Failed	
			"SIVR"->APIGEE:Send PIN to Samson for Storage (PINManagement API)
			activate APIGEE
			APIGEE->Samson:PINManagement API
			activate Samson
			Samson-->APIGEE:PIN does not meet standard
			deactivate Samson
			APIGEE-->"SIVR":PIN does not meet standard
			deactivate APIGEE
			"SIVR" -> "Customer": Play Error Message
		end
        else Enter PIN and Re-enter PIN Do Not Match
        SIVR->Customer:Play Error Message
        end
		
		else PIN Validation Failed
		SIVR->APIGEE:Validate Password API
		activate APIGEE
		APIGEE->Samson:Validate Password API
		activate Samson
		Samson-->APIGEE:PIN does not meet standard
		deactivate Samson
		APIGEE-->SIVR:PIN does not meet standard
		deactivate APIGEE
		SIVR -> Customer: Play Error Message
	end


@enduml
