@startuml
actor Customer
participant eServices
participant "Digital Billing NextGen" as DBNG
participant "eBill WebServices" as eBill
participant "DSPA/ T-Selfcare" as DSPA
participant "PEBX database" as eBillDB
Customer->eServices: Login request
eServices->Customer: Present My T-Mobile home page
Customer->eServices: Click on Billing tab
eServices->DBNG: getBillList request
DBNG->eServices: getBillList reponse
eServices->eBill: printsummary REST request for BILL_STATEMENT_ID, BAN, MSISDN
group Retrieve customer permissions
	eBill->DSPA: retrieve customer type [SelfCareService.getCustomerType(MSISDN)]
	DSPA->eBill: return customer type
	eBill->DSPA: retrieve customer profile [SelfCareService.getCustomerProfile(type, MSISDN)]	
	DSPA->eBill: return customer profile
end
alt BAN-level permissions (PAH/Master)
	eBill->eBillDB: query DB to retrieve data (excluding CDRs) for ALL lines on account
else MSISDN-level permissions
	eBill->eBillDB: query DB to retrieve data (excluding CDRs) for single MSISDN
end
eBill->eBill: convert Samson XML to eBill XML (using XSLT)
alt JSON response requested
	eBill->eBill: convert eBill XML to JSON
else PDF response requested
	eBill->eBill: convert eBill XML to PDF (using Apache FO)
end
eBill->eServices: printsummary response
eServices->Customer: present statement data in page or pass PDF
@enduml
