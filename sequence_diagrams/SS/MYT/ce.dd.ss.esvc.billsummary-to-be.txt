@startuml
actor Customer
participant Browser
participant Web
participant AEM
participant Mezzo
participant EOS
participant Apigee
actor Middleware
participant DICE
participant eBill
participant Samson

== Legacy Billing Page == 

Customer -> Browser: Click Billing on navigation bar

Browser->Web : billing/Summary.html
Web->AEM: billing/Summary.html
AEM->Web: BillingSummary Page
note left : Page is cached by dispatcher
Web-> Browser: BillingSummary Page

Browser->Browser: <b>Load Bill Cycle Selector Section</b>
Browser->Web: servlet/billing/billingSummary:getBillList
Web->AEM:servlet/ billing/billingSummary:getBillList
AEM->EOS: getBillList
EOS->Apigee: getBillList
Apigee->DICE: getBillList
DICE->Apigee: BillList
Apigee->EOS: BillList
EOS->AEM: BillList
AEM->Web: BillList
Web->Browser: BillList

Browser->Browser: <b>Load Balance Due, Charges Section</b>
Browser->Web: servlet/billing/balancedue

Web->AEM: servlet/billing/balancedue

AEM->Mezzo: getSubscriberInfo
Mezzo->Middleware: getSubscriberInfo
Middleware->Samson: getSubscriberInfo
Samson->Middleware: SubscriberInfo
Middleware->Mezzo: SubscriberInfo
Mezzo->AEM: SubscriberInfo

AEM->Mezzo: getServiceAgreement
Mezzo->Middleware: getServiceAgreement
Middleware->Samson: getServiceAgreement
Samson->Middleware: ServiceAgreement
Middleware->Mezzo: ServiceAgreement
Mezzo->AEM: ServiceAgreement

AEM->Mezzo: getServiceAgreement
Mezzo->Middleware: getServiceAgreement
Middleware->Samson: getServiceAgreement
Samson->Middleware: ServiceAgreement
Middleware->Mezzo: ServiceAgreement
Mezzo->AEM: ServiceAgreement

AEM->Mezzo: getSchedulePayment
Mezzo->Middleware: getSchedulePayment
Middleware->Samson: getSchedulePayment
Samson->Middleware: SchedulePayment
Middleware->Mezzo: SchedulePayment
Mezzo->AEM: SchedulePayment

AEM->Mezzo: getPaymentArrangement
Mezzo->Middleware: getPaymentArrangement
Middleware->Samson: getPaymentArrangement
Samson->Middleware: PaymentArrangement
Middleware->Mezzo: PaymentArrangement
Mezzo->AEM: PaymentArrangement



AEM->Web: BalanceDueResponse
Web->Browser: BalanceDueResponse(IsSuspended, easyPayStatus, paperlessStatus, dueDate, balance)


Browser->Web:servlet/ billing/billingSummary:getBinaryBillDetails
Web->AEM: servlet/billing/billingSummary:getBinaryBillDetails
AEM->EOS: getBinaryBillDetails
EOS->eBill: getBinaryBillDetails
eBill->EOS: BillDetails
EOS->AEM: BillDetails
AEM->Web: BillDetails
Web->Browser: BillDetails


Browser->Browser: <b>Load Payment Settings Section</b>
Browser->Web: servlet/EquipmentPlan
Web->AEM: servlet/EquipmentPlan

AEM->Mezzo: getInstallmentPlanHistory, 
Mezzo->Middleware: getInstallmentPlanHistory
Middleware->eBill: getInstallmentPlanHistory
eBill->Middleware: InstallmentPlanHistory
Middleware->Mezzo: InstallmentPlanHistory
Mezzo->AEM: InstallmentPlanHistory

AEM->Mezzo: getEligibleSalesScenerios 
Mezzo->Middleware: getEligibleSalesScenerios
Middleware->eBill: getEligibleSalesScenerios 
eBill->Middleware: EligibleSalesScenerios
Middleware->Mezzo: EligibleSalesScenerios
Mezzo->AEM: EligibleSalesScenerios

AEM->Web: EquipmentPlanDetails
Web->Browser: EquipmentPlanDetails

Browser->Web: servlet/profile/easypay
Web->AEM: servlet/profile/easypay

AEM->Mezzo: getEasyPay
Mezzo->Middleware: getEasyPay, 
Middleware->Samson: getEasyPay
Samson->Middleware: EasyPayDetails
Middleware->Mezzo: EasyPayDetails
Mezzo->AEM: EasyPayDetails

AEM->Web: EasyPayDetails
Web->Browser: EasyPayDetails

Browser->Browser: <b>Load Bill HighLights Section</b>
Browser->Web: discounts/getEligibleDiscounts
Web->AEM: discounts/getEligibleDiscounts

AEM->Mezzo: getEligibleDiscounts
Mezzo->Middleware: getEligibleDiscounts
Middleware->Samson: getEligibleDiscounts
Samson->Middleware: EligibleDiscounts
Middleware->Mezzo: EligibleDiscounts
Mezzo->AEM: EligibleDiscounts

AEM->Mezzo: getDiscountInfo, 
Mezzo->Middleware: getDiscountInfo
Middleware->Samson: getDiscountInfo
Samson->Middleware: DiscountInfo
Middleware->Mezzo: DiscountInfo
Mezzo->AEM: DiscountInfo


AEM->Web: EligibleDiscounts
Web->Browser: EligibleDiscounts


Browser->Web: servlet/billing/balancedue:autoPayEligibilityCheck
Web->AEM: servlet/billing/balancedue:autoPayEligibilityCheck

AEM->EOS: getAutoPayEligibility
EOS->Middleware: getAutoPayEligibility
Middleware->Samson: getAutoPayEligibility
Samson->Middleware: AutoPayEligibility
Middleware->EOS: AutoPayEligibility
EOS->AEM: AutoPayEligibility


AEM->Web: AutoPayEligibility
Web->Browser: AutoPayEligibility




@enduml