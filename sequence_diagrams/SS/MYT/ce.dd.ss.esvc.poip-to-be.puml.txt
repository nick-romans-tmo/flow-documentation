@startuml
	participant ui as "WEB" 
	participant cms as "AEM" 
	participant eos as "EOS" 
	participant mezzo as "Mezzo"
	participant dps as "DPS" 
	participant mw as "Middleware" 
	participant omni as "OMNI"
	participant Docusign as "Docusign" 
	participant xecm as "xECM" 
	participant eip as "EIP" 
	
	ui -> cms: Lease to POIP
	cms -> mezzo : createSession
	mezzo -> mw : createSession
	mw -> omni : createSession
	omni -> mw : sessionId
	mw -> mezzo : sessionId 
    mezzo -> cms : sessionId 

    cms -> mezzo : createDraftOrder \n(sessionId, ban, address, equipmentId, installmentPlanId, msisdn)
	mezzo -> mw : createDraftOrder
	mw -> omni : createDraftOrder
	omni -> mw : orderId
	mw -> mezzo : orderId 
    mezzo -> cms : orderId 
    
    cms -> mezzo : getJumpOrderDetails(orderId)
	mezzo -> mw : getJumpOrderDetails(orderId)
	mw -> omni : getJumpOrderDetails(orderId)
	omni -> mw : OrderDetails 
	mw -> mezzo : OrderDetails 
    mezzo -> cms : OrderDetails 
    
	cms->ui : POIP Review Page
	
	
	ui -> cms: Enter card Details
	cms -> eos : ValidatePaymentMethod(EncryptedCardNumber)
	eos -> dps : ValidatePaymentMethod 
	dps->eos: Card Validation Response
	eos->cms: Card Validation Response
    cms->ui : Card Validation Response
    
    ui -> cms: Continue
	cms -> mezzo : updateOptionDetails\n(EncryptedCardNumber, ZIP, CVV, ExpDate)
	mezzo -> mw : updateOptionDetails
	mw -> omni : updateOptionDetails
	omni -> mw : payment method update status
	mw -> mezzo : payment method update status
    mezzo -> cms : payment method update status 


    cms-> eos : createEIPDisclosure
	eos -> xecm : /legal/v1/agreements/e-signature(OrderDetails)
	xecm -> eos : EIP signing document URL returned
	eos -> cms : EIP signing document URL returned
	
	cms->ui: EIP signing document URL returned
	ui->ui: Redirect to Document domain
	ui->ui: Redirect to MyTMO after signature
	ui->cms: Submit Order
    
	cms -> mezzo : confirmOrder(OrderId, DocumentId)
    mezzo -> mw : confirmOrder
    mw -> omni : confirmOrder
    omni->omni: CreateResidualLoan
    omni -> eip : eSigNotify\n/device-finance/v1/agreements/poip/esignature\n(documentId, agreementId)
	eip -> omni : EIP eSigNotify Status
	alt eSigNotify Success
    	omni -> mw : confirmOrder success
        mw -> mezzo : confirmOrder success	
        mezzo -> cms : confirmOrder success	
    		
    	cms -> mezzo : getJumpOrderDetails(OrderId)
    	mezzo -> mw : getJumpOrderDetails 	
    	mw -> mezzo : order details
    	mezzo -> cms : order details 
    	cms->ui: order confirmation page
	else eSigNotify Failure
	    omni->omni: cancelResidualLoan
	    omni -> mw : confirmOrder failure
        mw -> mezzo : confirmOrder failure	
        mezzo -> cms : confirmOrder failure	
    		
    	cms->ui: error page
	end
	
@enduml