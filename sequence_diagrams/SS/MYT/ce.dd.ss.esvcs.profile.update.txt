'See http://plantuml.com/sequence-diagram for details on how to create Sequence Diagrams in PlantUML'

@startuml
actor Customer
participant UI
participant CMS
participant "[[EOS_Profile]] \n ([[/swagger/ss.eos/v1/profileApi.yaml profileApi.yaml]])" as EOS_Profile
participant "[[EOS_Employee]] \n ([[/swagger/ss.eos/v1/profileApi.yaml employeeApi.yaml]])" as EOS_Employee
participant "[[EOS_Consent]] \n ([[/swagger/ss.eos/v1/eos-consents.yaml consentsApi.yaml]])" as EOS_Consent
participant "[[EOS_family_allowances]] \n ([[/swagger/ss.eos/v1/eos-family-allowances-api.yml familyAllowancesApi]])" as EOS_family_allowances
participant "[[EOS_Plans]] \n ([[/swagger/ss.eos/v1/submit_cps_swagger.yaml submit_cps_swagger.yaml]])" as EOS_Plans
participant Tibco
Customer -> UI: Login to MYTMO
Customer -> UI: Go to Profile


Activate UI
UI -> CMS: CustomerProfileServlet-updateProfile
Activate CMS
CMS -> EOS_Profile: PUT /v1/profile
Activate EOS_Profile
alt e911/ppu/billing address update
EOS_Profile -> Tibco: /validateAddress
EOS_Profile -> Tibco: /changeAddress
end
alt coverage address update
EOS_Profile -> Tibco: /validateAddress
EOS_Profile -> Tibco: /updateDevice
end
alt name update
EOS_Profile -> Tibco: /manageProfile
end
alt permissions update
EOS_Profile -> Tibco: /manageProfile
alt updating permission to NOACCESS
EOS_Profile -> Tibco: /manageProfile
note right: set logonStatus to DISABLED
end
alt updating permission FROM NOACCESS
EOS_Profile -> Tibco: /manageProfile
note right: set logonStatus to ENABLED
EOS_Profile -> Tibco: /manageProfile
note right: set role
end
end

alt success
EOS_Profile -> CMS: HTTP 200
else failure
EOS_Profile -> CMS: HTTP 500
end
Deactivate EOS_Profile
CMS -> UI: return profile data
Deactivate CMS


UI -> CMS: CustomerProfileServlet-updateFeatures
Activate CMS
CMS -> EOS_Profile: PUT /v1/profile/profileFeatures
Activate EOS_Profile
EOS_Profile -> EOS_Plans: /submit
alt success
EOS_Profile -> CMS: HTTP 200
else failure
EOS_Profile -> CMS: HTTP 500
end

Customer -> UI: Go to Privacy & Notifications
UI -> CMS: CustomerProfileServlet-updatePreferences
Activate CMS
CMS -> EOS_Profile: PUT /v1/profile/preferences/{msisdn}
Activate EOS_Profile
alt advertising update
EOS_Profile -> EOS_Consent: /consentGet
end
alt marketing update
EOS_Profile -> Tibco: /saveCustomerProfile
end
alt insights/newsletter update
EOS_Profile -> Tibco: /manageProgram
end
alt notification update
EOS_Profile -> Tibco: /updateSubscriberPrivileges
end
alt success
EOS_Profile -> CMS: HTTP 200
else failure
EOS_Profile -> CMS: HTTP 500
end
Deactivate EOS_Profile
CMS -> UI: return preferences data
Deactivate CMS

Customer -> UI: Go to Update Paperless
UI -> CMS: CustomerProfileServlet-updateBilling
Activate CMS
CMS -> EOS_Profile: PUT /v1/profile/billing
Activate EOS_Profile
EOS_Profile -> Tibco: /manageBillDeliveryFormat
alt success
EOS_Profile -> CMS: HTTP 200
else failure
EOS_Profile -> CMS: HTTP 500
end
Deactivate EOS_Profile
CMS -> UI: return paperlessBilling data
Deactivate CMS

Customer -> UI: Go to Family Controls
UI -> CMS: FAParentServlet-updateParent
Activate CMS
CMS -> EOS_family_allowances: PUT /v1/familyallowance/parent
Activate EOS_family_allowances
EOS_family_allowances -> Tibco: /setMAParent
alt success
EOS_family_allowances -> CMS: HTTP 200
else failure
EOS_family_allowances -> CMS: HTTP 500
end
Deactivate EOS_family_allowances
CMS -> UI: return parent info
Deactivate CMS

Customer -> UI: Go to Update Employee LineDesignation
UI -> CMS: EmployeeServlet-updateLinedesignation
Activate CMS
CMS -> EOS_Employee: PUT /v1/employee/linedesignation
Activate EOS_Employee
EOS_Employee -> Tibco: /updateLineDesignation
alt success
EOS_Employee -> CMS: HTTP 200
else failure
EOS_Employee -> CMS: HTTP 500
end
Deactivate EOS_Employee
CMS -> UI: return linedesignation data
Deactivate CMS

Deactivate UI

@enduml
