@startuml

actor TMOCustomer
participant TMobile_App
participant TMOID_SDK
participant RAS_BRASS
Participant EUI
participant IAM
participant Config_Service
participant EDGE

TMOCustomer->TMobile_App: Launch App
group Periodic (every 24hours)
TMobile_App->Config_Service: getConfigInfo
Config_Service->TMobile_App: App Configuation
end
TMobile_App->TMOID_SDK: getToken
TMOID_SDK->TMOID_SDK:showLoginScreen
TMOCustomer->TMOID_SDK:Enter Login credentials
TMOID_SDK->RAS_BRASS:getToken(UsernamePwd)
RAS_BRASS->EDGE: seederService
EDGE->EDGE:Retrieve data from\n downstream systems
RAS_BRASS->IAM:autheticateUsernamePwd
IAM->RAS_BRASS:authSuccess
RAS_BRASS->IAM:getProfileInfo
IAM->RAS_BRASS:ProfileInfo
RAS_BRASS->RAS_BRASS:formatIDtoken
RAS_BRASS->TMOID_SDK:IDToken
TMOID_SDK->TMobile_App:IDToken


@enduml