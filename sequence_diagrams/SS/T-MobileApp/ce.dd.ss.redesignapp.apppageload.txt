@startuml

actor TMOCustomer
participant TMobile_App
participant Bulwark
participant CardEngine
participant Chrome
participant FactsController
participant Heimdall
participant EDGE
participant IAM
participant eServices

==Login Successful, Fetch screen layout==
group Every app login 
TMobile_App-->Bulwark: getChrome
Bulwark->Bulwark:validatetoken (DAT, IDToken, PoP)
Bulwark->CardEngine:getChrome (pass IDToken entitlements as headers)
CardEngine->Chrome:getChrome
Chrome->CardEngine:ChromeConfig
CardEngine->TMobile_App:ChromeConfig
end

==Load App pages simultaneously==
TMobile_App->TMobile_App:Retrieve Chrome CTAs
activate TMobile_App
TMobile_App->Bulwark: getfeatureEligibility
TMobile_App->Bulwark: getCards_homepage
TMobile_App->Bulwark: getCards_AccountPage
TMobile_App->Bulwark: getCards_More
TMobile_App->eServices: getBillSummary
TMobile_App->eServices: getShop
deactivate TMobile_App

==Retrieve heimdall information (feature eligibility)==
Bulwark->Bulwark:validatetoken (DAT, IDToken, PoP) for all requests
Bulwark->Heimdall:getFeatureEligibility
Heimdall->TMobile_App: Send Messaging, CallUs eligibility
TMobile_App->TMobile_App: Update Messaging, \nCallUs icon based on eligiility


@enduml