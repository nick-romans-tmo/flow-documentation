@startuml

title T-Mobile Tuesdays - Get DoNotSell Settings
participant "TMT Client" as TmtClient
participant "TMT WebApi" as TmtWebApi
participant "TMT OAuth2" as TmtOAuth2
participant "TMT Redis Cache" as TmtRedisCache
participant "Apigee OAuth2" as ApigeeOAuth2
participant "Chub DoNotSell" as ChubWebApi

==TMT User Authenticating==
TmtClient->TmtOAuth2: Get TMT Bearer Access Token "/particpant/verify" (details omitted here)
TmtClient<--TmtOAuth2: TMT Bearer Access Token with ParticipantId idenity claim

==Get DoNotSell Settings==
TmtClient->TmtWebApi: "POST /profile/v1/consumer/get-do-not-sell-setting" - GetDoNotSellSettingsRequest
alt if 400BadRequest
    TmtClient<--TmtWebApi: HttpStatusCode.400BadRequest
else if 401Unauthorized
    TmtClient<--TmtWebApi: HttpStatusCode.401Unauthorized
else if 404NotFound
    TmtClient<--TmtWebApi: HttpStatusCode.404NotFound
else if 405MethodNotAllowed
    TmtClient<--TmtWebApi: HttpStatusCode.405MethodNotAllowed
else if 409Conflict
    TmtClient<--TmtWebApi: HttpStatusCode.409Conflict
else if 415UnsupportedMediaType
    TmtClient<--TmtWebApi: HttpStatusCode.415UnsupportedMediaType
else if 500InternalServerError
    TmtClient<--TmtWebApi: HttpStatusCode.500InternalServerError
else if 503ServiceUnavailable
    TmtClient<--TmtWebApi: HttpStatusCode.503ServiceUnavailable
end

TmtWebApi->TmtWebApi: Check DoNotSell Feature Flag
alt If DoNotSellFeatureFlag is disabled
    TmtClient<--TmtWebApi: HttpStatusCode.Locked "Feature Flag Not Enabled"
end

TmtWebApi->TmtWebApi: Get User.ParticipantId from TMT Bearer Access Token
TmtWebApi->TmtRedisCache: Get MSISDN from participantCachePassThru.GetParticipantSummaryWithLogin(participantId)
TmtWebApi<--TmtRedisCache: MSISDN

TmtWebApi->ApigeeOAuth2: Get Apigee Bearer Access Token using Key/Password "/oauth2/v4/tokens" 
TmtWebApi<--ApigeeOAuth2: Apigee Bearer Access Token with ParticipantId idenity claim
alt if 400BadRequest
    TmtWebApi<--ApigeeOAuth2: HttpStatusCode.400BadRequest
    TmtClient<--TmtWebApi: HttpStatusCode.400BadRequest, EngineStatusCode.ApigeeAccessTokenError (3000)
else if 401Unauthorized
    TmtWebApi<--ApigeeOAuth2: HttpStatusCode.401Unauthorized
    TmtClient<--TmtWebApi: HttpStatusCode.401Unauthorized, EngineStatusCode.ApigeeAccessTokenError (3000)
else if 404NotFound
    TmtWebApi<--ApigeeOAuth2: HttpStatusCode.404NotFound
    TmtClient<--TmtWebApi: HttpStatusCode.404NotFound, EngineStatusCode.ApigeeAccessTokenError (3000)
else if 405MethodNotAllowed
    TmtWebApi<--ApigeeOAuth2: HttpStatusCode.405MethodNotAllowed
    TmtClient<--TmtWebApi: HttpStatusCode.405MethodNotAllowed, EngineStatusCode.ApigeeAccessTokenError (3000)
else if 409Conflict
    TmtWebApi<--ApigeeOAuth2: HttpStatusCode.409Conflict
    TmtClient<--TmtWebApi: HttpStatusCode.409Conflict, EngineStatusCode.ApigeeAccessTokenError (3000)
else if 415UnsupportedMediaType
    TmtWebApi<--ApigeeOAuth2: HttpStatusCode.415UnsupportedMediaType
    TmtClient<--TmtWebApi: HttpStatusCode.415UnsupportedMediaType, EngineStatusCode.ApigeeAccessTokenError (3000)
else if 500InternalServerError
    TmtWebApi<--ApigeeOAuth2: HttpStatusCode.500InternalServerError
    TmtClient<--TmtWebApi: HttpStatusCode.500InternalServerError, EngineStatusCode.ApigeeAccessTokenError (3000)
else if 503ServiceUnavailable
    TmtWebApi<--ApigeeOAuth2: HttpStatusCode.503ServiceUnavailable
    TmtClient<--TmtWebApi: HttpStatusCode.503ServiceUnavailable, EngineStatusCode.ApigeeAccessTokenError (3000)
end

TmtWebApi->ChubWebApi: Get Apigee GetDoNotSellSettings - "/customer-profile/v1/consumer/get-do-not-sell-setting"
TmtClient<--TmtWebApi: Get DoNotSellSettings Response
alt if 400BadRequest
    TmtWebApi<--ChubWebApi: HttpStatusCode.ChubWebApi
    TmtClient<--TmtWebApi: HttpStatusCode.ChubWebApi
else if 401Unauthorized
    TmtWebApi<--ChubWebApi: HttpStatusCode.401Unauthorized
    TmtClient<--TmtWebApi: HttpStatusCode.401Unauthorized
else if 404NotFound
    TmtWebApi<--ChubWebApi: HttpStatusCode.404NotFound
    TmtClient<--TmtWebApi: HttpStatusCode.404NotFound
else if 405MethodNotAllowed
    TmtWebApi<--ChubWebApi: HttpStatusCode.405MethodNotAllowed
    TmtClient<--TmtWebApi: HttpStatusCode.405MethodNotAllowed
else if 409Conflict
    TmtWebApi<--ChubWebApi: HttpStatusCode.409Conflict
    TmtClient<--TmtWebApi: HttpStatusCode.409Conflict
else if 415UnsupportedMediaType
    TmtWebApi<--ChubWebApi: HttpStatusCode.415UnsupportedMediaType
    TmtClient<--TmtWebApi: HttpStatusCode.415UnsupportedMediaType
else if 500InternalServerError
    TmtWebApi<--ChubWebApi: HttpStatusCode.500InternalServerError
    TmtClient<--TmtWebApi: HttpStatusCode.500InternalServerError
else if 503ServiceUnavailable
    TmtWebApi<--ChubWebApi: HttpStatusCode.503ServiceUnavailable
    TmtClient<--TmtWebApi: HttpStatusCode.503ServiceUnavailable
end


==Set DoNotSell Settings==
TmtClient->TmtWebApi: "POST /profile/v1/consumer/set-do-not-sell-setting" - SetDoNotSellSettingsRequest
alt if 400BadRequest
    TmtClient<--TmtWebApi: HttpStatusCode.400BadRequest
else if 401Unauthorized
    TmtClient<--TmtWebApi: HttpStatusCode.401Unauthorized
else if 404NotFound
    TmtClient<--TmtWebApi: HttpStatusCode.404NotFound
else if 405MethodNotAllowed
    TmtClient<--TmtWebApi: HttpStatusCode.405MethodNotAllowed
else if 409Conflict
    TmtClient<--TmtWebApi: HttpStatusCode.409Conflict
else if 415UnsupportedMediaType
    TmtClient<--TmtWebApi: HttpStatusCode.415UnsupportedMediaType
else if 500InternalServerError
    TmtClient<--TmtWebApi: HttpStatusCode.500InternalServerError
else if 503ServiceUnavailable
    TmtClient<--TmtWebApi: HttpStatusCode.503ServiceUnavailable
end

TmtWebApi->TmtWebApi: Check DoNotSell Feature Flag
alt If DoNotSellFeatureFlag is disabled
    TmtClient<--TmtWebApi: HttpStatusCode.Locked "Feature Flag Not Enabled"
end

TmtWebApi->TmtWebApi: Get User.ParticipantId from TMT Bearer Access Token
TmtWebApi->TmtRedisCache: Get MSISDN from participantCachePassThru.GetParticipantSummaryWithLogin(participantId)
TmtWebApi<--TmtRedisCache: MSISDN

TmtWebApi->ApigeeOAuth2: POST "/oauth2/v4/tokens" - Get Apigee Bearer Access Token using Key/Password
TmtWebApi<--ApigeeOAuth2: Apigee Bearer Access Token (with ParticipantId idenity claim)
alt if 400BadRequest
    TmtWebApi<--ApigeeOAuth2: HttpStatusCode.400BadRequest
    TmtClient<--TmtWebApi: HttpStatusCode.400BadRequest, EngineStatusCode.ApigeeAccessTokenError (3000)
else if 401Unauthorized
    TmtWebApi<--ApigeeOAuth2: HttpStatusCode.401Unauthorized
    TmtClient<--TmtWebApi: HttpStatusCode.401Unauthorized, EngineStatusCode.ApigeeAccessTokenError (3000)
else if 404NotFound
    TmtWebApi<--ApigeeOAuth2: HttpStatusCode.404NotFound
    TmtClient<--TmtWebApi: HttpStatusCode.404NotFound, EngineStatusCode.ApigeeAccessTokenError (3000)
else if 405MethodNotAllowed
    TmtWebApi<--ApigeeOAuth2: HttpStatusCode.405MethodNotAllowed
    TmtClient<--TmtWebApi: HttpStatusCode.405MethodNotAllowed, EngineStatusCode.ApigeeAccessTokenError (3000)
else if 409Conflict
    TmtWebApi<--ApigeeOAuth2: HttpStatusCode.409Conflict
    TmtClient<--TmtWebApi: HttpStatusCode.409Conflict, EngineStatusCode.ApigeeAccessTokenError (3000)
else if 415UnsupportedMediaType
    TmtWebApi<--ApigeeOAuth2: HttpStatusCode.415UnsupportedMediaType
    TmtClient<--TmtWebApi: HttpStatusCode.415UnsupportedMediaType, EngineStatusCode.ApigeeAccessTokenError (3000)
else if 500InternalServerError
    TmtWebApi<--ApigeeOAuth2: HttpStatusCode.500InternalServerError
    TmtClient<--TmtWebApi: HttpStatusCode.500InternalServerError, EngineStatusCode.ApigeeAccessTokenError (3000)
else if 503ServiceUnavailable
    TmtWebApi<--ApigeeOAuth2: HttpStatusCode.503ServiceUnavailable
    TmtClient<--TmtWebApi: HttpStatusCode.503ServiceUnavailable, EngineStatusCode.ApigeeAccessTokenError (3000)
end

TmtWebApi->ChubWebApi: Set Apigee GetDoNotSellSettings - "/customer-profile/v1/consumer/set-do-not-sell-setting"
TmtClient<--TmtWebApi: Set DoNotSellSettings Response
alt if 400BadRequest
    TmtWebApi<--ChubWebApi: HttpStatusCode.ChubWebApi
    TmtClient<--TmtWebApi: HttpStatusCode.ChubWebApi
else if 401Unauthorized
    TmtWebApi<--ChubWebApi: HttpStatusCode.401Unauthorized
    TmtClient<--TmtWebApi: HttpStatusCode.401Unauthorized
else if 404NotFound
    TmtWebApi<--ChubWebApi: HttpStatusCode.404NotFound
    TmtClient<--TmtWebApi: HttpStatusCode.404NotFound
else if 405MethodNotAllowed
    TmtWebApi<--ChubWebApi: HttpStatusCode.405MethodNotAllowed
    TmtClient<--TmtWebApi: HttpStatusCode.405MethodNotAllowed
else if 409Conflict
    TmtWebApi<--ChubWebApi: HttpStatusCode.409Conflict
    TmtClient<--TmtWebApi: HttpStatusCode.409Conflict
else if 415UnsupportedMediaType
    TmtWebApi<--ChubWebApi: HttpStatusCode.415UnsupportedMediaType
    TmtClient<--TmtWebApi: HttpStatusCode.415UnsupportedMediaType
else if 500InternalServerError
    TmtWebApi<--ChubWebApi: HttpStatusCode.500InternalServerError
    TmtClient<--TmtWebApi: HttpStatusCode.500InternalServerError
else if 503ServiceUnavailable
    TmtWebApi<--ChubWebApi: HttpStatusCode.503ServiceUnavailable
    TmtClient<--TmtWebApi: HttpStatusCode.503ServiceUnavailable
end
@enduml