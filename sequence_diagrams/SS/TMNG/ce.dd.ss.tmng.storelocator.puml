@startuml 

!define markerIcon <img src="https://www.t-mobile.com/content/dam/t-mobile/storeLocator/tmobile-stores-default.png">
title TMNG Store Locator \n markerIcon \n Loading Scenarios and Key User Flows

skinparam backgroundColor #dcdfe5
skinparam roundcorner 10
skinparam ParticipantBorderColor Black
skinparam ParticipantBackgroundColor White
skinparam ActorBorderColor Magenta
skinparam ActorFontColor Magenta
skinparam ActorBackgroundColor White
skinparam padding 4
skinparam BoundaryBackgroundColor White

actor TMNGUser
participant UI [[https://www.t-mobile.com/store-locator]]
participant GoogleAPI [[https://developers.google.com/maps/documentation/geocoding/start]]
participant StoreLocatorV2API [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ss.tmng/swagger.json]]
participant AppointmentAPI [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ss.tmng/API_Swagger_TMNG_Store_Locator_API.yaml]]

== Initialization ==

TMNGUser -> UI : <b>User requests t-mobile.com/store-locator/*
UI -> AppointmentAPI : [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ss.tmng/API_Swagger_TMNG_Store_Locator_API.yaml GET]] /appointment/stores
AppointmentAPI --> UI : List of StoreIDs for Stores that accept appointments
UI -> UI: Check URL for deep link parameters

== Landing Page Load ==
alt /store-locator
UI -> UI : Check for TMNGUser geoLocation in browser
UI -> StoreLocatorV2API : [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ss.tmng/swagger.json GET]] /search(latitude, longitude, count, radius)
StoreLocatorV2API --> UI : Results
UI -> TMNGUser : <font color=green><b>Search Results view with Store Blades and Store Markers
alt User geoLocation is not allowed in browser
UI -> UI : Check for TMNGUser geoLocation in Adobe Target 
UI -> StoreLocatorV2API : [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ss.tmng/swagger.json GET]] /search(latitude, longitude, count, radius)
StoreLocatorV2API --> UI : Results
UI -> TMNGUser : Display results on map, search results blades, carousel (desktop only)
alt Adobe Target does not have Location
UI -> TMNGUser : Show default landing of US Map with no stores
end
end
end

== Deep Link Scenarios ==
alt /store-locator/wa
UI -> StoreLocatorV2API : [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ss.tmng/swagger.json GET]] /stores/{state} (state, storeType)
StoreLocatorV2API --> UI : List of Cities
UI -> TMNGUser : <font color=green><b>Search Results view with City Blades and Bubble Markers
alt 0 Results or Error
UI -> TMNGUser : <font color=red><b>Search Results view with No Results Message
end
end
note left
City List Response:
{
  "name": "washington",
  "abbr": "WA",
  "contextPath": "/store-locator/st",
  "cityList": [
    {
      "name": "bellevue",
      "displayName": "Bellevue",
      "storeCount": 10,
      "url": "/store-locator/st/city",
      "centerPoint": {
        "latitude": 40.467777,
        "longitude": -74.45364
      }
    }, {...}, {...}
  ]
}
end note

alt /store-locator/wa/seattle
UI -> StoreLocatorV2API : [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ss.tmng/swagger.json GET]] /stores/{state}/{city} (state, city, storeType)
StoreLocatorV2API --> UI : List of Stores
UI -> TMNGUser : <font color=green><b>Search Results view with Store Blades and Store Markers
alt 0 Results or Error
UI -> TMNGUser : <font color=red><b>Search results view with No Results Message
end
end

alt /store-locator/wa/seattle/downtown-seattle
UI -> StoreLocatorV2API : [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ss.tmng/swagger.json GET]] /stores/{state}/{city}/{store} (state, city, store)
StoreLocatorV2API --> UI : A Store
UI -> StoreLocatorV2API : [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ss.tmng/swagger.json GET]] /search (latitude, longitude, count, radius)
StoreLocatorV2API --> UI : List of Stores
UI -> TMNGUser : <font color=green><b>Store Details Page View with Nearby Stores
alt 0 Results or Error
UI -> TMNGUser : <font color=red><b>404 Page
end
end

note left
Store List Response Data:
[{
    "id": "2963A",
    "type": "RETAIL",
    "name": "T-Mobile North Brunswick",
    "telephone": "732-317-9170",
    "url": "/url/deeplink/to/store",
    "inStoreAppointment": true,
    "description": "Visit T-Mobile North Brunswick ...",
    "inStoreWaitTime": "5",
    "storeDistance": 5,
    "photos": [
      {
        "url": "/url/to/image",
        "description": "DAM image url for store photo",
        "details": "uncertain what this is",
        "alternateText": "Store Photo 1",
        "width": 65,
        "height": 64,
        "derivatives": [
          {
            "url": "https://www.derivative.photo.url",
            "width": 0,
            "height": 0
          }
        ]
      }
    ],
    "hours": [
      {
        "day": "Monday",
        "opens": "09:00",
        "closes": "21:00"
      }
    ],
    "holidays": [
      {
        "month": "December",
        "day": 25,
        "eventName": "Christmas",
        "hours": {
          "day": "Monday",
          "opens": "09:00",
          "closes": "21:00"
        }
      }
    ],
    "location": {
      "utcOffset": -5,
      "latitude": 40.467777,
      "longitude": -74.45364,
      "address": {
        "streetAddress": "695 Georges Rd Suite 100",
        "addressLocality": "North Brunswick",
        "addressRegion": "NJ",
        "postalCode": "08902-3330"
      }
    }
  }]
end note

alt /store-locator/wa/seattle/downtown-seattle/appointment or /get-in-line
UI -> StoreLocatorV2API : [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ss.tmng/swagger.json GET]] /stores/{state}/{city}/{store} (state, city, store)
StoreLocatorV2API --> UI : A Store
UI -> TMNGUser : <font color=green><b>Appointment or Get In Line flow for Store
alt 0 Result or Error
UI -> TMNGUser: <font color=red><b>404 Page
end
end

alt /store-locator?search="search string"
UI -> GoogleAPI : Request Latitude/Longitude for the search string captured from URL
GoogleAPI -> UI : Latitude/Longitude
UI -> StoreLocatorV2API : [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ss.tmng/swagger.json GET]] /search (latitude, longitude, count, radius)
StoreLocatorV2API --> UI : List of Stores
UI -> TMNGUser : <font color=green><b>Search Results view with Store Blades and Map Markers
alt No Lat/Lng from Google
UI -> TMNGUser : <font color=red><b>Mangles the URL and redirects to Marketing Landing Page
end
alt No Results or Error getting Stores
UI -> TMNGUser : <font color=red><b>Search results view with No Results Messaging
end
end

== Optional User Flows ==
opt User Executes a Search
TMNGUser -> UI : Input text into search field
UI -> GoogleAPI : Request Latitude/Longitude for the search string captured from search field
GoogleAPI -> UI : Lat/Lng
UI -> StoreLocatorV2API : [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ss.tmng/swagger.json GET]] /search (latitude, longitude, count, radius)
StoreLocatorV2API --> UI : List of Stores
UI -> TMNGUser : <font color=green><b>Search Results view with Store Blades and Map Markers
alt No Lat/Lng
UI -> TMNGUser : <font color=red><b>Mangles the URL and redirects to Marketing Landing Page
end
alt No Search Results for Lat/Lng
UI -> TMNGUser : <font color=red><b>Search results view with No Results Messaging
end
end

opt User Selects a Store
TMNGUser -> UI : Select a store from results
UI -> StoreLocatorV2API : [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ss.tmng/swagger.json GET]] /waitTime (storeId)
StoreLocatorV2API -> UI : Updated Wait Time for selected store
UI -> TMNGUser : <font color=green><b>Displays Store Details Page with Wait Time and Get In Line features
alt Receive non-numerical wait time or error
UI -> TMNGUser : <font color=red><b>Displays Store Details Page without Wait Time and Get In Line features
end
end
note left
Wait Time Response:
[
  {
    "StoreID": "104D",
    "Waittime": "5"
  }
]
end note
opt User Enters Get In Line Flow
TMNGUser -> UI: Click "Get In Line" CTA
UI -> StoreLocatorV2API : [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ss.tmng/swagger.json GET]] /reasons ()
StoreLocatorV2API -> UI : List of Reasons
UI -> TMNGUser : Get In Line Form
TMNGUser -> UI : Submit Get In Line Form
UI -> StoreLocatorV2API : [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ss.tmng/swagger.json POST]] /addCustomer ()
StoreLocatorV2API -> UI : Position In Line
UI -> TMNGUser : <font color=green><b>Confirmation page with Position In Line
alt Bad response or error
UI -> TMNGUser :  <font color=red><b>Form submission error messaging
end
end

note left
Reasons for Getting In Line:
[
  {
    "ReasonID": 45,
    "ReasonEN": "Accessory",
    "ReasonES": "Accesorio"
  }
]

Position In Line Response:
[
  {
    "Waittime": 0,
    "PositionInQueue": 1,
    "CustomerName": "Joe Smith"
  }
]
end note

opt User Enters Appointment Flow
note over TMNGUser, AppointmentAPI
Appointment Flows can currently be found on   [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/sequence_diagrams/SS/TMNG/ce.dd.ss.tmng.d2r.appointments.txt]]
end note
end

@enduml
