@startuml
actor UI
Participant APIGEE
Participant PCF
Participant EIP
Participant CasandraDB
'comment: actor boundary control entity
UI -> APIGEE: getLoanStatus Request
APIGEE -> PCF: getLoanStatus Request
PCF -> EIP : queryInstallmentHistory API req
EIP -> PCF :queryInstallmentHistory API resp
PCF -> CasandraDB: Query product Type from LoanView
CasandraDB -> PCF : Success
alt 
PCF -> EIP : queryInstallmentHistory API req
EIP -> PCF : Error response
PCF -> CasandraDB: Query product Type from LoanView
CasandraDB -> PCF : Error response
end
  
APIGEE  <-- PCF: getLoanStatus Response
UI  <-- APIGEE: getLoanStatus Response
@enduml
