@startuml
autonumber
title __Sequence of events for the Void Balance Transfer Reversal flow__
IHAPS -> PostLoan_MSA: activityType = <b>BALANCE_TRANSFER_REVERSAL
activate IHAPS
deactivate IHAPS
PostLoan_MSA -> PostLoan_Grid: Insert validated data
activate PostLoan_Grid
deactivate PostLoan_Grid
activate PostLoan_MSA
PostLoan_MSA -> "Global_Event_Log(GEL)": Update loan entry
activate "Global_Event_Log(GEL)"
"Global_Event_Log(GEL)" --> PostLoan_MSA: Success
deactivate "Global_Event_Log(GEL)"
PostLoan_MSA -> DEEP.io: Publish \nLoanVoidBalanceTransferReversalUpdate \nevent
deactivate PostLoan_MSA
activate DEEP.io
Payment_FinancePost -> DEEP.io: Consume \nLoanVoidBalanceTransferReversalUpdate \nevent
activate Payment_FinancePost
alt EventType = "LoanVoidBalanceTransferReversalUpdate"
	Payment_FinancePost -> GetPayments_MSA: Request all Payment Records
	activate GetPayments_MSA
	GetPayments_MSA --> Payment_FinancePost: Return all Payment Records
	deactivate GetPayments_MSA
	hnote over Payment_FinancePost: For eligible payment records
Payment_FinancePost -> PostPayment_MSA: Send \nPayment \nData (\batchReversals)
activate PostPayment_MSA
deactivate PostPayment_MSA
deactivate Payment_FinancePost
	end
PostPayment_MSA --> DEEP.io: Publish \nPaymentBatchReversalUpdate \nevent
activate PostPayment_MSA
deactivate PostPayment_MSA
note left: This event is common \nacross all Activity Types
Payment_Finance -> DEEP.io: Consume \nPaymentBatchReversalUpdate \nevent
activate Payment_Finance
	loop for each payment reversal record
	Payment_Finance -> Payment_Grid: Insert \nrecord into \nGetPayments Table
	activate Payment_Grid
	deactivate Payment_Grid
	rnote over Payment_Finance: Set syncPending flag = <b>TRUE 
	end
deactivate Payment_Finance
Loan_Finance -> DEEP.io: Consume \nLoanVoidBalanceTransferReversalUpdate \nevent
activate Loan_Finance
Loan_Finance -> GetLoan_Grid: Update \nCurrent Loan Data
rnote over Loan_Finance: Set syncPending flag = <b>TRUE
activate GetLoan_Grid
deactivate GetLoan_Grid
deactivate Loan_Finance
Loan_OFSLL -> DEEP.io: Consume \nLoanVoidBalanceTransferReversalUpdate event
activate Loan_OFSLL
Loan_OFSLL -> "Global_Event_Log(GEL)": Request records
activate "Global_Event_Log(GEL)"
"Global_Event_Log(GEL)" --> Loan_OFSLL: Send records
deactivate "Global_Event_Log(GEL)"
Loan_OFSLL -> Loan_OFSLL: Validate if thisVoid the loan account is \nthe first entry
alt First in queue
Loan_OFSLL -> OFSLL: Invoke \nOFSLL \nService
activate OFSLL
OFSLL -> OFSLL: Perform \nreversal of all Payment records \nfor the Loan ID
OFSLL -> OFSLL: Void the loan account
OFSLL --> Loan_OFSLL: Publish Success Response
deactivate OFSLL
Loan_OFSLL --> DEEP.io: Publish \nLoanVoidBalanceTransferReversalCompleted \nevent
Loan_OFSLL -> "Global_Event_Log(GEL)": Update record
activate "Global_Event_Log(GEL)"
deactivate "Global_Event_Log(GEL)"
else Not first in queue
Loan_OFSLL -> DEEP.io: Send exception
deactivate Loan_OFSLL
end
Loan_CFAM -> DEEP.io: Consume \nLoanVoidBalanceTransferReversalCompleted \nevent
activate Loan_CFAM
Loan_CFAM -> CFAM: Map to CFAM
activate CFAM
deactivate CFAM
deactivate Loan_CFAM
Loan_Finance -> DEEP.io: Consume \nLoanVoidBalanceTransferReversalCompleted \nevent
activate Loan_Finance
Loan_Finance -> GetLoan_Grid: Update \nrecords in \nGetLoan Tables
activate GetLoan_Grid
deactivate GetLoan_Grid
rnote over Loan_Finance: Set syncPending flag = <b>FALSE
deactivate Loan_Finance
Loan_SAP_PI -> DEEP.io: Consume \nLoanVoidBalanceTransferReversalCompleted \nevent
activate Loan_SAP_PI
Loan_SAP_PI -> SAP: Map to SAP
activate SAP
deactivate SAP
deactivate Loan_SAP_PI
Loan_CHUB -> DEEP.io: Consume \nLoanVoidBalanceTransferReversalCompleted \nevent
activate Loan_CHUB
Loan_CHUB -> CHUB: Map to CHUB
activate CHUB
deactivate CHUB
deactivate Loan_CHUB
PROMO -> DEEP.io: Consume \nLoanVoidBalanceTransferReversalCompleted \nevent
activate PROMO
deactivate PROMO
Payment_Finance -> DEEP.io: Consume \nLoanVoidBalanceTransferReversalCompleted \nevent
loop for each payment reversal record
	Payment_Finance -> Payment_Grid: Insert \nrecord into \nGetPayments Table
	activate Payment_Grid
	deactivate Payment_Grid
	rnote over Payment_Finance: Set syncPending flag = <b>FALSE 
	end
deactivate Payment_Finance
Payment_Streamline -> DEEP.io: Consume \nLoanVoidBalanceTransferReversalCompleted \nevent
activate Payment_Streamline
Payment_Streamline -> Streamline: Map to Streamline
activate Streamline
deactivate Streamline
deactivate Payment_Streamline
Loan_SCMS -> DEEP.io: Consume \nLoanVoidBalanceTransferReversalCompleted event
activate Loan_SCMS
Loan_SCMS -> SCMS: Map to SCMS
activate SCMS
deactivate SCMS
deactivate Loan_SCMS
Payment_Finance --> GetLoan_MSA: Invoke service by passing oldLoanId
activate Payment_Finance
note right: URI is (/customerfinance/v1/loans/{loanid})
activate GetLoan_MSA
GetLoan_MSA -> Payment_Finance: Return response
deactivate GetLoan_MSA
Payment_Finance -> Payment_Finance: Retrieve effectiveStartDate
Payment_Finance -> GetPayments_MSA: Retrieve records for oldLoanId
note right: URI is (<customerfinance>/v1/loans/{loanId}/payments)
activate GetPayments_MSA
GetPayments_MSA --> Payment_Finance: Return records
deactivate GetPayments_MSA
Payment_Finance -> Payment_Finance: Process records
Payment_Finance -> PostPayment_MSA: Send payment data
activate PostPayment_MSA
PostPayment_MSA -> DEEP.io: Publish \nLoanPaymentReversalUpdate \nevent
deactivate PostPayment_MSA
deactivate Payment_Finance
note left: Loan Payment Reversal Update event is published by Payments MSA \nonly when a record with Payment_Balxfer is present for the old loan id linked.
Payment_Finance -> DEEP.io: Consume \nLoanPaymentReversalUpdate \nevent
activate Payment_Finance
Payment_Finance -> Payment_Grid: Insert \nrecord into \nGetPayments Table
activate Payment_Grid
deactivate Payment_Grid
rnote over Payment_Finance: Set syncPending flag = <b>TRUE 
deactivate Payment_Finance 
Loan_OFSLL -> DEEP.io: Consume \nLoanPaymentReversalUpdate \nevent
activate Loan_OFSLL
Loan_OFSLL -> OFSLL: Invoke \nOFSLL \nService
activate OFSLL
OFSLL -> OFSLL: Perform \nreversal of Payment IDs \nhaving Payment_Balxfer
OFSLL -> OFSLL: Make the loan account active
OFSLL --> Loan_OFSLL: Publish Success Response
deactivate OFSLL
Loan_OFSLL --> DEEP.io: Publish \nPaymentReversalBalanceTransferReversalCompleted \nevent
deactivate Loan_OFSLL
Loan_CFAM -> DEEP.io: Consume \nPaymentReversalBalanceTransferReversalCompleted \nevent
activate Loan_CFAM
deactivate Loan_CFAM
Payment_Finance -> DEEP.io: Consume \nPaymentReversalBalanceTransferReversalCompleted \nevent
activate Payment_Finance
	Payment_Finance -> GetPayments_MSA: Invoke service
	activate GetPayments_MSA
	deactivate Payment_Finance	
	deactivate GetPayments_MSA
	rnote over Payment_Finance: Set syncPending flag = <b>FALSE
newpage Balance Transfer Reversal flow continued
Loan_Finance -> DEEP.io: Consume \nPaymentReversalBalanceTransferReversalCompleted \nevent
activate Loan_Finance
Loan_Finance -> GetLoan_Grid: Update \nrecords in \nGetLoan Tables
activate GetLoan_Grid
deactivate GetLoan_Grid
Loan_Finance -> DEEP.io: Publish \nLoanStatusUpdateCompleted event
note left: This event is published only when \nthere is a change in the status of the loan account.
deactivate Loan_Finance
Payment_Finance -> DEEP.io: Consume \nPaymentReversalBalanceTransferReversalCompleted \nevent
activate Payment_Finance
	Payment_Finance -> GetPayments_MSA: Invoke service
	activate GetPayments_MSA
	deactivate Payment_Finance	
	deactivate GetPayments_MSA
	rnote over Payment_Finance: Set syncPending flag = <b>FALSE
Loan_CHUB -> DEEP.io: Consume \nPaymentReversalBalanceTransferReversalCompleted \nevent
activate Loan_CHUB
Loan_CHUB -> CHUB: Map to CHUB
activate CHUB
deactivate CHUB
deactivate Loan_CHUB
Loan_SAP_PI -> DEEP.io: Consume \nLoanStatusUpdateCompleted \nevent
activate Loan_SAP_PI
Loan_SAP_PI -> SAP: Map to SAP
activate SAP
deactivate SAP
deactivate Loan_SAP_PI
Loan_CHUB -> DEEP.io: Consume \nLoanStatusUpdateCompleted \nevent
activate Loan_CHUB
Loan_CHUB -> CHUB: Map to CHUB
activate CHUB
deactivate CHUB
deactivate Loan_CHUB
PROMO -> DEEP.io: Consume \nLoanStatusUpdateCompleted \nevent
activate PROMO
deactivate PROMO
Loan_SCMS -> DEEP.io: Consume \nLoanStatusUpdateCompleted event
deactivate DEEP.io
activate Loan_SCMS
Loan_SCMS -> SCMS: Map to SCMS
activate SCMS
deactivate SCMS
deactivate Loan_SCMS
@enduml