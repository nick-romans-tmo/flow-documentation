@startuml
autonumber
title Refresh Credit Check - After manual review or credit application run in TOGA UI
participant UI
participant APIGEE
box "Credit Domain" #lightblue
participant PCF
database CFAM
end box
participant TOGA
autonumber

UI -> APIGEE: [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.cfs.credit.co/consumerCreditReports_V3.0.json getcreditReports_V3.0 (CRID)]]
APIGEE -> PCF: [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.cfs.credit.co/consumerCreditReports_V3.0.json getcreditReports_V3.0 (CRID)]]
PCF-> TOGA: RefreshCreditApplication (CRID)
TOGA -> TOGA: Retrieve credit details for input CRID
TOGA -> PCF: CIRD, CreditClass, Credit Date
PCF -> PCF: Merge responses and apply business logic
PCF --> APIGEE: Return CRID, CreditClass, Credit Date
APIGEE --> UI: Return CRID, CreditClass, Credit Date
@enduml