@startuml
autonumber
title Return list of accepted id types for running credit check
participant UI
participant APIGEE
box "Credit Domain" #lightblue
participant PCF
database CFAM
end box
autonumber

UI -> APIGEE: [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.cfs.credit.co/creditIdTypes_V1.0.json creditIDTypes_V1.0]]
APIGEE -> PCF: [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.cfs.credit.co/creditIdTypes_V1.0.jsonn creditIDTypes_V1.0]]
PCF -> CFAM: credit id details
CFAM -> CFAM: Fetch Data
CFAM -> PCF: Return customer id types
PCF --> APIGEE: Return customer id types
APIGEE --> UI: Return customer id types
@enduml