@startuml
autonumber
title TempDouble
participant REP
participant OFSLLUI
participant EIP
participant CreditDomain

REP -> OFSLLUI : tempDouble
OFSLLUI -> OFSLLUI: return ECLX2
OFSLLUI-> EIP: tempDouble API
EIP -> EIP: Calculate MPEC, ECL, ECA
EIP -> OFSLLUI: Return tempDouble Eligibility
OFSLLUI -> REP: Return tempDouble Eligibility
EIP -> CreditDomain:[[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.cfs.credit.co/tempDoubleECL_V1.0.json tempDoubleECL_V1.0(store BAN)]]
@enduml