@startuml
autonumber
title The API returns the credit class, line limits and deposits information for an existing customer.
participant UI
participant APIGEE
participant PCF
participant Samson

UI -> APIGEE: customerCreditLimits
APIGEE -> PCF: customerCreditLimits
PCF-> Samson: customerCreditLimits(accountNumber)
Samson-> PCF: Return Data (credit class,linelimits, deposit information)
PCF->PCF: Merge response and apply business logic
PCF --> APIGEE: Return credit class Details
APIGEE --> UI: Return credit class Details
@enduml