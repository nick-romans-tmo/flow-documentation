@startuml
autonumber
title Return list of credit class override reasons 
participant UI
participant APIGEE
box "Credit Domain" #lightblue
participant PCF
database CFAM
end box
autonumber

UI -> APIGEE: [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.cfs.credit.co/creditClassOverrideReasons_V1.0.json creditClassOverrideReasonse_V1.0]]
APIGEE -> PCF: [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.cfs.credit.co/creditClassOverrideReasons_V1.0.json creditClassOverrideReasonse_V1.0]]
PCF -> CFAM: customer details
CFAM -> CFAM: Fetch Data
CFAM -> PCF: Return override reasons
PCF --> APIGEE: Return override reasons
APIGEE --> UI: Return override reasons
@enduml