@startuml
title First Time Usage Flow
participant TIBCO
participant APIGEE
participant DFS
participant BANK
autonumber

TIBCO -> APIGEE: First Time Usage service_Token service
APIGEE -> APIGEE: Authenticate
APIGEE -> APIGEE: Generate JWT Token 
APIGEE -> DFS: First Time Usage service_V1
DFS-> DFS: Validate JWT Token
DFS-> TIBCO: service responce
DFS -> BANK: Integration_FirstTimeDeviceActivity
@enduml