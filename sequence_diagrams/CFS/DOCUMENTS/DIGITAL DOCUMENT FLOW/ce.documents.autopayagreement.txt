@startuml
actor Client
'comment: actor boundary control entity
Client -> autoPayAgreement: Client invokes autoPay API /legal/v1/agreements/autopay [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.cfs.documents/autopayagreements.swagger.json Swagger]]
note right of Client : method: POST 
autoPayAgreement -> autoPayAgreement: Validates the request
alt Validation Fails		
   autoPayAgreement -> Client: Returns error message
else Validation Success
   autoPayAgreement -> "OTSS" : Sends request to create HTML document
end
OTSS -> OTSS: Validates the request and create the HTML document
OTSS -> autoPayAgreement : sends response with HTML document
autoPayAgreement -> OracleDB : requests payload, document type, channel id (from header)
OracleDB -> autoPayAgreement : returns ingestId
autoPayAgreement -> Client : sends response with ingestId and HTML document
alt If User Denies	
   Client -> Client: No further request
else If User Agrees
   Client -> "autoPayAgreement" : Requests acceptanceId (it is same ingestId) and signatureId (time stamp) along with returnContent = true/false
end
note right of Client : method: PUT 
autoPayAgreement -> autoPayAgreement: reads and validates the request using acceptanceId and signatureId
alt If if transaction_status is AGREEMENT_INGEST_COMPLETE or AGREEMENT_BIND_COMPLETE 	
   autoPayAgreement -> Client: Throws exception. As agreement is already binded with signature
else else
   autoPayAgreement <- "OracleDB" : gets request payload from DB 
end
note right of autoPayAgreement : If app can’t retrieve data from Db then throw exception to client
autoPayAgreement -> OTSS : appends with signatureId and requestType = pdf
OTSS -> autoPayAgreement : generates and sends the response with pdf bytes along with signatureId appended
autoPayAgreement -> preserveAgreement : requests with PDF bytes
preserveAgreement -> OTCS : requests to save the PDF bytes
OTCS -> preserveAgreement : saves the pdf bytes and returns a docId
preserveAgreement -> autoPayAgreement : returns docId
autoPayAgreement -> OracleDB : saves PDF bytes and docId and update transaction status to AGREEMENT_INGEST_COMPLETE
autoPayAgreement -> Client : returns the successful response along with docId
@enduml