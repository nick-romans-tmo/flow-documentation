@startuml
autonumber
title CDES 2.0 Collection message flow \n \n
activate CDES
CDES -> COLLECTION_API:CDES post DD/PC \n message to collection API \n 
COLLECTION_API -> TRIAD_API: Collection API post DD/PC \n message to TRIAD API \n
TRIAD_API -> TRIAD : TRIAD API post DD/PC \n message to TRIAD(FICO) \n
TRIAD -> TRIAD_API : TRIAD sent back response\n to TRIAD API \n
TRIAD_API -> COLLECTION_API : TRIAD API sent back response\n to Collection API \n
COLLECTION_API ->CDES: Collection API sent back response\n to CDES \n
@enduml