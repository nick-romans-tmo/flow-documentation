@startuml

autonumber
activate CDES
CDES -> TRIAD: Send Collections Data for collection risk assessment
TRIAD -> TRIAD: Apply Exclusions
TRIAD -> TRIAD: Classify the account
TRIAD -> TRIAD: Perform Scoring
TRIAD -> TRIAD: Execute Collection Strategies
TRIAD -> TRIAD: Evaluate Collection Risk
TRIAD -> CDES: Send Collection Response 
CDES -> CDES: Store Collection Response
CDES -> Samson: Send Collection Response
Samson -> Samson: Validate Collection Response
Samson -> CDES: if invalid: Send Reject Response
Samson -> Samson: if valid: Update Collection Response
CDES -> CDES: Update Reject Response

@enduml