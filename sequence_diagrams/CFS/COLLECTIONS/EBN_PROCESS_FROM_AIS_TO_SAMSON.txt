@startuml
autonumber
title EBN_PROCESS_FROM_AIS_TO_SAMSON \n \n
AIS -> SFG: Bankruptcy Vendor creates and sends \n 2 pipe delimited files \n <b>*TMOEBNYYYYMMDD.txt \n <b>*TMOBKOALLYYYYMMDD.txt
SFG -> OCB: SFG receives the 2 pipe delimited files \n from AIS and Sends to OCB
OCB -> CENTIVIA: Perform De-duplication & Account looks uo \n on 2 pipe Delimited files \n <b>*TMOEBNYYYYMMDD.txt \n <b>*TMOBKOALLYYYYMMDD.txt
OCB -> CENTIVIA: Produce Single Output File \n for Centivia to Process \n (AIS_Centivia_YYYYMMDD.txt)
OCB -> CENTIVIA: Send Single EBN consolidated file to Centivia
CENTIVIA -> SAMSON: Receives single EBN consolidated file from OCB
CENTIVIA -> SAMSON: Centivia runs production job \n <b> *AIS_BANKRUPTCY_AUTOMATION </b> \n to assign treatment skills set for each transaction
CENTIVIA -> SAMSON: For manual treatment skill sets, Create 1 extract file per skill set
CENTIVIA -> SAMSON: Centivia SFTPs extract files and summary email to OCB
CENTIVIA -> OCB: OCB Processes Extract Files 
CENTIVIA -> SAMSON: Centivia calls Texedo services for update \n Samson for each treatment skillset
CENTIVIA -> SAMSON: Centivia sends a summary email along with the files \n A) POC_AddFile_Centivia_MMDDYYYY.csv \n B) OCB-BK_Skill_Assignments \n C)OCB-BK_Automation_errorsYYYYMMDD.csv
SAMSON -> SAMSON: Samson updated treatment skillsets
@enduml