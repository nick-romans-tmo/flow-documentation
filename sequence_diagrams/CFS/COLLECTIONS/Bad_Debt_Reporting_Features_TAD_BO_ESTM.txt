@startuml
skinparam sequenceArrowThickness 3

CDES-> TRIAD_UNIX:\nAfter CDES goes to offline state\n Run ([[https://qwiki.internal.t-mobile.com/display/FD/Operations+Support+POC a09WKLYT.sh)]]
TRIAD_UNIX <- TRIAD_UNIX: \n a09WKLYT.sh daily job FAILED 
TRIAD_UNIX <- CDES: \n In case of Error, logs can be found \n/opt/triad86/triad01/triad860/t860auto/triadcyc-XXX(Current Cycle)/log/a11log \n Debug the issue and \nRun ([[https://qwiki.internal.t-mobile.com/display/FD/Operations+Support+POC a09WKLYT.sh)]]
CDES <-TRIAD_UNIX:\n ([[https://qwiki.internal.t-mobile.com/display/FD/Operations+Support+POC a09WKLYT.sh daily job)]] Completed successfully
TRIAD_UNIX-> NFS:\nLocation : opt/triad86/triad860/t860auto/dbload/9999 \n Creates \nLinkage.daily \n Samrecc.daily \nLinkageDataCount.CSV \nReportRecordCount.CSV
NFS <- NFS:
TRIAD_UNIX-> NFS:\nDaily Batch Jobs(TIDAL)(BOT1_FTP_TAD_Consolidate_Data*) \n moves files to common share\n \gsm1900.org\dfsroot\DataTransfer\TAD_IMPORT_DATA
NFS-> TAD_WINDOWS: DAILY 10:00 AM \nWindows Scheduled job copies files to \nlocal TAD directory 
TAD_WINDOWS-> TAD_DB: Scheduler jobs for TAD to import files.\n Linkage file is scheduled for 1 PM PST \n RR file is scheduled for 6 PM EST. 
TAD_DB<- TAD_DB: \n
TAD_WINDOWS-> TAD_DB:\n TAD Scheduler Import loads \nLinkage.daily \n Samrecc.daily
TAD_DB<- TAD_DB: \n
TAD_WINDOWS -> TAD_DB: \n Business to run reports to evaluate \nScore card and \ndata driven strategies
TRIAD_UNIX-> NFS: Run ([[https://qwiki.internal.t-mobile.com/display/FD/Operations+Support+POC a10MONTH.sh)]]\n Located at : /opt/triad86/triad/triad860/t860data/LDEST_Output \n needs to complete prior running Estimator \n Creates single Linkage file called LDEST \nwhich is a concatenation of every daily linkage file from previous month and\n used as a input to A11 Estimator Script
NFS <- NFS: \n a10MONTH.sh job FAILED
TRIAD_UNIX-> NFS:In case of Error, logs can be found \n/opt/triad86/triad01/triad860/t860auto/triadcyc-XXX/log/a10log \n Debug the issue and \nRun ([[https://qwiki.internal.t-mobile.com/display/FD/Operations+Support+POC a10MONTH.sh)]]
TRIAD_UNIX-> NFS: Run ([[https://qwiki.internal.t-mobile.com/display/FD/Operations+Support+POC a11ESTIM.sh)]]\n Success message when the script completes successfully \n[a11ESTIM]> Estimator process completed successfully \nUpon completion of the a11ESTIM.sh the following ‘estm’ folder \nlocated at: t/triad86/triad/triad860/t860auto/dbload/9999/estm \n will need to be copied to a shared location
NFS <- NFS: \n a11ESTIM.sh job FAILED
TRIAD_UNIX-> NFS:In case of Error, logs can be found \n/opt/triad86/triad01/triad860/t860auto/triadcyc-XXX/log/a11log \n Debug the issue and \nRun ([[https://qwiki.internal.t-mobile.com/display/FD/Operations+Support+POC a11ESTIM.sh)]]
NFS ->ESTIMATOR_WINDOWS: \nCopy Estimator files from shared drive to a \nPCTMS windows shared server folder \n /dbload/data/estimator which contains two subfolders (tally and control) \n Login into PCTMS and select to load 'Estimator Reports - All decision Areas'
TAD_DB-> FRRP: Reporting Report Record Segmentation
TAD_DB-> BO: \n Login to Desktop Intelligence \nImport from Repository to get your list of Estimator report templates \n Load BO reports
@enduml
