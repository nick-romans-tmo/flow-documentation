@startuml
actor "Frontend App" as TOM
participant "APIGEE" as APIGEE
participant "SAPPI" as SAPPI
participant "SAPECC" as SAPECC
Activate TOM
TOM->APIGEE:Is going to initiate getOrderList Request for a Frontend order#
Activate APIGEE
APIGEE->SAPPI:getOrderList Request
Activate SAPPI
SAPPI->SAPECC:getOrderList Request
Activate SAPECC
SAPECC->SAPECC:will Query Return Orders associated with frontend order ( RA & Blind Receipts)
alt success
SAPECC-->SAPPI:getOrderList response key attributes SAPOrderID, date, ordertype, status, IMEI, SKU, credit Amount, Is return eligible
else 
SAPECC-->SAPPI: error-Order details not found
end
alt success
SAPPI-->APIGEE:getOrderList response containing SAP Order details
else
SAPPI-->APIGEE:Order details not found
end
APIGEE-->TOM:getOrderList response
@enduml
