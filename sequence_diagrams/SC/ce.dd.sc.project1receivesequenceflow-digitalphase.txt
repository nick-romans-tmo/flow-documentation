@startuml
title __RECEIVE CAPABILITY SEQUENCE FLOW (Digital Transformation Phase)__
box "T-Mobile" #FFFFFF
participant SoA
participant SoA_DeliveryAPI
participant SoA_ReceiveAPI
participant DEEP.IO
participant SAP_PI
participant SAP_ECC
participant SAP_BW
participant BI
end box

box "FedEx" #FFFFFF
participant Middleware
participant WM
end box

autonumber
WM -[#blue]> Middleware: Dock Receipt
Middleware -[#blue]> SoA_DeliveryAPI: POST/Dock Receipt
SoA_DeliveryAPI -[#blue]> SoA: Process Dock Receipt
SoA -[#blue]> DEEP.IO: Box Delivered Notification

WM -[#000000]> Middleware: Receipt/sku change
Middleware -[#blue]> SoA_ReceiveAPI: POST/Receipt & sku change
SoA_ReceiveAPI -[#C0C0C0]-> Middleware: http status code
SoA_ReceiveAPI -[#blue]> SoA: Process Receipt & sku change
SoA -[#green]> SoA: Process Receipt/Change inventory state
SoA -[#green]> SoA: Close Transfer 

SoA -[#green]> DEEP.IO: Publish Store2DC receipt/sku change event
SAP_PI -[#blue]> DEEP.IO: Subscribe Store2DC receipt/sku change event
SAP_PI -[#blue]> SAP_ECC: Publish Store2DC receipt/sku change
SAP_ECC -[#green]> SAP_ECC: Process Receipt/Change inventory state


BI -[#green]> DEEP.IO: Subscribe Store2DC Receipt event

SAP_ECC -[#000000]> SAP_BW: Publish{legacy}/receipt data
SAP_BW -[#000000]> BI: Subscribe{legacy}/receipt data

note over SoA, SoA_DeliveryAPI #FFFFFF
	black = legacy
	green = RIS integration
	blue = Project1 integration
end note

@enduml
