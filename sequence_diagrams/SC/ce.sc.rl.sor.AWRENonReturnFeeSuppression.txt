@startuml
title __AWRE [Device Handling Journey]__

actor "Customer" AS CUS
participant "Care" AS CAR
participant "Distribution Center" AS DC
participant UPS
participant "Return Center" AS RC
participant "SAP ECC" AS ECC
participant "SAP OER" AS OER
participant HRT
participant EIP

autonumber
CUS -> CAR: Request Warranty Exchange
CAR -> ECC: Create forward order
ECC -> DC: Send delivery
DC -> ECC: Send ship confirmation
ECC -> EIP: Update IMEI
ECC -> ECC: Create RA
ECC -> RC: Create return delivery
alt Happy Path
DC -> CUS: Ship replacement device
CUS -> RC: Ship damaged device
RC -> ECC: Post Receipt
ECC -> ECC: Receive and fulfill RA
ECC -> OER: Post event Z_RETURN
else Undeliverable
DC ->x CUS: Device not delivered
alt Rejected/Returned
DC --> UPS: Routed to UPS hub
UPS -> RC: Rejected/Returned
alt within 45/config days
RC -> ECC: Blind Receipt 
ECC -> OER: Post event Z_RETURN
ECC -> ECC: Close RA for defective device
ECC -> EIP: Update IMEI and SKU details
else after 45/config days
RC -> ECC: Blind receipt
ECC -> OER: Post event Z_RECEIVE_GOODS
end
else Device lost in transit
end
note over ECC: after 45(configurable) days and defective device not returned
alt Replacement device received at Return Center
ECC -> ECC: Check replacement IMEI events Z_RECEIVE_GOODS/Z_RETURN > Z_SHIPMENT
note over ECC: suppress NRF
else Replacement device not delivered to customer
ECC <-> SOA: Check tracking status Delivered missing
note over ECC: suppress NRF
else 
ECC -> CUS: Charge Non-Return Fee
end
end
@enduml