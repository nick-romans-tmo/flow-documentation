@startuml

actor CUST AS "Customer"
actor MBXP AS "Mobile Expert"
participant RETAIL AS "Store/Customer"
participant QVXP
participant OMD AS "Order Management"
participant DICE
participant DMW
participant SAPECC AS "SAP"
participant JDC AS "Distribution Center"
participant RDC AS "FedEx Return Center"
participant TETRA AS "Device Attributor"
participant SAPFI AS "SAP Finance"

CUST -> MBXP: Request warranty exchange
MBXP -> QVXP: Perform checks and initiate exchange
QVXP -> DICE: Get exchange eligibility and replacement \n(getDeviceEligibility, getReplacementDevice)
DICE -> DMW: Get warranty details
DICE -> WEM: Get replacement details
DICE -> QVXP: Return eligibility and replacement devices
QVXP -> OMD: Order submit
OMD -> SAPECC: Submit Forward Order (Replacement device)
SAPECC -> JDC: Submit Delivery order
JDC -> SAPECC: Ship confirmation
SAPECC -> RDC: Return authorization/STO
JDC -> RETAIL: Deliver replacement device
RETAIL -> RDC: Ship defective device
RDC <-> TETRA: Device disposition
RDC -> SAPECC: Receipt confirmation
SAPECC -> SAPFI: Post accounting documents
@enduml