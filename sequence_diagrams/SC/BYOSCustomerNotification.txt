@startuml
participant "SAPECC" as SAPECC
participant "SAPPI" as SAPPI
participant "DEEP.Io" as DEEP
participant "DND" as DND
Activate SAPECC
Activate SAPPI
Activate DEEP
SAPECC->SAPECC: Trigger Notification event 
SAPECC->SAPPI: Trigger Notification details as event 'BYOSCustomerNotification'
SAPPI->DEEP:  Publish event 'BYOSCustomerNotification'
DEEP->DND: Consume event 'BYOSCustomerNotification'
@enduml