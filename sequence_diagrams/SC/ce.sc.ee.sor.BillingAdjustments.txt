@startuml
title __Billing Adjustments [Device Handling Journey]__

participant "Frontend/Order Management" AS OM
participant "SAP Order Fulfilment" AS ECC
participant "SAP Finance" AS FI
participant "SAP PI" AS PI
participant "Distribution Center" AS JDC
participant "Return Center" AS RDC
participant "TIBCO" AS MW
participant "SAMSON" AS SAM

autonumber
group Forward Order
OM -> PI: Submit Forward Order 
PI -> ECC: Submit Order
ECC -> PI: Submit Delivery
PI -> JDC: Post Delivery
JDC -> PI: Goods Issue Confirmation
PI -> ECC: Post Goods Issue
ECC -> FI: Create Billing
FI -> PI: Charge Customer \n (si_EquipmentInvoice_Out)
PI -> MW: Send Charge \n (/Equipmentdetails/Post)
MW -> SAM: Create Charge \n (Tuxedo API evCrInvCM)
end
group Return Order
OM -> PI: Submit Return Order 
PI -> ECC: Submit Order
ECC -> PI: Submit Delivery
PI -> RDC: Post Delivery
RDC -> PI: Goods Receipt Confirmation
PI -> ECC: Post Goods Receipt
ECC -> FI: Create Credit Memo
FI -> PI: Refund Customer \n (si_EquipmentInvoice_Out)
PI -> MW: Send Adjustment \n (/Equipmentdetails/Post)
MW -> SAM: Apply Adjustment at BAN \n (Tuxedo API evCrInvCM)
end
group Customer Credit
OM -> PI: Submit Customer Credit 
PI -> ECC: Submit Credit
ECC -> FI: Create Credit Memo
FI -> PI: Refund Customer \n (si_EquipmentInvoice_Out)
PI -> MW: Send Adjustment \n (/Equipmentdetails/Post)
MW -> SAM: Apply Adjustment at BAN \n (Tuxedo API evCrInvCM)
end
@enduml