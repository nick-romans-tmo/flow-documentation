@startuml
title __Supply Chain Device Activations__

actor "Customer" AS CUST
participant "Frontend/Order Management" AS OM
participant "SAP ECC" AS SAPECC
participant "SAP PI" AS SAPPI
participant "Distribution Center" AS JDC
participant "TIBCO" AS TIBCO
participant "RSP" AS RSP
participant "Samson" AS SAM
participant "Startek" AS STK

autonumber
group Abstracted flow
CUST -> OM: Place Order
OM -> SAPPI: Submit Order 
SAPPI -> SAPECC: Submit Order
SAPECC -> SAPPI: Submit delivery
SAPPI -> JDC: Post delivery
JDC -> CUST: Ship device
JDC -> SAPPI: Goods issue confirmation
SAPPI -> SAPECC: Post goods issue
end
SAPECC -> RSP: Generate and send activation file\nLocation: \\gsm1900.org\dfsroot\AS\VPR_INTF\activation\nProgram: ZTISD_ACTIVATION_REQUEST_5MIN\n Service [[https://servicecatalog.internal.t-mobile.com/servicecat/rsp/2018-12-08/sid/html/ActivationService.html#activateSubscribers ActivateSubscribers]]
alt Online activation
RSP <-> SAM: Activate device
else Offline activation
RSP <-> STK: Activate device
end
TIBCO -> SAPPI: Activation response
SAPPI -> SAPECC: Confirm activation
SAPECC -> CUST: Send activation confirmation
@enduml