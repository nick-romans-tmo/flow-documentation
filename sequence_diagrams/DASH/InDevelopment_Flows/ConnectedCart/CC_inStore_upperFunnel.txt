@startuml 
actor REP
Group Rep login
note over DASH
	Retail -
	Tapestry login by Rep using creds
	Starts in QV and pass the token to DASH
	Store address discussion
end note


REP -> DASH: Rep provides login creds
note over DASH
	DASH calls OKTA via SDK directly
end note
DASH -> OKTA : authentication & authorization
OKTA --> DASH : response
DASH --> REP : response
end

Group Customer Lookup & Verification
note over DASH
	Retail -
	Customer lookup can happen in QV/Tapestry (Introduce Alt flow)
end note
note over DASH
	Retrieve customer details
	Get JWT token representing customer
end note
REP -> DASH : customer search invoked
DASH -> APIGEE : register token to APIGEE
APIGEE -> CustomerDomain : customer search request 
CustomerDomain--> APIGEE : response
APIGEE --> DASH : response
DASH --> REP : response & customer verification
end

Group Landing Page
note over DASH
	Retrieve account details
end note
REP -> DASH : Account Overview page
DASH -> CustomerDomain : Account Details/credit Check/EIP creditLine/subscriber's List/subscriber's Info etc.
CustomerDomain --> DASH : response
DASH -> DICE : credit Profile/Profile/installment History etc.
DICE --> DASH : response
DASH --> REP : account page
end

Group Upgrade eligibility
note over DASH
	BAN level check  -- probable Performance impact
end note
REP -> DASH : Rep on line selector page
DASH -> OMD : Eligibility check request DASH -> OMD : Eligibility check request (Upgrade/Jump/JOD eligibility)
OMD--> DASH : Eligibility response
DASH --> REP : response 
end

Group Upgrade journey start
REP -> DASH : Rep starts browsing
DASH -> DCP : Tokens Registration: [[../../DCP/ce.dsg.dcp.authtoken.svg Post]] /tokens - [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v3.1/Authentication_Swagger.json swagger]]
DCP --> DASH : response
end

Group Browse Products
note over DASH
	DASH - POS project is setting up the product catalog data in DASH cache
end note
DASH -> DASH : Products catalog data from cache
alt Data not found in cache
DASH -> MPM : Pull products data
MPM --> DASH : warm up cache with products data 
end
DASH -> DCP : products selling contextual data: [[../../DCP/ce.dsg.dcp.products.svg POST]] /v3/product/{product-id} - [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v3.1/Products_Swagger.json swagger]]
DCP --> DASH : contextual data (FRP/EIP/Lease pricing specific to customer and availability status & Promotions)
DASH --> REP : PDP/PLP page display
end

Group upgrade cart flow
REP -> DASH : Rep adds to cart
DASH -> DCP : get cart: [[../../DCP/ce.dsg.dcp.getcart.svg Get]] /Cart - [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v3.1/Cart_Swagger.json swagger]]
DASH -> DCP : create cart line:  [[../../DCP/ce.dsg.dcp.createline.svg POST]] /carts/{cartID}/lines  -[[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v3.1/Cart_Swagger.json swagger]]
note over DASH
	Retail - DCP to supplyChain for inventory validate during device scan
end note
DASH -> DCP : add device to cart: [[../../DCP/dcp/ce.dsg.dcp.adddevice.svg POST]]/{cart-id}/lines/{line-id}/device [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v3.1/Cart_Swagger.json swagger]]

alt Add Accessory
DASH -> DCP : add accessory: [[../../DCP/dcp/ce.dsg.dcp.addaccessory.svg POST]] /{cart-id}/lines/{line-id}/accessories [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v3.1/Cart_Swagger.json swagger]]
end

note over DASH
	trade-in
	inline claims for trade-ins. (scenario: under warranty)
	Ship-to : apply trade-in FMV to previous loan/no loan then bill credit (accept-deferred scenario)
	In Retail -- Override trade-in device conditions, separate call invocations for JOD/JUMP trade-in flows. Update inventory details(Accept scenario)
end note
opt trade-in flow
DASH -> Device : check trade-in device eligibility
DASH -> DCP : post trade-in details to cart: [[../../DCP/ce.dsg.dcp.posttradein.svg Post]] POST /carts/{cartID}/lines/{lineId}/trade-ins - Add Trade-in information to cart - [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v3.1/Cart_Swagger.json swagger]]
end
note over DASH
	Payoffs
	eligibility check -- multi team discussion needed
	order Submit vs order shipped payoff scenarios
	Rules around JUMP/std upgrade
end note
opt Payoffs
DASH -> DCP : Post payoffs: /{cart-id}/lines/{line-id}/finance-payoffs [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v3.1/Cart_Swagger.json swagger]]
end
opt optional services
DASH -> DCP : get optional-services for line: [[../../DCP/ce.dsg.dcp.getlinelevelservices.svg GET]] /{cart-id}/lines/{line-id}/services [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v3.1/Cart_Swagger.json swagger]]
DASH -> DCP : post optional-services to line: [[../../DCP/ce.dsg.dcp.postlinelevelservices.svg POST]] /{cart-id}/lines/{line-id}/services [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v3.1/Cart_Swagger.json swagger]]
end
note over DASH
	EIP
	Pay Extra --> Item level or cart level? Finance domain handling
end note

opt Pay Extra/pay towards load/delay pay
DASH -> DCP : Pay Extra API call
end

note over DASH
	Temp Double - DASH to DCP call. Increases ECA of customer.
	flag in estimate call to Finance domain.
	pass details to OMD
end note

opt Loan/Lease
DASH -> DCP : Finance Estimate installment call (Loan/Lease) [[../../DCP/ce.dsg.dcp.postestimateisntallement.svg POST]] /{cart-id}/finance-estimates  - [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v3.1/Checkout_Swagger.json swagger]]
DCP --> DASH : updated cart with pricing and promotions 
end
note over DASH
	Price override metadata and generic codes retrieval
end note

note over DASH
	Auto-Pay support for upgrade transactions
end note


DASH -> SupplyChainDomain : Pull Price override metadata
opt Price Override by REP
DASH -> DCP : Rep overrides the pricing in cart
note left
	Details around price override
	Authorization checks
end note
end
note over DASH
	Upgrade fee - 
	MPM delivered in future
	Transaction and SKU dependent
	Eligibility Rules?
	Retail - Waive fees/insurance claims
end note
DASH --> REP : updated cart
end

@enduml 