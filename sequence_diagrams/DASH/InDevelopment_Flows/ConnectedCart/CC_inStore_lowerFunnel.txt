@startuml 
actor REP
Group upgrade checkout flow
REP -> DASH : Rep on checkout flow

note over DASH
	Ship-to: DICE(MW) handles tax exempt
	Retails : REP can override tax exempt
	Discussion Needed
end note

DASH -> DCP : get quote with all applicable taxes: [[../../DCP/ce.dsg.dcp.getquotes.svg Get]] /quotes - [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v3.1/Cart_Swagger.json swagger]]

opt for each payment tender

opt  <size:12><font color=magenta><b>cash payment -  retail</b></font></size>
DASH -> DICE : cash drawer calls (Remote cash drawer access over N/W call) 
note over DASH
	Remote cash drawer access 
	over N/W call
	Native Desktop
end note
end

alt <size:12><font color=magenta><b>retail only</b></font></size>
opt CC Scan
DASH -> Tapestry : Encrypted credit card details
note over DASH
	Need More details
	to Tapestry using library
end note
end
opt CC Scan
DASH -> HAS : Encrypted credit card details
note over DASH
	Need More details
	to HAS using library
end note
end
DASH -> DCP : post encrypted card payment details: [[../../DCP/ce.dsg.dcp.postpayments.svg POST]]/{cart-id}/payments [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v3.1/Checkout_Swagger.json swagger]]
end

note over DASH
	Full Auth & payoffs settlement - Discussion needed
end note

note over DASH
	3 card tries(Declined) - cancel order
end note

note over DASH
	Bill-to/Delay Pay Discussion
end note


note over DASH
	For retail 
	Fraud happens for the first tender only, integrated with auth call
	Rest of the tender calls with fraud flag as false
	Only Cash, fraud call is triggered
	Debit/Credit rules -- discussion needed
	Scenario when Payments is down -- discussion
end note

alt <size:12><font color=Magenta><b>Retail</b></font></size>
opt <size:12><font color=magenta><b>For first tender</b></font></size>
DCP -> EPS : card auth call with fraud flag as true
end

opt <size:12><font color=magenta><b>For subsequent tenders</b></font></size>
DCP -> EPS : auth call only
end
end

opt <size:12><font color=magenta><b>for $0 today -- only fraud check </b></font></size>
DCP -> EPS : Fraud call - no card
end

DCP --> DASH : payment response
end


opt <size:12><font color=magenta><b>Inline Order -  retail</b></font></size>
note over DASH
	On behalf signing Authorization data flow
	Inline Flow variant(Wet Signature): signature on paper document. Data flow needed. Notify disclosure call made.
end note
DASH -> DocumentDomain : EIP disclosure for customer signing
DASH -> DocumentDomain : notify disclosure acceptance
DASH -> DCP : document details

end


opt <size:12><font color=magenta><b>Inventory Update -  retail</b></font></size>
note over DASH
	Retail - DCP to supplyChain for inventory Update Post order checkout
end note
end

note over DASH
	Retail Direct: Trade-in/inventory/payment reversal in case of undo/fallouts 
end note

end
DASH -> DCP : submit order: [[../../DCP/ce.dsg.dcp.submitorder.svg POST]] /orders/{extCartId} - [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v3.1/Order_Swagger.json swagger]]
note over DASH
	Order# series change? hard reqmt by any system
end note
DASH --> REP : order details with order#

opt <size:12><font color=magenta><b>receipt flow - retail</b></font></size>
note over DASH
	Order# on the receipt
	Separate receipt for AS-IS and TO-BE flows 
end note
DCP -> DocumentDomain : receipt template request
DASH -> DocumentDomain : receipt retrieval
DASH -> Tapestry : print receipt
end

DCP --> OMD : Push order details to OMD

@enduml 