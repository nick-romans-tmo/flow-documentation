@startuml
box "Digital Commerce Platform" #LightGrey
participant Apigee
participant Java
participant Cortex
participant Redis
participant APIGateway
end box
Apigee -> Java: Post finance-estimates API call
Java -> Cortex: finance-estimates request 
Cortex -> APIGateway: Estimate call with cart content
APIGateway -> APIGEE: [[../BD/CE.DD.BD.EIP.loan_bundle_quotes_regular.puml.svg EIP Plan Estimate]] (/v1/loan-bundle-quote)
APIGEE --> APIGateway: Estimate Installment Plan response
Redis <-- APIGateway: Store EIP details 
Java -> Cortex: Get Cart
Cortex -> Redis: Read EIP pricing
Cortex --> Java:Return cart with updated price
Java --> Apigee: response
@enduml