@startuml
box "Digital Commerce Platform" #LightGrey
participant Apigee
participant Java
participant Cortex
end box
Apigee -> Java: Get eligible plans API call
Java -> Cortex: Get eligible plans
Cortex --> Java: Response
Java --> Apigee: response
@enduml