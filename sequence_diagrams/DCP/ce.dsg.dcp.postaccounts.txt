@startuml 
box "Digital Commerce Platform" #LightGrey
participant Apigee
participant Java
participant Cortex
participant Redis
end box
Apigee -> Java: POST Account API call
Java -> Cortex: POST Account details
Java -> Redis: Store Account details
Cortex --> Java: Response
Java --> Apigee: response
@enduml 