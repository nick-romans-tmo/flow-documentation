@startuml
box "Digital Commerce Platform" #LightGrey
participant Apigee
participant Java
participant Redis
participant Solr
participant Elasticsearch
end box
Apigee -> Java: POST Products by Id API call
Java-> Redis: Read selling context details
Redis-->Java: response
Java-> Solr: Query solr with selling context and requested params
Solr --> Java: Return Products in response 
Java-> Elasticsearch: Availability & Eligibility & Pricing query
Elasticsearch --> Java: response with status
Java --> Apigee: Contextual products response
@enduml