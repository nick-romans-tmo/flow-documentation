@startuml
box "Digital Commerce Platform" #LightGrey
participant Apigee
participant Java
participant Cortex
participant Database
end box
Apigee -> Java: Delete Device API call
Java -> Cortex: Delete Device from line
Cortex --> Database:Save cart to persistence source
Cortex --> Java: Device deleted successfully
alt follow = true
Java -> Cortex: get cart
Cortex --> Java: Return updated cart in response
end
Java --> Apigee: response
@enduml