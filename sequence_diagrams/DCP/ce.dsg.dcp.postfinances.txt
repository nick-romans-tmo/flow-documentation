@startuml
box "Digital Commerce Platform" #LightGrey
participant Apigee
participant Java
participant Cortex
end box
Apigee -> Java: Post finances API call
Java -> Cortex: Update profile account with details
Cortex --> Java: response
Java --> Apigee: response
@enduml