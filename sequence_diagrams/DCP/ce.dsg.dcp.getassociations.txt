@startuml 
box "Digital Commerce Platform" #LightGrey
participant Apigee
participant Java
participant Cortex
end box
Apigee -> Java: Get Associations API call
Java -> Cortex:Get Associations details
Cortex --> Java: Response
Java --> Apigee: response
@enduml 