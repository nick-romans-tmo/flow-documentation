@startuml
box "Digital Commerce Platform" #LightGrey
participant Apigee
participant Java
participant Cortex
end box
Apigee -> Java: Post fraud review API call
Java -> Cortex: Post fraud review (Lexis Nexis Q&A outcome)
Cortex --> Java: Fraud Q&A updated
Java --> Apigee: response
@enduml