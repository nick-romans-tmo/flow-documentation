@startuml
box "Digital Commerce Platform" #LightGrey
participant Apigee
participant Java
participant Cortex
participant Database
end box
Apigee -> Java: Update Shipping-Options API call
Java -> Cortex: Update Shipping-Options
Cortex -> Database: Persist details
Cortex --> Java:Return Response
Java --> Apigee: response
@enduml