@startuml 
box "Digital Commerce Platform" #LightGrey
participant Apigee
participant Java
participant Cortex
end box
Apigee -> Java: POST Associations API call
Java -> Cortex: POST Associations details
Cortex --> Java: Response
Java --> Apigee: response
@enduml 