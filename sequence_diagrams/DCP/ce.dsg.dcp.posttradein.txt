@startuml
box "Digital Commerce Platform" #LightGrey
participant Apigee
participant Java
participant Cortex
participant Database
end box
Apigee -> Java: Post trade-in API call
Java -> Cortex: Post trade-in details to cart
Cortex -> Database:Save cart to persistence source
Cortex --> Java: Return success response
alt follow = true
Java -> Cortex: get cart
Cortex --> Java: Return updated cart in response
end
Java --> Apigee: response
@enduml