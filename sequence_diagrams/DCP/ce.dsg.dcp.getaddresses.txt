@startuml 
box "Digital Commerce Platform" #LightGrey
participant Apigee
participant Java
participant Cortex
end box
Apigee -> Java: Get addresses API call
Java ->Cortex: GET addresses 
Cortex --> Java: Return associated user addresses in response 
Java --> Apigee: response
@enduml 