@startuml
box "Digital Commerce Platform" #LightGrey
participant Apigee
participant Java
participant Cortex
end box
Apigee -> Java: Clone line API call
Java -> Cortex: Clone Line
Cortex --> Java: Return success response
alt follow = true
Java -> Cortex: get cart
Cortex --> Java: Return updated cart in response
end
Java --> Apigee: response
@enduml 