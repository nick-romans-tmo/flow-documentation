@startuml 
box "Digital Commerce Platform" #LightGrey
    participant Java
    participant Cortex
    participant ActiveMQ
    participant Integration
    participant Redis
    participant Database
    participant APIGateway
end box
alt FRP Order 
Java -> Cortex: Update Audit Records

Java -> Cortex: Submit Order
Cortex -> Database: Persist Order
Cortex --> ActiveMQ: ORDER_RECEIVED event for Dashboard
ActiveMQ --> Integration: Pick Order event for Dashboard
Integration --> APIGateway: Order received event
APIGateway --> ReportingDashboard: [[../REPORTING/ce.orderreporting.createevent.svg Post ORDER_RECEIVED event to Reporting API (/api/order-flow/events)]] [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v2.4/Order_Swagger.json swagger]]
Cortex --> Java: Return Order Id
Cortex --> ActiveMQ: Drop Order
ActiveMQ --> Integration: Pick Order
Integration --> APIGateway: Push order event
APIGateway --> ReportingDashboard: [[../REPORTING/ce.orderreporting.createevent.svg Post ORDER_PUSHED event to Reporting API (/api/order-flow/events)]] [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v2.4/Order_Swagger.json swagger]]
Integration --> APIGateway: Post order request
APIGateway --> CapabilityMS: [[../OMD/ce.dsg.od.api.postOrder.svg Post Order to order API (/ordermanagement/v4/orders)]] - [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.om/Order.json swagger]]
end 
alt INLINE EIP Order
Java -> Cortex: Update Audit Records

Java -> Cortex: Submit Order
Cortex -> Database: Persist Order
Cortex --> ActiveMQ: ORDER_RECEIVED event for Dashboard
ActiveMQ --> Integration: Pick Order event for Dashboard
Integration --> APIGateway: Order received event
APIGateway --> ReportingDashboard: [[../REPORTING/ce.orderreporting.createevent.svg Post ORDER_RECEIVED event to Reporting API (/api/order-flow/events)]] [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v2.4/Order_Swagger.json swagger]]
Cortex --> Java: Return Order Id
Cortex --> ActiveMQ: Drop Order
ActiveMQ --> Integration: Pick Order
Integration --> APIGateway: Push order event
APIGateway --> ReportingDashboard: [[../REPORTING/ce.orderreporting.createevent.svg Post ORDER_PUSHED event to Reporting API (/api/order-flow/events)]] [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v2.4/Order_Swagger.json swagger]]
Integration --> APIGateway: Post order request
APIGateway --> CapabilityMS: [[../OMD/ce.dsg.od.api.postOrder.svg Post Order to order API (/ordermanagement/v4/orders)]] - [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.om/Order.json swagger]]
end
alt OFFLINE EIP Order
Java -> Cortex: Update Audit Records

Java -> Cortex: Submit Order
Cortex -> Database: Persist Order
Cortex --> ActiveMQ: ORDER_RECEIVED  event for Dashboard
ActiveMQ --> Integration: Pick Order event for Dashboard
Integration --> APIGateway: Order received event
APIGateway --> ReportingDashboard: [[../REPORTING/ce.orderreporting.createevent.svg Post ORDER_RECEIVED event to Reporting API (/api/order-flow/events)]] [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v2.4/Order_Swagger.json swagger]]
Cortex --> Java: Return Order Id
Cortex --> ActiveMQ: Drop Order
ActiveMQ --> Integration: Pick Order
Integration --> APIGateway: Push order event
APIGateway --> ReportingDashboard: [[../REPORTING/ce.orderreporting.createevent.svg Post ORDER_PUSHED event to Reporting API (/api/order-flow/events)]] [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dcp/v2.4/Order_Swagger.json swagger]]
Integration --> APIGateway: Post order request
APIGateway --> CapabilityMS: [[../OMD/ce.dsg.od.api.postOrder.svg Post Order to order API (/ordermanagement/v4/orders)]] - [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.om/Order.json swagger]]
end
@enduml 