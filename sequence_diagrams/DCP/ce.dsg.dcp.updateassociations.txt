@startuml
box "Digital Commerce Platform" #LightGrey
participant Apigee
participant Java
participant Redis
end box
Apigee -> Java: Update Associations API call
Java -> Redis :Update Associations details
Redis --> Java: Response
Java --> Apigee: response
@enduml