@startuml
box "Digital Commerce Platform" #LightGrey
participant Apigee
participant Java
participant Solr
end box
Apigee -> Java: Get sort options API call
Java-> Solr: Query solr with requested params
Solr --> Java: Return sort options in response 
Java --> Apigee: response
@enduml