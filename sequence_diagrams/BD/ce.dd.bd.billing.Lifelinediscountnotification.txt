
@startuml

title Trigger Lifeline discount notification
actor REP
actor Customer
participant REP
participant CSM
participant Samson
participant DEEP.IO
participant DND
participant Customer
REP -> CSM:REP Login and Enroll life line discount
CSM -> Samson: Trigger Enrollment notification
Samson ->DEEP.IO: Event triggered to Deep.io
DEEP.IO->DND:Notification Trigged to Customer
CHUB->DND: Customer preference for notification
DND->Customer:Customer receives enrollment Notification

@enduml