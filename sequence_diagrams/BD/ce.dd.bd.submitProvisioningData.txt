@startuml

title Provisioning MS : Switch Domain Flow
participant "Client application"
participant "APIGEE Gateway"
participant "switch-control-ms"
database "Switch DB"

"Client application" -> "APIGEE Gateway" : POST /provisioning/v1/submitProvisioningData [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/digitalbilling/Switch%20Control/switch-control-ms-v1.json?at=refs%2Fheads%2FFeature%2FSwitch-Control-Provisioning  swagger]]  
"APIGEE Gateway" -> "switch-control-ms" : POST /provisioning/v1/submitProvisioningData [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/digitalbilling/Switch%20Control/switch-control-ms-v1.json?at=refs%2Fheads%2FFeature%2FSwitch-Control-Provisioning  swagger]] 
"switch-control-ms"--> "Switch DB" : DB query
"Switch DB"->"switch-control-ms": Response
"switch-control-ms" ->"APIGEE Gateway":  /provisioning/v1/submitProvisioningData
"APIGEE Gateway"->"Client application" : /provisioning/v1/submitProvisioningData

@enduml