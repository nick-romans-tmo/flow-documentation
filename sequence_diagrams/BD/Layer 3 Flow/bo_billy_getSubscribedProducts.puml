@startuml

title Get a List of Subscribed Products for a User's Billing Account

bo_billy -> bo_billy: checkBillingVendor
activate bo_billy
alt no billing vendor
    bo_billy -> l3tv_bo_utils: must provide billing property
    l3tv_bo_utils --> bo_billy: JSError
else billing vendor is not zuora
    bo_billy -> l3tv_bo_utils: not a supported billing provider
    l3tv_bo_utils --> bo_billy: JSError
else billing vendor has no meta id
    bo_billy -> l3tv_bo_utils: must provide billing.meta.id property
    l3tv_bo_utils --> bo_billy: JSError
end
deactivate bo_billy

alt JSError present
    bo_billy -> bo_billy: callback with error
else no JS Error, proceed to get subscribed products
    bo_billy -> l3tv_bo_clients: getSubscribedProducts
    note over bo_billy,l3tv_bo_utils: billingVendor.meta.id is required
    activate l3tv_bo_clients
    alt if no meta.id
        l3tv_bo_clients --> bo_billy: callback with error Invalid billing metadata
    else meta.id is present
        l3tv_bo_clients -> l3tv_bo_clients: resolveSubscriptionFromMeta
        note right: get details of standard "type" subscription
        alt if type is not standard or one_time
            l3tv_bo_clients --> bo_billy: JSError (Invalid subscription type specified)
            else subscription is present and is of Type 1
            note right
            Type 1: two classified subscriptions
              {
                id: '2c92a0fa4f2b5e5a014f42bc9b385a2c',
                subscriptions: {
                  standard: 'A-S00000527',
                  one_time: 'A-S00000528'
                }
              }

              Type 2: Two subscriptions in array. 1st is recurring, 2nd is for VOD.
              {
                id: '2c92c0fb4ed842a9014edaf9e377464c',
                subscriptions: [
                  'A-S00000527',
                  'A-S00000528'
                ]
              }

              Type 3:  Only Zuora account ID saved. Should only be a single subscription.
              {
                id: '2c92c0fa4db9204d014dc795480318af'
              }
            end note

            l3tv_bo_clients -> l3tv_bo_clients: getSubscriptionByNumber
            participant zuora #aqua
            l3tv_bo_clients -> zuora: REST GET call to get subscriptions
            note right: /subscriptions/<<subscription type number>>
            alt REST call error
                zuora --> l3tv_bo_clients: callback with error
                l3tv_bo_clients --> bo_billy: callback with error
            else REST call success
                 zuora --> l3tv_bo_clients: plans
                 l3tv_bo_clients -> l3tv_bo_clients: handle success callback
                 note right: Iterate through plans and get list of id,name,price for each plan
                 l3tv_bo_clients --> bo_billy: send plans list

            end
        else subscription is present and is of Type 2 (Array)
            alt subscription array length is not equal to 2
            l3tv_bo_clients --> bo_billy: callback with error Unexpected subscriptions array length in billing meta
            else subscription array length is equal to 2
                l3tv_bo_clients -> l3tv_bo_clients: getSubscriptionByNumber
                l3tv_bo_clients -> zuora: REST GET call to get subscriptions
                note right: /subscriptions/<<subscription type number>>
                alt REST call error
                    zuora --> l3tv_bo_clients: callback with error
                    l3tv_bo_clients --> bo_billy: callback with error
                else REST call success
                    zuora --> l3tv_bo_clients: plans
                    l3tv_bo_clients -> l3tv_bo_clients: handle success callback
                    note right: Iterate through plans and get list of id,name,price for each plan
                    l3tv_bo_clients --> bo_billy: send plans list
                end
            end
        else subscription is present and is of Type 3 (no subscriptions and only zuora account id)
            l3tv_bo_clients -> l3tv_bo_clients: getSingleActiveSubscription
            l3tv_bo_clients -> l3tv_bo_clients: getActiveSubscriptions
            l3tv_bo_clients -> l3tv_bo_clients: getSubscriptions
            l3tv_bo_clients -> zuora: REST GET call to get subscriptions
            note right: /subscriptions/accounts/<<account id>>
            alt REST call error
                zuora --> l3tv_bo_clients: callback with error
                l3tv_bo_clients --> bo_billy: callback with error
            else REST call success
                 zuora --> l3tv_bo_clients: plans list
                 l3tv_bo_clients -> l3tv_bo_clients: filter out only active plans
                 note right
                 If multiple plans, select first one.
                 If no plans send back error No subscriptions found for Zuora account
                 Iterate through plans and get list of id,name,price for each plan
                 end note
                 l3tv_bo_clients --> bo_billy: send plans list

            end
        else return error
        l3tv_bo_clients --> bo_billy: callback with error Unable to determine subscription lookup strategy from billing meta
        end
    end
deactivate l3tv_bo_clients
end

@enduml