@startuml
title <u>Activate</u>
autonumber
SOE -> "APIGEE": JSON Request
"APIGEE" -> "Resource SIM MS": activate
"Resource SIM MS" -> "Resource SIM MS" : Input validation
alt Successful case Update SOR
"Resource SIM MS" -> "SAMSON": Activate SIM L1 API
"SAMSON" -> "Resource SIM MS" : Successful
else some kind of failure
"SAMSON" -> "Resource SIM MS" : Failure (Log the Failed SIM in output)
"Resource SIM MS"->"APIGEE": <b><font color=red>Response with Error and HTTP Status</font></b>
end
"Resource SIM MS" -> "Resource SIM MS": DAPI to TMF conversion
alt Successful Core Perform Activity
	"Resource SIM MS" -> "Physical Resource Core MS": POST performActivity
	"Physical Resource Core MS" -> "Physical Resource Core MS": Input Validations
	"Physical Resource Core MS" -> "Mongo DB": Get Resource By Sim Number
		alt Successful case Pre hook Custom Validations
		"Physical Resource Core MS" -> "Physical Resource Extension MS": Custom Validations and set Classification Type
		"Physical Resource Extension MS" -> "Physical Resource Core MS": Successful
		else some kind of failure
		"Physical Resource Extension MS"->"Physical Resource Core MS": <b><font color=red>Response with Error and HTTP Status</font></b>
		"Physical Resource Core MS"->"Resource SIM MS": <b><font color=red>Response with Error and HTTP Status</font></b>
		"Resource SIM MS"->"APIGEE": <b><font color=green>Success Response as SOR is updated</font></b>
		end
		alt Successful State Transition
		"Physical Resource Core MS" -> "Physical Resource Core MS": State Transition using State Machine
		else State Transition is not evaluated
		"Physical Resource Core MS"->"Resource SIM MS": <b><font color=red>Response with Error and HTTP Status</font></b>
		"Resource SIM MS"->"APIGEE": <b><font color=green>Success Response as SOR is updated</font></b>
		end
		alt Successful case Post Hook Custom Validations
		"Physical Resource Core MS" -> "Physical Resource Extension MS": Post Hook Set Activity Code
		"Physical Resource Extension MS" -> "Physical Resource Core MS": Successful
		else some kind of failure
		"Physical Resource Extension MS"->"Physical Resource Core MS": <b><font color=red>Response with Error and HTTP Status</font></b>
		"Physical Resource Core MS"->"Resource SIM MS": <b><font color=red>Response with Error and HTTP Status</font></b>
		"Resource SIM MS"->"APIGEE": <b><font color=green>Success Response as SOR is updated</font></b>
		end
		"Physical Resource Core MS" -> "Mongo DB": Update the resource in Mongo DB
		"Physical Resource Core MS" -> "Resource SIM MS": <b><font color=green>Successful Response with Resource</font></b>
end
"Resource SIM MS" -> "Resource SIM MS": TMF to DAPI conversion
"Resource SIM MS"->"APIGEE":<b><font color=green>Output HTTP Entity</font></b>
"APIGEE"->"SOE":JSON Output
@enduml