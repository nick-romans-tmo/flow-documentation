@startuml
title <u>Notify Subscriber FDP Deletion</u>
autonumber
SOR -> "KAFKA": Publish FDP Removal event payload to FDP_REMOVAL Topic
alt Successful case
"KAFKA" -> "Payment Arrangement MS": Consume FDP Removal event payload
"Payment Arrangement MS" -> "Payment Arrangement MS" : Payload validation
"Payment Arrangement MS" -> "KAFKA": Convert FDP Removal payload to DeepIO generic format\nand Publish to DEEPIO_SYNC_SERVICES topic
else some kind of failure
"Payment Arrangement MS" -> "KAFKA": Publish FDP Removal payload with error details to TRANSACTION_AUDIT topic
end
"KAFKA" -> "Atwork Deepio Integration MS" : Consume FDP Removal event
"Atwork Deepio Integration MS" -> "DeepIO": Publish FutureDatedPayment Deleted event
@enduml