@startuml
participant InternalEmail_MicroService
participant EMS
participant InternalEmailProcessor
participant NotificationTemplate 
participant FileStorage #B0C4DE
participant SMTP
participant "ErrorLookup" as EL

InternalEmail_MicroService  [#blue]->EMS : Publishes messages to EMS queue \n TMOBILE.ENVNAME-ENV1.NOTIFICATIONMANAGEMENT.INTERNALEMAIL.V2
EMS-[#blue]>InternalEmailProcessor:Consume Messages

group InternalEmailProcessor
InternalEmailProcessor<-[#blue]>NotificationTemplate: POST:applyTemplateData(size,template) \n [[http://flows.corporate.t-mobile.com/all-swaggers/swagger/ce.dsg.dnd/templateservice_v2.yml/  swaggerLink]] \n [[http://flows.corporate.t-mobile.com/all-diagrams/sequence_diagrams/dnd/ce.dsg.dnd.notificationtemplate.txt/ NotificationTemplate SSD]] \n Response of applyTemplateData(size,template)

group Attachment is available 
alt Attachment is available in Payload
InternalEmailProcessor-[#blue]>InternalEmailProcessor: Look for the attachments(0KB > Attachment Content < 200KB and Max. 10 attachments ) in the payload 
InternalEmailProcessor<-[#blue]>SMTP : Embed attachment in Email and send to SMTP
end
 
alt Attachment is available as Link
InternalEmailProcessor-[#blue]>InternalEmailProcessor: Look for attachment Links in the payload 
InternalEmailProcessor<-[#blue]>FileStorage : Download the attachment via HTTP[[ HTTP://HOSTNAME:PORT ]] call (0KB > Attachment Content < 200KB and Max. 10 attachments)
InternalEmailProcessor<-[#blue]>SMTP : Embed attachment in Email and send to SMTP
end

alt exception
InternalEmailProcessor-[#red]>InternalEmailProcessor: Attachment Content >200 KB OR >10 number of attachments.\n Response " File is greater than 200KB" or "Attachment is more than 10 in numbers
end
end
InternalEmailProcessor<-[#blue]>SMTP : Transform and send an Email to SMTP
EMS<[#green]-InternalEmailProcessor:Success
end



alt Failure

InternalEmailProcessor<-[#red]->EL: Error Response
alt Retry=true

EMS<[#red]-EMS: Implement 10 hours retry(Total Retries=30) policy for every 20 minutes interval delay
end
alt Retry=False
EMS<[#green]-InternalEmailProcessor:Acknowledged
end
end

@enduml