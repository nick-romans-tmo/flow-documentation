@startuml
participant DEEP #yellow
participant NotificationAPI
participant EMS
participant NodeEventProcessor
participant NotificationConfigProcessorMicroService
participant TFB #yellow
participant CHUB #yellow
participant Samson #yellow
participant Union #yellow
participant Product_Catalog #yellow
participant NotificationHandler
participant SMS_MicroService
participant EMAIL_MicroService
participant IntEmail_MicroService
participant ALERTS_MICROSERVICE
participant AAL_MicroService
participant PushNotification_MICROSERVICE
participant NotificationHistoryService
participant ErrorLookup

DEEP -> NodeEventProcessor : Payload \n [[https://deep-internal-npe.test.px-npe01.cf.t-mobile.com/qlab02/console/#/configuration/viewRules  DeepRules URI]] \n 
NotificationAPI -> EMS : Payload \n // ** API Resource : /v1/domain-notifications-events**// \n //Operation = POST// \n [[http://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dnd/domain-notifications-events.json  NotificationAPI SwaggerLink]] \n //**Queue name: TMOBILE.ENVNAME-ENV1.NOTIFICATIONMANAGEMENT.NOTIFICATION**//
EMS -> NodeEventProcessor : Event Payload
NodeEventProcessor <-> NotificationConfigProcessorMicroService : get Notification Configuration \n //**API Resource: notifications-config-V1-searchByEventName **//\n //Operation : GET// \n [[http://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dnd/Services.configAPI-Resource.json \n NotificationConfigProcessorMicroService SwaggerLink]] \n [[http://flows.corporate.t-mobile.com/all-diagrams/sequence_diagrams/dnd/ce.dsg.dnd.NotificationConfigProcessorMicroService.txt/ NotificationConfigProcessorMicroService SSD ]]
Alt To Get Notification and Langage Preference ,Email address, MSISDN,phoneSmsAllowed
NodeEventProcessor <-> CHUB : // **API  : getCustomerPreference(BAN,MSISDN,Role)**// \n //API Resource : /serviceNotifications/search// \n //Operation : GET// \n [[http://flows.corporate.t-mobile.com/swagger/ce.dsg.cd/CustomerServiceNotificationsSearch.json \n CHUB SwaggerLink]] \n [[http://flows.corporate.t-mobile.com/all-diagrams/sequence_diagrams/dice/ce.dsg.dice.cd.customernotificationpreferences.txt/ CHUB SSD ]]
end
Group Aggregators
Alt If Aggegator = TFB
NodeEventProcessor <-> TFB : // **API  : getTFBInfo(BAN)**// \n //API Resource : /billing-accounts/{billing-account}// \n //Operation : GET// \n [[http://flows.corporate.t-mobile.com/all-swaggers/swagger/ce.dd.tfb/v1.0/b2b-experience-entitlements.json \n TFB SwaggerLink]]
end

Alt If Aggegator = Samson-EffectiveSocs
alt
NodeEventProcessor <-> Samson : // **API  : service-agreement-ms-v1(BAN,MSISDN,pdpIndicator='false')**// \n //API Resource : /billing/v1/subscribers/rate-plan \n //Operation : GET// \n [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/billing.samson/service-agreement-ms-v3-soc.json \n Samson Rate Plan SwaggerLink]] \n 
alt If Aggegator = Product Catalog
NodeEventProcessor<->Product_Catalog :**API  : /services(/services?soc=P3601&fields=serviceOffers[samsonBilling.pricePoint.recurringCharges[amount]])**// \n //API Resource : /services \n //Operation : GET// \n [[https://bitbucket.service.edp.t-mobile.com/projects/PSAPI/repos/postman-scripts/browse/Common%20Postman%20Collection \n Product Catalog API SwaggerLink]] \n
end
end
Alt To Get  Pass Details
NodeEventProcessor <-> Samson : // **API  : Pass-agreement-ms-v1(financialAccountNumber,MSISDN,mobileNumbersList)**// \n //API Resource : /serviceNotifications/search// \n //Operation : GET// \n [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/billing.samson/data-pass-information-sa-v1.json \n Samson Pass agreement SwaggerLink]] \n 
end
end
Alt Aggegator = Union
NodeEventProcessor <-> Union : // **API  : locationdetails(MSISDN)**// \n //API Resource : locationdetails// \n //Operation : GET// \n  \n Note: Engg. swagger isn't availble in CW \n 
end
alt If Aggegator = Product Catalog
NodeEventProcessor<->Product_Catalog :**API  : /services(/services?soc=P3601&fields=serviceOffers[samsonBilling.pricePoint.recurringCharges[amount]])**// \n //API Resource : /services \n //Operation : GET// \n [[https://bitbucket.service.edp.t-mobile.com/projects/PSAPI/repos/postman-scripts/browse/Common%20Postman%20Collection \n Product Catalog API SwaggerLink]] \n
end
end
Critical
NodeEventProcessor->NodeEventProcessor: IF Valid Notification exists \n THEN create Notification List
alt : EXIT CONDITION  
NodeEventProcessor->NodeEventProcessor: No Notification will be created \n IF No Valid Notification exists OR NO recipients found \n GRACEFUL EXIT 
END
NodeEventProcessor->NodeEventProcessor: if Duplicate Recipients exist \n THEN Filter duplicate recipients
NodeEventProcessor->NodeEventProcessor: if multiple notification type (SMS,Email,AAL,Alert,Int Email) \n THEN publish to corresponding handlers

end
NodeEventProcessor -> NotificationHandler
alt : SMS Flow
NotificationHandler <-> SMS_MicroService :To Generate SMS for Customers [[http://flows.corporate.t-mobile.com/all-diagrams/sequence_diagrams/dnd/ce.dsg.dnd.sms.txt SMS MicroService]] 
end 
alt : Email Flow 
NotificationHandler <-> EMAIL_MicroService: To Send Email to Customers [[http://flows.corporate.t-mobile.com/all-diagrams/sequence_diagrams/dnd/ce.dsg.dnd.email.txt   Email MicroService]] 
end 
alt : Internal Email Flow 
NotificationHandler <-> IntEmail_MicroService:to send internal email for customers [[http://flows.corporate.t-mobile.com/all-diagrams/sequence_diagrams/dnd/ce.dsg.dnd.internal_email.txt   Internal Email MicroService]]  
end 
alt : Alerts Flow 
NotificationHandler <-> ALERTS_MICROSERVICE :Create Alerts for customers [[http://flows.corporate.t-mobile.com/all-diagrams/sequence_diagrams/dnd/ce.dsg.dnd.alert.txt  ALERT MicroService]]\n 
end 
alt : Account Activity Log Flow 
NotificationHandler <-> AAL_MicroService :Create Acc Activity Log for customers [[http://flows.corporate.t-mobile.com/all-diagrams/sequence_diagrams/dnd/ce.dsg.dnd.aal.txt   AAL MicroService]] 
end 
alt : Push Notification Flow 
NotificationHandler <-> PushNotification_MICROSERVICE :To send Push Notifications to customers [[https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/commits/dbc5d399696b0a3374e47d2499f7006bfb1cb805  Push Notification MicroService]] 
end 
alt Failure
SMS_MicroService -> ErrorLookup : ErrorResponse
EMAIL_MicroService -> ErrorLookup : ErrorResponse
IntEmail_MicroService -> ErrorLookup : ErrorResponse
ALERTS_MICROSERVICE -> ErrorLookup : ErrorResponse
AAL_MicroService -> ErrorLookup : ErrorResponse
PushNotification_MICROSERVICE-> ErrorLookup : ErrorResponse
EMS <-[#red]- ErrorLookup : Error Response
alt Retry = True and System Error
EMS <[#red]- EMS : If Retry=True: Retry 10 times(configurable)
NodeEventProcessor ->NotificationHistoryService :Publish messages to Notification History
end
alt Retry = True and Data Error
NodeEventProcessor ->NotificationHistoryService :Publish messages to Notification History
end
alt Retry = False
EMS <[#green]- NotificationHandler : Acknowledged
NodeEventProcessor ->NotificationHistoryService :Publish messages to Notification History
end 
end
@enduml