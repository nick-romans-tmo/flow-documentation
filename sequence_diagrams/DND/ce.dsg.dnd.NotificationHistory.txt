@startuml

participant NotificationHistoryService
participant NodeStateService
participant NotificationHistoryDB
participant OrderRetryProcessor_Queue



alt : search operation
NotificationHistoryService <-> NodeStateService : //API: searchNotificationHistory(eventID)// \n // API Resource: notifications/v1/history/search// \n //Operation : GET// \n  [[http://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dnd/NodeStateService.json \n SwaggerLink]]
 


alt : Retrieve Notification Status from NotificationHistory DB table
NodeStateService -> NodeStateService : getNodeState(eventID) 
NodeStateService<-> NotificationHistoryDB : query NotificationHistory DB table
end 


end 

alt : create operation 
NotificationHistoryService <-> NodeStateService : //API: createNotificationHistory(notificationData//) \n //API Resource: notifications/V1/history// \n //Operation : POST// \n  [[http://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dnd/NodeStateService.json \n SwaggerLink]]

alt : CreateNotficationHistory : insert into NotificationHistory DB table
NodeStateService <-> NodeStateService : updateNodeState() 
NodeStateService -> NotificationHistoryDB: Update notificationHistory Table in DB
end 

end 

alt : update operation

NotificationHistoryService <-> NodeStateService : //API: updateNodeStatus(notificationData)// \n //API Resource :notifications/v1/history/status// \n //Operation : POST// \n [[http://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.dnd/NodeStateService.json \n SwaggerLink]]
alt : update NotificationHistory DB table
NodeStateService <-> NodeStateService : updateNodeState()
NodeStateService -> NotificationHistoryDB : Update notificationHistory Table in DB
alt status= Failure
NotificationHistoryDB->OrderRetryProcessor_Queue : Post messages to Delay Queue
end 

end 



@enduml