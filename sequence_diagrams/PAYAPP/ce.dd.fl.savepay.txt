@startuml
actor CareAgent #Magenta
participant QVUI
participant AutopayUI
participant AutopayExperienceAPI
participant CHUBDomainService
participant PaymentDomainService


title  Frontline Applications - Wallet Management flow (QVXP/QVFlex)	

CareAgent -> QVUI: manageWalletPaymentInstruments	

group#YELLOW PageLoad
	QVUI -> AutopayUI: toggleData
	AutopayUI -> AutopayExperienceAPI: /auth
	AutopayExperienceAPI -> EnterpriseToken: toggledata
	EnterpriseToken -> AutopayExperienceAPI
	AutopayExperienceAPI -> PaymentDomainService: getPublicKey
	PaymentDomainService -> AutopayExperienceAPI
	AutopayExperienceAPI -> PaymentDomainService: negativeFile
	PaymentDomainService -> AutopayExperienceAPI
	AutopayExperienceAPI -> CHUBDomainService: CustomerSummarySearch(walletIdentificationMsisdn)
	CHUBDomainService -> AutopayExperienceAPI
	AutopayExperienceAPI -> PaymentDomainService: SPI-Search	
	PaymentDomainService -> AutopayExperienceAPI
	AutopayExperienceAPI -> AutopayUI
end

group#AQUA UpdateWallet
	AutopayUI -> AutopayUI: encrypt
	AutopayUI -> AutopayExperienceAPI:/mpi
	AutopayExperienceAPI -> PaymentDomainService: managePaymentsInstruments
end

@enduml