@startuml
actor CareAgent #Magenta
participant TMAG_QVUI
participant PaymentExperienceUI
control Apigee
participant PaymentExperienceService
control APIGEE
participant PaymentRestAPI
database PaymentDB
participant PaymentDomainService
participant EnterpriseToken

title  Frontline Applications - NR payment flow	

CareAgent -> TMAG_QVUI: makePayment	
TMAG_QVUI -> APIGEE: payment-transactions
APIGEE -> PaymentRestAPI: payment-transactions
PaymentRestAPI -> PaymentDB:
PaymentDB -> PaymentRestAPI: transactionId
PaymentRestAPI -> APIGEE: transactionId
APIGEE -> Apigee: transactionId
Apigee -> TMAG_QVUI: transactionId

group#YELLOW PageLoad
	TMAG_QVUI -> PaymentExperienceUI: paymentCollection
	PaymentExperienceUI -> Apigee: retrieveTransactionDetails
	Apigee -> PaymentExperienceService: retrieveTransactionDetails
	PaymentExperienceService -> APIGEE: toggledata
	APIGEE -> EnterpriseToken: toggledata
	EnterpriseToken -> APIGEE
	APIGEE -> PaymentExperienceService
	PaymentExperienceService -> APIGEE:getPayment-transactions
	APIGEE -> PaymentRestAPI: getPayment-transactions
	PaymentRestAPI -> APIGEE
	APIGEE -> PaymentExperienceService
	PaymentExperienceService -> APIGEE: getPublicKey
	APIGEE -> PaymentDomainService: getPublicKey		
	PaymentDomainService -> APIGEE
	APIGEE -> PaymentExperienceService
	PaymentExperienceService -> Apigee
	Apigee -> PaymentExperienceUI
end

group#AQUA Payment
	PaymentExperienceUI -> PaymentExperienceUI: encrypt	
	PaymentExperienceUI -> Apigee: authorizePayment
	Apigee -> PaymentExperienceService: authorizePayment
	PaymentExperienceService -> APIGEE: processPayment
	APIGEE -> PaymentDomainService: processPayment
end

PaymentExperienceUI -> 	TMAG_QVUI: redirect

@enduml