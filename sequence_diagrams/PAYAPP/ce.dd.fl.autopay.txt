@startuml
actor CareAgent #Magenta
participant QVUI
participant AutopayUI
participant AutopayExperienceAPI
participant CHUBDomainService
participant PaymentDomainService
participant EnterpriseToken


title  Frontline Applications - Autopay flow (QVXP/QVFlex)	

CareAgent -> QVUI: autopayEnroll

group#YELLOW PageLoad	
	QVUI -> AutopayUI: toggleData
	AutopayUI -> AutopayExperienceAPI: /auth
	AutopayExperienceAPI -> EnterpriseToken: toggledata
	EnterpriseToken -> AutopayExperienceAPI
	AutopayExperienceAPI -> PaymentDomainService: getPublicKey
	PaymentDomainService -> AutopayExperienceAPI
	AutopayExperienceAPI -> PaymentDomainService: negativeFile
	PaymentDomainService -> AutopayExperienceAPI
	AutopayExperienceAPI -> CHUBDomainService: CustomerSummarySearch(walletIdentificationMsisdn)
	CHUBDomainService -> AutopayExperienceAPI
	AutopayExperienceAPI -> PaymentDomainService: autopaySearch, SPI-Search	
	PaymentDomainService -> AutopayExperienceAPI
	AutopayExperienceAPI -> AutopayUI
end

group#AQUA AutopayEnrollment
	AutopayUI -> AutopayUI: encrypt	
	AutopayUI -> AutopayExperienceAPI: /mpi
	AutopayExperienceAPI -> PaymentDomainService: managePaymentsInstruments
end

@enduml