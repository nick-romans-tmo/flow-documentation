---
title: "Clearwater Portal"
date: 2017-10-22T12:47:15-07:00
draft: false
---
# Overview

Project [Clearwater](http://tm/clearwater) is a change to how we approach software projects between teams.  Instead of each team working independently to implement the requirements as they are understood, the design will be broken down into independent "flows" that span systems.  Each "flow" will be designed together:

1. The teams will build out a sequence diagram as the primary artifact
1. The sequence diagram will be completed as the primary design artifact
1. Swaggers will be built based off the sequence diagram
1. Sprints will be aligned.  Teams will deliver the functionality together

{{% button href="https://tmobileusa.sharepoint.com/teams/IDSS/Shared%20Documents/Initiatives/Clearwater/Clearwater%20Resource%20Kit/Clearwater-Intro-0IIHEUmiec2o_beta%20(1).mp4?csf=1" icon="fas fa-video" %}}Watch the Clearwater Introduction Video{{% /button %}}

## Getting Started

The [BitBucket repo](https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse) has all the tools and source that you need. Fork the repo.  Add your sequence diagrams.  Make a pull request. Lather, rinse, repeat.

## Process

From a high level, the process will be:

1. A flow will be identified during iterative design or according to a Journey.
2. An architect will identify the as-is flow documentation to be used as a starting point.  If one does not exist, it will be created.
3. An ephemeral team will be organized around that flow, with representation from each of the systems.
4. The "to-be" flow will be designed by the architect with input from the ephemeral team, which will be signed off by that team.  
  1. This will be built in a separate branch of this repo.
  2. The work will be done in a fork, with Pull Requests capturing the dialog from the team members.
  3. When all agree, the PR will be merged into the branch.  This leaves us with the as-is in the master branch, and the to-be in the feature branch.
  4. Lather, rinse, repeat.
5. APIs will be designed around that flow.
  1. Each API will be a swagger.
  2. Swaggers must live in source code control.
  3. PR's will be approved by the impacted parties (i.e., the callers of the API)
  4. Once a base set of swaggers have been defined and approved, stubs will be built.
  5. Stubs should be deployed directly into production.  This will enforce API versioning, deployment automation, and front-load any networking issues that need to be overcome.
  6. Lather, rinse, repeat
6. Once the systems are released, the documentation feature branches will be merged back into master and tagged as the baseline for the next iteration.


# Tools

PlantUML is an open-source, easy-to-use tool that generates sequence diagrams.  More importantly, the "language" for expressing a sequence diagram is a simple text file.

This gives us the following benefits:

1. The layouts are automatic, so time is spent on function, not form.
2. The text files are very conducive to diff'ing in a commit / PR.  This makes it easy to see at-a-glance what is changing.
3. The tooling for creating robust sequence diagrams is free, simple and cross-platform.  See the [PlantUML Tools guide](crt/tools/plantuml).
4. The process for updating is a reflection of the process for coding, which means that it's easy to associate the code with the documentation that was used to build it.
5. The process promotes asynchronous, open communication, which will increase velocity and promote collaboration.  For tracking, we know who-changed-what, when, automatically, and it's easy to rollback to a previous version.
