---
title: "Clearwater Resource Kit"
weight: 1
---

**Clearwater** is a process improvement aimed at simplifying the design and creation of REST APIs, promoting collaboration between dependent teams, early identification of misalignment, and automation to reduce errors and improve velocity.

API definition is treated like any other development task, creating Swagger documents using trunk-based development.

{{< mermaid >}}
graph LR
    Flows(Define/Refine Flows)-->Swaggers(Build Swaggers)
    Swaggers --> Builds(Automate Builds)
{{< /mermaid >}}

## Where can I get help?

Try the Slack channel [#ce_e2edesignpi](https://t-mo.slack.com/messages/C7MJ41B7D).

## How do I get setup?

1. Install git.  You can find this at https://git-scm.com/ 
1. Fork [the repo](http://tm/clearwater). You can log in using your NT credentials. Click the “fork” icon on the left, accept the defaults.
1. Clone your fork:
  * Add your SSH key under [Manage Account, SSH Keys](https://bitbucket.service.edp.t-mobile.com/plugins/servlet/ssh/account/keys)
  * Click the “clone” icon on the left.
  * Copy the link.
  * Open your command window.
  * Type `git clone <link>` (pasting in the link)
1. Install PlantUML (see the [guide](tools/plantuml))

## Defining Flows

Once you have identified the team that will be participating in the flow, begin addressing the following questions:

1. Do you understand the “flow” that is being built?
1. What are the inputs and outputs?
1. What APIs are you exposing?  Who is calling those? (they are one of your peers)
1. What APIs are you consuming?  Who owns those? (they are also a peer)
1. How are you managing your components / microservices?

## Modeling the Flows

1. Create (or update!) the sequence diagram.
    * PlantUML syntax is simple and straightforward.
1. Check into your fork of the BitBucket repository.
    * `git add <filename>`
    * `git commit –m “words that are describing the changes”`
    * `git push`
1. Submit a pull request.  
    * Open BitBucket, navigate to your fork.
    * Click the “Create Pull Request” link
    * Request a review from your peer.
1. The images will be automatically updated at http://flows.corporate.t-mobile.com/ 

## PlantUML Syntax
PlantUML syntax is simple – see http://plantuml.com/sequence-diagram for details.

```PlantUML
@startuml
Alice -> Bob: I have a secret
@enduml
```

{{< puml file="plantuml-sample.puml" >}}

## Building Swaggers

1. Identify new API’s.  Add these to your repo.
1. Update existing API’s.   Make sure they’re backwards compatible.
1. Check into your fork of the BitBucket repository.
1. Submit a pull request.  Request a review from your peer.

## The Role of the Peer

The “peer” is the developer that was identified during the flow definition: this is the person that will be consuming your API or producing an API for you.

{{< mermaid >}}
graph LR
    Review(Review Pull Requests)-->OK{OK?}
    OK-->|Yes|Approve(Mark Pull Request Approve)
    OK-->|No|Respond(Respond with Updates)
    Approve-->Merge
{{< /mermaid >}}  

Review PRs. If okay, then approve and merge.  If not, then respond with updates.

## Which Branch?

Trunk-based, so use the master branch in your fork.  Create PRs against the master branch in the main repo.  When it’s ready to release, merge the commit into the release branch.  

From there, the swaggers can be promoted to the API Catalog: https://developer.t-mobile.com 

## Swagger Best Practices

The more specific, the better.
1. Add descriptions wherever possible.
1. Be clear about required and optional fields.
1. Add validations.
    * Look at the `format` directive for built-in types
    * Is it a number?  Integer?
    * If a string, consider using a regular expression and/or minimum and maximum qualifiers

The more specific, the better, as input validations will become automatic, and the swagger can be used as part of your preventive controls.

## Automating Builds

Your project should pull in the swaggers from the Clearwater repository with every build.  This ensures you always have the correct version in your code.

Your project should leverage swagger-codegen to build the model for calling the API with every build.  The also ensures that the input validations are being honored, and no defects can be introduced due to misunderstanding the API definition.

This also allows the peers to iterate quickly on the swagger definition: field changes become simple recompiles + chasing the build errors, instead of analyzing documentation to determine what has changed.
