---
title: "Clearwater Grab-n-Go"
draft: false
weight: 450
---

Feel free to ask questions to the Clearwater community. Join us on the [#clearwater](https://t-mo.slack.com/messages/C8D77CK7H/) Slack channel. You can also reach the Clearwater core team by emailing us @[mailto:Clearwater@t-mobile.com](mailto:Clearwater@t-mobile.com) for any suggested changes/edits to this documentation.

## What is Clearwater?

Clearwater is establishing and managing contracts between development teams across the development lifecycle.

A process improvement initiative aimed at simplifying the design and creation of REST APIs, Clearwater promotes collaboration between dependent teams, early identification of misalignment, and automation to reduce errors and improve velocity. API definition is treated like any other development task, creating Swagger documents using trunk-based development.

## Why Clearwater?

Project Clearwater is a change to how we approach software projects between teams. Instead of each team working independently to implement the requirements as they are understood, the design will be broken down into independent "flows" that span systems. This will build a better understanding of how the subsystems interact, and project requirements can be better scoped and understood.

## Who is the audience?

The audience includes both contributors, such as designers and developers, and non-contributors, such as mid- and upper-level managers. The designers create the Swagger documents and the sequence diagrams and then store them in the project Bitbucket repository. Developers go to [https://flows.corporate.t-mobile.com](https://flows.corporate.t-mobile.com/) to use the current library of sequence diagrams and Swagger documents for APIs. Finally, the API developers use the Swagger documents to build the APIs.

The role of the peer is also important to understand. Peers are the developers who are identified during the flow definition – it's the person who will be consuming your API or producing an API for you. Peers review pull requests, and then, if they agree with your additions and changes, they approve the pull request.

## Where can I get help?

- Look through the threads and comments on the Slack channel at [#clearwater](https://t-mo.slack.com/messages/C8D77CK7H/details/).
- To review process and tools information, see the [Flow document](https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/README.md).
- To see the current library of sequence diagrams and to review the Clearwater FAQ document, go to the [Clearwater Distillery site](https://flows.corporate.t-mobile.com).

## How do I get set up?

1. The minimum system requirements include:
   * Windows 10 (you can use Windows 7, but there may be some issues)
   * Mac version 10.x
2. Install the prerequisites.
   * Install Git. For instructions, see [https://git-scm.com/](https://git-scm.com/).
   * Install Docker. For instructions on installing Docker, see the /[docs](https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/docs/crt/tools/plantuml/) folder of the project.
3. Go to the Slack channel [#edp-support](https://t-mo.slack.com/messages/C60G34X7C/convo/C02KY507N-1532031430.000087/) and request access to the Clearwater Bitbucket repository ([http://tm/clearwater](http://tm/clearwater)).
4. Fork the repo using the steps below.

   **Note** : Do not clone the project. You must first fork the project, and then clone your fork. You do all your work in the fork.
   1. Create a folder on your local machine (C:\Users\abc\Clearwater) where you want to have the local branch.
   2. Go to [http://tm/clearwater](http://tm/clearwater).
   3. Log in using your NT credentials. You don't need to use the domain name, GSM1900, to log in.
   4. Click the **Fork** icon on the left and accept the default values.
   5. In the dialog box that appears, make sure **Enable fork syncing** is selected, and then press **Fork repository**.

    **Note**: You can also review this [video](https://web.microsoftstream.com/video/b8c376f0-27bc-4d61-afad-ecb6bfc16efb) to see how to fork the repo.

5. Add an SSH key. Under your profile in the repository, click **Manage account** , and then click **SSH keys** on the left. On the **SSH keys** page, click **Add key** and then create a key. For more information, see [Using SSH keys to secure Git operations](https://confluence.atlassian.com/bitbucketserver0414/using-ssh-keys-to-secure-git-operations-895367582.html?utm_campaign=in-app-help&amp;utm_medium=in-app-help&amp;utm_source=stash).
6. Clone the fork.
    1. Click the **Clone** icon on the left.
    2. Copy the link.
    3. Open a command window.
    4. Type **git clone** and then paste the link you just copied.
7. Download the PlantUML .jar file. To download plantuml.jar, see the [/docs](https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/docs/crt/tools/plantuml/files/plantuml.jar) folder of the project.
8. Review the taxonomy. You can find this in [md](https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/README.md) of the project.

**Note**: To render PlantUML diagrams, use Visual Studio Code with the [PlantUML plug-in](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml). Many IDE tools, such as Eclipse and IntelliJ, have plug-ins for PlantUML that allow for real-time feedback of your sequence diagram syntax.

## How do I define the flows?

A flow is identified during the iterative design process. A designer identifies the "as-is" flow documentation to use as a starting point. If one does not exist, it will be created. A team is organized around that flow with representation from each of the systems. The "to-be" flow is created by the designer with input from the team.

Once you have identified the team that will be participating in the flow, begin addressing the following questions:

1. Do you understand the "flow" that is being built?
2. What are the inputs and outputs?
3. What APIs are you exposing? Who is calling those? They are your peer reviewers.
4. What APIs are you consuming? Who owns those? They are also your peer reviewers.
5. How are you managing your components and microservices?

## How do I commit (check in) the PlantUML diagrams or changes to the Clearwater repository?

Watch supporting [VIDEO 1](https://web.microsoftstream.com/video/9a58adf6-624b-49e7-a68b-6c6433a49e73?list=studio) and [VIDEO 2](https://web.microsoftstream.com/video/5836f0ed-499e-452a-827f-6f7b08764283?list=studio) or read the instructions below.

1. Create (or update) the sequence diagram using PlantUML. For syntax information, see PlantUML Syntax below.
2. Save the diagram in the local repository using the taxonomy defined in the [md](https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/README.md).
3. Validate that the sequence diagram renders correctly.
    1. If you're using an IDE plugin, you can view your work as you are making changes.
    2. You can also run the jar file directly, see the [/docs](https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/docs/crt/tools/plantuml/files/plantuml.jar) folder of the project.
4. Start Docker. If you don't have it installed, see the section for installing and running Docker.
5. Run the *build.sh* script to create the new Docker image of the flow site locally.
6. Use the *run.sh* script to test the new or updated diagrams locally with the updated Docker image.
7. Open Git Bash and make sure to run it as an administrator.
8. In the Git Bash command window, check in your fork of the Bitbucket repository by typing the following:

   * **git add** \&lt;filename\&gt;
   * **git commit**  **- m** (use **-m** to add a comment)
   * **git push**

9. Test the changes in the local host:-http://localhost:9000/.
10. Submit a pull request.
   1. Open Bitbucket and navigate to your fork.
   2. Click the **Create pull request** icon on the left.
   3. Request a review from your peer reviewers. Please be sure to write appropriate comments, so your reviewer can understand the scope of the changes.

11. The web site, [https://flows.corporate.t-mobile.com](https://flows.internal.t-mobile.com/), will be automatically updated daily and will reflect the newly added sequence diagrams and Swagger documents.

## Why we love UMLs and the PlantUML tool

PlantUML is an open-source tool allowing users to create UML, or Unified Modeling Language, diagrams from a plain text language. While the tool can produce several different types of UML, we are focusing on the sequence diagram, as it can model a large amount of information with relatively small effort. These diagrams offer a **clear** blueprint of interactions between development teams and tools through the development lifecycle.

<img src="/images/Grab-n-Go_diagram_exp.png" width="400" alt=""/>

**Why Clearwater loves UML diagrams:**

- It builds a solid understanding of how the subsystems interact and is used as the base for product documentation.
- Project requirements can be better scoped and understood by having solid documentation.
- Sequence diagrams can be easily iterated upon, starting with a coarse level of detail, then evolving as more information becomes available.
- These sequence diagrams can serve as the basis for operational processes, reducing MTTR and facilitating troubleshooting.
- This process will serve as a catalyst for teams to change how they interact -- for each flow, or sub-flow, the accountable engineers can be identified, and a &quot;mini-JAD&quot; can be organized around just that area.

**Why Clearwater loves the PlantUML Tool:**

- Quickly create clear diagrams
- Focus on function over form
- Easy to edit over time, since the layout is automatic
- Is text-based to allow for diffs and versioning
- Allows for easy exporting and linking to diagrams

## PlantUML Syntax

The syntax for PlantUML is simple, see [http://plantuml.com/sequence-diagram](http://plantuml.com/sequence-diagram) for details.

**Important**: Do not put T-Mobile sequence diagrams on [http://plantuml.com/sequence-diagram](http://plantuml.com/sequence-diagram).


```
@startuml
Alice -> Bob: I have a secret
@enduml
```

<img src="../images/Grab-n-Go_diagram2.png" width="175" alt=""/>

## How do I build Swagger files?

1. Identify the new APIs you want to build and add them to your Bitbucket repo.
2. Update existing APIs. When updating an API, make sure that they're backwards compatible.
3. Check in your fork of the Bitbucket repository.
4. Submit a pull request using the steps below.
    1. Open Bitbucket and navigate to your fork.
    2. Click the **Create pull request** icon on the left.
    3. Request a review from your peer reviewers.

## What are some best practices when using Swagger?

Here are some guidelines for using Swagger:

- When writing Swagger files, you should be as specific as possible.
- Add descriptions for each Swagger file you generate.
- Make sure you are clear whether a field is required or optional.
- Add some input validations, such as:

  - Review the format directive for built-in types.
  - Is it a number or an integer?
  - If it's a string, consider using a regular expression and/or minimum and maximum qualifiers.

- Your project should pull in the Swagger files from the repository with every build. This ensures you always have the correct version in your code.
- Your project should leverage *swagger-codegen* to build the *model* for calling the API with every build. This ensures that the input validations are being honored, and no defects are introduced due to misunderstanding the API definition. This also allows peer reviewers to quickly iterate on the Swagger definition: field changes become simple recompiles, instead of analyzing documentation to determine what has changed.

## Glossary of terms

| Term | Definition |
| --- | --- |
| Flow | A method of documenting the stages involved in a specific procedure and the relationships between key components. |
| Critical Flow | Refers to the technical or system flow that&#39;s at the core of a specific use case, scenario, or feature. For example, when customers request an upgrade path through the web channel, the critical flow is the system or technical flow that&#39;s leveraged or impacted during the upgrade. |
| Contracts | An agreement between two or more teams outlining what expectations need to be established by each team. |
| Autonomous Team | Unifies around a specific goal without the direct influence of an outside party. The members of the autonomous team are given latitude to establish their own internal goals and work practices. |
| Async Communication | Communication that occurs when all participants aren&#39;t online or available at the same time. |
