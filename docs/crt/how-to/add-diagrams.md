---
title: "Add Sequence Diagram"
draft: false
weight: 100
---

We're going to need some way to identify the flows, and to be able to reliably link them together in a web format.  For now, let's go with a simple taxonomy and a flat directory structure.

### Naming

Reverse dot notation:

* org
* domain
* team
* system
* flow

For now, the org will always be "CE".  The domain is "DD" or "DSG".  The team is one of the ones below:

 ID  | Description
---- | -----------
FL   | Frontline
SS   | SelfService
DXT  | Digital Transformation
EDS  | Enterprise Data Services
BD   | Billing Domain
PCF  | Pivotal Cloud Foundry
CD   | Customer Domain
CFAM | Credit and Finance Management
DD   | Documents Domain
DM   | Device Management
TBCE | Tibco Cloud Enterprise
TD   | Trade In
OD   | Order Domain
WPC  | WEB Product Catalog
RSP  | Retail Service Platform
SC   | Supply Chain

> Feel free to add to this as necessary

The `system` and `flow` are more loose, for the initial "Upgrade" flow it makes sense to use "esvcs.upgrade" or "esvcs.upgrade.accessory."

So a logical start would be: `ce.dd.ss.esvcs.upgrade.puml`.

Oh, the .puml extension is important for the scripting to pick it up to render the diagrams.
