---
title: "Access the Clearwater Repository"
draft: false
weight: 300
---

## Request Access in [#edp-support](https://t-mo.slack.com/messages/C60G34X7C/convo/C02KY507N-1532031430.000087/)

Provide your name, NTID, and manager's name when making the request.

## Self-Signed Root Certificate: git Trust Procedure

These instructions will cause a (self-signed) root cert to be trusted when you use git in the future, and already trusted root certs will continue to be trusted.

1. WINDOWS: Start a git bash window and enter "git config -l" command.

	Mac or Linux: Start a terminal window and enter "git config -l" command.

2. From the bottom of the listing, find the first instance (moving up) of the name "http.sslcainfo". 
   On a Windows machine this could read: http.sslcainfo=C:/Program Files/Git/mingw64/ssl/certs/ca-bundle.crt
   
    NOTE the forward slashes, even on Windows! 
   
    NOTE: If an entry for http.sslcainfo does not exist, your git/machine may trust any SSL/TLS root cert--in which case these instructions will likely not help!

3. Open the file identified in step 2 in a code or simple text editor (on windows the editor must be running as administrator).
   
4. Using your browser on the repo web site, copy the contents of the repository file "tmrootcert.pem" into the clipboard.

5. Append the clipboard contents to the end of the file opened in step 3.

6. Save the file.

7. This step is important only on the first time you add a root certificate.
   To be certain that git updates do not overwrite the newly created file, you 
   should save it to a private directory, and then set the http.sslcainfo name 
   in git to have the value of the full path of the copied file. 
   e.g. Save the file to: /user/<your name>/gitCerts/ca-bundle-TMRoots.crt (on Windows, c:\users\<your name>\gitCerts\ca-bundle-TMRoots.crt) enter the command "git config --system http.sslcainfo "/user/<your name>/gitCerts/ca-bundle-TMRoots.crt" (on windows, c:/users/<your name>/gitCerts/ca-bundle-TMRoots.crt)
 
NOTE: any number of root certificates can be appended to the file in this manner, and if the private file is edited each time, then step 7 is unnecessary. 