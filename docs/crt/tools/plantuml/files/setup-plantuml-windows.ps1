###################################################################################
#                                                                                 #
# Installs Windows tools for creating, editing, and previewing PlantUML diagrams. #
#                                                                                 #
###################################################################################

function RefreshEnvPath
{
    $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") `
        + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")
}

iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
RefreshEnvPath

# 
# PlantUML Dependencies
#

choco install jre8
RefreshEnvPath
choco install graphviz --yes

#
# Clearwater Tools
#

choco install visualstudiocode --yes # includes dotnet
RefreshEnvPath
code --install-extension jebbs.plantuml

choco install git --yes
choco install tortoisegit --yes

# 
# Sample Diagram
#

$text = '@startuml verification
left to right direction
skinparam packageStyle rectangle
actor customer
actor clerk
rectangle checkout {
  customer -- (checkout)
  (checkout) .> (payment) : include
  (help) .> (checkout) : extends
  (checkout) -- clerk
}
@enduml'

$text > 'sample.puml'

code sample.puml

#
# Additional Steps & Resources
#

Start-Process http://plantuml.com/
Start-Process https://bitbucket.service.edp.t-mobile.com/plugins/servlet/ssh/account/keys

Write-Output "Please make sure you've added an SSH key to your BitBucket account! I've gone ahead and opened your web browser to the appropriate page."
Write-Output "I've also opened the PlantUML website so that you can learn how to create diagrams. All the tools you need are now installed and running!"