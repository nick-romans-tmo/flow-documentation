---
title: "PlantUML"
weight: 200
---

[PlantUML](http://plantuml.com) is a simple app that generates various types of UML documentation -- specifically, it does amazing sequence diagrams.

Here is sample syntax for a simple sequence diagram:

```
@startuml
Alice -> Bob: I have a secret
@enduml
```

This generates a sequence diagram that looks like this:

{{< puml file="sample.puml" >}}

See the [product documentation](http://plantuml.com/sequence-diagram) for a simple usage guide.

## Get Started with PlantUML

**With Docker:** If you're a fan of Docker, you can run the [`build-docker.sh` script](files/build-docker.sh) to create a container local to your machine.  The you can put the [`txt2png` script](files/txt2png) in your PATH, and henceforth all your sequence diagramming needs can be met with a `txt2png input.puml`.  W00t.

**With Java:** If you prefer the Java route, it's just `java -jar plantuml.jar input.puml`.  That's the same thing, just a little more unweildy, and you have to mess about with pathing and stuff.  Or you can just omit all the parameters and get the GUI.

**On Windows (VSCode):** If you just want to get started quickly on Windows, download [setup-plantuml-windows.ps1](files/setup-plantuml-windows.ps1) and run it in PowerShell as an administrator. It will install PlantUML dependencies (jre8, graphviz) plus [VS Code](https://code.visualstudio.com/) with the [`jebbs.plantuml` plug-in](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml) so that you can preview your diagram as you type:

![PlantUML VS Code plug-in](https://github.com/qjebbs/vscode-plantuml/raw/master/images/auto_update_demo.gif)

*Help wanted:* Write an equivelent script for MacOS using Brew.