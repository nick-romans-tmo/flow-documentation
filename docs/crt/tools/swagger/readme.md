---
title: "Swagger"
weight: 100
---

Swagger is the language that's to represent REST APIs and the JSON document structures that are involved in those transactions.  We love it, but sometimes it's hard to read, and it sure would be useful to have an HTML representation.

## Visualize Swaggers using Docker

1. Navigate to the directory containing the file.
2. Enter this command: `docker run -p 1333:8080 -v $(pwd):/usr/share/nginx/html/local swaggerapi/swagger-ui`
3. Open your browser to `http://localhost:1333`
4. In the edit box at the top of the page, type `local/swagger.json` (replacing "swagger.json" with the name of your json or yaml)

## Visualize Swaggers using VS Code

If you are using VS Code, you can install the [Swagger Viewer plug-in](https://marketplace.visualstudio.com/items?itemName=Arjun.swagger-viewer).