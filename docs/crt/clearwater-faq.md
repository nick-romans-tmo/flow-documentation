---
title: "Clearwater FAQs"
draft: false
weight: 500
---

## Clearwater Process Improvement - FAQ

**Note**: Feel free to ask any questions to the Clearwater community. Join us on the [#clearwater](https://t-mo.slack.com/messages/C8D77CK7H/) Slack channel. You can also reach the Clearwater core team by emailing us @[mailto:Clearwater@t-mobile.com](mailto:Clearwater@t-mobile.com) for any suggested changes/edits to this documentation.

Use this FAQ to find answers to commonly asked questions and solutions to issues encountered when using Clearwater. Also, please join the Slack channel [#clearwater](https://t-mo.slack.com/messages/C8D77CK7H/details/) where you can post your questions, share your successes, learn from others, and stay current on Clearwater.

### General

**Q** Where can I find a list of resources to get started using Clearwater?

**A** Here's a list of resources to help you get started with Clearwater:

- [Clearwater resources page](https://tmobileusa.sharepoint.com/sites/eit/clearwater-resource-page). This site contains links to various Clearwater training, tools, and communication channels. Everything you need to know to get started!
- [Clearwater Bitbucket repo](http://tm/clearwater). This repo contains the Swagger documents and sequence diagrams as well as the tools and resources you need to use Clearwater. You can also find sample files on the repo. You need to go to the Slack channel [#edp-support](https://t-mo.slack.com/messages/C60G34X7C/convo/C02KY507N-1532031430.000087/) to request access to the repo.
- [Clearwater Distillery site](https://flows.corporate.t-mobile.com/). For an easier way to search for specific API&#39;s, you can search via the Distillery site.
- [Clearwater dashboard](http://tm/CWswaggerScore). You can find many examples on this dashboard, which also lists all Swagger scores. For a good API example that you could use as a reference when writing your APIs, see the API example [here](https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.om/ProvisioningOrder.json).
- [#clearwater](https://t-mo.slack.com/messages/C8D77CK7H/) Slack channel. We encourage you to openly discuss anything Clearwater related in this Slack channel. All our OTD&#39;s and champions monitor this channel regularly and can help with anything as it pertains to Clearwater

**Q** Where and how does Clearwater fit in with the SLDC process?

**A** Clearwater is designed to ladder up into the new Iterative Design Process. Once the domains are identified after the HLSD is developed, the flows will be identified, and the Swagger documents will be expanded. The Clearwater process will be utilized by teams that produce and consume APIs.

**Q** At what stage in the project would the Clearwater involvement start?

**A** The stage is design. You could start at the HLSD or at the funding stage, if there&#39;s a high-level of confidence. If not, then start it as part of your solution design process after funding, which should be reasonably mature when you start to write your first code.

**Q** What is the targeted audience for Clearwater?

**A** Clearwater applies to everyone. However, Dev teams that create or consume APIs to/from other Dev teams will be the primary consumers. While Clearwater is a design time activity to get these contracts started, the usability of the information is applicable to everyone. For example, testers use the Swagger documents to plan API testing and also to capture request/response logs from Splunk and use them to troubleshoot.

**Q** Is Clearwater used for all types of software development or only specific types of software development?

**A** Clearwater is for all types of software development. It&#39;s primarily aimed at simplifying the design and creation of REST APIs exposed between development teams.

**Q** Can you provide the roadmap for when all the projects will be in Clearwater?

**A** No, not via domain. The focus should be on Clearwater being a process and behavior change as opposed to a repo.

**Q** How is Clearwater going to be enforced?

**A** Clearwater will be enforced by using automated quality checks, such as Lint, and with Swagger peer reviews. We want to build on peer-to-peer reviews to create a community-driven, world-class development shop that&#39;s good at self-monitoring.

**Q** Does Clearwater mandate or tie up to one specific developer portal or is it open?

**A** We want to promote the API Developer portal as a place where developers can collaborate and discuss issues. Eventually, the APIs will be heavily reused.

**Q** What is the role for analysts as it relates to Clearwater?

**A** Analysts will document the flows and help with defining and handing off APIs. Their role will vary. The end goal is getting the developers talking to other developers. The &quot;as is&quot; and &quot;to be&quot; flows that the analysts document and leave behind should continue to evolve and not be versioned or forgotten.

**Q** How does Clearwater align with the API Developer portal and API Gateway?

**A** Clearwater and the API Developer portal are complementary. The API Developer Portal provides broad exposure at the DTD and partner level, and it publishes what&#39;s in production. Clearwater is an internal development process that will be the source for generating or modifying the APIs and Swagger documents which will eventually be published to the API Developer portal.
The API Gateway is an architectural pattern. Since Clearwater is a process improvement effort, it does not use the API Gateway.

**Q** There are three repos for APIs. Is there a plan to eventually merge all three? The three repos are: https://flows.corporate.t-mobile.com, the Bitbucket repos (various paths), and the Developer portal.

**A** No, Clearwater and the Developer portal are separate things that have separate jobs. The Developer portal is the clear definition of your API and how it&#39;s used, and this portal represents what is in production. Clearwater is the working portal that's used in runtime to modify and improve work in progress. It pushes the APIs to the Developer portal once it goes to production.

**Note** : Before you can access the Clearwater Bitbucket repository (http://tm/clearwater), you need to go to the Slack channel [#edp-support](https://t-mo.slack.com/messages/C60G34X7C/convo/C02KY507N-1532031430.000087/) and request access.

**Q** How do you decide which repo to use, and how do all the stakeholders find what they need?

**A** Clearwater uses https://flows.corporate.t-mobile.com and the various Bitbucket repos. You can find information on how to add sequence diagrams and Swagger files [here](https://flows.corporate.t-mobile.com/crt/how-to/). You also need to leverage the [taxonomy](https://flows.corporate.t-mobile.com/crt/how-to/add-diagrams/) to determine the naming convention to use. The Clearwater repo, https://flows.corporate.t-mobile.com, stores the sequence diagrams and the Swagger documents. When there&#39;s a change within the repo, a pull request is used where reviewers must review/approve the change. An audit process is also run against the repo, and the results are presented to leadership on a quarterly basis.

**Note** : Clearwater [Bitbucket repository] (http://tm/clearwater) is public within T-Mobile, hence should be accesible to any T-Mobile network users as long as they have EDP access. EDP, Enterprise Delivery Pipeline, is the bitbucket server instance that hosts many repositories including Clearwater. If you are unable to access, you likely need EDP access, for which you need to go to the Slack channel [#edp-support](https://t-mo.slack.com/messages/C60G34X7C/convo/C02KY507N-1532031430.000087/) and request access.

**Q** Are all teams in P&T (formerly EIT or DTD) and Engineering, going to be included in the Clearwater process?

**A** Yes, Clearwater was deployed within CE (Customer Experience team within P&T) first. But now it is applicable and used across all P&T teams. Rollout and use within Engineering is going to be tried next.

**Q** Will this morph into a service catalog UI, so that people can log in and browse what&#39;s available or under development?

**A** Yes, developers can go to [https://flows.corporate.t-mobile.com](https://flows.corporate.t-mobile.com)to see a current list of the sequence diagrams.

**Q** Since APIs and Swagger documents primarily apply to application flows, what about _data sources-to-data sources_ flows (for example, Golden Gate replications)? Should they follow same approach?

**A** For APIs, Swagger documents, and sequence diagrams, the purpose is to clearly define the contract between two domains. It doesn't define what happens within the domains. If you want, you can document your internal domain pieces within the sequence diagrams. Some teams are fully exposing their internal flows. Note that sequence diagrams should show the full top-to-bottom flow.

### Process

**Q** Who is responsible for creating the sequence diagrams, the architect or the lead developer? How do they know who is assigned and who is responsible?

**A** System analysts, architects, or developers who understand the flow are responsible for creating the sequence diagrams. As long as you are qualified and understand the flow, then you can create the diagram. Over time, directors, the lead architects, and lead system analysts will be assigned ownership to every major flow.

**Q** For the initial step of the flows (and, specifically, for flows that might be complex), what is the vision for how this might work?

**A** As you create the flows, when there is a dependency to downstream flows, you would link to that. This allows anyone looking at the flow diagrams to navigate end-to-end. Please make sure the link URL you want to reference is in following format: _&quot;\&lt;domain\&gt;/\&lt;name of flowdiagram\&gt;.svg&quot;_.

**Q** Where can I find an example to use as a reference when writing API&#39;s for my applications?

**A** You can find a good API example [here](https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.om/ProvisioningOrder.json). You can find other examples at the [Clearwater Distillery](http://tm/CWswaggerScore), which lists all swagger scores.

**Q** Should you follow the Clearwater process only when the integration is between UI and a domain service or between multiple domain services?

**A** Clearwater should be followed when integration is needed between multiple domain services.

**Q** What if the development teams working on a project are following different methodologies, such as Agile or Waterfall, what happens then?

**A** As long as teams understand the dependencies between the teams via flow diagrams, it doesn't matter what methodologies teams follow.

**Q** When a new program comes in with a lot of new flows (for example, activation), how does that break down?

**A** For part of the Iterative Design Process from the E2E Architecture team, you have an initial E2E flow and then you break it down into the individual calls. The lower-level Swagger documents are what will be leveraged with Clearwater.

**Q** Are the sequence diagrams scoped to a specific user story or feature?

**A** No, they are scoped to E2E flows (such as upgrade flows).

**Q** Since the Clearwater process must be followed between multiple dependent domains, how will we ensure there is proper governance and communication? If the backend specification changes, who will manage the dependency matrix and inform the impacted parties?

**A** A backend specification cannot change because this violates the contract. However, new versions can be added, and old versions can be deprecated. This needs to be negotiated between the producer and the consumers (who can be identified via API Gateway reporting).

**Q** Is API versioning a part of Clearwater?

**A** Once an API is published, it cannot change. If you need to add or change the API, you need work with the other teams to come to a consensus. We don't want mini-versions of APIs – there shouldn't be a v1, v2, etc. APIs also need to be backward compatible. If, for any reason, it cannot be backwards compatible, then the APIs need to be versioned. When ready, APIs are promoted to the API development catalog where we'll get heavy reuse of the APIs. This will lead to less rework on the APIs. Also, we'll publish to the API gateway, so we can track who's using what API.

**Q** Some projects require us to start the work before all intake is done which is not something that's going to change.

**A** That's not a problem. You can begin this work at any time whether intake is fully completed, as long as all teams collaborate on the flows.

**Q** Once you have all the API's, what will the deployment model be?

**A** The APIs are released to the Developer portal and then onboarded to the API Gateway.

**Q** My team has uploaded a Swagger file to the Clearwater Bitbucket repo. What are the steps to get it published in the developer portal?

**A** You can find the instructions to publish your API documentation here: [https://developer.t-mobile.com/content/publish-api](https://developer.t-mobile.com/content/publish-api).

**Q** What should you do when an API is being developed for one project, but another group is leveraging that same API?

**A** Ask for your own API, even if it's mostly duplicative so it is less impactful if changes are made.

**Q** We have identified duplicate APIs that accomplish the same thing. Are there any processes or check points in Clearwater that address this issue?

**A** We're not yet sure how we should handle duplicate APIs, but we need teams to meet and simplify on one API.

**Q** Are there any plans to apply Clearwater to our current Samson system (which currently has multiple interfaces that go into it) and use a service-based API approach?

**A** The goal is to initially focus on CE with the intent to take Clearwater to the broader organization once we work through the most appropriate approach to take with vendors, etc.

**Q** Who will be responsible for stitching together the different diagrams coming from different domains into one end-to end sequence diagram in Clearwater?

**A** You don't create a single PlantUML sequence diagram that stitches together an end-to-end flow and store it in the Clearwater repo. Instead, flows for each domain are documented and stored as separate documents. PlantUML does allow us to link them together. This gives anyone viewing the sequence diagram from the top of the stack the ability to navigate all the way down to the bottom of the stack, and, understand, the flows end-to-end. Here is an [example](https://flows.corporate.t-mobile.com/all-diagrams/sequence_diagrams/ss/myt/ce.dd.ss.esvcs.authentication.as-is.puml/). You can navigate all the way to the bottom of the stack using the hyperlinks in the diagram.

### Contracts

**Q** What are contracts?

**A** Contracts are inputs and outputs for an API. The API provider collaborates with API consumers to define these and ensure everyone has a common understanding.

**Q** What if you don&#39;t know who the consumers are? Are there any bindings to who is consuming the information??

**A** The list of people that are consuming the APIs should be identified at the beginning of the design process. The consumers can be identified through API Gateway reporting.

**Q** When does the contract become binding between teams?

**A** This depends on the change, the impact of the change, and the timelines associated with project. Based on the criteria, development teams will set this date themselves.

**Q** What will trigger the contract to change?

**A** Any new requirement, capability, or enhancement bug can trigger a contract change. In some cases, teams are effectively only supporting a single version of their API and then expect all users to change. When the contract changes, and the team that is consuming the flow is not ready for the change, then the APIs would need to be backwards compatible. If, for any reason, it cannot be backwards compatible, then the APIs need to be versioned.

### Third-party vendors and partners

**Q** How does the Clearwater process handle third-party vendor development, such as wholesale where our connectivity comes from an internal platform to a vendor-hosted platform?

**A** Assuming the vendor provides a Swagger document, a sequence diagram should still be created and published. If the vendor is willing to participate, partner with the vendor and ask them to adopt this methodology, if possible.

**Q** Since we work on the back-end ERP applications and the development is done through a managed service provider, how do they integrate into this framework?

**A** We will be introducing Clearwater to our partners as well, and the diagrams will make it easier for Operations.

**Q** A lot of our projects utilize partners, such as Amdocs and Capgemini, to develop APIs for us. How do we engage them to use Clearwater?

**A** When we engage with our partners, using Clearwater should be part of the standard process – all APIs should leverage Clearwater for all partners.

**Q** How should the Clearwater process work with managed services that are set up like Amdocs?

**A** Once we get Clearwater working appropriately in CE, we'll start bringing on other domains and third-parties. Using Amdocs as an example, if they're creating APIs for us to consume, we'll ask that they contribute to the Clearwater repository, just like everyone else. If they have concerns, then we can address those on a case-by-case basis. Overall, with third-party agreements, we'll start to call out this process in the SOW's/contracts, as they come up for review.

### Tools and training

**Q** What are the tools and products associated with Clearwater? What training will be available for those who need it?

**A** To help you get started, a Grab and Go document is available on the [Clearwater Distillery site](https://flows.corporate.t-mobile.com) and additional training where you can review the current Swagger documents that are posted there and read through some peer review best practices. There will also be formal and informal training, and more training on [Cornerstone](https://t-mobile.csod.com/LMS/catalog/Welcome.aspx?tab_page_id=-67&amp;tab_id=-1).

**Q** What is Swagger?

**A** Swagger is a set of open-source tools for designing, building, documenting, and consuming REST APIs. Good Swagger files should define examples for request and response body, parameters, object and properties, and arrays. They should reuse examples from multiple places within Swagger by defining them under the components section. You can find some examples of good Swagger files at: [http://tm/CWswaggerScore](http://tm/CWswaggerScore) and https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/ce.dsg.om/ProvisioningOrder.json.

**Q** Where can I find examples of Swagger files and sequence diagrams of the &quot;as is&quot; flow and &quot;to be&quot; flow?

**A** You can find examples on [https://flows.corporate.t-mobile.com/](https://flows.corporate.t-mobile.com/).

**Q** Where can I find some example PlantUML scripts, and do you have an example where a pull request is automating this PlantUML graphic?

**A** You can find examples on [https://flows.corporate.t-mobile.com/](https://flows.corporate.t-mobile.com/).

**Q** What is the recommended approach for viewing PlantUML diagrams?

**A** To view PlantUML diagrams, use Visual Studio Code with the [PlantUML plug-in](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml). You can also use other IDE tools, such as Eclipse and IntelliJ, as extensions for PlantUML.

**Q** Is the repository used only for capability API&#39;s or for experience API&#39;s as well?

**A** Any exposed contract that you plan to honor should be placed in the repository. If you change something and it causes a cascading effect to other APIs, then it should also go there.

**Q** Can I use SourceTree along with Bitbucket?

**A** Yes, for more information, see [https://confluence.atlassian.com/bitbucket/copy-your-repository-and-add-files-729980492.html](https://confluence.atlassian.com/bitbucket/copy-your-repository-and-add-files-729980492.html).

**Q** Since I already have most of my sequence diagrams created in Sparx EA, do I have to recreate the sequence diagrams in PlantUML?

**A** When recreating the Sparx EA sequence diagrams in PlantUML and then storing them in the Clearwater repo, you would be doing a huge favor to your upstream/downstream domains (assuming they also bring their diagrams into Clearwater). We would be able to link those diagrams and create an end-to-end picture. Anyone with a GSM900 ID can view and access the Swagger documents as well as the sequence diagrams. Sparx EA requires a license, and therefore, not everyone has access. By recreating the diagrams in PlantUML and then storing in the Clearwater repo, it will help upstream and downstream domains. The process is lightweight and is designed to create end-to-end transparency with simple OSS tools.

**Q** What is [**http://tm/groove**](http://tm/groove)?

**A** You can go to [http://tm/groove](http://tm/groove) to create a new API proxy code from Swagger. It can also score your Swagger (using the Apigee CoE Swagger Scoring API) directly from the UI. You can either upload the Swagger file from your local machine or paste the Bitbucket URL (including the Clearwater repo) into the UI. Once you see your score, you can choose to go back and improve the Swagger or create the API proxy on the spot. It&#39;s currently in Beta, so please provide feedback and suggestions at the Slack channel [#groove](https://t-mo.slack.com/messages/C9BJZ29K6/). Now the latest version of this can be accessed on https://devcenter.t-mobile.com/ in API Journey: https://devcenter.t-mobile.com/devJourney?artifactName=apigeeAPI

**Q** Do you have a plan for Clearwater to be presented directly to individual teams?

**A** You can send email to [Clearwater@T-Mobile.com](mailto:Clearwater@T-Mobile.com) to discuss the need for individual presentations. There are also several Clearwater Process Improvement training modules that are available on the [Cornerstone](https://t-mobile.csod.com/LMS/catalog/Welcome.aspx?tab_page_id=-67&amp;tab_id=-1) training site.

**Q** Is there a tool I can use to compare two Swagger files, or do I need to do a manual check?

**A** There is no available comparison tool, but you can use the [Swagger Linter](https://api-linter.devcenter.t-mobile.com/) tool to validate the Swagger file and suggest improvements to it. If you need to directly compare two Swagger files, we recommend you do a manual visual comparison of the text.

### Miscellaneous

**Q** What percentage of Clearwater sequence diagrams currently exist in production?

**A** We don&#39;t know the true percentage, but we believe the number of total flows is under 20% across the company (perhaps even 10%). Creating sequence diagrams is mandatory across Warren Cody&#39;s organization. Other groups are getting involved, and we&#39;re gaining maturity across the company. The expectation is that anytime an API is touched, we should be applying Clearwater principles to those updates.

**Q** What does Clearwater add that the normal source control and code review process do not?

**A** Source code control is a tool, and Clearwater is a process that uses that tool. Code reviews are also processes.

**Q** Will Clearwater apply to New Finance System (NFS) at some point going forward?

**A** If we are going to be &quot;API first&quot; developers, Clearwater will eventually apply to everything that exposes APIs.

**Q** Is Clearwater specific to the development and consumption of microservices?

**A** No, it&#39;s not specific for microservices. Clearwater is applicable to all new products.

**Q** Who defines the Swagger documents outside of the sprints?

**A** It depends. We need to collectively determine how to do this outside of the sprints. Swaggers should be defined and fairly solid as we enter the sprint.
API specifications (Swagger docs) should be created during the design phase by an architect collaborating with analysts. Once in the development phase, the architect can work closely with development teams to ensure API implementation aligns with the defined Swagger document. Any changes identified during development need to be incorporated back into the API specification. To get early feedback, when a pull request is submitted to the Clearwater repository, you should include other analysts and architects from teams that are consuming the API as reviewers.

**Q** How is Clearwater compatible with current automation scripting? (Elaborate on the automation pieces).

**A** Clearwater is still a process improvement effort. It&#39;s not intended to dictate development tooling, scripting, or other mechanisms teams might be using.

**Q** If a shared API gets modified, how are the impacted teams notified? Do we know who they all are?

**A** If a shared API gets modified, you should open a pull request and set all analysts and architects from the consuming API team as reviewers for the pull request. The reviewers need to approve the pull request. The list of people that are consuming the APIs should be identified at the beginning of the design process. Also, for an existing API, Apigee should be able to report who all the consumers are.

**Q** What triggers the API pull request?

**A** The architect who updates the API spec due to an identified change will open a pull request to merge those changes to the Clearwater repository.

**Q** How will existing APIs be migrated into Clearwater?

**A** The existing APIs that are currently under investigation will need to be prioritized and iteratively pulled over to Clearwater.

**Q** Does Clearwater create a type of &quot;API MarketPlace&quot; where the dev teams can see what APIs are available and what data is available rather than trying to find this information in another manner?

**A** Yes, in many ways, Clearwater and the Developer portal are &quot;self-service&quot; places where developers can go to get API information without having to spend a lot of time contacting various people to search for APIs. The APIs are all located in one central location.

**Q** Can teams subscribe to specific APIs and then publish something to them whenever a change occurs?

**A** As part of Clearwater process improvement, the change we are making is that the API provider needs to include the architects and/or analysts that are consuming the APIs as reviewers when they open a pull request. An email notification automatically goes out to the reviewers and then gives them an option to review the change.

**Q** Since we're implementing Clearwater, what's the plan for using Sparks going forward?

**A** Clearwater is not a replacement for Sparks, but a complement to Sparks. At its core, Clearwater is used by development teams to help us communicate better between different teams.

**Q** Should proof of concept (POC) designs be a part of Clearwater, or are POCs only for the real projects?

**A** There is no restriction on using POCs with Clearwater.

**Q** Is there a direct tie between Clearwater and an email Cody sent out that discussed each team having an API Czar?

**A** True, we are introducing a couple of additional concepts. One of which is an API compliance tool, and as that matures, it can be leveraged by the API Czar.

**Q** Is there any specific API versioning type we are going to use for Clearwater?

**A** You can find information on API versioning in the Clearwater training modules that are available on the [Cornerstone](https://t-mobile.csod.com/LMS/catalog/Welcome.aspx?tab_page_id=-67&amp;tab_id=-1) training site and also on the Slack channel [#clearwater](https://t-mo.slack.com/messages/C8D77CK7H/details/).

**Q** I have a Swagger file that has 5 operations in it. All 5 operations are logically related, for example, insert, select, and update, which are all in same the Oracle database. For this scenario, if I want to change the Swagger file (per the Clearwater process), do I need to decouple all these APIs into Independently deployable APIs? Or, is it still all right to have a one deployable component for all 5 APIs since they are related to each other?

**A** In this scenario, one deployable (Spring Boot microservice containerized) should suffice. This means that all CRUD APIs could exist in one deployable API. The &quot;bounded context&quot; is the thing that we try to model in a microservice -- which sounds like what you&#39;ve done. Microservices don&#39;t always equal API&#39;s as they don&#39;t need to be 1:1 (and often aren&#39;t).

**Q** Is there a plan for legacy SOAP services in Clearwater?  Also, should we include WSDLs in this process?

**A** You should document sequence diagram and link to the WSDLs from the diagram, if needed. We could create a placeholder for WSDL in the repo to make sure that they don&#39;t get mixed up.

**Q** Our team has existing PlantUML diagrams for SOAP APIs, and we do not have REST APIs. Do we need to put them in Swagger files and convert them to REST APIs? Our SOAP APIs are a private implementation.

**A** There is no mandate to convert to REST APIs, but the decision will be determined on a case-by-case basis. You should be aware that Clearwater will eventually move on to other types of APIs and other interfaces. Clearwater does have controls in place to ensure security for the private APIs.

**Q** For the peer review process, you are implementing any kind of &#39;lessons learned&#39; about the process to improve it?

**A** We are capturing feedback and identifying issues by reviewing the pull request comments in Bitbucket and by collecting input directly from teams. The information that we gather in the pull request is also useful because the comments are archived in Bitbucket with the artifacts themselves. If we find issues that need to be addressed, we&#39;ll make changes to the process and document those changes in this Clearwater FAQ document and other documents.

### Issues

Find solutions to issues you may encounter using Clearwater in the list below.

**Issue**: SourceTree allows you to commit to a repository without promoting or resolving any conflicts that occur.

**Solution**: Use Git Bash instead of SourceTree, which resolved the issue. This option is a workaround and does not identify the actual issue.

**Issue**: The following error message appears:

```
Permission denied (publickey). fatal: The remote end hung up unexpectedly.
```

**Solution**: The SSH keys are not configured, for more information, see [https://qwiki.internal.t-mobile.com/display/JCM/Setting+up+SourceTree+for+EDP+Bitbucket](https://qwiki.internal.t-mobile.com/display/JCM/Setting+up+SourceTree+for+EDP+Bitbucket).

**Issue**: An unsecured error occurred in Chrome when trying to access the on-premises Swagger Editor. Also, when adding an exception to access the on-premises Swagger Editor in Firefox, a 503 error occurs.

**Solution**: The certificate is wrong as it's issued against another site. Therefore, HTTPS is not working. If you have Docker on your local machine, you can run the Swagger Editor locally by running this command:

```
docker run -d -p 80:8080 swaggerapi/swagger-editor
```

If you are using Atom Editor, use this Swagger linter for Basic syntax check.: [https://atom.io/packages/linter-swagger](https://atom.io/packages/linter-swagger).
For T-Mobile specific Swagger standards validation, you still need to run your Swagger against [Swagger Linter](https://api-linter.devcenter.t-mobile.com/)

**Issue**: Can't validate a sequence diagram in Chrome/QWIKI.

**Solution**: In Chrome, open the file you created to instantly validate the diagram. To download Chrome, see [https://chrome.google.com/webstore/detail/plantuml-viewer/legbfeljfbjgfifnkmpoajgpgejojooj?hl=en](https://chrome.google.com/webstore/detail/plantuml-viewer/legbfeljfbjgfifnkmpoajgpgejojooj?hl=en).

**Issue**: When setting up Git for the first time, the following error message appears:

```
SSL certificate problem: self-signed certificate in certificate chain
```

**Solution**: This is caused by Git not trusting the certificate that's provided. Add the T-Mobile certificate to the list of trusted places.
