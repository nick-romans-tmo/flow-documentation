---
title: "Peer Review best practicies"
draft: false
weight: 200
---

<img src="/images/clearwater_logo.jpg" width="300" alt=""/>

Here are some Clearwater peer review best practices that you can follow to improve the reviews!

1. Be familiar with the REST design guidelines. Find the API Design Standards at [http://tm/apis](http://tm/apis).
2. Review the API Linter rules and use the helpful information. Find the API Contract Validation list at [http://tm/lintRules](http://tm/lintRules).
3. Be familiar with JSON/YAML Swagger notations and editor tools.
4. Review and familiarize yourself with the design documents, flow diagrams, and any other information to understand the functionality that's needed in the API.
5. Follow the [API Peer Review Checklist Sample](https://tmobileusa.sharepoint.com/teams/IDSS/Shared%20Documents/Initiatives/Clearwater/Clearwater%20Resource%20Kit/Swagger%20Peer%20Review%20Smaple.pdf).
6. When possible, consider a longer-term and more extensible design – rather than using limited exact use cases.
7. Review sensitive data considerations and ensure the API design complies.
8. Review API security guidelines and compliance at [https://developer.t-mobile.com/documents/api-security-guidelines](https://developer.t-mobile.com/documents/api-security-guidelines).
9. Include API authors and teams and then discuss the rationale for the recommendations and findings This is important because it becomes a valuable learning experience that teams will follow in the future. If we're not learning, then we might see similar issues being repeated.
10. Within a domain, a group of at least 2-3 members should develop expertise in the above best practices and share with others.
11. Review a sample of the [Clearwater Peer Review Process](https://tmobileusa.sharepoint.com/teams/digitaldevelopment/sda/Documents/Clearwater/Peer%20Review%20Process/Sample%20Clearwater%20Review%20Process%20-%20Supply%20Chain.pdf) as performed by the Supply Chain team.
