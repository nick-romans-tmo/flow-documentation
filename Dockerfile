FROM artifactory.corporate.t-mobile.com/sda-docker-release-local/sda/distillery-build:0.18.0-24

WORKDIR /clearwater
# Copy assets
COPY docker-entrypoint.sh ./
COPY docs/ content/docs
COPY swagger/ content/swagger
COPY sequence_diagrams/ content/sequence_diagrams
COPY content-distillery/ ./content-distillery

ARG ENVIRONMENT 
ENV S3prefix=${ENVIRONMENT}
ENV AWS_DEFAULT_REGION us-west-2

ENTRYPOINT ["/clearwater/docker-entrypoint.sh"]
